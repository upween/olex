var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var port = process.env.PORT || 7000;
var session = require('express-session')
var routes = require('./routes/index');
var multer = require('multer');
var bodyParser = require('body-parser');
const helmet = require('helmet');
var fs = require('fs');
//var csrf = require('csurf')
//var csrfProtection = csrf({ cookie: true })
var app = express();
app.use(helmet.frameguard({ action: "deny" }));
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
//var exceltojson = require("xls-to-json-lc");

var xlsxtojson = require("xlsx-to-json-lc");
//var xlstojson = require("xls-to-json-lc");
// exceltojson({
//   input: "pass the input excel file here (.xls format)",
//   output: "if you want output to be stored in a file",
//   sheet: "sheetname",  // specific sheetname inside excel file (if you have multiple sheets)
//   lowerCaseHeaders:true //to convert all excel headers to lowr case in json
// }, function(err, result) {
//   if(err) {
//     console.error(err);
//   } else {
//     console.log(result);
//     //result will contain the overted json data
//   }
// });
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: 'rojgarclient'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname,'upload')));
app.use(express.static('upload'));
app.all('/upload/*', function (req,res, next) {

  if (req.session.login){
    res.status(200).send();
  }
  else{
    res.status(403).send({
      message: 'Access Forbidden'
    });
  }
  
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// app.get('/form', csrfProtection, function (req, res) {
//   // pass the csrfToken to the view
//   res.render('send', { csrfToken: req.csrfToken() })
// });
// app.post('/process',  csrfProtection, function (req, res) {
//   res.send('data is being processed')
// });
routes(app);

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (req.cookies.modaltype == "jobfair"){
      cb(null, path.join(__dirname,'upload/jobfair/'))
    }
    if (req.cookies.modaltype == "photogallery"){
      cb(null, path.join(__dirname,'upload/photogallery/'))
    }
    if (req.cookies.modaltype == "itijobfair"){
      cb(null, path.join(__dirname,'upload/itijobfair/'))
    }
    if (req.cookies.modaltype == "jobfairresult"){
      cb(null, path.join(__dirname,'upload/jobfairresult/'))
    }
    if (req.cookies.modaltype == "iticompany"){
      cb(null, path.join(__dirname,'upload/iticompany/'))
    }
    if (req.cookies.modaltype == "placement" ||req.cookies.modaltype == "placementreport"){
      cb(null, path.join(__dirname,'upload/PlacementDetail/'))
    }
    if (req.cookies.modaltype == "careercounselling"){
      cb(null, path.join(__dirname,'upload/careercounselling/'))
    }
    if (req.cookies.modaltype == "gstdoc"){
      cb(null,  path.join(__dirname,'upload/gstdoc/'))
    }
	 if (req.cookies.modaltype == "pandoc"){
    cb(null,  path.join(__dirname,'upload/pandoc/'))
    }
  },
  filename: function (req, file, cb) {

    if (req.cookies.modaltype == "jobfair"){
      cb(null, req.cookies.JFTitle+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "photogallery"){
      cb(null, req.cookies.PGDesc+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "itijobfair"){
      cb(null, req.cookies.JFTitle+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "jobfairresult"){
      cb(null, req.cookies.JFTitle+'-'+req.cookies.Employer+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "iticompany"){
      cb(null,req.cookies.Employer+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "placement"){
      cb(null,req.cookies.RegNo+req.cookies.FileName+path.extname(file.originalname))
    }
	if (req.cookies.modaltype == "placementreport"){
      cb(null,'PlacementReport'+Date.now()+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "careercounselling"){
      cb(null,'careercounselling'+Date.now()+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "gstdoc"){
      cb(null, 'GSTDoc'+req.cookies.CompanyName+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "pandoc"){
      cb(null, 'PANDoc'+req.cookies.CompanyName+path.extname(file.originalname))
    }
  }
});
const upload = multer({
  storage: storage,
  limits:{fileSize: 200000},
  fileFilter: function(req, file, cb){
    checkFileType(req,file, cb);
  }
}).single('myFile');
function checkFileType(req,file, cb){
  // Allowed ext
  var ext = path.extname(file.originalname);
 if (req.cookies.modaltype == "jobfair" || req.cookies.modaltype == "placement" || req.cookies.modaltype == "itijobfair" || req.cookies.modaltype == "iticompany" || req.cookies.modaltype == "careercounselling"){
	  if(ext !== '.docx' && ext !== '.doc' && ext !== '.jpg' && ext !== '.pdf' ) {
      //console.log('Error: Document Only!');
            cb('Error: Document Only!');
        }
      
		else{
		return cb(null,true);}
 }
 else  if (req.cookies.modaltype == "photogallery"){
  if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
          cb('Error: Images Only!');
      }
  else{
  return cb(null,true);}
}
else  if (req.cookies.modaltype == "jobfairresult" || req.cookies.modaltype == "placementreport"){
  if(ext !== '.xls' && ext !== '.xlsx') {
          cb('Error: Excel Only!');
      }
  else{
  return cb(null,true);}
}
else if (req.cookies.modaltype == "gstdoc" || req.cookies.modaltype == "pandoc"){
  if( ext !== '.jpg' && ext !== '.pdf' && ext !== '.jpeg' && ext !== '.png' ) {
    //console.log('Error: Document Only!');
          cb('Error: PDF and Image File Only!');
      }
    
  else{
  return cb(null,true);}
}
}
app.post('/PlacementDetails', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 

app.post('/RegistrationXML', (req, res) => {

  // Now execute the 'OBJtoXML' function
  var output = OBJtoXML(req.body);
  console.log(req.body);
  var fileName = './upload/someFile.xml',
  buffer = new Buffer('<jobseeker>'+output+'</jobseeker>\n'+'</xml>'),
  fileSize = fs.statSync(fileName)['size'];
  console.log(buffer);
  fs.open(fileName, 'r+', function(err, fd) {
  fs.write(fd, buffer, 0, buffer.length, fileSize-7, function(err,res) {
  console.log(res);
  if (err) throw err
  // res.end('Registration Successfull! Your Registration will be generated in 24 hours');
  })
  });
  
  });



  
function OBJtoXML(obj) {
  var xml = '';
  for (var prop in obj) {
  xml += "<" + prop + ">";
  if(Array.isArray(obj[prop])) {
  for (var array of obj[prop]) {
  
  // A real botch fix here
  xml += "</" + prop + ">";
  xml += "<" + prop + ">";
  
  xml += OBJtoXML(new Object(array));
  }
  } else if (typeof obj[prop] == "object") {
  xml += OBJtoXML(new Object(obj[prop]));
  } else {
  xml += obj[prop];
  }
  xml += "</" + prop + ">";
  }
  var xml = xml.replace(/<\/?[0-9]{1,}>/g,'');
  return xml
  }

app.post('/JobFairDetails', (req, res) => {
  //console.log(req);
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 
app.post('/PhotoUpload', (req, res) => {
  //console.log(req);
  upload(req, res, (err) => {
    //console.log(req);
    if(err){
      res.render('pages/PhotoGallery/PhotoUpload', {
        msg: err,
        file:'',
        type: req.cookies.modaltype
      });
    } else {
      if(req.file == undefined){
        res.render('pages/PhotoGallery/PhotoUpload', {
          msg: 'Error: No File Selected!',
          file:'',
          type: req.cookies.modaltype
        });
      } else {
        res.render('pages/PhotoGallery/PhotoUpload', {
          msg: 'File Uploaded!',
          file: req.file.filename,
          type: req.cookies.modaltype
        });
      }
    }
  });
}); 
app.post('/ITI/JobFairDetails', (req, res) => {
  //console.log(req);
  upload(req, res, (err) => {
    //console.log(req);
    if(err){
      res.render('pages/ITI/JobFairDetails', {
        msg: err,
        file:'',
        type: req.cookies.modaltype
      });
    } else {
      if(req.file == undefined){
        res.render('pages/ITI/JobFairDetails', {
          msg: 'Error: No File Selected!',
          file:'',
          type: req.cookies.modaltype
        });
      } else {
        res.render('pages/ITI/JobFairDetails', {
          msg: 'File Uploaded!',
          file: req.file.filename,
          type: req.cookies.modaltype
        });
      }
    }
  });
}); 
app.post('/ITI/JobFairResultUpload', function(req, res) {
  var exceltojson; //Initialization
  upload(req,res,function(err){
      if(err){
           res.json({error_code:1,err_desc:err});
           return;
      }
      /** Multer gives us file info in req.file object */
      if(!req.file){
          res.json({error_code:1,err_desc:"No file passed"});
          return;
      }

      if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx' || req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xls'){
          exceltojson = xlsxtojson;
      } else {
          exceltojson = xlstojson;
      }
      try {
          exceltojson({
              input: req.file.path, //the same path where we uploaded our file
              output: null, //since we don't need output.json
              lowerCaseHeaders:true
          }, function(err,result){
           
              if(err) {
                console.log(err)
                  return res.json({error_code:1,err_desc:err, data: null});
              }
              // res.json({error_code:0,err_desc:null, data: result});
              else{
                
                res.render('pages/ITI/JobFairResult', {
                  msg: 'done',
                  data:JSON.stringify(result)
          
                });
               
              }
              
          });
      } catch (e){
          res.json({error_code:1,err_desc:"Corupted excel file"});
      }
  });
});
app.post('/ITI/CompanyRegister', (req, res) => {
  console.log(req);
  upload(req, res, (err) => {
    console.log(req);
    if(err){
      res.render('pages/ITI/CompanyRegister', {
        msg: err,
        file:'',
        type: req.cookies.modaltype
      });
    } else {
      if(req.file == undefined){
        res.render('pages/ITI/CompanyRegister', {
          msg: 'Error: No File Selected!',
          file:'',
          type: req.cookies.modaltype
        });
      } else {
        res.render('pages/ITI/CompanyRegister', {
          msg: 'File Uploaded!',
          file: req.file.filename,
          type: req.cookies.modaltype
        });
      }
    }
  });
}); 
app.post('/PlacementReportByExcel', function(req, res) {
  var exceltojson; //Initialization
  upload(req,res,function(err){
      if(err){
           res.json({error_code:1,err_desc:err});
           return;
      }
      /** Multer gives us file info in req.file object */
      if(!req.file){
          res.json({error_code:1,err_desc:"No file passed"});
          return;
      }

      if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx' || req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xls'){
          exceltojson = xlsxtojson;
      } else {
          exceltojson = xlstojson;
      }
      try {
          exceltojson({
              input: req.file.path, //the same path where we uploaded our file
              output: null, //since we don't need output.json
              lowerCaseHeaders:true
          }, function(err,result){
           
              if(err) {
                console.log(err)
                  return res.json({error_code:1,err_desc:err, data: null});
              }
              // res.json({error_code:0,err_desc:null, data: result});
              else{
              
                res.end('File Uploaded!'+JSON.stringify(result));
               
              }
              
          });
      } catch (e){
          res.json({error_code:1,err_desc:"Corupted excel file"});
      }
  });
});
app.post('/GSTUpload', (req, res) => {
  
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
app.post('/PANUpload', (req, res) => {
  
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
app.post('/CareerCounsellingDetails', (req, res) => {
  
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 400);
  res.render('pages/error');
});
app.listen(port, function() {
  console.log('Server listening on port ' + port + '...');
});
module.exports = app;

