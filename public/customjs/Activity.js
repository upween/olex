function FillType() {
    var path = serverpath + "ActivityMaster/'"+$("#ActivityType").val()+"'"
    securedajaxget(path,'parsedataFillType','comment','control');
}
function parsedataFillType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillType();
        // UpdateActivity();
    }
    else{
    var data1 = data[0];
    var appenddata = '';
    for (var i = 0; i < data1.length; i++) {
    
        if (data1[i].activity_status=="Pending"){
 
            appenddata += "<tr><td>" +[i+1]+ "</td><td style='word-break: break-word;'>" + data1[i].activity_type  + "</td><td style='word-break: break-word;'>" + data1[i].activity_type_id  + "</td><td><a data-toggle='modal' data-target='#myModal' href='#' onclick=sessionStorage.setItem('Activityid','"+data1[i].activity_id+"')>"+ data1[i].activity_status +"</a></td></tr>";
               }
       } jQuery("#tbodyvalue").html(appenddata);
}
}

function UpdateActivity() {
    var MasterData = {
        "p_activity_id": sessionStorage.getItem("Activityid"),
        "p_activity_status": $('#Status').val(),
        "p_response": $('#Response').val(),
        "p_comment": $('#Comments').val(), 
        "p_response_by": sessionStorage.getItem("RegistrationId"),

    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/ActivityMaster";
    securedajaxpost(path, 'parsrdataUpdateActivity', 'comment', MasterData, 'control')
}
function parsrdataUpdateActivity(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        
    }

    else if (data[0][0].ReturnValue == "1") {
        $('#myModal').modal('toggle');

        toastr.success("Update Successful", "", "success")
        // $('#myModal1').modal('toggle');
        FillType();
        return true;
    }

}