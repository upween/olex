
createcaptchaheader();
function ValidateLoginHome() {
  var path = serverpath + "validateadmin/'"+jQuery('#txtlogin').val()+"'/'"+md5(jQuery('#txtpassword').val())+"'"
  ajaxget(path,'parsedatalogin','comment');
  } 
  function parsedatalogin(data){ 
  data = JSON.parse(data)
  if (data.result.userDetails.ReturnValue =="1") {
    console.log(data);
    InsupdLoginlog(data.result.userDetails.Emp_Id)
    sessionStorage.setItem("CandidatePassword",jQuery('#txtpassword').val())
 
  
  sessionStorage.setItem("refreshToken", data.result.refreshToken)
  sessionStorage.setItem("token", data.result.token);
  sessionStorage.setItem("RegistrationId", data.result.userDetails.User_Type_Id);
  sessionStorage.setItem("CandidateId", data.result.userDetails.Emp_Id);
  sessionStorage.setItem("CandidateName", data.result.userDetails.Emp_Name);
  sessionStorage.setItem("MobileNumber", data.result.userDetails.Mobile);
  sessionStorage.setItem("User_Type_Id", data.result.userDetails.User_Type_Id); 
  sessionStorage.setItem("EmailId", data.result.userDetails.Email_Id);
  sessionStorage.setItem("CandidateUserName", data.result.userDetails.UserName);
  sessionStorage.setItem("Ex_name", data.result.userDetails.Ex_name);  
  sessionStorage.setItem("Ex_id", data.result.userDetails.Ex_id);  
  sessionStorage.setItem("DivisionId", data.result.userDetails.DivisionId); 
  sessionStorage.setItem("DistrictId", data.result.userDetails.District_id); 
  sessionStorage.setItem("Emp_Id", data.result.userDetails.Emp_Id); 
  sessionStorage.setItem("DistrictName", data.result.userDetails.DistrictName); 
  Cookies.set('User_Type_Id', data.result.userDetails.User_Type_Id, { expires: 1, path: '/' });

  InsupdLoginlog(data.result.userDetails.Emp_Id)

 setTimeout(() => {
  FetchMenu('parsedatasecuredFetchMenu','');
}, 300);
 
  //RoleMenuMapping(data[0].RoleId);
  
  }
  
  else if(data.result.userDetails.ReturnValue == "2"){
  toastr.warning("Invalid UserId or Password", "", "info")
  createcaptchaheader();
  jQuery('#cpatchaTextBoxheader').val('');
  return true;
  
  }
  else{
  toastr.warning(data, "", "info")
  }
  }
  function FetchMenu(funct,control) {
  var path = serverpath + "admusermenu/'"+ sessionStorage.getItem("RegistrationId") +"'"
  securedajaxget(path,funct,'comment',control);
  }


function parsedatasecuredFetchMenu(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FetchMenu('parsedatasecuredFetchMenu',control);
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
      else{
        sessionStorage.setItem("UserMenu", JSON.stringify(data[0]));
        window.location="/index";
      }
            
}

function CheckValidationHome(){
if(isValidation){
  if (jQuery('#txtlogin').val() == '') {
    getvalidated('txtlogin','text','Username');
    return false;
}
else if (jQuery('#txtpassword').val() == '') {
   getvalidated('txtpassword','text','Password');
  return false;
  }
else if(isCaptchaValidated){
 if (jQuery('#cpatchaTextBox').val() == '') {
    getvalidated('cpatchaTextBox','text','Captcha');
    return false;
  }
  else { 
      jQuery('#cpatchaTextBox').css('border-color', '');
      validateCaptchaheader();
   }
}
else{
  ValidateLoginHome();
}
}
  else{
    ValidateLoginHome();
  }

        
       
  
}
var codeheader;
                function createcaptchaheader() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaheader').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaheader";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeheader = captcha.join("");
                  document.getElementById("captchaheader").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaheader() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBox").value == codeheader) {
                    ValidateLoginHome() ;
                    
                  }else{
                    jQuery('#cpatchaTextBoxheader').val("");
                   // toastr.warning("Invalid Captcha", "", "info");
                   $("#validcpatchaTextBox").html("Invalid Captcha")
                   jQuery('#cpatchaTextBoxheader').css('border-color', 'red'); 
                    createcaptchaheader();
                   return true;
                    
                  }
                }
    

                function checkpassword(){
                  var txtpassword;
                  txtpassword = jQuery('#txtpassword').val();
                  var reg = /[0-9a-fA-F]{4,8}/;
                  if (reg.test(txtpassword) == false) {
                      $("#txtpassword").focus();
                      toastr.warning("Please Enter Correct Password", "", "info")
                      return true;
                  }
              }
              function InsupdLoginlog(empid) {
                var MasterData = {
                  "p_Emp_id": empid,
                  "p_IpAddress": sessionStorage.ipAddress,
                  "p_Type":"LoggedIn"
              
              
                };
                MasterData = JSON.stringify(MasterData)
                var path = serverpath + "Olexlogslogin";
                securedajaxpost(path, 'parsrdataitLoginlog', 'comment', MasterData, 'control')
              }
              
              
              function parsrdataitLoginlog(data) {
                data = JSON.parse(data)
                if (data[0][0].ReturnValue == "1") {
                  FetchMenu('parsedatasecuredFetchMenu','');
              
                }
              
              
              }