function FillAdvCategory() {
	var path = serverpath + "advcategory/0/0"
	ajaxget(path, "parsedataFillAdvCategory", 'comment', "control");
}
function parsedataFillAdvCategory(data) {
	data = JSON.parse(data)


	jQuery("#ddlCategory").empty();
	var data1 = data[0];

	
	for (var i = 0; i < data1.length; i++) {
		jQuery("#ddlCategory").append(jQuery("<option></option>").val(data1[i].Adv_Cat_Id).html(data1[i].Adv_Category));
	}
	
}
jQuery('#ddlCategory').on('change', function () {
    FillAdd(jQuery('#ddlCategory').val());
      });
function FillAdd(Cat_Id) {
	var path = serverpath + "advertisement/"+Cat_Id+"/1"
	ajaxget(path, "parsedataFillAdd", 'comment', "control");
}
function parsedataFillAdd(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
			active = "N"
		}
		else if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Adv_Id + "</td><td style='    word-break: break-word;'>" + data1[i].Adv_Discrption + "</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button'  onclick=EditMode('" + data1[i].Adv_Id + "','" + encodeURI(data1[i].Adv_Discrption)+ "','" + encodeURI(data1[i].Adv_ClosingDate) + "','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].UrgentIMG)+"','"+encodeURI(data1[i].DisplayInFront)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);

	
}
function CheckValidation(){
	if(isValidation){
	if(jQuery("#txtDescription").val()==""){
		getvalidated('txtDescription','text','Description');
		return false;
	}
	if(jQuery("#m_datepicker_2").val()==""){
		getvalidated('m_datepicker_2','text','End Date of Display');
		return false;
	}
	else{
		InsUpdAdvertisement();
	}
}
	else{
		InsUpdAdvertisement();
	}
}
function InsUpdAdvertisement(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
	var display = $("#displaystatus")[0].checked
	if (display == true){
		display = "Y"
	}else{
		display="N"
	}
var MasterData = {  
	
    "p_Adv_Id":localStorage.Adv_Id,
    "p_Adv_Cat_Id":jQuery("#ddlCategory").val(),
	"p_EX_Id":'100',
	"p_Adv_Title":"",
    "p_Adv_Discrption":jQuery("#txtDescription").val(),
	"p_Adv_ClosingDate":$("#m_datepicker_2").val(),
	"p_Adv_FileName":"",
    "p_Verify_Status":"0",
	"p_Verify_By":"0",
	"p_Verify_Date":"",
	"p_Active_YN":chkval,
    "p_LMDT":"0",
	"p_LMBY":sessionStorage.CandidateId,
	"p_UrgentIMG":jQuery("#ddlflag").val(),
	"p_DisplayInFront":display,
    "p_Adv_DiscrptionH":"0",
	"p_Cat_ID":jQuery("#ddlCategory").val(),
	"p_Cat_Name":""

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/advertisement";
securedajaxpost(path, 'parsrdataAdvertisement', 'comment', MasterData, 'control')
}

function parsrdataAdvertisement(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdAdvertisement();
	}
	else if (data[0][0].ReturnValue == "1") {
		FillAdd('22');
		resetMode();
		$('#Addneweventform').hide();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		FillAdd('22');
		 resetMode();
			toastr.success("Update Successful", "", "success");
			$('#Addneweventform').hide();
			$('#uploadform').hide();
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function resetMode(){
	localStorage.Adv_Id='0',
	jQuery("#ddlCategory").val("22");
	jQuery("#txtDescription").val("");
	$("#m_datepicker_2").val("");
	jQuery("#ddlflag").val("");
	$("#chkActiveStatus")[0].checked=false;
	$("#displaystatus")[0].checked=false;
	$('#ddlCategory').attr('disabled',false),
	jQuery("#txtfiledescription").val(""),
	$("#addnewevent").attr('disabled',false)


}
function EditMode(Adv_Id,Adv_Discrption,Adv_ClosingDate,Active_YN,UrgentIMG,DisplayInFront){
	localStorage.Adv_Id=Adv_Id,
	localStorage.File_ID='0'
	FillDocumentList()
	$('#Addneweventform').show();
	$('#uploadform').show();
	$('#ddlCategory').attr('disabled',true),
	$("#addnewevent").attr('disabled',true)
	jQuery("#txtDescription").val(decodeURI(Adv_Discrption));
	$("#m_datepicker_2").val(decodeURI(Adv_ClosingDate));
	jQuery("#ddlflag").val(decodeURI(UrgentIMG));
	if(decodeURI(Active_YN)=='1'){
		$("#chkActiveStatus")[0].checked=true;
	}
	else{
		$("#chkActiveStatus")[0].checked=false;
	}
	if(decodeURI(DisplayInFront)=='Y'){
	$("#displaystatus")[0].checked=true;
	}
	else{
		$("#displaystatus")[0].checked=false;
	}
}

function InsUpdFileUpload(){
	
var MasterData = {  
	
    "p_File_ID":localStorage.File_ID,
    "p_Adv_Id":localStorage.Adv_Id,
	"p_File_Discrption":jQuery("#txtfiledescription").val(),
	"p_File_Name":"t",
    "p_Cat_ID":jQuery("#ddlCategory").val()
	

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/fileuploaddtls";
securedajaxpost(path, 'parsrdataInsUpdFileUpload', 'comment', MasterData, 'control')
}

function parsrdataInsUpdFileUpload(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdFileUpload();
	}
	else if (data[0][0].ReturnValue == "1") {
		FillAdd('22');
		resetMode();
		FillDocumentList();
		$('#Addneweventform').hide();
		$('#uploadform').hide();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function FillDocumentList(){
	var path = serverpath + "fileuploaddtls/"+localStorage.Adv_Id+""
	ajaxget(path, "parsedataFillDocumentList", 'comment', "control");
}
function parsedataFillDocumentList(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	
	for (var i = 0; i < data1.length; i++) {
		
		appenddata += "<li>"+data1[i].File_Name+"</li>";
	}
	 jQuery("#documentlist").html(appenddata);

	
}