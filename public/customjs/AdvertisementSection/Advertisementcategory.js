function CheckValidation(){
	if(isValidation){
	if($("#txtad").val()==""){
		getvalidated('txtad','text','Advertisement Category');
		return false;
	}

	else{
		InsUpdAdv_category();
	}
	
	}
	
	else{
		InsUpdAdv_category();
	}
	
	}
function InsUpdAdv_category(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Adv_Cat_Id":localStorage.Adv_Cat_Id,
    "p_Adv_Category":jQuery("#txtad").val(),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/advcategory";
securedajaxpost(path, 'parsrdataAddcategory', 'comment', MasterData, 'control')
}

function parsrdataAddcategory(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdAdv_category();
	}
	else if (data[0][0].ReturnValue == "1") {
		FetchAdv_Category();
		resetMode();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		FetchAdv_Category();
		resetMode();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}

function resetMode() {

	localStorage.Adv_Cat_Id='0'
    jQuery("#txtad").val("");
    $("#ActiveStatus").prop("checked", false);
}



function FetchAdv_Category() {
	var path = serverpath + "advcategory/0/0"
	ajaxget(path, 'parsedataFetchAdd', 'comment', "control");
}
function parsedataFetchAdd(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
			active = "N"
		}
		else if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Adv_Cat_Id + "</td><td style='    word-break: break-word;'>" + data1[i].Adv_Category + "</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button'  onclick=EditMode('" + data1[i].Adv_Cat_Id + "','" + encodeURI(data1[i].Adv_Category)+ "','" + encodeURI(data1[i].Active_YN) + "') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=Delete('" + data1[i].Adv_Cat_Id + "','Adv_Category')>Delete</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMode(Adv_Cat_Id,Adv_Category,Active_YN){
	jQuery('#m_scroll_top').click();
	localStorage.Adv_Cat_Id=Adv_Cat_Id,
	$("#txtad").val(decodeURI(Adv_Category))
    if(decodeURI(Active_YN)=='0'){
		$("#ActiveStatus")[0].checked=false;
		}
		else{	
			$("#ActiveStatus")[0].checked=true;
		}
}
