function FillUsername(funct,control) {
    var path =  serverpath +"crmuser"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillUsername(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillUsername('parsedatasecuredFillUsername',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            console.log(data)
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('All'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
             
        }
        //FillFolderDdl();
              
}

function FillFolderDdl() {
    var path = serverpath + "getcrmuploadfodlermaster/1/"+$("#recruiterddl").val()+""
    ajaxget(path, 'parsedataFillFolderddl', 'comment', "control");
}
function parsedataFillFolderddl(data){
    var data=JSON.parse(data);
    var data1 = data[0];
    
    jQuery("#folderddl").append(jQuery("<option ></option>").val("0").html("Select Folder"));
    if(data[0].length){
        $('.folderddl').show();
        for (var i = 0; i < data1.length; i++) {
            jQuery("#folderddl").append(jQuery("<option></option>").val(data1[i].FolderId).html(data1[i].FolderName));
        }
    }
    else{
        $('.folderddl').hide();
    }
     
}
function handlechangerecruiter(){
    if($('#recruiterddl').val()==0){
        $('.folderddl').hide();
        jQuery("#folderddl").val(0);
    }
    else{
        $('.folderddl').show();
        FillFolderDdl();
    }
}

function fetchCVDatabase(){
    if($("#m_datepicker_5").val()==""){
        var entryDate="1999-01-01";
    }
    else{
        var date1=$("#m_datepicker_5").val().split("/");
        var entryDate=date1[2]+"-"+date1[1]+"-"+date1[0];
    }
    var path = serverpath + "getcvupload/"+$("#recruiterddl").val()+"/'"+entryDate+"'/"+jQuery("#folderddl").val()+""
    ajaxget(path, 'parsedataCVDatabase', 'comment', "control");
}
function parsedataCVDatabase(data){  
    var data=JSON.parse(data);
    var data1 = data[0];
    var appenddata="";
    for(var i =0; i<data1.length;i++){
        appenddata+=`<tr>
        <td>${i+1}</td>
        <td>${data1[i].CandidateName}</td>
        <td>${data1[i].CandidateEmailId}</td>
        <td>${data1[i].CandidateMobile}</td>
        <td><button class="folder-container">
        <div class="folder-icon">
          <i class="fa fa-file file-icon-color" style="font-size:3.1rem" ></i>
        </div>
        <div class="folder-name">${data1[i].CandidateName}_${data1[i].CVFIleName}<i class="fa fa-download" onclick=downloadURI("../${data1[i].Emp_Name}${sessionStorage.Emp_Id}/${encodeURI(data1[i].FolderName)}/${data1[i].CVFIleName}","${data1[i].CVFIleName}") aria-hidden="true" style="padding:0 30px"></i></div>
      </button></td>
        <td>${data1[i].Emp_Name}</td>
        </tr>`
    }
    $('#tbodyvalue').html(appenddata);
    $('a#backToFolders').on('click', function() {
        $('#foldersGroup').removeClass('d-none');
        $('#filesGroup').addClass('d-none')
      });
}


function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
  }
