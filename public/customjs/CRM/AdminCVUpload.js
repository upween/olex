function FillUsername(funct,control) {
    var path =  serverpath +"crmuser"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillUsername(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillUsername('parsedatasecuredFillUsername',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            console.log(data)
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('All'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
             
        }
        FillFolderDdl();
              
}

function FillFolderDdl() {
    var path = serverpath + "getcrmuploadfodlermaster/1/"+$("#recruiterddl").val()+""
    ajaxget(path, 'parsedataFillFolderddl', 'comment', "control");
}
function parsedataFillFolderddl(data){
    var data=JSON.parse(data);
    var data1 = data[0];
    jQuery("#foldenameddl").append(jQuery("<option ></option>").val("0").html("Select Folder"));
     for (var i = 0; i < data1.length; i++) {
         jQuery("#foldenameddl").append(jQuery("<option></option>").val(data1[i].FolderId).html(data1[i].FolderName));
     }
}