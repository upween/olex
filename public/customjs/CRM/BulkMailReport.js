function FillUsername(funct,control) {
    var path =  serverpath +"crmuser"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillUsername(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillUsername('parsedatasecuredFillUsername',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            console.log(data)
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('All'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
             
        }
              
}

function FetchCRMMailReport() {
  
     var fd = $('#m_datepicker_6').val().split('/')
    var fromdate = fd[2]+'-'+fd[1]+'-'+fd[0]
    
    var td = $('#m_datepicker_5').val().split('/')
    var todate = td[2]+'-'+td[1]+'-'+td[0]
    
        var path = serverpath + "fetchmassmailreport/'"+fromdate+"'/'"+todate+"'/'"+$('#empusername').val()+"'"
        securedajaxget(path, 'parsedataCRMEmailReport', 'comment', 'control');
    }
    function parsedataCRMEmailReport(data, control) {
     
    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    if(sessionStorage.User_Type_Id=='0'){
        var btnhead=`<th>Sent BY</th>`;
    }
    else{
        var btnhead=``;
    }
    var	tablehead ="<th>S.No.</th><th>Subject</th><th>Entry date</th>"+btnhead+"";

    


    $('#tablehead').html(tablehead);
    for (var i = 0; i < data1.length; i++) {

        if(sessionStorage.User_Type_Id=='0'){
            var btnbody=`<td>${data1[i].Emp_Name}</td>`
        }
        else{
          var btnbody=``
        }
        

       
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td <a href='/subject' style='word-break: break-word;cursor:pointer' onclick=EditMode('"+encodeURI(data1[i].too)+"','"+encodeURI(data1[i].cc)+"','"+encodeURI(data1[i].bcc )+"','"+encodeURI(data1[i].subject )+"','"+encodeURI(data1[i].content )+"')>" + data1[i].subject + "</a></td><td>"+ data1[i].Entry_date  + "</td>"+ btnbody + "</tr>";
    } jQuery("#tbodyvalue").html(appenddata);
  
}

function EditMode(too,cc,bcc,subject,content) {
    jQuery("#totext").text(decodeURI(too))
    jQuery("#cctext").text(decodeURI(cc))
    jQuery("#bcctext").text(decodeURI(bcc))
    jQuery("#subjecttxt").text(decodeURI(subject))
    jQuery('#emailbody').text(decodeURI(content))
   $('#myLargeModal2').modal('toggle');
 
}
