function FillFolderDdl() {
    var path = serverpath + "getcrmuploadfodlermaster/1/"+sessionStorage.Emp_Id+""
    ajaxget(path, 'parsedataFillFolderddl', 'comment', "control");
}
function parsedataFillFolderddl(data){
    var data=JSON.parse(data);
    var data1 = data[0];
    jQuery("#foldenameddl").append(jQuery("<option ></option>").val("0").html("Select Folder"));
     for (var i = 0; i < data1.length; i++) {
         jQuery("#foldenameddl").append(jQuery("<option></option>").val(data1[i].FolderId).html(data1[i].FolderName));
     }
}
function createFolder(){
    var MasterData={"FolderName":sessionStorage.CandidateName+sessionStorage.Emp_Id+'/'+$('#foldenameddl option:selected').text()};
    Cookies.set('filefolder',MasterData['FolderName'] , { expires: 1, path: '/' });
    MasterData= JSON.stringify(MasterData)
     jQuery.ajax({
         url: '/createFolder',
         type: "POST",
         data: MasterData,
         contentType: "application/json; charset=utf-8",
         success: function (successdata, status, jqXHR) {
             console.log(JSON.stringify(successdata));
             $('#cvpostattach').click();
         },
         error: function (errordata) {
             if (errordata.status == 0) {
                 toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                 return true;
             }
             else {
                 console.log(JSON.stringify(errordata));
             }
         }
     });
 }
 $('#cvupload').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function(response) {
            if (response == "Error No File Selected For GST Upload"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Request Entity Too Large"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!"){
                toastr.warning(response, "", "info")
            }
            else{
                var str = response;
                console.log(str);
                var res = str.split("!");
                sessionStorage.setItem("CVfileName", res[1])
                insupdCVUpload()
              
            }
        }
    });
    return false;
 });
 function CheckValidationCV(){
    if(jQuery("#foldenameddl").val()==""){
        getvalidated('foldenameddl','select','Folder');
        return false;
    }
    if(jQuery("#nametext").val()==""){
        getvalidated('nametext','text','Candidate Name');
        return false;
    }
    if(jQuery("#emailidtext").val()==""){
        getvalidated('emailidtext','text','Candidate EmailId');
        return false;
    }
    if(jQuery("#mobilenumbertext").val()==""){
        getvalidated('mobilenumbertext','text','Candidate Mobile Number');
        return false;
    }
    else{
        createFolder();
    }
}
function insupdCVUpload(){
       
    var MasterData = {  
        
        "p_CVId":0,
        "p_CandidateName":jQuery("#nametext").val(),
        "p_CandidateEmailId":$('#emailidtext').val(),
        "p_CandidateMobile":$('#mobilenumbertext').val(),
        "p_CVFileName":sessionStorage.CVfileName,
        "p_FolderId":$('#foldenameddl').val(),
        "p_EntryBy":sessionStorage.Emp_Id,
        "p_IpAddress":sessionStorage.ipAddress
    
    
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "insupdcrmcvupload";
    ajaxpost(path, 'parsrcvupload', 'comment', MasterData, 'control')
    }
    
    function parsrcvupload(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                InsUpdFolder();
        }
        else if (data[0][0].ReturnValue == "1") {
            toastr.success("CV Uploaded Successfully");
        }
    }
    function FillFolderGroup(){
        var path = serverpath + "getcrmuploadfodlermaster/1/"+sessionStorage.Emp_Id+""
        ajaxget(path, 'parsedataFillFolderGroup', 'comment', "control");
    }
    function parsedataFillFolderGroup(data){
        var data=JSON.parse(data);
        var data1 = data[0];
        var appenddata=""
        for(var i =0; i<data1.length;i++){
            appenddata+=` <div class="d-inline-flex">
            <button class="folder-container" onclick="FillCVFiles(${data1[i].FolderId})">
              <div class="folder-icon">
                <i class="fa fa-folder folder-icon-color"></i>
              </div>
              <div class="folder-name">${data1[i].FolderName}</div>
            </button>
          </div>`
        }
        $('#main-folders').html(appenddata);
      
          
    }
    function FillCVFiles(fodlerId){
        var path = serverpath + "getcvuploadbyfoldername/"+fodlerId+"/"+sessionStorage.Emp_Id+""
        ajaxget(path, 'parsedataFillCVFiles', 'comment', "control");
    }
    function parsedataFillCVFiles(data){
        var data=JSON.parse(data);
        var data1 = data[0];
            $('#filesGroup').removeClass('d-none');
            $('#foldersGroup').addClass('d-none')
            var appenddata="";
            for(var i =0; i<data1.length;i++){
                appenddata+=`   <div class="d-inline-flex">
                <button class="folder-container">
                  <div class="folder-icon">
                    <i class="fa fa-file file-icon-color" style="font-size:3.1rem" onclick="viewCandidateDetails('${data1[i].CandidateName}','${data1[i].CandidateEmailId}',${data1[i].CandidateMobile})"></i>
                  </div>
                  <div class="folder-name">${data1[i].CandidateName}_${data1[i].CVFIleName}<i class="fa fa-download" onclick=downloadURI("../${data1[i].Emp_Name}${sessionStorage.Emp_Id}/${encodeURI(data1[i].FolderName)}/${data1[i].CVFIleName}","${data1[i].CVFIleName}") aria-hidden="true" style="padding:0 30px"></i></div>
                </button>
              </div>`
            }
            $('#main-files').html(appenddata);
            $('a#backToFolders').on('click', function() {
                $('#foldersGroup').removeClass('d-none');
                $('#filesGroup').addClass('d-none')
              });
          
    }
    function viewCandidateDetails(name,email,mobile){
        $('#myModal').modal('toggle');
        $("#canddiatenamespan").text(name)
$("#canddiateemailspan").text(email)
$("#canddiatemobilespan").text(mobile)
    }

    function downloadURI(uri, name) {
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        delete link;
      }