
function InsUpdFolder(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_FolderId":localStorage.FolderId,
    "p_FolderName":jQuery("#foldernametext").val(),
    "p_Active_YN":chkval,
    "p_EntryBy":sessionStorage.Emp_Id,
    "p_UpdatedBy":sessionStorage.Emp_Id,
    "p_IpAddress":sessionStorage.ipAddress,


};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "insupdcrmuploadfodlermaster";
ajaxpost(path, 'parsrdatafolder', 'comment', MasterData, 'control')
}

function parsrdatafolder(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdFolder();
	}
	else if (data[0][0].ReturnValue == "1") {
        FillFolderDetails();
       // createFolder();
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FillFolderDetails();
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.FolderId = "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#foldernametext").val("")
        // $("#ddltehsil").val("0");
        // $("#village").val("");
    }


function FillFolderDetails() {
        var path = serverpath + "getcrmuploadfodlermaster/0/"+sessionStorage.Emp_Id+""
        ajaxget(path, 'parsedataFillFolderDetails', 'comment', "control");
    }
    function parsedataFillFolderDetails(data) {

        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        var active = "";
        if(data1.length){
        for (var i = 0; i < data1.length; i++) {

            if (data1[i].Active_YN == '0') {
            var	active = "No"
            }
             if (data1[i].Active_YN == '1') {
              var  active = "Yes"
            }
            appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].FolderName + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode('"+data1[i].FolderId +"','"+encodeURI(data1[i].FolderName)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        } jQuery("#tbodyvalue").html(appenddata);
        }
    
    }


    function EditMode(FolderId,FolderName,IsActive) {
        jQuery("#foldernametext").val(decodeURI(FolderName))
        //jQuery("#ddltehsil").val(decodeURI(CityId))
        localStorage.FolderId =FolderId
        if (decodeURI(IsActive) == '0') {
            $("#ActiveStatus")[0].checked = false;
        }
        else {
            $("#ActiveStatus")[0].checked = true;
        }
    }

    
    function CheckValidationFolder(){
        if(jQuery("#foldernametext").val()==""){
            getvalidated('foldernametext','text','Category');
            return false;
        }

        else{
            InsUpdFolder();
        }
    }
 