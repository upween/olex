function Inscrmquery(){
var date1=$("#m_datepicker_5").val().split('/');
var Date2=date1[2]+'/'+date1[1]+'/'+date1[0]

    var MasterData = {  
    
        "p_QueryId":sessionStorage.QueryId,
        "p_CandidateId":sessionStorage.crmCandidateID,
        "p_Date":Date2,
        "p_Name":$("#Name").val(),
        "p_ContactNumber":$("#ContactNumber").val(),
        "p_EmailId":$("#EmailId").val(),
        "p_Location":$("#Location").val(),
        "p_AadharNo":'',
        "p_ConcernRelatedTo":$("#ConcernRelatedTo").val(),
        "p_InformationProvidedTo":$("#InformationProvidedTo").val(),
        "p_Status":$("#Status").val(),
        "p_EntryDate":'',
        "p_EntryBy":sessionStorage.Emp_Id,
        "p_IpAddress":sessionStorage.ipAddress,
        "p_Updatedby":sessionStorage.Emp_Id 
        
    
    };
    MasterData = JSON.stringify(MasterData)
    sessionStorage.crmMasterData=MasterData;
    var path = serverpath + "insupdcrmQuery";
    ajaxpost(path, 'parsrdatainsupdcrmQuery', 'comment', MasterData, 'control')
    }
    
    function parsrdatainsupdcrmQuery(data) {
        data = JSON.parse(data)
    
     if (data[0][0].ReturnValue == "1") {
            InsupdCRMquerylogs('Insert')
            resetMode() ;
            FillcrmQuery()
             toastr.success("Insert Successful", "", "success")
             return true;
        }
        if (data[0][0].ReturnValue == "2") {
            InsupdCRMquerylogs('Update')
            FillcrmQuery()
            resetMode() ;
             toastr.success("Update Successful", "", "success")
             return true;
        }
     
    }

    
function resetMode() {
   location.reload();
   // $('#m_modal_6').toggle('modal')
    //$("#Ticket_No").val(),
   // $("#CandidateId").val(""),
    $("#m_datepicker_5").val(""),
    $("#Name").val(""),
    $("#ContactNumber").val(""),
    $("#EmailId").val(""),
    $("#Location").val(""),
    $("#ConcernRelatedTo").val(""),
    $("#InformationProvidedTo").val(""),
    $("#Status").val("0")
    sessionStorage.QueryId=0;
    $('#m_datepicker_5').datepicker('setDate', 'now');
   
}


function CheckValidationcrmForm(){
   
if($("#m_datepicker_5").val()==""){
    toastr.warning('Please Enter Date ')
    return false;
}
else if($("#Name").val()==""){
    toastr.warning('Please Enter Name ')
    return false;
}
else if($("#ContactNumber").val()==""){
    toastr.warning('Please Enter ContactNumber ')
    return false;
}
else if($("#Location").val()==""){
    toastr.warning('Please Enter Location ')
    return false;
}
else if($("#EmailId").val()==""){
    toastr.warning('Please Enter EmailId ')
    return false;
}
else if($("#Status").val()=="0"){
    toastr.warning('Please Enter Status ')
    return false;
}

else if($("#ConcernRelatedTo").val()==""){
    toastr.warning('Please Enter ConcernRelatedTo ')
    return false;
}
else if($("#InformationProvidedTo").val()==""){
    toastr.warning('Please Enter InformationProvidedTo ')
    return false;
}

else{
     
    Inscrmquery()
    }
}


function FillcrmQuery() {
    var path = serverpath +"getCrmQueryData/"+sessionStorage.crmCandidateID+"" 
    ajaxget(path, 'parsedataFillcrmQuery', 'comment', "control");
}
function parsedataFillcrmQuery(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    if(data[0].length){
    for (var i = 0; i < data1.length; i++) {

        appenddata += `<tr>
        <td>${[i+1]}</td>
        <td>${data1[i].Ticket_No}</td>
        <td>${data1[i].Date}</td>
        <td>${data1[i].Name}</td>
        <td>${data1[i].ContactNumber}</td>
       <td>${data1[i].Location}</td>
         <td>${data1[i].EmailId}</td>
        <td>${data1[i].ConcernRelatedTo}</td>
        <td>${data1[i].InformationProvidedTo}</td>
        <td>${data1[i].Status}</td>
        <td><button type="button" 
        class="btn btn-success" 
        style="background-color:#716aca;border-color:#716aca;"  onclick=editMode(${data1[i].QueryId},'${encodeURI(data1[i].Ticket_No)}','${encodeURI(data1[i].CandidateId)}','${encodeURI(data1[i].Date)}','${encodeURI(data1[i].Name)}','${encodeURI(data1[i].ContactNumber)}','${encodeURI(data1[i].EmailId)}','${encodeURI(data1[i].Location)}','${encodeURI(data1[i].AadharNo)}','${encodeURI(data1[i].ConcernRelatedTo)}','${encodeURI(data1[i].InformationProvidedTo)}','${encodeURI(data1[i].Status)}')>Modify</button></td>
        </tr>`;
    } jQuery("#tbodyvalue").html(appenddata);
    }

}
function editMode(QueryId,Ticket_No,CandidateId,Date,Name,ContactNumber,EmailId,Location,AadharNo,ConcernRelatedTo,InformationProvidedTo,Status)
{
            $('#m_modal_6').toggle('modal')
            sessionStorage.QueryId=QueryId,
            $("#m_datepicker_5").val(decodeURI(Date)),
            $("#Name").val(decodeURI(Name)),
            $("#ContactNumber").val(decodeURI(ContactNumber)),
            $("#EmailId").val(decodeURI(EmailId)),
            $("#Location").val(decodeURI(Location)),
            $("#ConcernRelatedTo").val(decodeURI(ConcernRelatedTo)),
            $("#InformationProvidedTo").val(decodeURI(InformationProvidedTo)),
            $("#Status").val(decodeURI(Status))
} 
function filluserDetails(){
            $("#Name").val(sessionStorage.crmCandidateName),
            $("#ContactNumber").val(sessionStorage.crmContact),
            $("#EmailId").val(sessionStorage.crmEmailId),
            $("#Location").val(sessionStorage.crmLocation)
}

function InsupdCRMquerylogs(Action) {
      
    var MasterData = {
        "p_MasterData":sessionStorage.crmMasterData,
        "p_EntryBy":sessionStorage.Emp_Id,
        "p_IpAddress": sessionStorage.ipAddress,
        "p_Action": Action,
        
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "insupdcrmQueryLogs";
    securedajaxpost(path, 'parsrdataCRMquerylogs', 'comment', MasterData, 'control')
}


function parsrdataCRMquerylogs(data) {
    data = JSON.parse(data)
  

}