

function Inscrmsms(content){


var MasterData = {  

    "p_ToNumber":jQuery("#tonumber").val(),
    "p_TemplateId":sessionStorage.TemplateId,
    "p_SentBy":sessionStorage.Emp_Id,
    "p_Ipaddress":sessionStorage.ipAddress,
    "p_Content": $('#finalcontent').text().replace("'",'[singleqoutes]'),
    "p_Type":jQuery("#reqtype").val(),
    "p_RequestedBy":jQuery("#requestedby").val(),
    "p_DistrictCovered":jQuery("#Districtcovered").val()
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "smscrm";
ajaxpost(path, 'parsrdatacemsms', 'comment', MasterData, 'control')
}

function parsrdatacemsms(data) {
	data = JSON.parse(data)

 if (data[0][0].ReturnValue == "1") {
       
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}

 
}

function resetMode() {
   

        jQuery("#tonumber").val("").css('border-color', '')
        $("#sms").html("");
        $("#validtonumber").html("");
        $("#reqtype").val("Yashaswi");
        FillCallCentreManager('parsedataFillCallCentreManager','requestedby');
        $("#Districtcovered")[0].selectize.clear();
       
    }


    function CheckValidationcrmsms(){
        if(jQuery("#tonumber").val()==""){
            console.log('in');
            getvalidated('tonumber','text','Candidate Number');
            return false;
        }else{
           // sentSmsGlobal(jQuery("#tonumber").val(),jQuery("#sms").val(),sessionStorage.TemplateId);
            var temp= $('#finalcontent').text();
            var count = (temp.match(/{#var/g) || []).length;     
             // $('#sms').text($('#summernote').text())
           //   console.log(count);
           
           setTimeout(() => {
                 
           var str=   $('#finalcontent').text();
           for(var i=0;i < count; i++){
               var replacedby=$("#var"+[i+1]+"").val();
               var index='{#var'+[i+1]+'#}'
               var replaceto= new RegExp( index, "g");
                str=str.replace(replaceto,replacedby)
               
         
            }
            $('#finalcontent').text(str);
            sentSmsGlobal(jQuery("#tonumber").val(),$('#finalcontent').text(),sessionStorage.TemplateId);
            Inscrmsms(str);
        }, 300);
            
        }
    }

    function FillTemplate(){
  
        var path =  serverpath + "temp"
 
securedajaxget(path,'parsedataFillTemplate','comment',"control");
  }
  function parsedataFillTemplate(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
      
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata = ''
            for (var i = 0; i < data1.length; i++) {
              appenddata +=` <button type="button" style='margin-top:5px' class="btn col-lg-1 btn-outline-dark" onclick="$('#m_modal_5').modal('toggle');setText('${encodeURI(data1[i].Template)}');sessionStorage.TemplateId='${data1[i].TemplateId}'">${[i+1]}</button>
              `
               
            }
            $('#templatediv').append(appenddata);
    }    
              
}
function setText(txt){
    $('#summernote').text(decodeURI(txt).replace('[singleqoutes]',"'"));
}
function handleExchChangerpt(){
    var str_pos=$('#summernote').text().indexOf("{#var") 

if(  str_pos > -1)
    
{
    var temp=$('#summernote').text();
    var count = (temp.match(/{#var/g) || []).length;     
     // $('#sms').text($('#summernote').text())
   //   console.log(count);
   
   $('#finalcontent').text(temp);
   setTimeout(() => {
    $('#finalcontentrow').show();     
   var str= $('#summernote').text();
   for(var i=0;i < count; i++){
       var replacedby="<input type='text' id='var"+[i+1]+"' style='paddingbottom:5px' maxlength=30 onfocusout='Smslength();updateFinalcontent("+i+")' />";
       var index='{#var'+[i+1]+'#}'
       var replaceto= new RegExp( index, "g");
        str=str.replace(replaceto,replacedby)
        $('#sms').html(str);
 
    }
}, 300);
    
}
else
{
    $('#finalcontent').text($('#summernote').text());
    $('#finalcontentrow').hide();
    $('#sms').text($('#summernote').text());
    
}
Smslength();
   

}
 function Smslength(type) {
   
    var limit = 160;   // it will be fixed value
    

        var charCount = $('#finalcontent').text().length  // it will be dynamic value
      
   

var result = Math.ceil(charCount/limit)
$('#SMSLENGTH').text(result+' SMS/'+charCount+' Chars');
 }
function updateFinalcontent(i){
    var replacedby=$("#var"+[i+1]+"").val();
    var index='{#var'+[i+1]+'#}'
    var replaceto= new RegExp( index, "g");
    var str=$('#finalcontent').text()
    str=str.replace(replaceto,replacedby);
    $('#finalcontent').text(str);
}
function checkNumberlength(){
    var numbers=$('#tonumber').val().split(',');
if(numbers.length){
if(numbers.length<100 || numbers.length==100){
    for(var i=0;i<numbers.length;i++){
        if(numbers[i].length!=10){
            toastr.warning('Phone number at position '+[i+1+' is not valid']);
            return false;
        }
    }
}
else{
        toastr.warning('Numbers should not be exceed 100');
        return false;
 }
}
}

function FillDistrict(funct,control) {
    var path =  serverpath + "district/'19'/0/1/0/"
    securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrict('parsedatasecuredFillDistrict',control);
     
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            
            jQuery("#"+control).empty();
            var data1 = data[0];
         
           // $("select")[0].selectize.off()
        
           var selectized = $("#"+control).selectize();
       
           var control1 = selectized[0].selectize;
       
           control1.destroy();
    $("#"+control).selectize({
        persist: false,
        createOnBlur: true,
        valueField: 'DistrictId',
        labelField: 'DistrictName',
        searchField: 'DistrictName',
        options: data1,
        create: false
    });
           
        }
              
}

function FillCallCentreManager(funct,control) {
    var path =  serverpath + "CallCentreManager"
    securedajaxget(path,funct,'comment',control);
}

function parsedataFillCallCentreManager(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCallCentreManager('parsedataFillCallCentreManager',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            if(control=='ddlentryby'){
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            }
            else{
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select"));
            }
            
            for (var i = 0; i < data1.length; i++) {
               
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_Id).html(data1[i].Ex_name));
           
                
             }
        }
}
function hanglereqTypeChange(){

    if ($('#reqtype').val()=='Govt'){
           
        FillSecuredexchangeoffice('parsedatasecuredFillSecuredexchangeoffice', 'requestedby');
    }
       else
{
    FillCallCentreManager('parsedataFillCallCentreManager','requestedby');
}    
}