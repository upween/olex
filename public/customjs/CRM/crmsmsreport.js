function FillUsername(funct,control) {
    var path =  serverpath +"crmuser"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillUsername(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillUsername('parsedatasecuredFillUsername',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            console.log(data)
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('All'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
             
        }
              
}

function FetchCRMSmsReport() {
  
     var fd = $('#m_datepicker_6').val().split('/')
    var fromdate = fd[2]+'-'+fd[1]+'-'+fd[0]
    
    var td = $('#m_datepicker_5').val().split('/')
    var todate = td[2]+'-'+td[1]+'-'+td[0]
    if(sessionStorage.User_Type_Id=='0'){
        var entryby=$('#empusername').val()
    }
    else{
        var entryby=sessionStorage.Emp_Id
    }
        var path = serverpath + "templatesmsreport/'"+fromdate+"'/'"+todate+"'/'"+entryby+"'"
        securedajaxget(path, 'parsedataCRMSmsReport', 'comment', 'control');
    }
    function parsedataCRMSmsReport(data, control) {
     
    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
 
    for (var i = 0; i < data1.length; i++) {

        if(sessionStorage.User_Type_Id=='0'){
            var btnbody=`<td>${data1[i].Emp_Name}</td>`
        }
        else{
          var btnbody=``
        }
        

       
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].ToNumber + "</td><td style='    word-break: break-word;'>" + data1[i].Content  + "</td><td>"+ data1[i].sentOn  + "</td>"+ btnbody + "</tr>";
    } jQuery("#tbodyvalue").html(appenddata);
  
}

function CheckValidationcrmsmsreport(){
    if(jQuery("#m_datepicker_6").val()==""){
       
        getvalidated('m_datepicker_6','text','From Date');
        return false;
    }
    if(jQuery("#m_datepicker_5").val()==""){
       
        getvalidated('m_datepicker_5','text','To Date');
        return false;
    }

 

    else{

        FetchCRMSmsReport();
    }
}
