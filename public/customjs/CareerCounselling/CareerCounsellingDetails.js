
   jQuery('#ddlExchangeName').on('change', function () {
    FillCCDetail(jQuery('#ddlExchangeName').val());
      });
      function FillCCDetail(Ex_Id){
       MasterData ={


        "p_CC_Id":localStorage.CCId,
        "p_Ex_id":Ex_Id,
        "p_CC_Title":"0",
        "p_Venue":"0",
        "p_CC_Details":"0",
        "p_Attachment_Filename":"t",
        "p_Date_of_Entry": "0",
        "p_CC_FromDt":"0",
        "p_CC_ToDt":"0",
        "p_CC_CreatedBy":"0",
        "p_Status":"0",
        "p_Verified_By":"0",
        "p_Verified_Dt":"0",
        "p_Reject_Reason":"0",
        "p_Active_YN":"0",
        "p_LMDT":"0",
        "p_LMBY":"0",
        "p_Flag":"4"
        
        };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/CCDetails";
    securedajaxpost(path, 'parsrdataitFillCCDetail', 'comment', MasterData, 'control')
    }
    function parsrdataitFillCCDetail(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillCCDetail(jQuery('#ddlExchangeName').val());
        }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            for (var i = 0; i < data1.length; i++) {
           if(data1[i].Active_YN=='0'){
    active="N"
           }
           else if(data1[i].Active_YN=='1'){
            active="Y"
                   }
                   if(data1[i].Status=='0'){
                    status='Not Verified'
                  disabled=false
                    
                   }
                   else if(data1[i].Status=='1'){
                    status='Verified';
                    disabled=true
                    
                   }
                   else if(data1[i].Status=='2'){
                    status='Reject'
                    disabled=true
                   
                   }
                   
       appenddata += "<tr><td style='text-align: center;'>"+data1[i].CC_Id+"</td><td style='    word-break: break-word;'><a href='#' onclick=CCDetails('"+data1[i].CC_Id+"','"+encodeURI(data1[i].Ex_name)+"','"+encodeURI(data1[i].CC_Title)+"','"+encodeURI(data1[i].CC_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].CC_FromDt)+"','"+encodeURI(data1[i].CC_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"')>" + data1[i].CC_Title+ "</a></td><td style='    word-break: break-word;'>"+data1[i].Venue+"</td><td >" + data1[i].CC_FromDt+ "</td><td>"+data1[i].CC_ToDt+"</td><td>"+active+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].CC_Id+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].CC_Title)+"','"+encodeURI(data1[i].CC_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].CC_FromDt)+"','"+encodeURI(data1[i].CC_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].Career_link)+"','"+encodeURI(data1[i].CareerType)+"')>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' data-toggle='modal' data-target='#m_modal_5' onclick='setCCID("+data1[i].CC_Id+","+data1[i].Ex_id+")' id='btnDelete'>Delete</button></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }  
     
    }
    function setCCID(id,ex_id){
        localStorage.CCId=id;
        localStorage.Ex_Id=ex_id;
       
        
    }
    function editMode(CC_Id,Ex_id,CC_Title,CC_Details,Venue,CC_FromDt,CC_ToDt,Active_YN,Career_link,CareerType)
    {
        $(".mform").scrollTop(0)
        $('#addCCdetails').show()
        localStorage.CCId=CC_Id,
    $("#AddCCexchange").val(Ex_id),
    $("#CCTitle").val(decodeURI(CC_Title)),
    $("#CCVenue").val(decodeURI(Venue)),
    console.log(CareerType)
    $("#m_datepicker_5").val(decodeURI(CC_FromDt));
    $("#m_datepicker_6").val(decodeURI(CC_ToDt));
    $("#formtype").val(decodeURI(CareerType));
    $("#zoomlink").val(decodeURI(Career_link));
    $("#CCDetail").val(decodeURI(CC_Details))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
        if ( CareerType == 'Online')
        {
          $(".dropdown").show();
          $('.venuetext').hide();
        }
        else
        {
          $(".dropdown").hide();
          $('.venuetext').show();
        }
    }
    function CheckValidation(){
        if($("#CCTitle").val()==""){
            getvalidated('CCTitle','text','Title');
            return false;
        }
        // if($("#CCVenue").val()==""){
        //     getvalidated('CCVenue','text','Venue')
        //     return false;
        // }
        if($("#m_datepicker_5").val()==""){
            getvalidated('m_datepicker_5','text','From Date');
            return false;
        }
        if($("#m_datepicker_6").val()==""){
            getvalidated('m_datepicker_6','text','To Date')
            return false;
        }
        else{
            InsUpdCCDetail();
        }
        
        }
    function InsUpdCCDetail(){
        var chkval = $("#chkActiveStatus")[0].checked
        if (chkval == true){
            chkval = "1"
        }else{
            chkval="0"
        }
        if(localStorage.CCId=='0'){
             flag='1'
        }
        else{
            flag='2'
        }
        var FromDt = jQuery("#m_datepicker_5").val().split(" ")
        var FromDt1 = FromDt[0].split("/")
        var FromDate = FromDt1[2] + "-" + FromDt1[1] + "-" + FromDt1[0] ;
    
        var ToDt = jQuery("#m_datepicker_6").val().split(" ")
        var ToDt1 = ToDt[0].split("/")
        var ToDate = ToDt1[2] + "-" + ToDt1[1] + "-" + ToDt1[0] ;

       
    var MasterData ={
        "p_CC_Id":localStorage.CCId,
        "p_Ex_id":$("#addjobfairexchange").val(),
        "p_CC_Title":$("#CCTitle").val(),
        "p_Venue":$("#CCVenue").val(),
        "p_CC_Details":$("#CCDetail").val(),
        "p_Attachment_Filename":"t",
        "p_Date_of_Entry": "0",
        "p_CC_FromDt":FromDate +' '+FromDt[1],
        "p_CC_ToDt":ToDate+ ' '+ToDt[1],
        "p_CC_CreatedBy":sessionStorage.CandidateId,
        "p_Status":"0",
        "p_Verified_By":"",
        "p_Verified_Dt":"",
        "p_Reject_Reason":"",
        "p_Active_YN":chkval,
        "p_LMDT":"0",
        "p_LMBY":sessionStorage.CandidateId,
        "p_Flag":flag,
        "p_CareerType":$("#formtype").val(),
        "p_Career_link":$("#zoomlink").val()
        
        };
    MasterData = JSON.stringify(MasterData)
    console.log(MasterData)
    var path = serverpath + "secured/CCDetails";
    securedajaxpost(path, 'parsrdataitInsUpdCCDetail', 'comment', MasterData, 'control')
    }
    function parsrdataitInsUpdCCDetail(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                InsUpdCCDetail();
        }
        else{
            resetmode()
            
           if(data){
           FillCCDetail(jQuery('#ddlExchangeName').val());
			toastr.success("Submit Successful", "", "success")
			return true;
        }
    }
    }
    function resetmode(){
        localStorage.CCId='0',
        $("#addjobfairexchange").val(""),
        $("#CCTitle").val(""),
        $("#CCVenue").val(""),
        $("#chkActiveStatus")[0].checked=false;
        $("#m_datepicker_5").val("");
        $("#m_datepicker_6").val("");
        $("#formtype").val("0")
        $("#zoomlink").val("")
        $("#CCDetail").val("")
        $('#addCCdetails').hide();
    
        }
        function CCDetails(CC_Id,Ex_name,CC_Title,CC_Details,Venue,CC_FromDt,CC_ToDt,Active_YN){

$('#CCdetailsById').show();
$('#searchjobdetail').hide();
$('#ExchangeById').html(decodeURI(Ex_name))
$('#TitleById').html(decodeURI(CC_Title))
$('#DetailById').html(decodeURI(CC_Details))
$('#VenueById').html(decodeURI(Venue))
$('#FrmdtById').html(decodeURI(CC_FromDt))
$('#ToDtById').html(decodeURI(CC_ToDt))
if(decodeURI(Active_YN)=='0'){
    $("#chkActiveStatusbyId")[0].checked=false;
    }
    else{	
        $("#chkActiveStatusbyId")[0].checked=true;
    }

        }


        function FillSecuredexchangeoffice1(funct,control) {
            var path =  serverpath +"secured/GetOfficeReport/101"
            securedajaxget(path,funct,'comment',control);
        }
        
        function parsedatasecuredFillSecuredexchangeoffice1(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillSecuredexchangeoffice1('parsedatasecuredFillSecuredexchangeoffice1','addjobfairexchange');                     }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#"+control).empty();
                    var data1 = data[0];
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                     }
                    if(sessionStorage.User_Type_Id !=0 && sessionStorage.User_Type_Id!=1){
                        console.log('in')
                        $('#addjobfairexchange').val(sessionStorage.Ex_id); 
                    }

                }
                      
        }