





function CheckCandidate(){
    var seletedid=''
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;
 }
  else{
    updSelectionStatus();
    
  }
}

function FillCC(){
    MasterData ={


        "p_CC_Id":0,
        "p_Ex_id":$('#ddlExchangeName').val(),
        "p_CC_Title":"0",
        "p_Venue":"0",
        "p_CC_Details":"0",
        "p_Attachment_Filename":"t",
        "p_Date_of_Entry": "0",
        "p_CC_FromDt":"0",
        "p_CC_ToDt":"0",
        "p_CC_CreatedBy":"0",
        "p_Status":"0",
        "p_Verified_By":"0",
        "p_Verified_Dt":"0",
        "p_Reject_Reason":"0",
        "p_Active_YN":"0",
        "p_LMDT":"0",
        "p_LMBY":"0",
        "p_Flag":"4"
        
        };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/CCDetails";
    securedajaxpost(path, 'parsedatasecuredCC', 'comment', MasterData, 'control')
  
      }
      function parsedatasecuredCC(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillCC();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                jQuery("#jobfairddl").empty();
                jQuery("#jobfairddl").append(jQuery("<option ></option>").val("0").html("Select Career Counselling"));
                for (var i = 0; i < data1.length; i++) {
                  
                    jQuery("#jobfairddl").append(jQuery("<option></option>").val(data1[i].CC_Id).html(data1[i].CC_Title + ' ' + data1[i].CC_FromDt + ' To ' + data1[i].CC_ToDt));
                    
                }
        }    
                  
    }


   
   

     function fetchCandidateList() {
        
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "reminder/"+$('#jobfairddl').val()+"/CareerCounselling",
            cache: false,
            dataType: "json",
            success: function (data) {
        var data1= data[0]
        if(data[0].length){
                      var appenddata="";
                      sessionStorage.Title=data[0][0].Title;
                      sessionStorage.Details=data[0][0].Details;
                      sessionStorage.Venue=data[0][0].Venue;
                      sessionStorage.FromDate=data[0][0].FromDate;
                      sessionStorage.EventType=data[0][0].EventType;
                      sessionStorage.ZoomLink=data[0][0].ZoomLink;
                      sessionStorage.TimeRange=data[0][0].TimeRange;
                      var tablehead="<th class='notinclude'><input type='checkbox' onclick='checkAll()' id='selectall'></th><th><a  style='color: white;'>SrNo</a></th><th>Candidate Name</th><th>Email Id</th><th>Mobile Number</th>";
                      $("#tablehead").html(tablehead);
                    for (var i = 0; i < data1.length; i++) {
                        
                    
                
                    appenddata += "<tr><td style='word-break: break-word;' class='notinclude'><input name='checkbox'  type='checkbox' id=" + data1[i].MobileNumber + " value=" + data1[i].CandidateId+'_'+data1[i].EmailId + "></td><td >" +[i+1]+ "</td><td>"+data1[i].CandidateName+"</td><td class='can_email'>"+data1[i].EmailId+"</td><td class='can_mobile'>"+data1[i].MobileNumber+"</td></tr>";
                    }
                   
                     $('#tbodyvalue').html(appenddata);
                }
                     $('.show').show()
                }
                    });
                }
            

                function CheckCandidate(type){
                    var seletedid=''
                    var radios = document.getElementsByName("checkbox");
                    var formValid = false;
                
                    var i = 0;
                    while (!formValid && i < radios.length) {
                        if (radios[i].checked)
                            formValid = true;
                        i++;
                    }
                
                    if (!formValid) {
                
                        $(window).scrollTop(0);
                        toastr.warning("Please Select Atleast One Candidate", "", "info")
                        return false;
                 }
                  else{
                      if(type=='email'){
                                    if(sessionStorage.EventType=='Online'){
var heading=`<p style='font-size:14px'>Please find link  below to attend soft skills training online:</p>`
var ifoffline=`<p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join The ${$('#formtype option:selected').text()}</a></strong></p>`
        
          }
          else{
            var heading=`<p style='font-size:14px'>Please find details below to attend soft skils training :</p>`
            var ifoffline=`<p style='font-size:14px'><strong>Venue:</strong>${sessionStorage.Venue}</p>`
         
          }
          var subject='DEPARTMENT OF EMPLOYMENT INVITES YOU FOR ATTENDING SOFT SKILLS TRAINING  AS JOBSEEKER !!';
 var dt1 = new Date(sessionStorage.FromDate.split('/')[1]+'/'+sessionStorage.FromDate.split('/')[0]+'/'+sessionStorage.FromDate.split('/')[2])
    // console.log('date',dt1);
      var dt=dt1.toLocaleString('en-us', {weekday:'long'})
        // var dt=new Date(sessionStorage.FromDate).toLocaleString('en-us', {weekday:'long'});
          var body=`<p style='font-size:14px'>Dear Jobseeker,</p>
                    <br>
                    <p style='font-size:14px'>
                    Greetings from Yashaswi Academy for Talent Management !!!</p>
                   
                    <p style='font-size:14px'>As we all know it is a difficult time due to pandemic wave in the country, we are unable to do Job fairs. It gives me an immense pleasure to inform you, that to resolve problem arise due to pandemic we have successfully develop the module of Online Job fair, we are going to host our first open Online Job fair for various companies of India on our department portal â€œwww.mprojgar.gov.inâ€</p>
                  
                   <p style='font-size:14px'>
                   We are hosting Career Counselling  which will be live on our Facebook and You tube channel. This Career Counselling  shall have various vacancies for fresherâ€™s & experiences which shall benefit the youths of Madhya Pradesh.</p>
                  
                   <p style='font-size:14px'>The said Career Counselling  is being hosted under the banner of Department of Employment & Skill Development and will be operated by Yashaswi being the PPP partner with the Department of Employment & Skill Development .
</p>
<br>
<p style='font-size:14px'><strong>We would like to invite you for attending Career Counselling  as Jobseeker.</strong></p>
                    <p style='font-size:14px;color:red'><strong >Day & Date : </strong>${dt} i.e. ${sessionStorage.FromDate}</p> 
                    <p style='font-size:14px;color:red'><strong> Time: </strong>${sessionStorage.TimeRange}</p>   
<p style='font-size:14px;color:red'><strong>Career Counselling  Link:</strong><p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join  Career Counselling </a></strong></p>`;     

          $('#subjecttxt').val(subject);
          $('.note-editable').html(body);
                        $('#m_modal_5').modal('toggle');
                      }
                      else{
                        $('#m_modal_6').modal('toggle');
                      }
                    
                  }
                }          
// function CheckCandidate(type){

//       if(type=='email'){
//           if(sessionStorage.EventType=='Online'){
// var heading=`<p style='font-size:14px'>Please find link  below to attend jobfair online:</p>`
// var ifoffline=`<p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join The ${$('#formtype option:selected').text()}</a></strong></p>`
        
//           }
//           else{
//             var heading=`<p style='font-size:14px'>Please find details below to attend jobfair :</p>`
//             var ifoffline=`<p style='font-size:14px'><strong>Venue:</strong>${sessionStorage.Venue}</p>`
         
//           }
//           var subject=sessionStorage.EventType+' '+$('#formtype option:selected').text();
//           var body=`<p style='font-size:14px'>Dear Candidate</p>
//           <br>
//           ${heading}
//           <br>
//           <p style='font-size:14px'><strong> ${$('#formtype option:selected').text()} Title: </strong>${sessionStorage.Title}</p> 
//           <p style='font-size:14px'><strong> ${$('#formtype option:selected').text()} Date: </strong> ${sessionStorage.FromDate}</p>
//           ${ifoffline} `
//           $('#subjecttxt').val(subject);
//           $('.note-editable').html(body);
//         $('#m_modal_5').modal('toggle');
//       }
//       else{
//         $('#m_modal_6').modal('toggle');
//       }
    
//   }

 
      
function SendAlert(type) {
   
    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function ()  {
       var emailID=this.value.split('_')[1];
       //var mobNo=$(this).find('td.can_mobile').text();
      // console.log('emailID',emailID);
        if (type == 'email') {
          sentmailglobal(emailID,$('.note-editable').html(),$('#subjecttxt').val(),'helpdesk.mprojgar@mp.gov.in');

                //reset();
          toastr.success('Mail Sent Successfully')
            
        }
        if (type == 'mobile') {
            // sendemail(this.value)
            sentSmsGlobal(this.id,$('#SmsDetail').val(),'1307159886762924438');
            toastr.success('SMS Sent Successfully')
           // reset();
        }

    });
   
}
function checkAll() {
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').prop('checked', true);
    } else {
        $('input:checkbox').prop('checked', false);
    }
}
    
    