function fetchcounsellingDetail() {

    var path = serverpath + "getcounsellingform/"+$('#month').val()+"/"+$('#year').val()+""
    ajaxget(path, 'parsedatacounsellingDetail', 'comment', "control");
}
function parsedatacounsellingDetail(data) {
    data = JSON.parse(data)
    var appenddata = "";

    var data1 = data[0];
    for (var i = 0; i < data1.length; i++) {
        
        appenddata += `<tr>
        <th>${data1[i].SrNo}</th>
        <th>${data1[i].CandidateRegNo}</th>
        <th>${data1[i].CandidateName}</th>
        <th>${data1[i].DateOfBirth}</th>
        <th>${data1[i].EntryDate==null?'':data1[i].EntryDate}</th>
        <th><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=setCandidateId(${data1[i].CandidateId})>Feedback</button></th>
        </tr>`
      }

    jQuery("#tbodyvalue").html(appenddata);


}
function FillMonthcounselling(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthcounselling(data,control){  
    data = JSON.parse(data)
    $.unblockUI()
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthcounselling('parsedatasecuredFillMonthcounselling',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}
function setCandidateId(CandidateId){
sessionStorage.counsellingCandidateId=CandidateId;
$('#m_modal_4').modal('toggle');

}
function InsSummaryForm(){
 
   
var MasterData ={
    "p_CandidateId":sessionStorage.counsellingCandidateId,
    "p_SummName":$("#SummName").val(),
    "p_SummAddress":$("#SummAddress").val(),
    "p_Summ_1":$("#Summ_1").val(),
    "p_Summ_2_1":$("#Summ_2_1").val(),
    "p_Summ_2_2":$("#Summ_2_2").val(),
    "p_Summ_2_3": $("#Summ_2_3").val(),
    "p_Summ_2_4":$("#Summ_2_4").val(),
    "p_SummDate":$('#m_datepicker_5').val(),
    "p_SummConsName":$('#SummConsName').val(),
    "p_SummConsComment":$('#SummConsComment').val(),
    "p_SummSpecComment":$('#SummSpecComment').val(),
    "p_SpecDate":$('#m_datepicker_2').val(),
    "p_SpecName":$('#SpecName').val(),
    "p_CaseNo":$('#CaseNo').val(),
    "p_CaseDate":$('#m_datepicker_1').val(),
    "p_Name":$('#Name').val(),
    "p_Age":$('#Age').val(),
    "p_EducationalQualification":$('#EducationalQualification').val(),
    "p_ConsRemark":$('#ConsRemark').val(),
    "p_SpecRemark":$('#SpecRemark').val(),
    "p_SpecialRemark":$('#SpecialRemark').val()
    
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "summaryform";
securedajaxpost(path, 'parsrdatasummary', 'comment', MasterData, 'control')
}
function parsrdatasummary(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            InsSummaryForm();
    }
    else{
     
        
       if(data){
        resetMode();
        $('#m_modal_4').modal('toggle');
        toastr.success("Submit Successful", "", "success")
        return true;
    }
}
}
function   resetMode(){
    sessionStorage.counsellingCandidateId=0;
    $("#SummName").val(''),
  $("#SummAddress").val(''),
    $("#Summ_1").val(''),
 $("#Summ_2_1").val(''),
    $("#Summ_2_2").val(''),
     $("#Summ_2_3").val(''),
  $("#Summ_2_4").val(''),
   $('#m_datepicker_5').val(''),
   $('#SummConsName').val(''),
    $('#SummConsComment').val(''),
  $('#SummSpecComment').val(''),
  $('#m_datepicker_2').val(''),
   $('#SpecName').val(''),
    $('#CaseNo').val(''),
  $('#m_datepicker_1').val(''),
    $('#Name').val(''),
    $('#Age').val(''),
  $('#EducationalQualification').val(''),
   $('#SpecRemark').val(''),
   $('#SpecialRemark').val('')
   $('#ConsRemark').val('')
} 