function InsUpdCompanyType(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Company_type_id":localStorage.p_Company_type_id,
    "p_Company_type":$.trim(jQuery("#companyN").val()),
    "p_Sector_ID":jQuery("#companyS").val(),
    "p_Active_YN":chkval,
    
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "companytype";
securedajaxpost(path, 'parsrdatacompanytype', 'comment', MasterData, 'control')
}

function parsrdatacompanytype(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdCompanyType()
	}
	else if (data[0][0].ReturnValue == "1") {
        Fetchcompanytype()
        resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        Fetchcompanytype()
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_Id = "0";
    $("#chkActiveStatus")[0].checked = false;
        jQuery("#companyN").val("")
        $("#companyS").val("0");
    }
function Fetchcompanytype() {
	var path = serverpath + "companytype/0/0"
	ajaxget(path, 'parsedataFetch', 'comment', "control");
}
function parsedataFetch(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
		 	active = "N"
	 }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].Company_type_id + "</td><td style='    word-break: break-word;'>" + data1[i].Company_type + "</td><td>"+data1[i].Description+"</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button' onclick=EditMode("+data1[i].Company_type_id+",'"+encodeURI(data1[i].Company_type)+"','"+encodeURI(data1[i].Sector_ID)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMode(Company_type_id,Company_type,Sector_ID,Active_YN) {
    jQuery("#companyN").val(decodeURI(Company_type))
    jQuery("#companyS").val(decodeURI(Sector_ID))
    localStorage.p_Company_type_id =Company_type_id
    if (decodeURI(Active_YN) == '0') {
		$("#chkActiveStatus")[0].checked = false;
	}
	else {
		$("#chkActiveStatus")[0].checked = true;
    }
}
function CheckValidationcompanytype(){
    if(jQuery("#companyS").val()=="0"){
        getvalidated('companyS','select','Company Sector');
        return false;
    }
  else  if($.trim(jQuery("#companyN").val())==" "){
        getvalidated('companyN','text','Company Name');
        return false;
    }
   
    else{
        InsUpdCompanyType();
    }
}
function FillSecuredSector(funct,control) {
    var path =  serverpath + "secured/sector/0/1"
    securedajaxget(path,'parsedatasecuredFillSector','comment',control);
}

function parsedatasecuredFillSector(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredSector('parsedatasecuredFillSector',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Company Sector"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sector_Id).html(data1[i].Description));
             }
             
        }
              
}
$(function Alphabet() {
        
    $('#companyN').keydown(function(e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
  });