// function FillSecuredDistrict_Tehsil(funct,control) {
//     var path =  serverpath + "secured/district/19/0/0/0"
//     securedajaxget(path,funct,'comment',control);
// }

// function parsedatasecuredFillSecuredDistrict(data,control){  
//     data = JSON.parse(data)
//     if (data.message == "New token generated"){
//         sessionStorage.setItem("token", data.data.token);
//         FillSecuredDistrict_Tehsil('parsedatasecuredFillSecuredDistrict',control);
//     }
//     else if (data.status == 401){
//         toastr.warning("Unauthorized", "", "info")
//         return true;
//     }
//         else{
//             jQuery("#"+control).empty();
//             var data1 = data[0];
//               jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
//             for (var i = 0; i < data1.length; i++) {
//                 jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
//              }
             
//         }
              
// }
function InsUpdcoursetype(){
	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_CourseTypeId":localStorage.CourseTypeId,
    "p_CourseTypeName":$.trim(jQuery("#coursetypename").val()),
    "p_IsActive":chkval,
    "p_IpAddress":0,
    "p_UserId":0


};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "coursetype";
securedajaxpost(path, 'parsrdatactmaster', 'comment', MasterData, 'control')
}

function parsrdatactmaster(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdcoursetype();
	}
	else if (data[0][0].ReturnValue == "1") {
       Fetchcoursetype();
       resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        Fetchcoursetype();
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_CourseTypeId = "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#coursetypename").val("");
    }
function Fetchcoursetype() {
	var path = serverpath + "coursetype/0/0"
	ajaxget(path, 'parsrdatacoursetypemaster', 'comment', "control");
}
function parsrdatacoursetypemaster(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].IsActive == '0') {
		 	active = "N"
	 }
		 if (data1[i].IsActive == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].CourseTypeId  + "</td><td style='word-break: break-word;'>" + data1[i].CourseTypeName  + "</td><td style='word-break: break-word;'>" + active  + "</td><td><button type='button' onclick=EditMode("+data1[i].CourseTypeId+",'"+encodeURI(data1[i].IsActive)+"','"+encodeURI(data1[i].CourseTypeName)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);
    }
function EditMode(CourseTypeId,IsActive,CourseTypeName) {
  
    localStorage.p_CourseTypeId =CourseTypeId
    if (decodeURI(IsActive) == '0') {
		$("#ActiveStatus")[0].checked = false;
	}
	else {
		$("#ActiveStatus")[0].checked = true;
    }
    jQuery("#coursetypename").val(decodeURI(CourseTypeName))
}
function CheckValidationCourseTypeMaster(){
  
    
  if($.trim(jQuery("#coursetypename").val())==""){
        getvalidated('coursetype','text','Course Type Name');
        return false;
    }
    else{
        InsUpdcoursetype();
    }
}