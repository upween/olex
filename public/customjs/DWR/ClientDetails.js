function addNewunfilledb() {
  var x = $('.clientdetailsrow').length;
  
  var dwrrow = `<tr class="clientdetailsrow" id='clientdetailsrow${x}'>
  <td>1</td>
  <td></td>
  <td ><input type="text" id="compname${x}"></td>
  <td> <select type="text" name="Designation" id="industry${x}" class="form-control Skill" placeholder="Sector" autocomplete="off"></select></td>
  <td> <select type="text" name="name" id="DESIGNATION${x}" class="form-control designation" > </select></td>
  <td><textarea class="form-control" onkeypress="if(this.value.length>=200) { return false;}" style="height: 110px;" id="JDINDETAIL${x}" name="permanentaddress" placeholder="Enter Head Office Address"></textarea></td>
  
  <td> <select type="text" name="name" id="VACANCY${x}" class="form-control Vacancy"> </select></td>
  <td><input type="number" onkeypress="if(this.value.length >= 5){return false}" id="POSITIONS${x}"></td>
  <td><input type="number" id="CTC${x}"></td>
  <td> <select type="text" name="name" id="STATE${x}" class="form-control state"> </select></td>
  <td> <select type="text" name="name" id="LOCATION${x}" class="form-control district"> </select></td>
  <td>
  <select type="text" name="name" id="GENDER${x}" class="form-control">
  <option value="M">Male </option>
  <option value="F">Female </option>
  <option value="B">Both</option>
  </select>
  </td>
  <td><input type="number" id="AGECRITERIAMin${x}"></td>
  <td><input type="number" id="AGECRITERIAMax${x}"></td>
  <td><input type="text" id="QUALIFICATION${x}"></td>
  <td> <select type="text" name="name" id="EXPERIENCE${x}"class="form-control">
  <option value="0">Select </option>
  <option value="0-6 Months">0-6 Months </option>
  <option value="6-12 Months">6-12 Months</option>
  <option value="1 Years">1 Years </option>
  <option value="2 Years">2 Years</option>
  <option value="3 Years">3 Years</option>
  <option value="4 Years">4 Years</option>
  <option value="5 Years">5 Years</option>
  <option value="6 Years">6 Years</option>
  <option value="7 Years">7 Years</option>
  <option value="8 Years">8 Years</option>
  <option value="9 Years">9 Years</option>
  <option value="10 Years">10 Years</option>
  <option value="More than 10 Years">More than 10 Years </option>
  </select></td>

  <td> <select type="text" name="name" id="RECRUITMENT${x}"class="form-control">
  <option value="0">Select</option>
  <option value="Neem">Neem</option>
  <option value="NAPS">NAPS</option>
  <option value="Company Role">Company Role </option>
  <option value="Outsourcing">Outsourcing </option>
  </select></td>
  <td> <select type="text" name="name" id="PERMITTED${x}"class="form-control">
  <option value="Yes">Yes </option>
  <option value="No">No </option>
  </select></td>
  <td><input type="text" id="MANAGER${x}"></td>
  <td><input type="number" onkeypress="if(this.value.length >= 10){return false}" id="MOBILE${x}"></td>
  <td><input type="text" id="EMAIL${x}"></td>
  <td> <select type="text" name="name" id="STATUS${x}"class="form-control">
  <option value="Paid">Paid </option>
  <option value="Unpaid">Unpaid </option>
  </select></td>
  <td><input type="text" id="COMMERCIAL${x}"></td>
  <td><input type="text" id="REPLACEMENT${x}"></td>
  <td><input type="text" id="PAYMENT${x}"></td>
  <td><button type="button" class="btn btn-success" onclick="$('#clientdetailsrow`+x+`').remove()" >Remove</button></td>
  <td> <button type="button" onclick="checkValidation(${x},'0');" id="Submit" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">Save</button></td>
  </tr>`;
  $('#tbodyvalue').append(dwrrow);
  FillDesignation(x);
  FillVacancy(x);
  Fillempstate(x);
  FillDistrict(x);
  FillSectoremp(x);
  
  }
  
  function FillDesignation(control) {
  var path = serverpath + "secured/adminDesignation/0/1"
  securedajaxget(path,'parsedatasecuredFillDesignation','comment',control);
  }
  
  function parsedatasecuredFillDesignation(data,control){
  data = JSON.parse(data)
  if (data.message == "New token generated"){
  sessionStorage.setItem("token", data.data.token);
  FillDesignation('parsedatasecuredFillDesignation','PlaceofWork');
  }
  else if (data.status == 401){
  toastr.warning("Unauthorized", "", "info")
  return true;
  }
  else{
  jQuery(`#DESIGNATION${control}`).empty();
  var data1 = data[0];
  jQuery(`#DESIGNATION${control}`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`#DESIGNATION${control}`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
  }
  }
  
  }
  
  function FillVacancy(control) {
  jQuery.ajax({
  type: "GET",
  contentType: "application/json; charset=utf-8",
  url: serverpath + "vacancytype/0/1",
  cache: false,
  dataType: "json",
  success: function (data) {
  jQuery(`#VACANCY${control}`).empty();
  var data1 = data[0];
  jQuery(`#VACANCY${control}`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`#VACANCY${control}`).append(jQuery("<option></option>").val(data1[i].VacancyType_id).html(data1[i].VacancyType));
  }
  },
  error: function (xhr) {
  toastr.success(xhr.d, "", "error")
  return true;
  }
  });
  }
  
  function Fillempstate(control) {
  var path = serverpath + "state/0/0/0/0"
  securedajaxget(path,'parsedatasecuredFillempstate','comment',control);
  }
  
  function parsedatasecuredFillempstate(data,control){
  data = JSON.parse(data)
  if (data.message == "New token generated"){
  sessionStorage.setItem("token", data.data.token);
  Fillempstate('parsedatasecuredFillempstate','state');
  }
  else if (data.status == 401){
  toastr.warning("Unauthorized", "", "info")
  return true;
  }
  else{
  jQuery(`#STATE${control}`).empty();
  var data1 = data[0];
  jQuery(`#STATE${control}`).append(jQuery("<option ></option>").val("0").html("Select State"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`#STATE${control}`).append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
  }
  }
  
  }
  function FillDistrict(control) {
  var path = serverpath + "district/0/0/0/0"
  securedajaxget(path,'parsedatasecuredFillDistrict','comment',control);
  }
  
  function parsedatasecuredFillDistrict(data,control){
  data = JSON.parse(data)
  if (data.message == "New token generated"){
  sessionStorage.setItem("token", data.data.token);
  FillDistrict('parsedatasecuredFillDistrict','PlaceofWork');
  }
  else if (data.status == 401){
  toastr.warning("Unauthorized", "", "info")
  return true;
  }
  else{
  jQuery(`#LOCATION${control}`).empty();
  var data1 = data[0];
  jQuery(`#LOCATION${control}`).append(jQuery("<option ></option>").val("0").html("Select District"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`#LOCATION${control}`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
  }
  }
  
  }
  
  function FillSectoremp(control){
  var path = serverpath + "SkillSet/0/0"
  securedajaxget(path, 'parsedatasectoremp', 'comment', control);
  }
  function parsedatasectoremp(data, control) {
  data = JSON.parse(data)
  if (data.message == "New token generated") {
  sessionStorage.setItem("token", data.data.token);
  FillSectoremp();
  }
  else if (data.status == 401) {
  toastr.warning("Unauthorized", "", "info")
  return true;
  }
  else {
  jQuery(`#industry${control}`).empty();
  var data1 = data[0];
  jQuery(`#industry${control}`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`#industry${control}`).append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
  }
  }
  }
  
  function InsupdClientDetail(i,dwrid) {
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  
  var MasterData ={
  
  "p_DwrClientId":dwrid,
  "p_RegNo":'0',
  "p_CompName":$(`#compname${i}`).val(),
  "p_Sector_Id":$(`#industry${i}`).val(),
  "p_Designation":$(`#DESIGNATION${i}`).val(),
  "p_JobProfile":$(`#JDINDETAIL${i}`).val(),
  "p_VacancyType":$(`#VACANCY${i}`).val(),
  "p_TotalVacancy":$(`#POSITIONS${i}`).val(),
  "p_CTC":$(`#CTC${i}`).val(),
  "p_JobState":$(`#STATE${i}`).val(),
  "p_JobLocation":$(`#LOCATION${i}`).val(),
  "p_Gender":$(`#GENDER${i}`).val(),
  "p_MinAge":$(`#AGECRITERIAMin${i}`).val(),
  "p_MaxAge":$(`#AGECRITERIAMax${i}`).val(),
  "p_EssentialQualification":$(`#QUALIFICATION${i}`).val(),
  "p_Experience":$(`#EXPERIENCE${i}`).val(),
  "p_RecruitmentType":$(`#RECRUITMENT${i}`).val(),
  "p_DiscloseCompName":$(`#PERMITTED${i}`).val(),
  "p_HRName":$(`#MANAGER${i}`).val(),
  "p_HRNumber":$(`#MOBILE${i}`).val(),
  "p_HREmail":$(`#EMAIL${i}`).val(),
  "p_CommercialStatus":$(`#STATUS${i}`).val(),
  "p_CommercialDetails":$(`#COMMERCIAL${i}`).val(),
  "p_ReplacementPeriod":$(`#REPLACEMENT${i}`).val(),
  "p_PaymentDuration":$(`#PAYMENT${i}`).val(),
  "p_EntryBy":sessionStorage.Emp_Id,
  "p_EntryIP":sessionStorage.ipAddress,
  "p_EntryDate":sessionStorage.datenow
  
  };
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "cliententry";
  securedajaxpost(path, 'parsedataclientemp', 'comment', MasterData, 'control')
  
  }
  function parsedataclientemp(data, control) {
  data = JSON.parse(data);
  console.log(data);
  //fetchDWRCount();
  if(data[0][0].ReturnValue=='1'){
    toastr.success('Insert Successfully', "", "success");
  }
  if(data[0][0].ReturnValue=='2'){
    toastr.success('Update Successfully', "", "success");
  }
  if(data[0][0].ReturnValue=='3'){
    toastr.warning('Client Already Exsist', "", "Info");
  }
  }
  
  function checkValidation(x,id){
  if($(`#compname${x}`).val()==''){
  
  toastr.warning('Please Enter CompanyName')
  return false;
  }
  if($(`#industry${x}`).val()=='0'){
  
  toastr.warning('Please Select SectorName')
  return false;
  }
  if($(`#DESIGNATION${x}`).val()=='0'){
  
  toastr.warning('Please Select Designation')
  return false;
  }
  if($(`#JDINDETAIL${x}`).val()==''){
  
  toastr.warning('Please Enter JD Details')
  return false;
  
  
  } if($(`#VACANCY${x}`).val()=='0'){
  
  toastr.warning('Please Select Vacancy')
  return false;
  
  } if($(`#POSITIONS${x}`).val()==''){
  
  toastr.warning('Please Enter Number Of Positions')
  return false;
  
  } if($(`#CTC${x}`).val()==''){
  
  toastr.warning('Please Enter Ctc')
  return false;
  
  } if($(`#STATE${x}`).val()=='0'){
  
  toastr.warning('Please Select State')
  return false;
  
  } if($(`#LOCATION${x}`).val()=='0'){
  
  toastr.warning('Please Select Location')
  return false;
  
  } if($(`#GENDER${x}`).val()=='0'){
  
  toastr.warning('Please Select Gender')
  return false;
  
  } if($(`#AGECRITERIAMin${x}`).val()==''){
  
  toastr.warning('Please Enter Min Age')
  return false;
  
  } if($(`#AGECRITERIAMax${x}`).val()==''){
  
  toastr.warning('Please Enter Max Age')
  return false;
  
  } if($(`#QUALIFICATION${x}`).val()==''){
  
  toastr.warning('Please Enter Qualification')
  return false;
  
  } if($(`#EXPERIENCE${x}`).val()=='0'){
  
  toastr.warning('Please Select Experience')
  return false;
  
  } if($(`#RECRUITMENT${x}`).val()=='0'){
  
  toastr.warning('Please Select Reccruitment')
  return false;
  
  
  } if($(`#MANAGER${x}`).val()==''){
  
  toastr.warning('Please Enter Hr Manager Name')
  return false;
  
  } if($(`#MOBILE${x}`).val()==''){
  
  toastr.warning('Please Enter Mobile Number')
  return false;
  
  } if($(`#EMAIL${x}`).val()==''){
  
  toastr.warning('Please Enter EmailId')
  return false;
  
  } if($(`#STATUS${x}`).val()=='0'){
  
  toastr.warning('Please Select Status')
  return false;
  
  } if($(`#COMMERCIAL${x}`).val()==''){
  
  toastr.warning('Please Enter Commercial Detalis')
  return false;
  
  }
  if($(`#REPLACEMENT${x}`).val()==''){
  
  toastr.warning('Please Enter Replacement Period')
  return false;
  
  } if($(`#PAYMENT${x}`).val()==''){
  
  toastr.warning('Please Enter Payment Duration')
  return false;
  }
  else{
  InsupdClientDetail(x,id)
  }
  }



  function fetchDWRbydate(){
    var date= sessionStorage.Date.split('/')
    var Date=date[2]+'-'+date[1]+'-'+date[0] 
    var path =  serverpath + "clientreport/"+sessionStorage.ManagerId+"/"+Date+"";  
   ajaxget(path,'parsedatafetchDWRbydate','comment',"control");
  }
  function parsedatafetchDWRbydate(data){
  data = JSON.parse(data)
  var appenddata='';
  var tablehead="<th style='border-right: inset;'>S.No</th><th style='border-right: inset;'>Job Id</th><th style='border-right: inset;'>Company Name</th><th style='border-right: inset;'>Industry/Sector</th><th style='border-right: inset;'> Designation</th><th style='border-right: inset;'>JD in Detail</th><th style='border-right: inset;'>Type Of Vacancy</th><th style='border-right: inset;'> Number of Positions</th><th style='border-right: inset;'>CTC/PM</th><th style='border-right: inset;'> Job State</th><th style='border-right: inset;'> Job Location</th><th style='border-right: inset;'>Gender</th><th style='border-right: inset;' colspan=2>Age Criteria<br>Min Age/Max Age </th><th style='border-right: inset;'>Essential Qualification</th><th style='border-right: inset;'>Experience(in years) </th><th style='border-right: inset;'>Recruitment Type</th><th style='border-right: inset;'>Permitted to disclose company's name on portal</th><th style='border-right: inset;'>HR Manager Name </th><th style='border-right: inset;'>HR Manager Name Mobile Number</th><th style='border-right: inset;'> HR Manager Name Email ID</th><th style='border-right: inset;'>Commercial Status</th><th style='border-right: inset;'>Commercial Details</th><th style='border-right: inset;'>Replacement Period</th><th style='border-right: inset;'>Payment Duration</th><th style='border-right: inset;'>Edit</th><th>Save</th>";
  $('#tablehead').html(tablehead);
  for (var i = 0; i < data[0].length; i++) {
    appenddata+=`<td>`+[i+1]+`</td>
		<td></td>
		<td ><input type="text" id="compname`+i+`" value="`+data[0][i].CompName+`" class="rpt"></td>
		<td> <select type="text" name="Designation" value="`+data[0][i].Sector_Id+`" id="industry`+i+`" class="inputresize form-control Skill rpt" placeholder="Sector" autocomplete="off"></select></td>
		<td> <select type="text" name="name" value="`+data[0][i].Designation+`" id="DESIGNATION`+i+`" class="inputresize form-control designation rpt"> </select></td>
		<td><textarea class="form-control rpt" value="`+data[0][i].JobProfile+`"  onkeypress="if(this.value.length>=200) { return false;}" style="height: 110px;width: 252px;" id="JDINDETAIL`+i+`" name="permanentaddress" placeholder="Enter Job Description Details">`+data[0][i].JobProfile+`</textarea></td>
		
		<td> <select type="number" name="name" value="`+data[0][i].VacancyType+`" id="VACANCY`+i+`" class="inputresize form-control Vacancy rpt"> </select></td>
		<td><input type="number" value="`+data[0][i].TotalVacancy+`" onkeypress="if(this.value.length >= 5){return false}" id="POSITIONS`+i+`" class="rpt"></td>
		<td><input type="text" id="CTC`+i+`" value="`+data[0][i].CTC+`" class="rpt"></td>
		<td> <select type="text" name="name" value="`+data[0][i].JobState+`" id="STATE`+i+`" class="inputresize form-control state rpt"> </select></td>
		<td> <select type="text" name="name" value="`+data[0][i].JobLocation+`" id="LOCATION`+i+`" class="inputresize form-control district rpt"> </select></td>
		<td> <select type="text" name="name" value="`+data[0][i].Gender+`" id="GENDER`+i+`" class="form-control inputresize rpt">
		<option value="M">Male </option>
		<option value="F">Female </option>
		<option value="B">Both</option>
		</select></td>
		<td><input type="number" id="AGECRITERIAMin`+i+`" value="`+data[0][i].MinAge+`" class="rpt"></td>
		<td><input type="number" id="AGECRITERIAMax`+i+`" value="`+data[0][i].MaxAge+`" class="rpt"></td>
		<td><input type="text" id="QUALIFICATION`+i+`" value="`+data[0][i].EssentialQualification+`" class="rpt"></td>
		<td> <select type="text" name="name" id="EXPERIENCE`+i+`" value="`+data[0][i].Experience+`" class="form-control inputresize rpt">
		<option value="0">Select</option>
		<option value="0-6 Months">0-6 Months</option>
		<option value="6-12 Months">6-12 Months</option>
		<option value="1 Years">1 Years</option>
		<option value="2 Years">2 Years</option>
		<option value="3 Years">3 Years</option>
		<option value="4 Years">4 Years</option>
		<option value="5 Years">5 Years</option>
		<option value="6 Years">6 Years</option>
		<option value="7 Years">7 Years</option>
		<option value="8 Years">8 Years</option>
		<option value="9 Years">9 Years</option>
		<option value="10 Years">10 Years</option>
		<option value="More than 10 Years">More than 10 Years</option>
		
		</select></td>
		<td> <select type="text" name="name" value="`+data[0][i].RecruitmentType+`" id="RECRUITMENT`+i+`" class="form-control inputresize rpt">
		<option value="0">Select</option>
		<option value="Neem">Neem</option>
		<option value="NAPS">NAPS</option>
		<option value="Company Role">Company Role</option>
		<option value="Outsourcing">Outsourcing </option>
		</select></td>
		<td> <select type="text" name="name" value="`+data[0][i].DiscloseCompName+`" id="PERMITTED`+i+`" class="form-control rpt">
		<option value="Y">Yes </option>
		<option value="N">No</option>
		</select></td>
		<td><input type="text" id="MANAGER`+i+`" value="`+data[0][i].HRName+`" class="rpt"></td>
		<td><input type="number" value="`+data[0][i].HRNumber+`" onkeypress="if(this.value.length >= 10){return false}" id="MOBILE`+i+`" class="rpt"></td>
		<td><input type="text" id="EMAIL`+i+`" value="`+data[0][i].HREmail+`" class="rpt"></td>
		<td> <select type="text" name="name" value="`+data[0][i].CommercialStatus+`" id="STATUS`+i+`" class="form-control inputresize rpt">
    <option value="Paid">Paid </option>
		<option value="Unpaid">Unpaid </option>
		</select></td>
		<td><input type="text" id="COMMERCIAL`+i+`" value="`+data[0][i].CommercialDetails+`" class="rpt"></td>
		<td><input type="text" id="REPLACEMENT`+i+`" value="`+data[0][i].ReplacementPeriod+`" class="rpt"></td>
		<td><input type="text" id="PAYMENT`+i+`" value="`+data[0][i].PaymentDuration+`" class="rpt"></td>
    <th><button type="button"  onclick=$(".rpt").attr("disabled",false)   id="btnedit`+i+`" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">Edit</button></th>
    <th><button type="button" onclick=checkValidation(`+i+`,`+data[0][i].DwrClientId+`) id="Submit" class="btn btn-success rpt" style="background-color:#716aca;border-color:#716aca;">Save</button></th></tr>`;
  }
  $('#tbodyvalue').html(appenddata);
  $('.rpt').attr( "disabled",true);
  FillDesignationrpt(data);
  FillVacancyrpt(data);
  Fillempstaterpt(data);
  FillDistrictrpt(data);
  FillSectoremprpt(data);
  sessionStorage.removeItem('page')
  }
  
  
  function fetchDWRCount(){
    var path =  serverpath + "dwrcount/"+sessionStorage.Emp_Id+"/"+sessionStorage.datenow+"";
    ajaxget(path,'parsedatafetchDWRCount','comment',"control");
}
function parsedatafetchDWRCount(data){
data = JSON.parse(data)
$('#signClientno').html(data[0].ClientCount);
$('#TodaysJoining').html(data[0].JoiningCount);
}







function FillDesignationrpt(control) {
  var path = serverpath + "secured/adminDesignation/0/1"
  securedajaxget(path,'parsedatasecuredFillDesignationrpt','comment',control);
  }
  
  function parsedatasecuredFillDesignationrpt(data,control){
  data = JSON.parse(data)
  
  jQuery(`.designation`).empty();
  var data1 = data[0];
  jQuery(`.designation`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`.designation`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
  } 
  for (var i = 0; i < control[0].length; i++) {
    jQuery(`#DESIGNATION${i}`).val(control[0][i].Designation);
}
  }
  
  
  function FillVacancyrpt(control) {
  jQuery.ajax({
  type: "GET",
  contentType: "application/json; charset=utf-8",
  url: serverpath + "vacancytype/0/0",
  cache: false,
  dataType: "json",
  success: function (data) {
  jQuery(`.Vacancy`).empty();
  var data1 = data[0];
  jQuery(`.Vacancy`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`.Vacancy`).append(jQuery("<option></option>").val(data1[i].VacancyType_id).html(data1[i].VacancyType));
  }
  for (var i = 0; i < control[0].length; i++) {
    jQuery(`#VACANCY${i}`).val(control[0][i].VacancyType);
  }
  },
  error: function (xhr) {
  toastr.success(xhr.d, "", "error")
  return true;
  }
  });
  }
  
  function Fillempstaterpt(control) {
  var path = serverpath + "state/0/0/0/0"
  securedajaxget(path,'parsedatasecuredFillempstaterpt','comment',control);
  }
  
  function parsedatasecuredFillempstaterpt(data,control){
  data = JSON.parse(data)
 
  jQuery(`.state`).empty();
  var data1 = data[0];
  jQuery(`.state`).append(jQuery("<option ></option>").val("0").html("Select State"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`.state`).append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
  }
  for (var i = 0; i < control[0].length; i++) {
    jQuery(`#STATE${i}`).val(control[0][i].JobState);
  }
  }
  
  function FillDistrictrpt(control) {
  var path = serverpath + "district/0/0/0/0"
  securedajaxget(path,'parsedatasecuredFillDistrictrpt','comment',control);
  }
  
  function parsedatasecuredFillDistrictrpt(data,control){
  data = JSON.parse(data)
 
  jQuery(`.district`).empty();
  var data1 = data[0];
  jQuery(`.district`).append(jQuery("<option ></option>").val("0").html("Select District"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`.district`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
  }
  for (var i = 0; i < control[0].length; i++) {
    jQuery(`#LOCATION${i}`).val(control[0][i].JobLocation);
  }
  }
  
  
  function FillSectoremprpt(control){
  var path = serverpath + "SkillSet/0/0"
  securedajaxget(path, 'parsedatasectoremprpt', 'comment', control);
  }
  function parsedatasectoremprpt(data, control) {
  data = JSON.parse(data)
 
  jQuery(`.Skill`).empty();
  var data1 = data[0];
  jQuery(`.Skill`).append(jQuery("<option ></option>").val("0").html("Select"));
  for (var i = 0; i < data1.length; i++) {
  jQuery(`.Skill`).append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
  }
  for (var i = 0; i < control[0].length; i++) {
    jQuery(`#industry${i}`).val(control[0][i].Sector_Id);
  }
  }
