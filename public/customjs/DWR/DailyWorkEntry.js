

function dailyworkentry(){
    var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); 
var yyyy = today.getFullYear();
today = yyyy+'-'+mm+'-'+dd;
$('#date').html(today);

    var MasterData = {
       
         "p_DistrictId": sessionStorage.DistrictId,
         "p_ClientVisited": $('#visitedClientno').val()==''?0:$("#visitedClientno").val(),
         "p_ClientSigned": $('#signClientno').html()==''?0:$('#signClientno').html(),
         "p_CandidateCalling": $('#noofcandidatecalling').val()==''?0:$('#noofcandidatecalling').val(),
         "p_CandidateInerview": $('#noofcandidatesinterview').val()==''?0:$('#noofcandidatesinterview').val(),
         "p_CandidateSelected": $('#candidateselected').val()==''?0:$('#candidateselected').val(),
         "p_ClientsForInterview": $('#ClientsForInterview').val()==''?0:$('#ClientsForInterview').val(),
         "p_TodaysJoining": $('#TodaysJoining').html()==''?0:$('#TodaysJoining').html(),
         "p_JobFair": $('#JobFair').val()==''?0:$('#JobFair').val(),
         "p_JobFairFootfall": $('#JobFairFootfall').val()==''?0:$('#JobFairFootfall').val(),
         "p_JobFairClients": $('#JobFairClients').val()==''?0:$('#JobFairClients').val(),
         "p_JobFairShortlisting": $('#JobFairShortlisting').val()==''?0:$('#JobFairShortlisting').val(),
         "p_EntryBy": sessionStorage.Emp_Id,
         "p_EntryIP": sessionStorage.ipAddress,
         "p_EntryDate":today,
         "p_msgsent": $('#noofmsg').val()==''?0:$('#noofmsg').val(),
         "p_ccsession": $('#ccsession').val()==''?0:$('#ccsession').val(),
         "p_Remark":$('#Remark').val()
 }
 MasterData = JSON.stringify(MasterData)
 var path = serverpath + "dailyworkentry";
 ajaxpost(path, 'parsedatadailyworkentry', 'comment', MasterData, 'control')
}

function parsedatadailyworkentry(data) {
data = JSON.parse(data)
toastr.success('Submit  Successfully');
$('.ent').val('');
}




function fetchDWRbydate(){

if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
var EmpId='0';
}
else{
    var EmpId=sessionStorage.Emp_Id;
}
$('#date1').html($('#m_datepicker_6').val())

    var date= $('#m_datepicker_6').val().split('/')
    var Date=date[2]+'-'+date[1]+'-'+date[0] 
        var path =  serverpath + "dailyworkreport/"+EmpId+"/"+Date+"";
        ajaxget(path,'parsedatafetchDWRbydate','comment',"control");
}
function parsedatafetchDWRbydate(data){
    data = JSON.parse(data)
var appenddata='';
var total=''
var ClientVisited=0;
var ClientSigned=0;
var CandidateCalling=0;
var CandidateInerview=0;
var CandidateSelected=0;
var ClientsForInterview=0;
var TodaysJoining=0;
var JobFair=0;
var JobFairFootfall=0;
var JobFairClients=0;
var JobFairShortlisting=0;
var msgsent=0;
var CCsession=0;
for (var i = 0; i < data[0].length; i++) {
    
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
     ClientVisited=ClientVisited+parseInt(data[0][i].ClientVisited);
 ClientSigned=ClientSigned+parseInt(data[0][i].ClientSigned);
 CandidateCalling=CandidateCalling+parseInt(data[0][i].CandidateCalling);
 CandidateInerview=CandidateInerview+parseInt(data[0][i].CandidateInerview);
 CandidateSelected=CandidateSelected+parseInt(data[0][i].CandidateSelected);
 ClientsForInterview=ClientsForInterview+parseInt(data[0][i].ClientsForInterview);
 TodaysJoining=TodaysJoining+parseInt(data[0][i].TodaysJoining);
 JobFair=JobFair+parseInt(data[0][i].JobFair);
 JobFairFootfall=JobFairFootfall+parseInt(data[0][i].JobFairFootfall);
 JobFairClients=JobFairClients+parseInt(data[0][i].JobFairClients);
 JobFairShortlisting=JobFairShortlisting+parseInt(data[0][i].JobFairShortlisting);
 msgsent=msgsent+parseInt(data[0][i].msgsent);
 CCsession=CCsession+parseInt(data[0][i].CCsession);
    total=`
            <tr style='    background: #615f5f96;'><td style='font-weight:700;'>Total</td>
            <td style='border-color:black;font-weight:700;'> `+[msgsent]+` </td>
            <td style='border-color:black;font-weight:700;' > `+[ClientVisited]+` </td>
            <td style='border-color:black;font-weight:700;'>`+[ClientSigned]+`</td> 
            <td style='border-color:black;font-weight:700;' > `+[CandidateCalling]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[CandidateInerview]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[CandidateSelected]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[ClientsForInterview]+` </td>
            <td style='border-color:black;font-weight:700;'>`+[TodaysJoining]+`</td>
            <td style='border-color:black;font-weight:700;'> `+[JobFair]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[JobFairFootfall]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[JobFairClients]+` </td>
            <td style='border-color:black;font-weight:700;'> `+[JobFairShortlisting]+` </td>
           
            <td style='border-color:black;font-weight:700;'> `+[CCsession]+` </td>
            <td></td>
            </tr>`;
}
        appenddata +=`  <tr>
        <td style='border-color:black;' id="district"> `+data[0][i].DistrictName +` </td>
        
        <td style='border-color:black;' id="JobFairShortlisting"> `+data[0][i].msgsent+` </td>
        <td style='border-color:black;' id="visitedClientno"> `+data[0][i].ClientVisited+` </td>
        <td style='border-color:black;' id="signClientno"><a href='/ClientDetails' onclick="sessionStorage.setItem('page','report');sessionStorage.setItem('ManagerId',${data[0][i].EntryBy})" style="font-size: initial;"> `+data[0][i].ClientSigned+`</a></td> 
        <td style='border-color:black;'  id="noofcandidatecalling"> `+data[0][i].CandidateCalling+` </td>
        <td style='border-color:black;' id="noofcandidatesinterview"> `+data[0][i].CandidateInerview+` </td>
        <td style='border-color:black;' id="candidateselected"> `+data[0][i].CandidateSelected+` </td>
        <td style='border-color:black;' id="ClientsForInterview"> `+data[0][i].ClientsForInterview+` </td>
        <td style='border-color:black;' id="TodaysJoining"><a href='/joiningdetails' onclick="sessionStorage.setItem('page','report');sessionStorage.setItem('ManagerId',${data[0][i].EntryBy})" style="font-size: initial;"> `+data[0][i].TodaysJoining+` </a></td>
        <td style='border-color:black;' id="JobFair"> `+data[0][i].JobFair+` </td>
        <td style='border-color:black;' id="JobFairFootfall"> `+data[0][i].JobFairFootfall+` </td>
        <td style='border-color:black;' id="JobFairClients"> `+data[0][i].JobFairClients+` </td>
        <td style='border-color:black;' id="JobFairShortlisting"> `+data[0][i].JobFairShortlisting+` </td>
        <td style='border-color:black;' id="JobFairShortlisting"> `+data[0][i].ccsession+` </td>
        <td style='border-color:black;' id="JobFairShortlisting"> `+data[0][i].Remark+` </td>
       
    </tr>`;
    }
    $('#tbodyvalue').html(appenddata+total); 
} 


function fetchDWRCount(){
            var path =  serverpath + "dwrcount/"+sessionStorage.Emp_Id+"/"+sessionStorage.datenow+"";
            ajaxget(path,'parsedatafetchDWRCount','comment',"control");
    }
    function parsedatafetchDWRCount(data){
        data = JSON.parse(data)
        if(data[0][0]){
            $('#signClientno').html(data[0][0].ClientCount);
            $('#TodaysJoining').html(data[0][0].JoiningCount);
        }    }