

function addNewfilledvacancyRowss() {
    var x = $('.addrowvacancydetail').length;

    var joiningdetails = `<tr class='addrowvacancydetail' id="joiningdetails${x}">
    <td></td>
    <td><input type="text" class="txtRegistrationNumber" id="txtRegistrationNumber${x}" onfocusout='CheckUser(${x})' onkeypress="if (this.value.length >= 14){return false;}"></td>
    <td><input type="text" id="candidatename${x}" disabled></td>
    <td><input type="text" class="txtAddress" id="txtAddress${x}" disabled></td>
    <td> <select type="text" name="name" class="form-control ddlCategory" id="ddlCategory${x}" style="height: 32px; width: 196px;" disabled> </select>
    </td>
    <td> <select type="text" name="name" class="form-control ddlDistrict" id="ddlDistrict${x}" style="height: 32px; width: 196px;" disabled> </select>
    </td>
    <td><input type="text" class="txtMobile" id="txtMobile${x}" onkeypress="if (this.value.length >= 10){return false;}" disabled ></td>
    <td><input type="text" class="txtUniqueIDNo" id="txtUniqueIDNo${x}"></td>
    <td> <select type="text" name="name" id="position${x}" class="form-control position" onchange=handleChangePosition("#position${x}") style="height: 32px; width: 196px;" > </select>
    </td>
    <td><select  class="form-control companyname" id="companyname${x}" onchange='handleExchChange(${x})'></select></td>
    <td><select  class="form-control subcompanyname rpttype"  id="subcompanyname${x}" style="width:200px;" disabled></select></td>
    <td><input type="text" class="companyHRname" id="companyHRname${x}"></td>
    <td><input type="text" class="companyHREmail" id="companyHREmail${x}"></td>
    <td><input type="text" class="companyHRnumber" id="companyHRnumber${x}" onkeypress="if (this.value.length >= 10){return false;}"></td>
    <td><input class="datepicker" id="joiningdate${x}" style="height: 32px; width: 196px;">
    </td>
    <td><input type="text" class="txtYearlyCTCOffered" id="txtYearlyCTCOffered${x}"></td>
    <td><input type="text" class="txtJobLocation" id="txtJobLocation${x}"></td>
   

      <td><button type="button" class="btn btn-success" onclick="$('#joiningdetails`+x+`').remove()">Remove</button></td>

     <td> <button type="button" onclick="checkValidation(${x},'0');"  id="Submit" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
            Save 
        </button></td>
    

</tr>`

    
    $('#tablebody').append(joiningdetails);
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'});
        FillPosition(x);
        FillCategoryss(x);
        FillDistrictss(x); 
        FillEmployer(x);
        FillSubcompany(x)
  }

  function FillPosition(control) {

    var path = serverpath + "adminDesignation/0/0";
    securedajaxget(path, 'parsedataPosition', 'comment', control)
    
}

function parsedataPosition(data,control) {
    data = JSON.parse(data)



    jQuery(`#position${control}`).empty();
    var data1 = data[0];


    jQuery(`#position${control}`).append(jQuery("<option></option>").val('0').html("Select"));

    for (var i = 0; i < data1.length; i++) {
        jQuery(`#position${control}`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
    }
    jQuery(`#position${control}`).append(jQuery("<option></option>").val('987654').html('Other'));

}

function FillCategoryss(control) {
    var path =  serverpath + "category/0/0/0/0"
    securedajaxget(path,'parsedatasecuredFillCategoryss','comment',control);
  }
  
  function parsedatasecuredFillCategoryss(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCategoryss('parsedatasecuredFillCategoryss',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery(`#ddlCategory${control}`).empty();
            var data1 = data[0];
            jQuery(`#ddlCategory${control}`).append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(`#ddlCategory${control}`).append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
             }
        }
              
  }
  function FillDistrictss(control) {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,'parsedatasecuredFillDistrictss','comment',control);
}

function parsedatasecuredFillDistrictss(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrictss('parsedatasecuredFillDistrictss',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery(`#ddlDistrict${control}`).empty();
            var data1 = data[0];
         
            for (var i = 0; i < data1.length; i++) {
                //jQuery("#"+control).append(jQuery("<option></option>").val("0").html("Select District"));

                jQuery(`#ddlDistrict${control}`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             
        }
              
}

function InsupdJoiningDetail(i,dwrid) {
    // $('.addrowvacancydetail').each(function (i) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    var Joining_Date =jQuery(`#joiningdate${i}`).val().split('/');
    var joining_date1 =Joining_Date[2]+'-'+Joining_Date[1]+'-'+Joining_Date[0]
    var MasterData = {
        "p_DwrJoiningId": dwrid,
        "p_Position": jQuery(`#position${i}`).val()==null? 0 :jQuery(`#position${i}`).val(),
        "p_candidate_name": jQuery(`#candidatename${i}`).val()==null? 0 :jQuery(`#candidatename${i}`).val(),
        "p_reg_no": jQuery(`#txtRegistrationNumber${i}`).val()==null? 0 :jQuery(`#txtRegistrationNumber${i}`).val(),
        "p_company_name": $(`#companyname${i}`).val()==null? 0 :jQuery(`#companyname${i}`).val(),
        "p_sub_company_name": $(`#subcompanyname${i}`).val()==null? 0 :jQuery(`#subcompanyname${i}`).val(),
        "p_con_per_name": $(`#companyHRname${i}`).val()==null? 0 :jQuery(`#companyHRname${i}`).val(),
        "p_con_email": $(`#companyHREmail${i}`).val()==null? 0 :jQuery(`#companyHREmail${i}`).val(),
        "p_con_number": $(`#companyHRnumber${i}`).val()==null? 0 :jQuery(`#companyHRnumber${i}`).val(),
        "p_joining_date": joining_date1,
        "p_ctc": jQuery(`#txtYearlyCTCOffered${i}`).val()==null? 0 :jQuery(`#txtYearlyCTCOffered${i}`).val(),
        "p_job_location": jQuery(`#txtJobLocation${i}`).val()==null? 0 :jQuery(`#txtJobLocation${i}`).val(),
        "p_candidate_address": jQuery(`#txtAddress${i}`).val()==null? 0 :jQuery(`#txtAddress${i}`).val(),
        "p_category_id":  jQuery(`#ddlCategory${i}`).val()==null? 0 :jQuery(`#ddlCategory${i}`).val(),
        "p_candidate_district": $(`#ddlDistrict${i}`).val()==null? 0 :jQuery(`#ddlDistrict${i}`).val(),
        "p_mobile_number": $(`#txtMobile${i}`).val()==null? 0 :jQuery(`#txtMobile${i}`).val(),
        "p_unique_id_no": jQuery(`#txtUniqueIDNo${i}`).val()==null? 0 :jQuery(`#txtUniqueIDNo${i}`).val(),
        "p_Entry_By":sessionStorage.Emp_Id,
        "p_Entered_Ip":sessionStorage.ipAddress,
        "p_Entry_date":sessionStorage.datenow,
        "p_NotRegisteredComp":$('#newemptxt').val()      
        

    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "joiningentry";
    securedajaxpost(path, 'parsrdataitmenu', 'comment', MasterData, 'control')    


}


function parsrdataitmenu(data) {
    data = JSON.parse(data)
   // console.log(data)
  //  fetchDWRCount();
  
  //sessionStorage.DwrJoiningId=data[0][0].DwrJoiningId;
  if(data[0][0].ReturnValue=='1'){
     // sessionStorage.DwrJoiningId=data[0][0].DwrJoiningId;
      if(sessionStorage.IsPosition=='Y'){
        InsupdDesignationDetail(data[0][0].DwrJoiningId)
      }
      $('#newemptxt').val('')
    toastr.success('Insert Successfully', "", "success");

}
    if(data[0][0].ReturnValue=='2'){
        if(sessionStorage.IsPosition=='Y'){
            InsupdDesignationDetail(data[0][0].DwrJoiningId)
          }
          $('#newemptxt').val('')
        toastr.success('Update Successfully', "", "success");
    }
    if(data[0][0].ReturnValue=='3'){
        $('#emp_name').html(data[0][0].EmpName);
        $('#m_modal_7').toggle('modal');
      }
}


    
   



function fetchDWRbydate(){
    var date= sessionStorage.Date.split('/')
    var Date=date[2]+'-'+date[1]+'-'+date[0] 
    var path =  serverpath + "joiningreport/"+sessionStorage.ManagerId+"/"+Date+"";
    ajaxget(path,'parsedatafetchDWRbydate','comment',"control");
}
function parsedatafetchDWRbydate(data){
data = JSON.parse(data)
var appenddata='';
var tablehead=`<th style="border-right: inset;"> Sno. </th>

<th style="border-right: inset;"> Registration Number </th>
<th style="border-right: inset;"> Candidate Name </th>
<th style="border-right: inset;">Address</th>
<th style="border-right: inset;">Category</th>
<th style="border-right: inset;">District</th>
<th style="border-right: inset;">Mobile Number</th>
<th style="border-right: inset;">UniqueID Number</th>
<th style="border-right: inset;"> Position</th>
<th style="border-right: inset;"> Company/Client Name </th>
<th style="border-right: inset;"> Sub Company Name </th>
<th style="border-right: inset;"> Company HR Name </th>
 <th style="border-right: inset;"> Company HR Number  </th>
<th style="border-right: inset;"> Company HR Email</th>
<th style="border-right: inset;"> Date of Joining</th>
<th style="border-right: inset;">Yearly CTC Offered</th>
<th style="border-right: inset;">Job Location</th>

<th style="border-right: inset;">Edit</th>
<th>Save</th>
`;
$('#tablehead').html(tablehead);
for (var i = 0; i < data[0].length; i++) {

    appenddata +='<tr><td>'+[i+1]+'</td>'+
    '<td><input type="text" class="txtRegistrationNumber rpt" id="txtRegistrationNumber'+i+'"  onkeypress="if (this.value.length >= 14){return false;}" value="'+data[0][i].reg_no+'"></td>'+
    '<td><input type="text"  class="candidatename rpt" id="candidatename'+i+'" value="'+data[0][i].candidate_name+'" disabled></td>'+
    '<td><input type="text" id="txtAddress'+i+'" class="txtAddress rpt" value="'+data[0][i].candidate_address+'" disabled></td>'+
    '<td><select type="text" name="name" id="ddlCategory'+i+'" class="form-control ddlCategory rpt" style="height: 32px; width: 196px;" value="'+data[0][i].category_id+'" disabled></select></td>'+
    '<td><select type="text" name="name" id="ddlDistrict'+i+'" class="form-control ddlDistrict rpt" style="height: 32px; width: 196px;" value="'+data[0][i].candidate_district+'" disabled></select></td>'+
    '<td><input type="text" id="txtMobile'+i+'" class="txtMobile rpt"  onkeypress="if (this.value.length >= 10){return false;}" value="'+data[0][i].mobile_number+'" disabled></td>'+
    '<td><input type="text" id="txtUniqueIDNo'+i+'" class="txtUniqueIDNo rpt" value="'+data[0][i].unique_id_no+'"></td>'+
    '<td><select type="text" name="name" class="form-control position rpt"  style="height: 32px; width: 196px;" id="position'+i+'" value="'+data[0][i].Position+'"></select></td>'+
    '<td><select class="companyname rpt form-control" id="companyname'+i+'" onchange=handleExchChange('+i+')></select></td>'+
    '<td><select class="subcompanyname rpt form-control" id="subcompanyname'+i+'" style="width:200px;" disabled></select></td>'+
    '<td><input type="text" class="companyHRname rpt" id="companyHRname'+i+'" value="'+data[0][i].con_per_name+'"></td>'+
    '<td><input type="number" class="companyHRnumber rpt" id="companyHRnumber'+i+'" onkeypress="if (this.value.length >= 10){return false;}" value="'+data[0][i].con_number+'"></td>'+
    '<td><input type="text" class="companyHREmail rpt" id="companyHREmail'+i+'" value="'+data[0][i].con_email+'"></td>'+
    '<td><input class="datepicker rpt" id="joiningdate'+i+'" style="height: 32px; width: 196px;" value="'+data[0][i].joining_date+'"></td>'+
    '<td><input type="text" id="txtYearlyCTCOffered'+i+'" class="txtYearlyCTCOffered rpt" value="'+data[0][i].ctc+'"></td>'+
    '<td><input type="text" id="txtJobLocation'+i+'" class="txtJobLocation rpt" value="'+data[0][i].job_location+'"></td>'+
     '<td><button type="button" onclick=$(".rpt").attr("disabled",false);handleExchChangerpt('+i+')  id="btnSubmit'+i+'" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">Edit</button></td>'+
    '<td><button type="button" onclick=checkValidation('+i+','+data[0][i].DwrJoiningId+'); id="Submit'+i+'" class="btn btn-success rpt" style="background-color:#716aca;border-color:#716aca;">Save</button></td>'+
    '</tr>';
  
 
}
$('#tablebody').html(appenddata);
$('.rpt').attr( "disabled",true);
$('.datepicker').datepicker({
    format: 'dd/mm/yyyy'});
    FillPositionrpt(data);
    FillCategoryssrpt(data);
    FillDistrictssrpt(data); 
    FillEmployerrpt(data);
    FillSubcompanyrpt(data)
sessionStorage.removeItem('page')
}



function FillPositionrpt(control) {
    var path = serverpath + "adminDesignation/0/0";
    securedajaxget(path, 'parsedataPositionrpt', 'comment', control)
}

function parsedataPositionrpt(data,control) {
    data = JSON.parse(data)
    jQuery(`.position`).empty();
    var data1 = data[0];
    jQuery(`.position`).append(jQuery("<option></option>").val('0').html("Select"));
    for (var i = 0; i < data1.length; i++) {
        jQuery(`.position`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
    }
    for (var i = 0; i < control[0].length; i++) {
        jQuery(`#position${i}`).val(control[0][i].Position);
    }
    jQuery(`#position${i}`).append(jQuery("<option></option>").val('987654').html('Other'));
}

function FillEmployerrpt(control) {
    var path = serverpath + "PlacementEmployer";
    securedajaxget(path, 'parsedataEmployerrpt', 'comment', control)
}

function parsedataEmployerrpt(data,control) {
    data = JSON.parse(data)
    jQuery(`.companyname`).empty();
    var data1 = data[0];
    jQuery(`.companyname`).append(jQuery("<option></option>").val('0').html("Select"));
    for (var i = 0; i < data1.length; i++) {
        jQuery(`.companyname`).append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
    }
    jQuery(`.companyname`).append(jQuery("<option></option>").val('111987654').html('Other'));

    for (var i = 0; i < control[0].length; i++) {
        jQuery(`#companyname${i}`).val(control[0][i].company_name);
    }
}



function FillCategoryssrpt(control) {
    var path =  serverpath + "category/0/0/0/0"
    securedajaxget(path,'parsedatasecuredFillCategoryssrpt','comment',control);
  }
  
  function parsedatasecuredFillCategoryssrpt(data,control){  
    data = JSON.parse(data)
            jQuery(`.ddlCategory`).empty();
            var data1 = data[0];
            jQuery(`.ddlCategory`).append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(`.ddlCategory`).append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
             }
             for (var i = 0; i < control[0].length; i++) {
                jQuery(`#ddlCategory${i}`).val(control[0][i].category_id);
            }
        }
              
 
  function FillDistrictssrpt(control) {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,'parsedatasecuredFillDistrictssrpt','comment',control);
}

function parsedatasecuredFillDistrictssrpt(data,control){  
    data = JSON.parse(data)
            jQuery(`.ddlDistrict`).empty();
            var data1 = data[0];
            jQuery(".ddlDistrict").append(jQuery("<option></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(`.ddlDistrict`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             for (var i = 0; i < control[0].length; i++) {
                jQuery(`#ddlDistrict${i}`).val(control[0][i].candidate_district);
            }
        }
              
        function checkValidation(i,dwrid){
            if($(`#position${i}`).val()=='0'){
            
            toastr.warning('Please Enter Position')
            return false;
            }
            
            
            if($(`#candidatename${i}`).val()==''){
            
            toastr.warning('Please Enter Candidate Name')
            return false;
            }
            if($(`#txtRegistrationNumber${i}`).val()==''){
            
            toastr.warning('Please Enter Registration Number')
            return false;
            }
            if($(`#companyname${i}`).val()==''){
            
            toastr.warning('Please Enter CompanyName')
            return false;
            }
            if($(`#companyHRname${i}`).val()==''){
            
            toastr.warning('Please Enter HR Name')
            return false;
            }
            if($(`#companyHREmail${i}`).val()==''){
            
            toastr.warning('Please Enter HR Email')
            return false;
            }
            if($(`#companyHRnumber${i}`).val()==''){
            
            toastr.warning('Please Enter HR Number')
            return false;
            }
            if($(`#joiningdate${i}`).val()==''){
            
            toastr.warning('Please Enter Date')
            return false;
            }
            if($(`#txtYearlyCTCOffered${i}`).val()==''){
            
            toastr.warning('Please Enter CTC')
            return false;
            }
            if($(`#txtJobLocation${i}`).val()==''){
            
            toastr.warning('Please Enter Job Location')
            return false;
            }
            if($(`#txtAddress${i}`).val()==''){
            
            toastr.warning('Please Enter Address')
            return false;
            }
            if($(`#ddlCategory${i}`).val()=='0'){
            
            toastr.warning('Please Select Category')
            return false;
            }
            if($(`#ddlDistrict${i}`).val()=='0'){
            
            toastr.warning('Please select District')
            return false;
            }
            if($(`#txtMobile${i}`).val()==''){
            
            toastr.warning('Please Enter Mobile Number')
            return false;
            }
            if($(`#txtUniqueIDNo${i}`).val()==''){
            
            toastr.warning('Please Enter UniqueID Number')
            return false;
            }
            else{
            InsupdJoiningDetail(i,dwrid)
            }
            }
            function FillEmployer(x) {
                jQuery.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: serverpath + "PlacementEmployer",
                    cache: false,
                    dataType: "json",
                    success: function (data) {
                       
                jQuery(`#companyname${x}`).empty();
                var data1 = data[0];
            
            
                jQuery(`#companyname${x}`).append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));
            
                for (var i = 0; i < data1.length; i++) {
                    jQuery(`#companyname${x}`).append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
                
                }
                jQuery(`#companyname${x}`).append(jQuery("<option></option>").val('111987654').html('Other'));

                
                    },
                    error: function (xhr) {
                        toastr.success(xhr.d, "", "error")
                        return true;
                    }
                });
            }

            function CheckUser(control) {
                sessionStorage.regcontrol=control;
                var regno=`#txtRegistrationNumber${control}`
                if ($(regno).val() != "") {
                    var path = serverpath + `Jobseekerinfo/'${$(regno).val()}'`;
                    ajaxget(path, 'parsedatacheckuser', 'comment', control)
                }
            
            }
            
            function parsedatacheckuser(data) {
                data = JSON.parse(data)
            
            
            
                var data1 = data[0];
                if (data1[0].ReturnValue == '1') {
                  toastr.warning("Invalid Registration Number");
                  $(`'#txtRegistrationNumber${control}'`).val('')
                  return false;
                } 
                else {
var control= sessionStorage.regcontrol
if (data1[0].CandidateName != null && data1[0].CandidateName != '') {
                    jQuery(`#candidatename${control}`).val(data1[0].CandidateName)
}
else{
    jQuery(`#candidatename${control}`).attr('disabled',false);
}
if (data1[0].PermanentAddress && data1[0].PermanentAddress !== null && data1[0].PermanentAddress != 'null' && data1[0].PermanentAddress != '') {
                        $(`#txtAddress${control}`).val(data1[0].PermanentAddress)
}
else{
    $(`#txtAddress${control}`).attr('disabled',false);
}
   
if (data1[0].MobileNumber != null && data1[0].MobileNumber != '') {
$(`#txtMobile${control}`).val(data1[0].MobileNumber)
}
else{
    $(`#txtMobile${control}`).attr('disabled',false); 
}
if (data1[0].DistrictId != null && data1[0].DistrictId != '0') {
                    $(`#ddlDistrict${control}`).val(data1[0].DistrictId)
}
else{
    $(`#ddlDistrict${control}`).attr('disabled',false);  
}

                    if (data1[0].Category != null && data1[0].Category != '0') {
                    $(`#ddlCategory${control}`).val(data1[0].Category)
                    }
                    else{
                        $(`#ddlCategory${control}`).attr('disabled',false);  
                    }
                    if (data1[0].IdentificationNumber != null && data1[0].IdentificationNumber != '') {
                    $(`#txtUniqueIDNo${control}`).val(data1[0].IdentificationNumber)
                    }
                    else{
                        $(`#txtUniqueIDNo${control}`).attr('disabled',false);
                    }
                    if (data1[0].DesignationId != null ) {
                        $(`#position0${control}`).val(data1[0].DesignationId)
                    }
                    else{
                        $(`#position0${control}`).attr('disabled',false);
                    }
                }
            }
            function handleChangePosition(i){
               // console.log('i',i);
                if($(i).val()=='987654'){
                    $('#m_modal_6').toggle('modal');
                }
            }
            
            function InsupdDesignationDetail(DwrJoiningId){
             
            var MasterData ={
                "p_PositionName":sessionStorage.PositionName,
                "p_EntryBy":sessionStorage.Emp_Id,
                "p_Dwr_Id":DwrJoiningId,
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "positionentry";
            securedajaxpost(path, 'parsrdataitInsupdDesignationDetail', 'comment', MasterData, 'control')
            
        }
            
            
            function parsrdataitInsupdDesignationDetail(data) {
                data = JSON.parse(data)
                if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        InsupdDesignationDetail();
                }
                else if (data[0][0].ReturnValue == "1") {
                    sessionStorage.removeItem('IsPosition');
                    sessionStorage.removeItem('PositionName');
                    //$('#m_modal_6').toggle('modal');
                    // toastr.success("Insert Successful", "", "success")
                        return true;
                }
               
                else if (data[0][0].ReturnValue == "0") {
                    sessionStorage.removeItem('IsPosition');
                    sessionStorage.removeItem('PositionName');
                    //toastr.warning("Already exist", "", "info")
                        return true;
                }
             
            }
            function handlePositionSubmit(){
                if($('#positiontxt').val()==''){
                    toastr.warning('Please Enter Position');
                    return false;
                }
                else{

                    sessionStorage.IsPosition='Y';
                    sessionStorage.PositionName=$('#positiontxt').val();
                    $('#m_modal_6').toggle('modal');
                }
            }

       

            function handleExchChange(x){
                
                    if(jQuery(`#companyname${x}`).val() == '00005202000034')
                    {
                        jQuery(`#subcompanyname${x}`).attr('disabled',false);
                        FillSubcompany(x)
                    }
                  else  if($(`#companyname${x}`).val()=='111987654'){
                        $('#m_modal_5').toggle('modal');
                    }
                    else
                    {
                        jQuery(`#subcompanyname${x}`).attr('disabled',true);
                        jQuery(`#subcompanyname${x}`).empty();
                        jQuery(`#subcompanyname${x}`).append(jQuery("<option></option>").val('0').html("Select Sub Company Name"));
                    
                    }
                   
                
                }

                function handleExchChangerpt(x){
                
                    if(jQuery(`#companyname${x}`).val() == '00005202000034')
                    {
                        jQuery(`#subcompanyname${x}`).attr('disabled',false);
                        
                    }
                    else
                    {
                        jQuery(`#subcompanyname${x}`).attr('disabled',true);
                     
                    }
                   
                
                }
     

                    function FillSubcompany(x) {
                        jQuery.ajax({
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            url: serverpath + "dwrsubcompany/0/'Y'",
                            cache: false,
                            dataType: "json",
                            success: function (data) {
                               
                        jQuery(`#subcompanyname${x}`).empty();
                        var data1 = data[0];
                    
                    
                        jQuery(`#subcompanyname${x}`).append(jQuery("<option></option>").val('0').html("Select Sub Company Name"));
                    
                        for (var i = 0; i < data1.length; i++) {
                            jQuery(`#subcompanyname${x}`).append(jQuery("<option></option>").val(data1[i].sub_client_id).html(data1[i].sub_client_name));
                        
                        }
                        
                            },
                            error: function (xhr) {
                                toastr.success(xhr.d, "", "error")
                                return true;
                            }
                        });
                    }

                    
                    function FillSubcompanyrpt(control) {
                        jQuery.ajax({
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            url: serverpath + "dwrsubcompany/0/'Y'",
                            cache: false,
                            dataType: "json",
                            success: function (data) {
                               
                        jQuery(`.subcompanyname`).empty();
                        var data1 = data[0];
                    
                    
                        jQuery(`.subcompanyname`).append(jQuery("<option></option>").val('0').html("Select Sub Company Name"));
                    
                        for (var i = 0; i < data1.length; i++) {
                            jQuery(`.subcompanyname`).append(jQuery("<option></option>").val(data1[i].sub_client_id).html(data1[i].sub_client_name));
                        
                        }
                        for (var i = 0; i < control[0].length; i++) {
                            if(control[0][i].company_name=='00005202000034'){
                                jQuery(`#subcompanyname${i}`).val(control[0][i].sub_client_id);
                            }
                           
                            
                        }
                        
                            },
                            error: function (xhr) {
                                toastr.success(xhr.d, "", "error")
                                return true;
                            }
                        });
                    }
              
                    