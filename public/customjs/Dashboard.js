function Counter(){
    var path = serverpath + "counter"
    ajaxget(path,'parsedatacounter','comment','control');
}
function parsedatacounter(data,control){
    data = JSON.parse(data)
    $("#jobseeker").html(data[0][0].JobSeekers);
    $("#employers").html(data[0][0].Employers);
   $("#Vacancies").html(data[0][0].Vacancies);
    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}

function Dashboard_JobseekerRecord() {
    var path = serverpath + "olexdashboard"
    ajaxget(path, 'parsedataDashboard_JobseekerRecord', 'comment', "control");
}
function parsedataDashboard_JobseekerRecord(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var data2=data[1];
    var data3=data[2];
    var appenddata = "";
    var appenddata1 = "";
    var appenddata2 = "";

    for (var i = 0; i < data1.length; i++) {

       
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Qualif_Level_name +"</td><td>"+ data1[i].Male+"</td><td>"+data1[i].Female+"</td></tr>";
    } 
    for (var i = 0; i < data2.length; i++) {

       
        appenddata1 += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data2[i].CategoryName +"</td><td>"+ data2[i].Male+"</td><td>"+data2[i].Female+"</td></tr>";
    }
    for (var i = 0; i < data3.length; i++) {

       
        appenddata2 += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data3[i].AgeGroup +"</td><td>"+ data3[i].Male+"</td><td>"+data3[i].Female+"</td></tr>";
    }
    jQuery("#tbodyvalue").html(appenddata);
    jQuery("#tbodyvalue2").html(appenddata1);
    jQuery("#tbodyvalue3").html(appenddata2);
    $('#jobseekerRecord').show();
    $('#employerRecord').hide();
}




function Dashboard_EmployerRecord() {
    var path = serverpath + "olexdashboardemp"
    ajaxget(path, 'parsedataDashboard_EmployerRecord', 'comment', "control");
}
function parsedataDashboard_EmployerRecord(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var data2=data[1];
    var data3=data[2];
    var appenddata = "";
    var appenddata1 = "";
    var appenddata2 = "";

    for (var i = 0; i < data1.length; i++) {

       
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Des_cription +"</td><td>"+ data1[i].Count+"</td></tr>";
    } 
    for (var i = 0; i < data2.length; i++) {

       
        appenddata1 += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data2[i].Description +"</td><td>"+ data2[i].Count+"</td></tr>";
    }
    for (var i = 0; i < data3.length; i++) {

       
        appenddata2 += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data3[i].Company_type +"</td><td>"+ data3[i].Count+"</td></tr>";
    }
    jQuery("#tbodyvalueemp1").html(appenddata);
    jQuery("#tbodyvalueemp2").html(appenddata1);
    jQuery("#tbodyvalueemp3").html(appenddata2);
    $('#employerRecord').show();
    $('#jobseekerRecord').hide();
}

