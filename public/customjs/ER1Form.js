function FillCCMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCCMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCCMonth('parsedatasecuredFillCCMonth',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}
function FillYear(funct,control) {
  var path =  serverpath + "secured/year/0/10"
  securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillYear(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillYear('parsedatasecuredFillYear','yearddl');
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
     else if(data.errno) {
          toastr.warning("Something went wrong Please try again later", "", "info")
          return false;
      }
  
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];
          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
           }
      }
            
}
function FillMonthER1(funct,control) {
  var path =  serverpath + "month/0"
  securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthER1(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillMonthER1('parsedatasecuredFillMonthER1',control);
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];

          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
          for (var i = 0; i < data1.length; i++) {
            if(data1[i].FullMonthName=='March' ||data1[i].FullMonthName=='June' ||data1[i].FullMonthName=='September' ||data1[i].FullMonthName=='December' ){
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FullMonthName).html(data1[i].FullMonthName));
         
            }
           }

          
      }
            
}

function FetchER1Form() {
  if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
    var entryfrom='Olex';
  }
  else{
    var entryfrom='yashaswi';
  }
  var path =  serverpath + "geter1formbyEmp/"+$('#yearddl').val()+"/"+$('#monthddl').val()+"/0/"+entryfrom+"/"+sessionStorage.CandidateId+""
      
   ajaxget(path,'parsedatasFetchER1Form','comment','control');
}

function parsedatasFetchER1Form(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchER1Form();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            var appenddata='';
            for (var i = 0; i < data1.length; i++) {
        appenddata+=`<tr class='i'><td>`+[i+1]+`</td><td>`+data1[i].Name+`</td><td>`+data1[i].Address+`</td><td>`+data1[i].OfficeType+`</td><td>`+data1[i].Date+`</td><td><button type="button" onclick="viewsreport(`+data1[i].EmpId+`)"  class="btn btn-success" style="background-color:#716aca;border-color:#716aca;"> View Report</button></td></tr>`;
            }
            $('#tbodyvalue').append(appenddata);
            $('#tbodyvalue').clientSidePagination();
        }
        sessionStorage.removeItem('back')        
}

function viewsreport(empid){
    sessionStorage.setItem('empid',empid)
    window.location='/ER1FormDetails'
}

    
    function FetchER1Report() {
      if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
        var entryfrom='Olex';
      }
      else{
        var entryfrom='yashaswi';
      }
    
        var path =  serverpath + "er1_form/"+sessionStorage.empid+"/2020-12-12/2020-12-12/"+entryfrom+"/"+sessionStorage.CandidateId+""
       ajaxget(path,'parsedatasFetchER1Report','comment','control');
    }
    
    function parsedatasFetchER1Report(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchER1Report();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];

$('#empName').val(data1[0].Name);
$('#empaddress').val(data1[0].Address);
$('#office').val(data1[0].OfficeType);
$('#natureofbus').val(data1[0].BuisnessNature);
$('#hrname').val(data1[0].HR_name);
$('#contactno').val(data1[0].Contactnumber);
$('#email').val(data1[0].EmailId);
$('#Month').val(data1[0].Month);
$('#men1').val(data1[0].PreQuaterMen);
$('#men2').val(data1[0].UndQuaterMen);
$('#Women1').val(data1[0].PreQuaterWomen);
$('#Women2').val(data1[0].UndQuaterWomen);
$('#Total1').val(data1[0].PreQuaterTotal);
$('#Total2').val(data1[0].UndQuaterTotal);
$('#b1').val(data1[0].IncDecQuater);
 $('#2b').val(data1[0].Reason);
// $('#3point').val(data1[0]);
$('#date').val(data1[0].Date);
$('#Signature').val(data1[0].Signature);
$('#Place').val(data1[0].Place);
         
            }
                  
    }







    function FetchER1Reporfilledt() {

        var path =  serverpath + "er1_vacancy_filled/"+sessionStorage.empid+""
       ajaxget(path,'parsedatasFetchER1Reporfilledt','comment','control');
    }
    
    function parsedatasFetchER1Reporfilledt(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchER1Reporfilledt();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];


                var appenddata='';
               
                for (var i = 0; i < data1.length; i++) {
            appenddata+=`<tr class="i">
            <td>
              <input type="text" id="occured0" class="form-control" placeholder="" value='`+data1[i].Occurred+`' disabled>
            </td>
            <td style="    width: 18%;">
              <input type="text" id="localemp0" class="form-control" placeholder="" value='`+data1[i].Local+`' disabled>
          
            </td>
            <td>
              <input type="text" id="centralemp0" class="form-control" placeholder="" value='`+data1[i].Central+`' disabled>
            </td>
            <td>
              <input type="text" id="filled0" class="form-control" placeholder="" value='`+data1[i].Filled+`' disabled>
            </td>
            <td>
              <input type="text" id="source0" class="form-control" placeholder="" value='`+data1[i].Source+`' disabled>
            </td>
          
            
          </tr>`;

                }
                $('#table1').append(appenddata);
            }
                  
    }



    function FetchER1Reportunfilled() {

        var path =  serverpath + "er1_vacancy_unfilled/"+sessionStorage.empid+""
       ajaxget(path,'parsedatasFetchER1Reportunfilled','comment','control');
    }
    
    function parsedatasFetchER1Reportunfilled(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchER1Reportunfilled();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];

                 var appenddata='';
               
                for (var i = 0; i < data1.length; i++) {
           
                    appenddata+=`     <tr class="addrowunfilled i">
                    <td>
                      <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].Designation+`' disabled>
                    </td>
                    <td style="    width: 18%;">
                      <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].Qualification+`' disabled>
                  
                    </td>
                    
                    <td>
                      <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].Experience+`' disabled>
                    </td>
                    <td>
                      <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].ExperienceNot+`' disabled>
                    </td>
                  
                  </tr>`;

                }
                $('#table2').append(appenddata);
            }
                  
    }
    

    function FetchER1Reportyashaswi() {

        var path =  serverpath + "er1_vacancy_yashaswi/"+sessionStorage.empid+""
       ajaxget(path,'parsedatasFetchER1Reportyashaswi','comment','control');
    }
    
    function parsedatasFetchER1Reportyashaswi(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchER1Reportyashaswi();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];


                var appenddata='';
               
                for (var i = 0; i < data1.length; i++) {
          
                    appenddata+=`<tr class="addproductrow i">
                                
          <td style="    width: 18%;">
            <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].Position+`' disabled>
        
          </td>
          <td>
            <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].JobDescription+`' disabled>
          </td>
          <td>
            <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].Salary+`' disabled>
          </td>
          <td>
            <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].NoOfReq+`' disabled>
          </td>
          <td>
            <input type="text" id="" class="form-control" placeholder="" value='`+data1[i].JobLocation+`' disabled>
          </td>
         
        </tr>`;
                }
                $('#table3').append(appenddata);
            }
                  
    }
    

 
    