function FillEmployerByExchange(){

    var path = serverpath + "EmpByExchange/5";
ajaxget(path, 'parsedataEmployerByExchange', 'comment', 'control')
}
function parsedataEmployerByExchange(data){  
data = JSON.parse(data)



jQuery("#ddlEmployer").empty();
var data1 = data[0];


jQuery("#ddlEmployer").append(jQuery("<option></option>").val('0').html("Select Employer"));

for (var i = 0; i < data1.length; i++) {
jQuery("#ddlEmployer").append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
} 
      
}

function fetchJobs(){
    
    var path =  serverpath + "EmployerWithJobCount/"+$('#ddlEmployer').val()+"/1";
    ajaxget(path,'parsedatafetchjobs','comment',"control");
}
function parsedatafetchjobs(data){  
    data = JSON.parse(data)
   var appenddata="";
   jQuery("#tbodyvaluemodal").empty();
    var data1 = data[0];  
    var cacheArr = [];  
            for (var i = 0; i < data1.length; i++) {
                if (cacheArr.indexOf( data1[i].Postid) >= 0) {
                    continue;
                }
                cacheArr.push( data1[i].Postid);
              
                appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Postid + "</td><td>"+data1[i].Designation+"</td><td> "+data1[i].Posted+" Ago</td><td><button type='button'   class='btn btn-success' onclick=sessionStorage.setItem('button','cancel');UpdJobStatus('"+data1[i].Postid+"') style='background-color:#716aca;border-color:#716aca;'>Cancel</button></td></tr>";
            } jQuery("#tbodyvalue").html(appenddata);
      
              
}

function UpdJobStatus(Id) {
    sessionStorage.setItem("JobId",Id);
    var MasterData = {
        "p_Postid":sessionStorage.getItem("JobId"),
        "p_Adt_YN":'4',
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "UpdJobStatus";
    securedajaxpost(path, 'parsedatasecuredUpdJobStatus', 'comment', MasterData, 'control')
}

function parsedatasecuredUpdJobStatus(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        UpdJobStatus();
         } 
         else{
            fetchJobs();
         }
    
}