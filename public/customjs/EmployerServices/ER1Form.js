function addNewProductRow() {
    var x = $('.addproductrow').length;

    var productrow = `<tr class="addproductrow" id='productrow`+ x+`'>
 
  <td style='    width: 18%;'>
  
  <input type="text" id="position`+x+`" class="form-control" placeholder="" />
  </td>
  </td>
  <td>
    <input type="text" id="jobdescription`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="text" id="salary`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="text" id="noofrequirement`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="text" id="joblocation`+x+`" class="form-control" placeholder="" />
  </td>
  <td>
    <input type="submit"  class="btn" value="Remove" onclick="$('#productrow`+ x+`').remove();  "  />
  </td>
</tr>`;
    $('.vacancydetail').append(productrow);
}
function insER1form() {
  if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
    var entryfrom='Olex';
  }
  else{
    var entryfrom='yashaswi';
  }


var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var MasterData = {
      "p_Name":$('#companyname option:selected').text(),
      "p_Address":$('#address').val(),
      "p_OfficeType":$('#officetype').val(),
      "p_PreQuaterMen":$('#prequartermen').val(),
      "p_PreQuaterWomen":$('#PreQuaterWomen').val(),
      "p_PreQuaterTotal":$('#PreQuaterTotal').val(),
      "p_UndQuaterMen":$('#UndQuaterMen').val(),
      "p_UndQuaterWomen":$('#UndQuaterWomen').val(),
      "p_UndQuaterTotal":$('#UndQuaterTotal').val(),
      "p_IncDecQuater":$('#IncDecQuater').val(),
      "p_Reason":$('#Reason').val(),
      "p_Date":$('#m_datepicker_5').val(),
      "p_Signature":$('#Signature').val(),
      "p_Place":$('#Place').val(),
      "p_EnteredOn":date,
      "p_EnteredIP":sessionStorage.ipAddress,
      "p_BuisnessNature":$('#BuisnessNature').val(),
      "p_HR_name":$('#HR_name').val(),
      "p_Contactnumber":$('#Contactnumber').val(),
      "p_EmailId":$('#EmailId').val(),
      "p_Month":$('#Month').val(),
      "p_Year":$('#yearddl').val(),
      "p_EmployerId":$('#companyname').val(),
      "p_EntryFrom":entryfrom,
      "p_EntryBy":sessionStorage.CandidateId
     
      }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "er1form";
  securedajaxpost(path, 'parsrdataer1form', 'comment', MasterData, 'control')
}
function parsrdataer1form(data) {
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      InsUpdCounsellingform();
  }
     else if(data.errno) {
          toastr.warning("Something went wrong Please try again later", "", "info")
          return false;
      }
  
  else if (data[0][0].EmpId) {
    sessionStorage.EmpId=data[0][0].EmpId
    Insvacancydetail(data[0][0].EmpId)

    Insvacancydetailunfilled() 
    Insvacancydetailyashaswi(data[0][0].EmpId)
  }

 

}


function addNewfilledvacancyRow() {
  var x = $('.addrowvacancydetail').length;
  var productrow = `<tr class="addrowvacancydetail" id='filledvacancy`+ x+`'>

<td style='    width: 18%;'>

<input type="text" id="occured`+x+`" class="form-control" placeholder="" />
</td>
</td>
<td>
  <input type="text" id="localemp`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="text" id="centralemp`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="text" id="filled`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="text" id="source`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="submit"  class="btn" value="Remove" onclick="$('#filledvacancy`+ x+`').remove();  "  />
</td>
</tr>`;
  $('.vacancydetailfilled').append(productrow);
}
function Insvacancydetail(id) {


  
  $('.addrowvacancydetail').each(function (i) {
    if($('#occured'+i+'').val()!='' && $('#localemp'+i+'').val()!=''&& $('#centralemp'+i+'').val()!=''&& $('#filled'+i+'').val()!=''&& $('#source'+i+'').val()!='')
    {
  var MasterData = {
      
          "p_EmpId":sessionStorage.EmpId,
          "p_Occurred":$('#occured'+i+'').val(),
          "p_Local":$('#localemp'+i+'').val(),
          "p_Central":$('#centralemp'+i+'').val(),
          "p_Filled":$('#filled'+i+'').val(),
          "p_Source":$('#source'+i+'').val()
          
      
      
      }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "er1formvacancy";
  securedajaxpost(path, 'parsrdataInsvacancydetail', 'comment', MasterData, 'control')
    }
});
}
function parsrdataInsvacancydetail(data) {
  data = JSON.parse(data)

  console.log(data)
 
  

 

}

function addNewunfilled() {
  var x = $('.addrowunfilled').length;
  var productrow = `<tr class="addrowunfilled" id='unfilledvacancy`+ x+`'>

<td style='    width: 18%;'>

<input type="text" id="occupation`+x+`" class="form-control" placeholder="" />
</td>
</td>
<td>
  <input type="text" id="qualification`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="text" id="experience`+x+`" class="form-control" placeholder="" />
</td>
<td>
  <input type="text" id="experiencenot`+x+`" class="form-control" placeholder="" />
</td>

<td>
  <input type="submit"  class="btn" value="Remove" onclick="$('#unfilledvacancy`+ x+`').remove();  "  />
</td>
</tr>`;
  $('.vacancydetailunfilled').append(productrow);
}
function Insvacancydetailunfilled(id) {


 
  $('.addrowunfilled').each(function (i) {
    if($('#occupation'+i+'').val()!='' && $('#qualification'+i+'').val()!=''&& $('#experience'+i+'').val()!=''&& $('#experiencenot'+i+'').val()!='')
    {
  var MasterData = {
      
          "p_EmpId":sessionStorage.EmpId,
          "p_Designation":$('#occupation'+i+'').val(),
          "p_Qualification":$('#qualification'+i+'').val(),
          "p_Experience":$('#experience'+i+'').val(),
          "p_ExperienceNot":$('#experiencenot'+i+'').val(),
         
          
      
      
      }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "er1formunfilled";
  securedajaxpost(path, 'parsrdataInsvacancydetailunfilled', 'comment', MasterData, 'control')
    }
});
}
function parsrdataInsvacancydetailunfilled(data) {
  data = JSON.parse(data)

  console.log(data)


 

}
function Insvacancydetailyashaswi(id) {


 
  $('.addproductrow').each(function (i) {
    if($('#position'+i+'').val()!='' && $('#jobdescription'+i+'').val()!=''&& $('#salary'+i+'').val()!=''&& $('#noofrequirement'+i+'').val()!=''&& $('#joblocation'+i+'').val()!='')
  {
    var MasterData = {
      
          "p_EmpId":id,
          "p_Position":$('#position'+i+'').val(),
          "p_JobDescription":$('#jobdescription'+i+'').val(),
          "p_Salary":$('#salary'+i+'').val(),
          "p_NoOfReq":$('#noofrequirement'+i+'').val(),
          "p_JobLocation":$('#joblocation'+i+'').val(),
          
      
      
      }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "er1formvacancyyashaswi";
  securedajaxpost(path, 'parsrdataInsvacancydetailyashaswi', 'comment', MasterData, 'control')
    }
    resetMode();
});

}
function parsrdataInsvacancydetailyashaswi(data) {
  data = JSON.parse(data)

  console.log(data)

toastr.success('Insert Successful')

 

}
function resetMode(){
  sessionStorage.EmpId=0,
  $('#position0').val(''),
      $('#jobdescription0').val(''),
       $('#salary0').val(''),
     $('#noofrequirement0').val(''),
     $('#joblocation0').val(''),
   $('#occured0').val(''),
     $('#localemp0').val(''),
     $('#centralemp0').val(''),
     $('#filled0').val(''),
   $('#source0').val('')
   $('#companyname').val(''),
    $('#address').val(''),
      $('#officetype').val(''),
    $('#prequartermen').val(''),
     $('#PreQuaterWomen').val(''),
    $('#PreQuaterTotal').val(''),
  $('#UndQuaterMen').val(''),
    $('#UndQuaterWomen').val(''),
     $('#UndQuaterTotal').val(''),
    $('#IncDecQuater').val(''),
    $('#Reason').val(''),
     $('#Date').val(''),
    $('#Signature').val(''),
     $('#Place').val(''),
   
$('#BuisnessNature').val(''),
   $('#HR_name').val(''),
 $('#Contactnumber').val(''),
$('#EmailId').val(''),
   $('#Month').val('0'),
   $('#occupation0').val(''),
          $('#qualification0').val(''),
         $('#experience0').val(''),
         $('#yearddl').val('0');
         $('#m_datepicker_5').val('')
        $('#experiencenot0').val(0),
  $('.addproductrow').each(function (i) {

    $('#productrow'+ i+'').remove();  
     
            
        
        
        });
        $('.addrowvacancydetail').each(function (i) {

          $('#filledvacancy'+ i+'').remove();  
           
                  
              
              
              });
              $('.addrowunfilled').each(function (i) {

                $('#unfilledvacancy'+ i+'').remove();  
                 
                        
                    
                    
                    });
}
function FillMonthER1(funct,control) {
  var path =  serverpath + "month/0"
  securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthER1(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillMonthER1('parsedatasecuredFillMonthER1',control);
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];

          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
          for (var i = 0; i < data1.length; i++) {
            if(data1[i].FullMonthName=='March' ||data1[i].FullMonthName=='June' ||data1[i].FullMonthName=='September' ||data1[i].FullMonthName=='December' ){
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FullMonthName).html(data1[i].FullMonthName));
         
            }
           }

          
      }
            
}
function CheckER1(){
  if(isValidation){
      if ($.trim(jQuery('#companyname').val()) == "") {
          getvalidated('companyname','text','Name of the Employer');
          return false;
       }
      if ($.trim(jQuery('#address').val()) == "") {
          getvalidated('address','text','Address of the Employer');
          return false; }
      if ($.trim(jQuery('#officetype').val()) == "0") {
          getvalidated('officetype','text','Office Type');
          return false; }
          if ($.trim(jQuery('#BuisnessNature').val()) == "") {
            getvalidated('BuisnessNature','text','Nature of Business');
            return false;
         }
        if ($.trim(jQuery('#HR_name').val()) == "") {
            getvalidated('HR_name','text','HR Name');
            return false; }
        if ($.trim(jQuery('#Contactnumber').val()) == "") {
            getvalidated('Contactnumber','number','Contact Number');
            return false; }
            if ($.trim(jQuery('#EmailId').val()) == "") {
              getvalidated('EmailId','email','Email Id');
              return false; }
              if ($.trim(jQuery('#Month').val()) == "0") {
                getvalidated('Month','text','Month');
                return false; }
      else{
        insER1form();
      }
  }
  else{
    insER1form();
  } 

}

function getTotalemployement(type){
  if(type=='pre'){
    var premen=0;
    var preqwomen=0; 
    var total=0;
    
    
      premen=$('#prequartermen').val()==''?0:$('#prequartermen').val();
  
      preqwomen=$('#PreQuaterWomen').val()==''?0:$('#PreQuaterWomen').val();
    

      total=parseInt(preqwomen)+parseInt(premen);
      if(total!=0){
        $('#PreQuaterTotal').val(total);
      }
    

  }
  if(type=='und'){
    var undmen=0;
    var undwomen=0; 
    var undtotal=0;
    
    
    undmen=$('#UndQuaterMen').val()==''?0:$('#UndQuaterMen').val();
  
    undwomen=$('#UndQuaterWomen').val()==''?0:$('#UndQuaterWomen').val();
    

    undtotal=parseInt(undmen)+parseInt(undwomen);
      if(undtotal!=0){
        $('#UndQuaterTotal').val(undtotal);
      }
    

  }
  
}

function FillYear(funct,control) {
  var path =  serverpath + "secured/year/0/2"
  securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillYear(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillYear('parsedatasecuredFillYear','yearddl');
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
     else if(data.errno) {
          toastr.warning("Something went wrong Please try again later", "", "info")
          return false;
      }
  
      else{
          jQuery("#"+control).empty();
          var data1 = data[0];
          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
           }
      }
            
}

function fetchInActiveEmp(Emp_Regno,V_type,Type){
  if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
    var path =  serverpath +"EmpRegbyEntryby/Olex"
    ajaxget(path,'parsedatafetchInActiveEmp','comment',"control");
  }
  else{
    var path =  serverpath +"EmpRegbyEntryby/Yashaswi"
    ajaxget(path,'parsedatafetchInActiveEmp','comment',"control");
  }
     
  }
  function parsedatafetchInActiveEmp(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fetchInActiveEmp('0','1','Fetch')
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#companyname").empty();
            var data1 = data[0];
            jQuery("#companyname").append(jQuery("<option ></option>").val("0").html("Select Employer"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#companyname").append(jQuery("<option></option>").val(data1[i].EmpId).html(data1[i].CompName));
             }
        }
              
  }














