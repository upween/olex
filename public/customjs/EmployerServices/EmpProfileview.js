function fetchEmpprofile(){
    var path =  serverpath + "EmployerRegDetail/'" + sessionStorage.getItem("empId") + "'"
    securedajaxget(path,'parsedatafetchEmpprofile','comment','control'); 
}

function parsedatafetchEmpprofile(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fetchEmpprofile('parsedatasecuredfetchEmpprofile');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
             if(data[0][0].HO_YN =='0'){
                var hO_YN="Head Office";
            }
            else{
                var hO_YN= "Branch Office" ;   
            }

            var image="";
            if(data[0][0].EmployerLogo !=null){
              
               image=EmployerDocPath+data[0][0].EmployerLogo
                }
                else{     
                image="images/jobimg.jpg"
              }
              var EmpImage="";
   EmpImage="<div id='headshot' class='quickFade' ><img src='"+image+"' alt='"+data[0][0].CompName+"' style='border-radius: 0%;' /></div>" +
        "<div id='name'><h1 class='quickFade delayTwo' id='NameCV'>" + data[0][0].CompName + "</h1></div><div class='clear'></div>";

            $("#RegistrationNo").html(data[0][0].Emp_Regno);
            $("#Registrationdate").html(data[0][0].Emp_RegDt);
            $("#Sector").html(data[0][0].Description);
            $("#Ownership").html(data[0][0].Company_type);
            $("#CompanyName").html(data[0][0].CompName);
            $("#NatureofBusiness").html(data[0][0].Business_Type);
            $("#OfficeType").html(hO_YN);
            $("#noofemp").html(data[0][0].TotalEmployee);
            $("#username").html(data[0][0].Email);
            $("#PanNo").html(data[0][0].PanNo);
            $("#ContactPerson").html(data[0][0].Contact_Person);
            $("#Designation").html(data[0][0].Contact_Person_Desig);
            $("#State").html(data[0][0].StateName);
            $("#District").html(data[0][0].DistrictName);
            $("#City").html(data[0][0].CityName);
            $("#Address").html(data[0][0].Address);
            $("#CorrespondenceAddress").html(data[0][0].CorrespondenceAdd);
            $("#PINCode").html(data[0][0].Pincode);
            $("#Fax").html(data[0][0].fax);
            $("#STDCode").html(data[0][0].STDCode);
            $("#mobile").html(data[0][0].ContactNo_M);
            $("#ContactNo").html(data[0][0].ContactNo);
            $("#email").html(data[0][0].Email);
            $("#Website").html(data[0][0].URL);

          $("#mainDetails").html(EmpImage);

        }
}