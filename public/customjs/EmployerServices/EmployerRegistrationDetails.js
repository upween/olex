function FetchEmployerRegDetail() {
	var path = serverpath + "EmployerRegDetail/'1'"
	ajaxget(path, 'parsedataFetchEmployerRegDetail', 'comment', "control");
}


function parsedataFetchEmployerRegDetail(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchEmployerRegDetail('parsedataFetchEmployerRegDetail');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            
            var data1 = data[0];
            
            jQuery("#RegNo").html(data1[0].Emp_RegId);
            jQuery("#RegDate").html(data1[0].Emp_RegDt);
            jQuery("#CompanyType").html(data1[0].Company_type);
            jQuery("#CompanyName").html(data1[0].CompName);
            jQuery("#NatureOfBus").html(data1[0].Business_Type);
            jQuery("#nicCode").html(data1[0].NICCode);
            jQuery("#OfficeType").html(data1[0].HO_YN);
            jQuery("#noOfEmp").html(data1[0].TotalEmployee);
            jQuery("#Username").html(data1[0].Email);
            jQuery("#Password").html(data1[0].Email);
            jQuery("#Contactperson").html(data1[0].Contact_Person);
            jQuery("#Designation").html(data1[0].Contact_Person_Desig);
            jQuery("#state").html(data1[0].StateName);
            jQuery("#district").html(data1[0].District_Name);
            jQuery("#city").html(data1[0].CityName);
            jQuery("#address").html(data1[0].Address);
            jQuery("#pinCode").html(data1[0].Pincode);
            jQuery("#stdCode").html(data1[0].StdCode);
            jQuery("#ContactNo").html(data1[0].ContactNo);
            jQuery("#emailId").html(data1[0].Email);
            jQuery("#website").html(data1[0].Email);

        }
              
}