function FillEmployer() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "EmpByExchange/0",
        cache: false,
        dataType: "json",
        success: function (data) {
           
    jQuery("#companyname").empty();
    var data1 = data[0];


    jQuery("#companyname").append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));

    for (var i = 0; i < data1.length; i++) {
        jQuery("#companyname").append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
    
    }
    
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function fetchOpenVacancies() {

    var path = serverpath + "EmployerWithJobCount/"+$('#companyname').val()+"/'1'";
    ajaxget(path,'parsedatafetchOpenVacancies','comment','control');
}
function parsedatafetchOpenVacancies(data) {
    data = JSON.parse(data)

    jQuery("#postvacancy").empty();
    var data1 = data[0];


    jQuery("#postvacancy").append(jQuery("<option></option>").val('0').html("Select Vacancy"));

    for (var i = 0; i < data1.length; i++) {
        jQuery("#postvacancy").append(jQuery("<option></option>").val(data1[i].Postid).html(data1[i].Designation +' ( '+data1[i].Placeofwork+' )'));
    
    }



}
function checkValidation(){
    if(isValidation){

    if($('#companyname').val()=='0'){
        getvalidated('companyname','select','Company/Client Name');
        return false;
    }
    if($('#postvacancy').val()=='0'){
        getvalidated('postvacancy','select','Vacancy');
        return false;
    
    }
    
    else{
        Candidatedetail();
    }
}
    else{
        Candidatedetail();
    }
}
function Candidatedetail(){
    var path = serverpath + "interestedcandidatereport/"+$('#postvacancy').val()+"";
   ajaxget(path,'parsedataCandidatedetail','comment','control'); 
}
function parsedataCandidatedetail(data){
    data = JSON.parse(data)
    var data1=data[0];
    var appenddata=''
    jQuery("#tbodyvalue").empty();
    $("#companynamexl").html("Company/Client Name : " +$("#companyname option:selected").html()+" , "+$("#postvacancy option:selected").html())

    for (var i = 0; i < data1.length; i++) {

        appenddata += "<tr class='i'><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].CandidateName + "</td><td>"+data1[i].MobileNumber+"</td><td style='    word-break: break-word;'>" + data1[i].EmailId + "</td><td>"+data1[i].IdentificationNumber+"</td><td>"+data1[i].DistrictName+"</td><td>"+data1[i].Qualif_name+"</td><td>"+data1[i].Experience+"</td><td>"+data1[i].ApplicationDate+"</td></tr>";
	
   
    } jQuery("#tbodyvalue").html(appenddata);
    $('#tbodyvalue').clientSidePagination();
           
}