var code;
function createCaptchaempreg() {
    document.getElementById('captchaEmployerRegistration').innerHTML = "";
    var charsArray =
        "0123456789";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
        var index = Math.floor(Math.random() * charsArray.length + 1); 
        if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
        else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captchaEmployerRegistration";
    canv.width = 100;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    code = captcha.join("");
    document.getElementById("captchaEmployerRegistration").appendChild(canv);
}

function validateCaptcha() {
    event.preventDefault();
    if (document.getElementById("cpatchaTextBoxEmployerRegistration").value == code) {
        $('#postattach').click()
    } else {
        toastr.warning("Invalid Captcha..", "", "info")
        createCaptchaempreg();
        return true;
        
    }
}
function checkemail() {
    var Email;
    Email = jQuery('#Email').val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(Email) == false) {
        $("#txtemail").focus();
        toastr.warning("Please Enter Correct Email ID", "", "info")
        return true;
    }
}
function CheckValidation() {
    if(isValidation){
       if (jQuery('#txtCompany').val() == '') {
        $(window).scrollTop(0);
        getvalidated('txtCompany','text','Establishment/Company Name')
        return false;
    }
    if (jQuery('#ddlOwnership').val() == '0') {
        $(window).scrollTop(0);
        getvalidated('ddlOwnership','select','Ownership')
        return false;
    }
    if (jQuery('#ddlsector').val() == '0') {
        $(window).scrollTop(0);
        getvalidated('ddlsector','select','Sector')
        return false;
    }
    if (jQuery('#NatureofBusiness').val() == '') {
        $(window).scrollTop(0);
        getvalidated('NatureofBusiness','text','Nature of Business')
        return false;
    }
    var gst=$('#GSTNumber').val();
    $('#validGSTNumber').text('');
   
    if (jQuery('#txtContact').val() == '') {
        
        getvalidated('txtContact','text','Name of Contact Person')
        return false;
    }
    if (jQuery('#Designation').val() == '0') {
      
        getvalidated('Designation','select','Designation')
        return false;
    }
    if (jQuery('#state').val() == '0') {
     
        getvalidated('state','select','State');
        return false;
    }
    if (jQuery('#district').val() == '0') {
        getvalidated('district','select','District')
        return false;
    }
  
    if (jQuery('#permanentaddress').val() == '') {
        getvalidated('permanentaddress','text','Head Office Address')
        return false;;
    }
    if (jQuery('#presentaddress').val() == '') {
        getvalidated('presentaddress','text','Branch Office Address')
        return false;;
    }
   
    if (jQuery('#Phone').val() == '') {
        getvalidated('Phone','number','Mobile Number');
        return false;;
    }
    if (jQuery('#Email').val() == '') {
        getvalidated('Email','email','Email')
        return false;;
    }
     if (jQuery('#ceoemail').val() == '') {
        getvalidated('ceoemail','email','CEO Email')
        return false;;
    }
    if (jQuery('#EmploymentExchange').val() == '0') {
        getvalidated('EmploymentExchange','select','Employment Exchange')
        return false;;
    }
    if (jQuery('#username').val() == '0') {
        getvalidated('username','select','User Name')
        return false;;
    }
    if (jQuery('#passwordfield').val() == '') {
        getvalidated('passwordfield','text','Password')
        return false;
    }


   

    if (jQuery('#Pin').val() == '') {
        getvalidated('Pin','text','PIN Code')
        return false;
    }

    if (jQuery('#Repassword').val() == '') {
        getvalidated('Repassword','text','Confirm Password')
        return false;
    }
    if (jQuery('#passwordfield').val() != jQuery('#Repassword').val()) {
        jQuery('#Repassword').css('border-color', 'red');
        jQuery('#validRepassword').html('Password not matched');
        return false;
    }
    else{
        $('#postattach').click();
    }
   
}
else{
    $('#postattach').click()
}
};
$("#Repassword").focusout(function () {
    $("#validRepassword").html("");
    $("#Repassword").css('border-color', '');
    if (jQuery('#Repassword').val() == ''){
        getvalidated('Repassword','text','Confirm Password');
    }
     if (jQuery('#passwordfield').val() != jQuery('#Repassword').val()) {
    
        $("#validRepassword").html("Password not matched");
        $("#Repassword").css('border-color', 'red');
         
        
    }
});
$("#passwordfield").focusout(function () {
    if($("#passwordfield").val()==''){
        getvalidated('passwordfield','text','Password')
    }
    else{
        var password = $("#passwordfield").val();
        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
        if(password!=''){
    
        
        if (!regularExpression.test(password)) {
            $("#validpasswordfield").html("Password must contain atleast one letter, atleast one number, and be longer than 8 charaters");
        }
        else {
            $("#validpasswordfield").html("");
        }
    }  
    }
    
   
});


$("#passwordfield").keyup(function () {
    var password = $("#passwordfield").val();
    var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
    if(password!=''){

    
    if (!regularExpression.test(password)) {
        $("#validpasswordfield").html("Password must contain atleast one letter, atleast one number, and be longer than 8 charaters");
    }
    else {
        $("#validpasswordfield").html("");
    }
}
});

$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
function Fillsector(funct,control) {
    var path =  serverpath + "sector/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSector(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillsector('parsedatasecuredFillSector','ddlsector');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sector"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sector_Id).html(data1[i].Description));
             }
        }
              
}


function FillOwnership(funct,control) {
    var path =  serverpath + "companytype/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillOwnership(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillOwnership('parsedatasecuredFillOwnership','ddlOwnership');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Ownership Type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Company_type_id).html(data1[i].Company_type));
             }
        }
              
}

function Fillsectortype(funct,control) {
    var path =  serverpath + "sectortype/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillsectortype(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillsectortype('parsedatasecuredFillsectortype','ddlcompany');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Company Type"));
            
           for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_Id).html(data1[i].Des_cription));
             }
        }
              
}
function Fillempstate(funct,control) {
    var path =  serverpath + "state/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillempstate(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillempstate('parsedatasecuredFillempstate','state');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select State"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
             }
        }
              
}

jQuery('#state').on('change', function () {
    FillDistrict('parsedatasecuredFillDistrict','district',jQuery('#state').val());
  
});
function FillDistrict(funct,control,state) {
    var path =  serverpath + "district/'" + state + "'/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrict('parsedatasecuredFillDistrict','district');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
        }
              
}


jQuery('#district').on('change', function () {
    FillCity('parsedatasecuredFillCity','city',jQuery('#district').val());
  
});
function FillCity(funct,control,district) {
    var path =  serverpath + "city/'" + district + "'/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCity(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCity('parsedatasecuredFillCity','city');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Tehsil"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
             }
        }
              
}
function Fillexchangeoffice(funct,control) {
    var path =  serverpath + "exchangeoffice/0/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillexchangeoffice('parsedatasecuredFillexchangeoffice','EmploymentExchange');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Employment Exchange"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
        }
              
}
function InsUpdEmployerRegistration() {
    sessionStorage.setItem("ceomail",jQuery("#ceoemail").val()); 
    sessionStorage.setItem("emailval",jQuery("#Email").val()); 
    var entryby='Yashaswi';
if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
     entryby='Olex';
}
    var MasterData = {
        "p_Emp_Regno":"0",
        "p_Emp_RegId": "0",
        "p_Ex_id": jQuery("#EmploymentExchange").val(),
        "p_Emp_RegDt":Date.now(),
        "p_CompName": jQuery("#txtCompany").val(),
        "p_NICCode":"",
        "p_CompanyProfile": jQuery("#ddlcompany").val(),
        "p_Company_type_id": jQuery("#ddlOwnership").val(),
        "p_V_Status":"0",
        "p_HO_YN": jQuery("#ddlOfficeType").val(),
        "p_TotalEmployee": jQuery("#NumberofEmployee").val(),
        "p_VerifyDt":"0000-00-00",
        "p_CancelDt":"0000-00-00",
        "p_Contact_Person": jQuery("#txtContact").val(),
        "p_Contact_Person_Desig": jQuery("#Designation").val(),
        "p_Address": jQuery("#permanentaddress").val(),
        "p_City": jQuery("#city").val(),
        "p_District_id": jQuery("#district").val(),
        "p_District_Name": "",
        "p_State_ID": jQuery("#state").val(),
        "p_Pincode": jQuery("#Pin").val(),
        "p_ContactNo": jQuery("#Landline").val(),
        "p_StdCode": jQuery("#STDCode").val(),
        "p_Email": jQuery("#Email").val(),
        "p_fax": "808080",
        "p_URL":jQuery("#Website").val(),
        "p_Business_Type": jQuery("#NatureofBusiness").val(),
        "p_E_Userid":"0",
        "p_PanNo": jQuery("#Panno").val(),
        "p_CorrespondenceAdd": jQuery("#presentaddress").val(),
        "p_ContactNo_M": jQuery("#Phone").val(),
        "p_Sector_Id": jQuery("#ddlsector").val(),
        "p_Gst_Number":$("#GSTNumber").val(),
        "p_Password":md5($("#passwordfield").val()),
        "P_GST_File":sessionStorage.getItem("GSTDoc"),
        "P_PAN_File":sessionStorage.getItem("PANDoc"),
        "p_EntryBy":entryby,
        }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "employerregistrations";
    ajaxpost(path, 'parsrdataregistration', 'comment', MasterData, 'control')
}
function parsrdataregistration(data) {
    data = JSON.parse(data)

    if (data[0][0].ReturnValue == "1") {
        toastr.warning("You Are Already Registered", "", "info")
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
     
        toastr.success('Registration Successful', "", "success");
        if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
            fetchInActiveEmp(data[0][0].NewEmpId,'0','Update');
       }
       else{
        if(sessionStorage.erType=='er1form'){
            
            sessionStorage.removeItem('erType');
            window.location='/EmployerServices/ER1Form';

        }
       }
        

    }
}

      $('input[type="radio"]').click(function () {
        if ($(this).attr('id') == 'yesAdd') {
          $("#presentaddress").val($("#permanentaddress").val())
          $("#presentaddress").attr("disabled",true)
          $("#branchofficeaddress").hide()
        }
        else if ($(this).attr('id') == 'noAdd') {
            $("#presentaddress").val("")
            $("#presentaddress").attr("disabled",false)
            $("#branchofficeaddress").show()
        }
      });
 function FillDesignation() {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "adminDesignation/0/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Designation").append(jQuery("<option ></option>").val("0").html("Select Designation"));
         
                for (var i = 0; i < data[0].length; i++) {
                    jQuery("#Designation").append(jQuery("<option></option>").val(data[0][i].Designation_Id).html(data[0][i].Designation));
                }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }
function checkWebsite() {
    if(jQuery('#Website').val()!=''){
    var myURL;
        myURL = jQuery('#Website').val();
       var web = new RegExp('^(https?:\\/\\/)?'+ 
       '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ 
       '((\\d{1,3}\\.){3}\\d{1,3}))'+ 
       '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ 
       '(\\?[;&amp;a-z\\d%_.~+=-]*)?'+ 
       '(\\#[-a-z\\d_]*)?$','i');
         if (web.test(myURL) == false) {
        toastr.warning("Please Enter Correct Website", "", "info")
        
    }
}
}
function sendmsg(Msg,Mobile){
    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://control.msg91.com/api/postsms.php",
    "method": "POST",
    "headers": {
    "content-type": "application",
    "Access-Control-Allow-Origin": "http://localhost:3200/Test1"
    },
    "data": "<MESSAGE> <AUTHKEY>264840AT4bfsGwDs5c74e909</AUTHKEY> <SENDER>Rojgar</SENDER><CAMPAIGN>MPRojgar</CAMPAIGN> <COUNTRY>+91</COUNTRY> <SMS TEXT=\""+Msg+"\" > <ADDRESS TO=\""+Mobile+"\"></ADDRESS></SMS> </MESSAGE>"
    }
    $.ajax(settings).done(function (response) {
    console.log(response);
    });
    }
    function InsUpdotp(Type,Id,verificationtype) {

        sessionStorage.verifytype=verificationtype;
        
       var  generateOTPmaster =generateOTP();
       sessionStorage.emailOTP=generateOTPmaster;
       sessionStorage.emailval=$("#Email").val();
           var MasterData = {
                  "p_EmpId" : Id,
                  "p_Flag" : verificationtype,
                  "p_FlagValue" :generateOTPmaster,
                  "p_Type":Type
                 
           }
           MasterData = JSON.stringify(MasterData)
           var path = serverpath + "employerverification";
           ajaxpost(path, 'parsrdataemailverification', 'comment', MasterData, 'control')
       }
       function parsrdataemailverification(data) {
           data = JSON.parse(data)
       
           if (data[0][0].ReturnValue == "3") {
               InsUpdotp1('Verify',sessionStorage.getItem("EmployerId"),'mobile') ;
           
               return true;
              
           }
           else if (data[0][0].ReturnValue == "4") {
             if(sessionStorage.verifytype=='email'){
              InsUpdotp('Verify',sessionStorage.EmployerId,'mobile') ;
              }
             else{
               window.location = '/SuccessRegistration'  
            }
         
           }
           else if (data[0][0].ReturnValue == "5") {
               
               toastr.warning("Please Enter Correct OTP", "", "info")
               return true;
           }
       }
    function InsUpdotp1(Type,Id,verificationtype) {

        sessionStorage.verifytype=verificationtype;
        if (Type=="Verify"){
        var generateOTPmaster =generateOTP();
        sessionStorage.mobileOTP=generateOTPmaster;
        sessionStorage.mobileval=$("#Phone").val();
        }
        
        if (Type=="Match" && verificationtype=='email') {
        var generateOTPmaster =$("#checkemailotp").val(); 
        
        }
        if (Type=="Match" && verificationtype=='mobile'){ 
        var generateOTPmaster =$("#checkotp").val();
        }
        
        var MasterData = {
        "p_EmpId" : Id,
        "p_Flag" : verificationtype,
        "p_FlagValue" :generateOTPmaster,
        "p_Type":Type
        
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "employerverification";
        ajaxpost(path, 'parsrdataemailverification1', 'comment', MasterData, 'control')
        }
        
        function parsrdataemailverification1(data) {
        data = JSON.parse(data)
        
        if (data[0][0].ReturnValue == "3") {
         window.location = '/SuccessRegistration' 
        return true;
      
        }
        else if (data[0][0].ReturnValue == "4") {
        if(sessionStorage.verifytype=='email'){
        InsUpdotp('Verify',sessionStorage.EmployerId,'mobile') ;
        
        }
        
        }
        else if (data[0][0].ReturnValue == "5") {
        
        toastr.warning("Please Enter Correct OTP", "", "info")
        return true;
        }
        }
       
        $('#gstupload').submit(function() {
            $(this).ajaxSubmit({
                error: function(xhr) {
                    toastr.warning(xhr.status, "", "info")
                },
                success: function(response) {
                    if (response == "Error No File Selected For GST Upload"){
                        Cookies.set('modaltype', 'pandoc', { expires: 1, path: '/' });
                        $('#panpostattach').click(); 
                    }
                    else if (response == "Request Entity Too Large"){
                        toastr.warning(response, "", "info")
                    }
                    else if (response == "Error: PDF and Image File Only!"){
                        toastr.warning(response, "", "info")
                    }
                    else{
                        var str = response;
                        
                        var res = str.split("!");
                        sessionStorage.setItem("GSTDoc", res[1])
                        Cookies.set('modaltype', 'pandoc', { expires: 1, path: '/' });
                        $('#panpostattach').click();
                      
                    }
                }
            });
            return false;
         });
   
         $('#panupload').submit(function() {
            $(this).ajaxSubmit({
                error: function(xhr) {
                    toastr.warning(xhr.status, "", "info")
                },
                success: function(response) {
                    if (response == "Error No File Selected For PAN Upload"){
                        InsUpdEmployerRegistration();
                    }
                    else if (response == "Request Entity Too Large"){
                        toastr.warning(response, "", "info")
                    }
                    else if (response == "Error: PDF and Image File Only!"){
                        toastr.warning(response, "", "info")
                    }
                    else{
                        var str = response;
                        
                        var res = str.split("!");
                        sessionStorage.setItem("PANDoc", res[1])
                        //toastr.success(res[0], "", "success")
                      InsUpdEmployerRegistration()
                    }
                }
            });
            return false;
         });
        function changeTurnover(){
            if($('#Annualturnover').val()=='1'){
                $('#gstLabel').text('GST Number');
$('#gstupoloadLabel').text('GST Upload')
            }
            if($('#Annualturnover').val()=='2'){
                $('#gstLabel').text('Non GST Number');
$('#gstupoloadLabel').text('Non GST Certificate Upload')
            }
        }
        function fetchInActiveEmp(Emp_Regno,V_type,Type){
           
              var path =  serverpath +"InActiveEmp/'"+Emp_Regno+"'/'"+V_type+"'/'"+Type+"'"
              ajaxget(path,'parsedatafetchInActiveEmp','comment',"control");
          }
          function parsedatafetchInActiveEmp(data){  
              data = JSON.parse(data)
              if(sessionStorage.erType=='er1form'){
            
                sessionStorage.removeItem('erType');
                window.location='/EmployerServices/ER1Form';
    
            }
            }