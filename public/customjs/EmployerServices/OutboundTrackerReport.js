function FillOutboundReport() {
    var fromDate=$('#m_datepicker_5').val().split('/')[2]+"-"+$('#m_datepicker_5').val().split('/')[1]+"-"+$('#m_datepicker_5').val().split('/')[0];
   if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
var empId=$('#recruiterddl').val();
   }
   else{
    var empId=sessionStorage.Emp_Id;
   }
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
       url: serverpath + "getboundtrckerbyEmpId/"+empId+"/'"+fromDate+"'/"+jQuery("#clientddl").val()+"/"+jQuery("#categoryddl").val()+"/"+jQuery("#districtddl").val()+"/'"+jQuery("#genderdl").val()+"'",
        cache: false,
        dataType: "json",
        success: function (data) {
           
    //jQuery("#companyname").empty();
    var data1 = data[0];

var appenddata="";
$('#tbodyvalue').empty();
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    var tablehead=`  <tr>
    <th>#</th>
    <th>Portal Registration Number</th>
    <th>Source</th>
    <th>Client</th>
    <th>Position</th>
    <th>Month</th>
    <th>Date of Calling</th>
    <th>Sr No</th>
    <th>Category(SC/ST/OBC/UR)</th>
    <th>Candidate District</th>
    <th>Age</th>
    <th>Candidate Name</th>
    <th>Contact No.</th>
    <th>Qualification</th>
    <th>Total Experience</th>
    <th>Job Location</th>
    <th>Candidate Current Location</th>
    <th>Current Company</th>
    <th>Current Designation</th>
    <th>Current/Last Salary</th>
    <th>Expected Salary(in months)</th>
    <th>Notice Period</th>
    <th>Reason for change</th>
    <th>Email id</th>
    <th>Gender</th>
    <th>Calling Status</th>
    <th>Remarks</th>
    <th>Date of Interview</th>
    <th>Date of Joining</th>
    <th>Follow Up(Reached/Not reached)</th>
    <th>Interview Status(appeared/selected/rejected)</th>
    <th>Recruiter</th>
    </tr>`
    $('#tablehead').html(tablehead);
for(var i =0;i<data1.length;i++){
    appenddata+=`<tr>
    <td>${i+1}</td>
    <td>${data1[i].PortalRegistrationNumber}</td>
    <td>${data1[i].Source}</td>
    <td>${data1[i].ClientName}</td>
    <td>${data1[i].Position}</td>
    <td>${data1[i].FullMonthName}</td>
    <td>${data1[i].DateofCalling}</td>
    <td>${data1[i].SrNo}</td>
    <td>${data1[i].CategoryName}</td>
    <td>${data1[i].DistrictName}</td>
    <td>${data1[i].Age}</td>
    <td>${data1[i].CandidateName}</td>
    <td>${data1[i].ContactNo}</td>
    <td>${data1[i].Qualification}</td>
    <td>${data1[i].TotalExperience}</td>
    <td>${data1[i].JobLocation}</td>
    <td>${data1[i].CandidateCurrentLocation}</td>
    <td>${data1[i].CurrentCompany}</td>
    <td>${data1[i].CurrentDesignation}</td>
    <td>${data1[i].LastSalary}</td>
    <td>${data1[i].ExpectedSalary}</td>
    <td>${data1[i].NoticePeriod}</td>
    <td>${data1[i].Reasonforchange}</td>
    <td>${data1[i].EmailId}</td>
    <td>${data1[i].Gender}</td>
    <td>${data1[i].CallingStatus}</td>
    <td>${data1[i].Remarks}</td>
    <td>${data1[i].DateofInterview}</td>
    <td>${data1[i].DateofJoining}</td>
    <td>${data1[i].FollowUp}</td>
    <td>${data1[i].InterviewStatus}</td>
    <td>${data1[i].Recruiter}</td>
    

    </tr>`
}

$('#tbodyvalue').append(appenddata);
}
else{
    var tablehead=`  <tr>
    <th>#</th>
    <th>Portal Registration Number</th>
    <th>Source</th>
    <th>Client</th>
    <th>Position</th>
    <th>Month</th>
    <th>Date of Calling</th>
    <th>Sr No</th>
    <th>Category(SC/ST/OBC/UR)</th>
    <th>Candidate District</th>
    <th>Age</th>
    <th>Candidate Name</th>
    <th>Contact No.</th>
    <th>Qualification</th>
    <th>Total Experience</th>
    <th>Job Location</th>
    <th>Candidate Current Location</th>
    <th>Current Company</th>
    <th>Current Designation</th>
    <th>Current/Last Salary</th>
    <th>Expected Salary(in months)</th>
    <th>Notice Period</th>
    <th>Reason for change</th>
    <th>Email id</th>
    <th>Gender</th>
    <th>Calling Status</th>
    <th>Remarks</th>
    <th>Date of Interview</th>
    <th>Date of Joining</th>
    <th>Follow Up(Reached/Not reached)</th>
    <th>Interview Status(appeared/selected/rejected)</th>
    <th>Recruiter</th>
    <th>Modify</th>
    <th>Save</th>
    <th>Whatsapp</th>
    <th>Mail</th>
    </tr>`
    $('#tablehead').html(tablehead);

    for(var i =0;i<data1.length;i++){
         appenddata+= `<tr class='i' >
         <td>${ [i + 1]}</td>
        <td><input type='text' value='${data1[i]['PortalRegistrationNumber']}' id='PortalRegistrationNumber${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['Source']}' id='Source${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><select id='client${i+1}' style='min-width:175px' class="formcontrols client inputControls${i+1}" disabled>/select></td>
        
        <td><input type='text' value='${data1[i]['Position']}' id='Position${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><select style='min-width:175px' id='month${ [i + 1]}' class="formcontrols month inputControls${i+1}" disabled>/select></td>
        <td><input type='text' value='${data1[i]['DateofCalling']}' id='DateOfCalling${i+1}' class="formcontrols inputControls${i+1} datepicker" disabled/></td>
        <td><input type='text' value='${data1[i]['SrNo']}' id='SrNo${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
       
        <td><select  style='min-width:175px' id='category${ [i + 1]}' class="formcontrols category inputControls${i+1}" disabled>/select></td>
      
        <td><select  id='district${ [i + 1]}' style='min-width:175px' class="formcontrols district inputControls${i+1}" disabled>/select></td>
        
        <td><input type='text' value='${data1[i]['Age']}' id='Age${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['CandidateName']}' id='CandidateName${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['ContactNo']}' id='ContactNo${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['Qualification']}' id='Qualification${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['TotalExperience']}' id='TotalExperience${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['JobLocation']}' id='JobLocation${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['CandidateCurrentLocation']}' id='CandidateCurrentLocation${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['CurrentCompany']}' id='CurrentCompany${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['CurrentDesignation']}' id='CurrentDesignation${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['LastSalary']}' id='CurrentSalary${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['ExpectedSalary']}' id='ExpectedSalary${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['NoticePeriod']}' id='NoticePeriod${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['Reasonforchange']}' id='ReasonForChange${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['EmailId']}' id='EmailId${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td>
        <select  id='gender${i+1}' class="formcontrols gender inputControls${i+1}" disabled>
        <option>Male</option>
        <option>Female</option>
        <option>Other</option>
        </select>
        </td>
        <td><input type='text' value='${data1[i]['CallingStatus']}' id='CallingStatus${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['Remarks']}' id='Remarks${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td><input type='text' value='${data1[i]['DateofInterview']}' id='DateOfInterview${i+1}' class="formcontrols inputControls${i+1} datepicker" disabled/></td>
        <td><input type='text' value='${data1[i]['DateofJoining']}' id='DateOfJoining${i+1}' class="formcontrols inputControls${i+1} datepicker" disabled/></td>
        <td> <select  id='followup${i+1}' class="formcontrols foolowup inputControls${i+1}" disabled>
        <option>Reached</option>
        <option>Not Reached</option>
        </select>
        </td>
        <td>
        <select id='interviewstatus${i+1}' class="formcontrols interviewstatus inputControls${i+1}" disabled>
        <option>Appeared</option>
        <option>Selected</option>
        <option>Rejected</option>
        </select></td>
        <td><input type='text' value='${data1[i]['Recruiter']}' id='Recruiter${i+1}' class="formcontrols inputControls${i+1}" disabled/></td>
        <td> <button type="button"  class="btn btn-success"
        style="background-color:#716aca;border-color:#716aca;" onclick="$('.inputControls${i+1}').attr('disabled',false);">
        Modify
    </button></td>
        <td> <button type="button"  class="btn btn-success"
        style="background-color:#716aca;border-color:#716aca;" onclick="InsupdateTracker(${data1[i].TrackerId},${i+1})">
       Save
    </button></td>
        <td>   <a href="https://api.whatsapp.com/send?phone=91${data1[i].ContactNo}" target='_blank'> <i  class="fa fa-whatsapp" style='color: limegreen;
        font-size: 37px;
        font-weight: 600;'></i></a></td>
        <td><a href="https://outlook.live.com/mail/0/inbox" target='_blank'> <i  class="fa fa-envelope-square" style='    color: blue;
        font-size: 37px;
        font-weight: 600;'></i></a></td>
        
        
         
<tr>
        
    
        </tr>`
    }
    
$('#tbodyvalue').append(appenddata);
    setTimeout(function(){ 
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        })
        FillCandidateCategory(data1);
        FillCandidatemonth(data1);
        FillCandidateDistrict(data1);
        FillCandidateEmployer(data1)
        for (var i = 0; i < data1.length; i++) {
               $('#gender' + [i + 1] + ' option:contains(' + data1[i]['Gender'] + ')').prop('selected', true);
               $('#followup' + [i + 1] + ' option:contains(' + data1[i]['FollowUp'] + ')').prop('selected', true);
          
               
               $('#interviewstatus' + [i + 1] + ' option:contains(' + data1[i]['InterviewStatus']+ ')').prop('selected', true);
              
           
          
       };
        },300);
}


        },
error: function (xhr) {
    toastr.success(xhr.d, "", "error")
    return true;
}
});
}
function FillCategory() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "category/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery("#categoryddl").empty();
            var data1 = data[0];


            jQuery("#categoryddl").append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#categoryddl").append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
            }
            
           
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillDistrict() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/19/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery("#districtddl").empty();
            var data1 = data[0];


            jQuery("#districtddl").append(jQuery("<option></option>").val('0').html("Select District"));

            for (var i = 0; i < data1.length; i++) {
                jQuery("#districtddl").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
            }
        
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillEmployer() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "fetch_dwr_clients/0/1",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery("#clientddl").empty();
            var data1 = data[0];
            jQuery(`#clientddl`).append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(`#clientddl`).append(jQuery("<option></option>").val(data1[i].ClientId).html(data1[i].ClientName));
            
            }

          
          
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillCandidateCategory(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "category/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".category").empty();
            var data1 = data[0];


            jQuery(".category").append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(".category").append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
            }
            
            for (var i = 0; i < candidatelist1.length; i++) {
                //     $('.clientname#clientname' + [i+1] + ' option:contains(' + candidatelist1[i]['company/client name'] +')').prop('selected', true);
                $('.category#category' + [i + 1] + ' option:contains(' + candidatelist1[i]['CategoryName'] + ')').prop('selected', true);
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillCandidatemonth(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "month/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".month").empty();
            var data1 = data[0];


            jQuery(".month").append(jQuery("<option ></option>").val("0").html("Select month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(".month").append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
            }
            
            for (var i = 0; i < candidatelist1.length; i++) {
                //     $('.clientname#clientname' + [i+1] + ' option:contains(' + candidatelist1[i]['company/client name'] +')').prop('selected', true);
                $('.month#month' + [i + 1] + ' option:contains(' + candidatelist1[i]['FullMonthName'] + ')').prop('selected', true);
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillCandidateDistrict(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/19/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".district").empty();
            var data1 = data[0];


            jQuery(".district").append(jQuery("<option></option>").val('0').html("Select District"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(".district").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
            }
            for (var i = 0; i < candidatelist1.length; i++) {
                console.log('in District')
                if(candidatelist1[i]['CandidateDistrict']!=='' || candidatelist1[i]['CandidateDistrict']!==null){
                    $('.district#district' + [i + 1] + ' option:contains(' + candidatelist1[i]['DistrictName'] + ')').prop('selected', true);

                }
               
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillCandidateEmployer(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "fetch_dwr_clients/0/1",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".client").empty();
            var data1 = data[0];
            jQuery(`.client`).append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(`.client`).append(jQuery("<option></option>").val(data1[i].ClientId).html(data1[i].ClientName));
            
            }

          
            for (var i = 0; i < candidatelist1.length; i++) {
                if(candidatelist1[i]['Client']!=='' || candidatelist1[i]['Client']!==null){
                    $('.client#client' + [i + 1] + ' option:contains(' + candidatelist1[i]['ClientName'] + ')').prop('selected', true);

                }
               
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function InsupdateTracker(id,i){
    var DateofCalling = moment($(`#DateOfCalling${i}`).val(),'DD/MM/YYYY').format('YYYY-MM-DD');
    var DateofInterview = moment($(`#DateOfInterview${i}`).val(),'DD/MM/YYYY').format('YYYY-MM-DD');
    var DateofJoining = moment($(`#DateOfJoining${i}`).val(),'DD/MM/YYYY').format('YYYY-MM-DD');

    var MasterData={
 "p_TrackerId":id,
"p_PortalRegistrationNumber":$(`#PortalRegistrationNumber${i}`).val(),
"p_Source":$(`#Source${i}`).val(),
"p_Client":$(`#client${i}`).val(),
"p_Position":$(`#Position${i}`).val(),
"p_Month":$(`#month${i}`).val(),
"p_DateofCalling":DateofCalling,
"p_SrNo":$(`#SrNo${i}`).val(),
"p_Category":$(`#category${i}`).val(),
"p_CandidateDistrict":$(`#district${i}`).val(),
"p_Age":$(`#Age${i}`).val(),
"p_CandidateName":$(`#CandidateName${i}`).val(),
"p_ContactNo":$(`#ContactNo${i}`).val(),
"p_Qualification":$(`#Qualification${i}`).val(),
"p_TotalExperience":$(`#TotalExperience${i}`).val(),
"p_JobLocation":$(`#JobLocation${i}`).val(),
"p_CandidateCurrentLocation":$(`#CandidateCurrentLocation${i}`).val(),
"p_CurrentCompany":$(`#CurrentCompany${i}`).val(),
"p_CurrentDesignation":$(`#CurrentDesignation${i}`).val(),
"p_LastSalary":$(`#CurrentSalary${i}`).val(),
"p_ExpectedSalary":$(`#ExpectedSalary${i}`).val(),
"p_NoticePeriod":$(`#NoticePeriod${i}`).val(),
"p_Reasonforchange":$(`#ReasonForChange${i}`).val(),
"p_EmailId":$(`#EmailId${i}`).val(),
"p_Gender":$(`#gender${i}`).val(),
"p_CallingStatus":$(`#CallingStatus${i}`).val(),
"p_Remarks":$(`#Remarks${i}`).val(),
"p_DateofInterview":DateofInterview,
"p_DateofJoining":DateofJoining,
"p_FollowUp":$(`#followup${i}`).val(),
"p_InterviewStatus":$(`#interviewstatus${i}`).val(),
"p_Recruiter":$(`#Recruiter${i}`).val(),
"p_EntryBy": sessionStorage.Emp_Id,
"p_IpAddress": sessionStorage.IpAddress ,
"p_UpdatedBy":sessionStorage.Emp_Id 
    }
    MasterData =JSON.stringify(MasterData)
    var path = serverpath + "insoutboundtrcker";
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        data: MasterData,
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (data, status, jqXHR) {
          
          if(data[0][0].ReturnValue=='2'){

            FillOutboundReport();
            toastr.success("Update Successfully");
          } 
        },
        error: function (errordata) {
         
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                toastr.warning(errordata, "", "info")
                return true;
            }
        }
    });
}
function FillUsername(funct,control) {
    var path =  serverpath +"crmuser"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillUsername(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillUsername('parsedatasecuredFillUsername',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            console.log(data)
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('Select'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
             
        }
              
}
