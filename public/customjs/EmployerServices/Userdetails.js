function fetchUserdetails() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "EmployerRegDetailbyEmail/'"+$('#uniquenumber').val()+"'",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
    var tablehead="<th>S.No</th><th>Company Name</th><th>Registration Id</th><th>Contact Number</th><th>Status</th><th>View Details</th><th>Reset Passward</th>"
    var appendata=''
    for(var i =0; i<data1.length;i++){
        if(data1[i].V_Status=='1'){
var activestatus='Verified';
        }
        if(data1[i].V_Status=='2'){
            var activestatus='Removed';
                    }
      if(data1[i].V_Status=='0'){
                        var activestatus='Not Verified';
                                }
     
appendata +=` <tr>
<th scope="row">
${[i+1]}
</th>
<td>
${data1[i].CompName}</td>



<td>
${data1[i].Emp_Regno}</td>


<td>
${data1[i].ContactNo}</td>


<td>
${activestatus}
</td>

<td>

<input type="button" value="View" onclick="sessionStorage.empId=${data1[i].EmpId};window.location='/EmpProfileview'" class="btn btn-primary py-3 px-5">
</td>

<td> 
 <input type="button"  value="ResetPassword" onclick="fetchResetpassword(${data1[i].EmpId})" class="btn btn-primary py-3 px-5"></td>
</tr>`
    }
    $('#tablehead').html(tablehead);
    $('#tbodyvalue').html(appendata);
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function fetchResetpassword(EmpId) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "Employer_ChangePasswordbyemail/"+EmpId+"",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
    if (data1[0].ReturnValue==1)
        {
            toastr.success("Password changed")
        }
    }
    });
}

 
