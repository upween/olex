function FillCCMonthemps(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCCMonthemps(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillCCMonthemps('parsedatasecuredFillCCMonthemps',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthNameHindi));
    }
    
    
    }
    
    }



    function FillYearemps(funct,control) {
        var path =  serverpath + "secured/year/0/20"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearemps(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearemps('parsedatasecuredFillYearemps',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
                
            }
                  
    }


function fetchPlacementExchange(){
    if(sessionStorage.User_Type_Id=='0'|| sessionStorage.User_Type_Id=='1'){
        var districtid=0
    }
    else{
        var districtid=sessionStorage.DistrictId;
    }
    var path =  serverpath + "emp_selfemployment_report/"+districtid+"/"+$('#Month').val()+"/"+$('#Year').val()+""
    ajaxget(path,'parsedatafetchplacement','comment',"control");
}
function parsedatafetchplacement(data){  
    data = JSON.parse(data)
   var appenddata="";

   var sumLR=0  
  
var data1 = data[0];    
    var m=0;
    var sum_renewal=0;
 $('#excel').show();
var EmploymentCount=0;
var EmploymentCount_OL=0;
var SelfEmploymentCount=0;
var grand_total=0;
var EmploymentCount_NP=0;
var Emp_Total=0;
var Emp_Grand_Total=0;
var total_row='';
$("#headingtext").html("माह "+$('#Month option:selected').text()+'-'+$('#Year').val()+" में रोजगार एवं स्वरोजगार की जानकारी" )
var head='<tr><th></th><th colspan=4>माह '+$("#Month option:selected").text()+'-'+$("#Year").val()+' में रोजगार एवं स्वरोजगार की जानकारी</th></tr>';
$('#Detailhead').html(head)
            for (var i = 0; i < data1.length; i++) {
              
        m=1+i;
        total=parseInt(data1[i].EmploymentCount)+(data1[i].EmploymentCount_OL)+(data1[i].EmploymentCount_NP)+parseInt(data1[i].SelfEmploymentCount);
        EmploymentCount+=parseInt(data1[i].EmploymentCount);
        EmploymentCount_OL+=parseInt(data1[i].EmploymentCount_OL);
        EmploymentCount_NP+=parseInt(data1[i].EmploymentCount_NP);
        SelfEmploymentCount+=parseInt(data1[i].SelfEmploymentCount);
        Emp_Total=(data1[i].EmploymentCount)+(data1[i].EmploymentCount_OL)+(data1[i].EmploymentCount_NP);
        Emp_Grand_Total=EmploymentCount+EmploymentCount_OL+EmploymentCount_NP;
        grand_total=EmploymentCount+EmploymentCount_OL+SelfEmploymentCount+EmploymentCount_NP;
 appenddata+= "<tr><td style='    word-break: break-word;text-align: center;'>"+m+"</td><td style='word-break: break-word;text-align: center;'>" + data1[i].DistrictName+ "</td><td style='    word-break: break-word;text-align: center;text-align: center;'>" + data1[i].EmploymentCount + "</td><td style='    word-break: break-word;text-align: center;text-align: center;'>" + data1[i].EmploymentCount_OL + "</td><td style='    word-break: break-word;text-align: center;text-align: center;'>" + data1[i].EmploymentCount_NP + "</td><td style='    word-break: break-word;text-align: center;text-align: center;'>"+Emp_Total+"</td><td style='    word-break: break-word;text-align: center;text-align: center;'>" + data1[i].SelfEmploymentCount + "</td><td style='text-align: center;'>"+total+"</td></tr>";
 
        }jQuery("#tbodyvalue").html(appenddata+"<tr><td></td><td style='text-align: center;'><strong>Total</strong></td><td style='font-weight:700;text-align: center'>"+EmploymentCount+"</td><td style='font-weight:700;text-align: center'>"+EmploymentCount_OL+"</td><td style='font-weight:700;text-align: center'>"+EmploymentCount_NP+"</td><td style='font-weight:700;text-align: center'>"+Emp_Grand_Total+"</td><td style='font-weight:700;text-align: center'>"+SelfEmploymentCount+"</td><td style='font-weight:700;text-align: center'>"+grand_total+"</td></tr>");
      
              
}
function checkValidation(){
    if($(`#month`).val()=='0'){
    
    toastr.warning('Please Select Month')
    return false;
    }
    if($(`#textContactYear`).val()=='0'){
    
        toastr.warning('Please Select Year')
        return false;
        }
    
    else{
        fetchPlacementExchange()
    }
}

