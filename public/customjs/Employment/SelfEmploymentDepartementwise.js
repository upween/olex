

var date = new Date();
function fetchselfemployment() {
    // var data =$('#m_datepicker_5').val();
    // var arr = data.split('/');
    // var frm_dt=arr[2] + "-" + arr[1]+"-"+arr[0]

    // var data1 =$('#m_datepicker_6').val();
    // var arr1 = data1.split('/');
    // var to_dt=arr1[2] + "-" + arr1[1]+"-"+arr1[0];
    if(sessionStorage.User_Type_Id=='0'|| sessionStorage.User_Type_Id=='1'){
        var districtid=0
    }
    else{
        var districtid=sessionStorage.DistrictId;
    }
    if( sessionStorage.User_Type_Id=='37'){
        var empDeptId=sessionStorage.empDeptId
    }
    else{
        var empDeptId=0;
    }
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "selfemploymentdepartmentmulti/"+empDeptId+"/"+districtid+"/'"+$('#month').val()+"'/"+$("#Year").val()+"",
        cache: false,
        dataType: "json",
        success: function (data) {
            $("#tablehead,#tbodyvalue").empty()
            if(data.length){
            var dataColumn= data[0][0].DyColumns.split(',');
    var data1= data[1]
    var schemeColumn='';
    var head="<th>Report Name-Self Employment Report</th><th>From Date-"+$('#m_datepicker_5').val()+"</th><th>TO Date-"+$('#m_datepicker_6').val()+"</th>";
    $("#headingtext").html("माह "+' '+$('#Month option:selected').text()+'-'+$('#Year').val() +" में स्वरोजगार की जानकारी" )
    var head='<th></th><th colspan=3>'+"माह "+' '+$('#Month option:selected').text()+'-'+$('#Year').val() +" में स्वरोजगार की जानकारी"+'</th>';
    $('#Detailhead').html(head);
    var dynColumn=""
    var selfempTotal=0
 	var appenddata="";
     for(var j=0;j<dataColumn.length;j++){
      
        schemeColumn+=`<th style='background: #716aca;border-right: inset;border-bottom:inset'>${dataColumn[j]}</th>`
      //  dynColumn+=`<td>${data1[i][dataColumn[j]]}</td>`;
      //  selfempTotal+=parseInt(data1[i][dataColumn[j]]);
    }
				for (var i = 0; i < data1.length; i++) {
                    dynColumn="",selfempTotal=0;
                    for(var j=0;j<dataColumn.length;j++){
      if(data1[i][dataColumn[j]]!=null){
        var dataColumnj=data1[i][dataColumn[j]];
      }else{
        var dataColumnj=0;
      }
                      //  schemeColumn+=`<th style='background: #716aca;border-right: inset;border-bottom:inset'>${dataColumn[j]}</th>`
                      if(data1[i]['DistrictName_a']=='Total'){
                      dynColumn+=`<td style='font-weight:600'>${dataColumnj}</td>`;
                      }
                      else{
                        dynColumn+=`<td>${dataColumnj}</td>`;  
                      }
                        selfempTotal+=parseInt(dataColumnj);
                    }
                    if(data1[i]['DistrictName_a']=='Total'){
                        appenddata += "<tr><td colspan=2 style='font-weight:600'>"+data1[i]['DistrictName_a']+"</td>"+dynColumn+"<td style='font-weight:600'>"+selfempTotal+"</td></tr>";   
               
                    }
                    else{
				appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i]['DistrictName_a']+"</td>"+dynColumn+"<td>"+selfempTotal+"</td></tr>";   
                 }
                }
                 $('#Detailhead').html(head);

                 var tablehead=`<tr><th style='background: #716aca;border-right: inset;border-bottom:inset'>S.No</th>
                 <th style='background: #716aca;border-right: inset;border-bottom:inset'>District Name</th>
                 ${schemeColumn}
                 <th style='background: #716aca;border-right: inset;border-bottom:inset'>Self Employment Total</th></tr>`
                 $("#tablehead").html(tablehead);          
                jQuery("#tbodyvalue").html(appenddata)
            }
            else{
                $("#tablehead").html("<th>No Record Found</th>");    
            }
        },
        
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });

}
var date = new Date();
function FillMonthself(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthself(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillMonthself();
    }
    else {
        var data1 = data[0];
        $('#month').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'MonthId',
            labelField: 'FullMonthName',
            searchField: 'FullMonthName',
            options: data1,
            create: true
        });
    }
              
}


function FillYearself(funct,control) {
    var path =  serverpath + "secured/year/0/20"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYearself(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYearself('parsedatasecuredFillYearself',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
          
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
              
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }
             jQuery("#"+control).val(date.getFullYear())
            
        }
              
}