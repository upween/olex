jQuery('#GDexchange').on('change', function () {
    FillGDDetail(jQuery('#GDexchange').val(),'1');
      });
function FillGDDetail(Ex_Id,Status){
    if(Status=='0'){
$("#GDHead").html("Modify Non-Verified Group Guidance");
$("#ExchangeddlVisible").hide()
    }
    if(Status=='1'){
$("#GDHead").html("Modify Verified Group Guidance");
$("#ExchangeddlVisible").show();
    }
    MasterData ={


     "p_GrpDiscuss_ID":localStorage.GrpDiscuss_ID,
     "p_Ex_id":Ex_Id,
     "p_GrpDiscussTopic":"0",
     "p_Venue":"0",
     "p_FromDate":"0000-00-00",
     "p_ToDate":"0000-00-00",
     "p_GrpDiscussDetail": "0",
     "p_AttachFileName":"0",
     "p_Status":Status,
     "p_VerifiedBy":"0",
     "p_EnteredBy":"0",
     "p_RejectReason":"0",
     "p_Active_YN":"0",
     "p_Client_IP":"0",
     "p_LMBY":"0",
     "p_Flag":"15"
     
     };
 MasterData = JSON.stringify(MasterData)
 var path = serverpath + "secured/GroupDiscussion";
 securedajaxpost(path, 'parsrdataitFillGDDetail', 'comment', MasterData, 'control')
 }
 function parsrdataitFillGDDetail(data) {
     data = JSON.parse(data)
     if (data.message == "New token generated"){
             sessionStorage.setItem("token", data.data.token);
             FillGDDetail(jQuery('#GDexchange').val());
     }
     else{
         var data1 = data[0];
         var appenddata="";
         var active="";
         for (var i = 0; i < data1.length; i++) {
        if(data1[i].Active_YN=='0'){
 active="N"
        }
        else if(data1[i].Active_YN=='1'){
         active="Y"
                }
                if(data1[i].Status=='0'){
                var status='Not Verified'
                var  disabled=false
                var  VerifiedBy="-"
                var VerifyingDt='-'
                 
                }
                else if(data1[i].Status=='1'){
                var status='Verified';
                var disabled=true;
                var  VerifiedBy=data1[i].Verify_By
                var VerifyingDt=data1[i].VerifyingDt
                 
                }
                else if(data1[i].Status=='2'){
                 var   status='Reject'
                 var disabled=true
                 var  VerifiedBy="-"
                 var VerifyingDt='-'
                }
                
    appenddata += "<tr><td>"+data1[i].GrpDiscuss_ID+"</td><td style='    word-break: break-word;'><a href='#' onclick=GDDetails('"+data1[i].GrpDiscuss_Id+"','"+encodeURI(data1[i].Ex_name)+"','"+encodeURI(data1[i].GrpDiscussTopic)+"','"+encodeURI(data1[i].GrpDiscussDetail)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].FromDate)+"','"+encodeURI(data1[i].ToDate)+"','"+encodeURI(data1[i].Active_YN)+"')>" + data1[i].GrpDiscussTopic+ "</a></td><td style='    word-break: break-word;'>"+data1[i].Venue+"</td><td >" + data1[i].FromDate+ "</td><td>"+data1[i].ToDate+"</td><td>"+data1[i].Ex_name+"</td><td>"+status+"</td><td>"+VerifiedBy+"</td><td>"+VerifyingDt+"</td><td>"+active+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].GrpDiscuss_ID+"','"+encodeURI(data1[i].Ex_id)+"','"+encodeURI(data1[i].GrpDiscussTopic)+"','"+encodeURI(data1[i].GrpDiscussDetail)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].FromDate)+"','"+encodeURI(data1[i].ToDate)+"','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].Status)+"','"+encodeURI(data1[i].RejectReason)+"')>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=Delete("+data1[i].GrpDiscuss_Id+",'GDDetail') id='btnDelete'>Delete</button></tr>";
     }jQuery("#tbodyvalue").html(appenddata);  
 }  
  
 }
 function GDDetails(GD_Id,Ex_name,GD_Title,GD_Details,Venue,GD_FromDt_c,GD_ToDt,Active_YN){
    $('#GDdetailsById').show();
    $('#SearchGD').hide();
    $('#ExchangeById').html(decodeURI(Ex_name))
    $('#TitleById').html(decodeURI(GD_Title))
    $('#DetailById').html(decodeURI(GD_Details))
    $('#VenueById').html(decodeURI(Venue))
    $('#FrmdtById').html(decodeURI(GD_FromDt_c))
    $('#ToDtById').html(decodeURI(GD_ToDt))
    if(decodeURI(Active_YN)=='0'){
        $("#chkActiveStatusbyId")[0].checked=false;
        }
        else{	
            $("#chkActiveStatusbyId")[0].checked=true;
        }
    
            }
            function editMode(GrpDiscuss_ID,Ex_id,GD_Title,GD_Details,Venue,GD_FromDt,GD_ToDt,Active_YN,Status,Reason)
            {
                $("#GDStatus").val(Status);
                if(decodeURI(Status)=='0' || decodeURI(Status)=='1'){
                    $("#GDReason").val("");
                    $("#GDReason").attr('disabled',true)
                }
                if(decodeURI(Status)=='2'){
                    $("#GDReason").val(Reason)
                    $("#GDReason").attr('disabled',true)
                }
                $('#modifyGDDetails').show()
                localStorage.GrpDiscuss_ID=GrpDiscuss_ID,
            $("#AddGDexchange").val(Ex_id),
            $("#GDTitle").val(decodeURI(GD_Title)),
            $("#GDVenue").val(decodeURI(Venue)),
            
            $("#m_datepicker_5").val(decodeURI(GD_FromDt));
            $("#m_datepicker_6").val(decodeURI(GD_ToDt));
            
            $("#GDDetail").val(decodeURI(GD_Details))
            if(decodeURI(Active_YN)=='0'){
                $("#chkActiveStatus")[0].checked=false;
                }
                else{	
                    $("#chkActiveStatus")[0].checked=true;
                }
        
            }
            function CheckValidation(){
                if($("#GDTitle").val()==""){
                    getvalidated('GDTitle','text','Title');
                    return false;
                }
                if($("#GDVenue").val()==""){
                    getvalidated('GDVenue','text','Venue')
                    return false;
                }
                if($("#m_datepicker_5").val()==""){
                    getvalidated('m_datepicker_5','text','From Date');
                    return false;
                }
                if($("#m_datepicker_6").val()==""){
                    getvalidated('m_datepicker_6','text','To Date')
                    return false;
                }
                else{
                    InsUpdGDDetail();
                }
                
                }
            function InsUpdGDDetail(){
                var chkval = $("#chkActiveStatus")[0].checked
                if (chkval == true){
                    chkval = "1"
                }else{
                    chkval="0"
                }
                if($("#GDStatus").val()=='1'){
                    verifyby=sessionStorage.CandidateId
                }
                else{
                    verifyby='0'
                }
                var Jobfair_FromDt = jQuery("#m_datepicker_5").val().split(" ")
                var Jobfair_FromDt1 = Jobfair_FromDt[0].split("/")
                var FromDate = Jobfair_FromDt1[2] + "-" + Jobfair_FromDt1[0] + "-" + Jobfair_FromDt1[1] ;
            
                var Jobfair_ToDt = jQuery("#m_datepicker_6").val().split(" ")
                var Jobfair_ToDt1 = Jobfair_ToDt[0].split("/")
                var ToDate = Jobfair_ToDt1[2] + "-" + Jobfair_ToDt1[0] + "-" + Jobfair_ToDt1[1] ;
               
            var MasterData ={
                "p_GrpDiscuss_ID":localStorage.GrpDiscuss_ID,
                "p_Ex_id":$("#AddGDexchange").val(),
                "p_GrpDiscussTopic":$("#GDTitle").val(),
                "p_Venue":$("#GDVenue").val(),
                "p_FromDate":FromDate,
                "p_ToDate":ToDate,
                "p_GrpDiscussDetail":$("#GDDetail").val(),
                "p_AttachFileName":"0",
                "p_Status":$("#GDStatus").val(),
                "p_VerifiedBy":'258',
                "p_EnteredBy":"0",
                "p_RejectReason":$("#GDReason").val(),
                "p_Active_YN":chkval,
                "p_Client_IP":sessionStorage.IpAddress,
                "p_LMBY":sessionStorage.CandidateId,
                "p_Flag":'11'
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "secured/GroupDiscussion";
            securedajaxpost(path, 'parsrdataitInsUpdGDDetail', 'comment', MasterData, 'control')
            }
            function parsrdataitInsUpdGDDetail(data) {
                data = JSON.parse(data)
                if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        InsUpdGDDetail();
                }
                else{
                    resetmode()
                   if(data){
                    FillGDDetail('0','0');
                    toastr.success("Submit Successful", "", "success")
                    return true;
                }
            }
            }
            function resetmode(){
                localStorage.CCId='0',
                $('#modifyGDDetails').hide();
                $("#AddGDexchange").val("100"),
                $("#GDTitle").val(""),
                $("#GDVenue").val(""),
                $("#chkActiveStatus")[0].checked=false;
                $("#m_datepicker_5").val("");
                $("#m_datepicker_6").val("");
                $("#GDReason").val("")
                $("#GDDetail").val("");
                $("#GDStatus").val("");
            
                }