function FillHCCategory(funct,control) {
    var path =  serverpath + "secured/HCCategory/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillHCCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillHCCategory('parsedatasecuredFillHCCategory',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_Category_id).html(data1[i].HC_Description));
             }
             
        }
              
}


function InsUpdHCCategory(){

	var chkval = $("#Activecheck")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_HC_SubCategory_id":localStorage.p_HC_SubCategory_id,
    "p_HC_Category_id":(jQuery("#HCCategory").val()),
    "p_HC_Category_Description":jQuery("#HCDiscription").val(),
    "p_LMBY":0,
    "p_Client_IP":0,
    "p_Active_YN":chkval


};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "HCsubCategory";
securedajaxpost(path, 'parsrdataHCCategory', 'comment', MasterData, 'control')
}

function parsrdataHCCategory(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdHCCategory();
	}
	else if (data[0][0].ReturnValue == "1") {
        FetchHCCategory();
        resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_CityId = "0";
    $("#Activecheck")[0].checked = false;
        jQuery("#txturl").val("")
        $("#ddlDistrict").val("0");
    }
function FetchHCCategory() {
	var path = serverpath + "HCsubCategory/0/0"
	ajaxget(path, 'parsedataFetchHCCategory', 'comment', "control");
}
function parsedataFetchHCCategory(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
		 	active = "N"
	 }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].HC_Category_id + "</td><td style='    word-break: break-word;'>" + data1[i].HC_Category_Description + "</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button' onclick=EditMode('"+encodeURI(data1[i].HC_Category_Description)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);

}
function EditMode(HC_SubCategory_id,HC_Category_id,HC_Category_Description,Active_YN) {
    jQuery("#HCDiscription").val(decodeURI(HC_Category_Description))
    jQuery("#HCCategory").val(decodeURI(HC_Category_id))
    localStorage.p_HC_SubCategory_id =HC_SubCategory_id
    if (decodeURI(Active_YN) == '0') {
		$("#Activecheck")[0].checked = false;
	}
	else {
		$("#Activecheck")[0].checked = true;
    }
}
function CheckValidationHCCategory(){
    if(jQuery("#HCCategory").val()=="0"){
        getvalidated('HCCategory','select','HC Category');
        return false;
    }
  else  if($.trim(jQuery("#HCDiscription").val())==""){
        getvalidated('HCDiscription','text','HC Category Discription');
        return false;
    }
   
    else{
        InsUpdHCCategory();
    }
}
// $(function Alphabet() {
        
//     $('#txturl').keydown(function(e) {
//       if (e.shiftKey || e.ctrlKey || e.altKey) {
//         e.preventDefault();
//       } else {
//         var key = e.keyCode;
//         if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
//           e.preventDefault();
//         }
//       }
//     });
//   });