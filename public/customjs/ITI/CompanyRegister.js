function InsUpdCompanyRegister() {

    var MasterData = {

        "p_JobfairEmpRegId": sessionStorage.JobfairEmpRegId,
        "p_JobfairId": '0',
        "p_Employer": $.trim(jQuery("#CompanyName").val()), 
        "p_Company_HR_Name": $.trim(jQuery("#hrname").val()), 
        "p_Designation": jQuery("#JobDesignation").val(),
        "p_Qualification": jQuery("#Qualification").val(),
        "p_Location": jQuery("#JobLocation").val(),
        "p_Trade": $.trim(jQuery("#Trade").val()),
        "p_MinAge": jQuery("#MinAge").val(),
        "p_MaxAge": jQuery("#MaxAge").val(),
        "p_TotalVacancy": jQuery("#NoofVacancy").val(),
        "p_Min_Salary": jQuery("#Min").val(),
        "p_Max_Salary": jQuery("#Max").val(),
        "p_AttachDoc": sessionStorage.CompanyDoc,
        "p_InsertedBy": sessionStorage.CandidateId,
        "p_gender": jQuery("#Gender").val(),
        "p_e_mail": jQuery("#EmailId").val(),
        "p_mobile_no": jQuery("#MobileNumber").val(),
        "p_Discription":$('#Description').val(),
        "p_Percentage":$("#Percentage").val()
  };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "jobfairempreg";
    ajaxpost(path, 'parsedataCompanyRegister', 'comment', MasterData, 'control')
}

function parsedataCompanyRegister(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdCompanyRegister();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetModes()
        redirect()
     toastr.success("Insert Successful", "", "success")
    //  window.location='/ITI/JobfairEmployerMapping'
        return true;
    }

    else if (data[0][0].ReturnValue == "2") {
        resetModes()
        redirect()
     toastr.success("Update Successful", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "0") {
        resetModes()
  
     toastr.success("Already exist", "", "success")
        return true;
    }
 
}


function CheckValidationRegistration(){
    $(".demo-default").css('border-color', '');
        $("#validQualification").html("");
    
    if(isValidation){
    if($.trim(jQuery("#CompanyName").val())==""){
          getvalidated('CompanyName','text','Company Name');
          return false;
      }

      if(jQuery("#hrname").val()==""){
        getvalidated('hrname','text','Company HR Name');
        return false;
    }

    if (jQuery('#JobLocation').val() == "") {
        $(".demo-default").css('border-color', 'red');
        $("#validJobLocation").html("Please select Job Location");
        //getvalidated('JobLocation','text','Job Location');
        return false; 
    }


    if(jQuery("#JobDesignation").val()==""){
        $(".demo-default").css('border-color', 'red');
        $("#validJobDesignation").html("Please select Job Designation");
        //getvalidated('JobDesignation','text','Job Designation');
        return false;
    }
    if(jQuery("#Min").val()==""){
        getvalidated('Min','text','Min Salary');
        return false;
    }

if(jQuery("#Max").val()==""){
    getvalidated('Max','text','Max Salary');
    return false;
}
    if(jQuery("#MinAge").val()==""){
        getvalidated('MinAge','text','Min Age');
        return false;
    }
    if(jQuery("#MaxAge").val()==""){
        getvalidated('MaxAge','text','Max Age');
        return false;
    }
    if(jQuery("#Qualification").val()==""){
        $(".demo-default").css('border-color', 'red');
        $("#validQualification").html("Please select Qualification");
        getvalidated('Qualification','text','Qualification');
        return false;
    }
  
      
  
    if($('#Description').val()==""){
        getvalidated('Description','text','Description');
        return false;
    }
    if(jQuery("#NoofVacancy").val()==""){
        getvalidated('NoofVacancy','text','No of Vacancy');
        return false;
    }
    if(jQuery("#MobileNumber").val()==""){
        getvalidated('MobileNumber','text','Mobile Number');
        return false;
    }

if(jQuery("#EmailId").val()==""){
    getvalidated('EmailId','text','E-mail Id');
    return false;
}
    if(jQuery("#Gender").val()==""){
        getvalidated('Gender','text','Gender');
        return false;
    }
    if($.trim(jQuery("#Trade").val())==""){
        getvalidated('Trade','text','Trade');
        return false;
    }
    else{
        InsUpdCompanyRegister()
            
      }
  }
      else{
       $('#postattach').click();
            
      }
  }

  function resetModes() {
   
$('.form-control').val('')
        jQuery("#CompanyName").val("")
        jQuery("#hrname").val("")
        $("#JobLocation").data("");
         $("#JobDesignation").data("");
         $("#Min").val("");
         $("#Max").val("");
        $("#MinAge").val("");
         $("#MaxAge").val("");
         jQuery("#Qualification").data("")
         $("#NoofVacancy").val("");
          $("#Trade").val("");
          $("#EmailId").val("");
          $("#Gender").val("");
          $("#MobileNumber").val("");
    }
    // }
function FillDesignation() {
    var path = serverpath + "adminDesignation/0/0"
    securedajaxget(path, 'parsedataFillDesignation', 'comment', 'control');
}
function parsedataFillDesignation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillDesignation();
    }
    else {
        var data1 = data[0];
        $('#JobDesignation').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Designation',
            labelField: 'Designation',
            searchField: 'Designation',
            options: data1,
            create: true
        });
    }
}
function FillLocation() {
    var path = serverpath + "district/19/0/0/0"
    securedajaxget(path, 'parsedataFillLocation', 'comment', 'control');
}
function parsedataFillLocation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillLocation();
    }
    else {
        var data1 = data[0];
        $('#JobLocation').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'DistrictName',
            labelField: 'DistrictName',
            searchField: 'DistrictName',
            options: data1,
            create: true
        });
    }
}

function FillQualification() {
    var path = serverpath + "qualification/0/0/0"
    securedajaxget(path, 'parsedataFillQualification', 'comment', 'control');
}
function parsedataFillQualification(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillQualification();
    }
    else {
        var data1 = data[0];
        $('#Qualification').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Qualif_name',
            labelField: 'Qualif_name',
            searchField: 'Qualif_name',
            options: data1,
            create: true
        });
    }
}


function Fetchemployerdetail() {
	var path = serverpath + "jobfairEmpRegbyId/"+sessionStorage.JobfairEmpRegId+""
	ajaxget(path, 'parsedataFetchemployerdetail', 'comment', "control");
}
function parsedataFetchemployerdetail(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	
	//  for (var i = 0; i < data1.length; i++) {
	
	// 	appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Employer+ "</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' data-toggle='modal' data-target='#myModal1'  onclick=editMode('"+data1[i].Employer+"','"+encodeURI(data1[i].Location)+"','"+encodeURI(data1[i].Designation)+"','"+encodeURI(data1[i].Qualification)+"','"+encodeURI(data1[i].Discription)+"','"+encodeURI(data1[i].Percentage)+"','"+encodeURI(data1[i].mobile_no)+"','"+encodeURI(data1[i].e_mail)+"','"+encodeURI(data1[i].MinAge)+"','"+encodeURI(data1[i].MaxAge)+"','"+encodeURI(data1[i].Min_Salary)+"','"+encodeURI(data1[i].Max_Salary)+"','"+encodeURI(data1[i].TotalVacancy)+"','"+encodeURI(data1[i].Trade)+"','"+encodeURI(data1[i].gender)+"');>Modify</button></td></td><td><input type='checkbox'  value="+data1[i].JobfairEmpRegId+"></td></tr>";
    // }
    $('#fileuploaddiv').hide()
    $("#CompanyName").val(data1[0].Employer),
    $("#hrname").val(data1[0].Company_HR_Name),
     sessionStorage.setItem('JobfairEmpRegId',data1[0].JobfairEmpRegId)

    //$("#JobLocation").val(data1[0].Location),

    $('#JobLocation').selectize({
        valueField: data1[0].Location,
        labelField: data1[0].Location
    });
    var Location = data1[0].Location;
    var LocationArr = Location.split(",");
    var $select4 = $("#JobLocation").selectize();
    var selectize4 = $select4[0].selectize;
    selectize4.setValue(LocationArr, true);


    //$("#JobDesignation").val(data1[0].Designation),

    $('#JobDesignation').selectize({
        valueField: data1[0].Designation,
        labelField: data1[0].Designation
    });
    var Designation = data1[0].Designation;
    var DesignationArr = Designation.split(",");
    var $select2 = $("#JobDesignation").selectize();
    var selectize2 = $select2[0].selectize;
    selectize2.setValue(DesignationArr, true);


    
    //$("#Qualification").val(data1[0].Qualification);


    $('#Qualification').selectize({
        valueField: data1[0].Qualification,
        labelField: data1[0].Qualification
    });
    var Qualification = data1[0].Qualification;
    var QualificationArr = Qualification.split(",");
    var $select1 = $("#Qualification").selectize();
    var selectize1 = $select1[0].selectize;
    selectize1.setValue(QualificationArr, true);


    $("#Description").val(data1[0].Discription);
    
    $("#Percentage").val(data1[0].Percentage);
    $("#MobileNumber").val(data1[0].mobile_no);

    $("#EmailId").val(data1[0].e_mail),
    $("#MinAge").val(data1[0].MinAge),
    $("#MaxAge").val(data1[0].MaxAge),
    
    $("#Min").val(data1[0].Min_Salary);
    $("#Max").val(data1[0].Max_Salary);
    
    $("#NoofVacancy").val(data1[0].TotalVacancy);  
    //$("#Trade").val(data1[0].Trade);

    $('#Trade').selectize({
        valueField: data1[0].Trade,
        labelField: data1[0].Trade
    });
    var Trade = data1[0].Trade;
    var TradeArr = Trade.split(",");
    var $select3 = $("#Trade").selectize();
    var selectize3 = $select3[0].selectize;
    selectize3.setValue(TradeArr, true);

    $("#Gender").val(data1[0].gender);

    sessionStorage.setItem('type','')
     }
// }


$('#companyupload').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function(response) {
            if (response == "Error No File Selected For GST Upload"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Request Entity Too Large"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!"){
                toastr.warning(response, "", "info")
            }
            else{
                var str = response;
                console.log(str);
                var res = str.split("!");
                sessionStorage.setItem("CompanyDoc", res[1])
                InsUpdCompanyRegister()
              
            }
        }
    });
    return false;
 });


// function editMode(Employer,Location,Designation,Qualification,Discription,Percentage,mobile_no,e_mail,MinAge,MaxAge,Min_Salary,Max_Salary,TotalVacancy,Trade,gender)
//     {

//     $("#CompanyName").html(Employer),
//     $("#JobLocation").html(decodeURI(Location)),
//     $("#JobDesignation").html(decodeURI(Designation)),
    
//     $("#Qualification").html(decodeURI(Qualification));
//     $("#Description").html(decodeURI(Discription));
    
//     $("#Percentage").html(decodeURI(Percentage))
//     $("#MobileNumber").html(decodeURI(mobile_no))

//     $("#EmailId").html(e_mail),
//     $("#MinAge").html(decodeURI(MinAge)),
//     $("#MaxAge").html(decodeURI(MaxAge)),
    
//     $("#Min").html(decodeURI(Min_Salary));
//     $("#Max").html(decodeURI(Max_Salary));hh 
    
//     $("#NoofVacancy").html(decodeURI(TotalVacancy));  
//     $("#Trade").html(decodeURI(Trade));
//     $("#Gender").html(gender); 


//     }

function redirect(){
    // sessionStorage.JFEmpRegId=JobfairEmpRegId

    
    if(sessionStorage.getItem('PageName','JobfairEmployerMapping')){
    window.location='/ITI/JobfairEmployerMapping'

    sessionStorage.setItem('PageName','1')

}


}

function FillTrades() {
    var path = serverpath + "trademaster"
    securedajaxget(path, 'parsedataFillTrades', 'comment', 'control');
}
function parsedataFillTrades(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillTrades();
    }
    else {
        var data1 = data[0];
        $('#Trade').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'TradeName',
            labelField: 'TradeName',
            searchField: 'TradeName',
            options: data1,
            create: true
        });
    }
}


function goBack() {
    window.history.back()
  } 

  function percentageCheck(){

    if  ($("#Percentage").val() < 35){
         $("#validPercentage").html("Percentage cannot be less than 35")
         //$("#Percentage").val("")
         $("#Percentage").focus()
     }
 
     
    else if ($("#Percentage").val() > 100){
          $("#validPercentage").html("Percentage cannot be greater than 100")
          //$("#Percentage").val("")
          //$("#validPercentage").val("")
          $("#Percentage").focus()
        }
 
 
 
       else{
         $("#validPercentage").html("")
        
      }
     }

     function CheckAgelength(){

        if ($("#MinAge").val() < '14'){
            $("#validMinAge").html("Age cannot be less than 14")
            $("#MinAge").focus()
        } 
       else if ($("#MaxAge").val() > '65'){
             $("#validMaxAge").html("Age cannot be greater than 65")
             $("#validMaxAge").val("")
             $("#MaxAge").focus()
           }
    
      else  if ($("#minAge").val() > $("#maxAge").val()){
            $("#validmaxAge").html("Age cannot be less than " +$("#minAge").val())
            $("#maxAge").val("")
        }
          else{
            $("#validmaxAge").html("")
           
         }
        }

        function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
    
    
