jQuery('#ddlExchangeName').on('change', function () {
    FillJobFairDetail(jQuery('#ddlExchangeName').val());
      });


     function FillJobFairDetail(Ex_Id){
    //$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
     var path =  serverpath + "itijfexchange/"+Ex_Id+""
        securedajaxget(path,'parsedatasecuredFillJobFairDetail','comment',"control");
    }
    function parsedatasecuredFillJobFairDetail(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillJobFairDetail(jQuery('#ddlExchangeName').val());
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
                var active="";
                for (var i = 0; i < data1.length; i++) {
               if(data1[i].Active_YN=='0'){
        active="N"
               }
               else if(data1[i].Active_YN=='1'){
                active="Y"
                       }
                       if(data1[i].Status=='0'){
                        status='Not Verified'
                      disabled=false
                        
                       }
                       else if(data1[i].Status=='1'){
                        status='Verified';
                        disabled=true
                        
                       }
                       else if(data1[i].Status=='2'){
                        status='Reject'
                        disabled=true
                       
                       }
                       $('#JFTrade').selectize({
                        valueField: data1[i].Trade,
                        labelField: data1[i].Trade
                    });
                    if(data1[i].Trade==null){
                        var TradeArr = data1[i].Trade;
                    }
                    else{
                    var Trade = data1[i].Trade;
                    var TradeArr = Trade.split(",");
                    }
                    var $select = $("#JFTrade").selectize();
                    var selectize = $select[0].selectize;
                    selectize.setValue(TradeArr, true);


           appenddata += "<tr><td >" + data1[i].Jobfair_Id+ "</td><td><a href='#' onclick=JFDetails('"+data1[i].Jobfair_Id+"','"+encodeURI(data1[i].Institute_Name)+"','"+encodeURI(data1[i].Jobfair_Title)+"','"+encodeURI(data1[i].Jobfair_Details)+"','"+encodeURI(data1[i]["Venue"])+"','"+encodeURI(data1[i].Jobfair_FromDt)+"','"+encodeURI(data1[i].Jobfair_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"')>" + data1[i].Jobfair_Title+ "</a></td><td style='    word-break: break-word;'>"+data1[i].Venue+"</td><td >" + data1[i].Jobfair_FromDt+ "</td><td>"+data1[i].Jobfair_ToDt+"</td><td>"+active+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].Jobfair_Id+"','"+encodeURI(data1[i].Zone)+"','"+encodeURI(data1[i].District)+"','"+encodeURI(data1[i].Institute_Name)+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].Jobfair_Title)+"','"+encodeURI(data1[i].Jobfair_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].Jobfair_FromDt)+"','"+encodeURI(data1[i].Jobfair_ToDt)+"','"+encodeURI(data1[i].percentage_req)+"','"+encodeURI(data1[i].Trade)+"','"+encodeURI(data1[i].passingoutyearfrom)+"','"+encodeURI(data1[i].passingoutyearto)+"','"+encodeURI(data1[i].Gender)+"','"+encodeURI(data1[i].District)+"','"+encodeURI(data1[i].Institute_Name)+"','"+encodeURI(data1[i].Active_YN)+"')>Modify</button></td></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
        //onclick=Delete("+data1[i].Jobfair_Id+",'JFDetail')        
    }
    function setJfID(id,ex_id){
        localStorage.JFId=id;
        localStorage.Ex_Id=ex_id;
        localStorage.JFtype='ajf'
        
    }
    
    function CheckValidation(){
      if(isValidation){
       
        if($("#Zonedropdown").val()=="0"){
            getvalidated('Zonedropdown','select','Zone')
            return false;
            }

            if($("#addjobfairexchange").val()=="0"){
                getvalidated('addjobfairexchange','select','ITI Name')
                return false;
                }

        if($.trim($("#JFTitle").val())==""){
        getvalidated('JFTitle','text','Title');
        return false;
        }

        if($.trim($("#JFDetail").val())==""){
            getvalidated('JFDetail','text','Detail')
            return false;
            }
        if($.trim($("#JFVenue").val())==""){
        getvalidated('JFVenue','text','Venue')
        return false;
        }
        if($("#m_datepicker_5").val()==""){
        getvalidated('m_datepicker_5','text','From Date');
        return false;
        }
        if($("#m_datepicker_6").val()==""){
        getvalidated('m_datepicker_6','text','To Date')
        return false;
        }
        if($.trim($("#Percentage").val())==""){
            getvalidated('Percentage','text','Percentage Required')
            return false;
            }

            if($.trim($("#FromYear").val())==""){
                getvalidated('FromYear','text','From')
                return false;
                }

                if($.trim($("#ToYear").val())==""){
                    getvalidated('ToYear','text','To')
                    return false;
                    }

        if($("#Gender").val()==""){
            getvalidated('Gender','text','Gender')
            return false;
            }

            if($("#District").val()==""){
                getvalidated('District','select','District')
                return false;
                }
                else{
                    sessionStorage.ModalType='JobfairFile'
                    sessionStorage.JobfairTitle=$.trim($("#JFTitle").val())
                    sessionStorage.JobfairVenue=$.trim($("#JFVenue").val())
                    sessionStorage.Jobfair_FromDate=$("#m_datepicker_5").val();
                    sessionStorage.Jobfair_ToDate=$("#m_datepicker_6").val();
                    sessionStorage.Trade=$.trim($("#JFTrade").val());
                    sessionStorage.Exchange=$("#addjobfairexchange option:selected").val();
                    sessionStorage.Percentage=$.trim($("#Percentage").val());
                    sessionStorage.FromYear=$("#FromYear").val()
                    sessionStorage.ToYear=$("#ToYear").val()
                    sessionStorage.Gender=$("#Gender").val()
                    sessionStorage.Zone=$("#Zone").val()
                    sessionStorage.District=$("#District").val()
                    sessionStorage.JFDetail=$("#JFDetail").val()
                    var chkval = $("#chkActiveStatus")[0].checked
                    if (chkval == true){
                    sessionStorage.chkval = "1"
                    }else{
                    sessionStorage.chkval="0"
                    }
                   // $("#postattach").click();
                    
                     InsUpdJobFairDetail()
                    }
                    
                    }
            
            
            
             else{
        sessionStorage.ModalType='JobfairFile'
        sessionStorage.JobfairTitle=$.trim($("#JFTitle").val())
        sessionStorage.JobfairVenue=$.trim($("#JFVenue").val())
        sessionStorage.Jobfair_FromDate=$("#m_datepicker_5").val();
        sessionStorage.Jobfair_ToDate=$("#m_datepicker_6").val();
        sessionStorage.Trade=$.trim($("#JFTrade").val());
        sessionStorage.Exchange=$("#addjobfairexchange option:selected").val();
        sessionStorage.Percentage=$.trim($("#Percentage").val());
        sessionStorage.FromYear=$("#FromYear").val()
        sessionStorage.ToYear=$("#ToYear").val()
        sessionStorage.Gender=$("#Gender").val()
        sessionStorage.Zone=$("#Zonedropdown").val()
        sessionStorage.District=$("#District").val()
        sessionStorage.JFDetail=$("#JFDetail").val()
        var chkval = $("#chkActiveStatus")[0].checked
        if (chkval == true){
        sessionStorage.chkval = "1"
        }else{
        sessionStorage.chkval="0"
        }
       // $("#postattach").click();
        
         InsUpdJobFairDetail()
        }
        
        }






function InsUpdJobFairDetail(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
    }
    var Jobfair_FromDt = sessionStorage.Jobfair_FromDate.split(" ")
    var Jobfair_FromDt1 = Jobfair_FromDt[0].split("/")
    var Jobfair_FromDate = Jobfair_FromDt1[2] + "-" + Jobfair_FromDt1[1] + "-" + Jobfair_FromDt1[0] ;

    var Jobfair_ToDt =sessionStorage.Jobfair_ToDate.split(" ")
    var Jobfair_ToDt1 = Jobfair_ToDt[0].split("/")
    var Jobfair_ToDate = Jobfair_ToDt1[2] + "-" + Jobfair_ToDt1[1] + "-" + Jobfair_ToDt1[0] ;
   
var MasterData ={
    
    "p_Jobfair_Id":localStorage.JFId,
	"p_Ex_id":sessionStorage.Exchange,
	"p_Jobfair_Title":sessionStorage.JobfairTitle,
	"p_Venue":sessionStorage.JobfairVenue,
	"p_Jobfair_Details":sessionStorage.JFDetail,
	"p_Attachment_Filename":$("#JobfairFile").text(),
	"p_Date_of_Entry": "0",
	"p_Jobfair_FromDt":Jobfair_FromDate,
	"p_Jobfair_ToDt":Jobfair_ToDate,
	"p_Jobfair_CreatedBy":sessionStorage.CandidateId,
	"p_Status":"0",
	"p_Verified_By":"",
	"p_Verified_Dt":"",
	"p_Reject_Reason":"",
	"p_Active_YN":sessionStorage.chkval,
	"p_LMDT":"0",
    "p_LMBY":sessionStorage.CandidateId,
    "p_Trade":sessionStorage.Trade,
    "p_passingoutyearfrom":sessionStorage.FromYear,
    "passingoutyearto":sessionStorage.ToYear,
    "p_Gender":sessionStorage.Gender,
    "p_percentage_req":sessionStorage.Percentage,
    "p_Zone":sessionStorage.Zone,
    "p_District":sessionStorage.District
    
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "itijfexchange";
securedajaxpost(path, 'parsrdataitInsUpdJobFairDetail', 'comment', MasterData, 'control')
}
function parsrdataitInsUpdJobFairDetail(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdJobFairDetail();
	}
	else if (data[0][0].ReturnValue == "1") {
         resetmode()
        // FillJobFairDetail('5')
        toastr.success("Insert Successful", "", "success")
        sessionStorage.JobFairId=data[0][0].Jobfair_Id
        window.location='/ITI/JobfairEmployerMapping'
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
         resetmode()
         //FillJobFairDetail('5')
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
          resetmode()
          //FillJobFairDetail('5')
	toastr.warning("Already Exist", "", "info")
			return true;
	}
 
}
function resetmode(){
    localStorage.JFId='0',
    $("#Zonedropdown").val('0')
   $("#addjobfairexchange").val("5"),
    $("#JFTitle").val(""),
    $("#JFVenue").val(""),
    $("#chkActiveStatus")[0].checked=false;
    $("#m_datepicker_6").val("");
    $("#m_datepicker_5").val("");
    FillJobFairDetail('5');
    $("#JFDetail").val("")
    $("#JobfairFile").text("")
    sessionStorage.ModalType=''
    Cookies.set('modaltype', '', { expires: 1, path: '/'} )
    $("#Percentage").val('')
    $("#FromYear").val('')
    $("#ToYear").val('')
    $("#Gender").val('0')
    $("#District").val('0')
    $("#JFTrade").val('0')
    sessionStorage.JobfairTitle=''
    sessionStorage.JobfairVenue=''
    sessionStorage.Jobfair_FromDate=''
    sessionStorage.Jobfair_ToDate=''
    sessionStorage.Exchange=''
    sessionStorage.chkval=''
    sessionStorage.District=''
    sessionStorage.Zone=''
    sessionStorage.Percentage='';
     sessionStorage.FromYear=''
     sessionStorage.ToYear=''
     sessionStorage.Gender=''
     sessionStorage.Zone=''
     sessionStorage.District=''
     sessionStorage.Trade=''
     $("#JFTrade").val('');
    }
    function searchTextInTable(tblName) {
        var tbl;
        tbl = tblName;
        jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.
    
        var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value
    
        if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
        {
            jQuery("#" + tbl + " tr:has(td)").show();
            return false;
        }
        //Iterate through all the td.
        jQuery("#" + tbl + " tr:has(td)").children().each(function () {
            var cellText = jQuery(this).text().toLowerCase();
            if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
            {
                jQuery(this).parent().show();
                return true;
            }
        });
       
    }
    function editMode(Jobfair_Id,Zone,district,Institute_Name,Ex_id,Jobfair_Title,Jobfair_Details,Venue,Jobfair_FromDt,Jobfair_ToDt,percentage_req,Trade,passingoutyearfrom,passingoutyearto,gender,Active_YN)
    {
        $(".btn btn-success").scrollTop(0)
       
        $('#addjobdetails').show()
        localStorage.JFId=Jobfair_Id,
        $("#Zonedropdown").val(decodeURI(Zone));
        localStorage.setItem("editdistrict",district)
        localStorage.setItem("editzone",Zone)
        localStorage.setItem("editinstitute",Ex_id)
        FillDistrictedit();
      $("#addjobfairexchange").val(Ex_id);
    $("#JFTitle").val(decodeURI(Jobfair_Title)),
    $("#JFVenue").val(decodeURI(Venue)),
    
    $("#m_datepicker_5").val(decodeURI(Jobfair_FromDt));
    $("#m_datepicker_6").val(decodeURI(Jobfair_ToDt));
 $("#FromYear").val(decodeURI(passingoutyearfrom)),
    $("#ToYear").val(decodeURI(passingoutyearto)),
    
    $("#Gender").val(decodeURI(gender));
    $("#Percentage").val(decodeURI(percentage_req));
    
    
    $("#JFDetail").val(decodeURI(Jobfair_Details))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}

    }
    function JFDetails(CC_Id,Institute_Name,CC_Title,CC_Details,Venue,CC_FromDt_c,CC_ToDt,Active_YN){
        $('#JFdetailsById').show();
        $('#searchjobdetail').hide();
        $('#ExchangeById').html(decodeURI(Institute_Name))
        $('#TitleById').html(decodeURI(CC_Title))
        $('#DetailById').html(decodeURI(CC_Details))
        $('#VenueById').html(decodeURI(Venue))
        $('#FrmdtById').html(decodeURI(CC_FromDt_c))
        $('#ToDtById').html(decodeURI(CC_ToDt))
        if(decodeURI(Active_YN)=='0'){
            $("#chkActiveStatusbyId")[0].checked=false;
            }
            else{	
                $("#chkActiveStatusbyId")[0].checked=true;
            }
        
                }
      
               
    
                function FillSecuredexchangeoffice1(funct,control) {
                    var path =  serverpath +"iti_institute"
                    securedajaxget(path,funct,'comment',control);
                }
                
                function parsedatasecuredFillSecuredexchangeoffice1(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        FillSecuredexchangeoffice1('parsedatasecuredFillSecuredexchangeoffice1','addjobfairexchange');                     }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[0];
                            jQuery("#ddlExchangeName").append(jQuery("<option ></option>").val("0").html("Select ITI Name"));

                            for (var i = 0; i < data1.length; i++) {
                                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].id).html(data1[i].Institute_Name));
                             }
                            // jQuery("#"+control).val('5');
                        }
                              
                }
                
                function FillZone(funct,control) {
                    var path =  serverpath +"iti_institute"
                    securedajaxget(path,funct,'comment',control);
                }
                
                function parsedatasecuredFillZone(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        FillZone('parsedatasecuredFillZone',control);                     }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[1];
                            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('Select Zone'));
                            for (var i = 0; i < data1.length; i++) {
                                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ZoneName).html(data1[i].ZoneName));
                             }
                             jQuery("#"+control).val('0');
                        }
                              
                }

                

               


        function FillZonewise_District(funct,control) {
            var path =  serverpath + "ZonewiseDistrict/'"+$('#Zonedropdown').val()+"'"
            securedajaxget(path,funct,'comment',control);
        }
      
        
        function parsedatasecuredFillZonewiseDistrict(data,control){  
            data = JSON.parse(data)
           // console.log(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillZonewise_District('parsedatasecuredFillZonewiseDistrict','District')
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#District").empty();
                    var data1 = data[0];
                      jQuery("#District").append(jQuery("<option ></option>").val("0").html("Select District Name"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#District").append(jQuery("<option></option>").val(data1[i].District).html(data1[i].District));
                     }
                     
                }
                      
        }



        function FillItiinstituteName(funct,control) {
            var path =  serverpath + "ItiinstituteName/'"+$('#District').val()+"'"
            securedajaxget(path,funct,'comment',control);
        }
      
        
        function parsedatasecuredFillItiinstituteName(data,control){  
            data = JSON.parse(data)
            //console.log(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillItiinstituteName('parsedatasecuredFillItiinstituteName','addjobfairexchange')
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#addjobfairexchange").empty();
                    var data1 = data[0];
                      jQuery("#addjobfairexchange").append(jQuery("<option ></option>").val("0").html("Select ITI Name"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#addjobfairexchange").append(jQuery("<option></option>").val(data1[i].id).html(data1[i].Institute_Name));
                     }
                     
                }
                      
        }


        function FillDistrictedit() {
            var path =  serverpath + "ZonewiseDistrict/'"+localStorage.getItem("editzone")+"'"
            securedajaxget(path,'parsedatasecuredFillZonewiseDistrictedit','comment',"");
        }
      
        
        function parsedatasecuredFillZonewiseDistrictedit(data,control){  
            data = JSON.parse(data)
           // console.log(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDistrictedit()
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#District").empty();
                    var data1 = data[0];
                      jQuery("#District").append(jQuery("<option ></option>").val("0").html("Select District Name"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#District").append(jQuery("<option></option>").val(data1[i].District).html(data1[i].District));
                     }
                     jQuery("#District").val(localStorage.getItem("editdistrict"));
                     FillItiinstituteNameedit();
                }
                      
        }



        function FillItiinstituteNameedit() {
            var path =  serverpath + "ItiinstituteName/'"+localStorage.getItem("editdistrict")+"'"
            securedajaxget(path,'parsedatasecuredFillItiinstituteNameedit','comment',"");
        }
      
        
        function parsedatasecuredFillItiinstituteNameedit(data,control){  
            data = JSON.parse(data)
            //console.log(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillItiinstituteNameedit()
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#addjobfairexchange").empty();
                    var data1 = data[0];
                      jQuery("#addjobfairexchange").append(jQuery("<option ></option>").val("0").html("Select ITI Name"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#addjobfairexchange").append(jQuery("<option></option>").val(data1[i].id).html(data1[i].Institute_Name));
                     }
                     jQuery("#addjobfairexchange").val(localStorage.getItem("editinstitute"));
                }
                      
        }



        function FillTrade() {
            var path = serverpath + "trademaster"
            securedajaxget(path, 'parsedataFillTrade', 'comment', 'control');
        }
        function parsedataFillTrade(data, control) {
            data = JSON.parse(data)
            if (data.message == "New token generated") {
                sessionStorage.setItem("token", data.data.token);
                FillTrade();
            }
            else {
                var data1 = data[0];
                $('#JFTrade').selectize({
                    persist: false,
                    createOnBlur: true,
                    valueField: 'TradeName',
                    labelField: 'TradeName',
                   // searchField: 'TradeName',
                    options: data1,
                    create: true,
                    maxItems:10
                });
            }
        }


      
        
    function Checkpercentage(){

       if  ($("#Percentage").val() < 35){
            $("#validPercentage").html("Percentage cannot be less than 35")
            //$("#Percentage").val("")
            $("#Percentage").focus()
        }
    
        
       else if ($("#Percentage").val() > 100){
             $("#validPercentage").html("Percentage cannot be greater than 100")
             //$("#Percentage").val("")
             //$("#validPercentage").val("")
             $("#Percentage").focus()
           }
    
    
    
          else{
            $("#validPercentage").html("")
           
         }
        }
    
       

        function CheckYear(){

            if ($("#FromYear").val() < '2000'){
                $("#validFromYear").html("From Year cannot be less than 2000")
                //$("#minAge").val("")
                $("#FromYear").focus()
            }
        
            
           else if ($("#ToYear").val() > '2020'){
                 $("#validToYear").html("To Year cannot be greater than 2020")
                 $("#ToYear").val("")
                 $("#validToYear").val("")
                 $("#ToYear").focus()
               }
        
           
        
          else  if ($("#FromYear").val() > $("#ToYear").val()){
                $("#validToYear").html("ToYear cannot be less than " +$("#FromYear").val())
                $("#ToYear").val("")
            }
        
              else{
                $("#validToYear").html("")
               
             }
            }
        

        

            function FillDistrict(funct,control,District) {
                sessionStorage.setItem("District",District)
                var path =  serverpath + "ZonewiseDistrict/'"+$('#Zone').val()+"'"
                securedajaxget(path,funct,'comment',control);
            }
          
            
            function parsedatasecuredFillDistrict(data,control){ 
                 
                data = JSON.parse(data)
               // console.log(data)
                if (data.message == "New token generated"){
                    sessionStorage.setItem("token", data.data.token);
                    FillDistrict('parsedatasecuredFillDistrict',sessionStorage.getItem("District",District))
                }
                else if (data.status == 401){
                    toastr.warning("Unauthorized", "", "info")
                    return true;
                }
                    else{
                        jQuery("#District").empty();
                        var data1 = data[0];
                          jQuery("#District").append(jQuery("<option ></option>").val("0").html("Select District Name"));
                        for (var i = 0; i < data1.length; i++) {
                            jQuery("#District").append(jQuery("<option></option>").val(data1[i].District).html(data1[i].District));
                        }
                         jQuery("#District").val(sessionStorage.getItem("District",District)) 
                    }
                          

                     }
                     
                     function FillPassingoutYearfrom(funct,control) {
                        var path =  serverpath + "year/0/20"
                        securedajaxget(path,funct,'comment',control);
                    }
                    
                    function parsedatasecuredFillpassingoutYearfrom(data,control){  
                        data = JSON.parse(data)
                        if (data.message == "New token generated"){
                            sessionStorage.setItem("token", data.data.token);
                            FillPassingoutYearfrom('parsedatasecuredFillpassingoutYearfrom',control);
                        }
                        else if (data.status == 401){
                            toastr.warning("Unauthorized", "", "info")
                            return true;
                        }
                            else{
                                jQuery("#"+control).empty();
                                var data1 = data[0];
                                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("From"));
                                for (var i = 0; i < data1.length; i++) {
                                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                                 }
                    
                                
                            }
                                  
                    }

                    function FillPassingoutYearto(funct,control) {
                        var path =  serverpath + "year/0/"+control+""
                        securedajaxget(path,funct,'comment',control);
                    }
                    
                    function parsedatasecuredFillPassingoutYearto(data,control){  
                        data = JSON.parse(data)
                        if (data.message == "New token generated"){
                            sessionStorage.setItem("token", data.data.token);
                            FillPassingoutYearto('parsedatasecuredFillPassingoutYearto',control);
                        }
                        else if (data.status == 401){
                            toastr.warning("Unauthorized", "", "info")
                            return true;
                        }
                            else{
                                jQuery("#ToYear").empty();
                                var data1 = data[0];
                                jQuery("#ToYear").append(jQuery("<option ></option>").val("0").html("To"));
                                for (var i = 0; i < data1.length; i++) {
                                    jQuery("#ToYear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                                 }
                    
                                
                            }
                                  
                    }
                    function CheckPassingoutYear(){
                        var currentyear=new Date().getFullYear()
                        var startedyear=$("#FromYear").val()
                        var difference = currentyear - startedyear;
                        FillPassingoutYearto('parsedatasecuredFillPassingoutYearto',difference);
                    }
            
    
