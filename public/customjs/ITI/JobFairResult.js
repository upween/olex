function FillJobFairDDL(){
    
    var path =  serverpath + "itijfexchange/0"
    securedajaxget(path,'parsedatasecuredFillJobFairDDL','comment',"control");
}
function parsedatasecuredFillJobFairDDL(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        jQuery("#JobFair").empty();
        var data1 = data[0];
        jQuery("#JobFair").append(jQuery("<option></option>").val('0').html('Select Job Fair'));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#JobFair").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title+' - '+data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt +' at '+data1[i].Venue+''));
         }

        
    }      
}


function FillJFEmpNotReg(){
    var MasterData ={
        "p_JobFair_ID":jQuery("#JobFair").val(),         
        "p_Emp_Regno":"",         
        "p_Flag":'3',
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "EmpRegister";
      securedajaxpost(path, 'parsedatasecuredFillJFEmpNotReg', 'comment', MasterData, 'control')
      }
function parsedatasecuredFillJFEmpNotReg(data){  
    data = JSON.parse(data)
    console.log(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJFEmpReg();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            jQuery("#Employer").empty()
            jQuery("#Employer").append(jQuery("<option></option>").val('0').html('Select Employer'));
         
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#Employer").append(jQuery("<option></option>").val(data1[i].e_mail).html(data1[i].Employer));
                 }
    }    
              
}



function FillJFJsReg(){
    var path =  serverpath + "Interestedcandidatejf/"+jQuery("#JobFair").val()+""
    securedajaxget(path,'parsedatasecuredFillJFJsReg','comment',"control");
      }
function parsedatasecuredFillJFJsReg(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJFJsReg();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
         
            for (var i = 0; i < data1.length; i++) {
         
       appenddata += "<tr><td>" + [i+1]+ "</td><td>"+data1[i].jf_Id+"</td><td style='    word-break: break-word;'>"+data1[i].uniqueId+"</td><td style='    word-break: break-word;'>"+data1[i].CandidateName+"</td><td style='    word-break: break-word;'>"+data1[i].RegistrationId+"</td><td>"+data1[i].MobileNumber+"</td><td >" + data1[i].EmailId+ "</td><td></td><td></td><td></td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}


$('#reportupload').submit(function () {
    $(this).ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
           // console.log(response);
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Request Entity Too Large") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: PDF and Image File Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $('#modalresultJf').modal('toggle');
                var candidatelist = res[1];
                var candidatelist1 = JSON.parse(candidatelist);
             
                CandidateList(candidatelist1);
         
            }
        }
    });
    return false;
});

function checkAll(){
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').attr('checked', true);
    } else {
        $('input:checkbox').attr('checked', false);
    }
}

function CandidateList(candidatelist){
    
   var candidatelist1 = candidatelist;
   var Candidatelist=''
   for (var i = 0; i < candidatelist1.length; i++) {
      
        Candidatelist+="<tr><td>"+candidatelist1[i].candidatename +"</td><td>" + candidatelist1[i].mobileno +"</td><td>" + candidatelist1[i].email +"</td><tr>"

   FillCandidateList(encodeURI(candidatelist1[i].registratonnumber),encodeURI(candidatelist1[i].candidatename),encodeURI(candidatelist1[i].candidatejofairid),$("#JobFair").val(),$("#Employer option:selected").text(),encodeURI(candidatelist1[i].mobileno),encodeURI(candidatelist1[i].email),encodeURI(candidatelist1[i].selected))
  }
 sendemail("<table border=1>  <tr><th>Name</th><th>Mobile No.</th><th>E-mailId</th></tr>"+Candidatelist+"</table>")
}


function sendemail(Candidatelist){
 var sub = "MP Rojgar";
var body = `Dear Employer,<br>
 Thank you for attending Job Fair on Dated ${jQuery("#hiddenjf option:selected").text()}   We thank you for participation and please find below the list of selected candidates in that particular job fair <br>
<br>${Candidatelist}<br>
We request you to please inform their joining status and provide us their copy of offer letter and their 3 month salary slip.<br>Our representative will be in touch with you for coordination and serving you in future also.<br>
<div> Thank you for attending Job Fair.</div><br>
           
     Regards,<br>
     Name : ${sessionStorage.CandidateName}`;

sentmailglobal($('#Employer').val(),body,sub,'');
}



function FillJobFairDDLHidden(){
    var path =  serverpath + "itijfexchange/0"
    securedajaxget(path,'parsedatasecuredFillJobFairDDLHidden','comment',"control");
}
function parsedatasecuredFillJobFairDDLHidden(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        jQuery("#hiddenjf").empty();
        var data1 = data[0];
       
        for (var i = 0; i < data1.length; i++) {
            jQuery("#hiddenjf").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt +' at '+data1[i].Venue));
         }

        
    }      
}




function FillCandidateList(Reg_No,Cnadidate_Name,Unique_Id,JobFair_Id,Company,Mobile,Email,Selected){
    var MasterData ={
        "p_Id":'0',
        "p_Reg_No": decodeURI(Reg_No),     
        "p_Cnadidate_Name":decodeURI(Cnadidate_Name),         
        "p_Unique_Id":decodeURI(Unique_Id),
        "p_JobFair_Id":decodeURI(JobFair_Id),
        "p_Company":decodeURI(Company),
        "p_Mobile":decodeURI(Mobile),
        "p_Email":decodeURI(Email),
        "p_Selected":Selected
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "SelectedCandJobFair";
      securedajaxpost(path, 'parsedatasecuredFillCandidateList', 'comment', MasterData, 'control')
      }
function parsedatasecuredFillCandidateList(data){  
    data = JSON.parse(data)
    console.log(data)
 }




 function AddCandidate(){
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;
   }
    else {
        CandidateList();
}

}
