function FillJobFairDDL(){
    var path =  serverpath + "itijfexchange/0"
    securedajaxget(path,'parsedatasecuredFillJobFairDDL','comment',"control");
}
function parsedatasecuredFillJobFairDDL(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        jQuery("#JobFair").empty();
        var data1 = data[0];
        jQuery("#JobFair").append(jQuery("<option></option>").val('0').html('Select Job Fair'));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#JobFair").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title+' - '+data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt +' at '+data1[i].Venue+''));
         }

        
    }      
}


function FillJFJsReg(){
     var path =  serverpath + "Interestedcandidatejf/"+jQuery("#JobFair").val()+""
 //   var path =  serverpath + "secured/Interestedcandidate_JfEmp/"+jQuery("#JobFair").val()+"/"+jQuery("#Employer").val()+""
    securedajaxget(path,'parsedatasecuredFillJFJsReg','comment',"control");
      }
function parsedatasecuredFillJFJsReg(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJFJsReg();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
         
            for (var i = 0; i < data1.length; i++) {
         
       appenddata += "<tr><td>" + [i+1]+ "</td><td style='word-break: break-word;'>"+data1[i].uniqueId+"</td><td style='    word-break: break-word;'>"+data1[i].CandidateName+"</td><td style='    word-break: break-word;'>"+data1[i].RegistrationId+"</td><td>"+data1[i].MobileNumber+"</td><td >" + data1[i].EmailId+ "</td><td><input type='checkbox' name='checkbox' value='"+data1[i].RegistrationId+","+ data1[i].CandidateName+" ,"+data1[i].uniqueId+","+data1[i].MobileNumber+" , "+data1[i].EmailId+"'  ></td></tr>";
      

        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}
function FillJFEmpNotReg(){
    var MasterData ={
        "p_JobFair_ID":jQuery("#JobFair").val(),         
        "p_Emp_Regno":"",         
        "p_Flag":'3',
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "EmpRegister";
      securedajaxpost(path, 'parsedatasecuredFillJFEmpNotReg', 'comment', MasterData, 'control')
      }
function parsedatasecuredFillJFEmpNotReg(data){  
    data = JSON.parse(data)
    console.log(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJFEmpReg();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            jQuery("#Employer").empty()
            jQuery("#Employer").append(jQuery("<option></option>").val('0').html('Select Employer'));
         
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#Employer").append(jQuery("<option></option>").val(data1[i].e_mail).html(data1[i].Employer));
                 }
    }    
              
}


function checkAll(){
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').attr('checked', true);
    } else {
        $('input:checkbox').attr('checked', false);
    }
}

function AddCandidate(){
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;

   }
    else {
      
 $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
   var regno='';var candidatename='';var uniqueid='';var mobile='';var email='';
    var candidatedetail1 = this.value.split(",")
 //   candidatedetail+=""+candidatedetail1[0] +"<br>"+candidatedetail1[1] +"<br>"+candidatedetail1[2] +"<br>"+candidatedetail1[3] +"<br>"+candidatedetail1[4] +""
    regno +=""+candidatedetail1[0] +""
    candidatename +=""+candidatedetail1[1] +""
    uniqueid +=""+candidatedetail1[2] +""
    mobile +=""+candidatedetail1[3] +""
    email +=""+candidatedetail1[4] +""
    
    FillCandidateList(regno,candidatename,uniqueid,mobile,email);
   
 });
}



}

function CandidateList(){
    var candidatedetail =''
    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
        var candidatedetail1 = this.value.split(",")
      
        candidatedetail += "<tr><td>"+candidatedetail1[0] +"</td><td>" + candidatedetail1[1] +"</td><td>" + candidatedetail1[2] +"</td><tr>";
    
    
     });
     sendemail("<table border=1>  <tr><th>Name</th><th>Mobile No.</th><th>E-mailId</th></tr>"+candidatedetail+"</table>")
    }
  

function sendemail(Candidatelist){
 var sub = "MP Rojgar";
var body = "Dear Employer,<br>"+
" Thank you for attending Job Fair on Dated "+  jQuery("#hiddenjf option:selected").text()+"   We thank you for participation and please find below the list of selected candidates in that particular job fair <br>"+
"<br>"+Candidatelist+"<br>"+
"We request you to please inform their joining status and provide us their copy of offer letter and their 3 month salary slip.<br>Our representative will be in touch with you for coordination and serving you in future also.<br>"+
"<div> Thank you for attending Job Fair .</div><br>"+
           
     "Regards,<br>"+
     "Name : "+sessionStorage.CandidateName;
     sentmailglobal($('#Employer').val(),body,sub,'');
}



function FillJobFairDDLHidden(){
    var path =  serverpath + "itijfexchange/0"
    securedajaxget(path,'parsedatasecuredFillJobFairDDLHidden','comment',"control");
}
function parsedatasecuredFillJobFairDDLHidden(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else{
        jQuery("#hiddenjf").empty();
        var data1 = data[0];
       
        for (var i = 0; i < data1.length; i++) {
            jQuery("#hiddenjf").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt +' at '+data1[i].Venue));
         }

        
    }      
}

function FillCandidateList(regno,candidatename,uniqueid,mobile,email){
    var MasterData ={
        "p_Id":'0',
        "p_Reg_No":regno,     
        "p_Cnadidate_Name":candidatename,         
        "p_Unique_Id":uniqueid,
        "p_JobFair_Id":$("#JobFair").val(),
        "p_Company":$("#Employer option:selected").html(),
        "p_Mobile":mobile,
        "p_Email":email,
        "p_Selected":'Y'
                
                };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "SelectedCandJobFair";
      securedajaxpost(path, 'parsedatasecuredFillCandidateList', 'comment', MasterData, 'control')
      }
function parsedatasecuredFillCandidateList(data){  
    data = JSON.parse(data)
    //console.log(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCandidateList()
}
else if (data[0][0].ReturnValue == "1") {
    CandidateList();
  
}
   
 }


 function CheckValidation(){

  if(isValidation){
    if($("#JobFair").val()=="0"){
        getvalidated('JobFair','select','job fair name')
        return false;
        }
        if($("#Employer").val()=="0"){
            getvalidated('Employer','select','Company name')
            return false;
            }
            else{
                AddCandidate();
            }
        } 
        else{
            AddCandidate();
        }
    } 

    