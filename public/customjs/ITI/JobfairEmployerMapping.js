function FillJobFair(funct,control) {
    var path =  serverpath + "itijfexchange/0"
    securedajaxget(path,'parsedatasecuredjobfairempmapping','comment',control);
}

function parsedatasecuredjobfairempmapping(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFair('parsedatasecuredjobfairempmapping',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Job Fair"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title+' - '+data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt +' at '+data1[i].Venue+''));
             }
             if (sessionStorage.JobFairId){
        
                jQuery("#"+control).val(sessionStorage.JobFairId)
             }
             sessionStorage.JobFairId='0'
        }
              
}

jQuery('#jobfairnameid').on('change', function () {
        Fetchemployer('parsedataFetchemployer ',jQuery('#jobfairnameid').val());
    });

function Fetchemployer(funct,control) {
    // var path = serverpath + "itiEmployer"
    var path = serverpath + "JobfairEmployer/'"+$("#jobfairnameid").val()+"'"
	ajaxget(path, 'parsedataFetchemployer', 'comment', control);
}
function parsedataFetchemployer(data,control) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fetchemployer('parsedataFetchemployer ',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }


	//data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
    var active = "";
    if(data[0].length){
        for (var i = 0; i < data1.length; i++) {
   
            appenddata += "<tr class='i'><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Employer+ "</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' data-toggle='modal' data-target='#myModal1'  onclick=set('"+data1[i].JobfairEmpRegId+"');>Modify</button></td></td><td><input type='checkbox' name='checkbox' id="+data1[i].e_mail+" value="+data1[i].JobfairEmpRegId+" class="+data1[i].JobfairEmpRegId+"></td></tr>";
        } jQuery("#tbodyvalue").html(appenddata);
        $('#tbodyvalue').clientSidePagination();
    
    }

    
}


function set(JobfairEmpRegId){
sessionStorage.setItem('type','editMode')
sessionStorage.setItem('JobfairEmpRegId',JobfairEmpRegId)
window.location='/ITI/CompanyRegister'

}


function checkAll(){
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').attr('checked', true);
    } else {
        $('input:checkbox').attr('checked', false);
    }
}
function AddEmployer(){
    var seletedid=''
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Company", "", "info")
        return false;



    }
    else {
      
 $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
     
    InsUpdjobfairemployermapping(this.value)
 });
}
}
function InsUpdjobfairemployermapping(Id){  
    sessionStorage.JobfairEmpRegId=Id
    
	var MasterData = {  
    "p_id":'0',
    "p_jf_id":jQuery("#jobfairnameid").val(),
    "p_emp_id":Id,
    
    
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "itijfempmapping";
securedajaxpost(path, 'parsrdatacompanytype', 'comment', MasterData, 'control')
        
}
function parsrdatacompanytype(data) {
    data = JSON.parse(data)
    console.log(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
            InsUpdCompanyType()
	}
	else if (data[0][0].ReturnValue == "1") {
        SendAlert('email')

    }
}


function FetchJobFairFilters() {
    var path = serverpath + "JobFairFiltersById/"+jQuery("#jobfairnameid").val()+""
	ajaxget(path, 'parsedataFetchJobFairFilters', 'comment', "control");
}
function parsedataFetchJobFairFilters(data) {
	data = JSON.parse(data)
    var data1 = data[0];
    
    sessionStorage.SearchCandidate='Y'
    sessionStorage.Designation=data1[0].u_Designation;
    sessionStorage.Education=data1[0].u_Qualification;
    sessionStorage.Location=data1[0].u_Location;
    window.location='/SearchCandidate'
    
}


function SendAlert(type){
    var seletedid=''
 $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
     if(type=='email'){
         sendemail(this.id)
     }
     if(type=='mobile'){
     }
 });

}


function sendemail(emailid){
    var sub = "MP Rojgar-Job Fair";
var body = "<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'><img src='http://www.upween.com/yashs.png' style='height: 70px; float: left;'> <img src='http://www.upween.com/mprojgar.png' style='float: right; margin-top: -14.5%;'></td> </tr>" +
"<tr><td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'><tr>" +
"<td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'><div style='font-family:Georgia, Times New Roman, Times, serif; font-size:56px; color:#000000;'>Directorate of Employment,Govt of M.P </div>" +
"<div style='font-size:14px;color:darkgreen'><br>Dear Employer, <br></div></br><div style='font-size:13px;'>There is a job fair<br></div></br><div style='font-size:13px;'></div></br>"+
"<br>हम आपकी सहायता करने के लिए तत्पर हैं।</div><br>किसी भी सहायता के लिए, कृपया टोल फ्री हेल्पलाइन 0755-2767927 (10:00 पूर्वाह्न से 07:00 बजे तक) पर संपर्क करें । or they may send an email at talent.management@yashaswi.edu.in</td></tr></table></td></tr></table>";
Email.send(globalemailid,
emailid,
sub,
body,
"smtp.gmail.com",
globalemailid,
globalemailpass);
//toastr.success("Email sent Successfully", "", "success")
FetchJobFairFilters();
}


function goBack() {
    window.history.back()
  }

  function CheckValidation(){

    if(isValidation){
    if($("#jobfairnameid").val()=="0"){
        getvalidated('jobfairnameid','select','job fair name')
        return false;
        }
        
        else{
             AddEmployer();
        }

    } 
    else{
        AddEmployer();
   }

} 


   