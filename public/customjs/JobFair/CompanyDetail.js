function FillType(funct,control) {
    var path =  serverpath + "companytype/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillType('parsedatasecuredFillType',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Company_type_id).html(data1[i].Company_type));
             }
        }
              
}

function CheckValidation(){
    var chkval = $("#chkActiveStatus")[0].checked
    if(jQuery("#textHindi").val()==''){
		getvalidated("textHindi","text","Company Name(In Hindi)")
		return false;
	}
	if(jQuery("#textEnglish").val()==''){
		getvalidated("textEnglish","text","Company Name(In English)")
		return false;
    }
    if($("#ddlType").val()==''){
		getvalidated("ddlType","select","Type ")
		return false;
	}
	if(jQuery("#Addresshindi").val()==''){
		getvalidated("Addresshindi","text","Address (in Hindi)")
		return false;
	}
	if(jQuery("#textContact").val()==''){
		getvalidated("textContact","number","Contact Number")
		return false;
	}
	
	if(jQuery("#addresseng").val()==''){
		getvalidated("addresseng","text","Address (in English)")
		return false;
	}
	
	else{
		InsupdCompanyprofile()
	}
}
function InsupdCompanyprofile(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	"p_CP_id": localStorage.CP_id,
    "p_CName":jQuery("#textEnglish").val(),
    "p_CNameH":jQuery("#textHindi").val(),
    "p_CType_id":$("#ddlType").val(),
	"p_Address":jQuery("#addresseng").val(),
	"p_AddressH":jQuery("#Addresshindi").val(),
    "p_ContactNo":jQuery("#textContact").val(),
	"p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/companyprofile";
securedajaxpost(path, 'parsrdataitmenu', 'comment', MasterData, 'control')
}


function parsrdataitmenu(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdCompanyprofile();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetmode();
		FetchCompany();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetmode();
		FetchCompany();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetmode()
	toastr.warning("already exist", "", "info")
			return true;
	}
 
}
function resetmode(){
    localStorage.CP_id='0'
    jQuery("#textEnglish").val('')
    jQuery("#textHindi").val('')
    $("#ddlType").val('0'),
	jQuery("#addresseng").val('')
	jQuery("#Addresshindi").val('')
    jQuery("#textContact").val('')
    $("#chkActiveStatus")[0].checked=false;
}
function FetchCompany() {
    var path = serverpath + "companyprofile/0/0"
    ajaxget(path, 'parsedataFetchCompany', 'comment', "control");
}
function parsedataFetchCompany(data) {
    data = JSON.parse(data)
	
    
     
    
		var data1 = data[0];
		var appenddata="";
		var active="";
		for (var i = 0; i < data1.length; i++) {
       if(data1[i].Active_YN=='0'){
active="No"
	   }
	   else if(data1[i].Active_YN=='1'){
		active="Yes"
			   }
   appenddata += "<tr><td style=' word-break: break-word;'>" + data1[i].CP_id+ "</td><td style='word-break: break-word;'>" + data1[i].CNameH+ "</td><td style='    word-break: break-word;'>" + data1[i].CName+ "</td><td style='    word-break: break-word;'>" + data1[i].AddressH+ "</td><td>" +data1[i].Address+ "</td><td>"+data1[i].ContactNo+"</td><td>"+active+"</td><td><button type='button'  onclick=EditCompany('"+data1[i].CP_id+"','"+encodeURI(data1[i].CNameH)+"','"+encodeURI(data1[i].CName)+"','"+encodeURI(data1[i].CType_id)+"','"+encodeURI(data1[i].ContactNo)+"','"+encodeURI(data1[i].AddressH)+"','"+encodeURI(data1[i].Address)+"','"+encodeURI(data1[i].Active_YN)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	}jQuery("#tbodyvalue").html(appenddata);  
      
   
	}
	function EditCompany(CP_id,CNameH,CName,CType_id,ContactNo,AddressH,Address,Active_YN){
		jQuery('#m_scroll_top').click();
		localStorage.CP_id=CP_id
	jQuery("#textHindi").val(decodeURI(CNameH))
    jQuery("#textEnglish").val(decodeURI(CName))
    $("#ddlType").val(CType_id),
    jQuery("#textContact").val(decodeURI(ContactNo))
	jQuery("#Addresshindi").val(decodeURI(AddressH))
    jQuery("#addresseng").val(decodeURI(Address))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
	}
