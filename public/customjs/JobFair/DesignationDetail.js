function FillDesignationTable() {
    var path =  serverpath + "adminDesignation/0/0"
    securedajaxget(path,'parsedatasecuredFillDesignationTable','comment',"control");
}
function parsedatasecuredFillDesignationTable(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDesignationTable();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            for (var i = 0; i < data1.length; i++) {
           if(data1[i].Active_YN=='0'){
    active="N"
           }
           else if(data1[i].Active_YN=='1'){
            active="Y"
                   }
       appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Designation_Id+ "</td><td style='    word-break: break-word;'>" + data1[i].DesignationH+ "</td><td style='    word-break: break-word;'>" + data1[i].Designation+ "</td><td>"+active+"</td><td><button type='button' onclick=editMode('"+data1[i].Designation_Id+"','"+encodeURI(data1[i].DesignationH)+"','"+encodeURI(data1[i].Designation)+"','"+encodeURI(data1[i].Active_YN)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}
function editMode(Designation_Id,DesignationH,Designation,Active_YN){
    jQuery("#scrolltop").click();
    localStorage.DesignationId=Designation_Id,
    $("#textEnglish").val(decodeURI(Designation)),
    $("#textHindi").val(decodeURI(DesignationH))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
}
function CheckValidation(){
	var chkval = $("#chkActiveStatus")[0].checked
	if(jQuery("#textHindi").val()==''){
		getvalidated("textHindi","text","Designation (In Hindi)")
		return false;
	}
	if(jQuery("#textEnglish").val()==''){
		getvalidated("textEnglish","text","Designation(In English)")
		return false;
	}
	
	else{
		InsupdDesignationDetail()
	}
}
function InsupdDesignationDetail(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData ={
    "p_Designation_Id":localStorage.DesignationId,
    "p_Designation":$("#textEnglish").val(),
    "p_DesignationH":$("#textHindi").val(),
    "p_Cadre_ID":'0',
     "p_Active_YN":chkval,
     "p_Client_IP":sessionStorage.IpAddress,
     "p_LMDT":"1",
     "p_LMBY":sessionStorage.CandidateId,
     "p_s_no":"1"
    
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "adminDesignation";
securedajaxpost(path, 'parsrdataitInsupdDesignationDetail', 'comment', MasterData, 'control')
}


function parsrdataitInsupdDesignationDetail(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdDesignationDetail();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetmode()
        FillDesignationTable();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetmode()
        FillDesignationTable();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
         resetmode()
        FillDesignationTable();
	toastr.warning("already exist", "", "info")
			return true;
	}
 
}
function resetmode(){
    localStorage.DesignationId='0',
    $("#textEnglish").val(""),
    $("#textHindi").val("")
    $("#chkActiveStatus")[0].checked=false;

    }