jQuery('#ddlExchangeName').on('change', function () {
    FillJobFairDDL(jQuery('#ddlExchangeName').val());

      });
     function FillJobFairDDL(Ex_Id){
        $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
        var path =  serverpath + "secured/itijfexchange/"+Ex_Id+""
        securedajaxget(path,'parsedatasecuredFillJobFairDDL','comment',"control");
    }
    function parsedatasecuredFillJobFairDDL(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillJobFairDDL(jQuery('#ddlExchangeName').val());
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
        else{
            jQuery("#jfddl").empty();
            var data1 = data[0];
            jQuery("#jfddl").append(jQuery("<option></option>").val('0').html('Select Job Fair'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#jfddl").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title+' '+data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt));
             }

            
        }      
    }
    function FillJFJsReg(){
        var path =  serverpath + "secured/Interestedcandidatejf/"+jQuery("#jfddl").val()+""
        securedajaxget(path,'parsedatasecuredFillJFJsReg','comment',"control");
          }
    function parsedatasecuredFillJFJsReg(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillJFJsReg();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
             
                for (var i = 0; i < data1.length; i++) {
             
           appenddata += "<tr><td>" + [i+1]+ "</td><td style='    word-break: break-word;'>" + data1[i].RegistrationId+ "</td><td style='    word-break: break-word;'>"+data1[i].CandidateName+"</td><td >" + data1[i].EmailId+ "</td><td>"+data1[i].MobileNumber+"</td></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
                  
    }
    function FillSecuredexchangeoffice1(funct,control) {
        var path =  serverpath +"secured/iti_institute"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillSecuredexchangeoffice1(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSecuredexchangeoffice1('parsedatasecuredFillSecuredexchangeoffice1','ddlExchangeName');                     }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option></option>").val('0').html('Select ITI Name'));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].id).html(data1[i].Institute_Name));
                 }
                
            }
                  
    }
    function CheckValidationinterstedcandidate(){
        if(jQuery("#ddlExchangeName").val()=="0"){
            getvalidated('ddlExchangeName','select','ITI Name');
           return false;
        }
      else  if(jQuery("#jfddl").val()==" "){
            getvalidated('jfddl','select',' Job Fair');
            return false;
        }
       
    }