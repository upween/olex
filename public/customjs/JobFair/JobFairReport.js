jQuery('#textReportName').on('change', function () {
    FillDDlExchOffice(jQuery('#textReportName').val());
   
      });
      function FillDDlExchOffice(ReportID){
        
        var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
       securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', 'control')
          }
    function parsedatasecuredFillDDlExchOffice(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDDlExchOffice();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
             
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#JFreportExchange").append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                 }
            }  
                  
    }

    function FillDDlReportName(){
        
        var path = serverpath + "secured/GetRptName/0/'JF'/1";
       securedajaxget(path, 'parsedatasecuredGetRptName', 'comment', 'control')
          }
    function parsedatasecuredGetRptName(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDDlReportName();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
             
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#textReportName").append(jQuery("<option></option>").val(data1[i].RptID).html(data1[i].RptNameEng));
                 }
            }  
                  
    }