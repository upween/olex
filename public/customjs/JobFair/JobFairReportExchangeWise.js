function Fillexchangeoffice(funct,control) {
    var path =  serverpath +"secured/exchangeoffice/0/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillexchangeoffice('parsedatasecuredFillexchangeoffice',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('Select Exchange'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
             jQuery("#"+control).val(sessionStorage.Ex_id);
        }
              
}


function checkValidate(){
    if($("#m_datepicker_6").val()==""){
        getvalidated('m_datepicker_6','text','From Date');
         return false;
     }
   else if($("#m_datepicker_5").val()==""){
     getvalidated('m_datepicker_5','text','To Date');
        return false;
   }
    else{
        FetchJobfairReport();
        }
}



function FetchJobfairReport() {
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    var Exid = $('#jobfairreportexchange').val();
}
else{
    var Exid = sessionStorage.Ex_id
}

var fd = $('#m_datepicker_6').val().split('/')
var fromdate = fd[2]+'-'+fd[1]+'-'+fd[0]

var td = $('#m_datepicker_5').val().split('/')
var todate = td[2]+'-'+td[1]+'-'+td[0]

    var path = serverpath + "jobfairReport/"+Exid+"/'"+fromdate+"'/'"+todate+"'"
    securedajaxget(path, 'parsedataFetchJobfairReport', 'comment', 'control');
}
function parsedataFetchJobfairReport(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FetchJobfairReport();
    }
    else {
        var data1 = data[0];
        var appenddata ='';
     

      // if(data[0][0].Selection_Status=='2'){
      //   var primaryStatus ='Yes'
      //   var SelectionStatus ='Yes'
      //   }
      //   else if(data[0][0].Selection_Status=='1'){
      //     var primaryStatus ='Yes'
      //     var SelectionStatus ='No'
      //   }
      //   else if(data[0][0].Selection_Status=='0'){
      //     var primaryStatus ='No'
      //     var SelectionStatus ='No'
      //   }

var NOOFEMPLOYERS = 0;
var NOOFVACANCIES = 0;
var NOOFCANDIDATESELECTED = 0;
var PRELIMINARYSTAGE = 0;
var OFFERLETTER = 0;
var Total="";
        for (var i = 0; i < data1.length; i++) {
            if(data1[i].VacancyCounts==null){
                var VacancyCount ='0'
                }
                else{
                  var VacancyCount = data1[i].VacancyCounts
                }

                NOOFEMPLOYERS=NOOFEMPLOYERS+data1[i].EmpCount;
                NOOFVACANCIES=NOOFVACANCIES+data1[i].VacancyCounts;
                NOOFCANDIDATESELECTED=NOOFCANDIDATESELECTED+data1[i].CandidateCount;
                PRELIMINARYSTAGE=PRELIMINARYSTAGE+data1[i].SelectionStatus;
                OFFERLETTER=OFFERLETTER+data1[i].FinalStatus;
// var fdate1 = data1[i].Jobfair_FromDt.split('-')
// var fdate = fdate1[1]+'/'+fdate1[0]
// var FD = fdate1[2].split('')
// var fd = FD[0]+FD[1]
// var Fromdate=fd+'/'+fdate

// var tdate1 = data1[i].Jobfair_ToDt.split('-')
// var tdate = tdate1[1]+'/'+tdate1[0]
// var TD = tdate1[2].split('')
// var td = TD[0]+TD[1]
// var Todate=td+'/'+tdate

        appenddata +='<tr class="i"><th>'+[i+1]+'</th><th>'+data1[i].Ex_name+'</th><th>'+data1[i].Jobfair_FromDt+' To '+data1[i].Jobfair_ToDt+'</th><th>'+data1[i].EmpCount+'</th><th>'+VacancyCount+'</th><th>'+data1[i].CandidateCount+'</th><th> '+data1[i].SelectionStatus+' </th><th>'+data1[i].FinalStatus+' </th></tr>';
     Total='<tr class="total" style="background-color: #edecfc;"><th>Total</th><th></th><th></th><th>'+NOOFEMPLOYERS+'</th><th>'+NOOFVACANCIES+'</th><th>'+NOOFCANDIDATESELECTED+'</th><th>'+PRELIMINARYSTAGE+'</th><th>'+OFFERLETTER+'</th></tr>';
    }
    
    $('#tbodyvalue').html(appenddata+Total);
    $('#tbodyvalue').clientSidePagination();
  }
}






var asc = 0;
function sort_table(table, col)
{  
	$('.sortorder').remove();
	if (asc == 2) {asc = -1;} else {asc = 2;}
	var rows = table.tBodies[0].rows;
	var rlen = rows.length-1;
	var arr = new Array();
	var i, j, cells, clen;
	// fill the array with values from the table
	for(i = 0; i < rlen; i++)
	{
		cells = rows[i].cells;
		clen = cells.length;
		arr[i] = new Array();
		for(j = 0; j < clen; j++) { arr[i][j] = cells[j].innerHTML; }
	}
	// sort the array by the specified column number (col) and order (asc)
	arr.sort(function(a, b)
	{
		var retval=0;
		var col1 = a[col].toLowerCase().replace(',', '').replace('$', '').replace(' usd', '')
		var col2 = b[col].toLowerCase().replace(',', '').replace('$', '').replace(' usd', '')
		var fA=parseFloat(col1);
		var fB=parseFloat(col2);
		if(col1 != col2)
		{
			if((fA==col1) && (fB==col2) ){ retval=( fA > fB ) ? asc : -1*asc; } //numerical
			else { retval=(col1 > col2) ? asc : -1*asc;}
		}
		return retval;      
	});
	for(var rowidx=0;rowidx<rlen;rowidx++)
	{
		for(var colidx=0;colidx<arr[rowidx].length;colidx++){ table.tBodies[0].rows[rowidx].cells[colidx].innerHTML=arr[rowidx][colidx]; }
	}
	
	hdr = table.rows[0].cells[col];
	if (asc == -1) {
		$(hdr).html($(hdr).html() + '<span class="sortorder">▲</span>');
		} else {
		$(hdr).html($(hdr).html() + '<span class="sortorder">▼</span>');
	}
}


function sortTable(n) {
   
	sort_table(document.getElementById("tableglobal"), n);
}








