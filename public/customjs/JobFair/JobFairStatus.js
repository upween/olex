function FillJobFairStatus(){
    var path =  serverpath + "secured/GetJobFairdetails/"+$("#jobfairstatusyear").val()+"/"+$("#jobfairstatusmonth").val()+""
    securedajaxget(path,'parsedataFillJobFairStatus','comment',"control");
}
function parsedataFillJobFairStatus(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairStatus();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            var m =0;
            for (var i = 0; i < data1.length; i++) {
                m=i+1;
                   
       appenddata += "<tr><td>"+m+"</td><td><a href='#' >" + data1[i].Jobfair_Title+ "</a></td><td style='    word-break: break-word;'>"+data1[i].Ex_name+"</td><td>"+data1[i].Venue+"</td><td >" + data1[i].Jobfair_FromDt+ "</td><td>"+data1[i].Jobfair_ToDt+"</td><td>"+data1[i].TotalEmp+"</td><td>"+data1[i].TotalJS+"</td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
    //onclick=Delete("+data1[i].Jobfair_Id+",'JFDetail')        
}
function FillMonthJF(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredFillMonthJF(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthJF('parsedatasecuredFillMonthJF',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}