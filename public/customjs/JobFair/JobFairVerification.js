jQuery('#JFVExchangeName').on('change', function () {
    FillJobFairDetailVerification(jQuery('#JFVExchangeName').val(),'1');
      });
     function FillJobFairDetailVerification(Ex_Id,Status){
         
        var path =  serverpath + "secured/exchjfdtlsbyStatus/0/"+Ex_Id+"/0/"+Status+""
    
   
        securedajaxget(path,'parsedatasecuredFillJobFairDetailVerification','comment',Status);
        if(Status=='1'){
               $('#ddlexchng').show();
               $("#JFheading").html('Modify Verified Job Fair')
               sessionStorage.Status='Verified';
        }
        else{
            $('#ddlexchng').hide()
            sessionStorage.Status='Not-Verified';
            $("#JFheading").html('Modify Not-Verified Job Fair')
        }
    }
    function parsedatasecuredFillJobFairDetailVerification(data,Status){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillJobFairDetailVerification(jQuery('#ddlExchangeName').val(),Status);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
                var active="";
                for (var i = 0; i < data1.length; i++) {
               if(data1[i].Active_YN=='0'){
        active="N"
               }
               else if(data1[i].Active_YN=='1'){
                active="Y"
                       }
                       if(data1[i].Status=='0'){
                        status='Not Verified'
                       actionby=data1[i].Jobfair_CreatedBy
                       actionDate=data1[i].LMDT
                       }
                       else if(data1[i].Status=='1'){
                        status='Verified';
                        actionby=data1[i].Verified_By
                        actionDate=data1[i].Verified_Dt
                    
                       }
                       else if(data1[i].Status=='2'){
                        status='Reject'
                        actionby=data1[i].Jobfair_CreatedBy
                        actionDate=data1[i].LMDT
                       }
                       
           appenddata += "<tr><td>" + data1[i].Jobfair_Id+ "</td><td ><a href='#' onclick=editTitleMode('"+data1[i].Jobfair_Id+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].Jobfair_Title)+"','"+encodeURI(data1[i].Jobfair_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].Jobfair_FromDt)+"','"+encodeURI(data1[i].Jobfair_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].Status)+"','"+encodeURI(data1[i].Reject_Reason)+"')>" + data1[i].Jobfair_Title+ "</a></td><td >"+data1[i].Venue+"</td><td >" + data1[i].Jobfair_FromDt+ "</td><td>"+data1[i].Jobfair_ToDt+"</td><td>"+data1[i].Ex_name+"</td><td>"+status+"</td><td>"+actionby+"</td><td>"+actionDate+"</td><td>"+active+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].Jobfair_Id+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].Jobfair_Title)+"','"+encodeURI(data1[i].Jobfair_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].Jobfair_FromDt)+"','"+encodeURI(data1[i].Jobfair_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].Status)+"','"+encodeURI(data1[i].Reject_Reason)+"')>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;'  onclick=setJfID('"+data1[i].Jobfair_Id+"','"+data1[i].Ex_id+"','"+data1[i].Status+"')  data-toggle='modal' data-target='#m_modal_5' id='btnDelete'>Delete</button></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
                  
    }
    function setJfID(id,ex_id,status){
        localStorage.JFId=id;
        localStorage.Ex_Id=ex_id;
        localStorage.JFtype='v'
        localStorage.VStatus=status
    }
    function FillVerifyJFexchangeoffice() {
        var path =  serverpath +"exchangeoffice/0/0/1"
        ajaxget(path,'parsedatasecuredJFVerifyexchangeoffice','comment');
    }
    
    function parsedatasecuredJFVerifyexchangeoffice(data){  
        data = JSON.parse(data)
       
                jQuery("#JFVExchangeName").empty();
                var data1 = data[0];
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#JFVExchangeName").append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                 }
                 jQuery("#JFVExchangeName").val('100');
            
                  
    }
    function FillJFexchangeoffice() {
        var path =  serverpath +"exchangeoffice/0/0/1"
        ajaxget(path,'parsedatasecuredFillSecuredJFexchangeoffice','comment');
    }
    
    function parsedatasecuredFillSecuredJFexchangeoffice(data){  
        data = JSON.parse(data)
       
                jQuery("#modifyjobfairexchange").empty();
                var data1 = data[0];
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#modifyjobfairexchange").append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                 }
                 jQuery("#modifyjobfairexchange").val('100');
            
                  
    }
    function editMode(Jobfair_Id,Ex_id,Jobfair_Title,Jobfair_Details,Venue,Jobfair_FromDt,Jobfair_ToDt,Active_YN,Status,Reason)
    {
       // FillSecuredexchangeoffice('parsedatasecuredFillSecuredexchangeoffice','modifyjobfairexchange');
       FillJFexchangeoffice() 
        $("#jfvStatus").val(Status);
        if(decodeURI(Status)=='0' || decodeURI(Status)=='1'){
            $("#JFVReason").val("");
            $("#JFVReason").attr('disabled',true)
        }
        if(decodeURI(Status)=='2'){
            $("#JFVReason").val(Reason)
            $("#JFVReason").attr('disabled',true)
        }
        
        $('#modifyjobdetails').show()
        localStorage.JFId=Jobfair_Id,
    $("#modifyjobfairexchange").val(Ex_id),
    $("#JFTitle").val(decodeURI(Jobfair_Title)),
    $("#JFVenue").val(decodeURI(Venue)),
    
    $("#m_datepicker_5").val(decodeURI(Jobfair_FromDt));
    $("#m_datepicker_6").val(decodeURI(Jobfair_ToDt));
    
    $("#JFDetail").val(decodeURI(Jobfair_Details))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}

    }
    function CheckValidation(){
        if($("#JFTitle").val()==""){
            getvalidated('JFTitle','text','Title');
            return false;
        }
        if($("#JFVenue").val()==""){
            getvalidated('JFVenue','text','Venue')
            return false;
        }
        if($("#m_datepicker_5").val()==""){
            getvalidated('m_datepicker_5','text','From Date');
            return false;
        }
        if($("#m_datepicker_6").val()==""){
            getvalidated('m_datepicker_6','text','To Date')
            return false;
        }
        else{
            InsUpdJobFairDetail();
        }
        
        }
    function InsUpdJobFairDetail(){
        var chkval = $("#chkActiveStatus")[0].checked
        if (chkval == true){
            chkval = "1"
        }else{
            chkval="0"
        }
        if(sessionStorage.Status=='Verified'){
            VerifyBy=sessionStorage.CandidateName;
            VerifyDate=Date.now()
        }
        if(sessionStorage.Status=='Not-Verified'){
            VerifyBy=''
            VerifyDate=''
        }
        var Jobfair_FromDt = jQuery("#m_datepicker_5").val().split(" ")
        var Jobfair_FromDt1 = Jobfair_FromDt[0].split("/")
        var Jobfair_FromDate = Jobfair_FromDt1[0] + "-" + Jobfair_FromDt1[1] + "-" + Jobfair_FromDt1[2] ;
    
        var Jobfair_ToDt = jQuery("#m_datepicker_6").val().split(" ")
        var Jobfair_ToDt1 = Jobfair_ToDt[0].split("/")
        var Jobfair_ToDate = Jobfair_ToDt1[0] + "-" + Jobfair_ToDt1[1] + "-" + Jobfair_ToDt1[2] ;
       
    var MasterData ={
        "p_Jobfair_Id":localStorage.JFId,
        "p_Ex_id":$("#modifyjobfairexchange").val(),
        "p_Jobfair_Title":$("#JFTitle").val(),
        "p_Venue":$("#JFVenue").val(),
        "p_Jobfair_Details":$("#JFDetail").val(),
        "p_Attachment_Filename":"t",
        "p_Date_of_Entry": "0",
        "p_Jobfair_FromDt":Jobfair_FromDate,
        "p_Jobfair_ToDt":Jobfair_ToDate,
        "p_Jobfair_CreatedBy":sessionStorage.CandidateId,
        "p_Status":$("#jfvStatus").val(),
        "p_Verified_By":VerifyBy,
        "p_Verified_Dt":VerifyDate,
        "p_Reject_Reason": $("#JFVReason").val(),
        "p_Active_YN":chkval,
        "p_LMDT":"0",
        "p_LMBY":sessionStorage.CandidateId
        
        };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/exchjfdtls";
    securedajaxpost(path, 'parsrdataitInsUpdJobFairDetail', 'comment', MasterData, 'control')
    }
    function parsrdataitInsUpdJobFairDetail(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                InsUpdJobFairDetail();
        }
        else if (data[0][0].ReturnValue == "1") {
             resetmode()
             if(sessionStorage.Status=='Not-Verified'){
                FillJobFairDetailVerification('0','0')
            }
            else{
                FillJobFairDetailVerification('100','1')
            }
             toastr.success("Insert Successful", "", "success")
                return true;
        }
        else if (data[0][0].ReturnValue == "2") {
             resetmode()
             if(sessionStorage.Status=='Not-Verified'){
                FillJobFairDetailVerification('0','0')
            }
            else{
                FillJobFairDetailVerification('100','1')
            }
                toastr.success("Update Successful", "", "success")
                return true;
        }
        else if (data[0][0].ReturnValue == "0") {
              resetmode()
              if(sessionStorage.Status=='Not-Verified'){
                FillJobFairDetailVerification('0','0')
            }
            else{
                FillJobFairDetailVerification('100','1')
            }
        toastr.warning("Already Exist", "", "info")
                return true;
        }
     
    }
    function resetmode(){
        localStorage.JFId='0',
        $("#addjobfairexchange").val("100"),
        $("#JFTitle").val(""),
        $("#JFVenue").val(""),
        $("#chkActiveStatus")[0].checked=false;
        $("#m_datepicker_5").val("");
        $("#m_datepicker_6").val("");
        $("#JFDetail").val("")
        $("#JFVReason").val("");
        $("#JFVReason").attr('disabled',false);
        $('#modifyjobdetails').hide();
        $("#jfvStatus").val("")
        }
      
        jQuery('#jfvStatus').on('change', function () {
            if(jQuery('#jfvStatus')=='0' ||jQuery('#jfvStatus')=='1'){
                $("#JFVReason").val("");
                $("#JFVReason").attr('disabled',true)
            }
            if(jQuery('#jfvStatus')=='2'){
                $("#JFVReason").val(Reason)
                $("#JFVReason").attr('disabled',true)
            }
              });
              function editTitleMode(Jobfair_Id,Ex_id,Jobfair_Title,Jobfair_Details,Venue,Jobfair_FromDt,Jobfair_ToDt,Active_YN,Status,Reason)
              {
                  $("#jfvStatus").val(Status);
                  if(decodeURI(Status)=='0' || decodeURI(Status)=='1'){
                      $("#JFVReason").val("");
                      $("#JFVReason").attr('disabled',true)
                  }
                  if(decodeURI(Status)=='2'){
                      $("#JFVReason").val(Reason)
                      $("#JFVReason").attr('disabled',true)
                  }
                  
                  $('#modifyjobdetails').show()
                  localStorage.JFId=Jobfair_Id,
              $("#modifyjobfairexchange").val(Ex_id),
              $("#JFTitle").val(decodeURI(Jobfair_Title)),
              $("#JFVenue").val(decodeURI(Venue)),
              
              $("#m_datepicker_5").val(decodeURI(Jobfair_FromDt));
              $("#m_datepicker_6").val(decodeURI(Jobfair_ToDt));
              
              $("#JFDetail").val(decodeURI(Jobfair_Details))
              if(decodeURI(Active_YN)=='0'){
                  $("#chkActiveStatus")[0].checked=false;
                  }
                  else{	
                      $("#chkActiveStatus")[0].checked=true;
                  }
                  $("#jfvStatus").attr('disabled',true);
                 
             
               
              $("#modifyjobfairexchange").attr('disabled',true),
              $("#JFTitle").attr('disabled',true),
              $("#JFVenue").attr('disabled',true),
              
              $("#m_datepicker_5").attr('disabled',true);
              $("#m_datepicker_6").attr('disabled',true);
              
              $("#JFDetail").attr('disabled',true)
              $("#chkActiveStatus").attr('disabled',true)
              }
              
              