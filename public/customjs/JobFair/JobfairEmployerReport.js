jQuery('#ddlExchangeName').on('change', function () {
    FillJobFairDetail(jQuery('#ddlExchangeName').val());
      });
function FillJobFairDetail(Ex_Id){
    var path =  serverpath + "secured/exchjfdtls/0/"+Ex_Id+"/0"
    securedajaxget(path,'parsedatasecuredFillJobFairDetail','comment',"control");
}
function parsedatasecuredFillJobFairDetail(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDetail(jQuery('#ddlExchangeName').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#jfddl").empty();
            var data1 = data[0];
            var appenddata='';
            var tablehead="<th><input type='checkbox'  id='selectall' onclick='checkAll()'></th><th> Jobfair Title</th><th> From Date</th><th>To Date</th>";
            $("#tablehead").html(tablehead);
                for (var i = 0; i < data1.length; i++) {
            
                  
                    appenddata += "<tr><td><input type='checkbox'  id='"+data1[i].Jobfair_Id+"'></td><td style='    word-break: break-word;'>" + data1[i].Jobfair_Title  + "</td><td>"+data1[i].Jobfair_FromDt+"</td><td>"+data1[i].Jobfair_ToDt+"</td></tr>";
                } jQuery("#tbodyvalue").html(appenddata);
            
           
            
          
           
    }           
}


function checkAll() {
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').attr('checked', true);
    } else {
        $('input:checkbox').attr('checked', false);
    }
}
function FillReport(type) {
    var jobfairid=[];
    if(type=='filter'){
        jobfairid=sessionStorage.JobfairFilter;
    }
    else{
        $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
      
            jobfairid.push(this.id);
    
            sessionStorage.JobfairFilter=jobfairid;
    
       
        });
    }
   console.log(jobfairid);
    var MasterData ={
    
        "p_Ex_id":jQuery('#ddlExchangeName').val(),
        "p_Jobfair_Id":jobfairid,
        "p_Order_By":$('#employerfilter').val()
        
        
        };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "empjfReport";
    securedajaxpost(path, 'parsrdataitJfReport', 'comment', MasterData, 'control')
}
function parsrdataitJfReport(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var Collectorate='';
    var exchangename='';
var tablehead="<th>Sr No</th><th> Name of Employer </th><th style='width:10%'> Number of Vacancies </th><th style='width:10%'> Number of Candidates Recieved Offer Letter </th>";
$("#tablehead").html(tablehead);
    for (var i = 0; i < data1.length; i++) {

        Collectorate=data1[i].CollectorateName ;
        exchangename= data1[i].ExchangeHead;
        appenddata += "<tr><td style='    word-break: break-word;'>" +[i+1]+ "</td><td style='    word-break: break-word;'>" + data1[i].CompName  + "</td><td>"+data1[i].VacancyCount+"</td><td>"+data1[i].CandidateCount+"</td></tr>";
    } 
    jQuery("#tbodyvalue").html(appenddata+'<tr ><td colspan="4"><div class="row"><div class="col-lg-1"></div> <div class="col-lg-4"><strong>Collectorate: </strong>'+ Collectorate +'</div><div class="col-lg-2"></div> <div class="col-lg-4"><strong>Exchange Head:</strong> '+exchangename+'</div></div></td></tr>');


}