jQuery('#PCExchange').on('change', function () {
    FillPCDetail(jQuery('#PCExchange').val());
      });
     function FillPCDetail(Ex_Id){
        var MasterData ={
            "p_PostId":'0',
            "p_Jobfair_Id":'0',
            "p_Ex_Id":Ex_Id,
            "p_PostName":'0',
            "p_NoOfVacancy":'0',
            "p_E_Userid":'0',
            "p_PayAllowance":"",
            "p_PlaceOfwork":"",
            "p_Gender":"",
            "p_MinAge":"",
            "p_MaxAge":"",
            "p_Date":"",
            "p_EQualification":"",
            "p_Dqualification":"",
            "p_Remark":"",
            'p_Year':"",
            "p_Month":"",
           "p_flag":'2'
            };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/CategoryWiseSetPostCountN";
        securedajaxpost(path, 'parsedatasecuredFillPCDetail', 'comment', MasterData, 'control')
    }
    function parsedatasecuredFillPCDetail(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillPCDetail(jQuery('#PCExchange').val());
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
                var verify="";
                for (var i = 0; i < data1.length; i++) {
              if(data1[i].verified=='0'){
                  verify="<button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=Verify("+data1[i].POST_ID+") >Verify</button>"
              }
              if(data1[i].verified=='1'){
                  verify="Verified";
             }
                       
           appenddata += "<tr><td>" + data1[i].Jobfair_Title+ "</td><td style='    word-break: break-word;'>" + data1[i].PostName+ "</td><td style='    word-break: break-word;'>"+data1[i].NoOfVacancy+"</td><td >" + data1[i].PayAllowance+ "</td><td>"+data1[i].PlaceOfwork+"</td><td>"+data1[i].Gender+"</td><td>"+data1[i].MinAge+"</td><td>"+data1[i].MaxAge+"</td><td>"+data1[i].Date+"</td><td>"+data1[i].EQualification+"</td><td>"+data1[i].Dqualification+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].POST_ID+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].Year)+"','"+encodeURI(data1[i].Month)+"','"+encodeURI(data1[i].Jobfair_Id)+"','"+encodeURI(data1[i].E_Userid)+"','"+encodeURI(data1[i].PostName)+"','"+encodeURI(data1[i].NoOfVacancy)+"','"+encodeURI(data1[i].PayAllowance)+"','"+encodeURI(data1[i].PlaceOfwork)+"','"+encodeURI(data1[i].Gender)+"','"+encodeURI(data1[i].MinAge)+"','"+encodeURI(data1[i].MaxAge)+"','"+encodeURI(data1[i].Date)+"','"+encodeURI(data1[i].EQualification)+"','"+encodeURI(data1[[i].Dqualification])+"','"+encodeURI(data1[i].Remark)+"')>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=Delete("+data1[i].Jobfair_Id+",'JFDetail') id='btnDelete'>Delete</button></td><td>"+verify+"</td></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
                  
    }
    // function editMode('"+data1[i].POST_ID+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].Year)+"','"+encodeURI(data1[i].Month)+"','"+encodeURI(data1[i].Jobfair_Id)+"','"+encodeURI(data1[i].E_Userid)+"','"+encodeURI(data1[i].PostName)+"','"+encodeURI(data1[i].NoOfVacancy)+"','"+encodeURI(data1[i].PayAllowance)+"','"+encodeURI(data1[i].PlaceOfwork)+"','"+encodeURI(data1[i].Gender)+"','"+encodeURI(data1[i].MinAge)+"','"+encodeURI(data1[i].MaxAge)+"','"+encodeURI(data1[i].Date)+"','"+encodeURI(data1[i].EQualification)+"','"+encodeURI(data1[[i].Dqualification])+"','"+encodeURI(data1[i].Remark)+"'){

    // }
    function Verify(PCId){
        var MasterData ={
            "p_PostId":PCId,
            "p_Jobfair_Id":'0',
            "p_Ex_Id":'0',
            "p_PostName":'0',
            "p_NoOfVacancy":'0',
            "p_E_Userid":'0',
            "p_PayAllowance":"",
            "p_PlaceOfwork":"",
            "p_Gender":"",
            "p_MinAge":"",
            "p_MaxAge":"",
            "p_Date":"",
            "p_EQualification":"",
            "p_Dqualification":"",
            "p_Remark":"",
            'p_Year':"",
            "p_Month":"",
           "p_flag":'3'
            };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "CategoryWiseSetPostCountN";
        ajaxpost(path, 'parsedatasecuredVerify', 'comment', MasterData, 'control')
    }
    function parsedatasecuredVerify(data){  
        data = JSON.parse(data)
      
        FillPCDetail(jQuery('#PCExchange').val());
                  
    }
    function InsUpdPCDetail(){
        var MasterData ={
            "p_PostId":localStorage.PCId,
            "p_Jobfair_Id":$('#ddlJF').val(),
            "p_Ex_Id":$('#AddPCexchange').val(),
            "p_PostName":$('#txtDesg').val(),
            "p_NoOfVacancy":$('#txtVacancies').val(),
            "p_E_Userid":$('#ddlEmp').val(),
            "p_PayAllowance":$("#txtPayAllowance").val(),
            "p_PlaceOfwork":$('#txtPlaceofWork').val(),
            "p_Gender":$("#ddlGender").val(),
            "p_MinAge":$("#txtMnAge").val(),
            "p_MaxAge":$("#txtmaxAge").val(),
            "p_Date":$("#m_datepicker_5").val(),
            "p_EQualification":$("#txtEQual").val(),
            "p_Dqualification":$("#txtDQual").val(),
            "p_Remark":$("#txtRemark").val(),
            'p_Year':$("#txtYear").val(),
            "p_Month":$("#txtMonth").val(),
           "p_flag":'1'
            };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/CategoryWiseSetPostCountN";
        securedajaxpost(path, 'parsedatasecuredFillPCDetail', 'comment', MasterData, 'control')
    }
    function parsedatasecuredFillPCDetail(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillPCDetail(jQuery('#PCExchange').val());
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
            }  
                  
    }

    jQuery('#ddlJF').on('change', function () {
        FillJFEmpReg();
          });
         function FillJFEmpReg(){
            var MasterData ={
                "p_JobFair_ID":jQuery("#ddlJF").val(),         
                "p_Emp_Regno":"0",         
                "p_Flag":'2',
                        
                        };
                    MasterData = JSON.stringify(MasterData)
                    var path = serverpath + "secured/EmpRegister";
              securedajaxpost(path, 'parsedatasecuredJFEmpReg', 'comment', MasterData, 'control')
              }
        function parsedatasecuredJFEmpReg(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillJFEmpReg();
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    var appenddata="";
                 
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                     }
                }  
                      
        }
    