jQuery('#jfemp').on('change', function () {
    FillJobFairDDL(jQuery('#jfemp').val());

});

function FillJobFairDDL(Ex_Id) {
    var path = serverpath + "secured/exchjfdtls/0/" + Ex_Id + "/0"
    securedajaxget(path, 'parsedatasecuredFillJobFairDDL', 'comment', "control");
}
function parsedatasecuredFillJobFairDDL(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL(jQuery('#jfemp').val());
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        jQuery("#jfddl").empty();
        var data1 = data[0];
        jQuery("#jfddl").append(jQuery("<option ></option>").val("0").html("Select Job Fair"));

        for (var i = 0; i < data1.length; i++) {
            jQuery("#jfddl").append(jQuery("<option></option>").val(data1[i].Jobfair_Id+'aspr'+data1[i].Jobfair_Type+'aspr'+data1[i].JobFair_link+'aspr'+data1[i].Venue+'aspr'+data1[i].Jobfair_Title + 'aspr' + data1[i].Jobfair_FromDt + 'aspr' + data1[i].Jobfair_ToDt).html(data1[i].Jobfair_Title + ' ' + data1[i].Jobfair_FromDt + ' To ' + data1[i].Jobfair_ToDt));
        }
        // if(sessionStorage.User_Type_Id=='31'){
        //     FillJFEmpNotReg()
        // }
        // else{
            FillJFEmpReg();
       // }
      

    }
}

function InsJFEmpReg(empregNo,notregEmpId) {
    sessionStorage.empregNo=empregNo;
    sessionStorage.notregEmpId=notregEmpId;
    if($('#EmpRegno').val()==''){
        getvalidated('EmpRegno','text','Registration No')
        return false;
    }
    else{
        var MasterData = {
            "p_JobFair_ID": $('#jfddl').val().split('aspr')[0],
            "p_Emp_Regno": empregNo,
            "p_Flag": '1',
            "p_VacancyCount":'0',
            "p_NotRegisteredId":notregEmpId
    
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/EmpRegister";
        securedajaxpost(path, 'parsrdataitInsJFEmpReg', 'comment', MasterData, 'control')
    }
  
}
function parsrdataitInsJFEmpReg(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsJFEmpReg();
    }

    else if (data[0][0].ReturnValue == "1") {
        FillJFEmpReg();
        if(sessionStorage.empregNo!='0'){

            var emp_email=$('#EmpRegno').val().split('aspr')[1];
        }
        else{
            
            var emp_email=$('#mail').val();
        }
       // console.log(emp_email)
     
      var eventtype=$('#jfddl').val().split('aspr')[1];
      var link=$('#jfddl').val().split('aspr')[2];
      var venue=$('#jfddl').val().split('aspr')[3];
      var title=$('#jfddl').val().split('aspr')[4];
      var fromdt=$('#jfddl').val().split('aspr')[5];
     // var dt1 = new Date($('#jfddl').val().split('aspr')[5]).toLocaleString('en-us', {weekday:'long'});
     var dt1 = new Date($('#jfddl').val().split('aspr')[5].split('/')[1]+'/'+$('#jfddl').val().split('aspr')[5].split('/')[0]+'/'+$('#jfddl').val().split('aspr')[5].split('/')[2])
     console.log('date',dt1);
      var dt=dt1.toLocaleString('en-us', {weekday:'long'})

      //console.log(dt);
      if(eventtype=='Online'){
        var heading=`<p style='font-size:14px'>Please find jobfair link  below to attend jobfair online:</p>`
        var ifoffline=`<p style='font-size:14px;color:red'><strong>Job Fair Link: <a href='${link}' style='color:blue'>Click Here To Join Job Fair</a></strong></p>`
                
                  }
                  else{
                    var heading=`<p style='font-size:14px'>Please find details below to attend jobfair :</p>`
                    var ifoffline=`<p style='font-size:14px'><strong>Venue:</strong>${venue}</p>`
                 
                  }
                  if(sessionStorage.EmpPartType=='Inaugurator'){
                    var subject='INVITATION FOR ATTENDING JOB FAIR AS INAUGURAL SPEAKER FOR EVENT !!';
                    var body=`<p style='font-size:14px'>Dear Sir/Madam,</p>
                    <br>
                    <p style='font-size:14px'>
                    Greetings from Yashaswi Academy for Talent Management !!!</p>
                   
                    <p style='font-size:14px'>As we all know it is a difficult time due to pandemic wave in the country, we are unable to do Job fairs. It gives me an immense pleasure to inform you, that to resolve problem arise due to pandemic we have successfully develop the module of Online Job fair, we are going to host our first open Online Job fair for various companies of India on our department portal “www.mprojgar.gov.in”</p>
                  
                   <p style='font-size:14px'>
                   We are hosting Job Fair which will be live on our Facebook and You tube channel. This Job Fair shall have various vacancies for fresher’s & experiences which shall benefit the youths of Madhya Pradesh.</p>
                 
                   <p style='font-size:14px'>The said Job Fair is being hosted under the banner of Department of Employment & Skill Development and will be operated by Yashaswi being the PPP partner with the Department of Employment & Skill Development .
</p>
<p style='font-size:14px'><strong>We would like to invite you for attending Job Fair as Inaugural Speaker for Event.</strong></p>
                    <p style='font-size:14px;color:red'><strong >Day & Date : </strong>${dt} i.e. ${fromdt}</p> 
                    <p style='font-size:14px;color:red'><strong> Time: </strong> 1:00 to 5:00 pm</p>
                    ${ifoffline} ` 
                   }
                   else if(sessionStorage.EmpPartType=='Viewer'){
                    var subject='YASHASWI INVITES YOU FOR ATTENDING JOB FAIR AS GUEST VIEWER FOR EVENT !!';
                    var body=`<p style='font-size:14px'>Dear Sir/Madam,</p>
                  
                    <p style='font-size:14px'>
                    Greetings from Yashaswi Academy for Talent Management !!!</p>
                   
                    <p style='font-size:14px'>As we all know it is a difficult time due to pandemic wave in the country, we are unable to do Job fairs. It gives me an immense pleasure to inform you, that to resolve problem arise due to pandemic we have successfully develop the module of Online Job fair, we are going to host our first open Online Job fair for various companies of India on our department portal “www.mprojgar.gov.in”</p>
                  
                   <p style='font-size:14px'>
                   We are hosting Job Fair which will be live on our Facebook and You tube channel. This Job Fair shall have various vacancies for fresher’s & experiences which shall benefit the youths of Madhya Pradesh.</p>
                  
                   <p style='font-size:14px'>The said Job Fair is being hosted under the banner of Department of Employment & Skill Development and will be operated by Yashaswi being the PPP partner with the Department of Employment & Skill Development .
</p>
<br>
<p style='font-size:14px'><strong>We would like to invite you for attending Job Fair as Guest Viewer for Event.</strong></p>
                    <p style='font-size:14px;color:red'><strong >Day & Date : </strong>${dt} i.e. ${fromdt}</p> 
                    <p style='font-size:14px;color:red'><strong> Time: </strong> 1:00 to 5:00 pm</p>
                    ${ifoffline} ` 
                   }
                   else{
                    var subject='YASHASWI INVITES  FOR ATTENDING JOB FAIR AS HR PROFESSIONAL FOR EVENT !!';
                    var body=`<p style='font-size:14px'>Dear Sir/Madam,</p>
                    <br>
                    <p style='font-size:14px'>
                    Greetings from Yashaswi Academy for Talent Management !!!</p>
                   
                    <p style='font-size:14px'>As we all know it is a difficult time due to pandemic wave in the country, we are unable to do Job fairs. It gives me an immense pleasure to inform you, that to resolve problem arise due to pandemic we have successfully develop the module of Online Job fair, we are going to host our first open Online Job fair for various companies of India on our department portal “www.mprojgar.gov.in”</p>
                  
                   <p style='font-size:14px'>
                   We are hosting Job Fair which will be live on our Facebook and You tube channel. This Job Fair shall have various vacancies for fresher’s & experiences which shall benefit the youths of Madhya Pradesh.</p>
                  
                   <p style='font-size:14px'>The said Job Fair is being hosted under the banner of Department of Employment & Skill Development and will be operated by Yashaswi being the PPP partner with the Department of Employment & Skill Development .
</p>
<br>
<p style='font-size:14px'><strong>We would like to invite you for attending Job Fair as HR Professional for Event.</strong></p>
                    <p style='font-size:14px;color:red'><strong >Day & Date : </strong>${dt} i.e. ${fromdt}</p> 
                    <p style='font-size:14px;color:red'><strong> Time: </strong> 1:00 to 5:00 pm</p>
                    ${ifoffline} ` 
                   }
                  sentmailglobal(emp_email,body,subject,'yatm.opshead@yashaswigroup.in');
        toastr.success("Insert Successful", "", "success")
        $('#EmpRegno').val('0');

        $('.modalinput').val('');
        $('#noofvacancy').val('');
        return true;
    }

    else if (data[0][0].ReturnValue == "0") {

        toastr.warning("Already Exist", "", "info")
        return true;
    }


}
jQuery('#jfddl').on('change', function () {
   
        FillJFEmpReg();
    
   
});

         function FillJFEmpReg(){
            var MasterData ={
                "p_JobFair_ID":jQuery("#jfddl").val()==null?0:jQuery("#jfddl").val().split('aspr')[0],         
                "p_Emp_Regno":"0",         
                "p_Flag":'2',
                "p_VacancyCount":'0',
                "p_NotRegisteredId":'0'     
                        };
                    MasterData = JSON.stringify(MasterData)
                    var path = serverpath + "secured/EmpRegister";
              securedajaxpost(path, 'parsedatasecuredJFEmpReg', 'comment', MasterData, 'control')
              }
        function parsedatasecuredJFEmpReg(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillJFEmpReg();
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    var appenddata="";
                 
                    for (var i = 0; i < data1.length; i++) {
                
               appenddata += "<tr><td>" + data1[i].Venue+ "</td><td style='    word-break: break-word;'>" + data1[i].Jobfair_FromDt_c+ "</td><td style='    word-break: break-word;'>"+data1[i].Jobfair_ToDt+"</td><td >" + data1[i].Emp_Regno+ "</td><td>"+data1[i].CompName+"</td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=window.location='/SearchCandidate' >Search Candidate</button></td></tr>";
                }jQuery("#tbodyvalue").html(appenddata);  
            }    
                      
        }
        function FillDesignation() {
            var path = serverpath + "adminDesignation/0/0"
            securedajaxget(path,'parsedataFillDesignation','comment','control');
        }
        function parsedataFillDesignation(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDesignation();
            }
            else{
            var data1 = data[0];
            $('#JobDesignation').selectize({
                persist: false,
                createOnBlur: true,
                 valueField: 'Designation',
                labelField: 'Designation',
                searchField: 'Designation',
                options: data1,
                create: true
            });
        }
    }
//     else if (data.status == 401) {
//         toastr.warning("Unauthorized", "", "info")
//         return true;
//     }
//     else {
//         var data1 = data[0];
//         var appenddata = "";

//         for (var i = 0; i < data1.length; i++) {

//             appenddata += "<tr><td>" + data1[i].Venue + "</td><td style='    word-break: break-word;'>" + data1[i].Jobfair_FromDt_c + "</td><td style='    word-break: break-word;'>" + data1[i].Jobfair_ToDt + "</td><td >" + data1[i].Emp_Regno + "</td><td>" + data1[i].CompName + "</td></tr>";
//         } jQuery("#tbodyvalue").html(appenddata);
//     }

// }
function FillDesignation() {
    var path = serverpath + "adminDesignation/0/0"
    securedajaxget(path, 'parsedataFillDesignation', 'comment', 'control');
}
function parsedataFillDesignation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillDesignation();
    }
    else {
        var data1 = data[0];
        $('#JobDesignation').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Designation',
            labelField: 'Designation',
            searchField: 'Designation',
            options: data1,
            create: true
        });
    }
}
function FillLocation() {
    var path = serverpath + "district/19/0/0/0"
    securedajaxget(path, 'parsedataFillLocation', 'comment', 'control');
}
function parsedataFillLocation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillLocation();
    }
    else {
        var data1 = data[0];
        $('#JobLocation').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'DistrictName',
            labelField: 'DistrictName',
            searchField: 'DistrictName',
            options: data1,
            create: true
        });
    }
}

function FillQualification() {
    var path = serverpath + "qualification/0/0/0"
    securedajaxget(path, 'parsedataFillQualification', 'comment', 'control');
}
function parsedataFillQualification(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillQualification();
    }
    else {
        var data1 = data[0];
        $('#Qualification').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Qualif_name',
            labelField: 'Qualif_name',
            searchField: 'Qualif_name',
            options: data1,
            create: true
        });
    }
}

function InsUpdNotRagistered() {

    var MasterData = {

        "p_JobfairEmpRegId": "0",
        "p_JobfairId": jQuery("#jfddl").val(),
        "p_Employer": jQuery("#CompanyName").val(),
        "p_Designation": jQuery("#JobDesignation").val(),
        "p_Qualification": jQuery("#Qualification").val(),
        "p_Location": jQuery("#JobLocation").val(),
        "p_Trade": jQuery("#Trade").val(),
        "p_MinAge": jQuery("#MinAge").val(),
        "p_MaxAge": jQuery("#MaxAge").val(),
        "p_TotalVacancy": jQuery("#NoofVacancy").val(),
        "p_CTC": jQuery("#MonthlyCtc").val(),
        "p_AttachDoc": "0",
        "p_InsertedBy": "0",
  };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "jobfairempreg";
    ajaxpost(path, 'parsedataNotRagistered', 'comment', MasterData, 'control')
}

function parsedataNotRagistered(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdNotRagistered();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetModes()
  
     toastr.success("Insert Successful", "", "success")
        return true;
    }


}

function CheckValidationRegistration(){
  
    
    if(jQuery("#CompanyName").val()==""){
          getvalidated('CompanyName','text','Company Name');
          return false;
      }

      if(jQuery("#JobLocation").val()==""){
        getvalidated('JobLocation','text','Job Location');
        return false;
    }
    if(jQuery("#JobDesignation").val()==""){
        getvalidated('JobDesignation','text','Job Designation');
        return false;
    }
    if(jQuery("#MonthlyCtc").val()==""){
        getvalidated('MonthlyCtc','text','Monthly Ctc');
        return false;
    }
    if(jQuery("#MinAge").val()==""){
        getvalidated('MinAge','text','Min Age');
        return false;
    }
    if(jQuery("#MaxAge").val()==""){
        getvalidated('MaxAge','text','Max Age');
        return false;
    }
    if(jQuery("#Qualification").val()==""){
        getvalidated('Qualification','text','Qualification');
        return false;
    }
    if(jQuery("#NoofVacancy").val()==""){
        getvalidated('NoofVacancy','text','No of Vacancy');
        return false;
    }
    if(jQuery("#Trade").val()==""){
        getvalidated('Trade','text','Trade');
        return false;
    }
      else{
        InsUpdNotRagistered()
            
      }
  }

  function resetModes() {
   
   
        jQuery("#CompanyName").val("")
        $("#JobLocation").data("");
         $("#JobDesignation").data("");
         jQuery("#MonthlyCtc").val("")
        $("#MinAge").val("");
         $("#MaxAge").val("");
         jQuery("#Qualification").data("")
         $("#NoofVacancy").val("");
          $("#Trade").val("");
    }
    function FillJFEmpNotReg(){
        var MasterData ={
            "p_JobFair_ID":jQuery("#jfddl").val(),         
            "p_Emp_Regno":"0",         
            "p_Flag":'3',
            "p_VacancyCount":$('#noofvacancy').val(),
            "p_NotRegisteredId":'0'
                    
                    };
                MasterData = JSON.stringify(MasterData)
                var path = serverpath + "secured/EmpRegister";
          securedajaxpost(path, 'parsedatasecuredFillJFEmpNotReg', 'comment', MasterData, 'control')
          }
    function parsedatasecuredFillJFEmpNotReg(data){  
        data = JSON.parse(data)
        console.log(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillJFEmpReg();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
             
                for (var i = 0; i < data1.length; i++) {
             
           appenddata += "<tr><td>" + data1[i].Venue+ "</td><td style='    word-break: break-word;'>" + data1[i].Jobfair_FromDt+ "</td><td style='    word-break: break-word;'>"+data1[i].Jobfair_ToDt+"</td><td >" + data1[i].JobfairEmpRegId+ "</td><td>"+data1[i].Employer+"</td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=SearchCandidate('"+encodeURI(data1[i].Designation)+"','"+encodeURI(data1[i].Qualification)+"','"+encodeURI(data1[i].Location)+"') >Search Candidate</button></td></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
                  
    }
    function SearchCandidate(Desg,Edu,Location){
        sessionStorage.Designation=decodeURI(Desg);
        sessionStorage.Education=decodeURI(Edu);
        sessionStorage.Location=decodeURI(Location);
        sessionStorage.SearchCandidate='Y'
        window.location='/SearchCandidate'
    }




    function fetchInActiveEmp(){
        if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7' || sessionStorage.User_Type_Id=='36'){
            var type='Olex'
        }
        else{
            var type='Yashaswi'
        }
        var path =  serverpath +"EmpRegbyEntryby/"+type+""
        ajaxget(path,'parsedatafetchInActiveEmp','comment',"control");
        }
        function parsedatafetchInActiveEmp(data,control){  
          data = JSON.parse(data)
          if (data.message == "New token generated"){
              sessionStorage.setItem("token", data.data.token);
            //   fetchInActiveEmp('0','1','Fetch')
            fetchInActiveEmp();
          }
          else if (data.status == 401){
              toastr.warning("Unauthorized", "", "info")
              return true;
          }
             else if(data.errno) {
                  toastr.warning("Something went wrong Please try again later", "", "info")
                  return false;
              }
          
              else{
                  jQuery("#EmpRegno").empty();
                  var data1 = data[0];
                  jQuery("#EmpRegno").append(jQuery("<option ></option>").val("0").html("Select Employer"));
                  for (var i = 0; i < data1.length; i++) {
                      jQuery("#EmpRegno").append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
                   }
              }
                    
        }
      
        function FillEmployerByExchange() {

            var path = serverpath + "PlacementEmployer";
            ajaxget(path, 'parsedataEmployerByExchange', 'comment', 'control')
        }
        
        function parsedataEmployerByExchange(data) {
            data = JSON.parse(data)
        
        
        
            jQuery("#EmpRegno").empty();
            var data1 = data[0];
        console.log(data)
        
            jQuery("#EmpRegno").append(jQuery("<option></option>").val('0').html("Select"));
        
            for (var i = 0; i < data1.length; i++) {
                jQuery("#EmpRegno").append(jQuery("<option></option>").val(`${data1[i].Emp_Regno}_${data1[i].Email}`).html(data1[i].CompName));
            }
        
        }


         
   function InsUpdNotRagisteredemp() {

    var MasterData = {
    "p_EmpName": jQuery("#name").val(),
    "p_MobileNumber":jQuery("#mobile").val(),
    "p_Email": jQuery("#mail").val(),
    "p_EntryBy": sessionStorage.Emp_Id,
    "p_IpAddress" : sessionStorage.ipAddress,
    "p_Type":sessionStorage.EmpPartType
    
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "notregisteredemp";
    securedajaxpost(path, 'parsrdataNotRagisteredemp', 'comment', MasterData, 'control')
    }
    
    
    function parsrdataNotRagisteredemp(data) {
    data = JSON.parse(data)
    console.log(data);
     //FillJFEmpReg()
        //var emp_email=$('#EmpRegno').val().split('_')[1];
        //console.log(emp_email)
        if(data[0][0].ReturnValue=='1')
        resetModes12()
        {
            InsJFEmpReg('0',data[0][0].EmpId);
     
               
                  $('#m_modal_6').modal('toggle');
                //  toastr.success('Register Successfully');
                }
    }
    
    function CheckValidationEmployer(){
  
        if(jQuery("#jfemp").val()=="0"){
            toastr.warning('Please Select  Exchange Name')

              return false;
          }
          if(jQuery("#jfddl").val()=="0"){
            toastr.warning('Please Select  Jobfair')

              return false;
          }
        if(jQuery("#name").val()==""){
            toastr.warning('Please Enter  Name')

              return false;
          }
    
          if(jQuery("#mobile").val()==""){
            toastr.warning('Please Enter  Mobile Number')
            return false;
        }
        if(jQuery("#mail").val()==""){
            toastr.warning('Please Enter  MailId')
            return false;
        }
      
      
          else{
            InsUpdNotRagisteredemp()
                
          }
      }

      
  function resetModes12() {
   
   
    jQuery("#name").val("")
    $("#mobile").data("");
     $("#mail").data("");
  
}


function CheckValidationEmployerjobfair(){
  
    if(jQuery("#jfemp").val()=="0"){
        toastr.warning('Please Select  Exchange Name')

          return false;
      }
      if(jQuery("#jfddl").val()=="0"){
        toastr.warning('Please Select  Jobfair')

          return false;
      }
    if(jQuery("#EmpRegno").val()=="0"){
        toastr.warning('Please Select Employer')

          return false;
      }

  
  
      else{
        InsJFEmpReg($('#EmpRegno').val().split('_')[0],'0')
            
      }
  }

  

  