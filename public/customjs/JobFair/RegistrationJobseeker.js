jQuery('#jfemp').on('change', function () {
    FillJobFairDDL(jQuery('#jfemp').val());

});

function FillJobFairDDL(Ex_Id) {
    var path = serverpath + "secured/exchjfdtls/0/" + Ex_Id + "/0"
    securedajaxget(path, 'parsedatasecuredFillJobFairDDL', 'comment', "control");
}
function parsedatasecuredFillJobFairDDL(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDDL(jQuery('#jfemp').val());
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        jQuery("#jfddl").empty();
        var data1 = data[0];

        for (var i = 0; i < data1.length; i++) {
            jQuery("#jfddl").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title + ' ' + data1[i].Jobfair_FromDt + ' To ' + data1[i].Jobfair_ToDt));
        }
        // if(sessionStorage.User_Type_Id=='31'){
        //     FillJFEmpNotReg()
        // }
        // else{
            FillJFEmpReg();
       // }
      

    }
}

function InsJFEmpReg() {
    if($('#EmpRegno').val()==''){
        getvalidated('EmpRegno','text','Registration No')
        return false;
    }
    else{
        var MasterData = {
            "p_Jobfair_Id": $('#jfddl').val(),
            "p_RegistrationId": $('#EmpRegno').val(),
            "p_Flag": '1'
        
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/JobSeekerRegister";
        securedajaxpost(path, 'parsrdataitInsJFEmpReg', 'comment', MasterData, 'control')
    }
  
}
function parsrdataitInsJFEmpReg(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsJFEmpReg();
    }

    else if (data[0][0].ReturnValue == "1") {
        FillJFEmpReg()
        toastr.success("Insert Successful", "", "success")
        $('#EmpRegno').val('')
        return true;
    }

    else if (data[0][0].ReturnValue == "0") {

        toastr.warning("Already Exist", "", "info")
        return true;
    }


}
jQuery('#jfddl').on('change', function () {
   
        FillJFEmpReg();
    
   
});

         function FillJFEmpReg(){
            var MasterData ={
                "p_Jobfair_Id": $('#jfddl').val(),
                "p_RegistrationId": $('#EmpRegno').val(),
                "p_Flag": '2'
                
                        };
                    MasterData = JSON.stringify(MasterData)
                    var path = serverpath + "secured/JobSeekerRegister";
              securedajaxpost(path, 'parsedatasecuredJFEmpReg', 'comment', MasterData, 'control')
              }
        function parsedatasecuredJFEmpReg(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillJFEmpReg();
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    var appenddata="";
                 
                    for (var i = 0; i < data1.length; i++) {
                 
               appenddata += "<tr><td>" + data1[i].Venue+ "</td><td style='    word-break: break-word;'>" + data1[i].Jobfair_FromDt_c+ "</td><td style='    word-break: break-word;'>"+data1[i].Jobfair_ToDt+"</td><td >" + data1[i].RegistrationId+ "</td><td>"+data1[i].CandidateName+"</td></tr>";
                }jQuery("#tbodyvalue").html(appenddata);  
            }    
                      
        }


  