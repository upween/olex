function ChangePassword(){
	
	var MasterData ={

    "p_CandidateId":sessionStorage.JSCandidateId,
    "p_Password":sessionStorage.JSPassword,
    "p_NewPassword":$("#confirmPass").val(),
       
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/changepassword";
securedajaxpost(path, 'parsrdataChangePassword', 'comment', MasterData, 'control')
}


function parsrdataChangePassword(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			ChangePassword();
	}
	else if (data[0][0].ReturnValue == "1") {
        reset();
        toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        reset();
	}
	
 
}

function reset() {

$("#changePass").val(""),
$("#confirmPass").val("")

}