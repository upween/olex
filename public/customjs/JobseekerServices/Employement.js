// function CheckEmployement()     
//  {
//     if ($("#iscurrentcompany").val() == "1") {
//     if (jQuery('#Designation').val() == "" ) {
//         getvalidated('Designation','text','Designation')
//     return false;}
//     if (sessionStorage.getItem("DesignationId") == '0') {
//         $("#Designation").css('border-color', 'red');
//         $("#validDesignation").html("Please enter proper Designation ");
//         return false;}
//     if (jQuery('#Organization').val() == '') {
//         getvalidated('Organization','text','Organization')
//     return false;} 
   
//     if (jQuery('#startedworkinginyear').val() == '0') {
//         getvalidated('startedworkinginyear','select','Started work in year') 
//     return false;}
//     if (jQuery('#startedworkinginmonth').val() == '0') {
//         getvalidated('startedworkinginmonth','select','Started work in Month')
//     return false;}
//     if (jQuery('#inlacs').val() == '0') {
//         getvalidated('inlacs','select','Salary in lacs') 
//     return false;}
//     if (jQuery('#inthousands').val() == '0') {
//         getvalidated('inthousands','select','Salary in Thousand')
//     return false;}
//     if (jQuery('#JobProfile').val() == '') {
//         getvalidated('JobProfile','text','JobProfile')
//     return false;}
//     if (jQuery('#noticeperiod').val() == '0') {
//         getvalidated('noticeperiod','select','Notice Period')
//     return false;}
//     else{
//         InsUpdEmployement();
//     }

//     }
//  else if ($("#iscurrentcompany").val() == "2" || $("#iscurrentcompany").val() == "0" ) {
//         if (jQuery('#Designation').val() == "" ) {
//             getvalidated('Designation','text','Designation')
//         return false;}
//         if (sessionStorage.getItem("DesignationId") == '0') {
//             $("#Designation").css('border-color', 'red');
//             $("#validDesignation").html("Please enter proper Designation ");
//             return false;}
//         if (jQuery('#Organization').val() == '') {
//             getvalidated('Organization','text','Organization')
//         return false;} 
        
//         if (jQuery('#startedworkinginyear').val() == '0') {
//             getvalidated('startedworkinginyear','select','Started work in year') 
//         return false;}
//         if (jQuery('#startedworkinginmonth').val() == '0') {
//             getvalidated('startedworkinginmonth','select','Started work in Month')
//         return false;}
//         if (jQuery('#workinyear').val() == '0') {
//             getvalidated('workinyear','select','Work Till in year')
//         return false;}
//         if (jQuery('#workinmonth').val() == '0') {
//             getvalidated('workinmonth','select','Work Till in Month')
//         return false;}
//         if (jQuery('#inlacs').val() == '0') {
//             getvalidated('inlacs','select','Salary in lacs') 
//         return false;}
//         if (jQuery('#inthousands').val() == '0') {
//             getvalidated('inthousands','select','Salary in Thousand')
//         return false;}
//         if (jQuery('#JobProfile').val() == '') {
//             getvalidated('JobProfile','text','JobProfile')
//         return false;}
//         else{
//             InsUpdEmployement();
//         }
//         }
    
    
    
// }
function FillDesignationSecured(funct) {
    var path = serverpath + "secured/designation/0/'" + sessionStorage.getItem("IpAddress") + "'/0/'" + sessionStorage.getItem("JSCandidateId") + "'"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasecureddesignation(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillDesignationSecured('parsedatasecureddesignation');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        if (data.length > 0) {
            sessionStorage.setItem('Designation', JSON.stringify(data[0]));
        }
    }
}
jQuery("#Designation").keyup(function () {
    sessionStorage.setItem('DesignationId', '0');
});

jQuery("#Designation").typeahead({
    source: function (query, process) {
        var data = sessionStorage.getItem('Designation');
        designation = [];
        map = {};
        var Designation = "";
        
        jQuery.each(jQuery.parseJSON(data), function (i, Designation) {
            map[Designation.DesignationName] = Designation;
           designation.push(Designation.DesignationName);
        
        });
        process(designation);
    },
    minLength: 3,
    updater: function (item) {
        sessionStorage.setItem('DesignationId', map[item].DesignationId);
        return item;
    },
    
    
});

function InsUpdEmployement() {
    if ($("#iscurrentcompany").val() == "1") {
        var MasterData = {
            "p_EmploymentId": localStorage.getItem("EmployementId"),
            "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
            "p_DesignationId": sessionStorage.getItem("DesignationId"),
            "p_Organisation": jQuery('#Organization').val(),
            "p_IsCurrentCompany": jQuery('#iscurrentcompany option:selected').val(),
            "p_StartedWorkinYear": jQuery('#startedworkinginyear').val(),
            "p_StartedWorkinMonth": jQuery('#startedworkinginmonth').val(),
            "p_WorkedTillinYear": "0",
            "p_WorkedTillinMonth":"0",
            "p_SalaryinLacs": jQuery('#inlacs').val(),
            "p_SalaryinThousand":jQuery('#inthousands').val(),
            "p_ProfileDescription": jQuery('#JobProfile').val(),
            "p_NoticePeriod": jQuery('#noticeperiod option:selected').text()

        }
    }
    else if ($("#iscurrentcompany").val() == "2") {
        var MasterData = {
            "p_EmploymentId": localStorage.getItem("EmployementId"),
            "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
            "p_DesignationId": sessionStorage.getItem("DesignationId"),
            "p_Organisation": jQuery('#Organization').val(),
            "p_IsCurrentCompany": jQuery('#iscurrentcompany option:selected').val(),
            "p_StartedWorkinYear": jQuery('#startedworkinginyear').val(),
            "p_StartedWorkinMonth": jQuery('#startedworkinginmonth').val(),
            "p_WorkedTillinYear": jQuery('#workinyear').val(),
            "p_WorkedTillinMonth": jQuery('#workinmonth').val(),
            "p_SalaryinLacs": jQuery('#inlacs').val(),
            "p_SalaryinThousand":jQuery('#inthousands').val(),
            "p_ProfileDescription": jQuery('#JobProfile').val(),
            "p_NoticePeriod": ''

        }
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/employement";
    securedajaxpost(path, 'parsrdataemployement', 'comment', MasterData, 'control')
}
function parsrdataemployement(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdEmployement();
    }

    else if (data[0][0].ReturnValue == "1") {
        ProfileStatus('parsedataProfileStatus','progressbar');
        toastr.success("Submit Successfully", "", "success")
        FillEmployementSecured('parsedatasecuredemployement');
        resetmodeEmployment();
        return true;
    }

    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Update Successfully", "", "success")
        $('#myModal1').modal('toggle');
        FillEmployementSecured('parsedatasecuredemployement');
        resetmodeEmployment();
        return true;
    }
    else if (data[0][0].ReturnValue == "0") {
      toastr.warning("Employement Detail already Exists", "", "info")
      return true;
    }
}
function FillEmployementSecured(funct, control) {
    var path = serverpath + "secured/employement/'" + sessionStorage.getItem("JSCandidateId") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredemployement(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillEmployementSecured('parsedatasecuredemployement');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        if (data[0] == "") {
            localStorage.setItem("EmployementId", "0");
            $("#EmpExperience").hide();
            $("#EmpDesignation").hide();
        }
        else {

           sessionStorage.setItem("StartedWorkinYear",StartedWorkinYear);
           localStorage.setItem("StartedWorkinMonth",StartedWorkinMonth);
           sessionStorage.setItem("WorkedTillinYear",WorkedTillinYear);
           localStorage.setItem("WorkedTillinMonth",WorkedTillinMonth);

            sessionStorage.setItem("Organisation",Organisation);
            localStorage.setItem("EmployementId",EmployementId);
            jQuery('#Designation').val(DesignationName),
            jQuery('#Organization').val(Organisation),
            jQuery('#iscurrentcompany').val(Iscurrentcompany),
            jQuery('#startedworkinginyear').val(StartedWorkinYear),
            jQuery('#startedworkinginmonth').val(StartedWorkinMonth),
            jQuery('#workinyear').val(WorkedTillinYear),
            jQuery('#workinmonth').val(WorkedTillinMonth),
            jQuery('#inlacs').val(SalaryinLacs),
            jQuery('#inthousands').val(SalaryinThousand),
            jQuery('#JobProfile').val(JobProfile),
            jQuery('#noticeperiod').val(NoticePeriod)
            $("#Designation").prop("disabled", true);
            $("#Organization").prop("disabled", true);
          
            if ((Iscurrentcompany)== "1") {
                $("#workinyear").hide();
                $("#workinmonth").hide();
               $("#present").show();
            }
            else if ((Iscurrentcompany) == "2") {
                $("#workinyear").show();
                $("#workinmonth").show();
                
                $("#present").hide();
        
            }
        }
    }
}
// function EditEmpDetail(EmployementId,DesignationName,Organisation,Iscurrentcompany,StartedWorkinYear,StartedWorkinMonth,WorkedTillinYear,WorkedTillinMonth,SalaryinLacs,SalaryinThousand,JobProfile,NoticePeriod) {
 
//     modal('employement');
//     localStorage.setItem("EmployementId",EmployementId);
//      jQuery('#Designation').val(decodeURI(DesignationName)),
//      jQuery('#Organization').val(decodeURI(Organisation)),
//      jQuery('#iscurrentcompany').val(decodeURI(Iscurrentcompany)),
//      jQuery('#startedworkinginyear').val(decodeURI(StartedWorkinYear)),
//      jQuery('#startedworkinginmonth').val(decodeURI(StartedWorkinMonth)),
//      jQuery('#workinyear').val(decodeURI(WorkedTillinYear)),
//      jQuery('#workinmonth').val(decodeURI(WorkedTillinMonth)),
//      jQuery('#inlacs').val(decodeURI(SalaryinLacs)),
//      jQuery('#inthousands').val(decodeURI(SalaryinThousand)),
//      jQuery('#JobProfile').val(decodeURI(JobProfile)),
//      jQuery('#noticeperiod').val(decodeURI(NoticePeriod))
//      $("#Designation").prop("disabled", true);
//      $("#Organization").prop("disabled", true);

//      if (decodeURI(Iscurrentcompany)== "1") {
//         $("#workingyr").hide();
//         $("#present").show();
       
//         $("#ntcperd").show();
//     }
//     else if (decodeURI(Iscurrentcompany) == "2") {
//         $("#workingyr").show();
//         $("#present").hide();
        
//         $("#ntcperd").hide();

//     }
// }

// function resetmodeEmployment() {
//     localStorage.setItem("EmployementId",'0'),
   
//     $("#Designation").prop("disabled", false);
//     $("#Organization").prop("disabled", false);
//     jQuery('#Designation').val(""),
//     jQuery('#Organization').val(""),
//     jQuery('#iscurrentcompany').val("0"),
//     jQuery('#startedworkinginyear').val("0"),
//     jQuery('#startedworkinginmonth').val("0"),
//     jQuery('#workinyear').val("0"),
//     jQuery('#workinmonth').val("0"),
//     jQuery('#inlacs').val("0"),
//     jQuery('#inthousands').val("0"),
//     jQuery('#JobProfile').val(""),
//     jQuery('#noticeperiod').val("0")
//     $("#present").hide();
   
//      $("#ntcperd").hide();

//      jQuery('#Designation').css('border-color', '');
//      jQuery('#Organization').css('border-color', '');
//      jQuery('#iscurrentcompany').css('border-color', '');
//      jQuery('#startedworkinginyear').css('border-color', '');
//      jQuery('#startedworkinginmonth').css('border-color', '');
//      jQuery('#JobProfile').css('border-color', '');

//          jQuery('#workinyear').css('border-color', '');
//         $('#validworkinyear').html("");
//         jQuery('#workinmonth').css('border-color', '');
//         $('#validworkinmonth').html("");

//         jQuery('#inlacs').css('border-color', '');
//         $('#validinlacs').html("");
//         jQuery('#inthousands').css('border-color', '');
//         $('#validinthousands').html("");

//      $('#validDesignation').html("");
//      $("#validOrganization").html("");
//      $("#validiscurrentcompany").html("");
//      $("#validstartedworkinginyear").html("");
//      $("#validstartedworkinginmonth").html("");
//      $("#validJobProfile").html("");
// }

function FillIscurrentcompany(funct,control) {
    var path =  serverpath + "secured/Status/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillIscurrentcompany(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillIscurrentcompany('parsedatasecuredFillIscurrentcompany','iscurrentcompany');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html(" Is your Current Company? "));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Status_id).html(data1[i].Status_name));
             }
        }
              
}


function Fillstartedworkinginyear(funct,control) {
    var path =  serverpath + "secured/year/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillstartedworkinginyear(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillstartedworkinginyear('parsedatasecuredFillstartedworkinginyear');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#startedworkinginyear").empty();
            var data1 = data[0];
            jQuery("#startedworkinginyear").append(jQuery("<option ></option>").val("0").html("Select Started Working From Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#startedworkinginyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

             jQuery("#workinyear").empty();
             var data1 = data[0];
             jQuery("#workinyear").append(jQuery("<option ></option>").val("0").html("Worked Till Year"));
             for (var i = 0; i < data1.length; i++) {
                 jQuery("#workinyear").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
              }
        }
              
}

function FillStartedworkinginmonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillStartedworkinginmonth(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillStartedworkinginmonth('parsedatasecuredFillStartedworkinginmonth');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#startedworkinginmonth").empty();
            var data1 = data[0];
            jQuery("#startedworkinginmonth").append(jQuery("<option ></option>").val("0").html("Started Working From Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#startedworkinginmonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
             }

             jQuery("#workinmonth").empty();
             var data1 = data[0];
             jQuery("#workinmonth").append(jQuery("<option ></option>").val("0").html("Worked Till Month"));
             for (var i = 0; i < data1.length; i++) {
                 jQuery("#workinmonth").append(jQuery("<option></option>").val(data1[i].ShortMonthName).html(data1[i].ShortMonthName));
              }
        }
              
}

function FillSalaryinlacs(funct,control) {
    var path =  serverpath + "secured/salary/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSalaryinlacs(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSalaryinlacs('parsedatasecuredFillSalaryinlacs');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#inlacs").empty();
            var data1 = data[0];
            jQuery("#inlacs").append(jQuery("<option ></option>").val("0").html("Salary in Lacs"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#inlacs").append(jQuery("<option></option>").val(data1[i].salary_l).html(data1[i].salary_l));
             }

             jQuery("#inthousands").empty();
             var data1 = data[0];
             jQuery("#inthousands").append(jQuery("<option ></option>").val("0").html("Select Salary in Thousands "));
             for (var i = 0; i < data1.length; i++) {
                 jQuery("#inthousands").append(jQuery("<option></option>").val(data1[i].salary_t).html(data1[i].salary_t));
              }
        }
              
}