function CheckIdentificationDetail() {
    var idno= document.getElementById('cardno');
    if (jQuery('#idtype').val() == "0") {
        getvalidated('idtype','select','Identification Type'); 
    return false;}
    if (jQuery('#cardno').val() == '') {
       
       
  
    if (jQuery('#idtype').val() == 'Driving Licence'){
        getvalidated('cardno','text','Driving License Number'); 
        return false; }
    if (jQuery('#idtype').val() == 'Electric Licence'){
        getvalidated('cardno','text','Electric License Number'); 
        return false;  }

}
if (jQuery('#idtype').val()  == 'Driving Licence' && idno.value.length < 8 && idno.value.length > 0 ) {
    checkLength('cardno','Driving license Number','8');
    return false;
}

if (jQuery('#idtype').val()  == 'Electric Licence'  && idno.value.length < 8 && idno.value.length > 0 ) {
    checkLength('cardno','Electric License Number','8');
    return false;
}
    else {
        
      InsUpdIdentificationDetail();
    }
}

function InsUpdIdentificationDetail() {
    var MasterData = {
        "p_IdentificationId": localStorage.getItem("IdentificationId"),
        "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
        "p_IdentificationType": 'Driving License',
        "p_IdentificationNumber": jQuery('#cardno').val(),
        "p_Date": jQuery('#IdDate').val(),
        "p_Upto": jQuery('#ValidUpto').val(),
        "p_IssuingAuthority": jQuery('#IssuingAuthority').val(),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
        "p_LicenseType":$('#licensetype').val(),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/identificationdetails";
    securedajaxpost(path, 'parsrdataidentificationdetails', 'comment', MasterData, 'control')
}
function parsrdataidentificationdetails(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdIdentificationDetail();
    }
    else if (data[0][0].ReturnValue == "0") {
        toastr.warning("Card Detail Already Exist", "", "info")
        resetmodeIdedntification();
        return true;
    }
    else if (data[0][0].ReturnValue == "1") {
        
       
        toastr.success("Submit Successfully", "", "success")
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails');
        return true;
    }
    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Submit Successfully", "", "success")
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails');
          return true;
    }

}
function FillIdentificationDetailSecured(funct, control) {
    var path = serverpath + "secured/identificationdetails/0/'" + sessionStorage.getItem("JSCandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredidentificationdetails(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillIdentificationDetailSecured('parsedatasecuredidentificationdetails');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        
        if (data[0] == "") {
            localStorage.setItem("IdentificationId", "0");
           
        }
        else {
            
            jQuery('#idtype').val(IdentificationType),
            jQuery('#cardno').val(IdentificationNumber),
            jQuery('#IdDate').val(Date),
            jQuery('#ValidUpto').val(Upto),
            jQuery('#IssuingAuthority').val(Authority)
        }
    }
}

// function EditIdDetail(IdentificationId, IdentificationType, IdentificationNumber, Date, Upto, Authority,LicenseType) {//Sonali
//     modal('dld');
//     $("#111").html("<h5>Edit-Identification Details</h5>");
//     $("#idtype").prop("disabled", true);
//         localStorage.setItem("IdentificationId", IdentificationId);
//         jQuery('#idtype').val(decodeURI(IdentificationType)),
//         jQuery('#cardno').val(decodeURI(IdentificationNumber)),
//         jQuery('#IdDate').val(decodeURI(Date)),
//         jQuery('#ValidUpto').val(decodeURI(Upto)),
//         jQuery('#IssuingAuthority').val(decodeURI(Authority))
//         $('#licensetype').empty();
//         if(decodeURI(IdentificationType)=='Driving Licence'){
//             $('#licensetype').append('<option>LMV Commercial</option>');
//             $('#licensetype').append('<option>HMV</option>');
//             $('#licensetype').val(decodeURI(LicenseType));
           
//         }else{
//             $('#licensetype').append('<option>Domestic</option>');
//             $('#licensetype').append('<option>Industrial</option>');
//             $('#licensetype').val(decodeURI(LicenseType));
           
//         }
     

// }

// function resetmodeIdedntification() {
//     localStorage.setItem("IdentificationId", "0");
//     jQuery('#idtype').val("0"),
//         jQuery('#cardno').val(""),
//         jQuery('#IdDate').val(""),
//         jQuery('#ValidUpto').val(""),
//         jQuery('#IssuingAuthority').val("")
//         $("#idtype").prop("disabled", false);
//         $('#licensetype').empty();
//         $("#111").html("<h5>Identification Details</h5>");
//         $('#licensetype').append('<option>License Type</option>');
//         $("#cardno").attr("placeholder", "Identification Number");
        
//         jQuery('#idtype').css('border-color', '');
//         jQuery('#cardno').css('border-color', '');
                
//         $('#valididtype').html("");
//         $("#validcardno").html("");
        
// }

// function placeholder() {
//     debugger;
//     var sel = document.getElementById("idtype");
//     var textbx = document.getElementById("cardno");
//     var indexe = sel.selectedIndex;
//     $("#validcardno").html("");
//     $("#cardno").css('border-color', '');
//     $("#cardno").val("");
//     if (indexe == 0) {
//         $("#cardno").attr("placeholder", "Identification Number");

//     }
    
//     if (indexe == 1) {
//         $("#cardno").attr("placeholder", "Driving license Number");
//     }
//     if (indexe == 2) {
//         $("#cardno").attr("placeholder", "Electric License Number");
//     }
// }

// $("#cardno").focusout(function () {
   
//     var sel = document.getElementById("idtype");
//     var textbx = document.getElementById("cardno");
//     var indexe = sel.selectedIndex;
//     var Idnumb = $("#cardno").val();
//     if (Idnumb == '') {
        

//         if (indexe == 1) {
//             getvalidated('cardno','text','Driving license Number')
//         }

//         if (indexe == 2) {
//             getvalidated('cardno','text','Electric License Number')
//         }
//     }
//         else{
            
//         if (indexe == 1) {
//             checkLength('cardno','Driving license Number','8')
//         }

//         if (indexe == 2) {
//             checkLength('cardno','Electric License Number','8')
//         }
            
//         }

    
// });

// $('#idtype').on('change', function(){
//     $('#licensetype').html('');
//     if($('#idtype').val()=='Driving Licence'){
//         $('#licensetype').append('<option>LMV Commercial</option>');
//         $('#licensetype').append('<option>HMV</option>');
       
//     }else if($('#idtype').val()=='Electric Licence'){
//         $('#licensetype').append('<option>Domestic</option>');
//         $('#licensetype').append('<option>Industrial</option>');
       
//     }else{
//         jQuery('#licensetype').append('<option>License Type</option>');;
//     }
// });

