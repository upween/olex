
  


   function FillPersonalDetailSecured(funct,control) {
          var path = serverpath +  "secured/personaldetails/'" + sessionStorage.getItem("JSCandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("JSCandidateId") + "'"
          securedajaxget(path,funct,'comment',control);            
      }

    function parsedatasecuredpersonaldetails(data){  
          data = JSON.parse(data)
          if (data.message == "New token generated"){
              sessionStorage.setItem("token", data.data.token);
              FillPersonalDetailSecured('parsedatasecuredpersonaldetails');
          }
     
          else{

            
              var data1 = data[0];
            
              sessionStorage.setItem("PersonalDetailId",data1[0].PersonalDetailsId)
              $("#DOBtextbox").val(data1[0].DateOfBirth)
             $("#resident").val(data1[0].Resident_id)
             $("#permanentaddress").val(data1[0].PermanentAddress),
             $("#presentaddress").val(data1[0].PresentAddress),
             $("#Pin").val(data1[0].PinCode),
             $("#BloodGroup").val(data1[0].BloodGroup),
             $("#Marital").val(data1[0].MaritalStatus),
             $("#Category").val(data1[0].CategoryId),
             $("#Religion").val(data1[0].Religion_id),
             $("#SubCaste").val(data1[0].SubCaste),
             $("#nationality").val(data1[0].Nationality_id),
             $("#Area").val(data1[0].Area),
             $("#Proficiency").val(data1[0].Proficiency),
             $("#differentlyabled").val(data1[0].DifferentlyAbled),
             $("#AreaOfInterest").val(data1[0].AreaInterest)
             
              
              if(data1[0].DifferentlyAbled=="Handicap"){
                $("#handiCategory1").show();
                $("#handiCategory2").show();
                $("#DisablityPrio").show();
                $("#DisabilityDetail").val(data1[0].DifferentlyAbledType)
                $("#HandicappedSubCategory").val(data1[0].DiffAbledsubType)
                $("#DisablityPriority").val(data1[0].DisablityPriority)
               
  
              }
              else{
                $("#handiCategory1").hide();
                $("#handiCategory2").hide();
                $("#DisablityPrio").hide();
                $("#DisabilityDetail").val("0")
                $("#HandicappedSubCategory").val("0")
                $("#DisablityPriority").val("0")
              }
             
      
      }
    }
   
  
  
  // function DOBCheck(){
  // if (jQuery('#DOB').val() != '') {
  //   var dob = jQuery('#DOB').val();
  //   var d1 = new Date();
  //   var d2 = new Date(dob);
  //   var years = d1.getFullYear() - d2.getFullYear();
  //   if (years < 14) {
  //       $("#validDOB").html("DOB should be 14+");
  //       jQuery('#DOB').css('border-color', 'red');
  //       return false;
  //   }
  //   else{
  //     $("#validDOB").html("");
  //     jQuery('#DOB').css('border-color', '');
  //   }
  // }
  // else{
  //   getvalidated('DOB','text','DoB');
  // }
  // }
  
  
  function FillMaritalstatus(funct,control) {
    var path =  serverpath + "secured/Maritalstatus/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillMaritalstatus(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMaritalstatus('parsedatasecuredFillMaritalstatus','Marital');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Marital Status"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Marital_id).html(data1[i].Marital_Status));
             }
        }
              
  }
  
 
  function FillBloodGroup(funct,control) {
    var path =  serverpath + "secured/BloodGroup/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillBloodGroup(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillBloodGroup('parsedatasecuredFillBloodGroup','BloodGroup');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Blood Group"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].bloodgroup_id).html(data1[i].bloodgroup_name));
             }
        }
              
  }
  
  
  function FillCategory(funct,control) {
    var path =  serverpath + "secured/category/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillCategory(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCategory('parsedatasecuredFillCategory');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#Category").empty();
            var data1 = data[0];
            jQuery("#Category").append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Category").append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
             }
        }
              
  }
  
  function FillReligion(funct,control) {
    var path =  serverpath + "secured/Religion/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillReligion(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillReligion('parsedatasecuredFillReligion');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#Religion").empty();
            var data1 = data[0];
            jQuery("#Religion").append(jQuery("<option ></option>").val("0").html("Select Religion"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Religion").append(jQuery("<option></option>").val(data1[i].Religion_id).html(data1[i].Religion_Name));
             }
        }
              
  }
  
  function FillNationality(funct,control) {
    var path =  serverpath + "secured/Nationality/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillNationality(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillNationality('parsedatasecuredFillNationality','nationality');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Nationality"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Nationality_id).html(data1[i].Nationality_name));
             }
        }
              
  }

  function FillAreaOfInterest(funct,control) {
    var path =  serverpath + "secured/AreaOfInterest/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillAreaOfInterest(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillAreaOfInterest('parsedatasecuredFillAreaOfInterest','AreaOfInterest');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Area Of Interest"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].AreaOfInterest_id).html(data1[i].AreaOfInterest));
             }
        }
              
  }

  function FillEmploymentstatus(funct,control) {
    var path =  serverpath + "secured/Employmentstatus/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillEmploymentstatus(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEmploymentstatus('parsedatasecuredFillEmploymentstatus','Proficiency');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Employed Status"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].jobstatus_id).html(data1[i].Employment_Status));
             }
        }
              
  }

  function FillResident(funct,control) {
    var path =  serverpath + "secured/Resident/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillResident(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillResident('parsedatasecuredFillResident','resident');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Domicile"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Resident_id).html(data1[i].Resident_name));
             }
        }
              
  }

  function FillHCCategory(funct,control) {
    var path =  serverpath + "secured/HCCategory/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillHCCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillHCCategory('parsedatasecuredFillHCCategory','DisabilityDetail');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_Category_id).html(data1[i].HC_Description));
             }
        }
              
  }

  function FillHCsubCategory(funct,control) {
    var path =  serverpath + "secured/HCsubCategory/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillHCsubCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillHCsubCategory('parsedatasecuredFillHCsubCategory','HandicappedSubCategory');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Sub Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_SubCategory_id).html(data1[i].HC_Category_Description));
             }
        }
              
  }

  function FillDisablityPriority(funct,control) {
    var path =  serverpath + "secured/DisablityPriority/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillDisablityPriority(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDisablityPriority('parsedatasecuredFillDisablityPriority','DisablityPriority');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Disablity Priority"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DisablityPriority_id).html(data1[i].DisablityPriority_name));
             }
        }
              
  }
  
  function FillArea(funct,control) {
    var path =  serverpath + "secured/Area/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillArea(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillArea('parsedatasecuredFillArea','Area');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Area"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Area_Id).html(data1[i].Area_Name));
             }
        }
              
  }
  
  function FillBlock(funct,control) {
    var path =  serverpath + "secured/city/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  function parsedatasecuredFillBlock(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillBlock('parsedatasecuredFillBlock','Block');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Block"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
             }
        }
              
  }
  
  
  jQuery('#Block').on('change', function () {
    FillPanchayat('parsedatasecuredFillPanchayat','Panchayat',jQuery('#Block').val());
  });
  
  function FillPanchayat(funct,control,city) {
    var path =  serverpath + "secured/village/" + city + "/0/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  function parsedatasecuredFillPanchayat(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillPanchayat('parsedatasecuredFillPanchayat','Panchayat',jQuery('#Block').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Panchayat"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
             }
        }
              
  }
    
  function InsUpdPersonalDetail(){
    var chkval = $("#chkgreencardStatus")[0].checked
    if (chkval == true){
        chkval = "Yes"
    }else{
        chkval="No"
    }

    var MasterData = {
      "p_PersonalDetailsId": sessionStorage.getItem("PersonalDetailId"),
      "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
      "p_DateOfBirth": $("#DOBtextbox").val(),
      "p_Resident": $("#resident").val(),
      "p_PermanentAddress": $("#permanentaddress").val(),
      "p_PresentAddress": $("#presentaddress").val(),
      "p_PinCode":$("#Pin").val(),
      "p_BloodGroup": $("#BloodGroup").val(),
      "p_MaritalStatus":$("#Marital").val(),
      "p_Category":$("#Category").val(),
      "p_Religion":$("#Religion").val(),
      "p_SubCaste":$("#SubCaste").val(),
      "p_Nationality":$("#nationality").val(),
      "p_Language":'0',
      "p_Proficiency":$("#Proficiency").val(),
      "p_DifferentlyAbled":$("#differentlyabled").val(),
      "p_DifferentlyAbledType":$("#DisabilityDetail").val(),
      "p_DiffAbledSubType":$("#HandicappedSubCategory").val(),
      "p_DisablityPriority":$("#DisablityPriority").val(),
      "p_IpAddress":sessionStorage.getItem("IpAddress"),
      "p_UserId":sessionStorage.getItem("CandidateId"),
      "p_GreenCard":chkval,
      "p_Area":$("#Area").val(),
      "p_Block":$("#Block").val(),
      "p_Panchayat":$("#Panchayat").val(),
      "p_Village":'1',
      "p_AreaOfInterest":$("#AreaOfInterest").val(),
      
          }
  
    MasterData =JSON.stringify(MasterData)
    var path = serverpath +"personaldetails";
    ajaxpost(path,'parsrdatainsupdpersonaldetails','comment',MasterData,'control')
}

function parsrdatainsupdpersonaldetails(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      InsUpdPersonalDetail();
  }
    if (data[0][0].ReturnValue == "1") {
      
     
     FillPersonalDetailSecured('parsedatasecuredpersonaldetails');
      toastr.success("Submit Successfully", "", "success")
   
    }
    else if (data[0][0].ReturnValue == "2") {
     
      toastr.success("Submit Successfully", "", "success")
     FillPersonalDetailSecured('parsedatasecuredpersonaldetails');
      return true;
                 }
     }

