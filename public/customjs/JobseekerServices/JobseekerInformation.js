function FetchJobseekerInformation() {
	var path = serverpath + "JobseekerProfile/"+sessionStorage.JSCandidateId+""
	ajaxget(path, 'parsedataFetchJobseekerInformation', 'comment', "control");
}


function parsedataFetchJobseekerInformation(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchJobseekerInformation('parsedataFetchJobseekerInformation');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            for (var j = 0; j < data.length; j++) {    
                var appenddata="";
                if (j==0){
                    jQuery("#NextRenewalDate").html(data[j][0].renewal_date);
                    jQuery("#RegistrationNumber").html(data[j][0].RegistrationId);
                    jQuery("#RegistrationDate").html(data[j][0].RegDate);
                    jQuery("#ApplicantName").html(data[j][0].CandidateName);
                    jQuery("#FatherOrHusbandName").html(data[j][0].GuardianFatherName);
                    jQuery("#DateofBirth").html(data[j][0].DateOfBirth);
                    jQuery("#MaritalStatus").html(data[j][0].Marital_Status);
                    jQuery("#Category").html(data[j][0].CategoryName);
                    jQuery("#Gender").html(data[j][0].Gender);
                    jQuery("#Username").html(data[j][0].UserName);
                    jQuery("#Area").html(data[j][0].Area_Name);
                    jQuery("#Address").html(data[j][0].PermanentAddress);
                    jQuery("#District").html(data[j][0].DistrictName);
                    jQuery("#City").html(data[j][0].CityName);
                    jQuery("#PinCode").html(data[j][0].PinCode);
                    jQuery("#MobileNo").html(data[j][0].MobileNumber);
                    // jQuery("#Std").html(data[j][0].);
                    // jQuery("#ContactNo").html(data[j][0].MobileNumber);
                    jQuery("#Emailid").html(data[j][0].EmailId);
                    sessionStorage.JSUserName=data[j][0].UserName;
                }
                if (j==1){
                    for (var i = 0; i < data[j].length; i++) {
                        appenddata += "<tr><td>" + data[j][i].Organisation+ "</td><td>" + data[j][i].DesignationName+ "</td><td>" + data[j][i].StartedWorkinMonth+ ' ' + data[j][i].StartedWorkinYear + "</td><td>" + data[j][i].WorkedTillinMonth+ ' ' + data[j][i].WorkedTillinYear + "</td><td></td></tr>";
                    } 
                    jQuery("#tbodyvalue").html(appenddata);           
                }
                if (j==2){
                    for (var i = 0; i < data[j].length; i++) {
                        appenddata += "<tr><td>" + data[j][i].Qualif_name+ "</td><td>" + data[j][i].Sub+ "</td><td>" + data[j][i].Division+ "</td><td>" + data[j][i].Percentage+ "</td><td>" + data[j][i].UniversityName+ "</td><td>" + data[j][i].Year+ "</td></tr>";
                    } 
                    jQuery("#tbodyvalue2").html(appenddata);           
                }
            }


        }
              
}