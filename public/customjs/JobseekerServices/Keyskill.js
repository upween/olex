// function CheckKeySkill() {
//     if ($("#SkillName").val()== '') {
//         $("#SkillName").css('border-color', 'red');
//         $("#validSkillName").html("Please select Key Skill");
//         return false;
//     }
//     else {
//         $("#SkillName").css('border-color', '');
//         $("#validSkillName").html("");

//         DeleteKeySkill();
//     }
// }
function InsUpdKeySkill() {
    var MasterData = {
        "p_SkillId": localStorage.getItem("SkillId"),
        "p_Skill": $("#keyskilldetail").val(),
        "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
        "p_IpAddress": sessionStorage.getItem("IpAddress"),
        "p_UserId": sessionStorage.getItem("JSCandidateId")
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/keyskill";
    securedajaxpost(path, 'parsrdatakeyskill', 'comment', MasterData, 'control')
}
function parsrdatakeyskill(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsUpdKeySkill();
    }
    else if (data[0][0].ReturnValue == "1") {
        toastr.success("Submit Successfully", "", "success")
        FillKeySkillSecured('parsedatasecuredkeyskill');

        return true;
    }


}
function FillKeySkillSecured(funct, control) {
    var path = serverpath + "secured/keyskill/'" + sessionStorage.getItem("JSCandidateId") + "'/'" + sessionStorage.getItem("IpAddress") + "'/'" + sessionStorage.getItem("JSCandidateId") + "'"
    securedajaxget(path, funct, 'comment', control);
}
function parsedatasecuredkeyskill(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillKeySkillSecured('parsedatasecuredkeyskill');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        if (data[0] == "") {
            localStorage.setItem("SkillId", "0");
        }
        else {
            var data1 = data[0];
            var appenddata="";
            $("#keyskilldetail").val(data1[0].Skill);
            appenddata += "<tr><td>" + data1[0].Organisation + "</td><td>" +data1[0].Skill+ "</td><td>" + data1[0].StartedWorkinMonth + ' ' + data1[0].StartedWorkinYear +"</td><td>" + data1[0].WorkedTillinMonth + ' ' + data1[0].WorkedTillinYear + "</td><td>"+ sessionStorage.ResumeHeadLineId +"</td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' id='btnDelete'>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' id='btnDelete'>Delete</button></td></tr>"
           
           
        }
        jQuery("#tbodyvalue").html(appenddata);
    }        
}
