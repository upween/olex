function InsUpdLanguage(){
    if(jQuery("#Language").val()=="0"){
        getvalidated('Language','select','Language');
        return false;
    }
    else{
    var Read = $("#Read")[0].checked
        if (Read == true){
            Read = "Yes"
        }else{
            Read="No"
        }

        var Write = $("#Write")[0].checked
        if (Write == true){
            Write = "Yes"
        }else{
            Write="No"
        }
        var Speak = $("#Speak")[0].checked
        if (Speak == true){
            Speak = "Yes"
        }else{
            Speak="No"
        }

    var MasterData = {
        "p_LanguageId": localStorage.getItem("LanguageId"),
        "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
        "p_language": jQuery("#Language").val(),
        "p_Read": Read,
        "p_Write": Write,
        "p_Speak":Speak,
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
       }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/Language";
    securedajaxpost(path,'parsrdataInsUpdLanguage','comment',MasterData,'control')
}
}

function parsrdataInsUpdLanguage(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdLanguage();
        
    }
    
    if (data[0][0].ReturnValue == "1") {
        
          FetchLanguage('parsedatasecuredFetchLanguage');
          toastr.success("Insert Successful", "", "success")
          return true;
      }
       else if (data[0][0].ReturnValue == "2") {
        
          FetchLanguage('parsedatasecuredFetchLanguage');
          resetLanguage();
          toastr.success("Update Successful", "", "success")
          return true;
      }
      else if (data[0][0].ReturnValue == "0") {
        resetLanguage();
		
        toastr.warning("Language Detail already Exists ", "", "info")
        return true;
    }
    
     }

    //  function resetLanguage() {
    //     $("#Language").prop("disabled", false);
    //     jQuery("#Language").val("0")
    //     $("#Read").prop("checked", false);
    //     $("#Write").prop("checked", false);
    //     $("#Speak").prop("checked", false);
    //     jQuery('#Language').css('border-color', '');
    //     $('#validLanguage').html("");
    //          }

     function FetchLanguage(funct,control) {
            var path = serverpath + "secured/Language/'0'/'"+sessionStorage.getItem("JSCandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'"
            securedajaxget(path,funct,'comment',control);
        }

    function parsedatasecuredFetchLanguage(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FetchLanguage('parsedatasecuredFetchLanguage');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
               ;
                if(data[0]=="")
                {
                   localStorage.setItem("LanguageId","0")
                }
                else{
                var data1 = data[0];
                localStorage.setItem("LanguageId",data1[0].LanguageId)
               
                $("#Language").val(LanguageId)
                if (Read_L == "Yes") {
                             $("#Read").prop("checked", true);
                        } 
                         else {
                             $("#Read").prop("checked", false);
                         }
                         if (Write_L == "Yes") {
                             $("#Write").prop("checked", true);
                         } 
                         else {
                             $("#Write").prop("checked", false);
                         }
                         if (Speak == "Yes") {
                             $("#Speak").prop("checked", true);
                         } 
                         else {
                             $("#Speak").prop("checked", false);
                         }
        
            }
            }
    }

    // function EditLanguage(LanguageId,Lang_id,Read_L,Write_L,Speak) { 
    //     modal('languagedetails');
        
    //      $("#Language").prop("disabled", true);
    //        localStorage.setItem("LanguageId", LanguageId);     
    //        $("#Language").val(Lang_id)

    //        if (Read_L == "Yes") {
    //         $("#Read").prop("checked", true);
    //     } 
    //     else {
    //         $("#Read").prop("checked", false);
    //     }
    //     if (Write_L == "Yes") {
    //         $("#Write").prop("checked", true);
    //     } 
    //     else {
    //         $("#Write").prop("checked", false);
    //     }
    //     if (Speak == "Yes") {
    //         $("#Speak").prop("checked", true);
    //     } 
    //     else {
    //         $("#Speak").prop("checked", false);
    //     }

    //     }

        
function FillLanguage(funct,control) {
    var path =  serverpath + "secured/Languagetype/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillLanguage(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillLanguage('parsedatasecuredFillLanguage','Language');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Language"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Lang_id).html(data1[i].Lang_name));
             }
        }
              
  }