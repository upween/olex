
  
  function FillProfileDetail(funct) {
    var path = serverpath + "secured/registration/'" + sessionStorage.getItem("JSCandidateId") + "'/0"
    securedajaxget(path, funct, 'comment', 'control'); 
  }
  
  function parsedatasecuredFillProfileDetail(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillProfileDetail('parsedatasecuredFillProfileDetail');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        var reg="";
       reg+="Registration Number : "+ data[0][0].RegistrationId +""
        jQuery("#registrationno").html(reg)
        sessionStorage.setItem("JSRegistrationId",data[0][0].RegistrationId)
        sessionStorage.setItem("Username",data[0][0].UserName)
        jQuery("#CandidateName").val(data[0][0].CandidateName)
        jQuery("#basicjobseekerfirstname").val(data[0][0].FirstName)
        jQuery("#jobseekermiddlename").val(data[0][0].MiddleName)
        jQuery("#jobseekerLastname").val(data[0][0].LastName)
        jQuery("#jobseekerfathername").val(data[0][0].GuardianFatherName)
        jQuery("#Phone").val(data[0][0].MobileNumber)
        jQuery("#email").val(data[0][0].EmailId)
        jQuery("#gender").val(data[0][0].Gender)
      
       
    }
  
  
  }
    
        // $("#pincode").focusout(function(){
        //   if($("#pincode").val()==''){
        //     getvalidated('Pin','text','Pin');
        //   }
        //   else{
        //     checkLength('Pin','Pin','6')
        //   }
        // })
        function InsUpdPersonal() {
          var MasterData = {
              "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
              "p_FirstName": jQuery("#basicjobseekerfirstname").val(),
              "p_MiddleName": '',
              "p_LastName": jQuery("#jobseekerLastname").val(),
              "p_FatherName": jQuery("#jobseekerfathername").val(),
              "p_Gender": jQuery("#gender").val(),
              "p_VillageId":  $("#village").val(),
              "p_DistrictId": $("#district").val(),
              "p_CityId": $("#city").val(),
              "p_UniqueIdentification": '',
              "p_IdentificationNumber": '',
              "p_MobileNumber": jQuery("#Phone").val(),
              "p_EmailId": jQuery("#email").val(),
              "p_UserName": $("#CandidateName").val(),
              "p_Password": '',
              "p_IpAddress": sessionStorage.getItem("IpAddress"),
          }
          MasterData = JSON.stringify(MasterData)
          var path = serverpath + "secured/registration";
          securedajaxpost(path, 'parsrdataInsUpdRegistration', 'comment', MasterData, 'control')
        }
        
        function parsrdataInsUpdRegistration(data) {
          data = JSON.parse(data)
          if (data.message == "New token generated") {
              sessionStorage.setItem("token", data.data.token);
              InsUpdPersonal() 
        
          }
          else if (data[0][0].ReturnValue == "3") {
              
              
              FillProfileDetail('parsedatasecuredFillProfileDetail');
              //toastr.success("Update Successful", "", "success")
              // $("#RegistrationId").text('Registration:- ' + sessionStorage.getItem('RegistrationId'));
              return true;
        
          }
        
        }