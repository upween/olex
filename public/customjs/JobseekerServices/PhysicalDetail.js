function InsUpdPhysicalDetail(){
    var MasterData = {
        "p_PhysicalDetailId": sessionStorage.getItem("JSPhysicalDetailId"),
        "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
        "p_Chest": jQuery("#txtchest").val(),
        "p_Height": jQuery("#txtHeight").val(),
        "p_Weight": jQuery("#txtWeight").val(),
        "p_EyeSight": jQuery("#Eyesight").val(),
        "p_IsActive": '1',
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
        "p_UserId":sessionStorage.getItem("JSCandidateId"),
        "p_SportsName": jQuery("#sportname").val(),
        "p_SportsCertificate": jQuery("#sportcertificate").val(),
        "p_NCCCertificate": $("#NCCcertificate").val(),
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/physicaldetails";
    securedajaxpost(path,'parsrdataInsUpdPhysicalDetail','comment',MasterData,'control')
}

function parsrdataInsUpdPhysicalDetail(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdPhysicalDetail();
        
    }
    
    if (data[0][0].ReturnValue == "1") {
         
          FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail');
          toastr.success("Insert Successful", "", "success")
          return true;
      }
       else if (data[0][0].ReturnValue == "2") {
        
          FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail');
        
          toastr.success("Update Successful", "", "success")
          return true;
      }
    
     }

   

     function FetchPhysicalDetail(funct,control) {
            var path = serverpath + "secured/physicaldetails/'"+sessionStorage.getItem("JSCandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'/'"+sessionStorage.getItem("JSCandidateId")+"'"
            securedajaxget(path,funct,'comment',control);
        }

    function parsedatasecuredFetchPhysicalDetail(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FetchPhysicalDetail('parsedatasecuredFetchPhysicalDetail');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
                
              var data1 = data[0];
            
              sessionStorage.setItem("JSPhysicalDetailId",data1[0].PhysicalDetailId)
               $("#txtchest").val(data1[0].Chest)
               $("#txtHeight").val(data1[0].Height)
               $("#txtWeight").val(data1[0].Weight),
               $("#Eyesight").val(data1[0].EyeSight),
               $("#sportname").val(data1[0].SportsName),
               $("#sportcertificate").val(data1[0].SportsCertificate),
               $("#NCCcertificate").val(data1[0].NCCCertificate)
               
            
        }
    }

    function FillEyesight(funct,control) {
        var path =  serverpath + "secured/EyeSight/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillEyesight(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillEyesight('parsedatasecuredFillEyesight','Eyesight');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Eye Sight"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Eye_sight_id).html(data1[i].Eye_sight));
                 }
            }
                  
      }

      function FillSportname(funct,control) {
        var path =  serverpath + "secured/SportsName/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillSportname(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSportname('parsedatasecuredFillSportname','sportname');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sport Name"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_id).html(data1[i].S_Name));
                 }
            }
                  
      }

      function FillSportcertificate(funct,control) {
        var path =  serverpath + "secured/SportsCertificate/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillSportcertificate(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSportcertificate('parsedatasecuredFillSportcertificate','sportcertificate');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sport Certificate"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_Certificate_id).html(data1[i].S_Certificate_name));
                 }
            }
                  
      }

      function FillNCCcertificate(funct,control) {
        var path =  serverpath + "secured/ncccertificate/0/0"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedatasecuredFillNCCcertificate(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillNCCcertificate('parsedatasecuredFillNCCcertificate','NCCcertificate');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select NCC Certificate"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ncc_Certificate_id).html(data1[i].ncc_Certificate_name));
                 }
            }
                  
      }
      
  

      