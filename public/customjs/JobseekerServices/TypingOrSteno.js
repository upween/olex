function CheckTyping()    //dimple 
 {

    if (jQuery('#language').val() == "0" ) {
        getvalidated('language','select','Language');
        return false;
    }
    if (jQuery('#type').val() == "0" ) {
        getvalidated('type','select','Type');
        return false;
    }
    else {
      
    InsUpdTypingOrSteno();}
 }

function InsUpdTypingOrSteno(){
  

    var MasterData = {
        "p_TypingStenoId": localStorage.getItem("TypingStenoId"),
        "p_CandidateId": sessionStorage.getItem("JSCandidateId"),
        "p_Language":jQuery("#language").val(),
        "p_Type": jQuery("#type").val(),
        "p_Institute": jQuery("#Institute").val(),
        "p_PassedYear": jQuery("#passYear").val(),
        "p_Speed":jQuery("#Speed").val(),
        "p_IpAddress":sessionStorage.getItem("IpAddress"),
       }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/Typing";
    securedajaxpost(path,'parsrdataInsUpdTypingOrSteno','comment',MasterData,'control')
}

function parsrdataInsUpdTypingOrSteno(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        InsUpdTypingOrSteno();
        
    }
    
    if (data[0][0].ReturnValue == "1") {
        
        FetchTyping('parsedatasecuredFetchTyping');
          toastr.success("Insert Successful", "", "success")
          return true;
      }
       else if (data[0][0].ReturnValue == "2") {
       
          FetchTyping('parsedatasecuredFetchTyping');
          resetTyping();
          toastr.success("Update Successful", "", "success")
          return true;
      }
      else if (data[0][0].ReturnValue == "0") {
        resetTyping();
        toastr.warning("Typing Detail already Exists ", "", "info")
        return false;
    }
    
     }

//  function resetTyping() {

//         $("#language").prop("disabled", false);
//         $("#type").prop("disabled", false);
//         jQuery("#language").val("0")
//         jQuery("#type").val("0")
//         jQuery("#Institute").val("0"),
//         jQuery("#passYear").val("NA"),
//         jQuery("#Speed").val("")
//         jQuery('#language').css('border-color', '');
//         $('#validlanguage').html("");
//         jQuery('#type').css('border-color', '');
//         $('#validtype').html("");
//          }

     function FetchTyping(funct,control) {
            var path = serverpath + "secured/Typing/'0'/'"+sessionStorage.getItem("JSCandidateId")+"'/'"+sessionStorage.getItem("IpAddress")+"'"
            securedajaxget(path,funct,'comment',control);
        }

    function parsedatasecuredFetchTyping(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FetchTyping('parsedatasecuredFetchTyping');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
                // jQuery("#" + control).empty();
                if(data[0]=="")
                {
                   localStorage.setItem("TypingStenoId","0")
                }
                else{
                var data1 = data[0];
                localStorage.setItem("TypingStenoId",data1[0].TypingStenoId)
               
                
                        // localStorage.setItem("TypingStenoId",TypingStenoId);     
                        $("#language").val(Lang_id)
                        $("#type").val(Type_id)
                        $("#Institute").val(Institute); 
                        $("#passYear").val(PassedYear)
                        $("#Speed").val(Speed)
              
            }
            }
    }

    // function EditTyping(TypingStenoId,Lang_id,Type_id,Institute,PassedYear,Speed) { 
    //     modal('Typing');
        
    //     $("#language").prop("disabled", true);
    //     $("#type").prop("disabled", true);
    //        localStorage.setItem("TypingStenoId",TypingStenoId);     
    //        $("#language").val(Lang_id)
    //        $("#type").val(Type_id)
    //        $("#Institute").val(decodeURI(Institute)); 
    //        $("#passYear").val(decodeURI(PassedYear))
    //        $("#Speed").val(decodeURI(Speed))

    //     }


   function FillPassingYear(funct,control) {
            var path =  serverpath + "secured/year/0/20"
            securedajaxget(path,funct,'comment',control);
      }
        
   function parsedatasecuredFillPassingYear(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillPassingYear('parsedatasecuredFillPassingYear','passYear');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    jQuery("#"+control).append(jQuery("<option ></option>").val("NA").html("Select Passed Year"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
                     }
                }
                      
      }

               
   function FillTSLanguage(funct,control) {
    var path =  serverpath + "secured/Languagetype/0/0"
    securedajaxget(path,funct,'comment',control);
      }
  
  function parsedatasecuredFillTSLanguage(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSLanguage('parsedatasecuredFillTSLanguage','language');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Language"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Lang_id).html(data1[i].Lang_name));
             }
        }
              
  }

  function FillTSType(funct,control) {
    var path =  serverpath + "secured/typingstenomaster/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillTSType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSType('parsedatasecuredFillTSType','type');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Type_id).html(data1[i].Type));
             }
        }
              
  }

  function FillTSInstitute(funct,control) {
    var path =  serverpath + "secured/TypingInstitute/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillTSInstitute(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSInstitute('parsedatasecuredFillTSInstitute','Institute');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Institute"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].TS_Institute_id).html(data1[i].TS_Institute_Name));
             }
        }
              
  }