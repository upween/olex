
function FetchMaritalstatus() {
    var path = serverpath +"Maritalstatus/0/0" 
    ajaxget(path, 'parsedataFetchMaritalstatus', 'comment', "control");
}
function parsedataFetchMaritalstatus(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].Active_YN == '0') {
        var	active = "No"
        }
         if (data1[i].Active_YN == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Marital_Status + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode('"+encodeURI(data1[i].Marital_Status)+"','"+encodeURI(data1[i].Marital_Status_H)+"','"+encodeURI(data1[i].Active_YN )+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);

}
function InsUpdMaritalstatus(){

	var chkval = $("#chkActive")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Marital_id":localStorage.Marital_id,
    "p_Marital_Status":$.trim(jQuery("#Marital").val()),
    "p_Marital_Status_H":$.trim(jQuery("#MaritalH").val()),
    //"p_Active_YN":'0',
   
    "p_Active_YN":chkval,
   
};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "Maritalstatus";
ajaxpost(path, 'parsedataMaritalStatus', 'comment', MasterData, 'control')
}

function parsedataMaritalStatus(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdMaritalstatus();
	}
	else if (data[0][0].ReturnValue == "1") {
        
        FetchMaritalstatus() 
        resetModeU() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FetchMaritalstatus() 
        resetModeU() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetModeU() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}



function CheckValidationMaritalstatus(){
  
    
    if($.trim(jQuery("#Marital").val()=="")){
          getvalidated('Marital','text','Marital Status');
          return false;
      }

      else  if($.trim(jQuery("#MaritalH").val()=="")){
        getvalidated('MaritalH','text','Marital Status Hindi');
        return false;
      }
      
      InsUpdMaritalstatus()
            
      }
  
  function EditMode(Marital_Status,Marital_Status_H,p_Active_YN) {
    //$(window).scrollTop(0);
   // jQuery("#ddlCountry").val(decodeURI(CountryId))
    jQuery("#Marital").val(Marital_Status)
    jQuery("#MaritalH").val(decodeURI(Marital_Status_H))
    // localStorage.CountryId =CountryId
    if (decodeURI(p_Active_YN) == '0') {
        $("#chkActive")[0].checked = false;
    }
    else {
        $("#chkActive")[0].checked = true;
    }
}
function resetModeU() {
   
    localStorage.Marital_id ="0"
    $("#chkActive")[0].checked = false;
        jQuery("#Marital").val("")
        $("#MaritalH").val("");
        // $("#village").val("");
    }

    
    $(function Alphabet() {
        
        $('#Marital,#MaritalH').keydown(function(e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
            e.preventDefault();
          } else {
            var key = e.keyCode;
            if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
              e.preventDefault();
            }
          }
        });
      });