function FillType() {
    var path = serverpath + "ActivityMaster/'"+$("#ActivityType").val()+"'"
    securedajaxget(path,'parsedataFillType','comment','control');
}
function parsedataFillType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillType();
        // UpdateActivity();
    }
    else{
    var data1 = data[0];
    var appenddata = '';
    for (var i = 0; i < data1.length; i++) {
    
        if (data1[i].activity_status=="Pending"){
 
            appenddata += "<tr><td>" +[i+1]+ "</td><td style='word-break: break-word;'>" + data1[i].activity_type  + "</td><td style='word-break: break-word;'><a onclick=Fillmodel('"+data1[i].activity_type_id+"') href='#'>" + data1[i].activity_type_id  + "</a></td></tr>";
               }
       } jQuery("#tbodyvalue").html(appenddata);
}
}

function UpdateActivity() {
    var MasterData = {
        "p_activity_id": sessionStorage.getItem("Activityid"),
        "p_activity_status": $('#Status').val(),
        "p_response": $('#Response').val(),
        "p_comment": $('#Comments').val(), 
        "p_response_by": sessionStorage.getItem("RegistrationId"),

    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/ActivityMaster";
    securedajaxpost(path, 'parsrdataUpdateActivity', 'comment', MasterData, 'control')
}
function parsrdataUpdateActivity(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        
    }

    else if (data[0][0].ReturnValue == "1") {
        $('#myModal').modal('toggle');

        toastr.success("Update Successful", "", "success")
        // $('#myModal1').modal('toggle');
        FillType();
        return true;
    }

}

function Fillmodel(activity_type_id) {
    var MasterData = {
        "p_Type": $('#ActivityType').val(),
        "p_activity_type_id": activity_type_id
     

    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "activity";
    securedajaxpost(path, 'parsrdataFillmodel', 'comment', MasterData, 'control')
}
function parsrdataFillmodel(data) {
    data = JSON.parse(data)
    var data1 = data[0];
    var Modalcontent='';
   if($('#ActivityType').val()=='FeedBack') {
    Modalcontent = "<div class='job-header'><div class='jobdetail'><ul>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>User Name</strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].UserName+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Mobile Number</strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].ContactNumber+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>E-mailid</strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].EmailId+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Location</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].Location+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Comments</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].Comments+"</span></div></li>"+

    "</ul></div></div>"
   }
   if($('#ActivityType').val()=='Grievence') {
    Modalcontent = "<div class='job-header'><div class='jobdetail'><ul>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Registered ID </strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].RegistrationId+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Name</strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].Name+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>E-mailid</strong></div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].EmailId+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Mobile Number</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].ContactNumber+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Department</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].Department+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Type of Grievance</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].TypeofGrievence+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' ><strong>Details</strong> </div><div class='col-md-6 col-xs-6' ><span style=' color: darkgrey; text-align: left; width: 145%;'>"+data1[0].GrivenceDetail+"</span></div></li>"+

    "</ul></div></div>"
   }
    
    $("#modalbody6").html(Modalcontent);
    $('#m_modal_activity').modal('toggle');

    
}









