function DistrictOffice() {

    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "districtoffice",
        cache: false,
        dataType: "json",
        success: function (data) {
         var data1 = data[0]
            var cityData=''
            for (var i = 0; i < data[0].length; i++) {
                
                     cityData = " <tr class='i'><td>"+[i+1]+"</td><td>"+data1[i].DivisionCallCentre+"</td><td>"+data1[i].Administration+"</td><td>"+data1[i].DistrictsCovered+"</td><td>"+data1[i].CentreManager+"</td><td>"+data1[i].Contact+"</td><td>"+data1[i].EMailId+"</td><td>"+data1[i].Address+"</td><td><a href='#'><button type='button' onclick=setdetails('"+encodeURI(data1[i].DivisionCallCentreId)+"','"+encodeURI(data1[i].DivisionCallCentre)+"','"+encodeURI(data1[i].Administration)+"','"+encodeURI(data1[i].DistrictsCovered)+"','"+encodeURI(data1[i].CentreManager)+"','"+encodeURI(data1[i].Contact)+"','"+encodeURI(data1[i].EMailId)+"','"+encodeURI(data1[i].Address)+"') data-toggle='modal' data-target='#m_modal_4' 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></a></td></tr>";
                    $('#tbodyvalue').append($(cityData));
            }
            $('#tbodyvalue').clientSidePagination();

        },
        error: function (xhr) {
            //swal(xhr.d, "", "error")
        }
    });


}

function setdetails(DivisionCallCentreId,DivisionCallCentre,Administration,DistrictsCovered,CentreManager,Contact,EMailId,Address){
    sessionStorage.setItem('DivisionCallCentreId',DivisionCallCentreId);
    sessionStorage.setItem('DivisionCallCentre',DivisionCallCentre);
    sessionStorage.setItem('Administration',Administration);
    sessionStorage.setItem('DistrictsCovered',DistrictsCovered);
    sessionStorage.setItem('CentreManager',CentreManager);
    sessionStorage.setItem('Contact',Contact);
    sessionStorage.setItem('EMailId',EMailId);
    sessionStorage.setItem('Address',Address);
    edit();
}

function edit(){
    jQuery("#DivisionCallCentre").val(decodeURI(sessionStorage.getItem('DivisionCallCentre'))),
    jQuery("#Administration").val(decodeURI(sessionStorage.getItem('Administration'))),
    jQuery("#DistrictsCovered").val(decodeURI(sessionStorage.getItem('DistrictsCovered'))),
    jQuery("#CentreManager").val(decodeURI(sessionStorage.getItem('CentreManager',))),
    jQuery("#Contact").val(decodeURI(sessionStorage.getItem('Contact'))),
    jQuery("#EMailId").val(decodeURI(sessionStorage.getItem('EMailId'))),
    jQuery("#Address").val(decodeURI(sessionStorage.getItem('Address')))

}


function UpdDistrictOffice(){

var MasterData = {  
	
    "p_DivisionCallCentreId":sessionStorage.DivisionCallCentreId,
    "p_DivisionCallCentre":jQuery("#DivisionCallCentre").val(),
    "p_Administration":jQuery("#Administration").val(),
    "p_DistrictsCovered":jQuery("#DistrictsCovered").val(),
    "p_CentreManager":jQuery("#CentreManager").val(),
    "p_Contact":jQuery("#Contact").val(),
    "p_EMailId":jQuery("#EMailId").val(),
    "p_Address":jQuery("#Address").val()
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "UpdDistrictOffice";
ajaxpost(path, 'parsrdataUpdDistrictOffice', 'comment', MasterData, 'control')
}

function parsrdataUpdDistrictOffice(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			UpdDistrictOffice();
	}
	else if (data[0][0].ReturnValue == "1") {
        $("#m_modal_4").modal("toggle");
        DistrictOffice();
      //  resetMode()
         toastr.success("Update Successful", "", "success")
        return true;
	}
	
 
}


function checkval(){

    if(isValidation){
    if(jQuery("#DivisionCallCentre").val()==""){
        getvalidated('DivisionCallCentre','text','Division Call Centre');
        return false;
    }
    if(jQuery("#Administration").val()==""){
        getvalidated('Administration','text','Administration');
        return false;
    }
    if(jQuery("#DistrictsCovered").val()==""){
        getvalidated('DistrictsCovered','text','DistrictsCovered');
        return false;
    }
    if(jQuery("#CentreManager").val()==""){
        getvalidated('CentreManager','text','Centre Manager');
        return false;
    }
    if(jQuery("#Contact").val()==""){
        getvalidated('Contact','text','Contact');
        return false;
    }
    if(jQuery("#EMailId").val()==""){
        getvalidated('EMailId','text','EMailId');
        return false;
    }
    if(jQuery("#Address").val()==""){
        getvalidated('Address','text','Address');
        return false;
    }
    else{
        UpdDistrictOffice()
          
    }
}
    else{
        UpdDistrictOffice()
          
    }
}



jQuery("#txtSearch").keyup(function () {
    searchTextInTable("tbodyvalue");
})