function FillDepartmentTable() {
    var path =  serverpath + "selfempdepartment"
    securedajaxget(path,'parsedatasecuredFillDepartmentTable','comment',"control");
}
function parsedatasecuredFillDepartmentTable
(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDepartmentTable();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            for (var i = 0; i < data1.length; i++) {
           if(data1[i].IsActive=='0'){
    active="N"
           }
           else if(data1[i].IsActive=='1'){
            active="Y"
                   }
       appenddata += "<tr><td style='    word-break: break-word;'>" + [i+1]+ "</td><td style='    word-break: break-word;'>" + data1[i].DepartmentNameHindi+ "</td><td style='    word-break: break-word;'>" + data1[i].DepartmentName+ "</td><td>"+active+"</td><td><button type='button' onclick=editMode('"+data1[i].DepartmentId+"','"+encodeURI(data1[i].DepartmentNameHindi)+"','"+encodeURI(data1[i].DepartmentName)+"','"+encodeURI(data1[i].IsActive)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}
function editMode(DepartmentId,DepartmentNameHindi,DepartmentName,IsActive){
    jQuery("#scrolltop").click();
    sessionStorage.DepartmentId=DepartmentId,
    $("#textHindi").val(decodeURI(DepartmentNameHindi)),
    $("#textEnglish").val(decodeURI(DepartmentName)),
    $(".passwordfield").hide();
    if(decodeURI(IsActive)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
}
function CheckValidation(){
    var chkval = $("#chkActiveStatus")[0].checked
    if(isValidation){
	if(jQuery("#textHindi").val()==''){
		getvalidated("textHindi","text","Department (In Hindi)")
		return false;
	}
	if(jQuery("#textEnglish").val()==''){
		getvalidated("textEnglish","text","Department (In English)")
		return false;
    }
    if(jQuery("#password").val()==''){
		getvalidated("password","text","Password")
		return false;
    }
    else{
		InsupdDepartmentDetail()
	}
}
	
	else{
		InsupdDepartmentDetail()
	}
    
}
function InsupdDepartmentDetail(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData ={
    "p_DepartmentId":sessionStorage.DepartmentId,
    "p_DepartmentName":$("#textEnglish").val(),
    "p_DepartmentNameHindi":$("#textHindi").val(),
     "p_IsActive":chkval,                               
     "p_Password":md5($("#password").val())
    
    };
MasterData = JSON.stringify(MasterData);
console.log(MasterData);
var path = serverpath + "selempdepartmentmaster";
securedajaxpost(path, 'parsrdataitInsupdDepartmentDetail', 'comment', MasterData, 'control')
}


function parsrdataitInsupdDepartmentDetail(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdDepartmentDetail();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetmode()
        FillDepartmentTable();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetmode()
        FillDepartmentTable();
			toastr.success("Update Successful", "", "success")
			return true;
	}
    
	else if (data[0][0].ReturnValue == "0") {
         resetmode()
         FillDepartmentTable();
	toastr.warning("already exist", "", "info")
			return true;
	}
 
 }
function resetmode(){
    sessionStorage.DepartmentId='0',
    $("#textEnglish").val(""),
    $("#textHindi").val("")
    $("#password").val("")
    $("#chkActiveStatus")[0].checked=false;
    $(".passwordfield").show();
    }
