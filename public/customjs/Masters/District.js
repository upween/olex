function FillState(funct,control) {
    var path =  serverpath + "state/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDistrict(data,control){  

    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillState('parsedatasecuredFillSecuredDistrict','dState');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
    else{
        console.log(data) 
            jQuery("#dState").empty();
            var data1 = data[0];
              jQuery("#dState").append(jQuery("<option ></option>").val("0").html("Select State"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#dState").append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
             }
             
        }
              
}

jQuery('#dState').on('change', function () {
    FillDivision('parsedatasecuredFillSecuredDivision','ddlDivision');
  });

function FillDivision(funct,control) {
    var path =  serverpath + "divisionbystate/"+jQuery('#dState').val()+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDivision(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDivision('parsedatasecuredFillSecuredDivision','ddlDivision');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
    else{
        
            jQuery("#ddlDivision").empty();
            var data1 = data[0];
              jQuery("#ddlDivision").append(jQuery("<option ></option>").val("0").html("Select Division"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddlDivision").append(jQuery("<option></option>").val(data1[i].Division_id).html(data1[i].Division_name));
             }
             
        }
              
}



function FetchDistrict() {
    var path = serverpath + "district/"+$("#dState").val()+"/0/0/0"
    ajaxget(path, 'parsedataFetchDistrict', 'comment', "control");
}
function parsedataFetchDistrict(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].IsActive == '0') {
        var	active = "No"
        }
         if (data1[i].IsActive == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].DistrictName + "</td><td style='    word-break: break-word;'>" + data1[i].StateName + "</><td>"+active+"</td><td><button type='button' onclick=EditMode("+data1[i].DistrictId+",'"+encodeURI(data1[i].DistrictName)+"','"+encodeURI(data1[i].StateId)+"','"+encodeURI(data1[i].Division_id)+"','"+encodeURI(data1[0].IsActive)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);


}

function InsUpdDistrict(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  

    "p_DistrictId":localStorage.p_DistrictId,
    "p_DistrictName":$.trim(jQuery("#txt").val()),
    "p_StateId":jQuery("#dState").val(),
    "p_IsActive":chkval,
    "p_IpAddress":'0',
    "p_UserId":'0'

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "district";
ajaxpost(path, 'parsrdataDistrict', 'comment', MasterData, 'control')
}

function parsrdataDistrict(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdDistrict();
	}
	else if (data[0][0].ReturnValue == "1") {
        FetchDistrict();
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {
   
    localStorage.p_DistrictId = "0";
    $("#chkActiveStatus")[0].checked = false;
        jQuery("#dState").val("0")
        $("#txt").val("");
    }

    function EditMode(DistrictId,DistrictName,dState,Division_id,IsActive) {
        jQuery("#txt").val(decodeURI(DistrictName))
        jQuery("#dState").val(decodeURI(dState))

        localStorage.p_DistrictId =DistrictId
        if (decodeURI(IsActive) == '0') {
            $("#chkActiveStatus")[0].checked = false;
        }
        else {
            $("#chkActiveStatus")[0].checked = true;
        }
    }

    function CheckValidationDistrict(){
        if(isValidation){
        if(jQuery("#dState").val()=="0"){
            getvalidated('dState','select','State');
            return false;
        }

    //   else  if(jQuery("#ddlDivision").val()=="0"){
    //         getvalidated('ddlDivision','select','Division');
    //         return false;
    //     }
       else if($.trim(jQuery("#txt").val())==""){
            getvalidated('txt','text','District');
            return false;
        }
        else{
            InsUpdDistrict();
        }
    }
        else{
            InsUpdDistrict();
        }
    }


