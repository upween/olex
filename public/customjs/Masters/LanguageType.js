

function FillLanguageType() {
    var path = serverpath + "Languagetype/0/0"
    ajaxget(path, 'parsedataLanguageType', 'comment', "control");
}
function parsedataLanguageType(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].Active_YN == '0') {
        var	active = "No"
        }
         if (data1[i].Active_YN == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Lang_name + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode("+data1[i].Lang_id+",'"+encodeURI(data1[i].Lang_name)+"','"+encodeURI(data1[i].Active_YN )+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);


}
function InsUpdLanguageType(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Lang_id":localStorage.LanguageTypeid,
    "p_Lang_name":jQuery("#Language").val(),
    "p_Active_YN":chkval,
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "Languagetype";
ajaxpost(path, 'parsrdataLanguage', 'comment', MasterData, 'control')
}

function parsrdataLanguage(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdLanguageType();
	}
	else if (data[0][0].ReturnValue == "1") {
        
        FillLanguageType()
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FillLanguageType()
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
       resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function CheckValidationLanguageType(){
  
    if(isValidation){
    if(jQuery("#Language").val()==""){
          getvalidated('Language','text','Language Type Name');
          return false;
      }
      else{
        InsUpdLanguageType();
      }
  }
      else{
        InsUpdLanguageType();
      }
  }
  function EditMode(IDLanguage,LanguageName,IsActive) {
    jQuery("#Language").val(decodeURI(LanguageName))
    
    localStorage.LanguageTypeid=IDLanguage
    if (decodeURI(IsActive) == '0') {
        $("#chkActiveStatus")[0].checked = false;
    }
    else {
        $("#chkActiveStatus")[0].checked = true;
    }
}
function resetMode() {
   
    localStorage.LanguageTypeid= "0";
    $("#chkActiveStatus")[0].checked = false;
        jQuery("#Language").val("")
}
       
