function InsUpdnationality(){
	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Nationality_id":localStorage.p_Nationality_id,
    "p_Nationality_name":$.trim(jQuery("#nationalname").val()),
    "p_Active_YN":chkval


};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "Nationality";
securedajaxpost(path, 'parsrdatanationalmaster', 'comment', MasterData, 'control')
}

function parsrdatanationalmaster(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdnationality();
	}
	else if (data[0][0].ReturnValue == "1") {
        Fetchnationality();
       resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        Fetchnationality();
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_Nationality_id = "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#nationalname").val("");
    }
function Fetchnationality() {
	var path = serverpath + "Nationality/0/0"
	ajaxget(path, 'parsrdataNationlitymaster', 'comment', "control");
}
function parsrdataNationlitymaster(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
		 	active = "N"
	 }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].Nationality_id  + "</td><td style='word-break: break-word;'>" + data1[i].Nationality_name  + "</td><td style='word-break: break-word;'>" + active  + "</td><td><button type='button' onclick=EditMode("+data1[i].Nationality_id+",'"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].Nationality_name)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);
    }
function EditMode(Nationality_id,Active_YN,Nationality_name) {
  
    localStorage.p_Nationality_id =Nationality_id
    if (decodeURI(Active_YN) == '0') {
		$("#ActiveStatus")[0].checked = false;
	}
	else {
		$("#ActiveStatus")[0].checked = true;
    }
    jQuery("#nationalname").val(decodeURI(Nationality_name))
}
function CheckValidationnationality(){
  
  if(isValidation){  
  if($.trim(jQuery("#nationalname").val())==""){
        getvalidated('nationalname','text','Nationality Name');
        return false;
	}
	else{
        InsUpdnationality();
    }
}
    else{
        InsUpdnationality();
    }
}