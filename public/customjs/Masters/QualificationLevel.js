function InsUpdQualification(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
    }

var MasterData = {  
	
    "p_Qualif_Level_id":localStorage.p_Qualif_Level_id,
    "p_Qualif_Level_name":$.trim(jQuery("#qualification").val()),
    "p_Qualif_Level_name_H":$.trim(jQuery("#qualificationnamehindi").val()),
    "p_Qualif_desc":$.trim(jQuery("#Description").val()),
    "p_QualifLevel_order":jQuery("#Order").val(),
    "p_Active_YN":chkval
    
};

MasterData = JSON.stringify(MasterData)
var path = serverpath + "Qualification";
ajaxpost(path, 'parsrdataqualification', 'comment', MasterData, 'control')

}
function parsrdataqualification(data) {
	data = JSON.parse(data)
      	if (data.message == "New token generated"){
      			sessionStorage.setItem("token", data.data.token);
      			InsUpdQualification();
      	}
      	else if (data[0][0].ReturnValue == "1") {
             FetchQualification();
             resetMode() ;
      		 toastr.success("Insert Successful", "", "success")
      			return true;
      	}
      	else if (data[0][0].ReturnValue == "2") {
              FetchQualification();
              resetMode() ;
      			toastr.success("Update Successful", "", "success")
      			return true;
      	}
      	else if (data[0][0].ReturnValue == "0") {
              resetMode() ;
      	toastr.info("Already exist", "", "info")
      			return true;
      	}
       
      }
      function resetMode() {
         
          localStorage.p_Qualif_Level_id = "0";
          $("#chkActiveStatus")[0].checked = false;
              jQuery("#qualification").val("");
              jQuery("#qualificationnamehindi").val("");
              jQuery("#Description").val("");
              jQuery("#Order").val("");
          }
      function FetchQualification() {
      	var path = serverpath + "Qualification/0/0"
      	ajaxget(path, 'parsrdataqualificationmaster', 'comment', "control");
      }
      function parsrdataqualificationmaster(data) {
      	data = JSON.parse(data)
      	var data1 = data[0];
      	var appenddata = "";
          var active = "";
          var QualificationLevel=""
      	for (var i = 0; i < data1.length; i++) {
      		if (data1[i].Active_YN == '0') {
      		 	active = "N"
      	 }
      		 if (data1[i].Active_YN == '1') {
      			active = "Y"
              }
              if (data1[i].Qualif_Level_name_H == null) {
                  QualificationLevel = "  "
          }
              else{
                  QualificationLevel =data1[i].Qualif_Level_name_H 
             }
             if (data1[i].Qualif_desc == null) {
              Qualificationdesc = "  "
      }
          else{
              Qualificationdesc =data1[i].Qualif_desc 
         }
      		appenddata += "<tr><td style='word  break: break  word;'>" + data1[i].Qualif_Level_id  + "</td><td style='word  break: break  word;'>" + data1[i].Qualif_Level_name  + "</td><td style='word  break: break  word;'>" + QualificationLevel  + "</td><td style='word  break: break  word;'>" + Qualificationdesc + "</td><td style='word  break: break  word;'>" + data1[i].QualifLevel_order + "</td><td style='word  break: break  word;'>" + active  + "</td><td><button type='button' onclick=EditMode("+data1[i].Qualif_Level_id+",'"+encodeURI(data1[i].Qualif_Level_name)+"','"+encodeURI(QualificationLevel)+"','"+encodeURI(Qualificationdesc)+"','"+encodeURI(data1[i].QualifLevel_order)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='bbtn btn-success' style='background  color:#716aca;border  color:#716aca;'>Modify</button></td></tr>";
          } jQuery("#tbodyvalue").html(appenddata);
          }
      function EditMode(Qualif_Level_id,Qualif_Level_name,Qualif_Level_name_H,Qualif_desc,QualifLevel_order,Active_YN) {
        
          jQuery("#qualification").val(decodeURI(Qualif_Level_name))
          jQuery("#qualificationnamehindi").val(decodeURI(Qualif_Level_name_H))
          jQuery("#Description").val(decodeURI(Qualif_desc))
          jQuery("#Order").val(decodeURI(QualifLevel_order))
          localStorage.p_Qualif_Level_id =Qualif_Level_id
          if (decodeURI(Active_YN) == '0') {
      		$("#ActiveStatus")[0].checked = false;
      	}
      	else {
      		$("#chkActiveStatus")[0].checked = true;
          }
          
      }
      function CheckValidationQualification(){
        
          if(isValidation){
        if($.trim(jQuery("#qualification").val())==""){
              getvalidated('qualification','text','qualification');
              return false;
          }
       else if($.trim(jQuery("#qualificationnamehindi").val())==""){
              getvalidated('qualificationnamehindi','text','qualificationnamehindi');
              return false;
          }
          else if($.trim(jQuery("#Description").val())==""){
              getvalidated('Description','text','Description');
              return false;
          }
          else if(jQuery("#Order").val()==""){
              getvalidated('Order','text','Order');
              return false;
          }
          
          else{
            InsUpdQualification();
        }
}
      
          else{
              InsUpdQualification();
          }
}