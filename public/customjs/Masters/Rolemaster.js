function InsUpdRole(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_FunctionalArea_id":localStorage.Role,
    "p_FunctionalArea":$('#Role').val(),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "insfunctionarea";
ajaxpost(path, 'parsrdatarole', 'comment', MasterData, 'control')
}

function parsrdatarole(data) {
	data = JSON.parse(data)
	if(data[0][0].ReturnValue=='0'){
        toastr.warning('Already exists');
        resetMode()
        return false
    }
    if(data[0][0].ReturnValue=='1'){
        FetchRole()
        toastr.success('Submit Successful');
        resetMode()
        return true;
    }
    if(data[0][0].ReturnValue=='2'){
        FetchRole()
        toastr.success('Update Successful');
        resetMode()
        return true;
    }
 
}
function resetMode() {
   
    localStorage.Role='0';
    $("#ActiveStatus")[0].checked = false;
        jQuery("#Role").val("")
        // $("#ddltehsil").val("0");
        // $("#village").val("");
    }


function FetchRole() {
        var path = serverpath + "getFunctionalArea"
        ajaxget(path, 'parsedataFetchrole', 'comment', "control");
    }
    function parsedataFetchrole(data) {

        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        var active = "";
       
        for (var i = 0; i < data1.length; i++) {

            if (data1[i].Active_YN == '0') {
            var	active = "No"
            }
             if (data1[i].Active_YN == '1') {
              var  active = "Yes"
            }
            appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].FunctionalArea + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode('"+data1[i].FunctionalArea_id+"','"+encodeURI(data1[i].FunctionalArea)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        } jQuery("#tbodyvalue").html(appenddata);
    
    
    }


    function EditMode(FunctionalArea_id,FunctionalArea,active) {
       $('#Role').val(decodeURI(FunctionalArea));
        //jQuery("#ddltehsil").val(decodeURI(CityId))
        localStorage.Role=FunctionalArea_id;
        if (decodeURI(active) == '0') {
            $("#ActiveStatus")[0].checked = false;
        }
        else {
            $("#ActiveStatus")[0].checked = true;
        }
    }

    
  