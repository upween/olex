function FillSchemeTable() {
    var path =  serverpath + "schememaster"
    securedajaxget(path,'parsedatasecuredFillSchemeTable','comment',"control");
}
function parsedatasecuredFillSchemeTable
(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSchemeTable();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            for (var i = 0; i < data1.length; i++) {
           if(data1[i].IsActive=='0'){
    active="N"
           }
           else if(data1[i].IsActive=='1'){
            active="Y"
                   }
       appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].SchemeID+ "</td><td style='    word-break: break-word;'>" + data1[i].DepartmentName+ "</td><td style='    word-break: break-word;'>" + data1[i].SchemeNameHindi+ "</td><td style='    word-break: break-word;'>" + data1[i].SchemeName+ "</td><td>"+active+"</td><td><button type='button' onclick=editMode('"+data1[i].SchemeID+"','"+encodeURI(data1[i].SchemeNameHindi)+"','"+encodeURI(data1[i].SchemeName)+"','"+encodeURI(data1[i].IsActive)+"','"+encodeURI(data1[i].DepartmentId)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}
function editMode(SchemeID,SchemeNameHindi,SchemeName,IsActive,DepartmentId){
    jQuery("#scrolltop").click();
    sessionStorage.SchemeID=SchemeID,
    $("#textHindi").val(decodeURI(SchemeNameHindi)),
    $("#textEnglish").val(decodeURI(SchemeName))
    $("#DepartmentId").val(decodeURI(DepartmentId))
    if(decodeURI(IsActive)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
}
function CheckValidation(){
    var chkval = $("#chkActiveStatus")[0].checked
    if(isValidation){
	if(jQuery("#textHindi").val()==''){
		getvalidated("textHindi","text","Designation (In Hindi)")
		return false;
	}
	if(jQuery("#textEnglish").val()==''){
		getvalidated("textEnglish","text","Designation(In English)")
		return false;
    }
    else{
		InsupdSchemeDetail()
	}
}
	
	else{
		InsupdSchemeDetail()
	}
}
function InsupdSchemeDetail(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData ={
    "p_SchemeID":sessionStorage.SchemeID,
    "p_SchemeName":$("#textEnglish").val(),
    "p_SchemeNameHindi":$("#textHindi").val(),
    "p_DepartmentId":$("#DepartmentId").val(),
     "p_IsActive":chkval
     
    
    };
MasterData = JSON.stringify(MasterData);
console.log(MasterData);
var path = serverpath + "selempschememaster";
securedajaxpost(path, 'parsrdataitInsupdSchemeDetail', 'comment', MasterData, 'control')
}


function parsrdataitInsupdSchemeDetail(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdSchemeDetail();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetmode()
        FillSchemeTable();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetmode()
        FillSchemeTable();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
         resetmode()
         FillSchemeTable();
	toastr.warning("already exist", "", "info")
			return true;
	}
 
// }
function resetmode(){
    sessionStorage.DepartmentId='0',
    $("#textEnglish").val(""),
    $("#textHindi").val(""),
    $("#DepartmentId").val("0")
    $("#chkActiveStatus")[0].checked=false;

    }
}

function FillSecuredDepartment(funct,control) {
    var path =  serverpath + "selfempdepartment"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDepartment(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDepartment('parsedatasecuredFillSecuredDepartment',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Department"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DepartmentId).html(data1[i].DepartmentName));
             }
             
        }
              
}