
                function fillSector(funct,control) {
                    var path =  serverpath +"SkillSet/0/0"
                    securedajaxget(path,funct,'comment',control);
                }
                
                function parsedatafillSector(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        fillSector('parsedatafillSector','sectorddl');                     }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[0];
                            jQuery("#sectorddl").append(jQuery("<option ></option>").val("0").html("Select Sector"));

                            for (var i = 0; i < data1.length; i++) {
                                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
                             }
                            // jQuery("#"+control).val('5');
                        }
                              
                }
                
                function fillRole(funct,control) {
                    var path =  serverpath +"getFunctionalArea"
                    securedajaxget(path,funct,'comment',control);
                }
                
                function parsedatafillRole(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        fillRole('parsedatafillRole','roleddl');                     }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[0];
                            jQuery("#roleddl").append(jQuery("<option ></option>").val("0").html("Select Role"));

                            for (var i = 0; i < data1.length; i++) {
                                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
                             }
                            // jQuery("#"+control).val('5');
                        }
                              
                }
                function InsUpdSectorRole(){

                var MasterData = {  
                    
                    "p_ESID":$('#sectorddl').val(),
                    "p_FunctionalArea_id":$('#roleddl').val(),
                   
                
                };
                MasterData = JSON.stringify(MasterData)
                var path = serverpath + "SectorRolemapping";
                ajaxpost(path, 'parsrdatasector', 'comment', MasterData, 'control')
                }
                
                function parsrdatasector(data) {
                    data = JSON.parse(data)
                    if(data[0][0].ReturnValue=='0'){
                        toastr.warning('Already exists');
                      
                        return false
                    }
                    if(data[0][0].ReturnValue=='1'){
                        
                        toastr.success('Submit Successful');
                      
                        return true;
                    }
                    if(data[0][0].ReturnValue=='2'){
                       
                        toastr.success('Update Successful');
                     
                        return true;
                    }
                 
                }