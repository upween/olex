function InsUpdSector(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_ESID":localStorage.SectorId,
    "p_Description":$('#Description').val(),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "employementSector";
ajaxpost(path, 'parsrdatasector', 'comment', MasterData, 'control')
}

function parsrdatasector(data) {
	data = JSON.parse(data)
	if(data[0][0].ReturnValue=='0'){
        toastr.warning('Already exists');
        resetMode()
        return false
    }
    if(data[0][0].ReturnValue=='1'){
        FetchSector()
        toastr.success('Submit Successful');
        resetMode()
        return true;
    }
    if(data[0][0].ReturnValue=='2'){
        FetchSector()
        toastr.success('Update Successful');
        resetMode()
        return true;
    }
 
}
function resetMode() {
   
    localStorage.SectorId='0'
    $("#ActiveStatus")[0].checked = false;
        jQuery("#Description").val("")
        // $("#ddltehsil").val("0");
        // $("#village").val("");
    }


function FetchSector() {
        var path = serverpath + "SkillSet/0/0"
        ajaxget(path, 'parsedataFetchSector', 'comment', "control");
    }
    function parsedataFetchSector(data) {

        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        var active = "";
       
        for (var i = 0; i < data1.length; i++) {

            if (data1[i].Active_YN == '0') {
            var	active = "No"
            }
             if (data1[i].Active_YN == '1') {
              var  active = "Yes"
            }
            appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].Description + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode('"+data1[i].ESID+"','"+encodeURI(data1[i].Description)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        } jQuery("#tbodyvalue").html(appenddata);
    
    
    }


    function EditMode(ESID,Description,IsActive) {
       $('#Description').val(decodeURI(Description));
        //jQuery("#ddltehsil").val(decodeURI(CityId))
        localStorage.SectorId=ESID;
        if (decodeURI(IsActive) == '0') {
            $("#ActiveStatus")[0].checked = false;
        }
        else {
            $("#ActiveStatus")[0].checked = true;
        }
    }

    
  