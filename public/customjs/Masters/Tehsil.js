function FillSecuredDistrict_Tehsil(funct,control) {
    var path =  serverpath + "secured/district/19/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDistrict_Tehsil('parsedatasecuredFillSecuredDistrict',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             
        }
              
}
function InsUpdcity(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_CityId":localStorage.p_CityId,
    "p_CityName":$.trim(jQuery("#txturl").val()),
    "p_DistrictId":jQuery("#ddlDistrict").val(),
    "p_IsActive":chkval,
    "p_IpAddress":0,
    "p_UserId":0

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "city";
securedajaxpost(path, 'parsrdatacity', 'comment', MasterData, 'control')
}

function parsrdatacity(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdcity();
	}
	else if (data[0][0].ReturnValue == "1") {
        FetchTehsil();
        resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
    FetchTehsil();
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_CityId = "0";
    $("#chkActiveStatus")[0].checked = false;
        jQuery("#txturl").val("")
        $("#ddlDistrict").val("0");
    }
function FetchTehsil() {
	var path = serverpath + "city/"+$("#ddlDistrict").val()+"/0/0/0"
	ajaxget(path, 'parsedataFetchTehsil', 'comment', "control");
}
function parsedataFetchTehsil(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].IsActive == '0') {
		 	active = "N"
	 }
		 if (data1[i].IsActive == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].CityName + "</td><td>"+data1[i].DistrictName+"</td><td >"+active +"</td><td><button type='button' onclick=EditMode("+data1[i].CityId+",'"+encodeURI(data1[i].CityName)+"','"+encodeURI(data1[i].DistrictId)+"','"+encodeURI(data1[i].IsActive)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMode(CityId,CityName,DistrictId,IsActive) {
    jQuery("#txturl").val(decodeURI(CityName))
    jQuery("#ddlDistrict").val(decodeURI(DistrictId))
    localStorage.p_CityId =CityId
    if (decodeURI(IsActive) == '0') {
		$("#chkActiveStatus")[0].checked = false;
	}
	else {
		$("#chkActiveStatus")[0].checked = true;
    }
}
function CheckValidationTehsil(){
  if(isValidation){
    if(jQuery("#ddlDistrict").val()=="0"){
        getvalidated('ddlDistrict','select','District');
        return false;
    }
  else  if($.trim(jQuery("#txturl").val())==""){
        getvalidated('txturl','text','Tehsil');
        return false;
    }
    else{
      InsUpdcity();
  }
}
    else{
        InsUpdcity();
    }
}
$(function Alphabet() {
        
    $('#txturl').keydown(function(e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
  });