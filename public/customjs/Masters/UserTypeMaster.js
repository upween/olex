

function FillUserType() {
    var path = serverpath +"usertype/0/0" 
    ajaxget(path, 'parsedataUserTypemaster', 'comment', "control");
}
function parsedataUserTypemaster(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].Active_YN == '0') {
        var	active = "No"
        }
         if (data1[i].Active_YN == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].User_Type_Id+ "</td><td style='    word-break: break-word;'>" + data1[i].User_Name  + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode("+data1[i].User_Type_Id+",'"+encodeURI(data1[i].User_Name)+"','"+encodeURI(data1[i].Active_YN )+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);


}
function InsUpdUserTypemaster(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_User_Type_Id":localStorage.User_Type_Id,
    "p_User_Name":jQuery("#usertypename").val(),
    "p_Active_YN":chkval
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "usertype";
ajaxpost(path, 'parsrdataparsedataUserTypemaster', 'comment', MasterData, 'control')
}

function parsrdataparsedataUserTypemaster(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdUserTypemaster();
	}
	else if (data[0][0].ReturnValue == "1") {
        FillUserType();
        resetMode()
//FillSectorType() 
      //  resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FillUserType() 
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
       resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function CheckValidationUserType(){
  
    if(isValidation){
    if(jQuery("#usertypename").val()==""){
          getvalidated('UserTypeName','text','UserTypeName');
          return false;
      }
      else{
        InsUpdUserTypemaster()
            
      }
  }
      else{
        InsUpdUserTypemaster()
            
      }
  }
  function EditMode(User_Type_Id,User_Name,Active_YN) {
    jQuery("#usertypename").val(decodeURI(User_Name))
    
    localStorage.User_Type_Id=User_Type_Id
    if (decodeURI(Active_YN) == '0') {
        $("#ActiveStatus")[0].checked = false;
    }
    else {
        $("#ActiveStatus")[0].checked = true;
    }
}
function resetMode() {
   
    localStorage.User_Type_Id= "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#usertypename").val("")
}