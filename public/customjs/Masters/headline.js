function InsUpdheadline(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Id":localStorage.p_Id,
    "p_Description":$.trim(jQuery("#headeng").val()),
    "p_Description_H":$.trim(jQuery("#headhindi").val()),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/indexnote";
securedajaxpost(path, 'parsrdataheadline', 'comment', MasterData, 'control')
}

function parsrdataheadline(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdheadline();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
    $("#btnSubmit").html('Submit');
    localStorage.p_Id = "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#headeng").val("")
        $("#headhindi").val("");
    }
function Fetchheadline() {
	var path = serverpath + "indexnote"
	ajaxget(path, 'parsedataFetchheadline', 'comment', "control");
}
function parsedataFetchheadline(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		// if (data1[i].Active_YN == '0') {
		// 	active = "N"
		// }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].Id + "</td><td style='    word-break: break-word;'>" + data1[i].Description + "</td><td>"+data1[i].Description_H+"</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button' onclick=EditMode("+data1[i].Id+",'"+encodeURI(data1[i].Description)+"','"+encodeURI(data1[i].Description_H)+"','"+encodeURI(data1[i].Active_YN)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMode(Id,Headline_Eng,Headline_Hn,Active_YN) {
    jQuery("#headeng").val(decodeURI(Headline_Eng))
    jQuery("#headhindi").val(decodeURI(Headline_Hn))
    localStorage.p_Id =Id
    if (decodeURI(Active_YN) == '0') {
		$("#ActiveStatus")[0].checked = false;
	}
	else {
		$("#ActiveStatus")[0].checked = true;
	}
}
function CheckValidationheadline(){
	if(isValidation){
    if($.trim(jQuery("#headeng").val())==""){
        getvalidated('headeng','text','Headline in English');
        return false;
    }
  else  if($.trim(jQuery("#headhindi").val())==""){
        getvalidated('headhindi','text','Headline in Hindi');
        return false;
    }
	else{
        InsUpdheadline();
    }
}
    else{
        InsUpdheadline();
    }
}