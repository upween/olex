function FillSecuredDistrict_village(funct,control) {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDistrict_village('parsedatasecuredFillSecuredDistrict',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
              jQuery("#ddlSearchDistrict").append(jQuery("<option ></option>").val("0").html("Select District"));

            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
                jQuery("#ddlSearchDistrict").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));

             }
             
        }
              
}
function FillCityByDistrict(funct,control) {
    var path =  serverpath + "secured/city/" +jQuery('#ddlDistrict').val()+ "/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCityByDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCityByDistrict('parsedatasecuredFillCityByDistrict',control)
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Tehsil"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddltehsil").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
             }
             
        }
              
}

jQuery('#ddlDistrict').on('change', function () {
    FillCityByDistrict('parsedatasecuredFillCityByDistrict','ddltehsil');
  });


  function FillCityBySearchDistrict(funct,control) {
    var path =  serverpath + "secured/city/" +jQuery('#ddlSearchDistrict').val()+ "/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCityBySearchDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCityBySearchDistrict('parsedatasecuredFillCityBySearchDistrict',control)
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Tehsil"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddlSearchtehsil").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
             }
             
        }
              
}

jQuery('#ddlSearchDistrict').on('change', function () {
    FillCityBySearchDistrict('parsedatasecuredFillCityBySearchDistrict','ddlSearchtehsil');
  });


  function InsUpdvillage(){

	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_VillageId":localStorage.p_VillageId,
    "p_VillageName":jQuery("#village").val(),
    "p_CityId":jQuery("#ddltehsil").val(),
    "p_IsActive":chkval,
    "p_IpAddress":'0',
    "p_UserId":'0'

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "village";
ajaxpost(path, 'parsrdataheadline', 'comment', MasterData, 'control')
}

function parsrdataheadline(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdvillage();
	}
	else if (data[0][0].ReturnValue == "1") {
     //   Fetchvillage();
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_VillageId = "0";
    $("#chkActiveStatus")[0].checked = false;
        jQuery("#ddlDistrict").val("0")
        $("#ddltehsil").val("0");
        $("#village").val("");
    }


function Fetchvillage() {
        var path = serverpath + "village/"+$("#ddlSearchtehsil").val()+"/0/0/0"
        ajaxget(path, 'parsedataFetchvillage', 'comment', "control");
    }
    function parsedataFetchvillage(data) {

        data = JSON.parse(data)
        var data1 = data[0];
        var appenddata = "";
        var active = "";
        for (var i = 0; i < data1.length; i++) {

            if (data1[i].IsActive == '0') {
            var	active = "No"
            }
             if (data1[i].IsActive == '1') {
              var  active = "Yes"
            }
            appenddata += "<tr><td style='word-break: break-word;'>" + [i+1] + "</td><td style='    word-break: break-word;'>" + data1[i].DistrictName + "</td><td>"+data1[i].CityName+"</td><td style='    word-break: break-word;'>" +data1[i].VillageName + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode('"+encodeURI(data1[i].VillageId)+"','"+encodeURI(data1[i].VillageName)+"','"+encodeURI(data1[i].CityId)+"','"+encodeURI(data1[i].IsActive)+"','"+data1[i].DistrictId+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
        } jQuery("#tbodyvalue").html(appenddata);
    
    
    }


    function EditMode(VillageId,VillageName,CityId,IsActive,DistrictId) {
        jQuery("#ddlDistrict").val(DistrictId)
        jQuery("#village").val(decodeURI(VillageName))
        jQuery("#ddltehsil").val(decodeURI(CityId))
        localStorage.p_VillageId =decodeURI(VillageId)
        if (decodeURI(IsActive) == '0') {
            $("#chkActiveStatus")[0].checked = false;
        }
        else {
            $("#chkActiveStatus")[0].checked = true;
        }
    }

    // function CheckValidation(){

  
    //     if (jQuery('#village').val() == '') {
    //       getvalidated('village','text','village');
    //       return false;
    //   }
    //   else if (jQuery('#ddltehsil').val() == '') {
    //      getvalidated('ddltehsil','text','tehsil');
    //     return false;
    //     }
    // }
    function CheckValidationvillage(){
        if(isValidation){
        if(jQuery("#ddlDistrict").val()=="0"){
            getvalidated('ddlDistrict','select','District');
            return false;
        }
      else  if(jQuery("#ddltehsil").val()=="0"){
            getvalidated('ddltehsil','select','Tehsil');
            return false;
        }
       else if(jQuery("#village").val()==""){
            getvalidated('village','text','Village');
            return false;
        }
         
        else{
            InsUpdvillage();
        }
    }
        else{
            InsUpdvillage();
        }
    }