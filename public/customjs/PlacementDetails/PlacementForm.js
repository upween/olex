$("#myappt").click(function () {
    if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
        InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'delete',"appointment","", $("#myappl").attr('href').split('/')[2]) ;
    }
    setTimeout(function(){

        $("#myapp").replaceWith($("#myapp").val('').clone(true));
        $("#myappus").css("display", "block");
        $("#myappls").css("display", "none");
        sessionStorage.setItem("Appointment",'');
    },300
    )
  
   
})
$("#myappt1").click(function () {
    if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
        InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'delete',"sal1","", $("#myappl1").attr('href').split('/')[2]) ;
    }
    setTimeout(function(){
    $("#myapp1").replaceWith($("#myapp1").val('').clone(true));
    $("#myappus1").css("display", "block");
    $("#myappls1").css("display", "none");
    sessionStorage.setItem("Sal1",'')
},300
)
})
$("#myappt2").click(function () {
    if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
        InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'delete',"sal2","", $("#myappl2").attr('href').split('/')[2]) ;
    }
    setTimeout(function(){

    $("#myapp2").replaceWith($("#myapp2").val('').clone(true));
    $("#myappus2").css("display", "block");
    $("#myappls2").css("display", "none");
    sessionStorage.setItem("Sal2",'')
},300
)
})
$("#myappt3").click(function () {
    if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
        InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'delete',"sal3","", $("#myappl3").attr('href').split('/')[2]) ;
    }
    setTimeout(function(){

    $("#myapp3").replaceWith($("#myapp3").val('').clone(true));
    $("#myappus3").css("display", "block");
    $("#myappls3").css("display", "none");
    sessionStorage.setItem("Sal3",'');
},300
)
})
$("#myappt4").click(function () {
    if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
        InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'delete',"undertaking","", $("#myappl4").attr('href').split('/')[2]) ;
    }
    setTimeout(function(){

    $("#myapp4").replaceWith($("#myapp4").val('').clone(true));
    $("#myappus4").css("display", "block");
    $("#myappls4").css("display", "none");
    sessionStorage.setItem("under",'');
},300
)
})
$("#myappu").click(function () {
    Cookies.set('FileName', "appointment", { expires: 1, path: '/' });
    $('#appointmentletter').ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $("#myapp").replaceWith($("#myapp").val('').clone(true));
                sessionStorage.setItem("Appointment", res[1])
                if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
                    InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'Upload',"appointment","", res[1]) ;
                }
                toastr.success(res[0], "", "success")
                $("#myappus").css("display", "none");
                $("#myappl").attr("href", "/PlacementDetail/" + res[1]);
                $("#myappls").css("display", "block");
            }
        }
    });
    return false;
})
$("#myappu1").click(function () {
    Cookies.set('FileName', "SalarySlip1", { expires: 1, path: '/' });
    $('#appointmentletter').ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $("#myapp1").replaceWith($("#myapp1").val('').clone(true));
                sessionStorage.setItem("Sal1", res[1])
                if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
                    InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'Upload',"Sal1","", res[1]) ;
                }
                toastr.success(res[0], "", "success")
                $("#myappus1").css("display", "none");
                $("#myappl1").attr("href", "/PlacementDetail/" + res[1]);
                $("#myappls1").css("display", "block");
            }
        }
    });
    return false;
})
$("#myappu2").click(function () {
    Cookies.set('FileName', "SalarySlip2", { expires: 1, path: '/' });
    $('#appointmentletter').ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $("#myapp2").replaceWith($("#myapp2").val('').clone(true));
                sessionStorage.setItem("Sal2", res[1])
                if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
                    InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'Upload',"Sal2","", res[1]) ;
                }
                toastr.success(res[0], "", "success")
                $("#myappus2").css("display", "none");
                $("#myappl2").attr("href", "/PlacementDetail/" + res[1]);
                $("#myappls2").css("display", "block");
            }
        }
    });
    return false;
})
$("#myappu3").click(function () {
    Cookies.set('FileName', "SalarySlip3", { expires: 1, path: '/' });
    $('#appointmentletter').ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $("#myapp3").replaceWith($("#myapp3").val('').clone(true));
                sessionStorage.setItem("Sal3", res[1])
                if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
                    InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'Upload',"Sal3","", res[1]) ;
                }
                toastr.success(res[0], "", "success")
                $("#myappus3").css("display", "none");
                $("#myappl3").attr("href", "/PlacementDetail/" + res[1]);
                $("#myappls3").css("display", "block");
            }
        }
    });
    return false;
})
$("#myappu4").click(function () {
    Cookies.set('FileName', "Undertaking", { expires: 1, path: '/' });
    $('#appointmentletter').ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            if (response == "Error No File Selected") {
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $("#myapp4").replaceWith($("#myapp4").val('').clone(true));
                sessionStorage.setItem("under", res[1])
                if(sessionStorage.placementdetailsid && sessionStorage.placementdetailstypelogs=='edit'){
        
                    InsupdPlacementdetailslogs(sessionStorage.placementdetailsid,'Upload',"undertaking","", res[1]) ;
                }
                toastr.success(res[0], "", "success")
                $("#myappus4").css("display", "none");
                $("#myappl4").attr("href", "/PlacementDetail/" + res[1]);
                $("#myappls4").css("display", "block");
            }
        }
    });
    return false;
})
function InsupdPersonalDetail() {
    var Joining_Dt = jQuery("#m_datepicker_5").val().split(" ")
    var Joining_Dt1 = Joining_Dt[0].split("/")
    var Joining_Date = Joining_Dt1[2] + "-" + Joining_Dt1[1] + "-" + Joining_Dt1[0];
    var under,under1,Sal1,Sal11,Sal2,Sal21,Sal3,Sal31,Appointment,Appointment1;
    if (sessionStorage.getItem("under") == ""){
        under="No";
        under1="";
    }
    else{
        under="Yes";
        under1=sessionStorage.getItem("under");
    }
    if (sessionStorage.getItem("Sal3") == ""){
        Sal3="No";
        Sal31="";
    }
    else{
        Sal3="Yes";
        Sal31=sessionStorage.getItem("Sal3");
    }
    if (sessionStorage.getItem("Sal2") == ""){
        Sal2="No";
        Sal21="";
    }
    else{
        Sal2="Yes";
        Sal21=sessionStorage.getItem("Sal2");
    }
    if (sessionStorage.getItem("Sal1") == ""){
        Sal1="No";
        Sal11="";
    }
    else{
        Sal1="Yes";
        Sal11=sessionStorage.getItem("Sal1");
    }
    if (sessionStorage.getItem("Appointment") == ""){
        Appointment="No";
        Appointment1="";
    }
    else{
        Appointment="Yes";
        Appointment1=sessionStorage.getItem("Appointment");
    }
    var MasterData = {
        "p_placement_deail_id": sessionStorage.placementdetailsid?sessionStorage.placementdetailsid:0,
        "p_Position": jQuery("#ddlPositon").val()==null? 0 :jQuery('#ddlPositon').val(),
        "p_candidate_name": jQuery("#txtCandidateName").val(),
        "p_reg_no": jQuery("#txtRegistrationNumber").val(),
        "p_company_name": $("#txtcompanyname").val()==0?null:$("#txtcompanyname").val(),
        "p_sub_company_name": $("#subclient").val(),
        "p_joining_date": Joining_Date,
        "p_ctc": jQuery("#txtYearlyCTCOffered").val(),
        "p_job_location": jQuery("#txtJobLocation").val(),
        "p_candidate_address": jQuery("#txtAddress").val(),
        "p_category_id":  jQuery('#ddlCategory').val()==null? 0 :jQuery('#ddlCategory').val(),
        "p_candidate_district": $("#ddlDistrict").val(),
        "p_mobile_number": $('#txtMobile').val(),
        "p_address_proof": "AddressFile",
        "p_unique_id_no": jQuery("#txtUniqueIDNo").val(),
        "p_appointment_letter_collected": Appointment,
        "p_attach_letter": Appointment1,
        "p_salaryslip_first": Sal1,
        "p_ss_first_attach": Sal11,
        "p_salaryslip_sec": Sal2,
        "p_ss_sec_attach": Sal21,
        "p_salaryslip_third": Sal3,
        "p_ss_third_attach": Sal31,
        "p_Recruiter": $("#textRecruiter").val(),
        "p_candidate_undertaking": under1,
        "p_Email": $("#txtContactEmail").val(),
        "p_con_number": $("#txtContactNumber").val(),
        "p_con_per_name": $("#txtContactPerson").val(),
        "p_Gender": $("#ddlGender").val(),
        "p_EducationType": $("#education").val(),
        "p_can_udertaking": under,
        "p_Entry_By":sessionStorage.Emp_Id,
        "p_Entered_Ip":sessionStorage.ipAddress,
        "p_NotRegisteredComp":$.trim($('#notregemp').val()),
        "p_UpdatedBy":sessionStorage.Emp_Id,
         "p_Remark":$('#remark').val(),
         "p_qualification": $("#Qualification").val(),


    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "secured/placementdetails";
    securedajaxpost(path, 'parsrdataitmenu', 'comment', MasterData, 'control')
}


function parsrdataitmenu(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        InsupdPersonalDetail();
    } else if (data[0][0].ReturnValue == "1") {
        InsupdPlacementdetailslogs(data[0][0].placement_deail_id,'Insert',"","","") ;
        resetMode();
        toastr.success("Insert Successful", "", "success")
        return false;
    } else if (data[0][0].ReturnValue == "3") {
        toastr.warning("already exist", "", "info")
        return false;
    }
    else if (data[0][0].ReturnValue == "2") {
        InsupdPlacementdetailslogs(data[0][0].placement_deail_id,'Update',"","","") ;
        toastr.success("Update Successful", "", "info")
        resetMode();
        sessionStorage.removeItem('placementdetailsid');
        return false;
    }

}

function resetMode() {
    jQuery("#ddlPositon").val('');
        jQuery("#txtCandidateName").val('');
        jQuery("#txtRegistrationNumber").val('');
        $("#txtcompanyname").val('0');
        $("#subclient").val('0');
        jQuery("#m_datepicker_5").val('')
    jQuery("#txtYearlyCTCOffered").val('');
        jQuery("#txtJobLocation").val('');
        jQuery("#txtAddress").val('');
        jQuery("#remark").val('');

        jQuery('#ddlCategory').val('0');
        $("#ddlDistrict").val('0');
        $('#txtMobile').val('');
        $('#Qualification').val('0');
        jQuery("#txtUniqueIDNo").val('');

        $("#textRecruiter").val('');

        $("#txtContactEmail").val('');
        $("#txtContactNumber").val("");
        $("#txtContactPerson").val('');
        $("#ddlGender").val('0')
        $("#education").val('0')
        $("#myapp").replaceWith($("#myapp").val('').clone(true));
        $("#myapp1").replaceWith($("#myapp1").val('').clone(true));
        $("#myapp2").replaceWith($("#myapp2").val('').clone(true));
        $("#myapp3").replaceWith($("#myapp3").val('').clone(true));
        $("#myapp4").replaceWith($("#myapp4").val('').clone(true));
        $("#txtregno").val('');
        $("#txtmono").val('');
}

function FillEmployerByExchange() {

    var path = serverpath + "PlacementEmployer";
    ajaxget(path, 'parsedataEmployerByExchange', 'comment', 'control')
}

function parsedataEmployerByExchange(data) {
    data = JSON.parse(data)



    jQuery("#txtcompanyname").empty();
    var data1 = data[0];


    jQuery("#txtcompanyname").append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));

    for (var i = 0; i < data1.length; i++) {
        jQuery("#txtcompanyname").append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));
    }

}

function FillPosition() {

    var path = serverpath + "adminDesignation/0/0";
    ajaxget(path, 'parsedataPosition', 'comment', 'control')
}

function parsedataPosition(data) {
    data = JSON.parse(data)



    jQuery("#ddlPositon").empty();
    var data1 = data[0];


    jQuery("#ddlPositon").append(jQuery("<option></option>").val('0').html("Select Position"));

    for (var i = 0; i < data1.length; i++) {
        jQuery("#ddlPositon").append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
    }

}

function CheckUser() {
    if ($('#txtRegistrationNumber').val() != "") {
        var path = serverpath + "Jobseekerinfo/'" + $('#txtRegistrationNumber').val() + "'";
        ajaxget(path, 'parsedatacheckuser', 'comment', 'control')
    }

}

function parsedatacheckuser(data) {
    data = JSON.parse(data)



    var data1 = data[0];
    if (data1[0].ReturnValue == '1') {
        $("#validtxtRegistrationNumber").html("Invalid Registration Number")
        $('#txtRegistrationNumber').val("")
    } else {
        console.log(data)
        Cookies.set('modaltype', 'placement', { expires: 1, path: '/' });
        Cookies.set('RegNo', $('#txtRegistrationNumber').val(), { expires: 1, path: '/' });

        jQuery("#txtCandidateName").val(data1[0].CandidateName),
            $("#txtAddress").val(data1[0].PermanentAddress),
            $('#txtMobile').val(data1[0].MobileNumber),
            $("#ddlGender option:selected").text(data1[0].Gender)
        $("#ddlDistrict").val(data1[0].DistrictId)
        $("#ddlCategory").val(data1[0].Category)
      $("#Qualification").val(data1[0].EducationId!==null?data1[0].EducationId:0)
        $("#txtUniqueIDNo").val(data1[0].IdentificationNumber)
        if (data1[0].DesignationId != null) {
            $("#ddlPositon").val(data1[0].DesignationId)
        }
        else{
            $("#ddlPositon").attr('disabled',false);
        }
    }
}
function CheckValidation(){
    var file1 = document.getElementById('myapp');
    var file2 = document.getElementById('myapp1'); 
    var file3 = document.getElementById('myapp2'); 
    var file4 = document.getElementById('myapp3'); 
    var file5 = document.getElementById('myapp4'); 

  
    if($("#txtcompanyname").val()=="0" && $("#notregemp").val()==""){
        getvalidated('txtcompanyname','select','Company Name')
        return false;
    }
    else if ($("#m_datepicker_5").val()==""){
        getvalidated('m_datepicker_5','text','Joining Date')
        return false;
    }
    else if ($("#txtJobLocation").val()==""){
        getvalidated('txtJobLocation','text','Job Location')
        return false;
    }
    else if ($("#txtYearlyCTCOffered").val()==""){
        getvalidated('txtYearlyCTCOffered','text','CTC')
        return false;
    }
    else if ($("#textRecruiter").val()==""){
        getvalidated('textRecruiter','text','Recruiter')
        return false;
    }
    else if ($("#txtContactPerson").val()==""){
        getvalidated('txtContactPerson','text','Contact Person')
        return false;
    }
    else if ($("#txtContactNumber").val()==""){
        getvalidated('txtContactNumber','number','Mobile Number')
        return false;
    }
    else if ($("#txtContactEmail").val()=="0"){
        getvalidated('txtContactEmail','email','Email')
        return false;
    }
  //  else if ($("#education").val()=="0"){
  //      toastr.warning('Please Select Education Type');
//return false;
 //   }
 //else if ($("#Qualification").val()=="0"){
  //      toastr.warning('Please Select Highest Qualification');
//return false;
 //   }

  else if(file1.files.item(0)&&!sessionStorage.getItem("Appointment")){
toastr.warning('Please Upload Selected Appointment Letter');
return false;
  }
  else if(file2.files.item(0)&&!sessionStorage.getItem("Sal1")){
    toastr.warning('Please Upload Selected Month 1 Salary Slip');
    return false;
      }
      else if(file3.files.item(0)&&!sessionStorage.getItem("Sal2")){
        toastr.warning('Please Upload Selected Month 2 Salary Slip');
        return false;
          }
          else if(file4.files.item(0)&&!sessionStorage.getItem("Sal3")){
            toastr.warning('Please Upload Selected Month 3 Salary Slip');
            return false;
              }
              else if(file5.files.item(0)&&!sessionStorage.getItem("under")){
                toastr.warning('Please Upload Selected Candidate Undertaking');
                return false;
                  }
    else{
        InsupdPersonalDetail();
    }
}
function fetchPlacementform() {

    var path = serverpath + "placementdetails/"+sessionStorage.placementdetailsid+"/0/0/0/0/0/0/0/0/0/0/0"
    ajaxget(path, 'parsedatafetchPlacementform', 'comment', "control");
}
function parsedatafetchPlacementform(data) {
    data = JSON.parse(data)
    jQuery("#txtCandidateName,#txtAddress,#txtMobile,#ddlGender,#ddlDistrict,#ddlCategory,#txtUniqueIDNo,#ddlPositon").attr('disabled',false);
 
    if(!data[0][0].Position||data[0][0].Position!=''||data[0][0].Position!=0||data[0][0].Position!=null){
        jQuery("#ddlPositon").val(data[0][0].Position?data[0][0].Position:0);  
    }
    if(!data[0][0].candidate_district||data[0][0].candidate_district!=''||data[0][0].candidate_district!=0||data[0][0].candidate_district!=null){
        jQuery("#ddlDistrict").val(data[0][0].candidate_district?data[0][0].candidate_district:0);  
    }
        jQuery("#txtCandidateName").val(data[0][0].candidate_name);
        jQuery("#txtRegistrationNumber").val(data[0][0].reg_no);
        Cookies.set('RegNo', data[0][0].reg_no, { expires: 1, path: '/' });
        if(data[0][0].company_name==0 || data[0][0].company_name===null){
            $('#notregemp').val(data[0][0].NotRegisteredComp)
        }
        else{

            $("#txtcompanyname").val(data[0][0].company_name);
        }
        if(data[0][0].company_name=='00005202000034'){
            $('.rpttype').show();
            FillSubCompany();
            setTimeout(() => {
              
        $("#subclient").val(data[0][0].sub_client_id);  
            }, 300);
                }
            
        jQuery("#m_datepicker_5").val(data[0][0].joining_date)
    jQuery("#txtYearlyCTCOffered").val(data[0][0].ctc);
        jQuery("#txtJobLocation").val(data[0][0].job_location);
        jQuery("#txtAddress").val(data[0][0].candidate_address);
        jQuery('#ddlCategory').val(data[0][0].category_id);
        $('#txtMobile').val(data[0][0].mobile_number);
        $("#ddlGender option:selected").text(data[0][0].Gender)
        jQuery("#txtUniqueIDNo").val(data[0][0].unique_id_no);

        $("#textRecruiter").val(data[0][0].Recruiter);

        $("#txtContactEmail").val(data[0][0].Email);
        $("#txtContactNumber").val(data[0][0].con_number);
        $("#txtContactPerson").val(data[0][0].con_per_name);
        $('#txtRegistrationNumber').val(data[0][0].reg_no);
          $('#remark').val(data[0][0].Remark);
           
    if(data[0][0].EducationType&&data[0][0].EducationType!=''&&data[0][0].EducationType!=0&&data[0][0].EducationType!==null){

        $('#education').val(data[0][0].EducationType);
    }
    if(data[0][0].qualification&&data[0][0].qualification!=''&&data[0][0].qualification!=0&&data[0][0].qualification!==null){
  
        $('#Qualification').val(data[0][0].qualification);
    }
   
        jQuery("#txtCandidateName").val(data[0][0].candidate_name),
            $("#txtAddress").val(data[0][0].candidate_address),
        
        $("#txtUniqueIDNo").val(data[0][0].unique_id_no)
        if (data[0][0].appointment_letter_collected == 'Yes') {
            $("#myappus").css("display", "none");
            $("#myappl").attr("href", "/PlacementDetail/" + data[0][0].attach_letter);
            $("#myappls").css("display", "block");
            Appointment1=sessionStorage.setItem("Appointment",data[0][0].attach_letter);
        }
        if (data[0][0].salaryslip_first == 'Yes') {
            $("#myappus1").css("display", "none");
            $("#myappl1").attr("href", "/PlacementDetail/" + data[0][0].ss_first_attach);
            $("#myappls1").css("display", "block");
            sessionStorage.setItem("Sal1",data[0][0].ss_first_attach);
        }
        if (data[0][0].salaryslip_sec == 'Yes') {
            $("#myappus2").css("display", "none");
                $("#myappl2").attr("href", "/PlacementDetail/" +  data[0][0].ss_sec_attach);
                $("#myappls2").css("display", "block");
                sessionStorage.setItem("Sal2",data[0][0].ss_sec_attach);
        }
        if (data[0][0].salaryslip_third == 'Yes') {
            $("#myappus3").css("display", "none");
            $("#myappl3").attr("href", "/PlacementDetail/" +  data[0][0].ss_third_attach);
            $("#myappls3").css("display", "block");
            sessionStorage.setItem("Sal3",data[0][0].ss_third_attach);
        }
        if (data[0][0].can_udertaking == 'Yes') {
            $("#myappus4").css("display", "none");
            $("#myappl4").attr("href", "/PlacementDetail/" + data[0][0].candidate_undertaking);
            $("#myappls4").css("display", "block");
            sessionStorage.setItem("under",data[0][0].candidate_undertaking);
        }
}

function FillSubCompany() {

    var path = serverpath + "companysubclientname/0/'Y'"
    ajaxget(path, 'parsedataSubCompany', 'comment', 'control')
}

function parsedataSubCompany(data) {
    data = JSON.parse(data)



    jQuery("#subclient").empty();
    var data1 = data[0];


    jQuery("#subclient").append(jQuery("<option></option>").val('0').html("Select Sub Company Name"));

    for (var i = 0; i < data1.length; i++) {
        jQuery("#subclient").append(jQuery("<option></option>").val(data1[i].sub_client_id).html(data1[i].sub_client_name));
    }


}

function handleExchChange(){
    if($('#txtcompanyname').val()=='00005202000034'){
$('.rpttype').show();
FillSubCompany()
    }
    else{
        $('.rpttype').hide(); 
        
        
        
    }
}

function FillJSExamPassed(funct,control,qual_level) {
    var path =  serverpath + "secured/qualification/0/0/0"
    securedajaxget(path,'parsedatasecuredFillJSExamPassed','comment',control);
}

function parsedatasecuredFillJSExamPassed(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJSExamPassed('parsedatasecuredFillJSExamPassed','ExamPassed',jQuery('#QualificationLevel').val());
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#Qualification").empty();
            jQuery("#Qualification").append(jQuery("<option ></option>").val("0").html("Select"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#Qualification").append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}

