var rowinserted = 0;
var tableData = "";
var srno=0;
$('#reportupload').submit(function () {
    $(this).ajaxSubmit({
        error: function (xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function (response) {
            console.log(response);
            if (response.err_desc == "No file passed") {
                toastr.warning(response.err_desc, "", "info")
            }
            else if (response == "Request Entity Too Large") {
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: PDF and Image File Only!") {
                toastr.warning(response, "", "info")
            }
            else {
                var str = response;
                var res = str.split("!");
                $('#modalresultPlacement').modal('toggle');
                $('#sbmitbtn').show();
                var candidatelist = res[1];
                var candidatelist1 = JSON.parse(candidatelist);
                var Candidatelist = '';
                console.log(candidatelist1);
                var dateofjoining='';
                var tablehead="<tr><th>SrNo</th><th>Position</th><th>Candidate Name</th><th>RegistratonNumber</th><th>Company/ Client Name</th><th>Company HR Name</th><th>Company HR Number</th><th>Company HR Email</th><th>Date Joining</th><th>Yearly CTC Offered</th><th>Job Location</th><th>Address</th><th>District</th><th>Category</th><th>Mobile Number</th><th>UniqueID Number</th><th>Not Registered Employer</th></tr>";
                $("#tablehead").html(tablehead);
                $('#excelicon').hide();
                for (var i = 0; i < candidatelist1.length; i++) {
                    var regno='';
                    if($.trim(candidatelist1[i]['registration number'])!=""){
                        regno=candidatelist1[i]['registration number'].replace(/(?!-)[^0-9.]/g, "");
                    }else{
                        regno="";
                    }
                    var dayofdate=$.trim(candidatelist1[i]['date of joining (dd)']);
                  var dateofjoining=dayofdate+'/'+$('#Month').val()+'/'+$('#Year').val();
                    
                                   
                    
                    Candidatelist += "<tr class='i' ><td>" + [i + 1] + "</td><td><select class='form-control m-input Position' id='position" + [i + 1] + "' style='min-width:175px'><option>Select Positon</option></select></td><td>" + candidatelist1[i]['candidate name'] + "</td><td>" +regno+ "</td><td><select class='form-control m-input clientname' id='clientname" + [i + 1] + "' style='min-width:175px'>/select></td><td>" + candidatelist1[i]['company hr name'] + "</td><td>" + candidatelist1[i]['company hr number'] + "</td><td>" + candidatelist1[i]['company hr email'] + "</td><td>" +dateofjoining+ "</td><td>" + candidatelist1[i]['yearly ctc offered'] + "</td><td>" + candidatelist1[i]['job location'] + "</td><td>" + candidatelist1[i]['address'] + "</td><td><select class='form-control m-input district' id='district" + [i + 1] + "' style='min-width:175px'>/select></td><td><select class='form-control m-input category' style='min-width:175px' id='category" + [i + 1] + "'>/select></td><td>" + candidatelist1[i]['mobile number'] + "</td><td>" + candidatelist1[i]['uniqueid number'] + "</td><td class='notregemp' id='notregemp" + [i] + "'>"+candidatelist1[i]['not registered employer']+"</td><tr>"
                    // 
                }
                $("#tbodyvalue").html(Candidatelist);
                setTimeout(function(){ 
                FillEmployerByExchange(candidatelist1);
                },0);
                setTimeout(function(){ 
                FillCandidateCategory(candidatelist1);
                },0);
                setTimeout(function(){ 
                FillCandidatePosition(candidatelist1);
                },0);
                setTimeout(function(){ 
                FillCandidateDistrict(candidatelist1);
                },0);
                //setTimeout(function(){ 
                //          for (var i = 0; i < candidatelist1.length; i++) {
                //     $('.clientname#clientname' + [i+1] + ' option:contains(' + candidatelist1[i]['company/client name'] +')').prop('selected', true);
                //     $('.category#category' + [i+1] + ' option:contains(' + candidatelist1[i]['category'] +')').prop('selected', true);
                // }; }, 300);




                $('#tbodyvalue').clientSidePagination();
                //addTopScrollbar($('#tbodyvalue').closest('table'), $('#tbodyvalue').closest('.form-group.m-form__group.row'));
                
            }
        }
    });
    return false;
});



function FillEmployerByExchange(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "PlacementEmployer",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".clientname").empty();
            var data1 = data[0];


            jQuery(".clientname").append(jQuery("<option></option>").val('0').html("Select Company/Client Name"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(".clientname").append(jQuery("<option></option>").val(data1[i].Emp_Regno).html(data1[i].CompName));

            }
          

            for (var i = 0; i < candidatelist1.length; i++) {
                $("select.clientname#clientname" + [i + 1]+" option").each(function (key) {
                   if($.trim(candidatelist1[i]['company/client name']).indexOf($.trim($(this).text()))!=-1){
                       $(this).prop('selected', true);
                        return false;
                    }
                })
              //  $('.clientname#clientname' + [i + 1] + ' option:contains(' +  + ')').prop('selected', true);
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function FillCandidateCategory(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "category/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".category").empty();
            var data1 = data[0];


            jQuery(".category").append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(".category").append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
            }
            
            for (var i = 0; i < candidatelist1.length; i++) {
                //     $('.clientname#clientname' + [i+1] + ' option:contains(' + candidatelist1[i]['company/client name'] +')').prop('selected', true);
                $('.category#category' + [i + 1] + ' option:contains(' + candidatelist1[i]['category'] + ')').prop('selected', true);
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function FillCandidatePosition(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "adminDesignation/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".Position").empty();
            var data1 = data[0];


            jQuery(".Position").append(jQuery("<option></option>").val('0').html("Select Position"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(".Position").append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
            }
            for (var i = 0; i < candidatelist1.length; i++) {
                $("select.Position#position" + [i + 1]+" option").each(function (key) {
                   if( candidatelist1[i]['position'].indexOf($(this).text())!=-1){
                       $(this).prop('selected', true);
                        return false;
                    }
                })
            
            }
           
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
function FillCandidateDistrict(candidatelist1) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "district/19/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {

            jQuery(".district").empty();
            var data1 = data[0];


            jQuery(".district").append(jQuery("<option></option>").val('0').html("Select District"));

            for (var i = 0; i < data1.length; i++) {
                jQuery(".district").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
            }
            for (var i = 0; i < candidatelist1.length; i++) {
                if(candidatelist1[i]['district']!=='' || candidatelist1[i]['district']!==null){
                    $('.district#district' + [i + 1] + ' option:contains(' + candidatelist1[i]['district'] + ')').prop('selected', true);

                }
               
            };
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}

function InsPlacementdetail() {
          
    $('#sbmitbtn').attr('disabled',true);
    
    $("#tableglobal tr.i").each(function (key) {
  
        $.blockUI();
        var this_row = $(this);
        var date = moment($.trim(this_row.find('td:eq(8)').html()),'DD/MM/YYYY').format('YYYY-MM-DD');
        var MasterData = {
            "p_placement_deail_id": 0,
            "p_Position": $.trim(this_row.find('td:eq(1) > select.Position').val()),
            "p_candidate_name": $.trim(this_row.find('td:eq(2)').html()),
            "p_reg_no": $.trim(this_row.find('td:eq(3)').html()),
            "p_company_name": $.trim(this_row.find('td:eq(4) > select.clientname').val()),
            "p_joining_date": date=='Invalid date'?'2020-01-01':date,
            "p_ctc": $.trim(this_row.find('td:eq(9)').html()),
            "p_job_location": $.trim(this_row.find('td:eq(10)').html()),
            "p_candidate_address": $.trim(this_row.find('td:eq(11)').html()),
            "p_category_id": $.trim(this_row.find('td:eq(13)  > select.category').val()),
            "p_candidate_district": $.trim(this_row.find('td:eq(12) > select.district').val()),
            "p_mobile_number": $.trim(this_row.find('td:eq(14)').html()),
            "p_address_proof": "AddressFile",
            "p_unique_id_no": $.trim(this_row.find('td:eq(15)').html()),
            "p_appointment_letter_collected": 'No',
            "p_attach_letter": '',
            "p_salaryslip_first": 'No',
            "p_ss_first_attach": '',
            "p_salaryslip_sec": 'No',
            "p_ss_sec_attach": '',
            "p_salaryslip_third": 'No',
            "p_ss_third_attach": '',
            "p_Recruiter": '',
            "p_candidate_undertaking": '',
            "p_Email": $.trim(this_row.find('td:eq(7)').html()),
            "p_con_number": $.trim(this_row.find('td:eq(6)').html()),
            "p_con_per_name": $.trim(this_row.find('td:eq(5)').html()),
            "p_Gender": '0',
            "p_can_udertaking": 'No',
            "p_Entry_By": sessionStorage.Emp_Id,
            "p_Entered_Ip": sessionStorage.ipAddress,
            "p_NotRegisteredComp":$.trim(this_row.find('td:eq(16)').html()),
            "p_UpdatedBy":'0'


        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "placementdetailsexcel";
        jQuery.ajax({
            url: path,
            type: "POST",
            headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
            data: MasterData,
            async:false,
            contentType: "application/json; charset=utf-8",
            success: function (data, status, jqXHR) {
              
              
                   if(data[0][0].ReturnValue=='1'){
                    rowinserted++;
                    }
                    else if(data[0][0].ReturnValue=='2'){
                        srno++;
                        tableData+='<tr><td>'+srno+'</td><td>'+data[0][0].candidate_name+'</td><td>'+data[0][0].joining_date+'</td><td>'+data[0][0].company_name+'</td></tr>'
                       
                    }
            },
            error: function (errordata) {
             
                if (errordata.status == 0) {
                    toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                    return true;
                }
                else if (errordata.status == 401) {
                    toastr.warning("Unauthorized", "", "info")
                    return true;
                }
                else {
                    toastr.warning(errordata, "", "info")
                    return true;
                }
            }
        });
     });
     $.unblockUI();
     
     $('#sbmitbtn').attr('disabled',false);
    $('#sbmitbtn').hide();
    console.log('Total Row inserted',rowinserted);
    $("#tbodyvalue").empty();
    $("#tablehead").empty();
    toastr.success('Inserted sucessfully');
    
    if(tableData!=""){
        var tablehead="<tr ><th colspan='4'>These candidate are duplicates Please check their details</th></tr><tr><th></th><th>Candidate Name</th><th>Joining  Date</th><th>Client/Company Name</th></tr>";
        $("#tablehead").html(tablehead);
        $("#tbodyvalue").html(tableData);
        $('#excelicon').show();
    }
    
 

}




// function parsrdataplacementDetail(data) {
//     data = JSON.parse(data)
//     $.unblockUI();
//        if(data[0][0].ReturnValue=='1'){
//         rowinserted++;
//         }
//         else if(data[0][0].ReturnValue=='2'){
//             console.log("in");
//             tableData+='<tr><td>'+data[0][0].candidate_name+'</td><td>'+data[0][0].joining_date+'</td><td>'+data[0][0].company_name+'</td></tr>'
//             console.log(tableData);
//         }

// }
function checkValidation() {
    var validate=true;
    $("#tableglobal tr.i").each(function (key) {
        if ($('#postion' + [key + 1] + '').val() == '0') {
            toastr.warning('Please Select Position for Candidate' + [key + 1]);
            validate=false;
            return false;
        }
        if ($('#clientname' + [key + 1] + '').val() == '0' && $('#notregemp' + [key] + '').html() == '') {
            toastr.warning('Please Select or Enter Company/Client Name for Candidate' + [key + 1]);
            validate=false;
            return false;
        }

    });
    if(validate){
        $('#sbmitbtn').attr('disabled',true);
    $.blockUI();
    setTimeout(function(){ InsPlacementdetail();}, 200);
    
    }
}
function FillMonthPlacement(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillMonthPlacement(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillMonthPlacement('parsedatasecuredFillMonthPlacement',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#Month").empty();
    var data1 = data[0];
    jQuery("#Month").append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#Month").append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
    }
    
    }
    
    }
    function FillYearPlacement(funct,control) {
        var path =  serverpath + "secured/year/0/20"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearPlacement(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearPlacement('parsedatasecuredFillYearPlacement',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#Year").empty();
                var data1 = data[0];
                jQuery("#Year").append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#Year").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
            }
                  
    }
    function checkUpload(){
        if($('#Month').val()=='0'){
            toastr.warning("Please Select Month");
        }
       else if($('#Year').val()=='0'){
            toastr.warning("Please Select Year");
        }
        else{
             $('#postattach').click()
        }
    }
