function FillCallCentreManager(funct,control) {
    var path =  serverpath + "CallCentreManager"
    securedajaxget(path,funct,'comment',control);
}

function parsedataFillCallCentreManager(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCallCentreManager('parsedataFillCallCentreManager',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            if(control=='ddlentryby'){
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            }
            else{
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Manager"));
            }
            
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
             }
        }
}

function fetchPlacmentdetailslogs() {
    var entryby=0;
    if(sessionStorage.User_Type_Id!='0' && sessionStorage.User_Type_Id!='1'){
        entryby=sessionStorage.Emp_Id;
    }
    var data =$('#m_datepicker_5').val();
    var arr = data.split('/');
    var frm_dt=arr[2] + "-" + arr[1]+"-"+arr[0]

    var data1 =$('#m_datepicker_6').val();
    var arr1 = data1.split('/');
    var to_dt=arr1[2] + "-" + arr1[1]+"-"+arr1[0]
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "placementdetailslogs/"+frm_dt+"/"+to_dt+"/"+entryby+"",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
    var head="<th>Report Name-Placment Logs </th><th>From Date-"+$('#m_datepicker_5').val()+"</th><th>TO Date-"+$('#m_datepicker_6').val()+"</th>";
    $("#headingtext").html("Placment Logs"+$('#m_datepicker_5').val()+'-'+$('#m_datepicker_6').val() )
    var head='<th></th><th colspan=5> Report Name : Placement Logs '+$('#m_datepicker_5').val()+'-'+$('#m_datepicker_6').val()+'</th>';
    $('#Detailhead').html(head)
   			var appenddata="";
			
				for (var i = 0; i < data1.length; i++) {
				
				appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i].candidate_name+"</td><td>"+data1[i].CompName+"</td><td>"+data1[i].Action+"</td><td>"+data1[i].EntryBy+"</td><td>"+data1[i].EntryOn+"</td></tr>";
				 }jQuery("#tbodyvalue").html(appenddata); 
                 $('#Detailhead').html(head);

                 
        },
        
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}


