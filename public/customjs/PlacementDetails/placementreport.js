function fetchPlacement() {
    sessionStorage.mnth=$('#selectedjobseekerdetailmonth').val()?$('#selectedjobseekerdetailmonth').val():0;
    sessionStorage.year=$('#ddlyear').val()?$('#ddlyear').val():0;
    sessionStorage.district =$('#ddlDistrict').val()?$('#ddlDistrict').val():0;
    sessionStorage.Category =$('#ddlCategory').val()?$('#ddlCategory').val():0;
    sessionStorage.Gender =$('#ddlGender').val()?$('#ddlGender').val():0;
    sessionStorage.Position =$('#ddlPositon').val()?$('#ddlPositon').val():0;
    sessionStorage.Client=$('#txtcompanyname').val()?$('#txtcompanyname').val():0;
    sessionStorage.Sort =$('#ddlsortby').val()?$('#ddlsortby').val():'SrNo';


     if($.trim($('#selectedjobseekerdetailmonth').val())=='0' && ($.trim( $("#txtregnoqwer").val()) =='' && $.trim($("#txtmono").val()) =='')){
        toastr.warning("Please Select Month", "info")
    }
    else  if($.trim($('#ddlyear').val())=='0' && ($.trim( $("#txtregnoqwer").val()) =='' && $.trim($("#txtmono").val()) =='')){
        toastr.warning("Please Select Year", "info")
    }
else{
    var entryby;
if(sessionStorage.User_Type_Id==0 ||sessionStorage.User_Type_Id==1){
entryby=$('#ddlentryby').val();
}
else{
    entryby=sessionStorage.Emp_Id;
}
var regno=$.trim($("#txtregnoqwer").val())==''?0:$.trim($("#txtregnoqwer").val());
var mobno=$.trim($("#txtmono").val())==''?0:$.trim($("#txtmono").val());
    var path = serverpath + "placementdetails/0/" + $("#ddlCategory").val() + "/" + $("#ddlDistrict").val() + "/'" + $("#selectedjobseekerdetailmonth").val() + "'/'" + $("#ddlyear").val() + "'/" + $("#ddlGender").val() + "/" + $("#txtcompanyname").val() + "/" + $("#ddlPositon").val() + "/"+entryby+"/'"+$("#ddlsortby").val() +"'/'"+regno+"'/'"+mobno+"'"
    ajaxget(path, 'parsedatafetchPlacement', 'comment', "control");
}
}
function parsedatafetchPlacement(data) {
    data = JSON.parse(data)
    var appenddata = "";
console.log(data)
    var data1 = data[0];
    for (var i = 0; i < data1.length; i++) {
        var appointment_letter = "";
        var salaryslip_first = "";
        var salaryslip_sec = "";
        var salaryslip_third = "";
        var can_udertaking = "";
        if (data1[i].appointment_letter_collected == 'Yes') {
            if(data1[i].IsVerified_appointment=='No'){
                if(sessionStorage.User_Type_Id=='0'){
                    appointment_letter = "<a target='/blank' href='../PlacementDetail/"+data1[i].attach_letter+"' style='color:red'>Yes</a><br><br><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=verifyPlacmentDocuments('"+data1[i].placement_deail_id+"','IsVerified_appointment')>Verify</button>"
       
                }
                if(sessionStorage.User_Type_Id=='31'){
                    appointment_letter = "<a target='/blank' href='../PlacementDetail/"+data1[i].attach_letter+"' style='color:red'>Yes</a><br><br><span style='color:red'>Not Verified</span>"
       
                }
                if(sessionStorage.Emp_Id=='27'){
                    appointment_letter = "<a target='/blank' href='../PlacementDetail/"+data1[i].attach_letter+"' >Yes</a>"
       
                }
               
            }
            else{
                appointment_letter = "<a target='/blank' href='../PlacementDetail/"+data1[i].attach_letter+"'>Yes</a><br><br><span style='color:green'>Verified</span>"
       
            }
        }
        else{
            appointment_letter = data1[i].appointment_letter_collected
        }
        if (data1[i].salaryslip_first == 'Yes') {
            if(data1[i].IsVerified_Sal1=='No'){
                     if(sessionStorage.User_Type_Id=='0'){
            salaryslip_first = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_first_attach+"' style='color:red'>Yes</a><br><br><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=verifyPlacmentDocuments('"+data1[i].placement_deail_id+"','IsVerified_Sal1')>Verify</button>"
           
                 }
                  if(sessionStorage.User_Type_Id=='31'){
                salaryslip_first = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_first_attach+"' style='color:red'>Yes</a><br><br><span style='color:red'>Not Verified</span>"
   
            }
            if(sessionStorage.User_Type_Id=='1'){
                salaryslip_first = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_first_attach+"' >Yes</a>"
   
            }
        }
        else{
            salaryslip_first = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_first_attach+"'>Yes</a><br><br><span style='color:green'>Verified</span>"
       
        }
    }
        else{
            salaryslip_first = data1[i].salaryslip_first
        }
        if (data1[i].salaryslip_sec == 'Yes') {
            if(data1[i].IsVerified_Sal2=='No'){   
                  if(sessionStorage.User_Type_Id=='0'){
            salaryslip_sec = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_sec_attach+"' style='color:red'>Yes</a><br><br><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=verifyPlacmentDocuments('"+data1[i].placement_deail_id+"','IsVerified_Sal2')>Verify</button>"
                }
            if(sessionStorage.User_Type_Id=='31'){
                salaryslip_sec = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_sec_attach+"' style='color:red'>Yes</a><br><br><span style='color:red'>Not Verified</span>"
   
            }
            if(sessionStorage.User_Type_Id=='1'){
                salaryslip_sec = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_sec_attach+"' >Yes</a>"
   
            }
        }
        else{
            salaryslip_sec = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_sec_attach+"'>Yes</a><br><br><span style='color:green'>Verified</span>"
       
        }
    }
        else{
            salaryslip_sec = data1[i].salaryslip_sec
        }
        if (data1[i].salaryslip_third == 'Yes') {
            if(data1[i].IsVerified_Sal3=='No'){
                if(sessionStorage.User_Type_Id=='0'){
            salaryslip_third = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_third_attach+"' style='color:red'>Yes</a><br><br><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=verifyPlacmentDocuments('"+data1[i].placement_deail_id+"','IsVerified_Sal3')>Verify</button>"
                }
            if(sessionStorage.User_Type_Id=='31'){
                 salaryslip_third = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_third_attach+"' style='color:red'>Yes</a><br><br><span style='color:red'>Not Verified</span>"
   
            }
            if(sessionStorage.User_Type_Id=='1'){
                salaryslip_third = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_third_attach+"' >Yes</a>"
  
           }
        }
        else{
            salaryslip_third = "<a target='/blank' href='../PlacementDetail/"+data1[i].ss_third_attach+"'>Yes</a><br><br><span style='color:green'>Verified</span>"
       
        }
    }
        else{
            salaryslip_third = data1[i].salaryslip_third
        }
        if (data1[i].can_udertaking == 'Yes') {
            if(data1[i].IsVerifiied_Undertaking=='NO'){
                if(sessionStorage.User_Type_Id=='0'){
            can_udertaking = "<a target='/blank' href='../PlacementDetail/"+data1[i].candidate_undertaking+"' style='color:red'>Yes</a><br><br><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=verifyPlacmentDocuments('"+data1[i].placement_deail_id+"','IsVerifiied_Undertaking')>Verify</button>"
          
                  }
                    if(sessionStorage.User_Type_Id=='31'){
               can_udertaking = "<a target='/blank' href='../PlacementDetail/"+data1[i].candidate_undertaking+"' style='color:red'>Yes</a><br><br><span style='color:red'>Not Verified</span>"
   
            }
            if(sessionStorage.User_Type_Id=='1'){
                can_udertaking = "<a target='/blank' href='../PlacementDetail/"+data1[i].candidate_undertaking+"'>Yes</a>"
    
             }
        }
        else{
            can_udertaking = "<a target='/blank' href='../PlacementDetail/"+data1[i].candidate_undertaking+"'>Yes</a><br><br><span style='color:green'>Verified</span>"
       
        }
    }
        else{
            can_udertaking = data1[i].can_udertaking
        }
      if(data1[i].Emp_Name!=null){
          var Emp_Name=data1[i].Emp_Name;
      }
      else{
        var Emp_Name='';
      }
      if(sessionStorage.User_Type_Id!='1'){
          var modifyButton="<button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'  onclick=editMode('"+data1[i].placement_deail_id+"')>Modify</button>"
      }
      else{
        var modifyButton="-"
     
      }
      if(sessionStorage.User_Type_Id=='0'){
          var btnhead=`<th>Delete</th>`;
          var btnbody=`<td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'  onclick=aboutdelete(${data1[i].placement_deail_id})>Delete</button></td>`
      }
      else{
        var btnhead=``;
        var btnbody=``
      }
      if(sessionStorage.User_Type_Id=='31' || sessionStorage.User_Type_Id=='0'){
var subclienthead=`<th>Sub Client Name</th>`;
var entryhead=`<th>Entry By</th><th>Entry Date</th>`;


var entrybody=`<td>${Emp_Name}</td><td>${data1[i].Entry_Date}</td>`;
  if(data1[i].sub_client_name===null){
        var  sub_client_name='<td></td>';
     }
     else{
       var  sub_client_name=`<td>${data1[i].sub_client_name}</td>`;
     }

      }
      else{
        var subclienthead='';
        var entryhead='';
        var entrybody='';
        var sub_client_name='';
      }
      var tablehead="<th>Modify</th>"+btnhead+"<th><a  style='color: white;'>Sr.&nbsp;No</a></th><th>Position</th><th>Candidate Name</th><th>Registration Number</th><th>Candidate Registered District</th><th>Qualification</th><th>Education Type</th><th>Company/Client Name</th><th>Company HR Name</th><th>Company HR&nbsp;Number </th><th>Company HR&nbsp;Email</th>"+subclienthead+"<th>Date of Joining</th><th>Yearly&nbsp;CTC Offered</th><th>Placement Category</th><th>Job Location</th><th>Address</th><th>Category</th><th>Candidate District</th><th>Mobile Number</th><th>Adhaar Number</th><th>Appointment Letter&nbsp;Collected</th><th> 1&nbsp;Month Salary&nbsp;Slip</th><th> 2&nbsp;Month Salary&nbsp;Slip</th><th>3&nbsp;Month Salary&nbsp;Slip</th><th>Candidate Undertaking</th><th>Recruiter Name</th><th>Remark</th>"+entryhead+"";
      if(data1[i].CompName=='null' || data1[i].CompName==null){
var CompName=data1[i].NotRegisteredComp;
      }
      else{
        var CompName=data1[i].CompName;
      }
            if(data1[i].reg_no !=''){
var regDistrictName=data1[i].RegDistrictName ;
      }
      else{
    var regDistrictName='';
     }
     var num_ctc=Number( data1[i].ctc.replace(/[^0-9\.]+/g,""));
    // console.log(num_ctc.length)
     if(num_ctc.toString().length>3){
        if(num_ctc>220000){
            var ctc_level='Higher';
         }
         else{
            var ctc_level='Normal';
         }
     }
     else{
        if(num_ctc>2.2){
            var ctc_level='Higher';
         }
         else{
            var ctc_level='Normal';
         }
     }
    

        appenddata += "<tr style='color:black'><td>"+modifyButton+"</td>"+btnbody+"<td style='break-word;'>" + data1[i].SrNo + "</td><td style='    word-break: break-word;'>" + data1[i].Designation + "</td><td style='     break-word;'>" + data1[i].candidate_name + "</td><td>" + data1[i].reg_no + "</td><td>"+regDistrictName+"</td><td>" + data1[i].Qualif_name + "</td><td>" + data1[i].EducationType + "</td><td>" + CompName + "</td><td>" + data1[i].con_per_name + "</td><td>" + data1[i].con_number + "</td><td>" + data1[i].Email + "</td>"+sub_client_name+"<td>" + data1[i].joining_date + "</td><td>" +data1[i].ctc+ "</td><td>"+ctc_level+"</td><td>" + data1[i].job_location + "</td><td>" + data1[i].candidate_address + "</td><td>" + data1[i].CategoryName + "</td><td>" + data1[i].DistrictName + "</td><td>" + data1[i].mobile_number + "</td><td>" + data1[i].unique_id_no + "</td><td>" + appointment_letter + "</td><td>" + salaryslip_first + "</td><td>" + salaryslip_sec + "</td><td>" + salaryslip_third + "</td><td>" + can_udertaking + "</td><td>" + data1[i].Recruiter + "</td><td>"+data1[i].Remark+"</td>"+entrybody+"</tr>";
    }
    $("#tablehead").html(tablehead);
    jQuery("#tbodyvalue").html(appenddata);
    
   // $('#tbodyvalue').clientSidePagination();
    
    addTopScrollbar($('#tbodyvalue').closest('table'), $('#tbodyvalue').closest('.form-group.m-form__group.row'));
    sessionStorage.removeItem('flag')      

}
function editMode(id){
sessionStorage.placementdetailsid=id;
sessionStorage.placementdetailstype='edit';
sessionStorage.flag='edit';
window.location='/placementdetails'
}
function FillDistrictbyEmp(funct,control) {
    var path =  serverpath + "getditrictbyemp/19/"+sessionStorage.CandidateId+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDistrictbyEmp(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrictbyEmp('parsedatasecuredFillDistrictbyEmp',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
         
            for (var i = 0; i < data1.length; i++) {
                
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             
        }
              
}
function verifyPlacmentDocuments(placementid,type) {
    
    var MasterData = {
        "p_placement_deail_id":placementid,
        "p_documentType": type,
       
        

    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "verifyplacement_documents";
    securedajaxpost(path, 'parsrdataitverify', 'comment', MasterData, 'control')
}


function parsrdataitverify(data) {

data = JSON.parse(data)
fetchPlacement();
toastr.success('Verified Sucessfully')
}


function FillCallCentreManager(funct,control) {
    var path =  serverpath + "CallCentreManager"
    securedajaxget(path,funct,'comment',control);
}

function parsedataFillCallCentreManager(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCallCentreManager('parsedataFillCallCentreManager',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            if(control=='ddlentryby'){
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            }
            else{
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Manager"));
            }
            
            for (var i = 0; i < data1.length; i++) {
                if(sessionStorage.User_Type_Id=='0'){
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Emp_Name));
           
                }
                else{
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].DistrictName));
           
                }
             }
        }
}



function FillPlacementDetails(funct,control,Flag) {
if($.trim($("#month").val())=='0' && ($.trim( $("#txtregnoqwer").val()) =='' && $.trim($("#txtmono").val()) =='')){
    toastr.warning("Please Select Month", "", "info")
    return true;
}
else if($.trim($("#year").val())=='0' && ($.trim( $("#txtregnoqwer").val()) =='' && $.trim($("#txtmono").val()) =='')){
    toastr.warning("Please Select Year", "", "info")
    return true;
}
else if($("#ddlManager").val()=='0'){
    toastr.warning("Please Select Manager", "", "info")
    return true;
}
else{
    sessionStorage.setItem('flag',Flag)
    var path =  serverpath + "CallCentreManagerPlacementDetails/"+$("#month").val()+"/"+$("#year").val()+"/"+$("#ddlManager").val()+"/"+$("#txtregno").val()+"/"+$("#txtmono").val()+"/"+Flag+""
        securedajaxget(path,funct,'comment',control);
}
}
function parsedataFillPlacementDetails(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillPlacementDetails('parsedataFillPlacementDetails',control,sessionStorage.flag);
    }
   
       if(sessionStorage.flag=='1'){
           $("#count").val(data[0][0].ReturnValue);
       }

       else if(sessionStorage.flag=='2'){
        $("#myModal").modal('hide')
        toastr.success("Delete Successful", "", "success")
        return true;
       }
}

function deleteMode(placementid) {
    var path =  serverpath + "delete_candidate_placementdetails/"+placementid+""
    securedajaxget(path,'parsedatadeleteplacement','comment','control');
}

function parsedatadeleteplacement(data,control){  
    data = JSON.parse(data)
   if(data[0][0].ReturnValue=='1')
   {
    fetchPlacement();
       toastr.success("Delete Successfully");
   }
}
function aboutdelete(Id){
    $("#myModal1").modal('show');
    $("#deletebtn1").on('click',function(){
        deleteMode(Id);
   $("#myModal1").modal('hide')
    })
   }