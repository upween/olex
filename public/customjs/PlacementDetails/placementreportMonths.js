function FillPlacementStatus() {
    var path = serverpath +"PlacementStatusMM/'"+$('#month').val()+"'/"+$('#textContactYear').val()+"/" +$('#exchange').val()+"/'"+$('input[name=rpttype]:checked').val()+"'"
    ajaxget(path, 'parsedataPlacementStatus', 'comment', "control");
}
function parsedataPlacementStatus(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    var total="";
    var placement=0;
    var appointment=0;
    var ssal1=0;
    var sal2=0;
    var sal3=0;
    var undertaking=0;
    for (var i = 0; i < data1.length; i++) {

        
        placement+=parseInt(data1[i].Placement)
         appointment+=parseInt(data1[i].Appointment);
         ssal1+=parseInt(data1[i].Salslip_fir);
         sal2+=parseInt(data1[i].Salslip_sec);
         sal3+=parseInt(data1[i].Salslip_thi);
         undertaking+=parseInt(data1[i].Udertaking);
       if($('input[name=rpttype]:checked').val()=='Detail'){
        appenddata += "<tr><td style='    word-break: break-word;'>" + [i+1]+ "</td><td style='    word-break: break-word;'>" + data1[i].Emp_Name  + "</td><td>"+data1[i].Ex_Name+"</td><td>"+data1[i].Month_Name+"</td><td>"+data1[i].Placement+"</td><td>"+data1[i].Appointment+"</td><td>"+data1[i].Salslip_fir+"</td><td>"+data1[i].Salslip_sec+"</td><td>"+data1[i].Salslip_thi+"</td><td>"+data1[i].Udertaking+"</td></tr>";
        total= `<tr><td colspan=4 style='font-weight:600;text-align: center;'>Total</td><td style='font-weight:600;'>${placement}</td><td style='font-weight:600;'>${appointment}</td><td style='font-weight:600;'>${ssal1}</td><td style='font-weight:600;'>${sal2}</td><td style='font-weight:600;'>${sal3}</td><td style='font-weight:600;'>${undertaking}</td>`
        var tablehead = "<th style='border-bottom:inset;border-right:inset;' style='border-bottom:inset;border-right:inset;'>S.No.</th><th style='border-bottom:inset;border-right:inset;'>Center Manager</th><th style='border-bottom:inset;border-right:inset;'>Exchange Name</th><th style='border-bottom:inset;border-right:inset;'>Month</th><th style='border-bottom:inset;border-right:inset;'>Placement </th><th style='border-bottom:inset;border-right:inset;'>Appointment Letter Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 1 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 2 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 3 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Undertaking Recieved </th>";
				
       }
       else{
        appenddata += "<tr><td style='    word-break: break-word;'>" + [i+1]+ "</td><td>"+data1[i].Month_Name+"</td><td>"+data1[i].Placement+"</td><td>"+data1[i].Appointment+"</td><td>"+data1[i].Salslip_fir+"</td><td>"+data1[i].Salslip_sec+"</td><td>"+data1[i].Salslip_thi+"</td><td>"+data1[i].Udertaking+"</td></tr>";
        total= `<tr><td colspan=2 style='font-weight:600;text-align: center;'>Total</td><td style='font-weight:600;'>${placement}</td><td style='font-weight:600;'>${appointment}</td><td style='font-weight:600;'>${ssal1}</td><td style='font-weight:600;'>${sal2}</td><td style='font-weight:600;'>${sal3}</td><td style='font-weight:600;'>${undertaking}</td>`
        var tablehead = "<th style='border-bottom:inset;border-right:inset;' style='border-bottom:inset;border-right:inset;'>S.No.</th><th style='border-bottom:inset;border-right:inset;'>Month</th><th style='border-bottom:inset;border-right:inset;'>Placement </th><th style='border-bottom:inset;border-right:inset;'>Appointment Letter Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 1 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 2 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 3 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Undertaking Recieved </th>";
       
       }
    }
    $("#tablehead").html(tablehead);
     jQuery("#tbodyvalue").html(appenddata+total);


    // var tablehead="<th style='border-bottom:inset;border-right:inset;' style='border-bottom:inset;border-right:inset;'>S.No.</th><th style='border-bottom:inset;border-right:inset;'>Employer Name </th><th style='border-bottom:inset;border-right:inset;'>Exchange Name </th><th style='border-bottom:inset;border-right:inset;'>Placement </th><th style='border-bottom:inset;border-right:inset;'>Appointment Letter Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 1 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 2 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Salary Slip 3 Recieved </th><th style='border-bottom:inset;border-right:inset;'>Undertaking Recieved </th>";
    // $("#tablehead").html(tablehead);

}

function checkValidation(){
    if($(`#month`).val()==''){
    
    toastr.warning('Please Select Month')
    return false;
    }
    if($(`#textContactYear`).val()=='0'){
    
        toastr.warning('Please Select Year')
        return false;
        }
    
    else{
        FillPlacementStatus()
    }
}
                 

function FillMonthplacement() {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,'parsedatasecuredFillMonthself','comment','control');
  }
  
  function parsedatasecuredFillMonthself(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillMonthplacement();
    }
    else {
        var data1 = data[0];
        $('#month').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'MonthId',
            labelField: 'FullMonthName',
            searchField: 'FullMonthName',
            options: data1,
            create: true
        });
    }
}

function FillDDlExchOfficename(ReportID,control){

    var path = serverpath + "exchangename"
    securedajaxget(path, 'parsedatasecuredFillDDlExchOfficename', 'comment', control)
    }
    function parsedatasecuredFillDDlExchOfficename(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillDDlExchOfficename();
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    var data1 = data[0];
    var appenddata="";
    jQuery("#exchange").append(jQuery("<option></option>").val('0').html('All'));
    
    for (var i = 0; i < data1.length; i++) {
    jQuery("#exchange").append(jQuery("<option></option>").val(data1[i].Ex_Id).html(data1[i].Ex_Name));
    }
    }
    
    }
    function handleExchChange(){
        if($('#exchange').val()=='0'){
$('.rpttype').show();
        }
        else{
            $('.rpttype').hide();
            $('input[name=rpttype]').filter('[value=Detail]').prop('checked', true);
        }
    }
