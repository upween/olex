
function FillPreferredLocationMaster() {
    var path = serverpath +"preflocation/0/0" 
    ajaxget(path, 'parsedataPreferredLocationMaster', 'comment', "control");
}
function parsedataPreferredLocationMaster(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].Active_YN == '0') {
        var	active = "No"
        }
         if (data1[i].Active_YN == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Location_id+ "</td><td style='    word-break: break-word;'>" + data1[i].Location_Name  + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode("+data1[i].Location_id+",'"+encodeURI(data1[i].Location_Name)+"','"+encodeURI(data1[i].Active_YN )+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);


}
function InsUpdPreferredLocationMaster(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Location_id":localStorage.Location_id,
    "p_Location_Name":jQuery("#LocationName").val(),
    "p_Active_YN":chkval
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "preflocation";
ajaxpost(path, 'parsrdataparsedataPreferredLocationMaster', 'comment', MasterData, 'control')
}

function parsrdataparsedataPreferredLocationMaster(data) {
    data = JSON.parse(data)
    
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdPreferredLocationMaster();
	}
	else if (data[0][0].ReturnValue == "1") {
        FillPreferredLocationMaster();
        resetMode()
//FillSectorType() 
      //  resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FillPreferredLocationMaster() 
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
       resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function CheckValidationbloodgroup(){
  
    
    if(jQuery("#LocationName").val()==""){
          getvalidated('LocationName','text','LocationName');
          return false;
      }
      else{
        InsUpdbloodgroupmaster()
            
      }
  }
  function EditMode(Location_id,Location_Name,Active_YN) {
    jQuery("#LocationName").val(decodeURI(Location_Name))
    
    localStorage.Location_id=Location_id
    if (decodeURI(Active_YN) == '0') {
        $("#ActiveStatus")[0].checked = false;
    }
    else {
        $("#ActiveStatus")[0].checked = true;
    }
}
function resetMode() {
   
    localStorage.Location_id= "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#LocationName").val("")
}