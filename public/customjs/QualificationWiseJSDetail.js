function FillExamPassed(funct,control) {
    var path =  serverpath + "secured/qualification/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExamPassed(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExamPassed('parsedatasecuredFillExamPassed','ExamPassed');
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
         
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}

jQuery('#ExamPassed').on('change', function () {
  //  FillCourse(jQuery('#ExamPassed').val());
  FillSubjectGroup('parsedatasecuredFillSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
       
       
});


function FillSubjectGroup(funct,control,EducationId) {
    var path =  serverpath + "secured/subjectgroup/0/'" + EducationId + "'/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSubjectGroup(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSubjectGroup('parsedatasecuredFillSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Subject Group"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
             }
        }
    }
function fetchQualificationWiseJSDetail(){
    
      var path =  serverpath +"QualififcationWiseDetail/'"+$('#ExamPassed').val()+"'/'"+$('#SubjectGroup').val()+"'/"+sessionStorage.Ex_id+""
      ajaxget(path,'parsedatafetchEmp','comment',"control");
  }
  function parsedatafetchEmp(data){  
      data = JSON.parse(data)
     var appenddata="";
      var data1 = data[0];  
      var cacheArr = [];
  
              for (var i = 0; i < data1.length; i++) {
          
                if (cacheArr.indexOf(data1[i].RegistrationId) >= 0) {
                    continue;
                }

                cacheArr.push(data1[i].RegistrationId);

                appenddata += "<tr class='i'><td style='    word-break: break-word;'><a href='#'>" +data1[i].RegistrationId+ "</a></td><td style='    word-break: break-word;'>" + data1[i].RegDate+ "</td><td style='    word-break: break-word;'>" + data1[i].CandidateName+ "</td><td>"+data1[i].GuardianFatherName+"</td><td>"+data1[i].DateOfBirth+"</td><td>"+data1[i].Gender+"</td><td><button type='button' class='btn btn-success' onclick=ViewProfile('"+data1[i].CandidateId+"')  style='background-color:#716aca;border-color:#716aca;'>View Profile</button></td></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
            $('#tbodyvalue').clientSidePagination();

        
                
  }
  function ViewProfile(CandidateId){
    sessionStorage.JSCandidateId=CandidateId;
    window.location='/CVTemplate'
  }
  
