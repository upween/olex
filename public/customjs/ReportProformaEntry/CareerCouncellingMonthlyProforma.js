

function FillCCMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCCMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCCMonth('parsedatasecuredFillCCMonth',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}
function FetchCCReportBymonth(monthid,year) {
    var path = serverpath + "CounsellingByMonth/"+monthid+"/"+year+"/5"
    ajaxget(path, 'parsedataFetchCCReportBymonth', 'comment', "control");
}
function parsedataFetchCCReportBymonth(data) {
    data = JSON.parse(data)
   var data1=data[0];
   var appenddata="";
    for (var i = 0; i < data1.length; i++) {
       
      appenddata += "<tr class='addrow'><td >1</td><td style='    word-break: break-word;'>" +data1[i].Total_Counseling+ "</a></td><td style='    word-break: break-word;'>" +data1[i].Total_CandidateS+ "</td><td>"+data1[i].UR_Candidates+"</td><td style='    word-break: break-word;'>" + data1[i].SC_Candidates+ "</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td><td>"+data1[i].Minority_Candidate+"</td><td>"+data1[i].Remark+"</td></tr>";
       }jQuery("#tbodyvalue").html(appenddata);  
    

   
    }



function addNewProductRow() {
    var x = $('.addrow').length;
    var row = `<tr class='addrow insert' id="addrow_`+x+`">
			<td> `+[x+1]+` </td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Total_Counseling" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Total_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols UR_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols SC_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols ST_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols OBC_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Female_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols PH_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Minority_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Remark" ></td>
			<td><button type="button" onclick="$('#addrow_`+x+`').remove();"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
			Remove</button></td>
			</tr>`;
    $('#tbodyvalue').append(row);
}




    function CareerCounselingMonthlyRpt(){

        $("#tableglobal tr.addrow").each(function (key) {
            var this_row = $(this);
            var rowid=this.id.split('_');
            if(rowid[0]=='modify'){
var flag=3;
            }
            else{
var flag=2
            }
            if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                var ex_id =  $("#ddExchange").val();
                    }
                else{
                  var ex_id =  sessionStorage.Ex_id;
                }

        var MasterData = {  
            "p_Flag":flag,
            "p_Xmonth":jQuery("#ddlMonth").val(),
            "p_Xyear":jQuery("#ddlYear").val(),
            "p_Ex_Id":ex_id,
            "p_Tran_Id":'0',
            "p_Total_Counseling":$.trim(this_row.find('td> input.Total_Counseling').val())==""?0:$.trim(this_row.find('td> input.Total_Counseling').val()),
            "p_Total_Candidates":$.trim(this_row.find('td> input.Total_Candidates').val())==""?0:$.trim(this_row.find('td> input.Total_Candidates').val()),
            "p_Female_Candidates":$.trim(this_row.find('td> input.Female_Candidates').val())==""?0:$.trim(this_row.find('td> input.Female_Candidates').val()),
            "p_SC_Candidates":$.trim(this_row.find('td> input.SC_Candidates').val())==""?0:$.trim(this_row.find('td> input.SC_Candidates').val()),
            "p_ST_Candidates":$.trim(this_row.find('td> input.ST_Candidates').val())==""?0:$.trim(this_row.find('td> input.ST_Candidates').val()),
            "p_OBC_Candidates":$.trim(this_row.find('td> input.OBC_Candidates').val())==""?0:$.trim(this_row.find('td> input.OBC_Candidates').val()),
            "p_PH_Candidates":$.trim(this_row.find('td> input.PH_Candidates').val())==""?0:$.trim(this_row.find('td> input.PH_Candidates').val()),
            "p_Minority_Candidates":$.trim(this_row.find('td> input.Minority_Candidates').val())==""?0:$.trim(this_row.find('td> input.Minority_Candidates').val()),
            "p_Remark":$.trim(this_row.find('td> input.Remark').val())==""?0:$.trim(this_row.find('td> input.Remark').val()),
            "p_UR_Candidates":$.trim(this_row.find('td> input.UR_Candidates').val())==""?0:$.trim(this_row.find('td> input.UR_Candidates').val()),
            "p_Client_IP":sessionStorage.ipAddress,
            "p_LM_BY":'0',
            "p_Status":'0'
        };
      
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "CareerCounsel_Pro_Reporting";
        ajaxpost(path, 'parsedataCareerCounselingMonthlyRpt', 'comment', MasterData, 'control')
        });
    }
function parsedataCareerCounselingMonthlyRpt(data){  
    data = JSON.parse(data)
    reset();

}
     



function reset(){
    $('.visiblecontrols').val("");
    $('#tbodyvalue').empty();
}


function fetchCareerCounselingMonthlyRpt(flag){
    sessionStorage.setItem('Flag',flag)
    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
        var ex_id =  $("#ddExchange").val();
            }
        else{
          var ex_id =  sessionStorage.Ex_id;
        }

    var MasterData = {  
        "p_Flag":flag,
        "p_Xmonth":jQuery("#ddlMonth").val(),
        "p_Xyear":jQuery("#ddlYear").val(),
        "p_Ex_Id":ex_id,
        "p_Tran_Id":'0',
        "p_Total_Counseling":'0',
        "p_Total_Candidates":'0',
        "p_Female_Candidates":'0',
        "p_SC_Candidates":'0',
        "p_ST_Candidates":'0',
        "p_OBC_Candidates":'0',
        "p_PH_Candidates":'0',
        "p_Minority_Candidates":'0',
        "p_Remark":'0',
        "p_UR_Candidates":'0',
        "p_Client_IP":'0',
        "p_LM_BY":'0',
        "p_Status":'0'
    };
  
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "CareerCounsel_Pro_Reporting";
    ajaxpost(path, 'parsedatafetchCareerCounselingMonthlyRpt', 'comment', MasterData, 'control')
    }
function parsedatafetchCareerCounselingMonthlyRpt(data){  
data = JSON.parse(data)
var appenddata="";
var data1 = data[0]; 

 if(sessionStorage.Flag=='4'){
    toastr.success('Successful Send To Head Office');
    $('.visiblecontrols').prop('disabled',true)
    $("#btnsendtoho,#btnsave").hide();
}



    else if(sessionStorage.Flag=='1'){
        if(data1.length>0){



for (var i = 0; i < data1.length; i++) {
 
    appenddata += `<tr class='addrow insert' id="modify_${i}">
    <td> `+[i+1]+` </td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols Total_Counseling" value='`+data1[i].Total_Counseling+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols Total_Candidates" value='`+ data1[i].Total_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols UR_Candidates" value='`+ data1[i].UR_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols SC_Candidates" value='`+data1[i].SC_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols ST_Candidates" value='`+data1[i].ST_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols OBC_Candidates" value='`+data1[i].OBC_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols Female_Candidates" value='`+data1[i].Female_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols PH_Candidates" value='`+data1[i].PH_Candidates+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols Minority_Candidates" value='`+data1[i].Minority_Candidate+`' ></td>
    <td><input style="width: 70px;" type="number" class="visiblecontrols Remark" value='`+data1[i].Remark+`' ></td>
    <td><button type="button" onclick="DeleteCareerCounselingMonthlyRpt(`+data1[i].Tran_Id+`)"   class="btn btn-success visiblecontrols" style="background-color:#716aca;border-color:#716aca;">
	Delete</button></td>
    </tr>`
}
jQuery("#tbodyvalue").html(appenddata);  

if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    $('.visiblecontrols').prop('disabled','disabled')
    $("#btnsendtoho,#btnsave").hide();
}

if(data1[0].Verify_YN=='1'){
    $('.visiblecontrols').prop('disabled',true)
    $("#btnsendtoho,#btnsave").hide(); 
}
else if(data1[0].Verify_YN=='0' || data1[0].Verify_YN==null){
    if(sessionStorage.User_Type_Id=='2'){
        $("#btnsendtoho,#btnsave").show();
        $('.visiblecontrols').prop('disabled',false)
    } 
    else  if(sessionStorage.User_Type_Id=='7'){
        $("#btnsave").show();
        $('#btnsendtoho').hide();
        $('.visiblecontrols').prop('disabled',false)
    } 
}

}
else if(data1.length<0){
    $('#tbodyvalue').empty();

    if(sessionStorage.User_Type_Id=='7'){
        $("#btnsave").show();
        $('#btnsendtoho').hide();
        $('.visiblecontrols').prop('disabled',false)
    } 
    else if(sessionStorage.User_Type_Id=='2'){
        $("#btnsave").show();
        $('#btnsendtoho').show();
        $('.visiblecontrols').prop('disabled',false)
    } 
    else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
        $("#btnsave,#btnsendtoho").hide();
        $('.visiblecontrols').prop('disabled',true)
    } 
}
}

}

 
function DeleteCareerCounselingMonthlyRpt(TranId){
    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
        var ex_id =  $("#ddExchange").val();
            }
        else{
          var ex_id =  sessionStorage.Ex_id;
        }

    var MasterData = {  
        "p_Flag":'11',
        "p_Xmonth":jQuery("#ddlMonth").val(),
        "p_Xyear":jQuery("#ddlYear").val(),
        "p_Ex_Id":ex_id,
        "p_Tran_Id":TranId,
        "p_Total_Counseling":'0',
        "p_Total_Candidates":'0',
        "p_Female_Candidates":'0',
        "p_SC_Candidates":'0',
        "p_ST_Candidates":'0',
        "p_OBC_Candidates":'0',
        "p_PH_Candidates":'0',
        "p_Minority_Candidates":'0',
        "p_Remark":'0',
        "p_UR_Candidates":'0',
        "p_Client_IP":'0',
        "p_LM_BY":'0',
        "p_Status":'0'
    };
  
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "CareerCounsel_Pro_Reporting";
    ajaxpost(path, 'parsedataDeleteCareerCounselingMonthlyRpt', 'comment', MasterData, 'control')
    }
function parsedataDeleteCareerCounselingMonthlyRpt(data){  
data = JSON.parse(data)
toastr.success('Delete Successful');
fetchCareerCounselingMonthlyRpt('1')

}
