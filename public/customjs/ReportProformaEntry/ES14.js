function ES14Entry(flag){
    sessionStorage.setItem('type',flag)
    if ($("#years").val() == "0") {
        toastr.warning("Please Select Year", "", "info")
        return true;
    }
    else{
        var todate = ''+$("#years").val()+'-12-31'


        $("#tbodyvalue tr.i").each(function (key) {
         var this_row = $(this);
         if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            var ex_id =  $("#ddExchange").val();
                }
            else{
              var ex_id =  sessionStorage.Ex_id;
            }
        var MasterData = {
  
            "p_Ex_Id": ex_id,  
            "p_ToDt":  todate ,  
            "p_Edu_id":   $.trim(this_row.find('td> .eduid').text())==''?0:$.trim(this_row.find('td> .eduid').text()),  
            "p_Upto19":   $.trim(this_row.find('td> input.upto19').val())==''?0:$.trim(this_row.find('td> input.upto19').val()),  
            "p_Age_20_29":  $.trim(this_row.find('td> input.Age_20_29').val())==''?0:$.trim(this_row.find('td> input.Age_20_29').val()),  
            "p_Age_30_39":   $.trim(this_row.find('td> input.Age_30_39').val())==''?0:$.trim(this_row.find('td> input.Age_30_39').val()),  
            "p_Age_40_49":  $.trim(this_row.find('td> input.Age_40_49').val())==''?0:$.trim(this_row.find('td> input.Age_40_49').val()) ,  
            "p_Age_50_59":  $.trim(this_row.find('td> input.Age_50_59').val())==''?0:$.trim(this_row.find('td> input.Age_50_59').val()) ,  
            "p_Age_60_above":   $.trim(this_row.find('td> input.Age_60_above').val())==''?0:$.trim(this_row.find('td> input.Age_60_above').val()),
            "p_Flag":   flag, 
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "ES14Entry";
        securedajaxpost(path,'parsrdataES14Entry','comment',MasterData,'control')
    })
    }
    }

function parsrdataES14Entry(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES14Entry();
    }

    if(sessionStorage.type=='1'){
    toastr.success("Insert Successful", "", "success")
        return true;
  }
  else  if(sessionStorage.type=='2'){
    toastr.success("Update Successful", "", "success")
    return true;
}
else  if(sessionStorage.type=='4'){
    toastr.success("Sent to HO  Successful", "", "success")
    $('#btnModify').hide();
    $('#btnSendtoho').hide();
    $( ".disable" ).prop( "disabled", true );
    return true;
}

else  if(sessionStorage.type=='3'){
    if(data[0].length>0 &&sessionStorage.User_Type_Id=='7'){
        $('#btnSaverpt').show();
        $('#btnModify').hide();
        $('#btnSendtoho').hide();
    }
    else {
        $('#btnSaverpt').show();
        $('#btnSendtoho').hide();
    }
  
    
    
    if(data[0].length>0 &&sessionStorage.User_Type_Id=='2'){
        $('#btnSendtoho, #btnModify').show();
        $('#btnSaverpt').hide();
    }

    // if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    //     $('#btnSendtoho, #btnModify,#btnSaverpt').hide();
    //     $( ".disable" ).prop( "disabled", true );
    // }

  
    for (var i = 0; i < data[0].length; i++) {
        if(data[0][i].Verify_YN=='1'){
            $( ".disable" ).prop( "disabled", true );
            $('#btnSendtoho, #btnModify').hide();
            $('#btnSaverpt').hide();
           }
           else{
            if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                $('#btnSendtoho, #btnModify,#btnSaverpt').hide();
                $( ".disable" ).prop( "disabled", true );
            }else{
            $( ".disable" ).prop( "disabled", false );
            }
           }
$('#upto19'+data[0][i].Edu_id+'').val(data[0][i].Upto19);
$('#Age_20_29'+data[0][i].Edu_id+'').val(data[0][i].Age_20_29);
$('#Age_30_39'+data[0][i].Edu_id+'').val(data[0][i].Age_30_39);
$('#Age_40_49'+data[0][i].Edu_id+'').val(data[0][i].Age_40_49);
$('#Age_50_59'+data[0][i].Edu_id+'').val(data[0][i].Age_50_59);
$('#Age_60_above'+data[0][i].Edu_id+'').val(data[0][i].Age_60_above);
$('#total'+data[0][i].Edu_id+'').val(data[0][i].total);
    }
}

}






function FillEduationalLevel(funct,control) {
    var path =  serverpath + "Edu_with_Gender"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedataFillEduationalLevel(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEduationalLevel('parsedataFillEduationalLevel','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            var appenddata = ''
       

            for (var i = 0; i < data1.length; i++) {
                if(data1[i].Gender=='M'){
                    var gender= 'Male'
                    var EduName= data1[i].Edu_Name
                   }
                    else if(data1[i].Gender=='F'){
                   var gender='Female'
                   var EduName=''
                      }
                      else if(data1[i].Gender=='B'){
                          var gender='Both'
                          var EduName=''
                             }

              appenddata+= `<tr class='i'>
              <td><label class='eduid'>`+data1[i].Edu_id+`</label></td>
              <td>`+EduName+`</td>
          
              <td > `+gender+`
              </td>
              <td>  <input type="txt"  onfocusout=getTotalColumn(${data1[i].Edu_id}) id='upto19`+data1[i].Edu_id+`' class='upto19 disable' style="    width: 50px;"  placeholder="0"  ></input>
              </td>
              <td >  <input type="txt"  id='Age_20_29`+data1[i].Edu_id+`' onfocusout=getTotalColumn(${data1[i].Edu_id}) class='Age_20_29 disable'  style="    width: 50px;"  placeholder="0"  ></input>
              </td>
              <td>  <input type="txt"  id='Age_30_39`+data1[i].Edu_id+`' onfocusout=getTotalColumn(${data1[i].Edu_id}) class='Age_30_39 disable' style="    width: 50px;"  placeholder="0"  ></input>
              </td>
              <td >  <input type="txt"  id='Age_40_49`+data1[i].Edu_id+`' onfocusout=getTotalColumn(${data1[i].Edu_id}) class='Age_40_49 disable' style="    width: 50px;" placeholder="0"   ></input>
              </td>
              <td>  <input type="txt"  id='Age_50_59`+data1[i].Edu_id+`' onfocusout=getTotalColumn(${data1[i].Edu_id}) class='Age_50_59 disable' style="    width: 50px;"  placeholder="0"  ></input>
              </td>
              <td >  <input type="txt"  id='Age_60_above`+data1[i].Edu_id+`' onfocusout=getTotalColumn(${data1[i].Edu_id}) class='Age_60_above disable' style="    width: 50px;" placeholder="0"   ></input>
              </td>
              <td >  <input type="txt" id="total`+data1[i].Edu_id+`" class='total disable' style="    width: 50px;" placeholder="0"   ></input>
              </td>
          </tr>
              
              </tr>`
             }
             $("#tbodyvalue").append(appenddata)
        }
              
  }
  function getTotalColumn(id){
    var total=0;
    var upto19=$(`#upto19${id}`).val()==''?0:$(`#upto19${id}`).val();
    var Age_20_29=$(`#Age_20_29${id}`).val()==''?0:$(`#Age_20_29${id}`).val();
    var Age_30_39=$(`#Age_30_39${id}`).val()==''?0:$(`#Age_30_39${id}`).val();
    var Age_40_49=$(`#Age_40_49${id}`).val()==''?0:$(`#Age_40_49${id}`).val();
    var Age_50_59=$(`#Age_50_59${id}`).val()==''?0:$(`#Age_50_59${id}`).val();
    var Age_60_above=$(`#Age_60_above${id}`).val()==''?0:$(`#Age_60_above${id}`).val();
    total=parseInt(upto19)+parseInt(Age_20_29)+parseInt(Age_30_39)+parseInt(Age_40_49)+parseInt(Age_50_59)+parseInt(Age_60_above);
    $(`#total${id}`).val(total);

  }