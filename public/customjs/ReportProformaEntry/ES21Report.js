function ES21Entry(flag){
    sessionStorage.setItem('type',flag)
    if ($("#ChooseReport").val() == "0") {
        toastr.warning("Please Select Criteria", "", "info")
        return true;
    }
   else if ($("#years").val() == "0") {
        toastr.warning("Please Select Year", "", "info")
        return true;
    }
    else{
        if($("#ChooseReport").val()=='1'){
          //  var fromdate = ''+$("#years").val()+'/01/01'
            var todate = ''+$("#years").val()+'-06-30'
            }
            else if($("#ChooseReport").val()=='2'){
         //    var fromdate = ''+$("#years").val()+'/07/01/'
                var todate = ''+$("#years").val()+'-12-31'
            }        
            $("#tbodyvalue tr.i").each(function (key) {
                var this_row = $(this);
                if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                    var ex_id =  $("#ddExchange").val();
                        }
                    else{
                      var ex_id =  sessionStorage.Ex_id;
                    }
        var MasterData = {
  
            "p_Ex_id": ex_id,  
            "p_ToDt":  todate ,  
            "p_Edu21_id":   $.trim(this_row.find('td> .Edu21_id').text())==''?0:$.trim(this_row.find('td> .Edu21_id').text()), 
            "p_Total_Regd":   $.trim(this_row.find('td> input.Total_Regd').val())==''?0: $.trim(this_row.find('td> input.Total_Regd').val()),  
            "p_Total_Placement":  $.trim(this_row.find('td> input.Total_Placement').val())==''?0:$.trim(this_row.find('td> input.Total_Placement').val()),  
            "p_Total_LR":   $.trim(this_row.find('td> input.Total_LR').val())==''?0:$.trim(this_row.find('td> input.Total_LR').val()),
            "p_W_Regd":  $.trim(this_row.find('td> input.p_W_Regd').val())==''?0:$.trim(this_row.find('td> input.p_W_Regd').val()),  
            "p_W_Placement":  $.trim(this_row.find('td>input .W_Placement').val())==''?0:$.trim(this_row.find('td>input .W_Placement').val()),  
            "p_W_LR":   $.trim(this_row.find('td>input .W_LR').val())==''?0:$.trim(this_row.find('td>input .W_LR').val()),
            "p_SC_Regd":   $.trim(this_row.find('td>input .SC_Regd').val())==''?0:$.trim(this_row.find('td>input .SC_Regd').val()),
            "p_SC_Placement":   $.trim(this_row.find('td> input.SC_Placement').val())==''?0:$.trim(this_row.find('td> input.SC_Placement').val()),
            "p_SC_LR":   $.trim(this_row.find('td> input.SC_LR').val())==''?0:$.trim(this_row.find('td> input.SC_LR').val()),
            "p_ST_Regd":   $.trim(this_row.find('td> input.ST_Regd').val())==''?0:$.trim(this_row.find('td> input.ST_Regd').val()),
            "p_ST_Placement":   $.trim(this_row.find('td> input.ST_Placement').val())==''?0:$.trim(this_row.find('td> input.ST_Placement').val()),
            "p_ST_LR":   $.trim(this_row.find('td> input.ST_LR').val())==''?0:$.trim(this_row.find('td> input.ST_LR').val()),
            "p_OBC_Regd":   $.trim(this_row.find('td> input.OBC_Regd').val())==''?0:$.trim(this_row.find('td> input.OBC_Regd').val()),
            "p_OBC_Placement":   $.trim(this_row.find('td> input.OBC_Placement').val())==''?0:$.trim(this_row.find('td> input.OBC_Placement').val()),
            "p_OBC_LR":   $.trim(this_row.find('td> input.OBC_LR').val())==''?0:$.trim(this_row.find('td> input.OBC_LR').val()),
            "p_Flag":   flag, 
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "ES21Entry";
        securedajaxpost(path,'parsrdataES21Entry','comment',MasterData,'control')
    })
    }
}


function parsrdataES21Entry(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES21Entry(sessionStorage.type);
    }

    if(sessionStorage.type=='1'){
       // toastr.success("Insert Successful", "", "success")
            return true;
      }
      else  if(sessionStorage.type=='2'){
        toastr.success("Update Successful", "", "success")
        return true;
    }
    else  if(sessionStorage.type=='4'){
        toastr.success("Send To HO Successful", "", "success")
        $('#btnModify,#btnVerify').hide();
        $( ".disable" ).prop( "disabled", true );
        return true;
    }
    else  if(sessionStorage.type=='3'){
        if(sessionStorage.User_Type_Id=='7'){
            
            if(data[0][0].Flag=='Add'){
                $('#btnSave').show();
                $('#btnModify').hide();
            }
            else if(data[0][0].Flag=='Mod'){
                $('#btnModify').show();
                $('#btnSave').hide(); 
                }
        }
        else  if(sessionStorage.User_Type_Id=='2'){
            
            if(data[0][0].Flag=='Add'){
                $('#btnSave').hide();
            }
            else if(data[0][0].Flag=='Mod'){
                $('#btnSave').hide();   
                }
        }

        // if(data[0][0].Flag=='Add'){
        //     $('#btnSave').show();
        //     $('#btnModify').hide();
        // }
        // else if(data[0][0].Flag=='Mod'){
        //     $('#btnModify').show();
        //     $('#btnSave').hide(); 
        //     $('#btnVerify').hide();   
        //     }
        
      
    
        for (var i = 0; i < data[0].length; i++) {
          
    $('#Total_Regd'+data[0][i].Edu21_id+'').val(data[0][i].Total_Regd);
    $('#Total_Placement'+data[0][i].Edu21_id+'').val(data[0][i].Total_Placement);
    $('#Total_LR'+data[0][i].Edu21_id+'').val(data[0][i].Total_LR);

    $('#W_Regd'+data[0][i].Edu21_id+'').val(data[0][i].W_Regd);
    $('#W_Placement'+data[0][i].Edu21_id+'').val(data[0][i].W_Placement);
    $('#W_LR'+data[0][i].Edu21_id+'').val(data[0][i].W_LR);

    $('#SC_Regd'+data[0][i].Edu21_id+'').val(data[0][i].SC_Regd);
    $('#SC_Placement'+data[0][i].Edu21_id+'').val(data[0][i].SC_Placement);
    $('#SC_LR'+data[0][i].Edu21_id+'').val(data[0][i].SC_LR);

    $('#ST_Regd'+data[0][i].Edu21_id+'').val(data[0][i].ST_Regd);
    $('#ST_Placement'+data[0][i].Edu21_id+'').val(data[0][i].ST_Placement);
    $('#ST_LR'+data[0][i].Edu21_id+'').val(data[0][i].ST_LR);

    $('#OBC_Regd'+data[0][i].Edu21_id+'').val(data[0][i].OBC_Regd);
    $('#OBC_Placement'+data[0][i].Edu21_id+'').val(data[0][i].OBC_Placement);
    $('#OBC_LR'+data[0][i].Edu21_id+'').val(data[0][i].OBC_LR);


    
    if(data[0][0].Verify_YN=='1'){
        $( ".disable" ).prop( "disabled", true );
        $('#btnVerify,#btnModify').hide();
    }
    else if (data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
        if(sessionStorage.User_Type_Id=='2'){
    $( ".disable" ).prop( "disabled", false );
    $('#btnVerify,#btnModify').show();
        }
        else if(sessionStorage.User_Type_Id=='7'){
            $( ".disable" ).prop( "disabled", false );
            $('#btnModify').show();
            $("#btnVerify").hide();
        }
    }
    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
        $( ".disable" ).prop( "disabled", true );
        $('#btnVerify,#btnModify,#btnSave').hide();
    }
}
    }
    
}






function FillES21_Master(funct,control) {
    var path =  serverpath + "ES21_Master"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedataFillES21_Master(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillES21_Master('parsedataFillES21_Master','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            var appenddata = ''
       

            for (var i = 0; i < data1.length; i++) {
               
              appenddata+= `<tr class='i'>
              <td><label class='Edu21_id'>`+data1[i].Edu21_id+`</label></td>
              <td colspan="3">`+data1[i].Edu_Name+`</td>

              <td>  <input type="txt" class='disable Total_Regd' id="Total_Regd`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable Total_Placement' id="Total_Placement`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable Total_LR' id="Total_LR`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable W_Regd' id="W_Regd`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable W_Placement' id="W_Placement`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable W_LR' id="W_LR`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable SC_Regd' id="SC_Regd`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>	
              <td >  <input type="txt" class='disable SC_Placement' id="SC_Placement`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable SC_LR' id="SC_LR`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable ST_Regd' id="ST_Regd`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable ST_Placement' id="ST_Placement`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable ST_LR' id="ST_LR`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable OBC_Regd' id="OBC_Regd`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td >  <input type="txt" class='disable OBC_Placement' id="OBC_Placement`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
              <td>  <input type="txt" class='disable OBC_LR' id="OBC_LR`+data1[i].Edu21_id+`" style="    width: 50px;" disable></input>
              </td>
          
          </tr>`
             }
             $("#tbodyvalue").append(appenddata)
        }
              
  }
  