function fetchES22(flag){
    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
		var ex_id =  $("#ddExchange").val();
			}
		else{
		  var ex_id =  sessionStorage.Ex_id;
		}
    sessionStorage.ES22Flag=flag;
    var todt=`${$('#yeares22rpt').val()}-12-31`;
	var path =  serverpath + "getES22Entry/"+ex_id+"/"+todt+"/"+flag+"";
		ajaxget(path,'parsedatafetchES22','comment',"control"); 
}
  function parsedatafetchES22(data){  
      data = JSON.parse(data);
      var appendata='';
      var data1 = data[0];
    if(sessionStorage.User_Type_Id=='2'){
        $('#btnVerify').show();
        
        $('#savebtn').show();
    }
    else if(sessionStorage.User_Type_Id=='7'){
        $('#savebtn').show();
    }
    if(sessionStorage.ES22Flag=='2'){
        toastr.success('Verified Successfully');
    }
    else{
        for (var i = 0; i < data1.length; i++) {
            appendata+=`	<tr class="tbl_row" id="${data1[i].Flag}_${i}">
            <td class="ES24_II_Id">${i+1}</td>
            <td class="NCO">${data1[i].NCO_Code}</td>
    
            <td>${data1[i].Subject}
            </td>
            <td ><input type="text"     class="TraineeReg" value='${data1[i].TraineeReg}' style="    width: 40px;" >
            </td>
            <td><input type="text"     class="TraineePlacement" value='${data1[i].TraineePlacement}' style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="TraineeLR" value='${data1[i].TraineeLR}'   style="    width: 40px;" > 
            </td>
            <td><input type="text"     class="ApprenticeReg" value='${data1[i].ApprenticeReg}'   style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="ApprenticePlace" value='${data1[i].ApprenticePlace}'   style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="ApprenticeLR" value='${data1[i].ApprenticeLR}'   style="    width: 40px;" > 
            </td>
      
       
    </td>
        </tr>`
          }
          
        
	$('#tbodyvalue').html(appendata);
    }
         
     
     
      

  }
  function reset(){
    $('#tbodyvalue').empty();
    $('#yeares22rpt').val('0')
  }

  function ES22Entry(){
    
		FromDt=`${$('#yeares22rpt').val()}-01-01`;
		ToDt=`${$('#yeares22rpt').val()}-12-31`;
	$("#tableglobal tr.tbl_row").each(function (key) {
		
    var this_row = $(this);
	var rowid=this.id.split('_');
	var type=rowid[0];
	if(type=='Add'){
		
		var path = serverpath + "ES22EntryInsert"; 
	}
	else if(type=='Mod'){
        var path = serverpath + "ES22EntryUpdate"; 
	
	}
        var MasterData = {
        "p_Ex_id":sessionStorage.Ex_id,
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
		"p_NCO_Code":$.trim(this_row.find('td.NCO').text())==""?0:$.trim(this_row.find('td.NCO').text()),        
		"p_TraineeReg":$.trim(this_row.find('td> input.TraineeReg').val())==""?0:$.trim(this_row.find('td> input.TraineeReg').val()),           
		"p_TraineePlacement":$.trim(this_row.find('td> input.TraineePlacement').val())==""?0:$.trim(this_row.find('td> input.TraineePlacement').val()),          
		"p_TraineeLR":$.trim(this_row.find('td> input.TraineeLR').val())==""?0:$.trim(this_row.find('td> input.TraineeLR').val()),          
		"p_ApprenticeReg":$.trim(this_row.find('td> input.ApprenticeReg').val())==""?0:$.trim(this_row.find('td> input.ApprenticeReg').val()),          
		"p_ApprenticePlace":$.trim(this_row.find('td> input.ApprenticePlace').val())==""?0:$.trim(this_row.find('td> input.ApprenticePlace').val()),          
		"p_ApprenticeLR":$.trim(this_row.find('td> input.ApprenticeLR').val())==""?0:$.trim(this_row.find('td> input.ApprenticeLR').val()),
        "p_Verify_YN":'0',         
	

        };
        MasterData = JSON.stringify(MasterData)
		securedajaxpost(path,'parsedataES22Rptentry','comment',MasterData,'control');
	}); 
	reset(); 
  }
  function parsedataES22Rptentry(data){
	data = JSON.parse(data);
    console.log(data);
  }