
function ES23RptEntryForm(Flag){
    var FromDt='';
    var ToDt='';
    var printonTblheadYear='';
    $('#exnametxt').text(sessionStorage.Ex_name);
    sessionStorage.Es23Flag=Flag;
if($('#criteriaddl').val()=='1'){
    FromDt=`${$('#year23').val()}-01-01`;
    ToDt=`${$('#year23').val()}-06-30`;
   printonTblheadYear=`30/06/${$('#year23').val()}`;
}
else if($('#criteriaddl').val()=='2'){
    FromDt=`${$('#year23').val()}-07-01`;
    ToDt=`${$('#year23').val()}-12-31`;
    printonTblheadYear=`${$('#year23').val()}`;
}
$('#tblheadYear').text(printonTblheadYear);
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
  var ex_id =  $("#ddExchange").val();
      }
  else{
    var ex_id =  sessionStorage.Ex_id;
  }
 var MasterData = {
        
"p_Ex_Id":ex_id,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Registration_M": $("#Registration_M").val()==''?0:$("#Registration_M").val(),
"p_PlacesJs_M": $("#PlacesJs_M").val()==''?0:$("#PlacesJs_M").val(),
"p_Registration_Removed_M": $("#Registration_Removed_M").val()==''?0:$("#Registration_Removed_M").val(),
"p_LR_M": $("#LR_M").val()==''?0:$("#LR_M").val(),
"p_Submissions_M": $("#Submissions_M").val()==''?0:$("#Submissions_M").val(),
"p_LR_Prev_M": $("#LR_Prev_M").val()==''?0:$("#LR_Prev_M").val(),

"p_Registration_C": $("#Registration_C").val()==''?0:$("#Registration_C").val(),
"p_PlacesJs_C": $("#PlacesJs_C").val()==''?0:$("#PlacesJs_C").val(),
"p_Registration_Removed_C": $("#Registration_Removed_C").val()==''?0:$("#Registration_Removed_C").val(),
"p_LR_C": $("#LR_C").val()==''?0:$("#LR_C").val(),
"p_Submissions_C": $("#Submissions_C").val()==''?0:$("#Submissions_C").val(),
"p_LR_Prev_C": $("#LR_Prev_C").val()==''?0:$("#LR_Prev_C").val(),

"p_Registration_S": $("#Registration_S").val()==''?0:$("#Registration_S").val(),
"p_PlacesJs_S": $("#PlacesJs_S").val()==''?0:$("#PlacesJs_S").val(),
"p_Registration_Removed_S": $("#Registration_Removed_S").val()==''?0:$("#Registration_Removed_S").val(),
"p_LR_S": $("#LR_S").val()==''?0:$("#LR_S").val(),
"p_Submissions_S": $("#Submissions_S").val()==''?0:$("#Submissions_S").val(),
"p_LR_Prev_S": $("#LR_Prev_S").val()==''?0:$("#LR_Prev_S").val(),

"p_Registration_B": $("#Registration_B").val()==''?0:$("#Registration_B").val(),
"p_PlacesJs_B": $("#PlacesJs_B").val()==''?0:$("#PlacesJs_B").val(),
"p_Registration_Removed_B": $("#Registration_Removed_B ").val()==''?0:$("#Registration_Removed_B").val(),
"p_LR_B": $("#LR_B").val()==''?0:$("#LR_B").val(),
"p_Submissions_B": $("#Submissions_B").val()==''?0:$("#Submissions_B").val(),
"p_LR_Prev_B": $("#LR_Prev_B").val()==''?0:$("#LR_Prev_B").val(),

"p_Registration_Z": $("#Registration_Z").val()==''?0:$("#Registration_Z").val(),
"p_PlacesJs_Z": $("#PlacesJs_Z").val()==''?0:$("#PlacesJs_Z").val(),
"p_Registration_Removed_Z": $("#Registration_Removed_Z").val()==''?0:$("#Registration_Removed_Z").val(),
"p_LR_Z": $("#LR_Z").val()==''?0:$("#LR_Z").val(),
"p_Submissions_Z": $("#Submissions_Z").val()==''?0:$("#Submissions_Z").val(),
"p_LR_Prev_Z": $("#LR_Prev_Z").val()==''?0:$("#LR_Prev_Z").val(),

"p_Registration_Total": $("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
"p_PlacesJs_Total": $("#PlacesJs_Total").val()==''?0:$("#PlacesJs_Total").val(),
"p_Registration_Removed_Total": $("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
"p_LR_Total": $("#LR_Total").val()==''?0:$("#LR_Total").val(),
"p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
"p_LR_Prev_Total": $("#LR_Prev_Total").val()==''?0:$("#LR_Prev_Total").val(),

"p_Flag" :Flag,
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "es23Entry";
  securedajaxpost(path,'parsrdataES23RptEntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES23RptEntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES23RptEntryForm();
        
    }
    $('#es25tbleDiv').show();
     if(sessionStorage.Es23Flag=='4'){
      resetMode();
      toastr.success('Records Successfully Send To Head Office');
      $('.visibleControls,.visible').attr('disabled',true);
      $('.btntbl').hide();
      $('#savebtn,#verifybtn').hide();
    } 


 else if(sessionStorage.Es23Flag=='3'){
      if(data[0][0]){
    //    $('#excel').show();
           
            if(data[0][0].Verify_YN=='1'){
              $('.visibleControls,.visible').attr('disabled',true);
              $('#savebtn,#verifybtn').hide();
            }
            else if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
              if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
                $('.visibleControls,.visible').attr('disabled',true);
                $('#savebtn,#verifybtn').hide();
              }

              if(sessionStorage.User_Type_Id=='2'){
              $('#savebtn,#verifybtn').show();
              $('.visibleControls').attr('disabled',true);
              $('.visible').attr('disabled',false);
            }
            else if(sessionStorage.User_Type_Id=='7'){
              $('#savebtn').show();
              $('#verifybtn').hide();
              $('.visibleControls').attr('disabled',true);
              $('.visible').attr('disabled',false);
            }
          }
        
        
        // if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
        //   $('.visibleControls,.visible').attr('disabled',true);
        //   $('#savebtn,#verifybtn').hide();
        // }
        
                sessionStorage.Es23Flag='1'
    $("#Registration_M").val(data[0][0].Registration_M)
    $("#LR_Prev_M").val(data[0][0].LR_Prev_M)
    $("#PlacesJs_M").val(data[0][0].PlacesJs_M) 
    $("#Registration_Removed_M").val(data[0][0].Registration_Removed_M)
    $("#LR_M").val(data[0][0].LR_M)
    $("#Submissions_M").val(data[0][0].Submissions_M)

    $("#Registration_C").val(data[0][0].Registration_C)
    $("#LR_Prev_C").val(data[0][0].LR_Prev_C)
    $("#PlacesJs_C").val(data[0][0].PlacesJs_C) 
    $("#Registration_Removed_C").val(data[0][0].Registration_Removed_C)
    $("#LR_C").val(data[0][0].LR_C)
    $("#Submissions_C").val(data[0][0].Submissions_C)

    $("#Registration_S").val(data[0][0].Registration_S)
    $("#LR_Prev_S").val(data[0][0].LR_Prev_S)
    $("#PlacesJs_S").val(data[0][0].PlacesJs_S) 
    $("#Registration_Removed_S").val(data[0][0].Registration_Removed_S)
    $("#LR_S").val(data[0][0].LR_S)
    $("#Submissions_S").val(data[0][0].Submissions_S)

    $("#Registration_B").val(data[0][0].Registration_B)
    $("#LR_Prev_B").val(data[0][0].LR_Prev_B)
    $("#PlacesJs_B").val(data[0][0].PlacesJs_B) 
    $("#Registration_Removed_B").val(data[0][0].Registration_Removed_B)
    $("#LR_B").val(data[0][0].LR_B)
    $("#Submissions_B").val(data[0][0].Submissions_B)

    $("#Registration_Z").val(data[0][0].Registration_Z)
    $("#LR_Prev_Z").val(data[0][0].LR_Prev_Z)
    $("#PlacesJs_Z").val(data[0][0].PlacesJs_Z) 
    $("#Registration_Removed_Z").val(data[0][0].Registration_Removed_Z)
    $("#LR_Z").val(data[0][0].LR_Z)
    $("#Submissions_Z").val(data[0][0].Submissions_Z)

    $("#Registration_Total").val(parseInt(data[0][0].Registration_M)+parseInt(data[0][0].Registration_C)+parseInt(data[0][0].Registration_S)+parseInt(data[0][0].Registration_B)+parseInt(data[0][0].Registration_Z))
    $("#LR_Prev_Total").val(parseInt(data[0][0].LR_Prev_M)+parseInt(data[0][0].LR_Prev_C)+parseInt(data[0][0].LR_Prev_S)+parseInt(data[0][0].LR_Prev_B)+parseInt(data[0][0].LR_Prev_Z))
    $("#PlacesJs_Total").val(parseInt(data[0][0].PlacesJs_M)+parseInt(data[0][0].PlacesJs_C)+parseInt(data[0][0].PlacesJs_S)+parseInt(data[0][0].PlacesJs_B)+parseInt(data[0][0].PlacesJs_Z))
    $("#Registration_Removed_Total").val(parseInt(data[0][0].Registration_Removed_M)+parseInt(data[0][0].Registration_Removed_C)+parseInt(data[0][0].Registration_Removed_S)+parseInt(data[0][0].Registration_Removed_B)+parseInt(data[0][0].Registration_Removed_Z))
    $("#LR_Total").val(parseInt(data[0][0].LR_M)+parseInt(data[0][0].LR_C)+parseInt(data[0][0].LR_S)+parseInt(data[0][0].LR_B)+parseInt(data[0][0].LR_Z))
    $("#Submissions_Total").val(parseInt(data[0][0].Submissions_M)+parseInt(data[0][0].Submissions_C)+parseInt(data[0][0].Submissions_S)+parseInt(data[0][0].Submissions_B)+parseInt(data[0][0].Submissions_Z))
    
       

      }
      else{
               sessionStorage.Es23Flag='1';
               resetMode();
               if(sessionStorage.User_Type_Id=='7'){
                 $('#savebtn').show();
                }
            
      }
  }

 else if(sessionStorage.Es23Flag=='1'){
    resetMode();
  toastr.success('Added Successfully');

}
 else  if(sessionStorage.Es23Flag=='2'){
    resetMode();
  toastr.success('Modified  Successfully');
  
}   


     }

     function visibilityControls(isDisabled){
         $('.visibleControls').attr('disabled',isDisabled);
         $('.btntbl').hide();
         $('#savebtn').show();

     }

     function resetMode(){
        $("#Registration_M").val(""),
        $("#PlacesJs_M").val(""),
       
        $("#Registration_Removed_M").val(""),
        $("#LR_M").val(""),
        $("#Submissions_M").val(""),
        $("#LR_Prev_M").val(""),

        $("#Registration_C").val(""),
        $("#PlacesJs_C").val(""),
       
        $("#Registration_Removed_C").val(""),
        $("#LR_C").val(""),
        $("#Submissions_C").val(""),
        $("#LR_Prev_C").val(""),

        $("#Registration_S").val(""),
        $("#PlacesJs_S").val(""),
       
        $("#Registration_Removed_S").val(""),
        $("#LR_S").val(""),
        $("#Submissions_S").val(""),
        $("#LR_Prev_S").val(""),

        $("#Registration_B").val(""),
        $("#PlacesJs_B").val(""),
       
        $("#Registration_Removed_B").val(""),
        $("#LR_B").val(""),
        $("#Submissions_B").val(""),
        $("#LR_Prev_B").val(""),
     
        $("#Registration_Z").val(""),
        $("#PlacesJs_Z").val(""),
       
        $("#Registration_Removed_Z").val(""),
        $("#LR_Z").val(""),
        $("#Submissions_Z").val(""),
        $("#LR_Prev_Z").val(""),
        
        $("#Registration_Total").val(""),
        $("#PlacesJs_Total").val(""),
       
        $("#Registration_Removed_Total").val(""),
        $("#LR_Total").val(""),
        $("#Submissions_Total").val(""),
        $("#LR_Prev_Total").val("")

      
     }
     function getTotal(type){
var total=0;
var M=$(`#${type}_M`).val()==''?0:$(`#${type}_M`).val();
var C=$(`#${type}_C`).val()==''?0:$(`#${type}_C`).val();
var S=$(`#${type}_S`).val()==''?0:$(`#${type}_S`).val();
var B=$(`#${type}_B`).val()==''?0:$(`#${type}_B`).val();
var Z=$(`#${type}_Z`).val()==''?0:$(`#${type}_Z`).val();

total=parseInt(M)+parseInt(C)+parseInt(S)+parseInt(B)+parseInt(Z);
$(`#${type}_Total`).val(total);
}
