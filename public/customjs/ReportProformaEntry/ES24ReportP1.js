
function ES24Part1RptEntryForm(Flag){
    var FromDt='';
    var ToDt='';
    var printonTblheadYear='';
    sessionStorage.Es24p1Flag=Flag;
if($('#criteriaddl').val()=='1'){
    FromDt=`${$('#yearses24p1').val()}-01-01`;
    ToDt=`${$('#yearses24p1').val()}-06-30`;
   printonTblheadYear=`30/06/${$('#yearses24p1').val()}`;
}
else if($('#criteriaddl').val()=='2'){
    FromDt=`${$('#yearses24p1').val()}-07-01`;
    ToDt=`${$('#yearses24p1').val()}-12-31`;
    printonTblheadYear=`31/12/${$('#yearses24p1').val()}`;
}
$('#tblheadYear').text(`${printonTblheadYear}`)
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    var ex_id =  $("#ddExchange").val();
        }
    else{
      var ex_id =  sessionStorage.Ex_id;
    }
var MasterData = {
        
"p_Ex_Id":ex_id,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Registration_SC": $("#Registration_SC").val()==''?0:$("#Registration_SC").val(),
"p_PlacesJs_CG_SC": $("#PlacesJs_CG_SC").val()==''?0:$("#PlacesJs_CG_SC").val(),
"p_PlacesJs_UT_SC": $("#PlacesJs_UT_SC").val()==''?0:$("#PlacesJs_UT_SC").val(),
"p_PlacesJs_SG_SC": $("#PlacesJs_SG_SC").val()==''?0:$("#PlacesJs_SG_SC").val(),
"p_PlacesJs_QG_SC": $("#PlacesJs_QG_SC").val()==''?0:$("#PlacesJs_QG_SC").val(),
"p_PlacesJs_LB_SC": $("#PlacesJs_LB_SC").val()==''?0:$("#PlacesJs_LB_SC").val(),
"p_PlacesJs_Private_SC": $("#PlacesJs_Private_SC").val()==''?0:$("#PlacesJs_Private_SC").val(),
"p_PlacesJs_Total_SC": $("#PlacesJs_Total_SC").val()==''?0:$("#PlacesJs_Total_SC").val(),
"p_Registration_Removed_SC": $("#Registration_Removed_SC ").val()==''?0:$("#Registration_Removed_SC ").val(),
"p_LR_SC": $("#LR_SC").val()==''?0:$("#LR_SC").val(),
"p_Submissions_SC": $("#Submissions_SC").val()==''?0:$("#Submissions_SC").val(),
"p_LR_Prev_1_SC": $("#LR_Prev_1_SC").val()==''?0:$("#LR_Prev_1_SC").val(),
"p_Registration_ST": $("#Registration_ST").val()==''?0:$("#Registration_ST").val(),
"p_PlacesJs_CG_ST": $("#PlacesJs_CG_ST").val()==''?0:$("#PlacesJs_CG_ST").val(),
"p_PlacesJs_UT_ST": $("#PlacesJs_UT_ST").val()==''?0:$("#PlacesJs_UT_ST").val(),
"p_PlacesJs_SG_ST": $("#PlacesJs_SG_ST").val()==''?0:$("#PlacesJs_SG_ST").val(),
"p_PlacesJs_QG_ST": $("#PlacesJs_QG_ST").val()==''?0:$("#PlacesJs_QG_ST").val(),
"p_PlacesJs_LB_ST": $("#PlacesJs_LB_ST").val()==''?0:$("#PlacesJs_LB_ST").val(),
"p_PlacesJs_Private_ST": $("#PlacesJs_Private_ST").val()==''?0:$("#PlacesJs_Private_ST").val(),
"p_PlacesJs_Total_ST": $("#PlacesJs_Total_ST").val()==''?0:$("#PlacesJs_Total_ST").val(),
"p_Registration_Removed_ST": $("#Registration_Removed_ST ").val()==''?0:$("#Registration_Removed_ST ").val(),
"p_LR_ST": $("#LR_ST").val()==''?0:$("#LR_ST").val(),
"p_Submissions_ST": $("#Submissions_ST").val()==''?0:$("#Submissions_ST").val(),
"p_LR_Prev_1_ST": $("#LR_Prev_1_ST").val()==''?0:$("#LR_Prev_1_ST").val(),
"p_Registration_OBC": $("#Registration_OBC").val()==''?0:$("#Registration_OBC").val(),
"p_PlacesJs_CG_OBC": $("#PlacesJs_CG_OBC").val()==''?0:$("#PlacesJs_CG_OBC").val(),
"p_PlacesJs_UT_OBC": $("#PlacesJs_UT_OBC").val()==''?0:$("#PlacesJs_UT_OBC").val(),
"p_PlacesJs_SG_OBC": $("#PlacesJs_SG_OBC").val()==''?0:$("#PlacesJs_SG_OBC").val(),
"p_PlacesJs_QG_OBC": $("#PlacesJs_QG_OBC").val()==''?0:$("#PlacesJs_QG_OBC").val(),
"p_PlacesJs_LB_OBC": $("#PlacesJs_LB_OBC").val()==''?0:$("#PlacesJs_LB_OBC").val(),
"p_PlacesJs_Private_OBC": $("#PlacesJs_Private_OBC").val()==''?0:$("#PlacesJs_Private_OBC").val(),
"p_PlacesJs_Total_OBC": $("#PlacesJs_Total_OBC").val()==''?0:$("#PlacesJs_Total_OBC").val(),
"p_Registration_Removed_OBC": $("#Registration_Removed_OBC ").val()==''?0:$("#Registration_Removed_OBC ").val(),
"p_LR_OBC": $("#LR_OBC").val()==''?0:$("#LR_OBC").val(),
"p_Submissions_OBC": $("#Submissions_OBC").val()==''?0:$("#Submissions_OBC").val(),
"p_LR_Prev_1_OBC": $("#LR_Prev_1_OBC").val()==''?0:$("#LR_Prev_1_OBC").val(),
"p_Flag" :Flag,
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "es24p1RptEntry";
  securedajaxpost(path,'parsrdataES25RptEntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES25RptEntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES25RptEntryForm();
        
    }
    $('#es25tbleDiv').show();
  if(sessionStorage.Es24p1Flag=='3'){
      if(data[0][0]){

          if(data[0][0].Verify_YN=='1'){
            $('#verifybtn,#savebtn').hide();
            $('.visibleControls,.visible').attr('disabled',true);
          }
          else if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
            if(sessionStorage.User_Type_Id=='2'){
            $('#verifybtn, #savebtn').show();
            $('.visibleControls').attr('disabled',true); 
            $('.visible').attr('disabled',false); 
            }
          
          else if(sessionStorage.User_Type_Id=='7'){
            $('#verifybtn').hide();
            $('#savebtn').show();
            $('.visibleControls').attr('disabled',true); 
            $('.visible').attr('disabled',false); 
          }
        }

        if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
          $('.visibleControls,.visible').attr('disabled',true);
          $('#savebtn,#verifybtn').hide();
        }
          
          sessionStorage.Es24p1Flag='2'
    $("#Registration_SC").val(data[0][0].Registration_SC),
    $("#Registration_ST").val(data[0][0].Registration_ST),
    $("#Registration_OBC").val(data[0][0].Registration_OBC)
    $("#LR_Prev_1_SC").val(data[0][0].LR_Prev_1_SC)
    $("#LR_Prev_1_ST").val(data[0][0].LR_Prev_1_ST)
    $("#LR_Prev_1_OBC").val(data[0][0].LR_Prev_1_OBC)
    $("#PlacesJs_CG_SC").val(data[0][0].PlacesJs_CG_SC)
    $("#PlacesJs_CG_ST").val(data[0][0].PlacesJs_CG_ST)
    $("#PlacesJs_CG_OBC").val(data[0][0].PlacesJs_CG_OBC)  
    $("#PlacesJs_UT_SC").val(data[0][0].PlacesJs_UT_SC)
    $("#PlacesJs_UT_ST").val(data[0][0].PlacesJs_UT_ST)
    $("#PlacesJs_UT_OBC").val(data[0][0].PlacesJs_UT_OBC)
    $("#PlacesJs_SG_SC").val(data[0][0].PlacesJs_SG_SC)
    $("#PlacesJs_SG_ST").val(data[0][0].PlacesJs_SG_ST)
    $("#PlacesJs_SG_OBC").val(data[0][0].PlacesJs_SG_OBC)
    $("#PlacesJs_QG_SC").val(data[0][0].PlacesJs_QG_SC)
    $("#PlacesJs_QG_ST").val(data[0][0].PlacesJs_QG_ST)
    $("#PlacesJs_QG_OBC").val(data[0][0].PlacesJs_QG_OBC)
    $("#PlacesJs_LB_SC").val(data[0][0].PlacesJs_LB_SC)
    $("#PlacesJs_LB_ST").val(data[0][0].PlacesJs_LB_ST)
    $("#PlacesJs_LB_OBC").val(data[0][0].PlacesJs_LB_OBC)
    $("#PlacesJs_Private_SC").val(data[0][0].PlacesJs_Private_SC)
    $("#PlacesJs_Private_ST").val(data[0][0].PlacesJs_Private_ST)
    $("#PlacesJs_Private_OBC").val(data[0][0].PlacesJs_Private_OBC)
    $("#Registration_Removed_SC").val(data[0][0].Registration_Removed_SC)
    $("#Registration_Removed_ST").val(data[0][0].Registration_Removed_ST)
    $("#Registration_Removed_OBC").val(data[0][0].Registration_Removed_OBC)
    $("#LR_SC").val(data[0][0].LR_SC)
    $("#LR_ST").val(data[0][0].LR_ST)
    $("#LR_OBC").val(data[0][0].LR_OBC)
    $("#Submissions_SC").val(data[0][0].Submissions_SC)
    $("#Submissions_ST").val(data[0][0].Submissions_ST)
    $("#Submissions_OBC").val(data[0][0].Submissions_OBC)
      }
      else{
              
               sessionStorage.Es24p1Flag='1';
               resetMode();
               if(sessionStorage.User_Type_Id=='7'){
                $('#savebtn').show();
                $('.visibleControls').attr('disabled',true);
                $('.visible').attr('disabled',false);
               }
            else  if(sessionStorage.User_Type_Id=='2'){
              $('#savebtn').show();
              $('.visibleControls').attr('disabled',true);
              $('.visible').attr('disabled',false);
            }
            else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
              $('#verifybtn, #savebtn').hide();
            $('.visibleControls,.visible').attr('disabled',true);
            }
          }
  }
 else if(sessionStorage.Es24p1Flag=='1'){
    resetMode();
  toastr.success('Added Successfully');
}
 else  if(sessionStorage.Es24p1Flag=='2'){
     
    resetMode();
  toastr.success('Modified  Successfully');
  
}   
else if(sessionStorage.Es24p1Flag=='4'){
    
    resetMode();
    toastr.success('Records Successfully Send To Head Office');
    $('#verifybtn,#savebtn').hide();
    $('.visibleControls').attr('disabled',true);
  } 

     }

     function visibilityControls(isDisabled){
         $('.visibleControls').attr('disabled',isDisabled);
         $('.btntbl').hide();
         $('#savebtn').show();

     }

     function resetMode(){
        $("#Registration_SC").val(""),
        $("#PlacesJs_CG_SC").val(""),
        $("#PlacesJs_UT_SC").val(""),
        $("#PlacesJs_SG_SC").val(""),
        $("#PlacesJs_QG_SC").val(""),
        $("#PlacesJs_LB_SC").val(""),
        $("#PlacesJs_Private_SC").val(""),
        $("#PlacesJs_Total_SC").val(""),
        $("#Registration_Removed_SC ").val(""),
        $("#LR_SC").val(""),
        $("#Submissions_SC").val(""),
        $("#LR_Prev_1_SC").val(""),

        $("#Registration_ST").val(""),
        $("#PlacesJs_CG_ST").val(""),
        $("#PlacesJs_UT_ST").val(""),
        $("#PlacesJs_SG_ST").val(""),
        $("#PlacesJs_QG_ST").val(""),
        $("#PlacesJs_LB_ST").val(""),
        $("#PlacesJs_Private_ST").val(""),
        $("#PlacesJs_Total_ST").val(""),
        $("#Registration_Removed_ST ").val(""),
        $("#LR_ST").val(""),
        $("#Submissions_ST").val(""),
        $("#LR_Prev_1_ST").val(""),

        $("#Registration_OBC").val(""),
        $("#PlacesJs_CG_OBC").val(""),
        $("#PlacesJs_UT_OBC").val(""),
        $("#PlacesJs_SG_OBC").val(""),
        $("#PlacesJs_QG_OBC").val(""),
        $("#PlacesJs_LB_OBC").val(""),
        $("#PlacesJs_Private_OBC").val(""),
        $("#PlacesJs_Total_OBC").val(""),
        $("#Registration_Removed_OBC ").val(""),
        $("#LR_OBC").val(""),
        $("#Submissions_OBC").val(""),
        $("#LR_Prev_1_OBC").val("")

      
     }

     function getTotal(type){
       total=0;
       var PlacesJs_CG=$(`#PlacesJs_CG_${type}`).val()==''?0:$(`#PlacesJs_CG_${type}`).val();
       var PlacesJs_UT=$(`#PlacesJs_UT_${type}`).val()==''?0:$(`#PlacesJs_UT_${type}`).val();
       var PlacesJs_SG=$(`#PlacesJs_SG_${type}`).val()==''?0:$(`#PlacesJs_SG_${type}`).val();
       var PlacesJs_QG=$(`#PlacesJs_QG_${type}`).val()==''?0:$(`#PlacesJs_QG_${type}`).val();
       var PlacesJs_LB=$(`#PlacesJs_LB_${type}`).val()==''?0:$(`#PlacesJs_LB_${type}`).val();
       var PlacesJs_Private=$(`#PlacesJs_Private_${type}`).val()==''?0:$(`#PlacesJs_Private_${type}`).val();
total=parseInt(PlacesJs_CG)+
parseInt(PlacesJs_UT)+
parseInt(PlacesJs_SG)+
parseInt(PlacesJs_QG)+
parseInt(PlacesJs_LB)+
parseInt(PlacesJs_Private);

$(`#PlacesJs_Total_${type}`).val(total);
     }

