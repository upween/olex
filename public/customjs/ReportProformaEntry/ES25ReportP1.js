function FetchES25(){
    if ($("#years").val() == "0") {
        toastr.warning("Please Select Year", "", "info")
        return true;
    }
    else{
if($("#ChooseReport").val()=='1'){
var fromdate = '01-01-'+$("#years").val()+''
var todate = '30-06-'+$("#years").val()+''
$("#year").html(todate)
$("#year1").html(todate)
}
else if($("#ChooseReport").val()=='2'){
    var fromdate = '01-07-'+$("#years").val()+''
    var todate = '31-12-'+$("#years").val()+''
    $("#year").html(todate)
    $("#year1").html(todate)
}

    var path =  serverpath + "getes25p1Rpt/"+fromdate+"/"+todate+"/"+sessionStorage.Ex_id+""
    ajaxget(path,'parsedataFetchES25Rpt','comment',"control");
    }
}

function parsedataFetchES25Rpt(data){
    data = JSON.parse(data)

    $("#LR_Prev_1_W").val(data[0][0].LR_Prev_1_W)
    $("#Registration_W").val(data[0][0].Registration_W) 
    $("#PlacesJs_CG_W").val(data[0][0].PlacesJs_CG_W)
    $("#PlacesJs_UT_W").val(data[0][0].PlacesJs_UT_W) 
    $("#PlacesJs_SG_W").val(data[0][0].PlacesJs_SG_W)
    $("#PlacesJs_QG_W").val(data[0][0].PlacesJs_QG_W)
    $("#PlacesJs_LB_W").val(data[0][0].PlacesJs_LB_W)
    $("#PlacesJs_Private_W").val(data[0][0].PlacesJs_Private_W)
    $("#Registration_Removed_W").val(data[0][0].Registration_Removed_W)
    $("#LR_W").val(data[0][0].LR_W)
    $("#Submissions_W").val(data[0][0].Submissions_W)

    $("#LR_Prev_1_Blind").val(data[0][0].LR_Prev_1_Blind)
    $("#Registration_Blind").val(data[0][0].Registration_Blind)
    $("#PlacesJs_CG_Blind").val(data[0][0].PlacesJs_CG_Blind)
    $("#PlacesJs_UT_Blind").val(data[0][0].PlacesJs_UT_Blind)
    $("#PlacesJs_SG_Blind").val(data[0][0].PlacesJs_SG_Blind)
    $("#PlacesJs_QG_Blind").val(data[0][0].PlacesJs_QG_Blind)
    $("#PlacesJs_LB_Blind").val(data[0][0].PlacesJs_LB_Blind)
    $("#PlacesJs_Private_Blind").val(data[0][0].PlacesJs_Private_Blind)
    $("#Registration_Removed_Blind").val(data[0][0].Registration_Removed_Blind)
    $("#LR_Blind").val(data[0][0].LR_Blind)    
    $("#Submissions_Blind").val(data[0][0].Submissions_Blind)    

    $("#LR_Prev_1_Deaf").val(data[0][0].LR_Prev_1_Deaf)   
    $("#Registration_Deaf").val(data[0][0].Registration_Deaf)   
    $("#PlacesJs_CG_Deaf").val(data[0][0].PlacesJs_CG_Deaf)   
    $("#PlacesJs_UT_Deaf").val(data[0][0].PlacesJs_UT_Deaf)   
    $("#PlacesJs_SG_Deaf").val(data[0][0].PlacesJs_SG_Deaf)   
    $("#PlacesJs_QG_Deaf").val(data[0][0].PlacesJs_QG_Deaf)   
    $("#PlacesJs_LB_Deaf").val(data[0][0].PlacesJs_LB_Deaf)   
    $("#PlacesJs_Private_Deaf").val(data[0][0].PlacesJs_Private_Deaf)   
    $("#Registration_Removed_Deaf").val(data[0][0].Registration_Removed_Deaf)   
    $("#LR_Deaf").val(data[0][0].LR_Deaf)   
    $("#Submissions_Deaf").val(data[0][0].Submissions_Deaf)  
     
    $("#LR_Prev_1_Orthopaedics").val(data[0][0].LR_Prev_1_Orthopaedics)   
    $("#Registration_Orthopaedics").val(data[0][0].Registration_Orthopaedics)   
    $("#PlacesJs_CG_Orthopaedics").val(data[0][0].PlacesJs_CG_Orthopaedics)   
    $("#PlacesJs_UT_Orthopaedics").val(data[0][0].PlacesJs_UT_Orthopaedics)   
    $("#PlacesJs_SG_Orthopaedics").val(data[0][0].PlacesJs_SG_Orthopaedics)   
    $("#PlacesJs_QG_Orthopaedics").val(data[0][0].PlacesJs_QG_Orthopaedics)   
    $("#PlacesJs_LB_Orthopaedics").val(data[0][0].PlacesJs_LB_Orthopaedics)   
    $("#PlacesJs_Private_Orthopaedics").val(data[0][0].PlacesJs_Private_Orthopaedics)   
    $("#Registration_Removed_Orthopaedics").val(data[0][0].Registration_Removed_Orthopaedics)   
    $("#LR_Orthopaedics").val(data[0][0].LR_Orthopaedics)   
    $("#Submissions_Orthopaedics").val(data[0][0].Submissions_Orthopaedics) 
    
    $("#LR_Prev_1_Respiratory").val(data[0][0].LR_Prev_1_Respiratory)   
    $("#Registration_Respiratory").val(data[0][0].Registration_Respiratory)   
    $("#PlacesJs_CG_Respiratory").val(data[0][0].PlacesJs_CG_Respiratory)   
    $("#PlacesJs_UT_Respiratory").val(data[0][0].PlacesJs_UT_Respiratory)   
    $("#PlacesJs_SG_Respiratory").val(data[0][0].PlacesJs_SG_Respiratory)   
    $("#PlacesJs_QG_Respiratory").val(data[0][0].PlacesJs_QG_Respiratory)   
    $("#PlacesJs_LB_Respiratory").val(data[0][0].PlacesJs_LB_Respiratory)   
    $("#PlacesJs_Private_Respiratory").val(data[0][0].PlacesJs_Private_Respiratory)   
    $("#Registration_Removed_Respiratory").val(data[0][0].Registration_Removed_Respiratory)   
    $("#LR_Respiratory").val(data[0][0].LR_Respiratory)   
    $("#Submissions_Respiratory").val(data[0][0].Submissions_Respiratory) 

    $("#LR_Prev_1_Leprosy").val(data[0][0].LR_Prev_1_Leprosy)   
    $("#Registration_Leprosy").val(data[0][0].Registration_Leprosy)   
    $("#PlacesJs_CG_Leprosy").val(data[0][0].PlacesJs_CG_Leprosy)   
    $("#PlacesJs_UT_Leprosy").val(data[0][0].PlacesJs_UT_Leprosy)   
    $("#PlacesJs_SG_Leprosy").val(data[0][0].PlacesJs_SG_Leprosy)   
    $("#PlacesJs_QG_Leprosy").val(data[0][0].PlacesJs_QG_Leprosy)   
    $("#PlacesJs_LB_Leprosy").val(data[0][0].PlacesJs_LB_Leprosy)   
    $("#PlacesJs_Private_Leprosy").val(data[0][0].PlacesJs_Private_Leprosy)   
    $("#Registration_Removed_Leprosy").val(data[0][0].Registration_Removed_Leprosy)   
    $("#LR_Leprosy").val(data[0][0].LR_Leprosy)   
    $("#Submissions_Leprosy").val(data[0][0].Submissions_Leprosy) 

    $("#LR_Prev_1_Total").val(data[0][0].LR_Prev_1_Total)   
    $("#Registration_Total").val(data[0][0].Registration_Total)   
    $("#PlacesJs_CG_Total").val(data[0][0].PlacesJs_CG_Total)   
    $("#PlacesJs_UT_Total").val(data[0][0].PlacesJs_UT_Total)   
    $("#PlacesJs_SG_Total").val(data[0][0].PlacesJs_SG_Total)   
    $("#PlacesJs_QG_Total").val(data[0][0].PlacesJs_QG_Total)   
    $("#PlacesJs_LB_Total").val(data[0][0].PlacesJs_LB_Total)   
    $("#PlacesJs_Private_Total").val(data[0][0].PlacesJs_Private_Total)   
    $("#Registration_Removed_Total").val(data[0][0].Registration_Removed_Total)   
    $("#LR_Total").val(data[0][0].LR_Total)   
    $("#Submissions_Total").val(data[0][0].Submissions_Total) 

   
}
function ES25RptEntryForm(Flag){
    var FromDt='';
    var ToDt='';
    var printonTblheadYear='';
    sessionStorage.Es25Flag=Flag;
if($('#criteriaddl').val()=='1'){
    FromDt=`${$('#yeares25').val()}-01-01`;
    ToDt=`${$('#yeares25').val()}-06-30`;
   printonTblheadYear=`30/06/${$('#yeares25').val()}`;
}
else if($('#criteriaddl').val()=='2'){
    FromDt=`${$('#yeares25').val()}-07-01`;
    ToDt=`${$('#yeares25').val()}-12-31`;
    printonTblheadYear=`31/12/${$('#yeares25').val()}`;
}
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    var ex_id =  $("#ddExchange").val();
      }
    else{
      var ex_id =  sessionStorage.Ex_id;
    }
$('#tblheadYear').text(`Return showing the work done by the Employment Exchnages in respect of All Disabled applicants during the Half Year ended_________${printonTblheadYear}__________Return showing the work done by the Employment Exchnages in respect of Scheduled Caste/Tribe and Other Backward Classes(OBC) applicats during Half Year-ended___${printonTblheadYear} `)
    var MasterData = {
        
"p_Ex_Id":ex_id,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Registration_Blind": $("#Registration_Blind").val()==''?0:$("#Registration_Blind").val(),
"p_PlacesJs_CG_Blind": $("#PlacesJs_CG_Blind").val()==''?0:$("#PlacesJs_CG_Blind").val(),
"p_PlacesJs_UT_Blind": $("#PlacesJs_UT_Blind").val()==''?0:$("#PlacesJs_UT_Blind").val(),
"p_PlacesJs_SG_Blind": $("#PlacesJs_SG_Blind").val()==''?0:$("#PlacesJs_SG_Blind").val(),
"p_PlacesJs_QG_Blind": $("#PlacesJs_QG_Blind").val()==''?0:$("#PlacesJs_QG_Blind").val(),
"p_PlacesJs_LB_Blind": $("#PlacesJs_LB_Blind").val()==''?0:$("#PlacesJs_LB_Blind").val(),
"p_PlacesJs_Private_Blind": $("#PlacesJs_Private_Blind").val()==''?0:$("#PlacesJs_Private_Blind").val(),
"p_PlacesJs_Total_Blind": $("#PlacesJs_Total_Blind").val()==''?0:$("#PlacesJs_Total_Blind").val(),
"p_Registration_Removed_Blind": $("#Registration_Removed_Blind ").val()==''?0:$("#Registration_Removed_Blind ").val(),
"p_LR_Blind": $("#LR_Blind").val()==''?0:$("#LR_Blind").val(),
"p_Submissions_Blind": $("#Submissions_Blind").val()==''?0:$("#Submissions_Blind").val(),
"p_LR_Prev_1_Blind": $("#LR_Prev_1_Blind").val()==''?0:$("#LR_Prev_1_Blind").val(),
"p_Registration_Deaf": $("#Registration_Deaf").val()==''?0:$("#Registration_Deaf").val(),
"p_PlacesJs_CG_Deaf": $("#PlacesJs_CG_Deaf").val()==''?0:$("#PlacesJs_CG_Deaf").val(),
"p_PlacesJs_UT_Deaf": $("#PlacesJs_UT_Deaf").val()==''?0:$("#PlacesJs_UT_Deaf").val(),
"p_PlacesJs_SG_Deaf":$("#PlacesJs_SG_Deaf").val()==''?0:$("#PlacesJs_SG_Deaf").val(),
"p_PlacesJs_QG_Deaf":$("#PlacesJs_QG_Deaf").val()==''?0:$("#PlacesJs_QG_Deaf").val(),
"p_PlacesJs_LB_Deaf":$("#PlacesJs_LB_Deaf").val()==''?0:$("#PlacesJs_LB_Deaf").val(),
"p_PlacesJs_Private_Deaf":$("#PlacesJs_Private_Deaf").val()==''?0:$("#PlacesJs_Private_Deaf").val(),
"p_PlacesJs_Total_Deaf":$("#PlacesJs_Total_Deaf").val()==''?0:$("#PlacesJs_Total_Deaf").val(),
"p_Registration_Removed_Deaf":$("#Registration_Removed_Deaf").val()==''?0:$("#Registration_Removed_Deaf").val(),
"p_LR_Deaf":$("#LR_Deaf").val()==''?0:$("#LR_Deaf").val(),
"p_Submissions_Deaf":$("#Submissions_Deaf").val()==''?0:$("#Submissions_Deaf").val(),
"p_LR_Prev_1_Deaf":$("#LR_Prev_1_Deaf").val()==''?0:$("#LR_Prev_1_Deaf").val(),
"p_Registration_Orthopaedics": $("#Registration_Orthopaedics").val()==''?0:$("#Registration_Orthopaedics").val(),
"p_PlacesJs_CG_Orthopaedics": $("#PlacesJs_CG_Orthopaedics").val()==''?0:$("#PlacesJs_CG_Orthopaedics").val(),
"p_PlacesJs_UT_Orthopaedics":$("#PlacesJs_UT_Orthopaedics").val()==''?0:$("#PlacesJs_UT_Orthopaedics").val(),
"p_PlacesJs_SG_Orthopaedics":$("#PlacesJs_SG_Orthopaedics").val()==''?0:$("#PlacesJs_SG_Orthopaedics").val(),
"p_PlacesJs_QG_Orthopaedics":$("#PlacesJs_QG_Orthopaedics").val()==''?0:$("#PlacesJs_QG_Orthopaedics").val(),
"p_PlacesJs_LB_Orthopaedics":$("#PlacesJs_LB_Orthopaedics").val()==''?0:$("#PlacesJs_LB_Orthopaedics").val(),
"p_PlacesJs_Private_Orthopaedics":$("#PlacesJs_Private_Orthopaedics").val()==''?0:$("#PlacesJs_Private_Orthopaedics").val(),
"p_PlacesJs_Total_Orthopaedics":$("#PlacesJs_Total_Orthopaedics").val()==''?0:$("#PlacesJs_Total_Orthopaedics").val(),
"p_Registration_Removed_Orthopaedics":$("#Registration_Removed_Orthopaedics").val()==''?0:$("#Registration_Removed_Orthopaedics").val(),
"p_LR_Orthopaedics":$("#LR_Orthopaedics").val()==''?0:$("#LR_Orthopaedics").val(),
"p_Submissions_Orthopaedics":$("#Submissions_Orthopaedics").val()==''?0:$("#Submissions_Orthopaedics").val(),
"p_LR_Prev_1_Orthopaedics":$("#LR_Prev_1_Orthopaedics").val()==''?0:$("#LR_Prev_1_Orthopaedics").val(),
"p_Registration_Respiratory":$("#Registration_Respiratory").val()==''?0:$("#Registration_Respiratory").val(),
"p_PlacesJs_CG_Respiratory":$("#PlacesJs_CG_Respiratory").val()==''?0:$("#PlacesJs_CG_Respiratory").val(),
"p_PlacesJs_UT_Respiratory":$("#PlacesJs_UT_Respiratory").val()==''?0:$("#PlacesJs_UT_Respiratory").val(),
"p_PlacesJs_SG_Respiratory":$("#PlacesJs_SG_Respiratory").val()==''?0:$("#PlacesJs_SG_Respiratory").val(),
"p_PlacesJs_QG_Respiratory":$("#PlacesJs_QG_Respiratory").val()==''?0:$("#PlacesJs_QG_Respiratory").val(),
"p_PlacesJs_LB_Respiratory":$("#PlacesJs_LB_Respiratory").val()==''?0:$("#PlacesJs_LB_Respiratory").val(),
"p_PlacesJs_Private_Respiratory":$("#PlacesJs_Private_Respiratory").val()==''?0:$("#PlacesJs_Private_Respiratory").val(),
"p_PlacesJs_Total_Respiratory":$("#PlacesJs_Total_Respiratory").val()==''?0:$("#PlacesJs_Total_Respiratory").val(),
"p_Registration_Removed_Respiratory":$("#Registration_Removed_Respiratory").val()==''?0:$("#Registration_Removed_Respiratory").val(),
"p_LR_Respiratory":$("#LR_Respiratory").val()==''?0:$("#LR_Respiratory").val(),
"p_Submissions_Respiratory":$("#Submissions_Respiratory").val()==''?0:$("#Submissions_Respiratory").val(),
"p_LR_Prev_1_Respiratory":$("#LR_Prev_1_Respiratory").val()==''?0:$("#LR_Prev_1_Respiratory").val(),
"p_Registration_Leprosy":$("#Registration_Leprosy").val()==''?0:$("#Registration_Leprosy").val(),
"p_PlacesJs_CG_Leprosy":$("#PlacesJs_CG_Leprosy").val()==''?0:$("#PlacesJs_CG_Leprosy").val(),
"p_PlacesJs_UT_Leprosy":$("#PlacesJs_UT_Leprosy").val()==''?0:$("#PlacesJs_UT_Leprosy").val(),
"p_PlacesJs_SG_Leprosy":$("#PlacesJs_SG_Leprosy").val()==''?0:$("#PlacesJs_SG_Leprosy").val(),
"p_PlacesJs_QG_Leprosy":$("#PlacesJs_QG_Leprosy").val()==''?0:$("#PlacesJs_QG_Leprosy").val(),
"p_PlacesJs_LB_Leprosy":$("#PlacesJs_LB_Leprosy").val()==''?0:$("#PlacesJs_LB_Leprosy").val(),
"p_PlacesJs_Private_Leprosy":$("#PlacesJs_Private_Leprosy").val()==''?0:$("#PlacesJs_Private_Leprosy").val(),
"p_PlacesJs_Total_Leprosy":$("#PlacesJs_Total_Leprosy").val()==''?0:$("#PlacesJs_Total_Leprosy").val(),
"p_Registration_Removed_Leprosy":$("#Registration_Removed_Leprosy").val()==''?0:$("#Registration_Removed_Leprosy").val(),
"p_LR_Leprosy":$("#LR_Leprosy").val()==''?0:$("#LR_Leprosy").val(),
"p_Submissions_Leprosy":$("#Submissions_Leprosy").val()==''?0:$("#Submissions_Leprosy").val(),
"p_LR_Prev_1_Leprosy":$("#LR_Prev_1_Leprosy").val()==''?0:$("#LR_Prev_1_Leprosy").val(),
"p_Registration_Total":$("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
"p_PlacesJs_CG_Total":$("#PlacesJs_CG_Total").val()==''?0:$("#PlacesJs_CG_Total").val(),
"p_PlacesJs_UT_Total":$("#PlacesJs_UT_Total").val()==''?0:$("#PlacesJs_UT_Total").val(),
"p_PlacesJs_SG_Total":$("#PlacesJs_SG_Total").val()==''?0:$("#PlacesJs_SG_Total").val(),
"p_PlacesJs_QG_Total":$("#PlacesJs_QG_Total").val()==''?0:$("#PlacesJs_QG_Total").val(),
"p_PlacesJs_LB_Total":$("#PlacesJs_LB_Total").val()==''?0:$("#PlacesJs_LB_Total").val(),
"p_PlacesJs_Private_Total":$("#PlacesJs_Private_Total").val()==''?0:$("#PlacesJs_Private_Total").val(),
"p_PlacesJs_Total_Total":$("#PlacesJs_Total_Total").val()==''?0:$("#PlacesJs_Total_Total").val(),
"p_Registration_Removed_Total":$("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
"p_LR_Total":$("#LR_Total").val()==''?0:$("#LR_Total").val(),
"p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
"p_LR_Prev_1_Total": $("#LR_Prev_1_Total").val()==''?0:$("#LR_Prev_1_Total").val(),
"p_Registration_W": $("#Registration_W").val()==''?0:$("#Registration_W").val(),
"p_PlacesJs_CG_W": $("#PlacesJs_CG_W").val()==''?0:$("#PlacesJs_CG_W").val(),
"p_PlacesJs_UT_W": $("#PlacesJs_UT_W").val()==''?0:$("#PlacesJs_UT_W").val(),
"p_PlacesJs_SG_W": $("#PlacesJs_SG_W").val()==''?0:$("#PlacesJs_SG_W").val(),
"p_PlacesJs_QG_W": $("#PlacesJs_QG_W").val()==''?0:$("#PlacesJs_QG_W").val(),
"p_PlacesJs_LB_W": $("#PlacesJs_LB_W").val()==''?0:$("#PlacesJs_LB_W").val(),
"p_PlacesJs_Private_W": $("#PlacesJs_Private_W").val()==''?0:$("#PlacesJs_Private_W").val(),
"p_PlacesJs_Total_W": $("#PlacesJs_Total_W").val()==''?0:$("#PlacesJs_Total_W").val(),
"p_Registration_Removed_W": $("#Registration_Removed_W").val()==''?0:$("#Registration_Removed_W").val(),
"p_LR_W": $("#LR_W").val()==''?0:$("#LR_W").val(),
"p_Submissions_W": $("#Submissions_W").val()==''?0:$("#Submissions_W").val(),
"p_LR_Prev_1_W": $("#LR_Prev_1_W").val()==''?0:$("#LR_Prev_1_W").val(),
"p_Flag" :Flag,
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "es25p1RptEntry";
  securedajaxpost(path,'parsrdataES25RptEntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES25RptEntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES25RptEntryForm();
        
    }
    $('#es25tbleDiv').show();
  if(sessionStorage.Es25Flag=='3'){
      if(data[0][0]){
        
      
               
                sessionStorage.Es25Flag='2'
                $("#Registration_Blind").val(data[0][0].Registration_Blind),
                $("#PlacesJs_CG_Blind").val(data[0][0].PlacesJs_CG_Blind),
                $("#PlacesJs_UT_Blind").val(data[0][0].PlacesJs_UT_Blind),
                $("#PlacesJs_SG_Blind").val(data[0][0].PlacesJs_SG_Blind),
                $("#PlacesJs_QG_Blind").val(data[0][0].PlacesJs_QG_Blind),
                $("#PlacesJs_LB_Blind").val(data[0][0].PlacesJs_LB_Blind),
                $("#PlacesJs_Private_Blind").val(data[0][0].PlacesJs_Private_Blind),
                $("#PlacesJs_Total_Blind").val(data[0][0].PlacesJs_Total_Blind),
                $("#Registration_Removed_Blind ").val(data[0][0].Registration_Removed_Blind),
                $("#LR_Blind").val(data[0][0].LR_Blind),
                $("#Submissions_Blind").val(data[0][0].Submissions_Blind),
                $("#LR_Prev_1_Blind").val(data[0][0].LR_Prev_1_Blind),
                

                $("#Registration_Deaf").val(data[0][0].Registration_Deaf),
                $("#PlacesJs_CG_Deaf").val(data[0][0].PlacesJs_CG_Deaf),
                $("#PlacesJs_UT_Deaf").val(data[0][0].PlacesJs_UT_Deaf),
                $("#PlacesJs_SG_Deaf").val(data[0][0].PlacesJs_SG_Deaf),
                $("#PlacesJs_QG_Deaf").val(data[0][0].PlacesJs_QG_Deaf),
                $("#PlacesJs_LB_Deaf").val(data[0][0].PlacesJs_LB_Deaf),
                $("#PlacesJs_Private_Deaf").val(data[0][0].PlacesJs_Private_Deaf),
                $("#PlacesJs_Total_Deaf").val(data[0][0].PlacesJs_Total_Deaf),
                $("#Registration_Removed_Deaf ").val(data[0][0].Registration_Removed_Deaf),
                $("#LR_Deaf").val(data[0][0].LR_Deaf),
                $("#Submissions_Deaf").val(data[0][0].Submissions_Deaf),
                $("#LR_Prev_1_Deaf").val(data[0][0].LR_Prev_1_Deaf),

                $("#Registration_Orthopaedics").val(data[0][0].Registration_Orthopaedics),
                $("#PlacesJs_CG_Orthopaedics").val(data[0][0].PlacesJs_CG_Orthopaedics),
                $("#PlacesJs_UT_Orthopaedics").val(data[0][0].PlacesJs_UT_Orthopaedics),
                $("#PlacesJs_SG_Orthopaedics").val(data[0][0].PlacesJs_SG_Orthopaedics),
                $("#PlacesJs_QG_Orthopaedics").val(data[0][0].PlacesJs_QG_Orthopaedics),
                $("#PlacesJs_LB_Orthopaedics").val(data[0][0].PlacesJs_LB_Orthopaedics),
                $("#PlacesJs_Private_Orthopaedics").val(data[0][0].PlacesJs_Private_Orthopaedics),
                $("#PlacesJs_Total_Orthopaedics").val(data[0][0].PlacesJs_Total_Orthopaedics),
                $("#Registration_Removed_Orthopaedics ").val(data[0][0].Registration_Removed_Orthopaedics),
                $("#LR_Orthopaedics").val(data[0][0].LR_Orthopaedics),
                $("#Submissions_Orthopaedics").val(data[0][0].Submissions_Orthopaedics),
                $("#LR_Prev_1_Orthopaedics").val(data[0][0].LR_Prev_1_Orthopaedics),

                $("#Registration_Respiratory").val(data[0][0].Registration_Respiratory),
                $("#PlacesJs_CG_Respiratory").val(data[0][0].PlacesJs_CG_Respiratory),
                $("#PlacesJs_UT_Respiratory").val(data[0][0].PlacesJs_UT_Respiratory),
                $("#PlacesJs_SG_Respiratory").val(data[0][0].PlacesJs_SG_Respiratory),
                $("#PlacesJs_QG_Respiratory").val(data[0][0].PlacesJs_QG_Respiratory),
                $("#PlacesJs_LB_Respiratory").val(data[0][0].PlacesJs_LB_Respiratory),
                $("#PlacesJs_Private_Respiratory").val(data[0][0].PlacesJs_Private_Respiratory),
                $("#PlacesJs_Total_Respiratory").val(data[0][0].PlacesJs_Total_Respiratory),
                $("#Registration_Removed_Respiratory ").val(data[0][0].Registration_Removed_Respiratory),
                $("#LR_Respiratory").val(data[0][0].LR_Respiratory),
                $("#Submissions_Respiratory").val(data[0][0].Submissions_Respiratory),
                $("#LR_Prev_1_Respiratory").val(data[0][0].LR_Prev_1_Respiratory),

                $("#Registration_Leprosy").val(data[0][0].Registration_Leprosy),
                $("#PlacesJs_CG_Leprosy").val(data[0][0].PlacesJs_CG_Leprosy),
                $("#PlacesJs_UT_Leprosy").val(data[0][0].PlacesJs_UT_Leprosy),
                $("#PlacesJs_SG_Leprosy").val(data[0][0].PlacesJs_SG_Leprosy),
                $("#PlacesJs_QG_Leprosy").val(data[0][0].PlacesJs_QG_Leprosy),
                $("#PlacesJs_LB_Leprosy").val(data[0][0].PlacesJs_LB_Leprosy),
                $("#PlacesJs_Private_Leprosy").val(data[0][0].PlacesJs_Private_Leprosy),
                $("#PlacesJs_Total_Leprosy").val(data[0][0].PlacesJs_Total_Leprosy),
                $("#Registration_Removed_Leprosy ").val(data[0][0].Registration_Removed_Leprosy),
                $("#LR_Leprosy").val(data[0][0].LR_Leprosy),
                $("#Submissions_Leprosy").val(data[0][0].Submissions_Leprosy),
                $("#LR_Prev_1_Leprosy").val(data[0][0].LR_Prev_1_Leprosy),

                $("#Registration_Total").val(data[0][0].Registration_Total),
                $("#PlacesJs_CG_Total").val(data[0][0].PlacesJs_CG_Total),
                $("#PlacesJs_UT_Total").val(data[0][0].PlacesJs_UT_Total),
                $("#PlacesJs_SG_Total").val(data[0][0].PlacesJs_SG_Total),
                $("#PlacesJs_QG_Total").val(data[0][0].PlacesJs_QG_Total),
                $("#PlacesJs_LB_Total").val(data[0][0].PlacesJs_LB_Total),
                $("#PlacesJs_Private_Total").val(data[0][0].PlacesJs_Private_Total),
                $("#PlacesJs_Total_Total").val(data[0][0].PlacesJs_Total_Total),
                $("#Registration_Removed_Total ").val(data[0][0].Registration_Removed_Total),
                $("#LR_Total").val(data[0][0].LR_Total),
                $("#Submissions_Total").val(data[0][0].Submissions_Total),
                $("#LR_Prev_1_Total").val(data[0][0].LR_Prev_1_Total),

                $("#Registration_W").val(data[0][0].Registration_W),
                $("#PlacesJs_CG_W").val(data[0][0].PlacesJs_CG_W),
                $("#PlacesJs_UT_W").val(data[0][0].PlacesJs_UT_W),
                $("#PlacesJs_SG_W").val(data[0][0].PlacesJs_SG_W),
                $("#PlacesJs_QG_W").val(data[0][0].PlacesJs_QG_W),
                $("#PlacesJs_LB_W").val(data[0][0].PlacesJs_LB_W),
                $("#PlacesJs_Private_W").val(data[0][0].PlacesJs_Private_W),
                $("#PlacesJs_Total_W").val(data[0][0].PlacesJs_W_W),
                $("#Registration_Removed_W ").val(data[0][0].Registration_Removed_W),
                $("#LR_W").val(data[0][0].LR_W),
                $("#Submissions_W").val(data[0][0].Submissions_W),
                $("#LR_Prev_1_W").val(data[0][0].LR_Prev_1_W)
     
     
                if(data[0][0].Verify_YN=='1'){
                    $('#savebtn,#verifybtn').hide();
                    $('.visibleControls,.visible').attr('disabled',true);
                }
                else if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
                    if(sessionStorage.User_Type_Id=='2'){
                        $('#savebtn,#verifybtn').show();
                    $('.visibleControls').attr('disabled',true);
                    $('.visible').attr('disabled',false);
                    }
                    else if(sessionStorage.User_Type_Id=='7'){
                        $('#savebtn').show();
                        $('#verifybtn').hide();
                        $('.visibleControls').attr('disabled',true);
                        $('.visible').attr('disabled',false);
                    }
                }
                   
               
         if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            $('#savebtn,#verifybtn').hide();
            $('.visibleControls,.visible').attr('disabled',true);
         }
            }
      else{
               sessionStorage.Es25Flag='1';
               resetMode();
               if(sessionStorage.User_Type_Id=='7'){
                $('.visibleControls').attr('disabled',true);
                $('.visible').attr('disabled',false);
                $('#savebtn').show();
            }
            else if(sessionStorage.User_Type_Id=='2'){
                $('.visibleControls').attr('disabled',true);
                $('.visible').attr('disabled',false);
               $('#verifybtn').show();
               $('#savebtn').show();
           }
           if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            $('#savebtn,#verifybtn').hide();
            $('.visibleControls,.visible').attr('disabled',true);
         }
            
      }
  }
 else if(sessionStorage.Es25Flag=='1'){
     
    resetMode();
  toastr.success('Added Successfully');

}
 else  if(sessionStorage.Es25Flag=='2'){
     
    resetMode();
  toastr.success('Modified  Successfully');
  
}   
else if(sessionStorage.Es25Flag=='4'){
    
    resetMode();
    toastr.success('Records Successfully Send To Head Office');
    
  } 

     }

     function visibilityControls(isDisabled){
         $('.visibleControls').attr('disabled',isDisabled);
         $('.btntbl').hide();
         $('#savebtn').show();

     }

     function resetMode(){
        $("#Registration_Blind").val(""),
        $("#PlacesJs_CG_Blind").val(""),
        $("#PlacesJs_UT_Blind").val(""),
        $("#PlacesJs_SG_Blind").val(""),
        $("#PlacesJs_QG_Blind").val(""),
        $("#PlacesJs_LB_Blind").val(""),
        $("#PlacesJs_Private_Blind").val(""),
        $("#PlacesJs_Total_Blind").val(""),
        $("#Registration_Removed_Blind ").val(""),
        $("#LR_Blind").val(""),
        $("#Submissions_Blind").val(""),
        $("#LR_Prev_1_Blind").val(""),

        $("#Registration_Deaf").val(""),
        $("#PlacesJs_CG_Deaf").val(""),
        $("#PlacesJs_UT_Deaf").val(""),
        $("#PlacesJs_SG_Deaf").val(""),
        $("#PlacesJs_QG_Deaf").val(""),
        $("#PlacesJs_LB_Deaf").val(""),
        $("#PlacesJs_Private_Deaf").val(""),
        $("#PlacesJs_Total_Deaf").val(""),
        $("#Registration_Removed_Deaf ").val(""),
        $("#LR_Deaf").val(""),
        $("#Submissions_Deaf").val(""),
        $("#LR_Prev_1_Deaf").val(""),

        $("#Registration_Orthopaedics").val(""),
        $("#PlacesJs_CG_Orthopaedics").val(""),
        $("#PlacesJs_UT_Orthopaedics").val(""),
        $("#PlacesJs_SG_Orthopaedics").val(""),
        $("#PlacesJs_QG_Orthopaedics").val(""),
        $("#PlacesJs_LB_Orthopaedics").val(""),
        $("#PlacesJs_Private_Orthopaedics").val(""),
        $("#PlacesJs_Total_Orthopaedics").val(""),
        $("#Registration_Removed_Orthopaedics ").val(""),
        $("#LR_Orthopaedics").val(""),
        $("#Submissions_Orthopaedics").val(""),
        $("#LR_Prev_1_Orthopaedics").val(""),

        $("#Registration_Respiratory").val(""),
        $("#PlacesJs_CG_Respiratory").val(""),
        $("#PlacesJs_UT_Respiratory").val(""),
        $("#PlacesJs_SG_Respiratory").val(""),
        $("#PlacesJs_QG_Respiratory").val(""),
        $("#PlacesJs_LB_Respiratory").val(""),
        $("#PlacesJs_Private_Respiratory").val(""),
        $("#PlacesJs_Total_Respiratory").val(""),
        $("#Registration_Removed_Respiratory ").val(""),
        $("#LR_Respiratory").val(""),
        $("#Submissions_Respiratory").val(""),
        $("#LR_Prev_1_Respiratory").val(""),

        $("#Registration_Leprosy").val(""),
        $("#PlacesJs_CG_Leprosy").val(""),
        $("#PlacesJs_UT_Leprosy").val(""),
        $("#PlacesJs_SG_Leprosy").val(""),
        $("#PlacesJs_QG_Leprosy").val(""),
        $("#PlacesJs_LB_Leprosy").val(""),
        $("#PlacesJs_Private_Leprosy").val(""),
        $("#PlacesJs_Total_Leprosy").val(""),
        $("#Registration_Removed_Leprosy ").val(""),
        $("#LR_Leprosy").val(""),
        $("#Submissions_Leprosy").val(""),
        $("#LR_Prev_1_Leprosy").val(""),

        $("#Registration_Total").val(""),
        $("#PlacesJs_CG_Total").val(""),
        $("#PlacesJs_UT_Total").val(""),
        $("#PlacesJs_SG_Total").val(""),
        $("#PlacesJs_QG_Total").val(""),
        $("#PlacesJs_LB_Total").val(""),
        $("#PlacesJs_Private_Total").val(""),
        $("#PlacesJs_Total_Total").val(""),
        $("#Registration_Removed_Total ").val(""),
        $("#LR_Total").val(""),
        $("#Submissions_Total").val(""),
        $("#LR_Prev_1_Total").val(""),

        $("#Registration_W").val(""),
        $("#PlacesJs_CG_W").val(""),
        $("#PlacesJs_UT_W").val(""),
        $("#PlacesJs_SG_W").val(""),
        $("#PlacesJs_QG_W").val(""),
        $("#PlacesJs_LB_W").val(""),
        $("#PlacesJs_Private_W").val(""),
        $("#PlacesJs_W_W").val(""),
        $("#Registration_Removed_W ").val(""),
        $("#LR_W").val(""),
        $("#Submissions_W").val(""),
        $("#LR_Prev_1_W").val("")
     }
function getrowTotal(type){
    var total=0;
    var grandtotal=0;
    var PlacesJs_CG=$(`#PlacesJs_CG_${type}`).val()==''?0:$(`#PlacesJs_CG_${type}`).val();
    var PlacesJs_UT=$(`#PlacesJs_UT_${type}`).val()==''?0:$(`#PlacesJs_UT_${type}`).val();
    var PlacesJs_SG=$(`#PlacesJs_SG_${type}`).val()==''?0:$(`#PlacesJs_SG_${type}`).val();
    var PlacesJs_QG=$(`#PlacesJs_QG_${type}`).val()==''?0:$(`#PlacesJs_QG_${type}`).val();
    var PlacesJs_LB=$(`#PlacesJs_LB_${type}`).val()==''?0:$(`#PlacesJs_LB_${type}`).val();
    var PlacesJs_Private=$(`#PlacesJs_Private_${type}`).val()==''?0:$(`#PlacesJs_Private_${type}`).val();
    total= parseInt(PlacesJs_CG)+parseInt(PlacesJs_UT)+parseInt(PlacesJs_SG)+parseInt(PlacesJs_QG)+parseInt(PlacesJs_LB)+parseInt(PlacesJs_Private)
    var PlacesJs_CG_Total=$(`#PlacesJs_CG_Total`).val()==''?0:$(`#PlacesJs_CG_Total`).val();
    var PlacesJs_UT_Total=$(`#PlacesJs_UT_Total`).val()==''?0:$(`#PlacesJs_UT_Total`).val();
    var PlacesJs_SG_Total=$(`#PlacesJs_SG_Total`).val()==''?0:$(`#PlacesJs_SG_Total`).val();
    var PlacesJs_QG_Total=$(`#PlacesJs_QG_Total`).val()==''?0:$(`#PlacesJs_QG_Total`).val();
    var PlacesJs_LB_Total=$(`#PlacesJs_LB_Total`).val()==''?0:$(`#PlacesJs_LB_Total`).val();
    var PlacesJs_Private_Total=$(`#PlacesJs_Private_Total`).val()==''?0:$(`#PlacesJs_Private_Total`).val();
    grandtotal= parseInt(PlacesJs_CG_Total)+parseInt(PlacesJs_UT_Total)+parseInt(PlacesJs_SG_Total)+parseInt(PlacesJs_QG_Total)+parseInt(PlacesJs_LB_Total)+parseInt(PlacesJs_Private_Total)
   $(`#PlacesJs_Total_Total`).val(grandtotal);
    $(`#PlacesJs_Total_${type}`).val(total);
}
function getcolumnTotal(type){
    total=0;
var Blind=$(`#${type}_Blind`).val()==''?0:$(`#${type}_Blind`).val();
var Deaf=$(`#${type}_Deaf`).val()==''?0:$(`#${type}_Deaf`).val();
var Orthopaedics=$(`#${type}_Orthopaedics`).val()==''?0:$(`#${type}_Orthopaedics`).val();
var Respiratory=$(`#${type}_Respiratory`).val()==''?0:$(`#${type}_Respiratory`).val();
var Leprosy=$(`#${type}_Leprosy`).val()==''?0:$(`#${type}_Leprosy`).val();

total=parseInt(Blind)+parseInt(Deaf)+parseInt(Orthopaedics)+parseInt(Respiratory)+parseInt(Leprosy);
$(`#${type}_Total`).val(total);
}