
function ES25RptP2Submit(Type){
sessionStorage.setItem('type',Type)
    if($("#ChooseReport").val()=='FirstHalf'){
        var fromdate = ''+$("#years").val()+'/01/01'
        var todate = ''+$("#years").val()+'/06/30/'
        }
        else if($("#ChooseReport").val()=='SecondHalf'){
            var fromdate = ''+$("#years").val()+'/07/01/'
            var todate = ''+$("#years").val()+'/12/31/'
        }
        if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
          var ex_id =  $("#ddExchange").val();
            }
          else{
            var ex_id =  sessionStorage.Ex_id;
          }
    var MasterData = {
  
"p_Ex_Id": ex_id,  
"p_FromDt":  fromdate ,  
"p_ToDt":   todate,  
"p_Outstanding_Prev_CG":   $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),  
"p_Outstanding_Prev_UT":  $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val() ,  
"p_Outstanding_Prev_SG":   $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),  
"p_Outstanding_Prev_Quasi_CG":  $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val() ,  
"p_Outstanding_Prev_Quasi_SG":  $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val() ,  
"p_Outstanding_Prev_LB":   $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),  
"p_Outstanding_Prev_Private":  $("#Outstanding_Prev_Private").val()==''?0:$("#Outstanding_Prev_Private").val() ,  
"p_Outstanding_Prev_Total":  $("#Outstanding_Prev_Total").val()==''?0:$("#Outstanding_Prev_Total").val() ,  
"p_Notified_CG":   $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),  
"p_Notified_UT":  $("#Notified_UT").val()==''?0:$("#Notified_UT").val() ,  
"p_Notified_SG":   $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),  
"p_Notified_Quasi_CG":  $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val() ,  
"p_Notified_Quasi_SG":  $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val() ,  
"p_Notified_LB":  $("#Notified_LB").val()==''?0:$("#Notified_LB").val() ,  
"p_Notified_Private":  $("#Notified_Private").val()==''?0:$("#Notified_Private").val() ,  
"p_Notified_Total":  $("#Notified_Total").val()==''?0:$("#Notified_Total").val() ,  
"p_Filled_CG":  $("#Filled_CG").val()==''?0:$("#Filled_CG").val() ,  
"p_Filled_UT":  $("#Filled_UT").val()==''?0:$("#Filled_UT").val() ,  
"p_Filled_SG":  $("#Filled_SG").val()==''?0:$("#Filled_SG").val() ,  
"p_Filled_Quasi_CG":  $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val() ,  
"p_Filled_Quasi_SG":  $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val() ,  
"p_Filled_LB":   $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),  
"p_Filled_Private":  $("#Filled_Private").val()==''?0:$("#Filled_Private").val() ,  
"p_Filled_Total":  $("#Filled_Total").val()==''?0:$("#Filled_Total").val() ,  
"p_Cancelled_NA_CG":  $("#Cancelled_NA_CG").val()==''?0:$("#Cancelled_NA_CG").val() ,  
"p_Cancelled_NA_UT":  $("#Cancelled_NA_UT").val()==''?0:$("#Cancelled_NA_UT").val() ,  
"p_Cancelled_NA_SG":  $("#Cancelled_NA_SG").val()==''?0:$("#Cancelled_NA_SG").val() ,  
"p_Cancelled_NA_Quasi_CG":  $("#Cancelled_NA_Quasi_CG").val()==''?0:$("#Cancelled_NA_Quasi_CG").val() ,  
"p_Cancelled_NA_Quasi_SG":  $("#Cancelled_NA_Quasi_SG").val()==''?0:$("#Cancelled_NA_Quasi_SG").val() ,  
"p_Cancelled_NA_LB":   $("#Cancelled_NA_LB").val()==''?0:$("#Cancelled_NA_LB").val(),  
"p_Cancelled_NA_Private":   $("#Cancelled_NA_Private").val()==''?0:$("#Cancelled_NA_Private").val(),  
"p_Cancelled_NA_Total":  $("#Cancelled_NA_Total").val()==''?0:$("#Cancelled_NA_Total").val() ,  
"p_Cancelled_Other_CG":  $("#Cancelled_Other_CG").val()==''?0:$("#Cancelled_Other_CG").val() ,  
"p_Cancelled_Other_UT":  $("#Cancelled_Other_UT").val()==''?0:$("#Cancelled_Other_UT").val() ,  
"p_Cancelled_Other_SG":  $("#Cancelled_Other_SG").val()==''?0:$("#Cancelled_Other_SG").val() ,  
"p_Cancelled_Other_Quasi_CG":  $("#Cancelled_Other_Quasi_CG").val()==''?0:$("#Cancelled_Other_Quasi_CG").val() ,  
"p_Cancelled_Other_Quasi_SG":   $("#Cancelled_Other_Quasi_SG").val()==''?0:$("#Cancelled_Other_Quasi_SG").val(),  
"p_Cancelled_Other_LB":  $("#Cancelled_Other_LB").val()==''?0:$("#Cancelled_Other_LB").val() ,  
"p_Cancelled_Other_Private":  $("#Cancelled_Other_Private").val()==''?0:$("#Cancelled_Other_Private").val() ,  
"p_Cancelled_Other_Total":  $("#Cancelled_Other_Total").val()==''?0:$("#Cancelled_Other_Total").val() ,  
"p_Outstanding_CG":  $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val() ,  
"p_Outstanding_UT":   $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),  
"p_Outstanding_SG":  $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val() ,  
"p_Outstanding_Quasi_CG":   $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),  
"p_Outstanding_Quasi_SG":  $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val() ,  
"p_Outstanding_LB":  $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val() ,  
"p_Outstanding_Private":   $("#Outstanding_Private").val()==''?0:$("#Outstanding_Private").val(),  
"p_Outstanding_Total":  $("#Outstanding_Total").val()==''?0:$("#Outstanding_Total").val(),    
"p_Flag": Type,

     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "ES25P2Entry";
  securedajaxpost(path,'parsrdataES25RptP2Submit','comment',MasterData,'control')
  }
  
  
  function parsrdataES25RptP2Submit(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES25RptP2Submit(sessionStorage.type);
    }

    // if(data[0][0]==''){
    //   $( ".disable" ).prop( "disabled", true );
    //   $("#btnadd").show();
    //   $("#btnSubmit").show();
    // }
    // else{
    //       $("#btnUpdate").show();
    // }

  if(sessionStorage.type=='1'){
        toastr.success("Insert Successful", "", "success")
$('.disable').val('');
        return true;
  }
   
       else if(sessionStorage.type=='2'){
        toastr.success("Update Successful", "", "success")
$('.disable').val('');
        return true;
  }
  
  else if(sessionStorage.type=='4'){
    toastr.success("Successful Send To Head Office", "", "success")
    $('#btnVerify, #btnUpdate,#btnSubmitt').hide();
    $( ".disable" ).prop( "disabled", true );
    return true;
  }

  else if(sessionStorage.type=='3'){

  
    if(data[0][0]){

     
  //    if(sessionStorage.User_Type_Id=='7'){
  //       $('#btnSubmitt').hide();
  //       $('#btnUpdate').show();
  //   }

  //   if(sessionStorage.User_Type_Id=='2'){
  //     $('#btnUpdate').hide();
  // }

   $("#Outstanding_Prev_CG").val(data[0][0].Outstanding_Prev_CG);
   $("#Outstanding_Prev_UT").val(data[0][0].Outstanding_Prev_UT);
   $("#Outstanding_Prev_SG").val(data[0][0].Outstanding_Prev_SG);
   $("#Outstanding_Prev_Quasi_CG").val(data[0][0].Outstanding_Prev_Quasi_CG);
   $("#Outstanding_Prev_Quasi_SG").val(data[0][0].Outstanding_Prev_Quasi_SG) ; 
   $("#Outstanding_Prev_LB").val(data[0][0].Outstanding_Prev_LB); 
   $("#Outstanding_Prev_Private").val(data[0][0].Outstanding_Prev_Private) ; 
   $("#Outstanding_Prev_Total").val(data[0][0].Outstanding_Prev_Total) ;
   $("#Notified_CG").val(data[0][0].Notified_CG);
     $("#Notified_UT").val(data[0][0].Notified_UT) ;  
      $("#Notified_SG").val(data[0][0].Notified_SG);
     $("#Notified_Quasi_CG").val(data[0][0].Notified_Quasi_CG) ;  
     $("#Notified_Quasi_SG").val(data[0][0].Notified_Quasi_SG) ; 
     $("#Notified_LB").val(data[0][0].Notified_LB) ;
     $("#Notified_Private").val(data[0][0].Notified_Private) ; 
     $("#Notified_Total").val(data[0][0].Notified_Total) ;
     $("#Filled_CG").val(data[0][0].Filled_CG) ;  
     $("#Filled_UT").val(data[0][0].Filled_UT) ;  
     $("#Filled_SG").val(data[0][0].Filled_SG) ;
    $("#Filled_Quasi_CG").val(data[0][0].Filled_Quasi_CG) ; 
     $("#Filled_Quasi_SG").val(data[0][0].Filled_Quasi_SG) ; 
     $("#Filled_LB").val(data[0][0].Filled_LB);
     $("#Filled_Private").val(data[0][0].Filled_Private) ;  
     $("#Filled_Total").val(data[0][0].Filled_Total) ;
     $("#Cancelled_NA_CG").val(data[0][0].Cancelled_NA_CG) ;  
     $("#Cancelled_NA_UT").val(data[0][0].Cancelled_NA_UT) ;  
     $("#Cancelled_NA_SG").val(data[0][0].Cancelled_NA_SG) ;
     $("#Cancelled_NA_Quasi_CG").val(data[0][0].Cancelled_NA_Quasi_CG) ; 
     $("#Cancelled_NA_Quasi_SG").val(data[0][0].Cancelled_NA_Quasi_SG) ;  
      $("#Cancelled_NA_LB").val(data[0][0].Cancelled_NA_LB);
      $("#Cancelled_NA_Private").val(data[0][0].Cancelled_NA_Private);  
     $("#Cancelled_NA_Total").val(data[0][0].Cancelled_NA_Total) ;  
     $("#Cancelled_Other_CG").val(data[0][0].Cancelled_Other_CG) ;  
     $("#Cancelled_Other_UT").val(data[0][0].Cancelled_Other_UT) ;  
     $("#Cancelled_Other_SG").val(data[0][0].Cancelled_Other_SG) ;
     $("#Cancelled_Other_Quasi_CG").val(data[0][0].Cancelled_Other_Quasi_CG) ;  
     $("#Cancelled_Other_Quasi_SG").val(data[0][0].Cancelled_Other_Quasi_SG); 
     $("#Cancelled_Other_LB").val(data[0][0].Cancelled_Other_LB) ;
     $("#Cancelled_Other_Private").val(data[0][0].Cancelled_Other_Private) ;  
     $("#Cancelled_Other_Total").val(data[0][0].Cancelled_Other_Total) ; 
    $("#Outstanding_CG").val(data[0][0].Outstanding_CG) ; 
      $("#Outstanding_UT").val(data[0][0].Outstanding_UT);  
     $("#Outstanding_SG").val(data[0][0].Outstanding_SG) ;
      $("#Outstanding_Quasi_CG").val(data[0][0].Outstanding_Quasi_CG);  
     $("#Outstanding_Quasi_SG").val(data[0][0].Outstanding_Quasi_SG) ; 
     $("#Outstanding_LB").val(data[0][0].Outstanding_LB) ;
      $("#Outstanding_Private").val(data[0][0].Outstanding_Private);  
     $("#Outstanding_Total").val(data[0][0].Outstanding_Total)


     if(data[0][0].Verify_YN=='1'){
      $('#btnVerify, #btnUpdate,#btnSubmitt').hide();
      $( ".disable" ).prop( "disabled", true );
    }
    else if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
      if(sessionStorage.User_Type_Id=='2'){
        $('#btnVerify,#btnUpdate').show();
        $('#btnSubmitt').hide();
        $( ".disable" ).prop( "disabled", false );
      }
      else if(sessionStorage.User_Type_Id=='7'){
        if(data[0][0]){
          $('#btnUpdate').show();
          $('#btnSubmitt').hide();
          $('#btnVerify').hide();
          $( ".disable" ).prop( "disabled", false );
        }
        else{
          $('#btnSubmitt').show();
        }
       
      }
    }
     
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
  $('#btnVerify, #btnUpdate,#btnSubmitt').hide();
  $( ".disable" ).prop( "disabled", true );
}
  
    }

else{
  $( ".disable" ).prop( "disabled", false );
  if(sessionStorage.User_Type_Id=='7'){
    $('#btnSubmitt').show();
    $('#btnVerify').hide();
    $('#btnUpdate').hide();
}
else  if(sessionStorage.User_Type_Id=='2'){
  $('#btnSubmitt').show();
  $('#btnVerify').show();
  $('#btnUpdate').hide();
}
else  if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
  $('#btnSubmitt').hide();
  $('#btnVerify').hide();
  $('#btnUpdate').hide();
  $( ".disable" ).prop( "disabled", true );

}
  }
     }
    }

     function getTotal(type){
       var total=0;
       var CG=$(`#${type}_CG`).val()==''?0:$(`#${type}_CG`).val();
       var UT=$(`#${type}_UT`).val()==''?0:$(`#${type}_UT`).val();
       var SG=$(`#${type}_SG`).val()==''?0:$(`#${type}_SG`).val();
       var Quasi_CG=$(`#${type}_Quasi_CG`).val()==''?0:$(`#${type}_Quasi_CG`).val();
       var Quasi_SG=$(`#${type}_Quasi_SG`).val()==''?0:$(`#${type}_Quasi_SG`).val();
       var LB=$(`#${type}_LB`).val()==''?0:$(`#${type}_LB`).val();
       var Private=$(`#${type}_Private`).val()==''?0:$(`#${type}_Private`).val();

       total=parseInt(CG)+parseInt(UT)+parseInt(SG)+parseInt(Quasi_CG)+parseInt(Quasi_SG)+parseInt(LB)+parseInt(Private);
       
       $(`#${type}_Total`).val(total);
     }