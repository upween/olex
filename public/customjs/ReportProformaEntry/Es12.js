
function FetchES12Entry(flag,Verify_YN){
	sessionStorage.Es12Flag=flag
	sessionStorage.Es12Verify_YN=Verify_YN

	if($('#criteriaddl').val()=='1'){
		FromDt=`${$('#yearses12').val()}-01-01`;
		ToDt=`${$('#yearses12').val()}-06-30`;

		// FromDt=`01/01/${$('#yearses12').val()}`;
		// ToDt=`/30/06/${$('#yearses12').val()}`;

	   printonTblheadYear=`30/06/${$('#yearses12').val()}`;
	}
	else if($('#criteriaddl').val()=='2'){
		FromDt=`${$('#yearses12').val()}-07-01`;
		ToDt=`${$('#yearses12').val()}-12-31`;

		// FromDt=`01/07/${$('#yearses12').val()}`;
		// ToDt=`31/12/${$('#yearses12').val()}`;

		printonTblheadYear=`31/12/${$('#yearses12').val()}`;
	}
	$('#year').text(`${printonTblheadYear}`);

	if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
		var ex_id =  $("#ddExchange").val();
			}
		else{
		  var ex_id =  sessionStorage.Ex_id;
		}
    var MasterData = {
        
        "p_Ex_id":ex_id,
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
		"p_NCO":"0",        
		"p_Women_LR":"0",           
		"p_SC_LR":"0",          
		"p_ST_LR":"0",          
		"p_OBC_LR":"0",          
		"p_Disabled_LR":"0",          
		"p_Total_LR":"0",
       
		"p_Women_Notify":"0",           
		"p_SC_Notify":"0",          
		"p_ST_Notify":"0",          
		"p_OBC_Notify":"0",          
		"p_Disabled_Notify":"0",          
		"p_Total_Notify":"0",

		"p_Women_Filled":"0",           
		"p_SC_Filled":"0",          
		"p_ST_Filled":"0",          
		"p_OBC_Filled":"0",          
		"p_Disabled_Filled":"0",          
		"p_Total_Filled":"0",

		"p_Women_Cancelled":"0",           
		"p_SC_Cancelled":"0",          
		"p_ST_Cancelled":"0",          
		"p_OBC_Cancelled":"0",          
		"p_Disabled_Cancelled":"0",          
		"p_Total_Cancelled":"0",

		"p_Women_Outstanding":"0",           
		"p_SC_Outstanding":"0",          
		"p_ST_Outstanding":"0",          
		"p_OBC_Outstanding":"0",          
		"p_Disabled_Outstanding":"0",          
		"p_Total_Outstanding":"0",

		"p_Verify_YN":Verify_YN,         
		"p_Flag":flag,        
		"p_ES12_Id":'0'
	
	}     
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "ES12_RptEntry";
	securedajaxpost(path,'parsedataES12Rpt','comment',MasterData,'control')   
}

function parsedataES12Rpt(data){
	data = JSON.parse(data);
	var data1=data[0];
	var appenddata="";
	if(sessionStorage.Es12Flag=='6'){
		toastr.success('Verified Sucessfully');
		$('#btnVerify,#btnadd,#btnsave').hide();
		$('.visiblecontrols').attr('disabled', true);
	}
else if(sessionStorage.Es12Flag=='5'){

                        
	$('#tbodyvalue').empty();
	
		if(data1.length>0){
	
// if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
// 	$('#btnVerify,#btnadd,#btnsave').hide();
// 	$('.visiblecontrols').attr('disabled', true);
// }

		

			for (var i = 0; i < data1.length; i++) {
				appenddata+=`<tr class='addrow modify' id="modify_${data1[i].ES12_Id}">
				<td><input type="text" class="NCO" value='${data1[i].NCO}' disabled></td>
				<td><input type="text" class="Total_LR" value='${data1[i].Total_LR}' disabled></td>
				<td><input type="text" class="Women_LR" onfocusout=getTotal('LR','modify_${data1[i].ES12_Id}') value='${data1[i].Women_LR}' disabled></td>
				<td><input type="text" class="SC_LR" onfocusout=getTotal('LR','modify_${data1[i].ES12_Id}') value='${data1[i].SC_LR}' disabled></td>
				<td><input type="text" class="ST_LR" onfocusout=getTotal('LR','modify_${data1[i].ES12_Id}') value='${data1[i].ST_LR}' disabled></td>
				<td><input type="text" class="OBC_LR" onfocusout=getTotal('LR','modify_${data1[i].ES12_Id}') value='${data1[i].OBC_LR}' disabled></td>
				<td><input type="text" class="Disabled_LR" onfocusout='getTotal('LR','modify_${data1[i].ES12_Id}')' value='${data1[i].Disabled_LR}' disabled></td>
				<td><input type="text" class="Total_Notify" value='${data1[i].Total_Notify}' disabled></td>
				<td><input type="text" class="visiblecontrols Women_Notify" onfocusout=getTotal('Notify','modify_${data1[i].ES12_Id}') value='${data1[i].Women_Notify}' ></td>
				<td><input type="text" class="visiblecontrols SC_Notify" onfocusout=getTotal('Notify','modify_${data1[i].ES12_Id}') value='${data1[i].SC_Notify}' ></td>
				<td><input type="text" class="visiblecontrols ST_Notify" onfocusout=getTotal('Notify','modify_${data1[i].ES12_Id}') value='${data1[i].ST_Notify}' ></td>
				<td><input type="text" class="visiblecontrols OBC_Notify" onfocusout=getTotal('Notify','modify_${data1[i].ES12_Id}') value='${data1[i].OBC_Notify}' ></td>
				<td><input type="text" class="visiblecontrols Disabled_Notify" onfocusout=getTotal('Notify','modify_${data1[i].ES12_Id}') value='${data1[i].Disabled_Notify}' ></td>
				<td><input type="text" class="Total_Filled"  value='${data1[i].Total_Filled}' disabled></td>
				<td><input type="text" class="visiblecontrols Women_Filled" onfocusout=getTotal('Filled','modify_${data1[i].ES12_Id}') value='${data1[i].Women_Filled}' ></td>
				<td><input type="text" class="visiblecontrols SC_Filled" onfocusout=getTotal('Filled','modify_${data1[i].ES12_Id}') value='${data1[i].SC_Filled}' ></td>
				<td><input type="text" class="visiblecontrols ST_Filled" onfocusout=getTotal('Filled','modify_${data1[i].ES12_Id}') value='${data1[i].ST_Filled}' ></td>
				<td><input type="text" class="visiblecontrols OBC_Filled" onfocusout=getTotal('Filled','modify_${data1[i].ES12_Id}') value='${data1[i].OBC_Filled}' ></td>
				<td><input type="text" class="visiblecontrols Disabled_Filled" onfocusout=getTotal('Filled','modify_${data1[i].ES12_Id}') value='${data1[i].Disabled_Filled}' ></td>
				<td><input type="text" class="Total_Cancelled" value='${data1[i].Total_Cancelled}' disabled></td>
				<td><input type="text" class="visiblecontrols Women_Cancelled" onfocusout=getTotal('Cancelled','modify_${data1[i].ES12_Id}') value='${data1[i].Women_Cancelled}' ></td>
				<td><input type="text" class="visiblecontrols SC_Cancelled" onfocusout=getTotal('Cancelled','modify_${data1[i].ES12_Id}') value='${data1[i].SC_Cancelled}' ></td>
				<td><input type="text" class="visiblecontrols ST_Cancelled" onfocusout=getTotal('Cancelled','modify_${data1[i].ES12_Id}') value='${data1[i].ST_Cancelled}' ></td>
				<td><input type="text" class="visiblecontrols OBC_Cancelled" onfocusout=getTotal('Cancelled','modify_${data1[i].ES12_Id}') value='${data1[i].OBC_Cancelled}' ></td>
				<td><input type="text" class="visiblecontrols Disabled_Cancelled" onfocusout=getTotal('Cancelled','modify_${data1[i].ES12_Id}') value='${data1[i].Disabled_Cancelled}' ></td>
				<td><input type="text" class="Total_Outstanding" value='${data1[i].Total_Outstanding}' disabled></td>
				<td><input type="text" class="visiblecontrols Women_Outstanding" onfocusout=getTotal('Outstanding','modify_${data1[i].ES12_Id}') value='${data1[i].Women_Outstanding}' ></td>
				<td><input type="text" class="visiblecontrols SC_Outstanding" onfocusout=getTotal('Outstanding','modify_${data1[i].ES12_Id}') value='${data1[i].SC_Outstanding}' ></td>
				<td><input type="text" class="visiblecontrols ST_Outstanding" onfocusout=getTotal('Outstanding','modify_${data1[i].ES12_Id}') value='${data1[i].ST_Outstanding}' ></td>
				<td><input type="text" class="visiblecontrols OBC_Outstanding" onfocusout=getTotal('Outstanding','modify_${data1[i].ES12_Id}') value='${data1[i].OBC_Outstanding}' ></td>
				<td><input type="text" class="visiblecontrols Disabled_Outstanding" onfocusout=getTotal('Outstanding','modify_${data1[i].ES12_Id}') value='${data1[i].Disabled_Outstanding}' ></td>
				<td><button type="button" onclick="DeleteNCODetails(${data1[0].ES12_Id})"   class="btn btn-success visiblecontrols" style="background-color:#716aca;border-color:#716aca;">
				Delete</button></td>
				</tr>`;
	}
	
	$('#tbodyvalue').html(appenddata);
	
	if(data1[0].Verify_YN=='1'){
		$('#btnVerify,#btnadd,#btnsave').hide();
		$('.visiblecontrols').attr('disabled', true);
	
}
else if(data1[0].Verify_YN=='0' || data1[0].Verify_YN==null){
	 if(sessionStorage.User_Type_Id=='2'){
		$('#btnVerify,#btnadd,#btnsave').show();
		$('.visiblecontrols').attr('disabled', false);
	}
	else if(sessionStorage.User_Type_Id=='7'){
		$('#btnadd,#btnsave').show();
		$('#btnVerify').hide();
		$('.visiblecontrols').attr('disabled', false);
	}

	
	else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
		$('#btnVerify,#btnadd,#btnsave').hide();
		$('.visiblecontrols').attr('disabled', true);
	}
}
	

		}
else{
	 if(sessionStorage.User_Type_Id=='7'){
		$('#btnadd,#btnsave').show();
		$('#btnVerify').hide();
		$('.visiblecontrols').attr('disabled', false);
}

}
}

	
	

    

 }

function addBtnClick(){

}

function addNewProductRow() {
    var x = $('.addrow').length;
    var row = `<tr class='addrow insert' id="addrow`+x+`">
			<td><input  type="text" id='NCO`+x+`' class="visiblecontrols NCO" onfocusout="checkAvailabilityNco(`+x+`)"  /></td>
			<td><input type="number" id='Total_LR`+x+`' class="Total_LR" disabled></td>
			<td><input type="number" id='Women_LR`+x+`' class="visiblecontrols Women_LR" onfocusout=getTotal('LR','addrow`+x+`') disabled></td>
			<td><input type="number" id='SC_LR`+x+`' class="visiblecontrols SC_LR" onfocusout=getTotal('LR','addrow`+x+`') disabled></td>
			<td><input type="number" id='ST_LR`+x+`' class="visiblecontrols ST_LR" onfocusout=getTotal('LR','addrow`+x+`') disabled></td>
			<td><input type="number" id='OBC_LR`+x+`' class="visiblecontrols OBC_LR" onfocusout=getTotal('LR','addrow`+x+`') disabled></td>
			<td><input type="number" id='Disabled_LR`+x+`' class="visiblecontrols Disabled_LR" onfocusout=getTotal('LR','addrow`+x+`') disabled></td>
			<td><input type="number" id='Total_Notify`+x+`' class="Total_Notify" disabled></td>
			<td><input type="number" id='Women_Notify`+x+`' class="visiblecontrols Women_Notify" onfocusout=getTotal('Notify','addrow`+x+`')></td>
			<td><input type="number" id='SC_Notify`+x+`' class="visiblecontrols SC_Notify" onfocusout=getTotal('Notify','addrow`+x+`')></td>
			<td><input type="number" id='ST_Notify`+x+`' class="visiblecontrols ST_Notify" onfocusout=getTotal('Notify','addrow`+x+`')></td>
			<td><input type="number" id='OBC_Notify`+x+`' class="visiblecontrols OBC_Notify" onfocusout=getTotal('Notify','addrow`+x+`')></td>
			<td><input type="number" id='Disabled_Notify`+x+`' class="visiblecontrols Disabled_Notify" onfocusout=getTotal('Notify','addrow`+x+`')></td>
			<td><input type="number" id='Total_Filled`+x+`' class="Total_Filled" disabled></td>
			<td><input type="number" id='Women_Filled`+x+`' class="visiblecontrols Women_Filled" onfocusout=getTotal('Filled','addrow`+x+`')></td>
			<td><input type="number" id='SC_Filled`+x+`' class="visiblecontrols SC_Filled" onfocusout=getTotal('Filled','addrow`+x+`')></td>
			<td><input type="number" id='ST_Filled`+x+`' class="visiblecontrols ST_Filled" onfocusout=getTotal('Filled','addrow`+x+`')></td>
			<td><input type="number" id='OBC_Filled`+x+`' class="visiblecontrols OBC_Filled" onfocusout=getTotal('Filled','addrow`+x+`')></td>
			<td><input type="number" id='Disabled_Filled`+x+`' class="visiblecontrols Disabled_Filled" onfocusout=getTotal('Filled','addrow`+x+`')></td>
			<td><input type="number" id='Total_Cancelled`+x+`' class="Total_Cancelled" disabled></td>
			<td><input type="number" id='Women_Cancelled`+x+`' class="visiblecontrols Women_Cancelled" onfocusout=getTotal('Cancelled','addrow`+x+`')></td>
			<td><input type="number" id='SC_Cancelled`+x+`' class="visiblecontrols SC_Cancelled" onfocusout=getTotal('Cancelled','addrow`+x+`')></td>
			<td><input type="number" id='ST_Cancelled`+x+`' class="visiblecontrols ST_Cancelled" onfocusout=getTotal('Cancelled','addrow`+x+`')></td>
			<td><input type="number" id='OBC_Cancelled`+x+`' class="visiblecontrols OBC_Cancelled" onfocusout=getTotal('Cancelled','addrow`+x+`')></td>
			<td><input type="number" id='Disabled_Cancelled`+x+`' class="visiblecontrols Disabled_Cancelled" onfocusout=getTotal('Cancelled','addrow`+x+`')></td>
			<td><input type="number" id='Total_Outstanding`+x+`' class="Total_Outstanding" disabled></td>
			<td><input type="number" id='Women_Outstanding`+x+`' class="visiblecontrols Women_Outstanding" onfocusout=getTotal('Outstanding','addrow`+x+`')></td>
			<td><input type="number" id='SC_Outstanding`+x+`' class="visiblecontrols SC_Outstanding" onfocusout=getTotal('Outstanding','addrow`+x+`')></td>
			<td><input type="number" id='ST_Outstanding`+x+`' class="visiblecontrols ST_Outstanding" onfocusout=getTotal('Outstanding','addrow`+x+`')></td>
			<td><input type="number" id='OBC_Outstanding`+x+`' class="visiblecontrols OBC_Outstanding" onfocusout=getTotal('Outstanding','addrow`+x+`')></td>
			<td><input type="number" id='Disabled_Outstanding`+x+`' class="visiblecontrols Disabled_Outstanding" onfocusout=getTotal('Outstanding','addrow`+x+`')></td>
			<td><button type="button" onclick="$('#addrow`+x+`').remove();"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
			Remove</button></td>
			</tr>`;
    $('#tbodyvalue').append(row);
  
    
}

function checkAvailabilityNco(x){
	sessionStorage.no=x
	var nco=$('#NCO'+x+'').val();
	if($.trim($('#NCO'+x+'').val())!=""){
		var path =  serverpath + "getNCOAvailability/'"+nco+"'";
		ajaxget(path,'parsedataFetchES11Rpt','comment',"control");
	}

}
function parsedataFetchES11Rpt(data){
	data = JSON.parse(data)
	if(data[0][0].ReturnValue=='2'){

		toastr.warning('Enter Correct NCO Code');
   }
   else{
	checkNcoDetails()
   }
  
}
function reset(){

  $('.visiblecontrols').val("");
 // $('.addrow').length==0;   
  window.location='/ES12Report'
}

function insupdES12Entry(){
	if($('#criteriaddl').val()=='1'){
		FromDt=`${$('#yearses12').val()}-01-01`;
		ToDt=`${$('#yearses12').val()}-06-30`;
	   printonTblheadYear=`30/06/${$('#yearses12').val()}`;
	}
	else if($('#criteriaddl').val()=='2'){
		FromDt=`${$('#yearses12').val()}-07-01`;
		ToDt=`${$('#yearses12').val()}-12-31`;
		printonTblheadYear=`31/12/${$('#yearses12').val()}`;
	}
	$("#tbodyvalue tr.addrow").each(function (key) {
		
        var this_row = $(this);
	var rowid=this.id.split('_');
	var es12Id=rowid[1];
	var flag;
	if(es12Id!=undefined){
		 flag='2';
	}
	else{
		flag='1';
		es12Id='0';
	}
        var MasterData = {
        "p_Ex_id":sessionStorage.Ex_id,
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
		"p_NCO":$.trim(this_row.find('td> input.NCO').val())==""?0:$.trim(this_row.find('td> input.NCO').val()),        
		"p_Women_LR":$.trim(this_row.find('td> input.Women_LR').val())==""?0:$.trim(this_row.find('td> input.Women_LR').val()),           
		"p_SC_LR":$.trim(this_row.find('td> input.SC_LR').val())==""?0:$.trim(this_row.find('td> input.SC_LR').val()),          
		"p_ST_LR":$.trim(this_row.find('td> input.ST_LR').val())==""?0:$.trim(this_row.find('td> input.ST_LR').val()),          
		"p_OBC_LR":$.trim(this_row.find('td> input.OBC_LR').val())==""?0:$.trim(this_row.find('td> input.OBC_LR').val()),          
		"p_Disabled_LR":$.trim(this_row.find('td> input.Disabled_LR').val())==""?0:$.trim(this_row.find('td> input.Disabled_LR').val()),          
		"p_Total_LR":$.trim(this_row.find('td> input.Total_LR').val())==""?0:$.trim(this_row.find('td> input.Total_LR').val()),
       
		"p_Women_Notify":$.trim(this_row.find('td> input.Women_Notify').val())==""?0:$.trim(this_row.find('td> input.Women_Notify').val()),           
		"p_SC_Notify":$.trim(this_row.find('td> input.SC_Notify').val())==""?0:$.trim(this_row.find('td> input.SC_Notify').val()),          
		"p_ST_Notify":$.trim(this_row.find('td> input.ST_Notify').val())==""?0:$.trim(this_row.find('td> input.ST_Notify').val()),          
		"p_OBC_Notify":$.trim(this_row.find('td> input.OBC_Notify').val())==""?0:$.trim(this_row.find('td> input.OBC_Notify').val()),          
		"p_Disabled_Notify":$.trim(this_row.find('td> input.Disabled_Notify').val())==""?0:$.trim(this_row.find('td> input.Disabled_Notify').val()),          
		"p_Total_Notify":$.trim(this_row.find('td> input.Total_Notify').val())==""?0:$.trim(this_row.find('td> input.Total_Notify').val()),

		"p_Women_Filled":$.trim(this_row.find('td> input.Women_Filled').val())==""?0:$.trim(this_row.find('td> input.Women_Filled').val()),           
		"p_SC_Filled":$.trim(this_row.find('td> input.SC_Filled').val())==""?0:$.trim(this_row.find('td> input.SC_Filled').val()),          
		"p_ST_Filled":$.trim(this_row.find('td> input.ST_Filled').val())==""?0:$.trim(this_row.find('td> input.ST_Filled').val()),          
		"p_OBC_Filled":$.trim(this_row.find('td> input.OBC_Filled').val())==""?0:$.trim(this_row.find('td> input.OBC_Filled').val()),          
		"p_Disabled_Filled":$.trim(this_row.find('td> input.Disabled_Filled').val())==""?0:$.trim(this_row.find('td> input.Disabled_Filled').val()),          
		"p_Total_Filled":$.trim(this_row.find('td> input.Total_Filled').val())==""?0:$.trim(this_row.find('td> input.Total_Filled').val()),

		"p_Women_Cancelled":$.trim(this_row.find('td> input.Women_Cancelled').val())==""?0:$.trim(this_row.find('td> input.Women_Cancelled').val()),           
		"p_SC_Cancelled":$.trim(this_row.find('td> input.SC_Cancelled').val())==""?0:$.trim(this_row.find('td> input.SC_Cancelled').val()),          
		"p_ST_Cancelled":$.trim(this_row.find('td> input.ST_Cancelled').val())==""?0:$.trim(this_row.find('td> input.ST_Cancelled').val()),          
		"p_OBC_Cancelled":$.trim(this_row.find('td> input.OBC_Cancelled').val())==""?0:$.trim(this_row.find('td> input.OBC_Cancelled').val()),          
		"p_Disabled_Cancelled":$.trim(this_row.find('td> input.Disabled_Cancelled').val())==""?0:$.trim(this_row.find('td> input.Disabled_Cancelled').val()),          
		"p_Total_Cancelled":$.trim(this_row.find('td> input.Total_Cancelled').val())==""?0:$.trim(this_row.find('td> input.Total_Cancelled').val()),

		"p_Women_Outstanding":$.trim(this_row.find('td> input.Women_Outstanding').val())==""?0:$.trim(this_row.find('td> input.Women_Outstanding').val()),           
		"p_SC_Outstanding":$.trim(this_row.find('td> input.SC_Outstanding').val())==""?0:$.trim(this_row.find('td> input.SC_Outstanding').val()),          
		"p_ST_Outstanding":$.trim(this_row.find('td> input.ST_Outstanding').val())==""?0:$.trim(this_row.find('td> input.ST_Outstanding').val()),          
		"p_OBC_Outstanding":$.trim(this_row.find('td> input.OBC_Outstanding').val())==""?0:$.trim(this_row.find('td> input.OBC_Outstanding').val()),          
		"p_Disabled_Outstanding":$.trim(this_row.find('td> input.Disabled_Outstanding').val())==""?0:$.trim(this_row.find('td> input.Disabled_Outstanding').val()),          
		"p_Total_Outstanding":$.trim(this_row.find('td> input.Total_Outstanding').val())==""?0:$.trim(this_row.find('td> input.Total_Outstanding').val()),

		"p_Verify_YN":'0',         
		"p_Flag":flag,        
		"p_ES12_Id":es12Id

        };
        MasterData = JSON.stringify(MasterData)
		var path = serverpath + "ES12_RptEntry";
		securedajaxpost(path,'parsedataES12Rptentry','comment',MasterData,'control');
	}); 
	
	// toastr.success('Submit Successfully');
	// $('#tbodyvalue').empty();
//	reset(); 
}
function parsedataES12Rptentry(data){
	data = JSON.parse(data);
	toastr.success('Submit Successfully');
	$('#tbodyvalue').empty();

	}

	function DeleteNCODetails(ES12_Id){
			if($('#criteriaddl').val()=='1'){
				FromDt=`${$('#yearses12').val()}-01-01`;
				ToDt=`${$('#yearses12').val()}-06-30`;
			   printonTblheadYear=`30/06/${$('#yearses12').val()}`;
			}
			else if($('#criteriaddl').val()=='2'){
				FromDt=`${$('#yearses12').val()}-07-01`;
				ToDt=`${$('#yearses12').val()}-12-31`;
				printonTblheadYear=`31/12/${$('#yearses12').val()}`;
			}
			$('#year').text(`${printonTblheadYear}`);
			var MasterData = {
				
				"p_Ex_id":sessionStorage.Ex_id,
				"p_FromDt":FromDt,
				"p_ToDt":ToDt,
				"p_NCO":"0",        
				"p_Women_LR":"0",           
				"p_SC_LR":"0",          
				"p_ST_LR":"0",          
				"p_OBC_LR":"0",          
				"p_Disabled_LR":"0",          
				"p_Total_LR":"0",
			   
				"p_Women_Notify":"0",           
				"p_SC_Notify":"0",          
				"p_ST_Notify":"0",          
				"p_OBC_Notify":"0",          
				"p_Disabled_Notify":"0",          
				"p_Total_Notify":"0",
		
				"p_Women_Filled":"0",           
				"p_SC_Filled":"0",          
				"p_ST_Filled":"0",          
				"p_OBC_Filled":"0",          
				"p_Disabled_Filled":"0",          
				"p_Total_Filled":"0",
		
				"p_Women_Cancelled":"0",           
				"p_SC_Cancelled":"0",          
				"p_ST_Cancelled":"0",          
				"p_OBC_Cancelled":"0",          
				"p_Disabled_Cancelled":"0",          
				"p_Total_Cancelled":"0",
		
				"p_Women_Outstanding":"0",           
				"p_SC_Outstanding":"0",          
				"p_ST_Outstanding":"0",          
				"p_OBC_Outstanding":"0",          
				"p_Disabled_Outstanding":"0",          
				"p_Total_Outstanding":"0",
		
				"p_Verify_YN":'0',         
				"p_Flag":'3',        
				"p_ES12_Id":ES12_Id
			
			}     
			MasterData = JSON.stringify(MasterData)
			var path = serverpath + "ES12_RptEntry";
			securedajaxpost(path,'parsedataES12Rptdel','comment',MasterData,'control')   
		}
		
		function parsedataES12Rptdel(data){
			data = JSON.parse(data);
			if(data[0][0].ReturnValue=='3'){
				toastr.success('Delete Successfully');
				FetchES12Entry(sessionStorage.Es12Flag,sessionStorage.Es12Verify_YN)
			}
		}

function getTotal(type,id){
	var total=0;
		var this_row=$(`#tbodyvalue tr#${id}`);
		console.log(type);
		var Women=this_row.find(`td input.Women_${type}`).val()==''?0:this_row.find(`td input.Women_${type}`).val();
		var SC=this_row.find(`td input.SC_${type}`).val()==''?0:this_row.find(`td input.SC_${type}`).val();
		var ST=this_row.find(`td input.ST_${type}`).val()==''?0:this_row.find(`td input.ST_${type}`).val();
		var OBC=this_row.find(`td input.OBC_${type}`).val()==''?0:this_row.find(`td input.OBC_${type}`).val();
		var Disabled=this_row.find(`td input.Disabled_${type}`).val()==''?0:this_row.find(`td input.Disabled_${type}`).val();
		 total=parseInt(Women)+parseInt(SC)+parseInt(ST)+parseInt(OBC)+parseInt(Disabled);
		this_row.find(`td input.Total_${type}`).val(total);
		}



		function checkNcoDetails(){
			var nco=$('#NCO'+sessionStorage.no+'').val();
				var path =  serverpath + "getNCO/'"+nco+"'/'"+$('#yearses12').val()+"'";
				ajaxget(path,'parsedatacheckNcoDetails','comment',"control");
		}
		function parsedatacheckNcoDetails(data){
			data = JSON.parse(data)
		$('#Total_LR'+sessionStorage.no+'').val(data[0][0].LR_Total);
		$('#Women_LR'+sessionStorage.no+'').val(data[0][0].LR_W);
		$('#SC_LR'+sessionStorage.no+'').val(data[0][0].LR_SC);
		$('#ST_LR'+sessionStorage.no+'').val(data[0][0].LR_ST);
		$('#OBC_LR'+sessionStorage.no+'').val(data[0][0].LR_OBC);
		$('#Disabled_LR'+sessionStorage.no+'').val(data[0][0].LR_Disabled);
		}