
function ES13Form(Flag){
    var FromDt='';
    var ToDt='';
    var printonTblheadYear='';
    sessionStorage.ES13Flag=Flag;
if($('#criteriaddl').val()=='1'){
    FromDt=`${$('#yearses13').val()}-01-01`;
    ToDt=`${$('#yearses13').val()}-06-30`;
   printonTblheadYear=`30/06/${$('#yearses13').val()}`;
}
else if($('#criteriaddl').val()=='2'){
    FromDt=`${$('#yearses13').val()}-07-01`;
    ToDt=`${$('#yearses13').val()}-12-31`;
    printonTblheadYear=`31/12/${$('#yearses13').val()}`;
}
$('#year').text(`${printonTblheadYear}`);

if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    var ex_id =  $("#ddExchange").val();
        }
    else{
      var ex_id =  sessionStorage.Ex_id;
    }
    var MasterData = {
        
"p_Ex_Id":ex_id,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Outstanding_Prev_CG": $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),
"p_Outstanding_Prev_SG": $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),
"p_Outstanding_Prev_UT": $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val(),
"p_Outstanding_Prev_Quasi_SG": $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val(),
"p_Outstanding_Prev_Quasi_CG": $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val(),
"p_Outstanding_Prev_LB": $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),
"p_Outstanding_Prev_Private_Act": $("#Outstanding_Prev_Private_Act").val()==''?0:$("#Outstanding_Prev_Private_Act").val(),
"p_Outstanding_Prev_Private_NonAct": $("#Outstanding_Prev_Private_NonAct").val()==''?0:$("#Outstanding_Prev_Private_NonAct").val(),
"p_Total_Prev_Outstanding": $("#Total_Prev_Outstanding").val()==''?0:$("#Total_Prev_Outstanding").val(),

"p_Transfer_Receive_CG": $("#Transfer_Receive_CG").val()==''?0:$("#Transfer_Receive_CG").val(),
"p_Transfer_Receive_SG": $("#Transfer_Receive_SG").val()==''?0:$("#Transfer_Receive_SG").val(),
"p_Transfer_Receive_UT": $("#Transfer_Receive_UT").val()==''?0:$("#Transfer_Receive_UT").val(),
"p_Transfer_Receive_Quasi_SG": $("#Transfer_Receive_Quasi_SG").val()==''?0:$("#Transfer_Receive_Quasi_SG").val(),
"p_Transfer_Receive_Quasi_CG": $("#Transfer_Receive_Quasi_CG").val()==''?0:$("#Transfer_Receive_Quasi_CG").val(),
"p_Transfer_Receive_LB": $("#Transfer_Receive_LB").val()==''?0:$("#Transfer_Receive_LB").val(),
"p_Transfer_Receive_Private_Act": $("#Transfer_Receive_Private_Act").val()==''?0:$("#Transfer_Receive_Private_Act").val(),
"p_Transfer_Receive_Private_NonAct": $("#Transfer_Receive_Private_NonAct").val()==''?0:$("#Transfer_Receive_Private_NonAct").val(),
"p_Total_Transfer_Receive": $("#Total_Transfer_Receive").val()==''?0:$("#Total_Transfer_Receive").val(),

"p_Notified_CG": $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),
"p_Notified_SG": $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),
"p_Notified_UT": $("#Notified_UT").val()==''?0:$("#Notified_UT").val(),
"p_Notified_Quasi_SG": $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val(),
"p_Notified_Quasi_CG": $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val(),
"p_Notified_LB": $("#Notified_LB").val()==''?0:$("#Notified_LB").val(),
"p_Notified_Private_Act": $("#Notified_Private_Act").val()==''?0:$("#Notified_Private_Act").val(),
"p_Notified_Private_NonAct": $("#Notified_Private_NonAct").val()==''?0:$("#Notified_Private_NonAct").val(),
"p_Total_Notified": $("#Total_Notified").val()==''?0:$("#Total_Notified").val(),

"p_Total_1_1a_2_CG": $("#Total_1_1a_2_CG").val()==''?0:$("#Total_1_1a_2_CG").val(),
"p_Total_1_1a_2_SG": $("#Total_1_1a_2_SG").val()==''?0:$("#Total_1_1a_2_SG").val(),
"p_Total_1_1a_2_UT": $("#Total_1_1a_2_UT").val()==''?0:$("#Total_1_1a_2_UT").val(),
"p_Total_1_1a_2_Quasi_SG": $("#Total_1_1a_2_Quasi_SG").val()==''?0:$("#Total_1_1a_2_Quasi_SG").val(),
"p_Total_1_1a_2_Quasi_CG": $("#Total_1_1a_2_Quasi_CG").val()==''?0:$("#Total_1_1a_2_Quasi_CG").val(),
"p_Total_1_1a_2_LB": $("#Total_1_1a_2_LB").val()==''?0:$("#Total_1_1a_2_LB").val(),
"p_Total_1_1a_2_Private_Act": $("#Total_1_1a_2_Private_Act").val()==''?0:$("#Total_1_1a_2_Private_Act").val(),
"p_Total_1_1a_2_Private_NonAct": $("#Total_1_1a_2_Private_NonAct").val()==''?0:$("#Total_1_1a_2_Private_NonAct").val(),
"p_Total_Total_1_1a_2": $("#Total_Total_1_1a_2").val()==''?0:$("#Total_Total_1_1a_2").val(),

"p_Filled_CG": $("#Filled_CG").val()==''?0:$("#Filled_CG").val(),
"p_Filled_SG": $("#Filled_SG").val()==''?0:$("#Filled_SG").val(),
"p_Filled_UT": $("#Filled_UT").val()==''?0:$("#Filled_UT").val(),
"p_Filled_Quasi_SG": $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val(),
"p_Filled_Quasi_CG": $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val(),
"p_Filled_LB": $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),
"p_Filled_Private_Act": $("#Filled_Private_Act").val()==''?0:$("#Filled_Private_Act").val(),
"p_Filled_Private_NonAct": $("#Filled_Private_NonAct").val()==''?0:$("#Filled_Private_NonAct").val(),
"p_Total_Filled": $("#Total_Filled").val()==''?0:$("#Total_Filled").val(),

"p_Cancelled_CG": $("#Cancelled_CG").val()==''?0:$("#Cancelled_CG").val(),
"p_Cancelled_SG": $("#Cancelled_SG").val()==''?0:$("#Cancelled_SG").val(),
"p_Cancelled_UT": $("#Cancelled_UT").val()==''?0:$("#Cancelled_UT").val(),
"p_Cancelled_Quasi_SG": $("#Cancelled_Quasi_SG").val()==''?0:$("#Cancelled_Quasi_SG").val(),
"p_Cancelled_Quasi_CG": $("#Cancelled_Quasi_CG").val()==''?0:$("#Cancelled_Quasi_CG").val(),
"p_Cancelled_LB": $("#Cancelled_LB").val()==''?0:$("#Cancelled_LB").val(),
"p_Cancelled_Private_Act": $("#Cancelled_Private_Act").val()==''?0:$("#Cancelled_Private_Act").val(),
"p_Cancelled_Private_NonAct": $("#Cancelled_Private_NonAct").val()==''?0:$("#Cancelled_Private_NonAct").val(),
"p_Total_Cancelled": $("#Total_Cancelled").val()==''?0:$("#Total_Cancelled").val(),

"p_Transfer_CG": $("#Transfer_CG").val()==''?0:$("#Transfer_CG").val(),
"p_Transfer_SG": $("#Transfer_SG").val()==''?0:$("#Transfer_SG").val(),
"p_Transfer_UT": $("#Transfer_UT").val()==''?0:$("#Transfer_UT").val(),
"p_Transfer_Quasi_SG": $("#Transfer_Quasi_SG").val()==''?0:$("#Transfer_Quasi_SG").val(),
"p_Transfer_Quasi_CG": $("#Transfer_Quasi_CG").val()==''?0:$("#Transfer_Quasi_CG").val(),
"p_Transfer_LB": $("#Transfer_LB").val()==''?0:$("#Transfer_LB").val(),
"p_Transfer_Private_Act": $("#Transfer_Private_Act").val()==''?0:$("#Transfer_Private_Act").val(),
"p_Transfer_Private_NonAct": $("#Transfer_Private_NonAct").val()==''?0:$("#Transfer_Private_NonAct").val(),
"p_Total_Transfer": $("#Total_Transfer").val()==''?0:$("#Total_Transfer").val(),

"p_Outstanding_CG": $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val(),
"p_Outstanding_SG": $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val(),
"p_Outstanding_UT": $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),
"p_Outstanding_Quasi_SG": $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val(),
"p_Outstanding_Quasi_CG": $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),
"p_Outstanding_LB": $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val(),
"p_Outstanding_Private_Act": $("#Outstanding_Private_Act").val()==''?0:$("#Outstanding_Private_Act").val(),
"p_Outstanding_Private_NonAct": $("#Outstanding_Private_NonAct").val()==''?0:$("#Outstanding_Private_NonAct").val(),
"p_Total_Outstanding": $("#Total_Outstanding").val()==''?0:$("#Total_Outstanding").val(),

"p_Total_4_5_5a_6_CG": $("#Total_4_5_5a_6_CG").val()==''?0:$("#Total_4_5_5a_6_CG").val(),
"p_Total_4_5_5a_6_SG": $("#Total_4_5_5a_6_SG").val()==''?0:$("#Total_4_5_5a_6_SG").val(),
"p_Total_4_5_5a_6_UT": $("#Total_4_5_5a_6_UT").val()==''?0:$("#Total_4_5_5a_6_UT").val(),
"p_Total_4_5_5a_6_Quasi_SG": $("#Total_4_5_5a_6_Quasi_SG").val()==''?0:$("#Total_4_5_5a_6_Quasi_SG").val(),
"p_Total_4_5_5a_6_Quasi_CG": $("#Total_4_5_5a_6_Quasi_CG").val()==''?0:$("#Total_4_5_5a_6_Quasi_CG").val(),
"p_Total_4_5_5a_6_LB": $("#Total_4_5_5a_6_LB").val()==''?0:$("#Total_4_5_5a_6_LB").val(),
"p_Total_4_5_5a_6_Private_Act": $("#Total_4_5_5a_6_Private_Act").val()==''?0:$("#Total_4_5_5a_6_Private_Act").val(),
"p_Total_4_5_5a_6_Private_NonAct": $("#Total_4_5_5a_6_Private_NonAct").val()==''?0:$("#Total_4_5_5a_6_Private_NonAct").val(),
"p_Total_Total_4_5_5a_6": $("#Total_Total_4_5_5a_6").val()==''?0:$("#Total_Total_4_5_5a_6").val(),


"p_Notified_NotSubmitted_CG": $("#Notified_NotSubmitted_CG").val()==''?0:$("#Notified_NotSubmitted_CG").val(),
"p_Notified_NotSubmitted_SG": $("#Notified_NotSubmitted_SG").val()==''?0:$("#Notified_NotSubmitted_SG").val(),
"p_Notified_NotSubmitted_UT": $("#Notified_NotSubmitted_UT").val()==''?0:$("#Notified_NotSubmitted_UT").val(),
"p_Notified_NotSubmitted_Quasi_SG": $("#Notified_NotSubmitted_Quasi_SG").val()==''?0:$("#Notified_NotSubmitted_Quasi_SG").val(),
"p_Notified_NotSubmitted_Quasi_CG": $("#Notified_NotSubmitted_Quasi_CG").val()==''?0:$("#Notified_NotSubmitted_Quasi_CG").val(),
"p_Notified_NotSubmitted_LB": $("#Notified_NotSubmitted_LB").val()==''?0:$("#Notified_NotSubmitted_LB").val(),
"p_Notified_NotSubmitted_Private_Act": $("#Notified_NotSubmitted_Private_Act").val()==''?0:$("#Notified_NotSubmitted_Private_Act").val(),
"p_Notified_NotSubmitted_Private_NonAct": $("#Notified_NotSubmitted_Private_NonAct").val()==''?0:$("#Notified_NotSubmitted_Private_NonAct").val(),
"p_Total_Notified_NotSubmitted": $("#Total_Notified_NotSubmitted").val()==''?0:$("#Total_Notified_NotSubmitted").val(),


"p_Filled_ByOtherExch_CG": $("#Filled_ByOtherExch_CG").val()==''?0:$("#Filled_ByOtherExch_CG").val(),
"p_Filled_ByOtherExch_SG": $("#Filled_ByOtherExch_SG").val()==''?0:$("#Filled_ByOtherExch_SG").val(),
"p_Filled_ByOtherExch_UT": $("#Filled_ByOtherExch_UT").val()==''?0:$("#Filled_ByOtherExch_UT").val(),
"p_Filled_ByOtherExch_Quasi_SG": $("#Filled_ByOtherExch_Quasi_SG").val()==''?0:$("#Filled_ByOtherExch_Quasi_SG").val(),
"p_Filled_ByOtherExch_Quasi_CG": $("#Filled_ByOtherExch_Quasi_CG").val()==''?0:$("#Filled_ByOtherExch_Quasi_CG").val(),
"p_Filled_ByOtherExch_LB": $("#Filled_ByOtherExch_LB").val()==''?0:$("#Filled_ByOtherExch_LB").val(),
"p_Filled_ByOtherExch_Private_Act": $("#Filled_ByOtherExch_Private_Act").val()==''?0:$("#Filled_ByOtherExch_Private_Act").val(),
"p_Filled_ByOtherExch_Private_NonAct": $("#Filled_ByOtherExch_Private_NonAct").val()==''?0:$("#Filled_ByOtherExch_Private_NonAct").val(),
"p_Total_Filled_ByOtherExch": $("#Total_Filled_ByOtherExch").val()==''?0:$("#Total_Filled_ByOtherExch").val(),

"p_Filled_ForOtherExch_CG": $("#Filled_ForOtherExch_CG").val()==''?0:$("#Filled_ForOtherExch_CG").val(),
"p_Filled_ForOtherExch_SG": $("#Filled_ForOtherExch_SG").val()==''?0:$("#Filled_ForOtherExch_SG").val(),
"p_Filled_ForOtherExch_UT": $("#Filled_ForOtherExch_UT").val()==''?0:$("#Filled_ForOtherExch_UT").val(),
"p_Filled_ForOtherExch_Quasi_SG": $("#Filled_ForOtherExch_Quasi_SG").val()==''?0:$("#Filled_ForOtherExch_Quasi_SG").val(),
"p_Filled_ForOtherExch_Quasi_CG": $("#Filled_ForOtherExch_Quasi_CG").val()==''?0:$("#Filled_ForOtherExch_Quasi_CG").val(),
"p_Filled_ForOtherExch_LB": $("#Filled_ForOtherExch_LB").val()==''?0:$("#Filled_ForOtherExch_LB").val(),
"p_Filled_ForOtherExch_Private_Act": $("#Filled_ForOtherExch_Private_Act").val()==''?0:$("#Filled_ForOtherExch_Private_Act").val(),
"p_Filled_ForOtherExch_Private_NonAct": $("#Filled_ForOtherExch_Private_NonAct").val()==''?0:$("#Filled_ForOtherExch_Private_NonAct").val(),
"p_Total_Filled_ForOtherExch": $("#Total_Filled_ForOtherExch").val()==''?0:$("#Total_Filled_ForOtherExch").val(),

"p_EmpUseExch_CG": $("#EmpUseExch_CG").val()==''?0:$("#EmpUseExch_CG").val(),
"p_EmpUseExch_SG": $("#EmpUseExch_SG").val()==''?0:$("#EmpUseExch_SG").val(),
"p_EmpUseExch_UT": $("#EmpUseExch_UT").val()==''?0:$("#EmpUseExch_UT").val(),
"p_EmpUseExch_Quasi_SG": $("#EmpUseExch_Quasi_SG").val()==''?0:$("#EmpUseExch_Quasi_SG").val(),
"p_EmpUseExch_Quasi_CG": $("#EmpUseExch_Quasi_CG").val()==''?0:$("#EmpUseExch_Quasi_CG").val(),
"p_EmpUseExch_LB": $("#EmpUseExch_LB").val()==''?0:$("#EmpUseExch_LB").val(),
"p_EmpUseExch_Private_Act": $("#EmpUseExch_Private_Act").val()==''?0:$("#EmpUseExch_Private_Act").val(),
"p_EmpUseExch_Private_NonAct": $("#EmpUseExch_Private_NonAct").val()==''?0:$("#EmpUseExch_Private_NonAct").val(),
"p_Total_EmpUseExch": $("#Total_EmpUseExch").val()==''?0:$("#Total_EmpUseExch").val(),

"p_Flag" :Flag,
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "ES13Entry";
  securedajaxpost(path,'parsrdataES13EntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES13EntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES25RptEntryForm();
        
    }
    $('#es25tbleDiv').show();
    
    $('.visibleControls').attr('disabled',false);
  if(sessionStorage.ES13Flag=='3'){
    if(sessionStorage.User_Type_Id=='7' && !data[0][0]){
  
        $('#addbtn').show();
        $('#updatebtn').hide();
        $('#verifybtn').hide();
        $('#savebtn').hide();
      
       
    }
    if(sessionStorage.User_Type_Id=='2'){
        if(data[0][0]){
            $('#updatebtn').hide();
            $('#verifybtn').show();
            $('#addbtn').hide();
            $('#savebtn').show();
        }
        else{
          $('#addbtn').show();
          $('#verifybtn').show();
        }
    }
    
      if(data[0][0]){
    
                sessionStorage.ES13Flag='2'
                $("#Outstanding_Prev_CG").val(data[0][0].Outstanding_Prev_CG),
                $("#Outstanding_Prev_SG").val(data[0][0].Outstanding_Prev_SG),
                $("#Outstanding_Prev_UT").val(data[0][0].Outstanding_Prev_UT),
                $("#Outstanding_Prev_Quasi_SG").val(data[0][0].Outstanding_Prev_Quasi_SG),
                $("#Outstanding_Prev_Quasi_CG").val(data[0][0].Outstanding_Prev_Quasi_CG),
                $("#Outstanding_Prev_LB").val(data[0][0].Outstanding_Prev_LB),
                $("#Outstanding_Prev_Private_Act").val(data[0][0].Outstanding_Prev_Private_Act),
                $("#Outstanding_Prev_Private_NonAct").val(data[0][0].Outstanding_Prev_Private_NonAct),
                $("#Total_Prev_Outstanding").val(data[0][0].Total_Prev_Outstanding),
            
                $("#Transfer_Receive_CG").val(data[0][0].Transfer_Receive_CG),
                $("#Transfer_Receive_SG").val(data[0][0].Transfer_Receive_SG),
                $("#Transfer_Receive_UT").val(data[0][0].Transfer_Receive_UT),
                $("#Transfer_Receive_Quasi_SG").val(data[0][0].Transfer_Receive_Quasi_SG),
                $("#Transfer_Receive_Quasi_CG").val(data[0][0].Transfer_Receive_Quasi_CG),
                $("#Transfer_Receive_LB").val(data[0][0].Transfer_Receive_LB),
                $("#Transfer_Receive_Private_Act").val(data[0][0].Transfer_Receive_Private_Act),
                $("#Transfer_Receive_Private_NonAct").val(data[0][0].Transfer_Receive_Private_NonAct),
                $("#Total_Transfer_Receive").val(data[0][0].Total_Transfer_Receive),
            
                $("#Notified_CG").val(data[0][0].Notified_CG),
                $("#Notified_SG").val(data[0][0].Notified_SG),
                $("#Notified_UT").val(data[0][0].Notified_UT),
                $("#Notified_Quasi_SG").val(data[0][0].Notified_Quasi_SG),
                $("#Notified_Quasi_CG").val(data[0][0].Notified_Quasi_CG),
                $("#Notified_LB").val(data[0][0].Notified_LB),
                $("#Notified_Private_Act").val(data[0][0].Notified_Private_Act),
                $("#Notified_Private_NonAct").val(data[0][0].Notified_Private_NonAct),
                $("#Total_Notified").val(data[0][0].Total_Notified),
            
                $("#Total_1_1a_2_CG").val(data[0][0].Total_1_1a_2_CG),
                $("#Total_1_1a_2_SG").val(data[0][0].Total_1_1a_2_SG),
                $("#Total_1_1a_2_UT").val(data[0][0].Total_1_1a_2_UT),
                $("#Total_1_1a_2_Quasi_SG").val(data[0][0].Total_1_1a_2_Quasi_SG),
                $("#Total_1_1a_2_Quasi_CG").val(data[0][0].Total_1_1a_2_Quasi_CG),
                $("#Total_1_1a_2_LB").val(data[0][0].Total_1_1a_2_LB),
                $("#Total_1_1a_2_Private_Act").val(data[0][0].Total_1_1a_2_Private_Act),
                $("#Total_1_1a_2_Private_NonAct").val(data[0][0].Total_1_1a_2_Private_NonAct),
                $("#Total_Total_1_1a_2").val(data[0][0].Total_Total_1_1a_2),
            
                $("#Filled_CG").val(data[0][0].Filled_CG),
                $("#Filled_SG").val(data[0][0].Filled_SG),
                $("#Filled_UT").val(data[0][0].Filled_UT),
                $("#Filled_Quasi_SG").val(data[0][0].Filled_Quasi_SG),
                $("#Filled_Quasi_CG").val(data[0][0].Filled_Quasi_CG),
                $("#Filled_LB").val(data[0][0].Filled_LB),
                $("#Filled_Private_Act").val(data[0][0].Filled_Private_Act),
                $("#Filled_Private_NonAct").val(data[0][0].Filled_Private_NonAct),
                $("#Total_Filled").val(data[0][0].Total_Filled),
            
                $("#Cancelled_CG").val(data[0][0].Cancelled_CG),
                $("#Cancelled_SG").val(data[0][0].Cancelled_SG),
                $("#Cancelled_UT").val(data[0][0].Cancelled_UT),
                $("#Cancelled_Quasi_SG").val(data[0][0].Cancelled_Quasi_SG),
                $("#Cancelled_Quasi_CG").val(data[0][0].Cancelled_Quasi_CG),
                $("#Cancelled_LB").val(data[0][0].Cancelled_LB),
                $("#Cancelled_Private_Act").val(data[0][0].Cancelled_Private_Act),
                $("#Cancelled_Private_NonAct").val(data[0][0].Cancelled_Private_NonAct),
                $("#Total_Cancelled").val(data[0][0].Total_Cancelled),
            
                $("#Transfer_CG").val(data[0][0].Transfer_CG),
                $("#Transfer_SG").val(data[0][0].Transfer_SG),
                $("#Transfer_UT").val(data[0][0].Transfer_UT),
                $("#Transfer_Quasi_SG").val(data[0][0].Transfer_Quasi_SG),
                $("#Transfer_Quasi_CG").val(data[0][0].Transfer_Quasi_CG),
                $("#Transfer_LB").val(data[0][0].Transfer_LB),
                $("#Transfer_Private_Act").val(data[0][0].Transfer_Private_Act),
                $("#Transfer_Private_NonAct").val(data[0][0].Transfer_Private_NonAct),
                $("#Total_Transfer").val(data[0][0].Total_Transfer),
            
                $("#Outstanding_CG").val(data[0][0].Outstanding_CG),
                $("#Outstanding_SG").val(data[0][0].Outstanding_SG),
                $("#Outstanding_UT").val(data[0][0].Outstanding_UT),
                $("#Outstanding_Quasi_SG").val(data[0][0].Outstanding_Quasi_SG),
                $("#Outstanding_Quasi_CG").val(data[0][0].Outstanding_Quasi_CG),
                $("#Outstanding_LB").val(data[0][0].Outstanding_LB),
                $("#Outstanding_Private_Act").val(data[0][0].Outstanding_Private_Act),
                $("#Outstanding_Private_NonAct").val(data[0][0].Outstanding_Private_NonAct),
                $("#Total_Outstanding").val(data[0][0].Total_Outstanding),
            
                $("#Total_4_5_5a_6_CG").val(data[0][0].Total_4_5_5a_6_CG),
                $("#Total_4_5_5a_6_SG").val(data[0][0].Total_4_5_5a_6_SG),
                $("#Total_4_5_5a_6_UT").val(data[0][0].Total_4_5_5a_6_UT),
                $("#Total_4_5_5a_6_Quasi_SG").val(data[0][0].Total_4_5_5a_6_Quasi_SG),
                $("#Total_4_5_5a_6_Quasi_CG").val(data[0][0].Total_4_5_5a_6_Quasi_CG),
                $("#Total_4_5_5a_6_LB").val(data[0][0].Total_4_5_5a_6_LB),
                $("#Total_4_5_5a_6_Private_Act").val(data[0][0].Total_4_5_5a_6_Private_Act),
                $("#Total_4_5_5a_6_Private_NonAct").val(data[0][0].Total_4_5_5a_6_Private_NonAct),
                $("#Total_Total_4_5_5a_6").val(data[0][0].Total_Total_4_5_5a_6),
            
                $("#Notified_NotSubmitted_CG").val(data[0][0].Notified_NotSubmitted_CG),
                $("#Notified_NotSubmitted_SG").val(data[0][0].Notified_NotSubmitted_SG),
                $("#Notified_NotSubmitted_UT").val(data[0][0].Notified_NotSubmitted_UT),
                $("#Notified_NotSubmitted_Quasi_SG").val(data[0][0].Notified_NotSubmitted_Quasi_SG),
                $("#Notified_NotSubmitted_Quasi_CG").val(data[0][0].Notified_NotSubmitted_Quasi_CG),
                $("#Notified_NotSubmitted_LB").val(data[0][0].Notified_NotSubmitted_LB),
                $("#Notified_NotSubmitted_Private_Act").val(data[0][0].Notified_NotSubmitted_Private_Act),
                $("#Notified_NotSubmitted_Private_NonAct").val(data[0][0].Notified_NotSubmitted_Private_NonAct),
                $("#Total_Notified_NotSubmitted").val(data[0][0].Total_Notified_NotSubmitted),
            
                $("#Filled_ByOtherExch_CG").val(data[0][0].Filled_ByOtherExch_CG),
                $("#Filled_ByOtherExch_SG").val(data[0][0].Filled_ByOtherExch_SG),
                $("#Filled_ByOtherExch_UT").val(data[0][0].Filled_ByOtherExch_UT),
                $("#Filled_ByOtherExch_Quasi_SG").val(data[0][0].Filled_ByOtherExch_Quasi_SG),
                $("#Filled_ByOtherExch_Quasi_CG").val(data[0][0].Filled_ByOtherExch_Quasi_CG),
                $("#Filled_ByOtherExch_LB").val(data[0][0].Filled_ByOtherExch_LB),
                $("#Filled_ByOtherExch_Private_Act").val(data[0][0].Filled_ByOtherExch_Private_Act),
                $("#Filled_ByOtherExch_Private_NonAct").val(data[0][0].Filled_ByOtherExch_Private_NonAct),
                $("#Total_Filled_ByOtherExch").val(data[0][0].Total_Filled_ByOtherExch),
            
                $("#Filled_ForOtherExch_CG").val(data[0][0].Filled_ForOtherExch_CG),
                $("#Filled_ForOtherExch_SG").val(data[0][0].Filled_ForOtherExch_SG),
                $("#Filled_ForOtherExch_UT").val(data[0][0].Filled_ForOtherExch_UT),
                $("#Filled_ForOtherExch_Quasi_SG").val(data[0][0].Filled_ForOtherExch_Quasi_SG),
                $("#Filled_ForOtherExch_Quasi_CG").val(data[0][0].Filled_ForOtherExch_Quasi_CG),
                $("#Filled_ForOtherExch_LB").val(data[0][0].Filled_ForOtherExch_LB),
                $("#Filled_ForOtherExch_Private_Act").val(data[0][0].Filled_ForOtherExch_Private_Act),
                $("#Filled_ForOtherExch_Private_NonAct").val(data[0][0].Filled_ForOtherExch_Private_NonAct),
                $("#Total_Filled_ForOtherExch").val(data[0][0].Total_Filled_ForOtherExch),
            
                $("#EmpUseExch_CG").val(data[0][0].EmpUseExch_CG),
                $("#EmpUseExch_SG").val(data[0][0].EmpUseExch_SG),
                $("#EmpUseExch_UT").val(data[0][0].EmpUseExch_UT),
                $("#EmpUseExch_Quasi_SG").val(data[0][0].EmpUseExch_Quasi_SG),
                $("#EmpUseExch_Quasi_CG").val(data[0][0].EmpUseExch_Quasi_CG),
                $("#EmpUseExch_LB").val(data[0][0].EmpUseExch_LB),
                $("#EmpUseExch_Private_Act").val(data[0][0].EmpUseExch_Private_Act),
                $("#EmpUseExch_Private_NonAct").val(data[0][0].EmpUseExch_Private_NonAct),
                $("#Total_EmpUseExch").val(data[0][0].Total_EmpUseExch)
     
     
                if(data[0][0].Verify_YN=='1'){
                  $('#verifybtn, #savebtn').hide();
                  $('.visibleControls').attr('disabled',true);
                 }
                 else if(data[0][0].Verify_YN=='0'){
                  if(sessionStorage.User_Type_Id=='2'){
                    $('#verifybtn,#addbtn,#savebtn').show();
                    $('.visiblecontrols').attr('disabled', false);
                  }
                  else if(sessionStorage.User_Type_Id=='7'){
                    $('#addbtn,#savebtn').show();
                    $('#verifybtn').hide();
                    $('.visiblecontrols').attr('disabled', false);
                  }
                
                  
                  else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                    $('#verifybtn,#addbtn,#savebtn').hide();
                    $('.visiblecontrols').attr('disabled', true);
                  }
                 }
              }
      else{
               sessionStorage.ES13Flag='1';
               resetMode();
      }
  }
 else if(sessionStorage.ES13Flag=='1'){
     
    resetMode();
  toastr.success('Added Successfully');

}
 else  if(sessionStorage.ES13Flag=='2'){
     
    resetMode();
  toastr.success('Modified  Successfully');
  
}   
else if(sessionStorage.ES13Flag=='4'){
    
    resetMode();
    toastr.success('Records Successfully Send To Head Office');
    $('#verifybtn, #savebtn').hide();
    $('.visibleControls').attr('disabled',true);
  } 

     }

     function visibilityControls(isDisabled){
         $('.visibleControls').attr('disabled',isDisabled);
         $('.btntbl').hide();
         $('#savebtn').show();

     }

     function resetMode(){
        $("#Outstanding_Prev_CG").val(""),
        $("#Outstanding_Prev_SG").val(""),
        $("#Outstanding_Prev_UT").val(""),
        $("#Outstanding_Prev_Quasi_SG").val(""),
        $("#Outstanding_Prev_Quasi_CG").val(""),
        $("#Outstanding_Prev_LB").val(""),
        $("#Outstanding_Prev_Private_Act").val(""),
        $("#Outstanding_Prev_Private_NonAct").val(""),
        $("#Total_Prev_Outstanding").val(""),
    
        $("#Transfer_Receive_CG").val(""),
        $("#Transfer_Receive_SG").val(""),
        $("#Transfer_Receive_UT").val(""),
        $("#Transfer_Receive_Quasi_SG").val(""),
        $("#Transfer_Receive_Quasi_CG").val(""),
        $("#Transfer_Receive_LB").val(""),
        $("#Transfer_Receive_Private_Act").val(""),
        $("#Transfer_Receive_Private_NonAct").val(""),
        $("#Total_Transfer_Receive").val(""),
    
        $("#Notified_CG").val(""),
        $("#Notified_SG").val(""),
        $("#Notified_UT").val(""),
        $("#Notified_Quasi_SG").val(""),
        $("#Notified_Quasi_CG").val(""),
        $("#Notified_LB").val(""),
        $("#Notified_Private_Act").val(""),
        $("#Notified_Private_NonAct").val(""),
        $("#Total_Notified").val(""),
    
        $("#Total_1_1a_2_CG").val(""),
        $("#Total_1_1a_2_SG").val(""),
        $("#Total_1_1a_2_UT").val(""),
        $("#Total_1_1a_2_Quasi_SG").val(""),
        $("#Total_1_1a_2_Quasi_CG").val(""),
        $("#Total_1_1a_2_LB").val(""),
        $("#Total_1_1a_2_Private_Act").val(""),
        $("#Total_1_1a_2_Private_NonAct").val(""),
        $("#Total_Total_1_1a_2").val(""),
    
        $("#Filled_CG").val(""),
        $("#Filled_SG").val(""),
        $("#Filled_UT").val(""),
        $("#Filled_Quasi_SG").val(""),
        $("#Filled_Quasi_CG").val(""),
        $("#Filled_LB").val(""),
        $("#Filled_Private_Act").val(""),
        $("#Filled_Private_NonAct").val(""),
        $("#Total_Filled").val(""),
    
        $("#Cancelled_CG").val(""),
        $("#Cancelled_SG").val(""),
        $("#Cancelled_UT").val(""),
        $("#Cancelled_Quasi_SG").val(""),
        $("#Cancelled_Quasi_CG").val(""),
        $("#Cancelled_LB").val(""),
        $("#Cancelled_Private_Act").val(""),
        $("#Cancelled_Private_NonAct").val(""),
        $("#Total_Cancelled").val(""),
    
        $("#Transfer_CG").val(""),
        $("#Transfer_SG").val(""),
        $("#Transfer_UT").val(""),
        $("#Transfer_Quasi_SG").val(""),
        $("#Transfer_Quasi_CG").val(""),
        $("#Transfer_LB").val(""),
        $("#Transfer_Private_Act").val(""),
        $("#Transfer_Private_NonAct").val(""),
        $("#Total_Transfer").val(""),
    
        $("#Outstanding_CG").val(""),
        $("#Outstanding_SG").val(""),
        $("#Outstanding_UT").val(""),
        $("#Outstanding_Quasi_SG").val(""),
        $("#Outstanding_Quasi_CG").val(""),
        $("#Outstanding_LB").val(""),
        $("#Outstanding_Private_Act").val(""),
        $("#Outstanding_Private_NonAct").val(""),
        $("#Total_Outstanding").val(""),
    
        $("#Total_4_5_5a_6_CG").val(""),
        $("#Total_4_5_5a_6_SG").val(""),
        $("#Total_4_5_5a_6_UT").val(""),
        $("#Total_4_5_5a_6_Quasi_SG").val(""),
        $("#Total_4_5_5a_6_Quasi_CG").val(""),
        $("#Total_4_5_5a_6_LB").val(""),
        $("#Total_4_5_5a_6_Private_Act").val(""),
        $("#Total_4_5_5a_6_Private_NonAct").val(""),
        $("#Total_Total_4_5_5a_6").val(""),
    
        $("#Notified_NotSubmitted_CG").val(""),
        $("#Notified_NotSubmitted_SG").val(""),
        $("#Notified_NotSubmitted_UT").val(""),
        $("#Notified_NotSubmitted_Quasi_SG").val(""),
        $("#Notified_NotSubmitted_Quasi_CG").val(""),
        $("#Notified_NotSubmitted_LB").val(""),
        $("#Notified_NotSubmitted_Private_Act").val(""),
        $("#Notified_NotSubmitted_Private_NonAct").val(""),
        $("#Total_Notified_NotSubmitted").val(""),
    
        $("#Filled_ByOtherExch_CG").val(""),
        $("#Filled_ByOtherExch_SG").val(""),
        $("#Filled_ByOtherExch_UT").val(""),
        $("#Filled_ByOtherExch_Quasi_SG").val(""),
        $("#Filled_ByOtherExch_Quasi_CG").val(""),
        $("#Filled_ByOtherExch_LB").val(""),
        $("#Filled_ByOtherExch_Private_Act").val(""),
        $("#Filled_ByOtherExch_Private_NonAct").val(""),
        $("#Total_Filled_ByOtherExch").val(""),
    
        $("#Filled_ForOtherExch_CG").val(""),
        $("#Filled_ForOtherExch_SG").val(""),
        $("#Filled_ForOtherExch_UT").val(""),
        $("#Filled_ForOtherExch_Quasi_SG").val(""),
        $("#Filled_ForOtherExch_Quasi_CG").val(""),
        $("#Filled_ForOtherExch_LB").val(""),
        $("#Filled_ForOtherExch_Private_Act").val(""),
        $("#Filled_ForOtherExch_Private_NonAct").val(""),
        $("#Total_Filled_ForOtherExch").val(""),
    
        $("#EmpUseExch_CG").val(""),
        $("#EmpUseExch_SG").val(""),
        $("#EmpUseExch_UT").val(""),
        $("#EmpUseExch_Quasi_SG").val(""),
        $("#EmpUseExch_Quasi_CG").val(""),
        $("#EmpUseExch_LB").val(""),
        $("#EmpUseExch_Private_Act").val(""),
        $("#EmpUseExch_Private_NonAct").val(""),
        $("#Total_EmpUseExch").val("")

      
     }

     function getrowTotal(type,totalid){
       var total=0;
       var grandtotal=0;
       if(totalid=='Total_1_1a_2'){
       var Outstanding_Prev=$(`#Outstanding_Prev_${type}`).val()==''?0:$(`#Outstanding_Prev_${type}`).val()
       var Transfer_Receive=$(`#Transfer_Receive_${type}`).val()==''?0:$(`#Transfer_Receive_${type}`).val()
       var Notified=$(`#Notified_${type}`).val()==''?0:$(`#Notified_${type}`).val()
       var totalOutstanding_Prev=$(`#Total_Prev_Outstanding`).val()==''?0:$(`#Total_Prev_Outstanding`).val()
       var totalTransfer_Receive=$(`#Total_Transfer_Receive`).val()==''?0:$(`#Total_Transfer_Receive`).val()
       var totalNotified=$(`#Total_Notified`).val()==''?0:$(`#Total_Notified`).val()
       total=parseInt(Outstanding_Prev)+parseInt(Transfer_Receive)+parseInt(Notified);
       grandtotal=parseInt(totalOutstanding_Prev)+parseInt(totalTransfer_Receive)+parseInt(totalNotified);
      
       $(`#Total_${totalid}`).val(grandtotal);
     }
     if(totalid=='Total_4_5_5a_6'){
      var Filled=$(`#Filled_${type}`).val()==''?0:$(`#Filled_${type}`).val()
      var Cancelled=$(`#Cancelled_${type}`).val()==''?0:$(`#Cancelled_${type}`).val()
      var Transfer=$(`#Transfer_${type}`).val()==''?0:$(`#Transfer_${type}`).val()
      var Outstanding=$(`#Outstanding_${type}`).val()==''?0:$(`#Outstanding_${type}`).val()
      var totalFilled=$(`#Total_Filled`).val()==''?0:$(`#Total_Filled`).val()
      var totalCancelled=$(`#Total_Cancelled`).val()==''?0:$(`#Total_Cancelled`).val()
      var totalTransfer=$(`#Total_Transfer`).val()==''?0:$(`#Total_Transfer`).val()
      var totalOutstanding=$(`#Total_Outstanding`).val()==''?0:$(`#Total_Outstanding`).val()
      grandtotal=parseInt(totalFilled)+parseInt(totalCancelled)+parseInt(totalTransfer)+parseInt(totalOutstanding);
     
      total=parseInt(Filled)+parseInt(Cancelled)+parseInt(Transfer)+parseInt(Outstanding);
      $(`#Total_${totalid}`).val(grandtotal);
    }

     $(`#${totalid}_${type}`).val(total);

     }

     function getColumnTotal(type,totalid){
              var total=0;
              var CG=$(`#${type}_CG`).val()==''?0:$(`#${type}_CG`).val();
              var SG=$(`#${type}_SG`).val()==''?0:$(`#${type}_SG`).val();
              var UT=$(`#${type}_UT`).val()==''?0:$(`#${type}_UT`).val();
              var Quasi_SG=$(`#${type}_Quasi_SG`).val()==''?0:$(`#${type}_Quasi_SG`).val();
              var Quasi_CG=$(`#${type}_Quasi_CG`).val()==''?0:$(`#${type}_Quasi_CG`).val();
              var LB=$(`#${type}_LB`).val()==''?0:$(`#${type}_LB`).val();
              var Private_Act=$(`#${type}_Private_Act`).val()==''?0:$(`#${type}_Private_Act`).val();
              var Private_NonAct=$(`#${type}_Private_NonAct`).val()==''?0:$(`#${type}_Private_NonAct`).val();
              total=parseInt(CG)+parseInt(SG)+parseInt(UT)+parseInt(Quasi_SG)+parseInt(Quasi_CG)+parseInt(LB)+parseInt(Private_Act)+parseInt(Private_NonAct);
              $(`#Total_${totalid}`).val(total);    
     }
