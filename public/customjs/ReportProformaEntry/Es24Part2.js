

function fetchES24Part2(flag,ES24_II_Id){
	sessionStorage.ES24Flag=flag
	if($('#criteriaddl').val()=='1'){
		FromDt=`${$('#yearses24p2').val()}-01-01`;
		ToDt=`${$('#yearses24p2').val()}-06-30`;
	   printonTblheadYear=`30/06/${$('#yearses24p2').val()}`;
	}
	else if($('#criteriaddl').val()=='2'){
		FromDt=`${$('#yearses24p2').val()}-07-01`;
		ToDt=`${$('#yearses24p2').val()}-12-31`;
		printonTblheadYear=`31/12/${$('#yearses24p2').val()}`;
  }
  if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
		var ex_id =  $("#ddExchange").val();
			}
		else{
		  var ex_id =  sessionStorage.Ex_id;
		}
	$('#year').text(`${printonTblheadYear}`);
    var MasterData = {
        
        "p_Ex_Id":ex_id,
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
        "p_ES24_II_Id":ES24_II_Id,
        "p_Outstanding_Prev_SC":"0",
        "p_Outstanding_Prev_ST":"0",
        "p_Outstanding_Prev_OBC":"0",
        "p_Notified_SC":"0",
        "p_Notified_ST":"0",
        "p_Notified_OBC":"0",
        "p_Filled_SC":"0",
        "p_Filled_ST":"0",
        "p_Filled_OBC":"0",
    
        "p_Cancelled_NA_SC":"0",
        "p_Cancelled_NA_ST":"0",
        "p_Cancelled_NA_OBC":"0",
    
        "p_Cancelled_Other_SC":"0",
        "p_Cancelled_Other_ST":"0",
        "p_Cancelled_Other_OBC":"0",
    
        "p_Outstanding_SC":"0",
        "p_Outstanding_ST":"0",
        "p_Outstanding_OBC":"0",
    
        "p_Flag":flag
  }
  MasterData = JSON.stringify(MasterData)
	var path = serverpath + "es24p2RptEntry";
	securedajaxpost(path,'parsedatafetchES24Part2','comment',MasterData,'control') 
}
  function parsedatafetchES24Part2(data){  
      data = JSON.parse(data);
      var appendata='';
      var data1 = data[0];
      if(	sessionStorage.ES24Flag=='4'){
        toastr.success('Successfully Send To Head Office');
        $( ".disable" ).prop( "disabled", true );
        $('#btnverify, #Save').hide();
      }
      else if(sessionStorage.ES24Flag=='3'){
        if(data1.length>0){
        
       
        for (var i = 0; i < data1.length; i++) {
          appendata+=`	<tr class="tbl_row" id="${data1[i].Flag}_${i}">
          <td class="ES24_II_Id">${data1[i].ES24_II_Id}</td>
          <td>${data1[i].Type_Of_Establishment}</td>
  
          <td><input type="text"   onfocusout="getTotal('Outstanding_Prev_SC')"  class="Outstanding_Prev_SC disable" value='${data1[i].Outstanding_Prev_SC}' style="    width: 40px;" />
          </td>
          <td ><input type="text"     onfocusout="getTotal('Outstanding_Prev_ST')" class="Outstanding_Prev_ST disable" value='${data1[i].Outstanding_Prev_ST}' style="    width: 40px;" >
          </td>
          <td><input type="text"     onfocusout="getTotal('Outstanding_Prev_OBC')" class="Outstanding_Prev_OBC disable" value='${data1[i].Outstanding_Prev_OBC}' style="    width: 40px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Notified_SC')" class="Notified_SC disable" value='${data1[i].Notified_SC}'   style="    width: 40px;" > 
          </td>
          <td><input type="text"     onfocusout="getTotal('Notified_ST')" class="Notified_ST disable" value='${data1[i].Notified_ST}'   style="    width: 40px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Notified_OBC')" class="Notified_OBC disable" value='${data1[i].Notified_OBC}'   style="    width: 40px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Filled_SC')" class="Filled_SC disable" value='${data1[i].Filled_SC}'   style="    width: 40px;" > 
          </td>
          	<td ><input type="text"     onfocusout="getTotal('Filled_ST')" class="Filled_ST disable" value='${data1[i].Filled_ST}'   style="    width: 40px;" > 
          </td>
          <td><input type="text"     onfocusout="getTotal('Filled_OBC')" class="Filled_OBC disable" value='${data1[i].Filled_OBC}'   style="    width: 40px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Cancelled_NA_SC')" class="Cancelled_NA_SC disable" value='${data1[i].Cancelled_NA_SC}'   style="    width: 40px;" > 
          </td>
          <td><input type="text"     onfocusout="getTotal('Cancelled_NA_ST')" class="Cancelled_NA_ST disable" value='${data1[i].Cancelled_NA_ST}'   style="    width: 50px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Cancelled_NA_OBC')" class="Cancelled_NA_OBC disable" value='${data1[i].Cancelled_NA_OBC}'   style="    width: 50px;" > 
          </td>
          <td><input type="text"     onfocusout="getTotal('Cancelled_Other_SC')" class="Cancelled_Other_SC disable" value='${data1[i].Cancelled_Other_SC}'   style="    width: 50px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Cancelled_Other_ST')" class="Cancelled_Other_ST disable" value='${data1[i].Cancelled_Other_ST}'   style="    width: 50px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Cancelled_Other_OBC')" class="Cancelled_Other_OBC disable" value='${data1[i].Cancelled_Other_OBC}'   style="    width: 50px;" > 
          </td>	<td ><input type="text"     onfocusout="getTotal('Outstanding_SC')" class="Outstanding_SC disable" value='${data1[i].Outstanding_SC}'   style="    width: 50px;" > 
          </td>
          <td ><input type="text"     onfocusout="getTotal('Outstanding_ST')" class="Outstanding_ST disable" value='${data1[i].Outstanding_ST}'   style="    width: 50px;" > 
          </td>
          <td><input type="text"     onfocusout="getTotal('Outstanding_OBC')" class="Outstanding_OBC disable" value='${data1[i].Outstanding_OBC}'   style="    width: 50px;" > 
          </td>
     
  </td>
      </tr>`
        }
      }
      $('#tbodyvalue').html(appendata);

        if(data[0][0].Verify_YN=='1'){
          $( ".disable" ).prop( "disabled", true );
          $('#btnverify, #Save').hide();
      }
      else if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
               if(sessionStorage.User_Type_Id=='2'){
                $( ".disable" ).prop( "disabled", false );
                $('#Save, #btnverify').show();
            }
            else if(sessionStorage.User_Type_Id=='7'){
              $( ".disable" ).prop( "disabled", false );
              $('#btnverify').hide();
              $('#Save').show();
          }
    }

    if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
      $( ".disable" ).prop( "disabled", true );
      $('#Save,#btnVerify').hide();
    }
     
     
    }

  }

  function insupdES24P2Entry(){
    if($('#criteriaddl').val()=='1'){
      FromDt=`${$('#yearses24p2').val()}-01-01`;
      ToDt=`${$('#yearses24p2').val()}-06-30`;
       printonTblheadYear=`30/06/${$('#yearses24p2').val()}`;
    }
    else if($('#criteriaddl').val()=='2'){
      FromDt=`${$('#yearses24p2').val()}-07-01`;
      ToDt=`${$('#yearses24p2').val()}-12-31`;
      printonTblheadYear=`31/12/${$('#yearses24p2').val()}`;
    }
    $("#tableglobal tr.tbl_row").each(function (key) {
      
          var this_row = $(this);
    var rowid=this.id.split('_');
    var type=rowid[0];
    var flag;
    console.log(rowid.class);
    if(type=='Add'){
       flag='1';
    }
    else{
      flag='2';
    }
          var MasterData = {
        
     "p_Ex_Id":sessionStorage.Ex_id,
      "p_FromDt":FromDt,
      "p_ToDt":ToDt,
      "p_ES24_II_Id":$.trim(this_row.find('td.ES24_II_Id').text())==""?0:$.trim(this_row.find('td.ES24_II_Id').text()),
      "p_Outstanding_Prev_SC":$.trim(this_row.find('td> input.Outstanding_Prev_SC').val())==""?0:$.trim(this_row.find('td> input.Outstanding_Prev_SC').val()),
      "p_Outstanding_Prev_ST":$.trim(this_row.find('td> input.Outstanding_Prev_ST').val())==""?0:$.trim(this_row.find('td> input.Outstanding_Prev_ST').val()),
      "p_Outstanding_Prev_OBC":$.trim(this_row.find('td> input.Outstanding_Prev_OBC').val())==""?0:$.trim(this_row.find('td> input.Outstanding_Prev_OBC').val()),
      "p_Notified_SC":$.trim(this_row.find('td> input.Notified_SC').val())==""?0:$.trim(this_row.find('td> input.Notified_SC').val()),
      "p_Notified_ST":$.trim(this_row.find('td> input.Notified_ST').val())==""?0:$.trim(this_row.find('td> input.Notified_ST').val()),
      "p_Notified_OBC":$.trim(this_row.find('td> input.Notified_OBC').val())==""?0:$.trim(this_row.find('td> input.Notified_OBC').val()),
      "p_Filled_SC":$.trim(this_row.find('td> input.Filled_SC').val())==""?0:$.trim(this_row.find('td> input.Filled_SC').val()),
      "p_Filled_ST":$.trim(this_row.find('td> input.Filled_ST').val())==""?0:$.trim(this_row.find('td> input.Filled_ST').val()),
      "p_Filled_OBC":$.trim(this_row.find('td> input.Filled_OBC').val())==""?0:$.trim(this_row.find('td> input.Filled_OBC').val()),
  
      "p_Cancelled_NA_SC":$.trim(this_row.find('td> input.Cancelled_NA_SC').val())==""?0:$.trim(this_row.find('td> input.Cancelled_NA_SC').val()),
      "p_Cancelled_NA_ST":$.trim(this_row.find('td> input.Cancelled_NA_ST').val())==""?0:$.trim(this_row.find('td> input.Cancelled_NA_ST').val()),
      "p_Cancelled_NA_OBC":$.trim(this_row.find('td> input.Cancelled_NA_OBC').val())==""?0:$.trim(this_row.find('td> input.Cancelled_NA_OBC').val()),
  
      "p_Cancelled_Other_SC":$.trim(this_row.find('td> input.Cancelled_Other_SC').val())==""?0:$.trim(this_row.find('td> input.Cancelled_Other_SC').val()),
      "p_Cancelled_Other_ST":$.trim(this_row.find('td> input.Cancelled_Other_ST').val())==""?0:$.trim(this_row.find('td> input.Cancelled_Other_ST').val()),
      "p_Cancelled_Other_OBC":$.trim(this_row.find('td> input.Cancelled_Other_OBC').val())==""?0:$.trim(this_row.find('td> input.Cancelled_Other_OBC').val()),
  
      "p_Outstanding_SC":$.trim(this_row.find('td> input.Outstanding_SC').val())==""?0:$.trim(this_row.find('td> input.Outstanding_SC').val()),
      "p_Outstanding_ST":$.trim(this_row.find('td> input.Outstanding_ST').val())==""?0:$.trim(this_row.find('td> input.Outstanding_ST').val()),
      "p_Outstanding_OBC":$.trim(this_row.find('td> input.Outstanding_OBC').val())==""?0:$.trim(this_row.find('td> input.Outstanding_OBC').val()),
  
      "p_Flag":flag
  
          };
           MasterData = JSON.stringify(MasterData)
      var path = serverpath + "es24p2RptEntry";
      securedajaxpost(path,'parsedataES24Rptentry','comment',MasterData,'control');
     }); 
    // reset(); 
  }
  function parsedataES24Rptentry(data){
    data = JSON.parse(data);
    console.log(data);
    var data1=data[0];
    toastr.success('Saved Successfully');
  }

  function getTotal(type){
    var total=0;
    
    $("#tableglobal tr.tbl_row").each(function (key) {
      var this_row = $(this);
      if(key<7){
        total+=parseInt(this_row.find(`td> input.${type}`).val());
      }
      else{

        this_row.find(`td> input.${type}`).val(total);
      }
    });

    
      }