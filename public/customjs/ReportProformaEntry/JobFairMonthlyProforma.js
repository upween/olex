function FillCCMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCCMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCCMonth('parsedatasecuredFillCCMonth',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}


function addNewProductRow() {
    var x = $('.addrow').length;
    var row = `<tr class='addrow insert' id="addrow`+x+`">
			<td> `+[x+1]+` </td>
			<td><input style="width: 90px;height: 27px;" type="text" id="m_datepicker_1" autocomplete="off" class="visiblecontrols JF_Date datepicker" ></td>
			<td><input style="width: 160px;" type="text" class="visiblecontrols Company_Name" ></td>
			<td><input style="width: 100px;" type="text" class="visiblecontrols Post_Name" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Selected_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Female_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols SC_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols ST_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols OBC_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols PH_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Minority_Candidates" ></td>
			<td><input style="width: 70px;" type="number" class="visiblecontrols Remark" ></td>
			<td><input style="width: 70px;" type="text" class="visiblecontrols UR_Candidates" ></td>
			<td><button type="button" onclick="$('#addrow`+x+`').remove();"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
			Remove</button></td>
			</tr>`;
    $('#tbodyvalue').append(row);
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    })
}



function jobfairmonthlyreporting(){

    $("#tableglobal tr.addrow").each(function (key) {
        var this_row = $(this);
        var rowid=this.id.split('_');
        if(rowid[0]=='modify'){
var flag=3;
        }
        else{
var flag=2
        }

        if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            var ex_id =  $("#ddExchange").val();
                }
            else{
              var ex_id =  sessionStorage.Ex_id;
            }

    var MasterData = {  
        "p_Flag":flag,
        "p_Xmonth":jQuery("#ddlMonth").val(),
        "p_Xyear":jQuery("#ddlYear").val(),
        "p_Ex_Id":ex_id,
        "p_JF_Id":'0',
        "p_JF_Date":$.trim(this_row.find('td> input.JF_Date').val())==""?0:$.trim(this_row.find('td> input.JF_Date').val()),
        "p_Company_Name":$.trim(this_row.find('td> input.Company_Name').val())==""?0:$.trim(this_row.find('td> input.Company_Name').val()),
        "p_Post_Name":$.trim(this_row.find('td> input.Post_Name').val())==""?0:$.trim(this_row.find('td> input.Post_Name').val()),
        "p_Selected_Candidates":$.trim(this_row.find('td> input.Selected_Candidates').val())==""?0:$.trim(this_row.find('td> input.Selected_Candidates').val()),
        "p_Female_Candidates":$.trim(this_row.find('td> input.Female_Candidates').val())==""?0:$.trim(this_row.find('td> input.Female_Candidates').val()),
        "p_SC_Candidates":$.trim(this_row.find('td> input.SC_Candidates').val())==""?0:$.trim(this_row.find('td> input.SC_Candidates').val()),
        "p_ST_Candidates":$.trim(this_row.find('td> input.ST_Candidates').val())==""?0:$.trim(this_row.find('td> input.ST_Candidates').val()),
        "p_OBC_Candidates":$.trim(this_row.find('td> input.OBC_Candidates').val())==""?0:$.trim(this_row.find('td> input.OBC_Candidates').val()),
        "p_PH_Candidates":$.trim(this_row.find('td> input.PH_Candidates').val())==""?0:$.trim(this_row.find('td> input.PH_Candidates').val()),
        "p_Minority_Candidates":$.trim(this_row.find('td> input.Minority_Candidates').val())==""?0:$.trim(this_row.find('td> input.Minority_Candidates').val()),
        "p_Remark":$.trim(this_row.find('td> input.Remark').val())==""?0:$.trim(this_row.find('td> input.Remark').val()),
        "p_UR_Candidates":$.trim(this_row.find('td> input.UR_Candidates').val())==""?0:$.trim(this_row.find('td> input.UR_Candidates').val()),
        "p_Client_IP":sessionStorage.ipAddress,
        "p_LM_BY":'0',
        "p_Status":'0'
    };
  
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "jobfairmonthlyreporting";
    ajaxpost(path, 'parsedatajobfairmonthlyreporting', 'comment', MasterData, 'control')
    });
}
function parsedatajobfairmonthlyreporting(data){  
data = JSON.parse(data)
reset();
}
 

function reset(){
    $('.visiblecontrols').val("");
    $('#tbodyvalue').empty();
}



function Fetchjobfairmonthlyreporting(Flag){
    sessionStorage.setItem('Flag',Flag)
        
            if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                var ex_id =  $("#ddExchange").val();
                    }
                else{
                  var ex_id =  sessionStorage.Ex_id;
                }
    
        var MasterData = {  
            "p_Flag":Flag,
            "p_Xmonth":jQuery("#ddlMonth").val(),
            "p_Xyear":jQuery("#ddlYear").val(),
            "p_Ex_Id":ex_id,
            "p_JF_Id":'0',
            "p_JF_Date":'1990/01/01',
            "p_Company_Name":'0',
            "p_Post_Name":'0',
            "p_Selected_Candidates":'0',
            "p_Female_Candidates":'0',
            "p_SC_Candidates":'0',
            "p_ST_Candidates":'0',
            "p_OBC_Candidates":'0',
            "p_PH_Candidates":'0',
            "p_Minority_Candidates":'0',
            "p_Remark":'0',
            "p_UR_Candidates":'0',
            "p_Client_IP":'0',
            "p_LM_BY":'0',
            "p_Status":'0'
        };
      
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "jobfairmonthlyreporting";
        ajaxpost(path, 'parsedataFetchjobfairmonthlyreporting', 'comment', MasterData, 'control')
      
    }
    function parsedataFetchjobfairmonthlyreporting(data){  
    data = JSON.parse(data)
    

    var appenddata="";
    var data1 = data[0]; 

    if(sessionStorage.Flag=='4'){
        toastr.success('Successful Send To Head Office');
        $('.visiblecontrols').attr( "disabled", true );
        $("#btnsave, #btnsendtoho").hide(); 
    }

   
        else if(sessionStorage.Flag=='1'){
            if(data1.length>0){
       
    

  

    for (var i = 0; i < data1.length; i++) {
        var date=data1[i].JF_Date.split('-');
        var JF_Date=date[0]+"/"+date[1]+"/"+date[2]
        var d1=date[2].split('')
        var d2=d1[0]+''+d1[1]
        var JF_Date1=date[1]+"/"+d2+"/"+date[0]
        
appenddata += `<tr class='addrow insert' id="modify_${i}">
<td> `+[i+1]+` </td>
<td><input style="width: 90px;height: 27px;" type="text" id="m_datepicker_1" autocomplete="off" class="visiblecontrols JF_Date datepicker" value='` +JF_Date1+ `' ></td>
<td><input style="width: 160px;" type="text" class="visiblecontrols Company_Name" value='`+ data1[i].Company_Name+`' ></td>
<td><input style="width: 100px;" type="text" class="visiblecontrols Post_Name" value='`+ data1[i].Post_Name+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols Selected_Candidates" value='`+data1[i].Selected_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols Female_Candidates" value='`+data1[i].Female_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols SC_Candidates" value='`+data1[i].UR_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols ST_Candidates" value='`+data1[i].SC_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols OBC_Candidates" value='`+data1[i].ST_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols PH_Candidates" value='`+data1[i].OBC_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols Minority_Candidates" value='`+data1[i].PH_Candidates+`'></td>
<td><input style="width: 70px;" type="number" class="visiblecontrols Remark" value='`+data1[i].Minority_Candidates+`'></td>
<td><input style="width: 70px;" type="text" class="visiblecontrols UR_Candidates" value='`+data1[i].Remark+`'></td>
<td><button type="button" onclick="Deletejobfairmonthlyreporting(`+data1[i].JF_Id+`)"   class="btn btn-success visiblecontrols" style="background-color:#716aca;border-color:#716aca;">
Delete</button></td>
</tr>`
}
    jQuery("#tbodyvalue").html(appenddata);  
    

    if(data1[0].Verify_YN=='1'){
        $('.visiblecontrols').attr( "disabled", true );
        $("#btnsave, #btnsendtoho").hide();
    }
    else  if(data1[0].Verify_YN=='0' || data1[0].Verify_YN==null){
        if(sessionStorage.User_Type_Id=='2'){
            $("#btnsave, #btnsendtoho").show();
            $('.visiblecontrols').attr( "disabled", false );
        }
        else   if(sessionStorage.User_Type_Id=='7'){
            $("#btnsendtoho").hide();
            $("#btnsave").show();
            $('.visiblecontrols').attr( "disabled", false );
        }
    }
    
if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    $('.visiblecontrols').attr( "disabled", true );
        $("#btnsave, #btnsendtoho").hide(); 
}
else{
if(sessionStorage.User_Type_Id=='2'){
    $("#btnsave, #btnsendtoho").show();
    $('.visiblecontrols').attr( "disabled", false );
}
else if(sessionStorage.User_Type_Id=='7'){
    $("#btnsendtoho").hide();
    $("#btnsave").show();
    $('.visiblecontrols').attr( "disabled", false );
}
else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    $('.visiblecontrols').attr( "disabled", true );
        $("#btnsave, #btnsendtoho").hide(); 
}

}
    }
}
   

    }
     

    function Deletejobfairmonthlyreporting(JF_Id){
            
                if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                    var ex_id =  $("#ddExchange").val();
                        }
                    else{
                      var ex_id =  sessionStorage.Ex_id;
                    }
        
            var MasterData = {  
                "p_Flag":'11',
                "p_Xmonth":jQuery("#ddlMonth").val(),
                "p_Xyear":jQuery("#ddlYear").val(),
                "p_Ex_Id":ex_id,
                "p_JF_Id":JF_Id,
                "p_JF_Date":'1990/01/01',
                "p_Company_Name":'0',
                "p_Post_Name":'0',
                "p_Selected_Candidates":'0',
                "p_Female_Candidates":'0',
                "p_SC_Candidates":'0',
                "p_ST_Candidates":'0',
                "p_OBC_Candidates":'0',
                "p_PH_Candidates":'0',
                "p_Minority_Candidates":'0',
                "p_Remark":'0',
                "p_UR_Candidates":'0',
                "p_Client_IP":'0',
                "p_LM_BY":'0',
                "p_Status":'0'
            };
          
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "jobfairmonthlyreporting";
            ajaxpost(path, 'parsedataDeletejobfairmonthlyreporting', 'comment', MasterData, 'control')
          
        }
        function parsedataDeletejobfairmonthlyreporting(data){  
        data = JSON.parse(data)

        toastr.success('Delete Successful');
        Fetchjobfairmonthlyreporting('1');

        // if(sessionStorage.Flag=='11'){
        //     toastr.success('Delete Successful');
        // }
    
        }
         