var date = new Date();
function FillCCMonthemp(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCCMonthemp(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillCCMonthemp('parsedatasecuredFillCCMonthemp',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#Month").empty();
    var data1 = data[0];
    jQuery("#Month").append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#Month").append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthNameHindi));
    }
    jQuery("#Month").val(date.getMonth())
    
    }
    
    }



    function FillYearemp(funct,control) {
        var path =  serverpath + "secured/year/0/20"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearemp(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearemp('parsedatasecuredFillYearemp',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#Year").empty();
                var data1 = data[0];
                jQuery("#Year").append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#Year").append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
                 jQuery("#Year").val(date.getFullYear()) 
            }
                  
    }



    function FillDDlExchOffice(ReportID,control){
        var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
       securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', control)
          }
    function parsedatasecuredFillDDlExchOffice(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDDlExchOffice();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                 }
            }  
    }




    function addNewRow() {
        var x = $('.addrow').length;
        var row = `<tr class="addrow insert" id="addrow`+x+`">
        <td>${x+1}</td>
        <td> <select type="text" name="name" id="officename`+x+`" class="form-control candidatename Company_Name" style="height: 32px; width: 196px;"> </select></td>
        <td><input class="JF_Date datepicker candidatename" id="JF_Date`+x+`" style="height: 32px; width: 196px;"></td>
    <td><select type="text" class="form-control candidatename Company_Name"  id="typeofjoining${x}"  style="height: 32px; width: 196px;">
        <option value="0">Select</option>
        <option value="1">नियमित</option>
        <option value="2">संविदा</option>
        <option value="3">आउटसोर्सिंग</option>
    </select></td>
        <td><input type="text" class="form-control candidatename Post_Name"  id="post`+x+`" style="height: 32px; width: 196px;"></td>
        <td><input type="text"  class="candidatename Selected_Candidates" id="noofcanjoining`+x+`" ></td>
        
        <td><input type="text"  class="candidatename UR_Candidates" style="width: 50px;"  id="ur`+x+`"></td>
        <td><input type="text"  class="candidatename SC_Candidates" style="width: 50px;"  id="sc`+x+`"></td>
        <td><input type="text"  class="candidatename ST_Candidates" style="width: 50px;"  id="st`+x+`"></td>
        <td><input type="text"  class="candidatename OBC_Candidates" style="width: 50px;"  id="obc`+x+`"></td>
        <td><input type="text"  class="candidatename Female_Candidates" style="width: 50px;" id="femail`+x+`"></td>  
        <td><input type="text"  class="candidatename PH_Candidates" style="width: 50px;" id="disabled`+x+`"></td>
        <td><input type="text"  class="candidatename Minority_Candidates" style="width: 50px;" id="minority`+x+`"></td>
        
        <td><button type="button" onclick="$('#addrow`+x+`').remove();"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
                Remove</button></td>
        <td> <button type="button" onclick="checkValidation(${x})"  id="Submit`+x+`" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
        Save 
    </button></td>
        </tr>`;
        $('#tablebody').append(row);
        $('#ddlDistrict'+x+'').val(sessionStorage.Ex_name);
        FillOffice(x);
        var today = new Date();
  var startDate = new Date(today.getFullYear(), today.getMonth(0)-1, 1);
var lastDate = new Date(today.getFullYear(), today.getMonth(0)-1, 31);
$(".datepicker").datepicker({
    autoclose: true,
    startDate: startDate,
    endDate: lastDate,
    format: 'dd/mm/yyyy'
        });
    }
    

    function FillPosition(control) {
        var path = serverpath + "adminDesignation/0/0";
        securedajaxget(path, 'parsedataPosition', 'comment', control)
    }
    
    function parsedataPosition(data,control) {
        data = JSON.parse(data)
        jQuery(`#post${control}`).empty();
        var data1 = data[0];
        jQuery(`#post${control}`).append(jQuery("<option></option>").val('0').html("Select position"));
        for (var i = 0; i < data1.length; i++) {
            jQuery(`#post${control}`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
        }
    
    }


      function FillOffice(control){
        var path = serverpath + "employment/'0'/'Appointment'/'1'";
        securedajaxget(path, 'parsedatasecuredFillOffice', 'comment', control)
          }
    function parsedatasecuredFillOffice(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillOffice(control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        } 
            else{
                jQuery(`#officename${control}`).empty();
                var data1 = data[0];
                jQuery("#officename"+control).append(jQuery("<option ></option>").val("0").html("Select Office"));
                for (var i = 0; i < data1.length; i++) { 
                    jQuery(`#officename${control}`).append(jQuery("<option></option>").val(data1[i].Office_Id).html(data1[i].Office_NameH));
                 }
            }  
    }


    function NewAppointmentEntry(i,appId){
        sessionStorage.indexemp=i;
        sessionStorage.appId=appId;
        var date=$(`#JF_Date${i}`).val().split('/');
        var jfdate=date[2]+'-'+date[1]+'-'+date[0]
        if(appId=='0'){
            var flag=1
        }else{
            var flag=5 
        }
        var MasterData = {  
            "p_Flag":flag,
            "p_Xmonth":$(`#Month`).val(),
            "p_Xyear":$(`#Year`).val(),
            "p_DistrictId":sessionStorage.DistrictId,
            "p_appointment_Id":appId,
            "p_appoint_Date":jfdate,
            "p_Agency_Name":$(`#officename${i}`).val(),
            "p_Post_Name":$(`#post${i}`).val(),
            "p_Appointment_type":$(`#typeofjoining${i}`).val(),
            "p_Selected_Candidates":$(`#noofcanjoining${i}`).val(),
            "p_Female_Candidates":$(`#femail${i}`).val(),
            "p_SC_Candidates":$(`#sc${i}`).val(),
            "p_ST_Candidates":$(`#st${i}`).val(),
            "p_OBC_Candidates":$(`#obc${i}`).val(),
            "p_PH_Candidates":$(`#disabled${i}`).val(),
            "p_Minority_Candidates":$(`#minority${i}`).val(),
            "p_Remark":$(`#remark${i}`).val(),
            "p_UR_Candidates":$(`#ur${i}`).val(),
            "p_Client_IP":sessionStorage.ipAddress,
            "p_Status":'Appointment',
            "p_Company_Name":''
        };
      
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "Appointment";
        ajaxpost(path, 'parsedataNewAppointmentEntry', 'comment', MasterData,i)
      
    }
    function parsedataNewAppointmentEntry(data,control){  
    data = JSON.parse(data)
    if(data[0][0].ReturnValue=='1'){

        reset(sessionStorage.indexemp);
        toastr.success('Insert Successful');
        FillEmployment()
    }
    if(data[0][0].ReturnValue=='2'){
    
        enabledtoModify(sessionStorage.indexemp,true);
        InsupdEmploymentlogs(sessionStorage.appId,'Appointment','modify')

        toastr.success('Update Successful');
    }
    if(data[0][0].ReturnValue=='3'){
    
        //enabledtoModify(sessionStorage.indexemp,true);
        toastr.warning('Details Already Exists');
    }
 }



    function Fetchappointment(Flag){
        sessionStorage.setItem('Flag',Flag)
        
            var MasterData = {  
                "p_Flag":Flag,
                "p_Xmonth":jQuery("#ddlMonth").val(),
                "p_Xyear":jQuery("#ddlYear").val(),
                "p_DistrictId":sessionStorage.DistrictId,
                "p_appointment_Id":'0',
                "p_appoint_Date":'1990/01/01',
                "p_Company_Name":'0',
                "p_Post_Name":'0',
                "p_Appointment_type":'0',
                "p_Selected_Candidates":'0',
                "p_Female_Candidates":'0',
                "p_SC_Candidates":'0',
                "p_ST_Candidates":'0',
                "p_OBC_Candidates":'0',
                "p_PH_Candidates":'0',
                "p_Minority_Candidates":'0',
                "p_Remark":'0',
                "p_UR_Candidates":'0',
                "p_Client_IP":'0',
                "p_Status":'Appointment'
            };
          
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "Appointment";
            ajaxpost(path, 'parsedataFetchappointment', 'comment', MasterData, 'control')
          
        }
        function parsedataFetchappointment(data){  
        data = JSON.parse(data)
        
    
        var appenddata="";
        var data1 = data[0]; 
    
        if(sessionStorage.Flag=='2'){
            toastr.success('Successful Send To Head Office');
            $('.visiblecontrols').attr( "disabled", true );
            $("#btnsave, #btnsendtoho").hide(); 
        }
    
       
            else if(sessionStorage.Flag=='3'){
                if(data1.length>0){
           
  
    for (var i = 0; i < data1.length; i++) {
            var date=data1[i].appoint_Date.split('-');
            var appoint_Date=date[0]+"/"+date[1]+"/"+date[2]
            var d1=date[2].split('')
            var d2=d1[0]+''+d1[1]
            var appoint_Date1=date[1]+"/"+d2+"/"+date[0]
            
    appenddata += `<tr class='addrow insert' id="modify_${data1[i].appointment_Id}">
    <td> `+[i+1]+` </td>
    <td> <select type="text" name="name" id="officename0" value="${data1[i].Office_Id}" class="form-control Company_Name visiblecontrols" style="height: 32px; width: 196px;">  </select></td>
   
    <td><select type="text" class="form-control candidatename" id="month" value="${month}" style="height: 32px; width: 99px;"></select></td>
    <td><select type="text" class="form-control candidatename" id="textContactYear"  value="${textContactYear}" style="height: 32px; width: 85px;"></select></td>
 
    <td><input type="text" class="companyname typeofjoining visiblecontrols" value="${data1[i].Appointment_type}" id="typeofjoining"></td>
    <td><select type="text" class="form-control candidatename Post_Name visiblecontrols" value="${data1[i].Post_Name}"  id="post" style="height: 32px; width: 196px;"></select></td>
    <td><input type="text"  class="selected Selected_Candidates visiblecontrols" value="${data1[i].Selected_Candidates}" id="noofcanjoining"></td>
    
    <td><input type="text"  class="candidatename UR_Candidates visiblecontrols" value="${data1[i].UR_Candidates}" style="width: 50px;" id="ur"></td>
    <td><input type="text"  class="candidatename SC_Candidates visiblecontrols" value="${data1[i].SC_Candidates}" style="width: 50px;" id="sc"></td>
    <td><input type="text"  class="candidatename ST_Candidates visiblecontrols" value="${data1[i].ST_Candidates}" style="width: 50px;" id="st"></td>
    <td><input type="text"  class="candidatename OBC_Candidates visiblecontrols" value="${data1[i].OBC_Candidates}" style="width: 50px;" id="obc"></td>
    <td><input type="text"  class="candidatename Female_Candidates visiblecontrols" value="${data1[i].Female_Candidates}" style="width: 50px;" id="femail"></td>  
    <td><input type="text"  class="candidatename PH_Candidates visiblecontrols" value="${data1[i].PH_Candidates}" style="width: 50px;" id="disabled"></td>
    <td><input type="text"  class="candidatename Minority_Candidates visiblecontrols" value="${data1[i].Minority_Candidates}" style="width: 50px;" id="minority"></td>
    
    <td><input type="text"  class="remark visiblecontrols" id="remark" value="${data1[i].Remark}"></td> 
    <td><button type="button" onclick="addNewRow();"  id="btnSubmit" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
    Add </button></td>
    <td> <button type="button"  onclick="checkValidation(${i})"  id="Submit`+i+`" class="btn btn-success visiblecontrols" style="background-color:#716aca;border-color:#716aca;">
    Save </button></td>
    </tr>`
    }
        jQuery("#tablebody").html(appenddata);  
        FillOfficerpt(data);
        FillPositionrpt(data);
       var today = new Date();
  var startDate = new Date(today.getFullYear(), today.getMonth(0)-1, 1);
var lastDate = new Date(today.getFullYear(), today.getMonth(0)-1, 31);
$(".datepicker").datepicker({
    autoclose: true,
    startDate: startDate,
    endDate: lastDate,
    format: 'dd/mm/yyyy'
        });    
         //     if(data1[0].Verify_YN=='1'){
    //         $('.visiblecontrols').attr( "disabled", true );
    //         $("#btnsave, #btnsendtoho").hide();
    //     }
    //     else  if(data1[0].Verify_YN=='0' || data1[0].Verify_YN==null){
    //         if(sessionStorage.User_Type_Id=='2'){
    //             $("#btnsave, #btnsendtoho").show();
    //             $('.visiblecontrols').attr( "disabled", false );
    //         }
    //         else   if(sessionStorage.User_Type_Id=='7'){
    //             $("#btnsendtoho").hide();
    //             $("#btnsave").show();
    //             $('.visiblecontrols').attr( "disabled", false );
    //         }
    //     }
        
    // if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    //     $('.visiblecontrols').attr( "disabled", true );
    //         $("#btnsave, #btnsendtoho").hide(); 
    // }
    // else{
    // if(sessionStorage.User_Type_Id=='2'){
    //     $("#btnsave, #btnsendtoho").show();
    //     $('.visiblecontrols').attr( "disabled", false );
    // }
    // else if(sessionStorage.User_Type_Id=='7'){
    //     $("#btnsendtoho").hide();
    //     $("#btnsave").show();
    //     $('.visiblecontrols').attr( "disabled", false );
    // }
    // else if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
    //     $('.visiblecontrols').attr( "disabled", true );
    //         $("#btnsave, #btnsendtoho").hide(); 
    // }
    
    // }

        }
    }
       
    
        }
         
    
        function FillOfficerpt(control){
            var path = serverpath + "employment/'0'/'Appointment'/'1'";
            securedajaxget(path, 'parsedatasecuredFillOfficerpt', 'comment', control)
              }
        function parsedatasecuredFillOfficerpt(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillOffice(control);
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            } 
                else{
                    jQuery(`.Company_Name`).empty();
                    var data1 = data[0];
                    for (var i = 0; i < data1.length; i++) { 
                        jQuery(`.Company_Name`).append(jQuery("<option></option>").val(data1[i].Office_Id).html(data1[i].Office_Name));
                     }
                     for (var i = 0; i < control[0].length; i++) {
                        jQuery(`#officename${i}`).val(control[0][i].officename);
                    }
                }  
        }
    


        function FillPositionrpt(control) {
            var path = serverpath + "adminDesignation/0/0";
            securedajaxget(path, 'parsedataPositionrpt', 'comment', control)
        }
        
        function parsedataPositionrpt(data,control) {
            data = JSON.parse(data)
            jQuery(`.Post_Name`).empty();
            var data1 = data[0];
            jQuery(`.Post_Name`).append(jQuery("<option></option>").val('0').html("Select position"));
            for (var i = 0; i < data1.length; i++) {
                jQuery(`.Post_Name`).append(jQuery("<option></option>").val(data1[i].Designation_Id).html(data1[i].Designation));
            }
            for (var i = 0; i < control[0].length; i++) {
                jQuery(`#post${i}`).val(control[0][i].post);            }
        }
        // function getTotal(i){
        //     var ur=$(`#ur${i}`).val()==''?0:parseInt($(`#ur${i}`).val());
        //     var sc=$(`#sc${i}`).val()==''?0:parseInt($(`#sc${i}`).val());
        //     var st=$(`#st${i}`).val()==''?0:parseInt($(`#st${i}`).val());
        //     var obc=$(`#obc${i}`).val()==''?0:parseInt($(`#obc${i}`).val());
           
        //     var total=ur+sc+st+obc;
            
        //     $(`#noofcanjoining${i}`).val(total);
        // }
        function checkValidation(i,appid){
            if($(`#officename${i}`).val()=='0'){
            
            toastr.warning('Please Select Agency Name')
            return false;
            }
            
            
            if($(`#typeofjoining${i}`).val()=='0'){
            
            toastr.warning('Please Enter Appointment Type')
            return false;
            }
            if($(`#post${i}`).val()==''){
            
            toastr.warning('Please Enter Position')
            return false;
            }
            if($(`#ur${i}`).val()==''){
            
                toastr.warning('Please Enter UR Count')
                return false;
                }
                if($(`#sc${i}`).val()==''){
            
                    toastr.warning('Please Enter SC Count')
                    return false;
                    }
                    if($(`#st${i}`).val()==''){
            
                        toastr.warning('Please Enter ST Count')
                        return false;
                        }
                        if($(`#obc${i}`).val()==''){
            
                            toastr.warning('Please Enter OBC Count')
                            return false;
                            }
                            if($(`#femail${i}`).val()==''){
            
                                toastr.warning('Please Enter Women Count')
                                return false;
                                }
                                if($(`#disabled${i}`).val()==''){
            
                                    toastr.warning('Please Enter Disability Count')
                                    return false;
                                    }
                                    if($(`#minority${i}`).val()==''){
            
                                        toastr.warning('Please Enter Minority Count')
                                        return false;
                                        }
            else{
                NewAppointmentEntry(i,appid)
            }
            }
            function reset(i){
if(i=='0'){
           $(`#officename${i}`).val('0'),
           $(`#post${i}`).val(''),
           $(`#typeofjoining${i}`).val('0'),
           $(`#noofcanjoining${i}`).val(''),
           $(`#femail${i}`).val(''),
           $(`#sc${i}`).val(''),
           $(`#st${i}`).val(''),
           $(`#obc${i}`).val(''),
           $(`#disabled${i}`).val(''),
           $(`#minority${i}`).val(''),
           $(`#remark${i}`).val(''),
           $(`#ur${i}`).val(''),
           $(`#JF_Date${i}`).val('')
}
else{
    $(`#addrow${i}`).remove();
}
            }

            function FillEmployment() {
                var path = serverpath +"appointment_details/Appointment/"+date.getMonth()+"/"+date.getFullYear()+"/"+sessionStorage.DistrictId+""
                ajaxget(path, 'parsedataEmployment', 'comment', "control");
            }
            
            function parsedataEmployment(data) {
            
                data = JSON.parse(data)
                var data1 = data[0];
                var appenddata = "";
                var active = "";
                jQuery("#tablebody1").empty()
                for (var i = 0; i < data1.length; i++) {
            
                    appenddata += `<tr>
                    <td>${i+1}</td>
                    <td> <select type="text" name="name" id="officename${data1[i].appointment_Id}" class="form-control candidatename Company_Name" style="height: 32px; width: 196px;" disabled> </select></td>
                    <td><input class="JF_Date datepicker candidatename" id="JF_Date${data1[i].appointment_Id}" style="height: 32px; width: 196px;" value=${data1[i].appoint_Date} disabled></td>
                    <td><select type="text" class="form-control candidatename Company_Name" id="typeofjoining${data1[i].appointment_Id}" style="height: 32px; width: 196px;" disabled>
                    <option value="0">Select</option>
                    <option value="1">नियमित</option>
                    <option value="2">संविदा</option>
                    <option value="3">आउटसोर्सिंग</option>
                    </select></td>
                    <td><input type="text" class="form-control candidatename Post_Name" id="post${data1[i].appointment_Id}" value=${data1[i].Post_Name} disabled style="height: 32px; width: 196px;" disabled></td>
                    <td><input type="text" class="candidatename Selected_Candidates" id="noofcanjoining${data1[i].appointment_Id}" value=${data1[i].Selected_Candidates} disabled></td>
                    
                    <td><input type="text" class="candidatename UR_Candidates" style="width: 50px;" id="ur${data1[i].appointment_Id}" value=${data1[i].UR_Candidates} disabled></td>
                    <td><input type="text" class="candidatename SC_Candidates" style="width: 50px;" id="sc${data1[i].appointment_Id}" value=${data1[i].SC_Candidates} disabled></td>
                    <td><input type="text" class="candidatename ST_Candidates" style="width: 50px;" id="st${data1[i].appointment_Id}" value=${data1[i].ST_Candidates} disabled></td>
                    <td><input type="text" class="candidatename OBC_Candidates" style="width: 50px;" id="obc${data1[i].appointment_Id}" value=${data1[i].OBC_Candidates} disabled></td>
                    <td><input type="text" class="candidatename Female_Candidates" style="width: 50px;" id="femail${data1[i].appointment_Id}" value=${data1[i].Female_Candidates} disabled></td>
                    <td><input type="text" class="candidatename PH_Candidates" style="width: 50px;" id="disabled${data1[i].appointment_Id}" value=${data1[i].PH_Candidates} disabled></td>
                    <td><input type="text" class="candidatename Minority_Candidates" style="width: 50px;" id="minority${data1[i].appointment_Id}" value=${data1[i].Minority_Candidates} disabled></td>
                    
                    <td><button type="button" class="btn btn-success" onclick="enabledtoModify(${data1[i].appointment_Id},false)">Modify</button></td>
                    
                    <td> <button type="button" onclick="checkValidation(${data1[i].appointment_Id},${data1[i].appointment_Id});" class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">Save</button></td>
                    <td><button type="button" onclick="aboutdelete(${data1[i].appointment_Id})"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
                                                        Delete</button></td>
                    </tr>`;
                   FillOffice1(`${data1[i].appointment_Id}_${data1[i].Agency_Name}`)
                      } jQuery("#tablebody1").append(appenddata);
                      for (var i = 0; i < data1.length; i++) {
                        $(`#typeofjoining${data1[i].appointment_Id}`).val(data1[i].Appointment_type)
                   
                      }
            
            }
            function FillOffice1(control){
                var path = serverpath + "employment/'0'/'Appointment'/'1'";
                securedajaxget(path, 'parsedatasecuredFillOffice1', 'comment', control)
                  }
            function parsedatasecuredFillOffice1(data,control){  
                data = JSON.parse(data)
                if (data.message == "New token generated"){
                    sessionStorage.setItem("token", data.data.token);
                    FillOffice1(control);
                }
                else if (data.status == 401){
                    toastr.warning("Unauthorized", "", "info")
                    return true;
                } 
                    else{
                        var officeid=control.split('_')[0];
                        var officename=control.split('_')[1]
                        jQuery(`#officename${officeid}`).empty();
                        var data1 = data[0];
                        jQuery("#officename"+officeid).append(jQuery("<option ></option>").val("0").html("Select Agency"));
                        for (var i = 0; i < data1.length; i++) { 
                            jQuery(`#officename${officeid}`).append(jQuery("<option></option>").val(data1[i].Office_Id).html(data1[i].Office_NameH));
                         }
                         jQuery(`#officename${officeid}`).val(officename);
                    }  
            }

            function enabledtoModify(appId,isdis){
$(`#officename${appId},#JF_Date${appId},#typeofjoining${appId},#post${appId},#noofcanjoining${appId},#ur${appId},#sc${appId},#st${appId},#obc${appId},#femail${appId},#disabled${appId},#minority${appId}`).attr('disabled',isdis)
}


function deleteMode(Emp_Id) {
    var path =  serverpath + "deleteappointmentdetails/"+Emp_Id+"/'Appointment'"
    securedajaxget(path,'parsedataemploymentdetails','comment',Emp_Id);
}

function parsedataemploymentdetails(data,control){  
    data = JSON.parse(data)
   if(data[0][0].ReturnValue=='1')
   {
    FillEmployment();
       toastr.success("Delete Successfully");
       InsupdEmploymentlogs(control,'Appointment','delete')
   }
}
function aboutdelete(Id){
    $("#myModal1").modal('show');
    $("#deletebtn1").on('click',function(){
        deleteMode(Id);
   $("#myModal1").modal('hide')
    })
   }