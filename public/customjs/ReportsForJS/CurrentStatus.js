
function FilldivExchange(funct,control) {
    var path =  serverpath + "MstDivision/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFilldivExchange(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FilldivExchange('parsedatasecuredFilldivExchange','ddlDivision');
     
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Division_id).html(data1[i].Division_name));
             }
             if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
                $("#"+control).val(sessionStorage.DivisionId);
             }

        }
              
}

jQuery('#ddlDivision').on('change', function () {

    FillWorkingExchange('parsedatasecuredFillExchangeByDivision','ddlExchange',jQuery('#ddlDivision').val());
    
});


function FillWorkingExchange(funct,control,Division_id) {
    var path =  serverpath + "secured/exchangebydivision/"+Division_id+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExchangeByDivision(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillWorkingExchange('parsedatasecuredFillExchangeByDivision','ddlExchange',jQuery('#ddlDivision').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
             if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
             $("#"+control).val(sessionStorage.Ex_id);
             }
        }
              
}



function fetchCurrentStatusReport(){
    if($("#m_datepicker_2").val()==''){
         var FromDate = new Date()

        // var FDate = new Date().split(" ")
        // var fdate = FDate[0].split(" ")
        // var  FromDate =  fdate[2] + "-" +  fdate[1] + "-" +  fdate[0] ;

    }
    else{
        var  FromDt = jQuery("#m_datepicker_2").val().split(" ")
        var  FromDt1 =  FromDt[0].split("/")
        var  FromDate =  FromDt1[2] + "-" +  FromDt1[1] + "-" +  FromDt1[0] ;
    
    }
    

  

    var path =  serverpath + "currentstatusrt/" + $("#ddlDivision").val() + "/" + $("#ddlExchange").val() + "/'"+ FromDate +"'"
    ajaxget(path,'parsedatafetchCurrentStatusReport','comment',"control");
}
function parsedatafetchCurrentStatusReport(data){  
    data = JSON.parse(data)
   var appenddata="";
   var appenddataT2="";
    var data1 = data[0]; 
    $("#tablehead1").show()   
    var  sumExchangeside=0
    var TotalExcside=0
    var TotalOnlineReg=0
    var TotalRenewal=0
    var TotalLogins=0

            // for (var i = 0; i < data1.length; i++) {
                
                          
        
    //    appenddata += "<tr><td style='    word-break: break-word;'>" +Exchangeside+ "</td><td style='    word-break: break-word;'>" +[i+1]+ "</td><td style='    word-break: break-word;'>" +[i+1]  + "</td><td>"+ [i+1] +"</td><td>"+ [i+1]  +"</td></tr>";
        // }
        if(sessionStorage.User_Type_Id=='1')
        {
            $('#tableheading').text('Directorate of Employment, Madhya Pradesh -Registration Status on Date ')
    
        }
        else{
            $('#tableheading').text($('#ddlExchange option:selected').text()+'  -Registration Status on Date ')
    
        }
        var malerecord=0;
        var femalerecord=0;
        var totalrecord=0;
        $("#tablehead2").show() 
  $('#excel').show();

  $("#headingtext").html(sessionStorage.Ex_name+" </br>"+'As on Date '+$('#m_datepicker_2').val() )

     
        var head='<th></th><th colspan=4>'+sessionStorage.Ex_name+' </br>Report Name :  Current Status Report <br>Division :'+$('#ddlDivision option:selected').html()+',<br>Exchange : '+$('#ddlExchange option:selected').html()+'<br>Status on date : '+$('#m_datepicker_2').val()+'</th>';
        $('#Detailhead').html(head)
        for (var i = 0; i < data1.length; i++) {
        
            appenddataT2 += "<tr><td>"+[i+1]+"</td><td style='    word-break: break-word;text-align: left;'>" + data1[i].Ex_name+ "</td><td style='    word-break: break-word;' class='credit2' >" + data1[i].LiveJsMale+ "</td><td class='credit3'>"+data1[i].LiveJsFemale+"</td><td class='credit'>"+data1[i].LiveJsRecords+"</td></tr>";
            malerecord=parseInt(malerecord)+parseInt(data1[i].LiveJsMale);
            femalerecord=parseInt(femalerecord)+parseInt(data1[i].LiveJsFemale);
            totalrecord=parseInt(totalrecord)+parseInt(data1[i].LiveJsRecords);
        }
        appenddata = "<tr class='total'><td></td><td style='font-weight: 600;'>Total</td><td style='word-break: break-word;'>" +malerecord+ "</td><td style='    word-break: break-word;'>" +femalerecord+ "</td><td style='    word-break: break-word;'>" + totalrecord + "</td></tr>";

        // jQuery("#tbodyvalue1").html(appenddata);  
        jQuery("#tbodyvalue2").html(appenddataT2+appenddata);

     //       appenddata = "<tr><td style='word-break: break-word;'>" +malerecord+ "</td><td style='    word-break: break-word;'>" +femalerecord+ "</td><td style='    word-break: break-word;'>" + totalrecord + "</td></tr>";
         //   jQuery("#tbodyvalue1").html(appenddata);  

              
}

