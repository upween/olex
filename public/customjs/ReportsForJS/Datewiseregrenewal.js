function fetchDatewisereg() {
    var data =$('#m_datepicker_5').val();
    var arr = data.split('/');
    var frm_dt=arr[2] + "-" + arr[1]+"-"+arr[0]

    var data1 =$('#m_datepicker_6').val();
    var arr1 = data1.split('/');
    var to_dt=arr1[2] + "-" + arr1[1]+"-"+arr1[0]
   
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "Datewise/'"+$('#ddlExchange').val()+"'/'"+frm_dt+"'/'"+to_dt+"'",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
    var head="<th>Report Name-Date wise Registration and Renewal count</th><th>From Date-"+$('#m_datepicker_5').val()+"</th><th>TO Date-"+$('#m_datepicker_6').val()+"</th>";
    
    $("#headingtext").html(sessionStorage.Ex_name+" </br>Date wise Registration And Renewal Count "+$('#m_datepicker_5').val()+' - '+$('#m_datepicker_6').val())
     
    var head='<th></th><th colspan=4>'+sessionStorage.Ex_name+' </br>Report Name : Date wise Registration And Renewal Report <br>Exchange Name :'+$('#ddlExchange option:selected').html()+',<br>From Date : '+$('#m_datepicker_5').val()+" - To Date : "+$('#m_datepicker_6').val()+'</th>';
$('#Detailhead').html(head)

    var tablehead="<tr><th>S.No</th><th>Date</th><th>Registration Count</th><th>Renewal Count</th></tr>"
  				var appenddata="";
				var total=0;
                var grandtotal_CandidateCount=0;
                var grandtotal_CandidateRenewCount=0;
                var grand_total=0;
				for (var i = 0; i < data1.length; i++) {
				total=parseInt(data1[i].CandidateCount)+parseInt(data1[i].CandidateRenewCount);
				grandtotal_CandidateCount+=parseInt(data1[i].CandidateCount);
                grandtotal_CandidateRenewCount+=parseInt(data1[i].CandidateRenewCount);
                grand_total=grandtotal_CandidateCount+grandtotal_CandidateRenewCount;	
				appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i].dt_con+"</td><td>"+data1[i].CandidateCount+"</td><td>"+data1[i].CandidateRenewCount+"</td></tr>";
				 }jQuery("#tbodyvalue").html(appenddata+"<tr class='total'><td></td><td><strong>Total</strong></td><td>"+grandtotal_CandidateCount+"</td><td>"+grandtotal_CandidateRenewCount+"</td></tr>");  
                 $("#tablehead").html(tablehead);
                 $('#Detailhead').html(head);

                 
        },
        
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
