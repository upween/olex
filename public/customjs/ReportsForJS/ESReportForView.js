function FetchES11Rpt(){
    var path =  serverpath + "ES11_Rpt/'"+ sessionStorage.ES11Exid+"'/'"+sessionStorage.Monthrpt+"'/'"+sessionStorage.yearrpt+"'"
    ajaxget(path,'parsedataFetchES11Rpt','comment',"control");
   }
   function parsedataFetchES11Rpt(data){
    data = JSON.parse(data)
     
   
    var appenddata='';
    var Total_3_M=parseInt(data[0][0].Prev_LR_M)+data[0][0].ReceiveRegistration_M+data[0][0].FreshRegistration_M+data[0][0].ReRegistration_M;

    var Total_3_F=data[0][0].Prev_LR_F+data[0][0].ReceiveRegistration_F+data[0][0].FreshRegistration_F+data[0][0].ReRegistration_F;
  
    var Total_3=Total_3_M+Total_3_F;
    var Total_7_M=data[0][0].PlacedRegistration_M+data[0][0].RemovedRegistration_M+data[0][0].TransferRegistration_M;
    var Total_7_F=data[0][0].PlacedRegistration_F+data[0][0].RemovedRegistration_F+data[0][0].TransferRegistration_F;
    var Total_7=Total_7_M+Total_7_F;
    appenddata=`<tr >
                <td>1</td>
                <td>	Applicants on the Live Register at the end of the Previous Month</td>
                <td>${data[0][0].Prev_LR_M}</td>
                <td> ${data[0][0].Prev_LR_F}</td>
                <td>  ${data[0][0].Prev_LR_M+data[0][0].Prev_LR_F}</td>
          </tr>
        <tr >
                <td>1a</td>
                <td>	No. of Registration cards recieved on Transfer</td>
                <td> ${data[0][0].ReceiveRegistration_M}</td>
                <td> ${data[0][0].ReceiveRegistration_F}</td>
                <td> ${data[0][0].ReceiveRegistration_M+data[0][0].ReceiveRegistration_F}
                </td>
        </tr>

        <tr >
            <td>2</td>
            <td>	No. of Fresh registration made during the month</td>
            <td>  ${data[0][0].FreshRegistration_M}</td>
            <td>  ${data[0][0].FreshRegistration_F}</td>
            <td>  ${data[0][0].FreshRegistration_M+data[0][0].FreshRegistration_F}</input>
            </td>
          </tr>
          <tr >
            <td>2a</td>
            <td>		No. of Re-registration made during the month</td>
            <td>${data[0][0].ReRegistration_M}
            </td>
            <td>${data[0][0].ReRegistration_F}</td>
            <td>${data[0][0].ReRegistration_M+data[0][0].ReRegistration_F}</td>
          </tr>
          <tr >
            <td>3</td>
            <td>Total: Items 1, 1a, 2 and 2a</td>
            <td>  ${Total_3_M}
            </td>
            <td> ${Total_3_F}</td>
            <td>  ${Total_3} </td>
          </tr>
        <tr >
            <td>4</td>
            <td>No. of Job-seekers placed during the Month</td>
            <td>${data[0][0].PlacedRegistration_M}
            </td>
            <td>${data[0][0].PlacedRegistration_F}</td>
            <td>${data[0][0].PlacedRegistration_M+data[0][0].PlacedRegistration_F}</td>
        </tr>
        <tr >
            <td>5</td>
            <td>Registration Card removed from Live Register for reason other than Transfer to other exchanges</td>
            <td>  ${data[0][0].RemovedRegistration_M}
            </td>
            <td> ${data[0][0].RemovedRegistration_F}  </td>
            <td> ${data[0][0].RemovedRegistration_M+data[0][0].RemovedRegistration_F}  </td>
        </tr>
                      <tr >
                        <td>6</td>
                        <td>No. of Registration cards transferred during the month to other exchanges</td>
                        <td> ${data[0][0].TransferRegistration_M}
                        </td>
                        <td> ${data[0][0].TransferRegistration_F}</td>
                        <td> ${data[0][0].TransferRegistration_M+data[0][0].TransferRegistration_F}</td>
                  </tr>
                  <tr >
                    <td>7</td>
                    <td>	Total: Items 4, 5 and 6</td>
                    <td>${Total_7_M}
                    </td>
                    <td>${Total_7_F}</td>
                    <td>${Total_7}</td>
              </tr>
              <tr >
                <td>8</td>
                <td>Applicants remaining on the Live Register at the end of the Month</td>
                <td>${data[0][0].LR_M}</td>
                <td>${data[0][0].LR_F}</td>
                <td>${data[0][0].LR_M+data[0][0].LR_F}</td>
          </tr>
              <tr >
                <td>8a</td>
                <td>No. of applicants out of item 8 above belong to Rural Area</td>
                <td>${data[0][0].Rural_LR_M}</td>
                <td>${data[0][0].Rural_LR_F}</td>
                <td>${data[0][0].Rural_LR_M+data[0][0].Rural_LR_F}</td>
                </tr>
          <tr >
            <td>9</td>
            <td>Submissions made during the Month</td>
            <td>${data[0][0].Submissions_M}</td>
            <td>${data[0][0].Submissions_F}</td>
            <td>${data[0][0].Submissions_M+data[0][0].Submissions_F}</td>
      </tr>
      <tr >
        <td>10</td>
        <td>	Total Vacancies notified during the Month</td>
        <td>${data[0][0].NoOfNotify_M}</td>
        <td>${data[0][0].NoOfNotify_F}</td>
        <td>${data[0][0].NoOfNotify_M+data[0][0].NoOfNotify_F}</td>
  </tr>
  <tr>
  <tr>
    <td colspan="5"  style="text-align: end;" >Signature of Employment Officer<br>
      (With name and complete postal address of the Employment Exchange)
      
  </tr>

`;
 jQuery("#tbodyvalue").html(appenddata); 
   }

   function FetchRpt(){
       var rptid=sessionStorage.RptID;
if(rptid=='101'){
    $('.rptNametxt').text('E.S.1.1 (Monthly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return Showing the work done by the Employment Exchange in respect of all categories of applicants during the month ended : ${ sessionStorage.Monthname} ${sessionStorage.yearrpt}`)
    $('#exnameess1rpt').text('View  E.S.1.1 Reports ('+sessionStorage.Exname_es11+' )');
    var tablehead="<tr><td  style='background: #716aca;color:white;' colspan=1>S.No.</td><td  style='background: #716aca;color:white;' colspan=1>Item</td><td  style='background: #716aca;color:white;'>Men</td><td  style='background: #716aca;color:white;' colspan=1>Women</td><td  style='background: #716aca;color:white;' colspan=1>Total</td></tr>";
    $("#tablehead").html(tablehead); 
    FetchES11Rpt();
}
if(rptid=='102'){
    $('.rptNametxt').text('E.S.1.4 (Report)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.1.4 Reports ('+sessionStorage.Exname_es11+' )');
    FillEduationalLevel('parsedataFillEduationalLevel','tbodyvalue');
}
if(rptid=='103'){
    $('.rptNametxt').text('E.S.2.3(Half Yealy)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchange in respect of Minority Communities during the half year ended:  ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.2.3 Reports ('+sessionStorage.Exname_es11+' )');
    var tablehead =`	<tr><td  style='background: #716aca;color:white;'>Sr. No</td>
    <td  style='background: #716aca;color:white;'>Item</td>
    <td  style='background: #716aca;color:white;'>Musilm</td>
    <td  style='background: #716aca;color:white;'>Christian</td>
    <td  style='background: #716aca;color:white;'>Sikhs</td>
    <td  style='background: #716aca;color:white;'>Budhists</td>
    <td  style='background: #716aca;color:white;'>Zoarastrians</td>
    <td  style='background: #716aca;color:white;'>Total</td></tr>`;
    $('#tablehead').html(tablehead);
    var tbodyvalue=`<tr>
    <td>1</td>
    <td>	No. on the Live Register at the end of the previous half Yearates)</td>


    <td id="LR_Prev_M" >
    </td>
    <td  id="LR_Prev_C" >
    </td>
    <td id="LR_Prev_S" >
    </td>
    <td  id="LR_Prev_B" >
    </td>
    <td  id="LR_Prev_Z" >
    </td>
    <td  id="LR_Prev_Total" >
    </td>

</tr>
<tr>
        <td>2</td>
        <td>		No. of registration/re-registration Effected</td>
            
    <td id="Registration_M" >
    </td>
    <td  id="Registration_C" >
    </td>
    <td id="Registration_S" >
    </td>
    <td  id="Registration_B" >
    </td>
    <td  id="Registration_Z" >
    </td>
    <td  id="Registration_Total" >
    </td>

    </tr>
    <tr>
            <td>3</td>
            <td>	No. Placed during the Half year	</td>
            
            <td id="PlacesJs_M" >
            </td>
            <td  id="PlacesJs_C" >
            </td>
            <td id="PlacesJs_S" >
            </td>
            <td  id="PlacesJs_B" >
            </td>
            <td  id="PlacesJs_Z" >
            </td>
            <td  id="PlacesJs_Total" >
            </td>
        </tr>
        <tr>
                <td>4</td>
                <td>No. removed from the Live register</td>
            
                
                <td id="Registration_Removed_M" >
                </td>
                <td  id="Registration_Removed_C" >
                </td>
                <td id="Registration_Removed_S" >
                </td>
                <td  id="Registration_Removed_B" >
                </td>
                <td  id="Registration_Removed_Z" >
                </td>
                <td  id="Registration_Removed_Total" >
                </td>
                
            
            </tr>
            
        <tr>
                <td>5</td>
                <td>No. on the Live register at end of the half Year	</td>
            
                <td id="LR_M" >
                </td>
                <td  id="LR_C" >
                </td>
                <td id="LR_S" >
                </td>
                <td  id="LR_B" >
                </td>
                <td  id="LR_Z" >
                </td>
                <td  id="LR_Total" >
                </td>
            
            </tr>
            <tr>
                    <td>6</td>
                    <td>No. of Submission made	</td>
                    
                    <td id="Submissions_M" >
                    </td>
                    <td  id="Submissions_C" >
                    </td>
                    <td id="Submissions_S" >
                    </td>
                    <td  id="Submissions_B" >
                    </td>
                    <td  id="Submissions_Z" >
                    </td>
                    <td  id="Submissions_Total" >
                    </td>
                    
                </tr>`;
    $('#tbodyvalue').html(tbodyvalue);
    ES23RptEntryForm();
}

if(rptid=='104'){
    $('.rptNametxt').text('E.S.2.4 Part I(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchnages in respect of Scheduled Caste/Tribe and Other Backward Classes(OBC) applicats during Half Year-ended___ ${sessionStorage.criteria=='1'?'31/06/':'31/12/'}   ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.2.4 Part I Reports ('+sessionStorage.Exname_es11+' )');
    var tbodyvalue=`															<tr>
    <td>1</td>
    <td>No.of applicants on the Live Register at the end of the previous half year</td>
    <td id='LR_Prev_1_SC' ></td>
    <td id='LR_Prev_1_ST' ></td>
    <td id='LR_Prev_1_OBC' ></td>
</tr>
<tr>
    <td>2</td>
    <td>No.of applicants registered/re-registered during the half Year</td>
    <td id='Registration_SC' ></td>
    <td id='Registration_ST' ></td>
    <td id='Registration_OBC' ></td>
</tr>
<tr>
    <td>3</td>
    <td>	No. of applicants placed during the half year in:</td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td></td>
    <td>	a) Central Govt.</td>
    <td id='PlacesJs_CG_SC' ></td>
    <td id='PlacesJs_CG_ST' ></td>
    <td id='PlacesJs_CG_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>b) Union Territory</td>
    <td id='PlacesJs_UT_SC' ></td>
    <td id='PlacesJs_UT_ST' ></td>
    <td id='PlacesJs_UT_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>c) State Govt</td>
    <td id='PlacesJs_SG_SC' ></td>
    <td id='PlacesJs_SG_ST' ></td>
    <td id='PlacesJs_SG_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>d) Quasi-Govt. Estts/Public Sector undertakings under <br>i)  Center Govt.<br>ii) State Govt.</td>
    <td id='PlacesJs_QG_SC' ></td>
    <td id='PlacesJs_QG_ST' ></td>
    <td id='PlacesJs_QG_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>e) Local Bodies</td>
    <td id='PlacesJs_LB_SC' ></td>
    <td id='PlacesJs_LB_ST' ></td>
    <td id='PlacesJs_LB_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>f) Private Establishments</td>
    <td id='PlacesJs_Private_SC' ></td>
    <td id='PlacesJs_Private_ST' ></td>
    <td id='PlacesJs_Private_OBC' ></td>
</tr>
<tr>
    <td></td>
    <td>Total of Items 3 (a) to 3 (f)</td>
    <td id='PlacesJs_Total_SC' ></td>
    <td id='PlacesJs_Total_ST' ></td>
    <td id='PlacesJs_Total_OBC' ></td>

</tr>
<tr>
    <td>4</td>
    <td>	No.of applicants removed from the Live Register at the end of the half year.</td>
    <td id='Registration_Removed_SC' ></td>
    <td id='Registration_Removed_ST' ></td>
    <td id='Registration_Removed_OBC' ></td>
</tr>
<tr>
    <td>5</td>
    <td>No. of applicants remaining on the Live Register at the end of the half year. </td>
    <td id='LR_SC' ></td>
    <td id='LR_ST' ></td>
    <td id='LR_OBC' ></td>
</tr>
<tr>
    <td>6</td>
    <td>No. of Submissions made during the half year.</td>
    <td id='Submissions_SC' ></td>
    <td id='Submissions_ST' ></td>
    <td id='Submissions_OBC' ></td>
</tr>`;
$('#tbodyvalue').html(tbodyvalue);
var tablehead=`	<tr><td  style='background: #716aca;color:white;'>Sr. No</td>
<td  style='background: #716aca;color:white;'>Item</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td></tr>
`;
$('#tablehead').html(tablehead);
ES24Part1RptEntryForm();
}
if(rptid=='105'){
    $('.rptNametxt').text('E.S.2.4 Part II(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
  
    $('#exnameess1rpt').text('View  E.S.2.4 Part II Reports ('+sessionStorage.Exname_es11+' )');

var tablehead=`<tr>
<td  style='background: #716aca;color:white;' colspan=1>Sno</td>
<td  style='background: #716aca;color:white;' colspan=1>Type of Establishment</td>
<td  style='background: #716aca;color:white;' colspan=3>Outstanding at the end of the previous half year</td>
<td  style='background: #716aca;color:white;' colspan=3>Notified during the half year</td>
<td  style='background: #716aca;color:white;' colspan=3>Filled during the half year</td>
<td  style='background: #716aca;color:white;' colspan=6>Cancelled during the year due to</td>
<td  style='background: #716aca;color:white;' colspan=3>Outstanding at the end of the half year</td>
</tr>
<tr>
    <td  style='background: #716aca;color:white;' colspan=1>
    </td>
    <td  style='background: #716aca;color:white;' colspan=1>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;' >Non-availability of suitable candidates</td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;' >Other reasons</td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
</tr>
<tr>

<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
<td  style='background: #716aca;color:white;'>SC</td>
<td  style='background: #716aca;color:white;'>ST</td>
<td  style='background: #716aca;color:white;'>OBC</td>
</tr>
<tr>
<td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
<td>10</td>
<td>11</td>
<td>12</td>
<td>13</td>
<td>14</td>
<td>15</td>
<td>16</td>
<td>17</td>
<td>18</td>
<td>19</td>
<td>20</td>`;
$('#tablehead').html(tablehead);
fetchES24Part2();
}

if(rptid=='106'){
    $('.rptNametxt').text('E.S.2.5 Part I(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchnages in respect of All Disabled
    <br />
    applicants during the Half Year ended__ ${sessionStorage.criteria=='1'?'31/06/':'31/12/'}   ${sessionStorage.yearrpt}`)
    
    
    $('#exnameess1rpt').text('View  E.S.2.5 Part I Reports ('+sessionStorage.Exname_es11+' )');

var tablehead=`<tr>
<td  style='background: #716aca;color:white;'>Sr. No</td>
<td  style='background: #716aca;color:white;'>Item</td>
<td  style='background: #716aca;color:white;'>Blind</td>
<td  style='background: #716aca;color:white;'>Deaf & Dumb</td>
<td  style='background: #716aca;color:white;'>Orthopaedics</td>
<td  style='background: #716aca;color:white;'>Respiratory Disorde</td>
<td  style='background: #716aca;color:white;'>Nag.Leprosy Persons</td>
<td  style='background: #716aca;color:white;'>Total(col.3-7)</td>
<td  style='background: #716aca;color:white;'>Women(Included in total)</td>;
</tr>`
$('#tablehead').html(tablehead);
var tbodyvalue=`<tr>
<td>1</td>

<td>No.of Disabled on the live register at the end of the Previous Half Year.</td>
<td class="visibleControls"  id="LR_Prev_1_Blind" ></td>
<td class="visibleControls" id="LR_Prev_1_Deaf"></td>
<td class="visibleControls" id="LR_Prev_1_Orthopaedics"></td>
<td class="visibleControls" id="LR_Prev_1_Respiratory"></td>
<td class="visibleControls" id="LR_Prev_1_Leprosy"></td>
<td class="visibleControls" id="LR_Prev_1_Total"></td>
<td class="visibleControls" id="LR_Prev_1_W"></td>
</tr>
<tr>
<td>2</td>
<td>No.of Disabled registrations during the Half Year.</td>
<td class="visibleControls" id="Registration_Blind" ></td>
<td class="visibleControls" id="Registration_Deaf"></td>
<td class="visibleControls" id="Registration_Orthopaedics"></td>
<td class="visibleControls" id="Registration_Respiratory"></td>
<td class="visibleControls" id="Registration_Leprosy"></td>
<td class="visibleControls" id="Registration_Total"></td>
<td class="visibleControls" id="Registration_W"></td>
</tr>
<tr>
<td>3</td>
<td>No. of disabled applicants placed during the half year in:</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td>	a) Central Govt.</td>
<td class="visibleControls" id="PlacesJs_CG_Blind" ></td>
<td class="visibleControls" id="PlacesJs_CG_Deaf"></td>
<td class="visibleControls" id="PlacesJs_CG_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_CG_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_CG_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_CG_Total"></td>
<td class="visibleControls" id="PlacesJs_CG_W"></td>
</tr>
<tr>
<td></td>
<td>b) Union Territory</td>
<td class="visibleControls" id="PlacesJs_UT_Blind" ></td>
<td class="visibleControls" id="PlacesJs_UT_Deaf"></td>
<td class="visibleControls" id="PlacesJs_UT_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_UT_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_UT_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_UT_Total"></td>
<td class="visibleControls" id="PlacesJs_UT_W"></td>
</tr>
<tr>
<td></td>
<td>c) State Govt</td>
<td class="visibleControls" id="PlacesJs_SG_Blind" ></td>
<td class="visibleControls" id="PlacesJs_SG_Deaf"></td>
<td class="visibleControls" id="PlacesJs_SG_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_SG_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_SG_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_SG_Total"></td>
<td class="visibleControls" id="PlacesJs_SG_W"></td>
</tr>
<tr>
<td></td>
<td>d) Quasi-Govt. Estts/Public Sector undertakings under <br>i)  Center Govt.<br>ii) State Govt.</td>
<td class="visibleControls" id="PlacesJs_QG_Blind" ></td>
<td class="visibleControls" id="PlacesJs_QG_Deaf"></td>
<td class="visibleControls" id="PlacesJs_QG_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_QG_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_QG_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_QG_Total"></td>
<td class="visibleControls" id="PlacesJs_QG_W"></td>
</tr>
<tr>
<td></td>
<td>e) Local Bodies</td>
<td class="visibleControls" id="PlacesJs_LB_Blind" ></td>
<td class="visibleControls" id="PlacesJs_LB_Deaf"></td>
<td class="visibleControls" id="PlacesJs_LB_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_LB_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_LB_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_LB_Total"></td>
<td class="visibleControls" id="PlacesJs_LB_W"></td>
</tr>
<tr>
<td></td>
<td>f) Private Establishments</td>
<td class="visibleControls" id="PlacesJs_Private_Blind" ></td>
<td class="visibleControls" id="PlacesJs_Private_Deaf"></td>
<td class="visibleControls" id="PlacesJs_Private_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_Private_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_Private_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_Private_Total"></td>
<td class="visibleControls" id="PlacesJs_Private_W"></td>
</tr>
<tr>
<td></td>
<td>Total of Items 3 (a) to 3 (f)</td>
<td class="visibleControls" id="PlacesJs_Total_Blind" ></td>
<td class="visibleControls" id="PlacesJs_Total_Deaf"></td>
<td class="visibleControls" id="PlacesJs_Total_Orthopaedics"></td>
<td class="visibleControls" id="PlacesJs_Total_Respiratory"></td>
<td class="visibleControls" id="PlacesJs_Total_Leprosy"></td>
<td class="visibleControls" id="PlacesJs_Total_Total"></td>
<td class="visibleControls" id="PlacesJs_Total_W"></td>
</tr>
<tr>
<td>4</td>
<td>No.of Disabled applicants removed from the Live Register during the  half year.</td>
<td class="visibleControls" id="Registration_Removed_Blind" ></td>
<td class="visibleControls" id="Registration_Removed_Deaf"></td>
<td class="visibleControls" id="Registration_Removed_Orthopaedics"></td>
<td class="visibleControls" id="Registration_Removed_Respiratory"></td>
<td class="visibleControls" id="Registration_Removed_Leprosy"></td>
<td class="visibleControls" id="Registration_Removed_Total"></td>
<td class="visibleControls" id="Registration_Removed_W"></td>
</tr>
<tr>
<td>5</td>
<td>No. of Disable applicants on the Live Register at the end of the half year.  </td>
<td class="visibleControls" id="LR_Blind" ></td>
<td class="visibleControls" id="LR_Deaf"></td>
<td class="visibleControls" id="LR_Orthopaedics"></td>
<td class="visibleControls" id="LR_Respiratory"></td>
<td class="visibleControls" id="LR_Leprosy"></td>
<td class="visibleControls" id="LR_Total"></td>
<td class="visibleControls" id="LR_W"></td>
</tr>
<tr>
<td>6</td>
<td>No. of Submissions made during the half year.</td>
<td class="visibleControls" id="Submissions_Blind" ></td>
<td class="visibleControls" id="Submissions_Deaf"></td>
<td class="visibleControls" id="Submissions_Orthopaedics"></td>
<td class="visibleControls" id="Submissions_Respiratory"></td>
<td class="visibleControls" id="Submissions_Leprosy"></td>
<td class="visibleControls" id="Submissions_Total"></td>
<td class="visibleControls" id="Submissions_W"></td>
</tr>`;

$('#tbodyvalue').html(tbodyvalue);
ES25RptEntryForm();
}

if(rptid=='107'){
    $('.rptNametxt').text('E.S.2.5 Part II(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    
    $('#exnameess1rpt').text('View  E.S.2.5 Part II Reports ('+sessionStorage.Exname_es11+' )');

var tablehead=`	<tr>
<td  style='background: #716aca;color:white;'>S.no</td>
<td  style='background: #716aca;color:white;'>Item</td>
<td  style='background: #716aca;color:white;'>Central Govt.</td>
<td  style='background: #716aca;color:white;'>Union Territory</td>
<td  style='background: #716aca;color:white;'>State Govt.</td>
<td  style='background: #716aca;color:white;' colspan="2">Quasi-Govt. Estt./Public Sector undertaking</td>
<td  style='background: #716aca;color:white;' >Local Bodies</td>
<td  style='background: #716aca;color:white;'>Private Estts.</td>
<td  style='background: #716aca;color:white;'>Total</td>
</tr>
<tr>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'>Non-availability of suitable candidates</td>
<td  style='background: #716aca;color:white;'>Other reasons</td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
</tr>`
$('#tablehead').html(tablehead);
var tbodyvalue=`<tr>
<td>1</td>
<td> No.of vacancies for Disabled outstanding at the end of the prievious half year. </td>
<td id='Outstanding_Prev_CG' ></td>
<td id='Outstanding_Prev_UT' ></td>
<td id='Outstanding_Prev_SG' ></td>
<td id='Outstanding_Prev_Quasi_CG' ></td>
<td id='Outstanding_Prev_Quasi_SG' ></td>
<td id='Outstanding_Prev_LB' ></td>
<td id='Outstanding_Prev_Private' ></td>
<td id='Outstanding_Prev_Total' ></td>
</tr>
<tr>
<td>2</td>
<td> No. of vacancies for Disabled applicants
    a)Notified during the half year</td>
<td id='Notified_CG' ></td>
<td id='Notified_UT' ></td>
<td id='Notified_SG' ></td>
<td id='Notified_Quasi_CG' ></td>
<td id='Notified_Quasi_SG' ></td>
<td id='Notified_LB' ></td>
<td id='Notified_Private' ></td>
<td id='Notified_Total' ></td>
</tr>
<tr>
<td></td>
<td> b) filled during the half year</td>
<td id='Filled_CG' ></td>
<td id='Filled_UT' ></td>
<td id='Filled_SG' ></td>
<td id='Filled_Quasi_CG' ></td>
<td id='Filled_Quasi_SG' ></td>
<td id='Filled_LB' ></td>
<td id='Filled_Private' ></td>
<td id='Filled_Total' ></td>
</tr>
<tr>
<td></td>
<td> c) Cancelled during the half year due to :
    i)Non-availability of suitable candidates.</td>
<td id='Cancelled_NA_CG' ></td>
<td id='Cancelled_NA_UT' ></td>
<td id='Cancelled_NA_SG' ></td>
<td id='Cancelled_NA_Quasi_CG' ></td>
<td id='Cancelled_NA_Quasi_SG' ></td>
<td id='Cancelled_NA_LB' ></td>
<td id='Cancelled_NA_Private' ></td>
<td id='Cancelled_NA_Total' ></td>
</tr>
<tr>
<td></td>
<td>	ii)Other reasons </td>
<td id='Cancelled_Other_CG' ></td>
<td id='Cancelled_Other_UT' ></td>
<td id='Cancelled_Other_SG' ></td>
<td id='Cancelled_Other_Quasi_CG' ></td>
<td id='Cancelled_Other_Quasi_SG' ></td>
<td id='Cancelled_Other_LB' ></td>
<td id='Cancelled_Other_Private' ></td>
<td id='Cancelled_Other_Total' ></td>
</tr>
<tr>
<td>3</td>
<td> No. of vacancies for Disabled applicants outstanding at the end of the half year.  </td>
<td id='Outstanding_CG' ></td>
<td id='Outstanding_UT' ></td>
<td id='Outstanding_SG' ></td>
<td id='Outstanding_Quasi_CG' ></td>
<td id='Outstanding_Quasi_SG' ></td>
<td id='Outstanding_LB' ></td>
<td id='Outstanding_Private' ></td>
<td id='Outstanding_Total' ></td>
</tr>`;
$('#tbodyvalue').html(tbodyvalue);
ES25RptP2Submit();
}
if(rptid=='108'){
    $('.rptNametxt').text('E.S.1.4(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.2.1 Reports ('+sessionStorage.Exname_es11+' )');

    var tablehead="<tr><td  style='background: #716aca;color:white;'>S.NO</td><td  style='background: #716aca;color:white;' colspan=3>Eduational Level</td><td  style='background: #716aca;color:white;' colspan=3>Total Educated (All Categories)</td><td  style='background: #716aca;color:white;' colspan=3>Women (Included in Total)</td><td  style='background: #716aca;color:white;' colspan=3>SC (Included in Total)</td><td  style='background: #716aca;color:white;' colspan=3>ST (Included in Total)</td><td  style='background: #716aca;color:white;' colspan=3>OBC (Included in Total)</td></tr>"+
    "<tr><td  style='background: #716aca;color:white;'></td><td  style='background: #716aca;color:white;'></td><td  style='background: #716aca;color:white;'></td><td  style='background: #716aca;color:white;'></td><td  style='background: #716aca;color:white;'>Regd</td><td  style='background: #716aca;color:white;'>Placement</td><td  style='background: #716aca;color:white;'>LR</td></td><td  style='background: #716aca;color:white;'>Redg</td><td  style='background: #716aca;color:white;'>Placement</td><td  style='background: #716aca;color:white;'>LR</td><td  style='background: #716aca;color:white;'>Redg</td><td  style='background: #716aca;color:white;'>Placement</td><td  style='background: #716aca;color:white;'>LR</td><td  style='background: #716aca;color:white;'>Redg</td><td  style='background: #716aca;color:white;'>Placement</td><td  style='background: #716aca;color:white;'>LR</td><td  style='background: #716aca;color:white;'>Redg</td><td  style='background: #716aca;color:white;'>Placement</td><td  style='background: #716aca;color:white;'>LR</td></tr>";
$("#tablehead").html(tablehead);

FillES21_Master('parsedataFillES21_Master','tbodyvalue');

}
if(rptid=='109'){
    $('.rptNametxt').text('E.S.1.2(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return Showing by NCO work done by the Employment Exchange during the year ended:   ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.1.2 Reports ('+sessionStorage.Exname_es11+' )');

    var tablehead=`<tr>
    <td  style='background: #716aca;color:white;'></td>
    <td  style='background: #716aca;color:white;' colspan=6>No. of applicant on Live Register at the end of the year in respect of</td>
    <td  style='background: #716aca;color:white;' colspan=6>Notified during the year in respect of</td>
    <td  style='background: #716aca;color:white;' colspan=6>Filled during the year in respect of</td>
    <td  style='background: #716aca;color:white;' colspan=6>Cancelled during the year in respect of</td>
    <td  style='background: #716aca;color:white;' colspan=6>Outstanding at the end of the year in respect of</td>

    </tr>
<tr>
    <td  style='background: #716aca;color:white;'>NCO</td>
    <td  style='background: #716aca;color:white;'>Total</td>
    <td  style='background: #716aca;color:white;'>Woman LR</td>
    <td  style='background: #716aca;color:white;'>SC</td>
    <td  style='background: #716aca;color:white;'>ST</td>
    <td  style='background: #716aca;color:white;'>OBC</td>
    <td  style='background: #716aca;color:white;'>Disabled Persons</td>
    <td  style='background: #716aca;color:white;'>Total</td>
    <td  style='background: #716aca;color:white;'>Woman LR</td>
    <td  style='background: #716aca;color:white;'>SC</td>
    <td  style='background: #716aca;color:white;'>ST</td>
    <td  style='background: #716aca;color:white;'>OBC</td>
    <td  style='background: #716aca;color:white;'>Disabled Persons</td>
    <td  style='background: #716aca;color:white;'>Total</td>
    <td  style='background: #716aca;color:white;'>Woman LR</td>
    <td  style='background: #716aca;color:white;'>SC</td>
    <td  style='background: #716aca;color:white;'>ST</td>
    <td  style='background: #716aca;color:white;'>OBC</td>
    <td  style='background: #716aca;color:white;'>Disabled Persons</td>
    <td  style='background: #716aca;color:white;'>Total</td>
    <td  style='background: #716aca;color:white;'>Woman LR</td>
    <td  style='background: #716aca;color:white;'>SC</td>
    <td  style='background: #716aca;color:white;'>ST</td>
    <td  style='background: #716aca;color:white;'>OBC</td>
    <td  style='background: #716aca;color:white;'>Disabled Persons</td>
    <td  style='background: #716aca;color:white;'>Total</td>
    <td  style='background: #716aca;color:white;'>Woman LR</td>
    <td  style='background: #716aca;color:white;'>SC</td>
    <td  style='background: #716aca;color:white;'>ST</td>
    <td  style='background: #716aca;color:white;'>OBC</td>
    <td  style='background: #716aca;color:white;'>Disabled Persons</td>
  </tr>`;
    $("#tablehead").html(tablehead);
    FetchES12Entry();

}
if(rptid=='110'){
    $('.rptNametxt').text('E.S.1.3(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.1.3 Reports ('+sessionStorage.Exname_es11+' )');

    var tablehead=`	<tr>
    <td  style='background: #716aca;color:white;' colspan=1>S.NO</td>
    <td  style='background: #716aca;color:white;' colspan=1>Item</td>
    <td  style='background: #716aca;color:white;' colspan=1>Centrel Govt.</td>
    <td  style='background: #716aca;color:white;'>State Govt.</td>
    <td  style='background: #716aca;color:white;' colspan=1>UT</td>
    <td  style='background: #716aca;color:white;' colspan=2 >Quasi-Govt.Estt./Public Sector undertakings</td>
    <td  style='background: #716aca;color:white;' colspan=1>Local Bodies</td>
    <td  style='background: #716aca;color:white;' colspan=2>Private Sector</td>
    <td  style='background: #716aca;color:white;' colspan='2'>Total</td>
</tr>
<tr>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'>Central Govt.</td>
<td  style='background: #716aca;color:white;'>State Govt.</td>
<td  style='background: #716aca;color:white;'></td>
<td  style='background: #716aca;color:white;'>Falling within the Perview of Act</td>
<td  style='background: #716aca;color:white;'>Not Falling within the Perview of Act</td>
 <td  style='background: #716aca;color:white;'></td>
</tr>
`;
var tbodyvalue=`<tr><td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
<td>10</td>
<td>11</td>
</tr>
<tr>
<td>1</td>
<td>Vacancies remained outstanding at the end of the Previous year</td>
<td id="Outstanding_Prev_CG" >
</td>
<td  id="Outstanding_Prev_SG" >
</td>
<td id="Outstanding_Prev_UT" >
</td>
<td  id="Outstanding_Prev_Quasi_SG" >
</td>
<td id="Outstanding_Prev_Quasi_CG" >
</td>
<td  id="Outstanding_Prev_LB" >
</td>
<td id="Outstanding_Prev_Private_Act" >
</td>
<td  id="Outstanding_Prev_Private_NonAct" >
</td>
<td  id="Total_Prev_Outstanding" >
</td>
</tr>
<tr>
    <td>1a</td>
    <td>	Vacancies received on transfer from other exchanges during the year</td>
    <td id="Transfer_Receive_CG" >
    </td>
    <td  id="Transfer_Receive_SG" >
    </td>
    <td id="Transfer_Receive_UT" >
    </td>
    <td  id="Transfer_Receive_Quasi_SG" >
    </td>
    <td id="Transfer_Receive_Quasi_CG" >
    </td>
    <td  id="Transfer_Receive_LB" >
    </td>
    <td id="Transfer_Receive_Private_Act" >
    </td>
    <td  id="Transfer_Receive_Private_NonAct" >
    </td>
    <td  id="Total_Transfer_Receive" >
    </td>
</tr>
<tr>
        <td>2</td>
        <td>	Vacancies notified during the Year</td>
        <td id="Notified_CG" >
        </td>
        <td  id="Notified_SG" >
        </td>
        <td id="Notified_UT" >
        </td>
        <td  id="Notified_Quasi_SG" >
        </td>
        <td id="Notified_Quasi_CG" >
        </td>
        <td  id="Notified_LB" >
        </td>
        <td id="Notified_Private_Act" >
        </td>
        <td  id="Notified_Private_NonAct" >
        </td>
        <td  id="Total_Notified" >
        </td>
    </tr>
    <tr>
        <td>3</td>
        <td>Total(1+1a+2)</td>
        <td id="Total_1_1a_2_CG" >
        </td>
        <td  id="Total_1_1a_2_SG" >
        </td>
        <td id="Total_1_1a_2_UT" >
        </td>
        <td  id="Total_1_1a_2_Quasi_SG" >
        </td>
        <td id="Total_1_1a_2_Quasi_CG" >
        </td>
        <td  id="Total_1_1a_2_LB" >
        </td>
        <td id="Total_1_1a_2_Private_Act" >
        </td>
        <td  id="Total_1_1a_2_Private_NonAct" >
        </td>
        <td  id="Total_Total_1_1a_2" >
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>	Vacancies Filled during the Year</td>
        <td id="Filled_CG" >
        </td>
        <td  id="Filled_SG" >
        </td>
        <td id="Filled_UT" >
        </td>
        <td  id="Filled_Quasi_SG" >
        </td>
        <td id="Filled_Quasi_CG" >
        </td>
        <td  id="Filled_LB" >
        </td>
        <td id="Filled_Private_Act" >
        </td>
        <td  id="Filled_Private_NonAct" >
        </td>
        <td  id="Total_Filled" >
        </td>
    </tr>
    <tr>
            <td>5</td>
            <td>	Vacancies Cancelled during the Period</td>
            <td id="Cancelled_CG" >
            </td>
            <td  id="Cancelled_SG" >
            </td>
            <td id="Cancelled_UT" >
            </td>
            <td  id="Cancelled_Quasi_SG" >
            </td>
            <td id="Cancelled_Quasi_CG" >
            </td>
            <td  id="Cancelled_LB" >
            </td>
            <td id="Cancelled_Private_Act" >
            </td>
            <td  id="Cancelled_Private_NonAct" >
            </td>
            <td  id="Total_Cancelled" >
            </td>
        </tr>
        <tr>
                <td>5a</td>
                <td>	Vacancies transfered to other Exchanges during the Year</td>
                <td id="Transfer_CG" >
                </td>
                <td  id="Transfer_SG" >
                </td>
                <td id="Transfer_UT" >
                </td>
                <td  id="Transfer_Quasi_SG" >
                </td>
                <td id="Transfer_Quasi_CG" >
                </td>
                <td  id="Transfer_LB" >
                </td>
                <td id="Transfer_Private_Act" >
                </td>
                <td  id="Transfer_Private_NonAct" >
                </td>
                <td  id="Total_Transfer" >
                </td>
            </tr>
            <tr>
                    <td>6</td>
                    <td>		Vacancies Outstanding at the end of the Year</td>
                    <td id="Outstanding_CG" >
                    </td>
                    <td  id="Outstanding_SG" >
                    </td>
                    <td id="Outstanding_UT" >
                    </td>
                    <td  id="Outstanding_Quasi_SG" >
                    </td>
                    <td id="Outstanding_Quasi_CG" >
                    </td>
                    <td  id="Outstanding_LB" >
                    </td>
                    <td id="Outstanding_Private_Act" >
                    </td>
                    <td  id="Outstanding_Private_NonAct" >
                    </td>
                    <td  id="Total_Outstanding" >
                    </td>
                </tr>
                <tr>
                        <td>7</td>
                        <td>		Total(4+5+5a+6)	</td>
                        <td id="Total_4_5_5a_6_CG" >
                        </td>
                        <td  id="Total_4_5_5a_6_SG" >
                        </td>
                        <td id="Total_4_5_5a_6_UT" >
                        </td>
                        <td  id="Total_4_5_5a_6_Quasi_SG" >
                        </td>
                        <td id="Total_4_5_5a_6_Quasi_CG" >
                        </td>
                        <td  id="Total_4_5_5a_6_LB" >
                        </td>
                        <td id="Total_4_5_5a_6_Private_Act" >
                        </td>
                        <td  id="Total_4_5_5a_6_Private_NonAct" >
                        </td>
                        <td  id="Total_Total_4_5_5a_6" >
                        </td>
                    </tr>
                    <tr>
                            <td>7a</td>
                            <td>	Vacancies notified during the year by private employers falling within the preview of the Act against which submission is not required	</td>
                            <td id="Notified_NotSubmitted_CG" >
                            </td>
                            <td  id="Notified_NotSubmitted_SG" >
                            </td>
                            <td id="Notified_NotSubmitted_UT" >
                            </td>
                            <td  id="Notified_NotSubmitted_Quasi_SG" >
                            </td>
                            <td id="Notified_NotSubmitted_Quasi_CG" >
                            </td>
                            <td  id="Notified_NotSubmitted_LB" >
                            </td>
                            <td id="Notified_NotSubmitted_Private_Act" >
                            </td>
                            <td  id="Notified_NotSubmitted_Private_NonAct" >
                            </td>
                            <td  id="Total_Notified_NotSubmitted" >
                            </td>
                        </tr>
                        <tr>
                                <td>8</td>
                                <td>	Vacancies included in item 4 filled by the applicants of other exchange area during the year	</td>
                                <td id="Filled_ByOtherExch_CG" >
                                </td>
                                <td  id="Filled_ByOtherExch_SG" >
                                </td>
                                <td id="Filled_ByOtherExch_UT" >
                                </td>
                                <td  id="Filled_ByOtherExch_Quasi_SG" >
                                </td>
                                <td id="Filled_ByOtherExch_Quasi_CG" >
                                </td>
                                <td  id="Filled_ByOtherExch_LB" >
                                </td>
                                <td id="Filled_ByOtherExch_Private_Act" >
                                </td>
                                <td  id="Filled_ByOtherExch_Private_NonAct" >
                                </td>
                                <td  id="Total_Filled_ByOtherExch" >
                                </td>
                            </tr>
                            <tr>
                                    <td>9</td>
                                    <td>		Local applicants placed in other exchange during the year	</td>
                                    <td id="Filled_ForOtherExch_CG" >
                                    </td>
                                    <td  id="Filled_ForOtherExch_SG" >
                                    </td>
                                    <td id="Filled_ForOtherExch_UT" >
                                    </td>
                                    <td  id="Filled_ForOtherExch_Quasi_SG" >
                                    </td>
                                    <td id="Filled_ForOtherExch_Quasi_CG" >
                                    </td>
                                    <td  id="Filled_ForOtherExch_LB" >
                                    </td>
                                    <td id="Filled_ForOtherExch_Private_Act" >
                                    </td>
                                    <td  id="Filled_ForOtherExch_Private_NonAct" >
                                    </td>
                                    <td  id="Total_Filled_ForOtherExch" >
                                    </td>
                                </tr>	
                                <tr>
                                    <td>10</td>
                                    <td>No of employers using the exchange during the Year</td>
                                    <td id="EmpUseExch_CG" >
                                    </td>
                                    <td  id="EmpUseExch_SG" >
                                    </td>
                                    <td id="EmpUseExch_UT" >
                                    </td>
                                    <td  id="EmpUseExch_Quasi_SG" >
                                    </td>
                                    <td id="EmpUseExch_Quasi_CG" >
                                    </td>
                                    <td  id="EmpUseExch_LB" >
                                    </td>
                                    <td id="EmpUseExch_Private_Act" >
                                    </td>
                                    <td  id="EmpUseExch_Private_NonAct" >
                                    </td>
                                    <td  id="Total_EmpUseExch" >
                                    </td>
                                </tr>
                            
                                <tr>
                                        <td colspan="11"  style="text-align: end;" >Signature of Employment Officer<br>
                                            (With name and complete postal address of the Employment Exchange)
                                            
                                  </tr>`;
 $("#tablehead").html(tablehead);
     $("#tbodyvalue").html(tbodyvalue);
     ES13Form();

}
if(rptid=='113'){
    $('.rptNametxt').text('E.S.1.3(Half Yearly)');
    $('.exchangenametext').text(sessionStorage.Exname_es11);
    $('.descriptiontext').text(` Return Showing Trade-wise distribution of Craftmen trained at the ITIs and full term Apprentices trained under the apprenticeship Act, on the Live Registration by NCO at the end of the Year and their Number registrered & placed in employment during the year :From Date-01/01/${sessionStorage.yearrpt}-To Date- 31/12/
    ${sessionStorage.yearrpt}`)
    
    $('#exnameess1rpt').text('View  E.S.2.2 Reports ('+sessionStorage.Exname_es11+' )');

    var tablehead=`<tr >
    <td  style='background: #716aca;color:white;' >
        S.<br />
        No.</td>
    <td  style='background: #716aca;color:white;' >
        NCO Code (6 digit level &amp; totals at 4 digit level)</td>
    <td  style='background: #716aca;color:white;' >
        Trade in which Trained</td>
    <td  style='background: #716aca;color:white;'  colspan="3">
        Ex-ITI Trainees</td>
    <td  style='background: #716aca;color:white;' colspan="3">
        Full term Apprentices
    </td>
</tr>
<tr class="lblHeadingFontType">
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;'>
    </td>
    <td  style='background: #716aca;color:white;' >Registration</td>
    <td  style='background: #716aca;color:white;' >Placement</td>
    <td  style='background: #716aca;color:white;' >Live Register</td>
    <td  style='background: #716aca;color:white;' >Registration</td>
    <td  style='background: #716aca;color:white;' >Placement</td>
    <td  style='background: #716aca;color:white;' >Live Register</td>
</tr>

`;
$("#tablehead").html(tablehead);
fetchES22();

}
   }
   function fetchES22(){
    var todt=`${sessionStorage.yearrpt}-12-31`;
	var path =  serverpath + "getES22Entry/"+sessionStorage.ES11Exid+"/"+todt+"/1";
		ajaxget(path,'parsedatafetchES22','comment',"control"); 
}
  function parsedatafetchES22(data){  
      data = JSON.parse(data);
      var appendata='';
      var data1 = data[0];
      var isData=false;
        for (var i = 0; i < data1.length; i++) {
            if(data1[i].Flag=='Add'){
                isData=true;
            }
            appendata+=`	<tr class="tbl_row" id="${data1[i].Flag}_${i}">
            <td class="ES24_II_Id">${i+1}</td>
            <td class="NCO">${data1[i].NCO_Code}</td>
    
            <td>${data1[i].Subject}
            </td>
            <td ><input type="text"     class="TraineeReg" value='${data1[i].TraineeReg}' style="    width: 40px;" >
            </td>
            <td><input type="text"     class="TraineePlacement" value='${data1[i].TraineePlacement}' style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="TraineeLR" value='${data1[i].TraineeLR}'   style="    width: 40px;" > 
            </td>
            <td><input type="text"     class="ApprenticeReg" value='${data1[i].ApprenticeReg}'   style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="ApprenticePlace" value='${data1[i].ApprenticePlace}'   style="    width: 40px;" > 
            </td>
            <td ><input type="text"     class="ApprenticeLR" value='${data1[i].ApprenticeLR}'   style="    width: 40px;" > 
            </td>
      
       
    </td>
        </tr>`
          }
         if(isData) {

            $('#modalFile').modal('toggle');
            $(' #tbodyvalue, #tablehead').empty();
         }
        else{
           $('#tbodyvalue').html(appendata);
        }
  }
   function ES13Form(){
    var FromDt='';
    var ToDt='';
if(sessionStorage.criteria=='1'){
    FromDt=`${sessionStorage.yearrpt}-01-01`;
    ToDt=`${sessionStorage.yearrpt}-06-30`;
}
else if(sessionStorage.criteria=='2'){
    FromDt=`${sessionStorage.yearrpt}-07-01`;
    ToDt=`${sessionStorage.yearrpt}-12-31`;
}
    var MasterData = {
        
"p_Ex_Id":sessionStorage.ES11Exid,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Outstanding_Prev_CG": $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),
"p_Outstanding_Prev_SG": $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),
"p_Outstanding_Prev_UT": $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val(),
"p_Outstanding_Prev_Quasi_SG": $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val(),
"p_Outstanding_Prev_Quasi_CG": $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val(),
"p_Outstanding_Prev_LB": $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),
"p_Outstanding_Prev_Private_Act": $("#Outstanding_Prev_Private_Act").val()==''?0:$("#Outstanding_Prev_Private_Act").val(),
"p_Outstanding_Prev_Private_NonAct": $("#Outstanding_Prev_Private_NonAct").val()==''?0:$("#Outstanding_Prev_Private_NonAct").val(),
"p_Total_Prev_Outstanding": $("#Total_Prev_Outstanding").val()==''?0:$("#Total_Prev_Outstanding").val(),

"p_Transfer_Receive_CG": $("#Transfer_Receive_CG").val()==''?0:$("#Transfer_Receive_CG").val(),
"p_Transfer_Receive_SG": $("#Transfer_Receive_SG").val()==''?0:$("#Transfer_Receive_SG").val(),
"p_Transfer_Receive_UT": $("#Transfer_Receive_UT").val()==''?0:$("#Transfer_Receive_UT").val(),
"p_Transfer_Receive_Quasi_SG": $("#Transfer_Receive_Quasi_SG").val()==''?0:$("#Transfer_Receive_Quasi_SG").val(),
"p_Transfer_Receive_Quasi_CG": $("#Transfer_Receive_Quasi_CG").val()==''?0:$("#Transfer_Receive_Quasi_CG").val(),
"p_Transfer_Receive_LB": $("#Transfer_Receive_LB").val()==''?0:$("#Transfer_Receive_LB").val(),
"p_Transfer_Receive_Private_Act": $("#Transfer_Receive_Private_Act").val()==''?0:$("#Transfer_Receive_Private_Act").val(),
"p_Transfer_Receive_Private_NonAct": $("#Transfer_Receive_Private_NonAct").val()==''?0:$("#Transfer_Receive_Private_NonAct").val(),
"p_Total_Transfer_Receive": $("#Total_Transfer_Receive").val()==''?0:$("#Total_Transfer_Receive").val(),

"p_Notified_CG": $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),
"p_Notified_SG": $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),
"p_Notified_UT": $("#Notified_UT").val()==''?0:$("#Notified_UT").val(),
"p_Notified_Quasi_SG": $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val(),
"p_Notified_Quasi_CG": $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val(),
"p_Notified_LB": $("#Notified_LB").val()==''?0:$("#Notified_LB").val(),
"p_Notified_Private_Act": $("#Notified_Private_Act").val()==''?0:$("#Notified_Private_Act").val(),
"p_Notified_Private_NonAct": $("#Notified_Private_NonAct").val()==''?0:$("#Notified_Private_NonAct").val(),
"p_Total_Notified": $("#Total_Notified").val()==''?0:$("#Total_Notified").val(),

"p_Total_1_1a_2_CG": $("#Total_1_1a_2_CG").val()==''?0:$("#Total_1_1a_2_CG").val(),
"p_Total_1_1a_2_SG": $("#Total_1_1a_2_SG").val()==''?0:$("#Total_1_1a_2_SG").val(),
"p_Total_1_1a_2_UT": $("#Total_1_1a_2_UT").val()==''?0:$("#Total_1_1a_2_UT").val(),
"p_Total_1_1a_2_Quasi_SG": $("#Total_1_1a_2_Quasi_SG").val()==''?0:$("#Total_1_1a_2_Quasi_SG").val(),
"p_Total_1_1a_2_Quasi_CG": $("#Total_1_1a_2_Quasi_CG").val()==''?0:$("#Total_1_1a_2_Quasi_CG").val(),
"p_Total_1_1a_2_LB": $("#Total_1_1a_2_LB").val()==''?0:$("#Total_1_1a_2_LB").val(),
"p_Total_1_1a_2_Private_Act": $("#Total_1_1a_2_Private_Act").val()==''?0:$("#Total_1_1a_2_Private_Act").val(),
"p_Total_1_1a_2_Private_NonAct": $("#Total_1_1a_2_Private_NonAct").val()==''?0:$("#Total_1_1a_2_Private_NonAct").val(),
"p_Total_Total_1_1a_2": $("#Total_Total_1_1a_2").val()==''?0:$("#Total_Total_1_1a_2").val(),

"p_Filled_CG": $("#Filled_CG").val()==''?0:$("#Filled_CG").val(),
"p_Filled_SG": $("#Filled_SG").val()==''?0:$("#Filled_SG").val(),
"p_Filled_UT": $("#Filled_UT").val()==''?0:$("#Filled_UT").val(),
"p_Filled_Quasi_SG": $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val(),
"p_Filled_Quasi_CG": $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val(),
"p_Filled_LB": $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),
"p_Filled_Private_Act": $("#Filled_Private_Act").val()==''?0:$("#Filled_Private_Act").val(),
"p_Filled_Private_NonAct": $("#Filled_Private_NonAct").val()==''?0:$("#Filled_Private_NonAct").val(),
"p_Total_Filled": $("#Total_Filled").val()==''?0:$("#Total_Filled").val(),

"p_Cancelled_CG": $("#Cancelled_CG").val()==''?0:$("#Cancelled_CG").val(),
"p_Cancelled_SG": $("#Cancelled_SG").val()==''?0:$("#Cancelled_SG").val(),
"p_Cancelled_UT": $("#Cancelled_UT").val()==''?0:$("#Cancelled_UT").val(),
"p_Cancelled_Quasi_SG": $("#Cancelled_Quasi_SG").val()==''?0:$("#Cancelled_Quasi_SG").val(),
"p_Cancelled_Quasi_CG": $("#Cancelled_Quasi_CG").val()==''?0:$("#Cancelled_Quasi_CG").val(),
"p_Cancelled_LB": $("#Cancelled_LB").val()==''?0:$("#Cancelled_LB").val(),
"p_Cancelled_Private_Act": $("#Cancelled_Private_Act").val()==''?0:$("#Cancelled_Private_Act").val(),
"p_Cancelled_Private_NonAct": $("#Cancelled_Private_NonAct").val()==''?0:$("#Cancelled_Private_NonAct").val(),
"p_Total_Cancelled": $("#Total_Cancelled").val()==''?0:$("#Total_Cancelled").val(),

"p_Transfer_CG": $("#Transfer_CG").val()==''?0:$("#Transfer_CG").val(),
"p_Transfer_SG": $("#Transfer_SG").val()==''?0:$("#Transfer_SG").val(),
"p_Transfer_UT": $("#Transfer_UT").val()==''?0:$("#Transfer_UT").val(),
"p_Transfer_Quasi_SG": $("#Transfer_Quasi_SG").val()==''?0:$("#Transfer_Quasi_SG").val(),
"p_Transfer_Quasi_CG": $("#Transfer_Quasi_CG").val()==''?0:$("#Transfer_Quasi_CG").val(),
"p_Transfer_LB": $("#Transfer_LB").val()==''?0:$("#Transfer_LB").val(),
"p_Transfer_Private_Act": $("#Transfer_Private_Act").val()==''?0:$("#Transfer_Private_Act").val(),
"p_Transfer_Private_NonAct": $("#Transfer_Private_NonAct").val()==''?0:$("#Transfer_Private_NonAct").val(),
"p_Total_Transfer": $("#Total_Transfer").val()==''?0:$("#Total_Transfer").val(),

"p_Outstanding_CG": $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val(),
"p_Outstanding_SG": $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val(),
"p_Outstanding_UT": $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),
"p_Outstanding_Quasi_SG": $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val(),
"p_Outstanding_Quasi_CG": $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),
"p_Outstanding_LB": $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val(),
"p_Outstanding_Private_Act": $("#Outstanding_Private_Act").val()==''?0:$("#Outstanding_Private_Act").val(),
"p_Outstanding_Private_NonAct": $("#Outstanding_Private_NonAct").val()==''?0:$("#Outstanding_Private_NonAct").val(),
"p_Total_Outstanding": $("#Total_Outstanding").val()==''?0:$("#Total_Outstanding").val(),

"p_Total_4_5_5a_6_CG": $("#Total_4_5_5a_6_CG").val()==''?0:$("#Total_4_5_5a_6_CG").val(),
"p_Total_4_5_5a_6_SG": $("#Total_4_5_5a_6_SG").val()==''?0:$("#Total_4_5_5a_6_SG").val(),
"p_Total_4_5_5a_6_UT": $("#Total_4_5_5a_6_UT").val()==''?0:$("#Total_4_5_5a_6_UT").val(),
"p_Total_4_5_5a_6_Quasi_SG": $("#Total_4_5_5a_6_Quasi_SG").val()==''?0:$("#Total_4_5_5a_6_Quasi_SG").val(),
"p_Total_4_5_5a_6_Quasi_CG": $("#Total_4_5_5a_6_Quasi_CG").val()==''?0:$("#Total_4_5_5a_6_Quasi_CG").val(),
"p_Total_4_5_5a_6_LB": $("#Total_4_5_5a_6_LB").val()==''?0:$("#Total_4_5_5a_6_LB").val(),
"p_Total_4_5_5a_6_Private_Act": $("#Total_4_5_5a_6_Private_Act").val()==''?0:$("#Total_4_5_5a_6_Private_Act").val(),
"p_Total_4_5_5a_6_Private_NonAct": $("#Total_4_5_5a_6_Private_NonAct").val()==''?0:$("#Total_4_5_5a_6_Private_NonAct").val(),
"p_Total_Total_4_5_5a_6": $("#Total_Total_4_5_5a_6").val()==''?0:$("#Total_Total_4_5_5a_6").val(),


"p_Notified_NotSubmitted_CG": $("#Notified_NotSubmitted_CG").val()==''?0:$("#Notified_NotSubmitted_CG").val(),
"p_Notified_NotSubmitted_SG": $("#Notified_NotSubmitted_SG").val()==''?0:$("#Notified_NotSubmitted_SG").val(),
"p_Notified_NotSubmitted_UT": $("#Notified_NotSubmitted_UT").val()==''?0:$("#Notified_NotSubmitted_UT").val(),
"p_Notified_NotSubmitted_Quasi_SG": $("#Notified_NotSubmitted_Quasi_SG").val()==''?0:$("#Notified_NotSubmitted_Quasi_SG").val(),
"p_Notified_NotSubmitted_Quasi_CG": $("#Notified_NotSubmitted_Quasi_CG").val()==''?0:$("#Notified_NotSubmitted_Quasi_CG").val(),
"p_Notified_NotSubmitted_LB": $("#Notified_NotSubmitted_LB").val()==''?0:$("#Notified_NotSubmitted_LB").val(),
"p_Notified_NotSubmitted_Private_Act": $("#Notified_NotSubmitted_Private_Act").val()==''?0:$("#Notified_NotSubmitted_Private_Act").val(),
"p_Notified_NotSubmitted_Private_NonAct": $("#Notified_NotSubmitted_Private_NonAct").val()==''?0:$("#Notified_NotSubmitted_Private_NonAct").val(),
"p_Total_Notified_NotSubmitted": $("#Total_Notified_NotSubmitted").val()==''?0:$("#Total_Notified_NotSubmitted").val(),


"p_Filled_ByOtherExch_CG": $("#Filled_ByOtherExch_CG").val()==''?0:$("#Filled_ByOtherExch_CG").val(),
"p_Filled_ByOtherExch_SG": $("#Filled_ByOtherExch_SG").val()==''?0:$("#Filled_ByOtherExch_SG").val(),
"p_Filled_ByOtherExch_UT": $("#Filled_ByOtherExch_UT").val()==''?0:$("#Filled_ByOtherExch_UT").val(),
"p_Filled_ByOtherExch_Quasi_SG": $("#Filled_ByOtherExch_Quasi_SG").val()==''?0:$("#Filled_ByOtherExch_Quasi_SG").val(),
"p_Filled_ByOtherExch_Quasi_CG": $("#Filled_ByOtherExch_Quasi_CG").val()==''?0:$("#Filled_ByOtherExch_Quasi_CG").val(),
"p_Filled_ByOtherExch_LB": $("#Filled_ByOtherExch_LB").val()==''?0:$("#Filled_ByOtherExch_LB").val(),
"p_Filled_ByOtherExch_Private_Act": $("#Filled_ByOtherExch_Private_Act").val()==''?0:$("#Filled_ByOtherExch_Private_Act").val(),
"p_Filled_ByOtherExch_Private_NonAct": $("#Filled_ByOtherExch_Private_NonAct").val()==''?0:$("#Filled_ByOtherExch_Private_NonAct").val(),
"p_Total_Filled_ByOtherExch": $("#Total_Filled_ByOtherExch").val()==''?0:$("#Total_Filled_ByOtherExch").val(),

"p_Filled_ForOtherExch_CG": $("#Filled_ForOtherExch_CG").val()==''?0:$("#Filled_ForOtherExch_CG").val(),
"p_Filled_ForOtherExch_SG": $("#Filled_ForOtherExch_SG").val()==''?0:$("#Filled_ForOtherExch_SG").val(),
"p_Filled_ForOtherExch_UT": $("#Filled_ForOtherExch_UT").val()==''?0:$("#Filled_ForOtherExch_UT").val(),
"p_Filled_ForOtherExch_Quasi_SG": $("#Filled_ForOtherExch_Quasi_SG").val()==''?0:$("#Filled_ForOtherExch_Quasi_SG").val(),
"p_Filled_ForOtherExch_Quasi_CG": $("#Filled_ForOtherExch_Quasi_CG").val()==''?0:$("#Filled_ForOtherExch_Quasi_CG").val(),
"p_Filled_ForOtherExch_LB": $("#Filled_ForOtherExch_LB").val()==''?0:$("#Filled_ForOtherExch_LB").val(),
"p_Filled_ForOtherExch_Private_Act": $("#Filled_ForOtherExch_Private_Act").val()==''?0:$("#Filled_ForOtherExch_Private_Act").val(),
"p_Filled_ForOtherExch_Private_NonAct": $("#Filled_ForOtherExch_Private_NonAct").val()==''?0:$("#Filled_ForOtherExch_Private_NonAct").val(),
"p_Total_Filled_ForOtherExch": $("#Total_Filled_ForOtherExch").val()==''?0:$("#Total_Filled_ForOtherExch").val(),

"p_EmpUseExch_CG": $("#EmpUseExch_CG").val()==''?0:$("#EmpUseExch_CG").val(),
"p_EmpUseExch_SG": $("#EmpUseExch_SG").val()==''?0:$("#EmpUseExch_SG").val(),
"p_EmpUseExch_UT": $("#EmpUseExch_UT").val()==''?0:$("#EmpUseExch_UT").val(),
"p_EmpUseExch_Quasi_SG": $("#EmpUseExch_Quasi_SG").val()==''?0:$("#EmpUseExch_Quasi_SG").val(),
"p_EmpUseExch_Quasi_CG": $("#EmpUseExch_Quasi_CG").val()==''?0:$("#EmpUseExch_Quasi_CG").val(),
"p_EmpUseExch_LB": $("#EmpUseExch_LB").val()==''?0:$("#EmpUseExch_LB").val(),
"p_EmpUseExch_Private_Act": $("#EmpUseExch_Private_Act").val()==''?0:$("#EmpUseExch_Private_Act").val(),
"p_EmpUseExch_Private_NonAct": $("#EmpUseExch_Private_NonAct").val()==''?0:$("#EmpUseExch_Private_NonAct").val(),
"p_Total_EmpUseExch": $("#Total_EmpUseExch").val()==''?0:$("#Total_EmpUseExch").val(),

"p_Flag" :'3',
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "ES13Entry";
  securedajaxpost(path,'parsrdataES13EntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES13EntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES25RptEntryForm();
        
    }
      if(data[0][0]){

                $("#Outstanding_Prev_CG").text(data[0][0].Outstanding_Prev_CG),
                $("#Outstanding_Prev_SG").text(data[0][0].Outstanding_Prev_SG),
                $("#Outstanding_Prev_UT").text(data[0][0].Outstanding_Prev_UT),
                $("#Outstanding_Prev_Quasi_SG").text(data[0][0].Outstanding_Prev_Quasi_SG),
                $("#Outstanding_Prev_Quasi_CG").text(data[0][0].Outstanding_Prev_Quasi_CG),
                $("#Outstanding_Prev_LB").text(data[0][0].Outstanding_Prev_LB),
                $("#Outstanding_Prev_Private_Act").text(data[0][0].Outstanding_Prev_Private_Act),
                $("#Outstanding_Prev_Private_NonAct").text(data[0][0].Outstanding_Prev_Private_NonAct),
                $("#Total_Prev_Outstanding").text(data[0][0].Total_Prev_Outstanding),
            
                $("#Transfer_Receive_CG").text(data[0][0].Transfer_Receive_CG),
                $("#Transfer_Receive_SG").text(data[0][0].Transfer_Receive_SG),
                $("#Transfer_Receive_UT").text(data[0][0].Transfer_Receive_UT),
                $("#Transfer_Receive_Quasi_SG").text(data[0][0].Transfer_Receive_Quasi_SG),
                $("#Transfer_Receive_Quasi_CG").text(data[0][0].Transfer_Receive_Quasi_CG),
                $("#Transfer_Receive_LB").text(data[0][0].Transfer_Receive_LB),
                $("#Transfer_Receive_Private_Act").text(data[0][0].Transfer_Receive_Private_Act),
                $("#Transfer_Receive_Private_NonAct").text(data[0][0].Transfer_Receive_Private_NonAct),
                $("#Total_Transfer_Receive").text(data[0][0].Total_Transfer_Receive),
            
                $("#Notified_CG").text(data[0][0].Notified_CG),
                $("#Notified_SG").text(data[0][0].Notified_SG),
                $("#Notified_UT").text(data[0][0].Notified_UT),
                $("#Notified_Quasi_SG").text(data[0][0].Notified_Quasi_SG),
                $("#Notified_Quasi_CG").text(data[0][0].Notified_Quasi_CG),
                $("#Notified_LB").text(data[0][0].Notified_LB),
                $("#Notified_Private_Act").text(data[0][0].Notified_Private_Act),
                $("#Notified_Private_NonAct").text(data[0][0].Notified_Private_NonAct),
                $("#Total_Notified").text(data[0][0].Total_Notified),
            
                $("#Total_1_1a_2_CG").text(data[0][0].Total_1_1a_2_CG),
                $("#Total_1_1a_2_SG").text(data[0][0].Total_1_1a_2_SG),
                $("#Total_1_1a_2_UT").text(data[0][0].Total_1_1a_2_UT),
                $("#Total_1_1a_2_Quasi_SG").text(data[0][0].Total_1_1a_2_Quasi_SG),
                $("#Total_1_1a_2_Quasi_CG").text(data[0][0].Total_1_1a_2_Quasi_CG),
                $("#Total_1_1a_2_LB").text(data[0][0].Total_1_1a_2_LB),
                $("#Total_1_1a_2_Private_Act").text(data[0][0].Total_1_1a_2_Private_Act),
                $("#Total_1_1a_2_Private_NonAct").text(data[0][0].Total_1_1a_2_Private_NonAct),
                $("#Total_Total_1_1a_2").text(data[0][0].Total_Total_1_1a_2),
            
                $("#Filled_CG").text(data[0][0].Filled_CG),
                $("#Filled_SG").text(data[0][0].Filled_SG),
                $("#Filled_UT").text(data[0][0].Filled_UT),
                $("#Filled_Quasi_SG").text(data[0][0].Filled_Quasi_SG),
                $("#Filled_Quasi_CG").text(data[0][0].Filled_Quasi_CG),
                $("#Filled_LB").text(data[0][0].Filled_LB),
                $("#Filled_Private_Act").text(data[0][0].Filled_Private_Act),
                $("#Filled_Private_NonAct").text(data[0][0].Filled_Private_NonAct),
                $("#Total_Filled").text(data[0][0].Total_Filled),
            
                $("#Cancelled_CG").text(data[0][0].Cancelled_CG),
                $("#Cancelled_SG").text(data[0][0].Cancelled_SG),
                $("#Cancelled_UT").text(data[0][0].Cancelled_UT),
                $("#Cancelled_Quasi_SG").text(data[0][0].Cancelled_Quasi_SG),
                $("#Cancelled_Quasi_CG").text(data[0][0].Cancelled_Quasi_CG),
                $("#Cancelled_LB").text(data[0][0].Cancelled_LB),
                $("#Cancelled_Private_Act").text(data[0][0].Cancelled_Private_Act),
                $("#Cancelled_Private_NonAct").text(data[0][0].Cancelled_Private_NonAct),
                $("#Total_Cancelled").text(data[0][0].Total_Cancelled),
            
                $("#Transfer_CG").text(data[0][0].Transfer_CG),
                $("#Transfer_SG").text(data[0][0].Transfer_SG),
                $("#Transfer_UT").text(data[0][0].Transfer_UT),
                $("#Transfer_Quasi_SG").text(data[0][0].Transfer_Quasi_SG),
                $("#Transfer_Quasi_CG").text(data[0][0].Transfer_Quasi_CG),
                $("#Transfer_LB").text(data[0][0].Transfer_LB),
                $("#Transfer_Private_Act").text(data[0][0].Transfer_Private_Act),
                $("#Transfer_Private_NonAct").text(data[0][0].Transfer_Private_NonAct),
                $("#Total_Transfer").text(data[0][0].Total_Transfer),
            
                $("#Outstanding_CG").text(data[0][0].Outstanding_CG),
                $("#Outstanding_SG").text(data[0][0].Outstanding_SG),
                $("#Outstanding_UT").text(data[0][0].Outstanding_UT),
                $("#Outstanding_Quasi_SG").text(data[0][0].Outstanding_Quasi_SG),
                $("#Outstanding_Quasi_CG").text(data[0][0].Outstanding_Quasi_CG),
                $("#Outstanding_LB").text(data[0][0].Outstanding_LB),
                $("#Outstanding_Private_Act").text(data[0][0].Outstanding_Private_Act),
                $("#Outstanding_Private_NonAct").text(data[0][0].Outstanding_Private_NonAct),
                $("#Total_Outstanding").text(data[0][0].Total_Outstanding),
            
                $("#Total_4_5_5a_6_CG").text(data[0][0].Total_4_5_5a_6_CG),
                $("#Total_4_5_5a_6_SG").text(data[0][0].Total_4_5_5a_6_SG),
                $("#Total_4_5_5a_6_UT").text(data[0][0].Total_4_5_5a_6_UT),
                $("#Total_4_5_5a_6_Quasi_SG").text(data[0][0].Total_4_5_5a_6_Quasi_SG),
                $("#Total_4_5_5a_6_Quasi_CG").text(data[0][0].Total_4_5_5a_6_Quasi_CG),
                $("#Total_4_5_5a_6_LB").text(data[0][0].Total_4_5_5a_6_LB),
                $("#Total_4_5_5a_6_Private_Act").text(data[0][0].Total_4_5_5a_6_Private_Act),
                $("#Total_4_5_5a_6_Private_NonAct").text(data[0][0].Total_4_5_5a_6_Private_NonAct),
                $("#Total_Total_4_5_5a_6").text(data[0][0].Total_Total_4_5_5a_6),
            
                $("#Notified_NotSubmitted_CG").text(data[0][0].Notified_NotSubmitted_CG),
                $("#Notified_NotSubmitted_SG").text(data[0][0].Notified_NotSubmitted_SG),
                $("#Notified_NotSubmitted_UT").text(data[0][0].Notified_NotSubmitted_UT),
                $("#Notified_NotSubmitted_Quasi_SG").text(data[0][0].Notified_NotSubmitted_Quasi_SG),
                $("#Notified_NotSubmitted_Quasi_CG").text(data[0][0].Notified_NotSubmitted_Quasi_CG),
                $("#Notified_NotSubmitted_LB").text(data[0][0].Notified_NotSubmitted_LB),
                $("#Notified_NotSubmitted_Private_Act").text(data[0][0].Notified_NotSubmitted_Private_Act),
                $("#Notified_NotSubmitted_Private_NonAct").text(data[0][0].Notified_NotSubmitted_Private_NonAct),
                $("#Total_Notified_NotSubmitted").text(data[0][0].Total_Notified_NotSubmitted),
            
                $("#Filled_ByOtherExch_CG").text(data[0][0].Filled_ByOtherExch_CG),
                $("#Filled_ByOtherExch_SG").text(data[0][0].Filled_ByOtherExch_SG),
                $("#Filled_ByOtherExch_UT").text(data[0][0].Filled_ByOtherExch_UT),
                $("#Filled_ByOtherExch_Quasi_SG").text(data[0][0].Filled_ByOtherExch_Quasi_SG),
                $("#Filled_ByOtherExch_Quasi_CG").text(data[0][0].Filled_ByOtherExch_Quasi_CG),
                $("#Filled_ByOtherExch_LB").text(data[0][0].Filled_ByOtherExch_LB),
                $("#Filled_ByOtherExch_Private_Act").text(data[0][0].Filled_ByOtherExch_Private_Act),
                $("#Filled_ByOtherExch_Private_NonAct").text(data[0][0].Filled_ByOtherExch_Private_NonAct),
                $("#Total_Filled_ByOtherExch").text(data[0][0].Total_Filled_ByOtherExch),
            
                $("#Filled_ForOtherExch_CG").text(data[0][0].Filled_ForOtherExch_CG),
                $("#Filled_ForOtherExch_SG").text(data[0][0].Filled_ForOtherExch_SG),
                $("#Filled_ForOtherExch_UT").text(data[0][0].Filled_ForOtherExch_UT),
                $("#Filled_ForOtherExch_Quasi_SG").text(data[0][0].Filled_ForOtherExch_Quasi_SG),
                $("#Filled_ForOtherExch_Quasi_CG").text(data[0][0].Filled_ForOtherExch_Quasi_CG),
                $("#Filled_ForOtherExch_LB").text(data[0][0].Filled_ForOtherExch_LB),
                $("#Filled_ForOtherExch_Private_Act").text(data[0][0].Filled_ForOtherExch_Private_Act),
                $("#Filled_ForOtherExch_Private_NonAct").text(data[0][0].Filled_ForOtherExch_Private_NonAct),
                $("#Total_Filled_ForOtherExch").text(data[0][0].Total_Filled_ForOtherExch),
            
                $("#EmpUseExch_CG").text(data[0][0].EmpUseExch_CG),
                $("#EmpUseExch_SG").text(data[0][0].EmpUseExch_SG),
                $("#EmpUseExch_UT").text(data[0][0].EmpUseExch_UT),
                $("#EmpUseExch_Quasi_SG").text(data[0][0].EmpUseExch_Quasi_SG),
                $("#EmpUseExch_Quasi_CG").text(data[0][0].EmpUseExch_Quasi_CG),
                $("#EmpUseExch_LB").text(data[0][0].EmpUseExch_LB),
                $("#EmpUseExch_Private_Act").text(data[0][0].EmpUseExch_Private_Act),
                $("#EmpUseExch_Private_NonAct").text(data[0][0].EmpUseExch_Private_NonAct),
                $("#Total_EmpUseExch").text(data[0][0].Total_EmpUseExch)
      }
      else{
        $('#modalFile').modal('toggle');
        $(' #tbodyvalue, #tablehead').empty();
      }
    
  
 

     }
   function FetchES12Entry(){
   
    var FromDt;
    var ToDt;
	if(sessionStorage.criteria=='1'){
		FromDt=`${sessionStorage.yearrpt}-01-01`;
		ToDt=`${sessionStorage.yearrpt}-06-30`;
	}
	else if(sessionStorage.criteria=='2'){
		FromDt=`${sessionStorage.yearrpt}-07-01`;
		ToDt=`${sessionStorage.yearrpt}-12-31`;
	}
    var MasterData = {
        
        "p_Ex_id":sessionStorage.ES11Exid,
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
		"p_NCO":"0",        
		"p_Women_LR":"0",           
		"p_SC_LR":"0",          
		"p_ST_LR":"0",          
		"p_OBC_LR":"0",          
		"p_Disabled_LR":"0",          
		"p_Total_LR":"0",
       
		"p_Women_Notify":"0",           
		"p_SC_Notify":"0",          
		"p_ST_Notify":"0",          
		"p_OBC_Notify":"0",          
		"p_Disabled_Notify":"0",          
		"p_Total_Notify":"0",

		"p_Women_Filled":"0",           
		"p_SC_Filled":"0",          
		"p_ST_Filled":"0",          
		"p_OBC_Filled":"0",          
		"p_Disabled_Filled":"0",          
		"p_Total_Filled":"0",

		"p_Women_Cancelled":"0",           
		"p_SC_Cancelled":"0",          
		"p_ST_Cancelled":"0",          
		"p_OBC_Cancelled":"0",          
		"p_Disabled_Cancelled":"0",          
		"p_Total_Cancelled":"0",

		"p_Women_Outstanding":"0",           
		"p_SC_Outstanding":"0",          
		"p_ST_Outstanding":"0",          
		"p_OBC_Outstanding":"0",          
		"p_Disabled_Outstanding":"0",          
		"p_Total_Outstanding":"0",

		"p_Verify_YN":'0',         
		"p_Flag":'5',        
		"p_ES12_Id":'0'
	
	}     
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "ES12_RptEntry";
	securedajaxpost(path,'parsedataES12Rpt','comment',MasterData,'control')   
}

function parsedataES12Rpt(data){
	data = JSON.parse(data);
	var data1=data[0];
	var appenddata="";
	
		if(data1.length>0){
			
			for (var i = 0; i < data1.length; i++) {
				appenddata+=`<tr class='addrow modify' id="modify_${data1[i].ES12_Id}">
				
                <td>${data1[i].NCO}</td>
                <td>${data1[i].Total_LR}</td>
				<td>${data1[i].Women_LR}</td>
				<td>${data1[i].SC_LR}</td>
				<td>${data1[i].ST_LR}</td>
				<td>${data1[i].OBC_LR}</td>
                <td>${data1[i].Disabled_LR}</td>
                
				<td>${data1[i].Total_Notify}</td>
				<td>${data1[i].Women_Notify}</td>
				<td>${data1[i].SC_Notify}</td>
				<td>${data1[i].ST_Notify}</td>
				<td>${data1[i].OBC_Notify}</td>
				<td>${data1[i].Disabled_Notify}</td>
                
                <td>${data1[i].Total_Filled}</td>
				<td>${data1[i].Women_Filled}</td>
				<td>${data1[i].SC_Filled}</td>
				<td>${data1[i].ST_Filled}</td>
				<td>${data1[i].OBC_Filled}</td>
				<td>${data1[i].Disabled_Filled}</td>
                
                <td>${data1[i].Total_Cancelled}</td>
				<td>${data1[i].Women_Cancelled}</td>
				<td>${data1[i].SC_Cancelled}</td>
				<td>${data1[i].ST_Cancelled}</td>
				<td>${data1[i].OBC_Cancelled}</td>
				<td>${data1[i].Disabled_Cancelled}</td>
                
                <td>${data1[i].Total_Outstanding}</td>
				<td>${data1[i].Women_Outstanding}</td>
				<td>${data1[i].SC_Outstanding}</td>
				<td>${data1[i].ST_Outstanding}</td>
				<td>${data1[i].OBC_Outstanding}</td>
				<td>${data1[i].Disabled_Outstanding}</td>
				
				</tr>`;
	}
	
	$('#tbodyvalue').html(appenddata);
		}
else{
    $('#modalFile').modal('toggle');
    $(' #tbodyvalue, #tablehead').empty();
}
}
function ES14Rpt(){
   
        var todate = ''+sessionStorage.yearrpt+'-12-31'


   
        var MasterData = {
  
            "p_Ex_Id": sessionStorage.ES11Exid,  
            "p_ToDt":  todate ,  
            "p_Edu_id":0, 
            "p_Upto19":0,
            "p_Age_20_29":0, 
            "p_Age_30_39":0, 
            "p_Age_40_49":0,  
            "p_Age_50_59":0,  
            "p_Age_60_above":0,  
           "p_Flag":3, 
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "ES14Entry";
        securedajaxpost(path,'parsrdataES14Entry','comment',MasterData,'control')
  
    
    }

function parsrdataES14Entry(data){
    data = JSON.parse(data)
   
 if(data[0].length){
    for (var i = 0; i < data[0].length; i++) {
$('#upto19'+data[0][i].Edu_id+'').text(data[0][i].Upto19);
$('#Age_20_29'+data[0][i].Edu_id+'').text(data[0][i].Age_20_29);
$('#Age_30_39'+data[0][i].Edu_id+'').text(data[0][i].Age_30_39);
$('#Age_40_49'+data[0][i].Edu_id+'').text(data[0][i].Age_40_49);
$('#Age_50_59'+data[0][i].Edu_id+'').text(data[0][i].Age_50_59);
$('#Age_60_above'+data[0][i].Edu_id+'').text(data[0][i].Age_60_above);
$('#total'+data[0][i].Edu_id+'').text(data[0][i].total);
    }
}
    else{
        $('#modalFile').modal('toggle');
        $(' #tbodyvalue, #tablehead').empty();
    }
}
function FillEduationalLevel(funct,control) {
    var path =  serverpath + "Edu_with_Gender"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedataFillEduationalLevel(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEduationalLevel('parsedataFillEduationalLevel','tbodyvalue');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            var appenddata = ''
            var tablehead="<tr><td  style='background: #716aca;color:white;'>S. No</td><td  style='background: #716aca;color:white;'>Eduational Level</td><td  style='background: #716aca;color:white;'>Sex</td><td  style='background: #716aca;color:white;'>Up to 19</td><td  style='background: #716aca;color:white;'>20-29</td><td  style='background: #716aca;color:white;'>30-39</td><td  style='background: #716aca;color:white;'>40-49</td><td  style='background: #716aca;color:white;'>50-59</td><td  style='background: #716aca;color:white;'>60 and above</td><td  style='background: #716aca;color:white;'>Total</td></tr>";
            $("#tablehead").html(tablehead);

            for (var i = 0; i < data1.length; i++) {
                if(data1[i].Gender=='M'){
                    var gender= 'Male'
                    var EduName= data1[i].Edu_Name
                   }
                    else if(data1[i].Gender=='F'){
                   var gender='Female'
                   var EduName=''
                      }
                      else if(data1[i].Gender=='B'){
                          var gender='Both'
                          var EduName=''
                             }

              appenddata+= `<tr class='i'>
              <td><label class='eduid'>`+data1[i].Edu_id+`</label></td>
              <td>`+EduName+`</td>
          
              <td > `+gender+`
              </td>
              <td    id='upto19`+data1[i].Edu_id+`' >
              </td>
              <td     id='Age_20_29`+data1[i].Edu_id+`' >
              </td>
              <td  id='Age_30_39`+data1[i].Edu_id+`'    >
              </td>
              <td    id='Age_40_49`+data1[i].Edu_id+`' >
              </td>
              <td   id='Age_50_59`+data1[i].Edu_id+`'  >
              </td>
              <td  id='Age_60_above`+data1[i].Edu_id+`'   >
              </td>
              <td  id="total`+data1[i].Edu_id+`"  >
              </td>
          </tr>`
             }
             $("#tbodyvalue").append(appenddata);
             ES14Rpt();
        }
              
  }


  function ES23RptEntryForm(){
    var FromDt='';
    var ToDt='';
 
if(sessionStorage.criteria=='1'){
    FromDt=`${sessionStorage.yearrpt}-01-01`;
    ToDt=`${sessionStorage.yearrpt}-06-30`;
   printonTblheadYear=`30/06/${sessionStorage.yearrpt}`;
}
else if(sessionStorage.criteria=='2'){
    FromDt=`${sessionStorage.yearrpt}-07-01`;
    ToDt=`${sessionStorage.yearrpt}-12-31`;
    printonTblheadYear=`${sessionStorage.yearrpt}`;
}
$('#tblheadYear').text(printonTblheadYear);
 var MasterData = {
        
"p_Ex_Id":sessionStorage.ES11Exid,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Registration_M": $("#Registration_M").val()==''?0:$("#Registration_M").val(),
"p_PlacesJs_M": $("#PlacesJs_M").val()==''?0:$("#PlacesJs_M").val(),
"p_Registration_Removed_M": $("#Registration_Removed_M").val()==''?0:$("#Registration_Removed_M").val(),
"p_LR_M": $("#LR_M").val()==''?0:$("#LR_M").val(),
"p_Submissions_M": $("#Submissions_M").val()==''?0:$("#Submissions_M").val(),
"p_LR_Prev_M": $("#LR_Prev_M").val()==''?0:$("#LR_Prev_M").val(),

"p_Registration_C": $("#Registration_C").val()==''?0:$("#Registration_C").val(),
"p_PlacesJs_C": $("#PlacesJs_C").val()==''?0:$("#PlacesJs_C").val(),
"p_Registration_Removed_C": $("#Registration_Removed_C").val()==''?0:$("#Registration_Removed_C").val(),
"p_LR_C": $("#LR_C").val()==''?0:$("#LR_C").val(),
"p_Submissions_C": $("#Submissions_C").val()==''?0:$("#Submissions_C").val(),
"p_LR_Prev_C": $("#LR_Prev_C").val()==''?0:$("#LR_Prev_C").val(),

"p_Registration_S": $("#Registration_S").val()==''?0:$("#Registration_S").val(),
"p_PlacesJs_S": $("#PlacesJs_S").val()==''?0:$("#PlacesJs_S").val(),
"p_Registration_Removed_S": $("#Registration_Removed_S").val()==''?0:$("#Registration_Removed_S").val(),
"p_LR_S": $("#LR_S").val()==''?0:$("#LR_S").val(),
"p_Submissions_S": $("#Submissions_S").val()==''?0:$("#Submissions_S").val(),
"p_LR_Prev_S": $("#LR_Prev_S").val()==''?0:$("#LR_Prev_S").val(),

"p_Registration_B": $("#Registration_B").val()==''?0:$("#Registration_B").val(),
"p_PlacesJs_B": $("#PlacesJs_B").val()==''?0:$("#PlacesJs_B").val(),
"p_Registration_Removed_B": $("#Registration_Removed_B ").val()==''?0:$("#Registration_Removed_B").val(),
"p_LR_B": $("#LR_B").val()==''?0:$("#LR_B").val(),
"p_Submissions_B": $("#Submissions_B").val()==''?0:$("#Submissions_B").val(),
"p_LR_Prev_B": $("#LR_Prev_B").val()==''?0:$("#LR_Prev_B").val(),

"p_Registration_Z": $("#Registration_Z").val()==''?0:$("#Registration_Z").val(),
"p_PlacesJs_Z": $("#PlacesJs_Z").val()==''?0:$("#PlacesJs_Z").val(),
"p_Registration_Removed_Z": $("#Registration_Removed_Z").val()==''?0:$("#Registration_Removed_Z").val(),
"p_LR_Z": $("#LR_Z").val()==''?0:$("#LR_Z").val(),
"p_Submissions_Z": $("#Submissions_Z").val()==''?0:$("#Submissions_Z").val(),
"p_LR_Prev_Z": $("#LR_Prev_Z").val()==''?0:$("#LR_Prev_Z").val(),

"p_Registration_Total": $("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
"p_PlacesJs_Total": $("#PlacesJs_Total").val()==''?0:$("#PlacesJs_Total").val(),
"p_Registration_Removed_Total": $("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
"p_LR_Total": $("#LR_Total").val()==''?0:$("#LR_Total").val(),
"p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
"p_LR_Prev_Total": $("#LR_Prev_Total").val()==''?0:$("#LR_Prev_Total").val(),

"p_Flag" :'3',
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "es23Entry";
  securedajaxpost(path,'parsrdataES23RptEntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES23RptEntryForm(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES23RptEntryForm();
        
    }
    if(data[0][0]){
      
    $("#Registration_M").text(data[0][0].Registration_M)
    $("#LR_Prev_M").text(data[0][0].LR_Prev_M)
    $("#PlacesJs_M").text(data[0][0].PlacesJs_M) 
    $("#Registration_Removed_M").text(data[0][0].Registration_Removed_M)
    $("#LR_M").text(data[0][0].LR_M)
    $("#Submissions_M").text(data[0][0].Submissions_M)

    $("#Registration_C").text(data[0][0].Registration_C)
    $("#LR_Prev_C").text(data[0][0].LR_Prev_C)
    $("#PlacesJs_C").text(data[0][0].PlacesJs_C) 
    $("#Registration_Removed_C").text(data[0][0].Registration_Removed_C)
    $("#LR_C").text(data[0][0].LR_C)
    $("#Submissions_C").text(data[0][0].Submissions_C)

    $("#Registration_S").text(data[0][0].Registration_S)
    $("#LR_Prev_S").text(data[0][0].LR_Prev_S)
    $("#PlacesJs_S").text(data[0][0].PlacesJs_S) 
    $("#Registration_Removed_S").text(data[0][0].Registration_Removed_S)
    $("#LR_S").text(data[0][0].LR_S)
    $("#Submissions_S").text(data[0][0].Submissions_S)

    $("#Registration_B").text(data[0][0].Registration_B)
    $("#LR_Prev_B").text(data[0][0].LR_Prev_B)
    $("#PlacesJs_B").text(data[0][0].PlacesJs_B) 
    $("#Registration_Removed_B").text(data[0][0].Registration_Removed_B)
    $("#LR_B").text(data[0][0].LR_B)
    $("#Submissions_B").text(data[0][0].Submissions_B)

    $("#Registration_Z").text(data[0][0].Registration_Z)
    $("#LR_Prev_Z").text(data[0][0].LR_Prev_Z)
    $("#PlacesJs_Z").text(data[0][0].PlacesJs_Z) 
    $("#Registration_Removed_Z").text(data[0][0].Registration_Removed_Z)
    $("#LR_Z").text(data[0][0].LR_Z)
    $("#Submissions_Z").text(data[0][0].Submissions_Z)

    $("#Registration_Total").text(data[0][0].Registration_Total)
    $("#LR_Prev_Total").text(data[0][0].LR_Prev_Total)
    $("#PlacesJs_Total").text(data[0][0].PlacesJs_Total) 
    $("#Registration_Removed_Total").text(data[0][0].Registration_Removed_Total)
    $("#LR_Total").text(data[0][0].LR_Total)
    $("#Submissions_Total").text(data[0][0].Submissions_Total)
       

    }
    else{
        $('#modalFile').modal('toggle');
        $(' #tbodyvalue, #tablehead').empty();
    }


     }


     
function ES24Part1RptEntryForm(){
    var FromDt='';
    var ToDt='';
if(sessionStorage.criteria=='1'){
    FromDt=`${sessionStorage.yearrpt}-01-01`;
    ToDt=`${sessionStorage.yearrpt}-06-30`;
}
else if(sessionStorage.criteria=='2'){
    FromDt=`${sessionStorage.yearrpt}-07-01`;
    ToDt=`${sessionStorage.yearrpt}-12-31`;
}   var MasterData = {
        
"p_Ex_Id":sessionStorage.ES11Exid,
"p_FromDt":FromDt,
"p_ToDt":ToDt,
"p_Registration_SC": $("#Registration_SC").val()==''?0:$("#Registration_SC").val(),
"p_PlacesJs_CG_SC": $("#PlacesJs_CG_SC").val()==''?0:$("#PlacesJs_CG_SC").val(),
"p_PlacesJs_UT_SC": $("#PlacesJs_UT_SC").val()==''?0:$("#PlacesJs_UT_SC").val(),
"p_PlacesJs_SG_SC": $("#PlacesJs_SG_SC").val()==''?0:$("#PlacesJs_SG_SC").val(),
"p_PlacesJs_QG_SC": $("#PlacesJs_QG_SC").val()==''?0:$("#PlacesJs_QG_SC").val(),
"p_PlacesJs_LB_SC": $("#PlacesJs_LB_SC").val()==''?0:$("#PlacesJs_LB_SC").val(),
"p_PlacesJs_Private_SC": $("#PlacesJs_Private_SC").val()==''?0:$("#PlacesJs_Private_SC").val(),
"p_PlacesJs_Total_SC": $("#PlacesJs_Total_SC").val()==''?0:$("#PlacesJs_Total_SC").val(),
"p_Registration_Removed_SC": $("#Registration_Removed_SC ").val()==''?0:$("#Registration_Removed_SC ").val(),
"p_LR_SC": $("#LR_SC").val()==''?0:$("#LR_SC").val(),
"p_Submissions_SC": $("#Submissions_SC").val()==''?0:$("#Submissions_SC").val(),
"p_LR_Prev_1_SC": $("#LR_Prev_1_SC").val()==''?0:$("#LR_Prev_1_SC").val(),
"p_Registration_ST": $("#Registration_ST").val()==''?0:$("#Registration_ST").val(),
"p_PlacesJs_CG_ST": $("#PlacesJs_CG_ST").val()==''?0:$("#PlacesJs_CG_ST").val(),
"p_PlacesJs_UT_ST": $("#PlacesJs_UT_ST").val()==''?0:$("#PlacesJs_UT_ST").val(),
"p_PlacesJs_SG_ST": $("#PlacesJs_SG_ST").val()==''?0:$("#PlacesJs_SG_ST").val(),
"p_PlacesJs_QG_ST": $("#PlacesJs_QG_ST").val()==''?0:$("#PlacesJs_QG_ST").val(),
"p_PlacesJs_LB_ST": $("#PlacesJs_LB_ST").val()==''?0:$("#PlacesJs_LB_ST").val(),
"p_PlacesJs_Private_ST": $("#PlacesJs_Private_ST").val()==''?0:$("#PlacesJs_Private_ST").val(),
"p_PlacesJs_Total_ST": $("#PlacesJs_Total_ST").val()==''?0:$("#PlacesJs_Total_ST").val(),
"p_Registration_Removed_ST": $("#Registration_Removed_ST ").val()==''?0:$("#Registration_Removed_ST ").val(),
"p_LR_ST": $("#LR_ST").val()==''?0:$("#LR_ST").val(),
"p_Submissions_ST": $("#Submissions_ST").val()==''?0:$("#Submissions_ST").val(),
"p_LR_Prev_1_ST": $("#LR_Prev_1_ST").val()==''?0:$("#LR_Prev_1_ST").val(),
"p_Registration_OBC": $("#Registration_OBC").val()==''?0:$("#Registration_OBC").val(),
"p_PlacesJs_CG_OBC": $("#PlacesJs_CG_OBC").val()==''?0:$("#PlacesJs_CG_OBC").val(),
"p_PlacesJs_UT_OBC": $("#PlacesJs_UT_OBC").val()==''?0:$("#PlacesJs_UT_OBC").val(),
"p_PlacesJs_SG_OBC": $("#PlacesJs_SG_OBC").val()==''?0:$("#PlacesJs_SG_OBC").val(),
"p_PlacesJs_QG_OBC": $("#PlacesJs_QG_OBC").val()==''?0:$("#PlacesJs_QG_OBC").val(),
"p_PlacesJs_LB_OBC": $("#PlacesJs_LB_OBC").val()==''?0:$("#PlacesJs_LB_OBC").val(),
"p_PlacesJs_Private_OBC": $("#PlacesJs_Private_OBC").val()==''?0:$("#PlacesJs_Private_OBC").val(),
"p_PlacesJs_Total_OBC": $("#PlacesJs_Total_OBC").val()==''?0:$("#PlacesJs_Total_OBC").val(),
"p_Registration_Removed_OBC": $("#Registration_Removed_OBC ").val()==''?0:$("#Registration_Removed_OBC ").val(),
"p_LR_OBC": $("#LR_OBC").val()==''?0:$("#LR_OBC").val(),
"p_Submissions_OBC": $("#Submissions_OBC").val()==''?0:$("#Submissions_OBC").val(),
"p_LR_Prev_1_OBC": $("#LR_Prev_1_OBC").val()==''?0:$("#LR_Prev_1_OBC").val(),
"p_Flag" :'3',
     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "es24p1RptEntry";
  securedajaxpost(path,'parsrdataES24part1RptEntryForm','comment',MasterData,'control')
  }
  
  
  function parsrdataES24part1RptEntryForm(data){
    data = JSON.parse(data)
  
      if(data[0][0]){
   
                 $("#Registration_SC").text(data[0][0].Registration_SC),
    $("#Registration_ST").text(data[0][0].Registration_ST),
    $("#Registration_OBC").text(data[0][0].Registration_OBC)
    $("#LR_Prev_1_SC").text(data[0][0].LR_Prev_1_SC)
    $("#LR_Prev_1_ST").text(data[0][0].LR_Prev_1_ST)
    $("#LR_Prev_1_OBC").text(data[0][0].LR_Prev_1_OBC)
    $("#PlacesJs_CG_SC").text(data[0][0].PlacesJs_CG_SC)
    $("#PlacesJs_CG_ST").text(data[0][0].PlacesJs_CG_ST)
    $("#PlacesJs_CG_OBC").text(data[0][0].PlacesJs_CG_OBC)  
    $("#PlacesJs_UT_SC").text(data[0][0].PlacesJs_UT_SC)
    $("#PlacesJs_UT_ST").text(data[0][0].PlacesJs_UT_ST)
    $("#PlacesJs_UT_OBC").text(data[0][0].PlacesJs_UT_OBC)
    $("#PlacesJs_SG_SC").text(data[0][0].PlacesJs_SG_SC)
    $("#PlacesJs_SG_ST").text(data[0][0].PlacesJs_SG_ST)
    $("#PlacesJs_SG_OBC").text(data[0][0].PlacesJs_SG_OBC)
    $("#PlacesJs_QG_SC").text(data[0][0].PlacesJs_QG_SC)
    $("#PlacesJs_QG_ST").text(data[0][0].PlacesJs_QG_ST)
    $("#PlacesJs_QG_OBC").text(data[0][0].PlacesJs_QG_OBC)
    $("#PlacesJs_LB_SC").text(data[0][0].PlacesJs_LB_SC)
    $("#PlacesJs_LB_ST").text(data[0][0].PlacesJs_LB_ST)
    $("#PlacesJs_LB_OBC").text(data[0][0].PlacesJs_LB_OBC)
    $("#PlacesJs_Private_SC").text(data[0][0].PlacesJs_Private_SC)
    $("#PlacesJs_Private_ST").text(data[0][0].PlacesJs_Private_ST)
    $("#PlacesJs_Private_OBC").text(data[0][0].PlacesJs_Private_OBC)
    $("#Registration_Removed_SC").text(data[0][0].Registration_Removed_SC)
    $("#Registration_Removed_ST").text(data[0][0].Registration_Removed_ST)
    $("#Registration_Removed_OBC").text(data[0][0].Registration_Removed_OBC)
    $("#LR_SC").text(data[0][0].LR_SC)
    $("#LR_ST").text(data[0][0].LR_ST)
    $("#LR_OBC").text(data[0][0].LR_OBC)
    $("#Submissions_SC").text(data[0][0].Submissions_SC)
    $("#Submissions_ST").text(data[0][0].Submissions_ST)
    $("#Submissions_OBC").text(data[0][0].Submissions_OBC)
      }
      else{
        $('#modalFile').modal('toggle');
        $(' #tbodyvalue, #tablehead').empty();
      }
   

     }  
     
     function fetchES24Part2(){
         var FromDt;
         var ToDt;
        if(sessionStorage.criteria='1'){
            FromDt=`${sessionStorage.yearrpt}-01-01`;
            ToDt=`${sessionStorage.yearrpt}-06-30`;
        }
        else if(sessionStorage.criteria=='2'){
            FromDt=`${sessionStorage.yearrpt}-07-01`;
            ToDt=`${sessionStorage.yearrpt}-12-31`;
        }
        var MasterData = {
            
            "p_Ex_Id":sessionStorage.ES11Exid,
            "p_FromDt":FromDt,
            "p_ToDt":ToDt,
            "p_ES24_II_Id":'0',
            "p_Outstanding_Prev_SC":"0",
            "p_Outstanding_Prev_ST":"0",
            "p_Outstanding_Prev_OBC":"0",
            "p_Notified_SC":"0",
            "p_Notified_ST":"0",
            "p_Notified_OBC":"0",
            "p_Filled_SC":"0",
            "p_Filled_ST":"0",
            "p_Filled_OBC":"0",
        
            "p_Cancelled_NA_SC":"0",
            "p_Cancelled_NA_ST":"0",
            "p_Cancelled_NA_OBC":"0",
        
            "p_Cancelled_Other_SC":"0",
            "p_Cancelled_Other_ST":"0",
            "p_Cancelled_Other_OBC":"0",
        
            "p_Outstanding_SC":"0",
            "p_Outstanding_ST":"0",
            "p_Outstanding_OBC":"0",
        
            "p_Flag":'3'
      }
      MasterData = JSON.stringify(MasterData)
        var path = serverpath + "es24p2RptEntry";
        securedajaxpost(path,'parsedatafetchES24Part2','comment',MasterData,'control') 
    }
      function parsedatafetchES24Part2(data){  
          data = JSON.parse(data);
          var appendata='';
          var data1 = data[0];
       var isData=false;
            for (var i = 0; i < data1.length; i++) {
                if(data1[i].Flag=='Add'){
isData=true;
                }
              appendata+=`	<tr class="tbl_row" id="${data1[i].Flag}_${i}">
              <td class="ES24_II_Id">${data1[i].ES24_II_Id}</td>
              <td>${data1[i].Type_Of_Establishment}</td>
      
              <td>${data1[i].Outstanding_Prev_SC}
              </td>
              <td >${data1[i].Outstanding_Prev_ST}
              </td>
              <td>${data1[i].Outstanding_Prev_OBC} 
              </td>
              <td >${data1[i].Notified_SC} 
              </td>
              <td>${data1[i].Notified_ST} 
              </td>
              <td >${data1[i].Notified_OBC} 
              </td>
              <td >${data1[i].Filled_SC} 
              </td>
                  <td >${data1[i].Filled_ST} 
              </td>
              <td>${data1[i].Filled_OBC} 
              </td>
              <td >${data1[i].Cancelled_NA_SC} 
              </td>
              <td>${data1[i].Cancelled_NA_ST} 
              </td>
              <td >${data1[i].Cancelled_NA_OBC} 
              </td>
              <td>${data1[i].Cancelled_Other_SC} 
              </td>
              <td >${data1[i].Cancelled_Other_ST} 
              </td>
              <td >${data1[i].Cancelled_Other_OBC} 
              </td>	
              <td >${data1[i].Outstanding_SC} 
              </td>
              <td >${data1[i].Outstanding_ST} 
              </td>
              <td>${data1[i].Outstanding_OBC} 
              </td>
         
      </td>
          </tr>`
            }
            if(isData){
                $('#modalFile').modal('toggle');
                $(' #tbodyvalue, #tablehead').empty();
            }
            else{

                $('#tbodyvalue').html(appendata);
            }
          
          
    
      }

      function ES25RptEntryForm(){
        var FromDt='';
        var ToDt='';
    if(sessionStorage.criteria=='1'){
        FromDt=`${sessionStorage.yearrpt}-01-01`;
        ToDt=`${sessionStorage.yearrpt}-06-30`;
    }
    else if(sessionStorage.criteria=='2'){
        FromDt=`${sessionStorage.yearrpt}-07-01`;
        ToDt=`${sessionStorage.yearrpt}-12-31`;
    }
      var MasterData = {
            
    "p_Ex_Id":sessionStorage.ES11Exid,
    "p_FromDt":FromDt,
    "p_ToDt":ToDt,
    "p_Registration_Blind": $("#Registration_Blind").val()==''?0:$("#Registration_Blind").val(),
    "p_PlacesJs_CG_Blind": $("#PlacesJs_CG_Blind").val()==''?0:$("#PlacesJs_CG_Blind").val(),
    "p_PlacesJs_UT_Blind": $("#PlacesJs_UT_Blind").val()==''?0:$("#PlacesJs_UT_Blind").val(),
    "p_PlacesJs_SG_Blind": $("#PlacesJs_SG_Blind").val()==''?0:$("#PlacesJs_SG_Blind").val(),
    "p_PlacesJs_QG_Blind": $("#PlacesJs_QG_Blind").val()==''?0:$("#PlacesJs_QG_Blind").val(),
    "p_PlacesJs_LB_Blind": $("#PlacesJs_LB_Blind").val()==''?0:$("#PlacesJs_LB_Blind").val(),
    "p_PlacesJs_Private_Blind": $("#PlacesJs_Private_Blind").val()==''?0:$("#PlacesJs_Private_Blind").val(),
    "p_PlacesJs_Total_Blind": $("#PlacesJs_Total_Blind").val()==''?0:$("#PlacesJs_Total_Blind").val(),
    "p_Registration_Removed_Blind": $("#Registration_Removed_Blind ").val()==''?0:$("#Registration_Removed_Blind ").val(),
    "p_LR_Blind": $("#LR_Blind").val()==''?0:$("#LR_Blind").val(),
    "p_Submissions_Blind": $("#Submissions_Blind").val()==''?0:$("#Submissions_Blind").val(),
    "p_LR_Prev_1_Blind": $("#LR_Prev_1_Blind").val()==''?0:$("#LR_Prev_1_Blind").val(),
    "p_Registration_Deaf": $("#Registration_Deaf").val()==''?0:$("#Registration_Deaf").val(),
    "p_PlacesJs_CG_Deaf": $("#PlacesJs_CG_Deaf").val()==''?0:$("#PlacesJs_CG_Deaf").val(),
    "p_PlacesJs_UT_Deaf": $("#PlacesJs_UT_Deaf").val()==''?0:$("#PlacesJs_UT_Deaf").val(),
    "p_PlacesJs_SG_Deaf":$("#PlacesJs_SG_Deaf").val()==''?0:$("#PlacesJs_SG_Deaf").val(),
    "p_PlacesJs_QG_Deaf":$("#PlacesJs_QG_Deaf").val()==''?0:$("#PlacesJs_QG_Deaf").val(),
    "p_PlacesJs_LB_Deaf":$("#PlacesJs_LB_Deaf").val()==''?0:$("#PlacesJs_LB_Deaf").val(),
    "p_PlacesJs_Private_Deaf":$("#PlacesJs_Private_Deaf").val()==''?0:$("#PlacesJs_Private_Deaf").val(),
    "p_PlacesJs_Total_Deaf":$("#PlacesJs_Total_Deaf").val()==''?0:$("#PlacesJs_Total_Deaf").val(),
    "p_Registration_Removed_Deaf":$("#Registration_Removed_Deaf").val()==''?0:$("#Registration_Removed_Deaf").val(),
    "p_LR_Deaf":$("#LR_Deaf").val()==''?0:$("#LR_Deaf").val(),
    "p_Submissions_Deaf":$("#Submissions_Deaf").val()==''?0:$("#Submissions_Deaf").val(),
    "p_LR_Prev_1_Deaf":$("#LR_Prev_1_Deaf").val()==''?0:$("#LR_Prev_1_Deaf").val(),
    "p_Registration_Orthopaedics": $("#Registration_Orthopaedics").val()==''?0:$("#Registration_Orthopaedics").val(),
    "p_PlacesJs_CG_Orthopaedics": $("#PlacesJs_CG_Orthopaedics").val()==''?0:$("#PlacesJs_CG_Orthopaedics").val(),
    "p_PlacesJs_UT_Orthopaedics":$("#PlacesJs_UT_Orthopaedics").val()==''?0:$("#PlacesJs_UT_Orthopaedics").val(),
    "p_PlacesJs_SG_Orthopaedics":$("#PlacesJs_SG_Orthopaedics").val()==''?0:$("#PlacesJs_SG_Orthopaedics").val(),
    "p_PlacesJs_QG_Orthopaedics":$("#PlacesJs_QG_Orthopaedics").val()==''?0:$("#PlacesJs_QG_Orthopaedics").val(),
    "p_PlacesJs_LB_Orthopaedics":$("#PlacesJs_LB_Orthopaedics").val()==''?0:$("#PlacesJs_LB_Orthopaedics").val(),
    "p_PlacesJs_Private_Orthopaedics":$("#PlacesJs_Private_Orthopaedics").val()==''?0:$("#PlacesJs_Private_Orthopaedics").val(),
    "p_PlacesJs_Total_Orthopaedics":$("#PlacesJs_Total_Orthopaedics").val()==''?0:$("#PlacesJs_Total_Orthopaedics").val(),
    "p_Registration_Removed_Orthopaedics":$("#Registration_Removed_Orthopaedics").val()==''?0:$("#Registration_Removed_Orthopaedics").val(),
    "p_LR_Orthopaedics":$("#LR_Orthopaedics").val()==''?0:$("#LR_Orthopaedics").val(),
    "p_Submissions_Orthopaedics":$("#Submissions_Orthopaedics").val()==''?0:$("#Submissions_Orthopaedics").val(),
    "p_LR_Prev_1_Orthopaedics":$("#LR_Prev_1_Orthopaedics").val()==''?0:$("#LR_Prev_1_Orthopaedics").val(),
    "p_Registration_Respiratory":$("#Registration_Respiratory").val()==''?0:$("#Registration_Respiratory").val(),
    "p_PlacesJs_CG_Respiratory":$("#PlacesJs_CG_Respiratory").val()==''?0:$("#PlacesJs_CG_Respiratory").val(),
    "p_PlacesJs_UT_Respiratory":$("#PlacesJs_UT_Respiratory").val()==''?0:$("#PlacesJs_UT_Respiratory").val(),
    "p_PlacesJs_SG_Respiratory":$("#PlacesJs_SG_Respiratory").val()==''?0:$("#PlacesJs_SG_Respiratory").val(),
    "p_PlacesJs_QG_Respiratory":$("#PlacesJs_QG_Respiratory").val()==''?0:$("#PlacesJs_QG_Respiratory").val(),
    "p_PlacesJs_LB_Respiratory":$("#PlacesJs_LB_Respiratory").val()==''?0:$("#PlacesJs_LB_Respiratory").val(),
    "p_PlacesJs_Private_Respiratory":$("#PlacesJs_Private_Respiratory").val()==''?0:$("#PlacesJs_Private_Respiratory").val(),
    "p_PlacesJs_Total_Respiratory":$("#PlacesJs_Total_Respiratory").val()==''?0:$("#PlacesJs_Total_Respiratory").val(),
    "p_Registration_Removed_Respiratory":$("#Registration_Removed_Respiratory").val()==''?0:$("#Registration_Removed_Respiratory").val(),
    "p_LR_Respiratory":$("#LR_Respiratory").val()==''?0:$("#LR_Respiratory").val(),
    "p_Submissions_Respiratory":$("#Submissions_Respiratory").val()==''?0:$("#Submissions_Respiratory").val(),
    "p_LR_Prev_1_Respiratory":$("#LR_Prev_1_Respiratory").val()==''?0:$("#LR_Prev_1_Respiratory").val(),
    "p_Registration_Leprosy":$("#Registration_Leprosy").val()==''?0:$("#Registration_Leprosy").val(),
    "p_PlacesJs_CG_Leprosy":$("#PlacesJs_CG_Leprosy").val()==''?0:$("#PlacesJs_CG_Leprosy").val(),
    "p_PlacesJs_UT_Leprosy":$("#PlacesJs_UT_Leprosy").val()==''?0:$("#PlacesJs_UT_Leprosy").val(),
    "p_PlacesJs_SG_Leprosy":$("#PlacesJs_SG_Leprosy").val()==''?0:$("#PlacesJs_SG_Leprosy").val(),
    "p_PlacesJs_QG_Leprosy":$("#PlacesJs_QG_Leprosy").val()==''?0:$("#PlacesJs_QG_Leprosy").val(),
    "p_PlacesJs_LB_Leprosy":$("#PlacesJs_LB_Leprosy").val()==''?0:$("#PlacesJs_LB_Leprosy").val(),
    "p_PlacesJs_Private_Leprosy":$("#PlacesJs_Private_Leprosy").val()==''?0:$("#PlacesJs_Private_Leprosy").val(),
    "p_PlacesJs_Total_Leprosy":$("#PlacesJs_Total_Leprosy").val()==''?0:$("#PlacesJs_Total_Leprosy").val(),
    "p_Registration_Removed_Leprosy":$("#Registration_Removed_Leprosy").val()==''?0:$("#Registration_Removed_Leprosy").val(),
    "p_LR_Leprosy":$("#LR_Leprosy").val()==''?0:$("#LR_Leprosy").val(),
    "p_Submissions_Leprosy":$("#Submissions_Leprosy").val()==''?0:$("#Submissions_Leprosy").val(),
    "p_LR_Prev_1_Leprosy":$("#LR_Prev_1_Leprosy").val()==''?0:$("#LR_Prev_1_Leprosy").val(),
    "p_Registration_Total":$("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
    "p_PlacesJs_CG_Total":$("#PlacesJs_CG_Total").val()==''?0:$("#PlacesJs_CG_Total").val(),
    "p_PlacesJs_UT_Total":$("#PlacesJs_UT_Total").val()==''?0:$("#PlacesJs_UT_Total").val(),
    "p_PlacesJs_SG_Total":$("#PlacesJs_SG_Total").val()==''?0:$("#PlacesJs_SG_Total").val(),
    "p_PlacesJs_QG_Total":$("#PlacesJs_QG_Total").val()==''?0:$("#PlacesJs_QG_Total").val(),
    "p_PlacesJs_LB_Total":$("#PlacesJs_LB_Total").val()==''?0:$("#PlacesJs_LB_Total").val(),
    "p_PlacesJs_Private_Total":$("#PlacesJs_Private_Total").val()==''?0:$("#PlacesJs_Private_Total").val(),
    "p_PlacesJs_Total_Total":$("#PlacesJs_Total_Total").val()==''?0:$("#PlacesJs_Total_Total").val(),
    "p_Registration_Removed_Total":$("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
    "p_LR_Total":$("#LR_Total").val()==''?0:$("#LR_Total").val(),
    "p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
    "p_LR_Prev_1_Total": $("#LR_Prev_1_Total").val()==''?0:$("#LR_Prev_1_Total").val(),
    "p_Registration_W": $("#Registration_W").val()==''?0:$("#Registration_W").val(),
    "p_PlacesJs_CG_W": $("#PlacesJs_CG_W").val()==''?0:$("#PlacesJs_CG_W").val(),
    "p_PlacesJs_UT_W": $("#PlacesJs_UT_W").val()==''?0:$("#PlacesJs_UT_W").val(),
    "p_PlacesJs_SG_W": $("#PlacesJs_SG_W").val()==''?0:$("#PlacesJs_SG_W").val(),
    "p_PlacesJs_QG_W": $("#PlacesJs_QG_W").val()==''?0:$("#PlacesJs_QG_W").val(),
    "p_PlacesJs_LB_W": $("#PlacesJs_LB_W").val()==''?0:$("#PlacesJs_LB_W").val(),
    "p_PlacesJs_Private_W": $("#PlacesJs_Private_W").val()==''?0:$("#PlacesJs_Private_W").val(),
    "p_PlacesJs_Total_W": $("#PlacesJs_Total_W").val()==''?0:$("#PlacesJs_Total_W").val(),
    "p_Registration_Removed_W": $("#Registration_Removed_W").val()==''?0:$("#Registration_Removed_W").val(),
    "p_LR_W": $("#LR_W").val()==''?0:$("#LR_W").val(),
    "p_Submissions_W": $("#Submissions_W").val()==''?0:$("#Submissions_W").val(),
    "p_LR_Prev_1_W": $("#LR_Prev_1_W").val()==''?0:$("#LR_Prev_1_W").val(),
    "p_Flag" :'3',
         }
      MasterData = JSON.stringify(MasterData)
      var path = serverpath + "es25p1RptEntry";
      securedajaxpost(path,'parsrdataES25RptEntryForm','comment',MasterData,'control')
      }
      
      
      function parsrdataES25RptEntryForm(data){
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            ES25RptEntryForm();
            
        }
      if(data[0][0]){
           
                    $("#Registration_Blind").text(data[0][0].Registration_Blind),
                    $("#PlacesJs_CG_Blind").text(data[0][0].PlacesJs_CG_Blind),
                    $("#PlacesJs_UT_Blind").text(data[0][0].PlacesJs_UT_Blind),
                    $("#PlacesJs_SG_Blind").text(data[0][0].PlacesJs_SG_Blind),
                    $("#PlacesJs_QG_Blind").text(data[0][0].PlacesJs_QG_Blind),
                    $("#PlacesJs_LB_Blind").text(data[0][0].PlacesJs_LB_Blind),
                    $("#PlacesJs_Private_Blind").text(data[0][0].PlacesJs_Private_Blind),
                    $("#PlacesJs_Total_Blind").text(data[0][0].PlacesJs_Total_Blind),
                    $("#Registration_Removed_Blind ").text(data[0][0].Registration_Removed_Blind),
                    $("#LR_Blind").text(data[0][0].LR_Blind),
                    $("#Submissions_Blind").text(data[0][0].Submissions_Blind),
                    $("#LR_Prev_1_Blind").text(data[0][0].LR_Prev_1_Blind),
                    
    
                    $("#Registration_Deaf").text(data[0][0].Registration_Deaf),
                    $("#PlacesJs_CG_Deaf").text(data[0][0].PlacesJs_CG_Deaf),
                    $("#PlacesJs_UT_Deaf").text(data[0][0].PlacesJs_UT_Deaf),
                    $("#PlacesJs_SG_Deaf").text(data[0][0].PlacesJs_SG_Deaf),
                    $("#PlacesJs_QG_Deaf").text(data[0][0].PlacesJs_QG_Deaf),
                    $("#PlacesJs_LB_Deaf").text(data[0][0].PlacesJs_LB_Deaf),
                    $("#PlacesJs_Private_Deaf").text(data[0][0].PlacesJs_Private_Deaf),
                    $("#PlacesJs_Total_Deaf").text(data[0][0].PlacesJs_Total_Deaf),
                    $("#Registration_Removed_Deaf ").text(data[0][0].Registration_Removed_Deaf),
                    $("#LR_Deaf").text(data[0][0].LR_Deaf),
                    $("#Submissions_Deaf").text(data[0][0].Submissions_Deaf),
                    $("#LR_Prev_1_Deaf").text(data[0][0].LR_Prev_1_Deaf),
    
                    $("#Registration_Orthopaedics").text(data[0][0].Registration_Orthopaedics),
                    $("#PlacesJs_CG_Orthopaedics").text(data[0][0].PlacesJs_CG_Orthopaedics),
                    $("#PlacesJs_UT_Orthopaedics").text(data[0][0].PlacesJs_UT_Orthopaedics),
                    $("#PlacesJs_SG_Orthopaedics").text(data[0][0].PlacesJs_SG_Orthopaedics),
                    $("#PlacesJs_QG_Orthopaedics").text(data[0][0].PlacesJs_QG_Orthopaedics),
                    $("#PlacesJs_LB_Orthopaedics").text(data[0][0].PlacesJs_LB_Orthopaedics),
                    $("#PlacesJs_Private_Orthopaedics").text(data[0][0].PlacesJs_Private_Orthopaedics),
                    $("#PlacesJs_Total_Orthopaedics").text(data[0][0].PlacesJs_Total_Orthopaedics),
                    $("#Registration_Removed_Orthopaedics ").text(data[0][0].Registration_Removed_Orthopaedics),
                    $("#LR_Orthopaedics").text(data[0][0].LR_Orthopaedics),
                    $("#Submissions_Orthopaedics").text(data[0][0].Submissions_Orthopaedics),
                    $("#LR_Prev_1_Orthopaedics").text(data[0][0].LR_Prev_1_Orthopaedics),
    
                    $("#Registration_Respiratory").text(data[0][0].Registration_Respiratory),
                    $("#PlacesJs_CG_Respiratory").text(data[0][0].PlacesJs_CG_Respiratory),
                    $("#PlacesJs_UT_Respiratory").text(data[0][0].PlacesJs_UT_Respiratory),
                    $("#PlacesJs_SG_Respiratory").text(data[0][0].PlacesJs_SG_Respiratory),
                    $("#PlacesJs_QG_Respiratory").text(data[0][0].PlacesJs_QG_Respiratory),
                    $("#PlacesJs_LB_Respiratory").text(data[0][0].PlacesJs_LB_Respiratory),
                    $("#PlacesJs_Private_Respiratory").text(data[0][0].PlacesJs_Private_Respiratory),
                    $("#PlacesJs_Total_Respiratory").text(data[0][0].PlacesJs_Total_Respiratory),
                    $("#Registration_Removed_Respiratory ").text(data[0][0].Registration_Removed_Respiratory),
                    $("#LR_Respiratory").text(data[0][0].LR_Respiratory),
                    $("#Submissions_Respiratory").text(data[0][0].Submissions_Respiratory),
                    $("#LR_Prev_1_Respiratory").text(data[0][0].LR_Prev_1_Respiratory),
    
                    $("#Registration_Leprosy").text(data[0][0].Registration_Leprosy),
                    $("#PlacesJs_CG_Leprosy").text(data[0][0].PlacesJs_CG_Leprosy),
                    $("#PlacesJs_UT_Leprosy").text(data[0][0].PlacesJs_UT_Leprosy),
                    $("#PlacesJs_SG_Leprosy").text(data[0][0].PlacesJs_SG_Leprosy),
                    $("#PlacesJs_QG_Leprosy").text(data[0][0].PlacesJs_QG_Leprosy),
                    $("#PlacesJs_LB_Leprosy").text(data[0][0].PlacesJs_LB_Leprosy),
                    $("#PlacesJs_Private_Leprosy").text(data[0][0].PlacesJs_Private_Leprosy),
                    $("#PlacesJs_Total_Leprosy").text(data[0][0].PlacesJs_Total_Leprosy),
                    $("#Registration_Removed_Leprosy ").text(data[0][0].Registration_Removed_Leprosy),
                    $("#LR_Leprosy").text(data[0][0].LR_Leprosy),
                    $("#Submissions_Leprosy").text(data[0][0].Submissions_Leprosy),
                    $("#LR_Prev_1_Leprosy").text(data[0][0].LR_Prev_1_Leprosy),
    
                    $("#Registration_Total").text(data[0][0].Registration_Total),
                    $("#PlacesJs_CG_Total").text(data[0][0].PlacesJs_CG_Total),
                    $("#PlacesJs_UT_Total").text(data[0][0].PlacesJs_UT_Total),
                    $("#PlacesJs_SG_Total").text(data[0][0].PlacesJs_SG_Total),
                    $("#PlacesJs_QG_Total").text(data[0][0].PlacesJs_QG_Total),
                    $("#PlacesJs_LB_Total").text(data[0][0].PlacesJs_LB_Total),
                    $("#PlacesJs_Private_Total").text(data[0][0].PlacesJs_Private_Total),
                    $("#PlacesJs_Total_Total").text(data[0][0].PlacesJs_Total_Total),
                    $("#Registration_Removed_Total ").text(data[0][0].Registration_Removed_Total),
                    $("#LR_Total").text(data[0][0].LR_Total),
                    $("#Submissions_Total").text(data[0][0].Submissions_Total),
                    $("#LR_Prev_1_Total").text(data[0][0].LR_Prev_1_Total),
    
                    $("#Registration_W").text(data[0][0].Registration_W),
                    $("#PlacesJs_CG_W").text(data[0][0].PlacesJs_CG_W),
                    $("#PlacesJs_UT_W").text(data[0][0].PlacesJs_UT_W),
                    $("#PlacesJs_SG_W").text(data[0][0].PlacesJs_SG_W),
                    $("#PlacesJs_QG_W").text(data[0][0].PlacesJs_QG_W),
                    $("#PlacesJs_LB_W").text(data[0][0].PlacesJs_LB_W),
                    $("#PlacesJs_Private_W").text(data[0][0].PlacesJs_Private_W),
                    $("#PlacesJs_Total_W").text(data[0][0].PlacesJs_W_W),
                    $("#Registration_Removed_W ").text(data[0][0].Registration_Removed_W),
                    $("#LR_W").text(data[0][0].LR_W),
                    $("#Submissions_W").text(data[0][0].Submissions_W),
                    $("#LR_Prev_1_W").text(data[0][0].LR_Prev_1_W)
          }
        else{
            $('#modalFile').modal('toggle');
            $(' #tbodyvalue, #tablehead').empty();
        }
   
    
         }

         function ES25RptP2Submit(){
                if(sessionStorage.criteria=='1'){
                    var fromdate = ''+sessionStorage.yearrpt+'/01/01'
                    var todate = ''+sessionStorage.yearrpt+'/06/30/'
                    }
                    else if(sessionStorage.criteria=='2'){
                        var fromdate = ''+sessionStorage.yearrpt+'/07/01/'
                        var todate = ''+sessionStorage.yearrpt+'/12/31/'
                    }
            
                var MasterData = {
              
            "p_Ex_Id": sessionStorage.ES11Exid,  
            "p_FromDt":  fromdate,  
            "p_ToDt":   todate,  
            "p_Outstanding_Prev_CG":   $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),  
            "p_Outstanding_Prev_UT":  $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val() ,  
            "p_Outstanding_Prev_SG":   $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),  
            "p_Outstanding_Prev_Quasi_CG":  $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val() ,  
            "p_Outstanding_Prev_Quasi_SG":  $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val() ,  
            "p_Outstanding_Prev_LB":   $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),  
            "p_Outstanding_Prev_Private":  $("#Outstanding_Prev_Private").val()==''?0:$("#Outstanding_Prev_Private").val() ,  
            "p_Outstanding_Prev_Total":  $("#Outstanding_Prev_Total").val()==''?0:$("#Outstanding_Prev_Total").val() ,  
            "p_Notified_CG":   $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),  
            "p_Notified_UT":  $("#Notified_UT").val()==''?0:$("#Notified_UT").val() ,  
            "p_Notified_SG":   $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),  
            "p_Notified_Quasi_CG":  $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val() ,  
            "p_Notified_Quasi_SG":  $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val() ,  
            "p_Notified_LB":  $("#Notified_LB").val()==''?0:$("#Notified_LB").val() ,  
            "p_Notified_Private":  $("#Notified_Private").val()==''?0:$("#Notified_Private").val() ,  
            "p_Notified_Total":  $("#Notified_Total").val()==''?0:$("#Notified_Total").val() ,  
            "p_Filled_CG":  $("#Filled_CG").val()==''?0:$("#Filled_CG").val() ,  
            "p_Filled_UT":  $("#Filled_UT").val()==''?0:$("#Filled_UT").val() ,  
            "p_Filled_SG":  $("#Filled_SG").val()==''?0:$("#Filled_SG").val() ,  
            "p_Filled_Quasi_CG":  $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val() ,  
            "p_Filled_Quasi_SG":  $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val() ,  
            "p_Filled_LB":   $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),  
            "p_Filled_Private":  $("#Filled_Private").val()==''?0:$("#Filled_Private").val() ,  
            "p_Filled_Total":  $("#Filled_Total").val()==''?0:$("#Filled_Total").val() ,  
            "p_Cancelled_NA_CG":  $("#Cancelled_NA_CG").val()==''?0:$("#Cancelled_NA_CG").val() ,  
            "p_Cancelled_NA_UT":  $("#Cancelled_NA_UT").val()==''?0:$("#Cancelled_NA_UT").val() ,  
            "p_Cancelled_NA_SG":  $("#Cancelled_NA_SG").val()==''?0:$("#Cancelled_NA_SG").val() ,  
            "p_Cancelled_NA_Quasi_CG":  $("#Cancelled_NA_Quasi_CG").val()==''?0:$("#Cancelled_NA_Quasi_CG").val() ,  
            "p_Cancelled_NA_Quasi_SG":  $("#Cancelled_NA_Quasi_SG").val()==''?0:$("#Cancelled_NA_Quasi_SG").val() ,  
            "p_Cancelled_NA_LB":   $("#Cancelled_NA_LB").val()==''?0:$("#Cancelled_NA_LB").val(),  
            "p_Cancelled_NA_Private":   $("#Cancelled_NA_Private").val()==''?0:$("#Cancelled_NA_Private").val(),  
            "p_Cancelled_NA_Total":  $("#Cancelled_NA_Total").val()==''?0:$("#Cancelled_NA_Total").val() ,  
            "p_Cancelled_Other_CG":  $("#Cancelled_Other_CG").val()==''?0:$("#Cancelled_Other_CG").val() ,  
            "p_Cancelled_Other_UT":  $("#Cancelled_Other_UT").val()==''?0:$("#Cancelled_Other_UT").val() ,  
            "p_Cancelled_Other_SG":  $("#Cancelled_Other_SG").val()==''?0:$("#Cancelled_Other_SG").val() ,  
            "p_Cancelled_Other_Quasi_CG":  $("#Cancelled_Other_Quasi_CG").val()==''?0:$("#Cancelled_Other_Quasi_CG").val() ,  
            "p_Cancelled_Other_Quasi_SG":   $("#Cancelled_Other_Quasi_SG").val()==''?0:$("#Cancelled_Other_Quasi_SG").val(),  
            "p_Cancelled_Other_LB":  $("#Cancelled_Other_LB").val()==''?0:$("#Cancelled_Other_LB").val() ,  
            "p_Cancelled_Other_Private":  $("#Cancelled_Other_Private").val()==''?0:$("#Cancelled_Other_Private").val() ,  
            "p_Cancelled_Other_Total":  $("#Cancelled_Other_Total").val()==''?0:$("#Cancelled_Other_Total").val() ,  
            "p_Outstanding_CG":  $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val() ,  
            "p_Outstanding_UT":   $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),  
            "p_Outstanding_SG":  $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val() ,  
            "p_Outstanding_Quasi_CG":   $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),  
            "p_Outstanding_Quasi_SG":  $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val() ,  
            "p_Outstanding_LB":  $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val() ,  
            "p_Outstanding_Private":   $("#Outstanding_Private").val()==''?0:$("#Outstanding_Private").val(),  
            "p_Outstanding_Total":  $("#Outstanding_Total").val()==''?0:$("#Outstanding_Total").val(),    
            "p_Flag": '3',
            
                 }
              MasterData = JSON.stringify(MasterData)
              var path = serverpath + "ES25P2Entry";
              securedajaxpost(path,'parsrdataES25RptP2Submit','comment',MasterData,'control')
              }
              
              
              function parsrdataES25RptP2Submit(data){
                data = JSON.parse(data)
               
            
              
            
              if(data[0][0]){
                   
               $("#Outstanding_Prev_CG").text(data[0][0].Outstanding_Prev_CG);
               $("#Outstanding_Prev_UT").text(data[0][0].Outstanding_Prev_UT);
               $("#Outstanding_Prev_SG").text(data[0][0].Outstanding_Prev_SG);
               $("#Outstanding_Prev_Quasi_CG").text(data[0][0].Outstanding_Prev_Quasi_CG);
               $("#Outstanding_Prev_Quasi_SG").text(data[0][0].Outstanding_Prev_Quasi_SG) ; 
               $("#Outstanding_Prev_LB").text(data[0][0].Outstanding_Prev_LB); 
               $("#Outstanding_Prev_Private").text(data[0][0].Outstanding_Prev_Private) ; 
               $("#Outstanding_Prev_Total").text(data[0][0].Outstanding_Prev_Total) ;
               $("#Notified_CG").text(data[0][0].Notified_CG);
                 $("#Notified_UT").text(data[0][0].Notified_UT) ;  
                  $("#Notified_SG").text(data[0][0].Notified_SG);
                 $("#Notified_Quasi_CG").text(data[0][0].Notified_Quasi_CG) ;  
                 $("#Notified_Quasi_SG").text(data[0][0].Notified_Quasi_SG) ; 
                 $("#Notified_LB").text(data[0][0].Notified_LB) ;
                 $("#Notified_Private").text(data[0][0].Notified_Private) ; 
                 $("#Notified_Total").text(data[0][0].Notified_Total) ;
                 $("#Filled_CG").text(data[0][0].Filled_CG) ;  
                 $("#Filled_UT").text(data[0][0].Filled_UT) ;  
                 $("#Filled_SG").text(data[0][0].Filled_SG) ;
                $("#Filled_Quasi_CG").text(data[0][0].Filled_Quasi_CG) ; 
                 $("#Filled_Quasi_SG").text(data[0][0].Filled_Quasi_SG) ; 
                 $("#Filled_LB").text(data[0][0].Filled_LB);
                 $("#Filled_Private").text(data[0][0].Filled_Private) ;  
                 $("#Filled_Total").text(data[0][0].Filled_Total) ;
                 $("#Cancelled_NA_CG").text(data[0][0].Cancelled_NA_CG) ;  
                 $("#Cancelled_NA_UT").text(data[0][0].Cancelled_NA_UT) ;  
                 $("#Cancelled_NA_SG").text(data[0][0].Cancelled_NA_SG) ;
                 $("#Cancelled_NA_Quasi_CG").text(data[0][0].Cancelled_NA_Quasi_CG) ; 
                 $("#Cancelled_NA_Quasi_SG").text(data[0][0].Cancelled_NA_Quasi_SG) ;  
                  $("#Cancelled_NA_LB").text(data[0][0].Cancelled_NA_LB);
                  $("#Cancelled_NA_Private").text(data[0][0].Cancelled_NA_Private);  
                 $("#Cancelled_NA_Total").text(data[0][0].Cancelled_NA_Total) ;  
                 $("#Cancelled_Other_CG").text(data[0][0].Cancelled_Other_CG) ;  
                 $("#Cancelled_Other_UT").text(data[0][0].Cancelled_Other_UT) ;  
                 $("#Cancelled_Other_SG").text(data[0][0].Cancelled_Other_SG) ;
                 $("#Cancelled_Other_Quasi_CG").text(data[0][0].Cancelled_Other_Quasi_CG) ;  
                 $("#Cancelled_Other_Quasi_SG").text(data[0][0].Cancelled_Other_Quasi_SG); 
                 $("#Cancelled_Other_LB").text(data[0][0].Cancelled_Other_LB) ;
                 $("#Cancelled_Other_Private").text(data[0][0].Cancelled_Other_Private) ;  
                 $("#Cancelled_Other_Total").text(data[0][0].Cancelled_Other_Total) ; 
                $("#Outstanding_CG").text(data[0][0].Outstanding_CG) ; 
                  $("#Outstanding_UT").text(data[0][0].Outstanding_UT);  
                 $("#Outstanding_SG").text(data[0][0].Outstanding_SG) ;
                  $("#Outstanding_Quasi_CG").text(data[0][0].Outstanding_Quasi_CG);  
                 $("#Outstanding_Quasi_SG").text(data[0][0].Outstanding_Quasi_SG) ; 
                 $("#Outstanding_LB").text(data[0][0].Outstanding_LB) ;
                  $("#Outstanding_Private").text(data[0][0].Outstanding_Private);  
                 $("#Outstanding_Total").text(data[0][0].Outstanding_Total)
            }
else{
    $('#modalFile').modal('toggle');
    $(' #tbodyvalue, #tablehead').empty();
}



           
              
                 }

                 function FillES21_Master(funct,control) {
                    var path =  serverpath + "ES21_Master"
                    securedajaxget(path,funct,'comment',control);
                  }
                  
                  function parsedataFillES21_Master(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        FillES21_Master('parsedataFillES21_Master','tbodyvalue');
                    }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[0];
                            var appenddata = ''
                       
                
                            for (var i = 0; i < data1.length; i++) {
                               
                              appenddata+= `<tr class='i'>
                              <td><label class='Edu21_id'>`+data1[i].Edu21_id+`</label></td>
                              <td colspan="3">`+data1[i].Edu_Name+`</td>
                
                              <td   id="Total_Regd`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="Total_Placement`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="Total_LR`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="W_Regd`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="W_Placement`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="W_LR`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="SC_Regd`+data1[i].Edu21_id+`">
                              </td>	
                              <td    id="SC_Placement`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="SC_LR`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="ST_Regd`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="ST_Placement`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="ST_LR`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="OBC_Regd`+data1[i].Edu21_id+`">
                              </td>
                              <td    id="OBC_Placement`+data1[i].Edu21_id+`">
                              </td>
                              <td   id="OBC_LR`+data1[i].Edu21_id+`">
                              </td>
                          
                          </tr>`
                             }
                             $("#tbodyvalue").append(appenddata);
                             ES21Entry();
                        }
                              
                  }


                  function ES21Entry(){
                    
                        if(sessionStorage.criteria=='1'){
                          //  var fromdate = ''+$("#years").val()+'/01/01'
                            var todate = ''+sessionStorage.yearrpt+'-06-30'
                            }
                            else if(sessionStorage.criteria=='2'){
                         //    var fromdate = ''+$("#years").val()+'/07/01/'
                                var todate = ''+sessionStorage.yearrpt+'-12-31'
                            }        
                          
                        var MasterData = {
                  
                            "p_Ex_id": sessionStorage.ES11Exid,  
                            "p_ToDt":  todate ,  
                            "p_Edu21_id":0, 
                            "p_Total_Regd":0,  
                            "p_Total_Placement":0,  
                            "p_Total_LR": 0,
                            "p_W_Regd":0,  
                            "p_W_Placement":0,  
                            "p_W_LR": 0,
                            "p_SC_Regd":0,
                            "p_SC_Placement": 0,
                            "p_SC_LR": 0,
                            "p_ST_Regd": 0,
                            "p_ST_Placement": 0,
                            "p_ST_LR": 0,
                            "p_OBC_Regd": 0,
                            "p_OBC_Placement": 0,
                            "p_OBC_LR": 0,
                            "p_Flag": '3', 
                        }
                        MasterData = JSON.stringify(MasterData)
                        var path = serverpath + "ES21Entry";
                        securedajaxpost(path,'parsrdataES21Entry','comment',MasterData,'control')
                   
                    
                }
                
                
                function parsrdataES21Entry(data){
                    data = JSON.parse(data)
                  
                    if(data[0].length){
                        for (var i = 0; i < data[0].length; i++) {
                            $('#Total_Regd'+data[0][i].Edu21_id+'').text(data[0][i].Total_Regd);
                            $('#Total_Placement'+data[0][i].Edu21_id+'').text(data[0][i].Total_Placement);
                            $('#Total_LR'+data[0][i].Edu21_id+'').text(data[0][i].Total_LR);
                        
                            $('#W_Regd'+data[0][i].Edu21_id+'').text(data[0][i].W_Regd);
                            $('#W_Placement'+data[0][i].Edu21_id+'').text(data[0][i].W_Placement);
                            $('#W_LR'+data[0][i].Edu21_id+'').text(data[0][i].W_LR);
                        
                            $('#SC_Regd'+data[0][i].Edu21_id+'').text(data[0][i].SC_Regd);
                            $('#SC_Placement'+data[0][i].Edu21_id+'').text(data[0][i].SC_Placement);
                            $('#SC_LR'+data[0][i].Edu21_id+'').text(data[0][i].SC_LR);
                        
                            $('#ST_Regd'+data[0][i].Edu21_id+'').text(data[0][i].ST_Regd);
                            $('#ST_Placement'+data[0][i].Edu21_id+'').text(data[0][i].ST_Placement);
                            $('#ST_LR'+data[0][i].Edu21_id+'').text(data[0][i].ST_LR);
                        
                            $('#OBC_Regd'+data[0][i].Edu21_id+'').text(data[0][i].OBC_Regd);
                            $('#OBC_Placement'+data[0][i].Edu21_id+'').text(data[0][i].OBC_Placement);
                            $('#OBC_LR'+data[0][i].Edu21_id+'').text(data[0][i].OBC_LR);
                        
                        }
                    }
                       
                   else{
                    $('#modalFile').modal('toggle');
                    $(' #tbodyvalue, #tablehead').empty();
                   } 
                    
                }
                