var date = new Date();
function FillCCMonthemp(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCCMonthemp(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillCCMonthemp('parsedatasecuredFillCCMonthemp',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    var month=new Array();
    var selectized = $("#"+control).selectize();
           
    var control1 = selectized[0].selectize;

    control1.destroy();
$("#"+control).selectize({
 persist: false,
 createOnBlur: true,
 valueField: 'MonthId',
 labelField: 'FullMonthNameHindi',
 searchField: 'FullMonthNameHindi',
 options: data1,
 create: false
 //maxItems:3
});
    // jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
    // for (var i = 0; i < data1.length; i++) {
    //     month[i]=data1[i].MonthId;
    // jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthNameHindi));
    // }
    // jQuery("#"+control).val(month[date.getMonth()])
    
    }
    
    }



    function FillYearemp(funct,control) {
        var path =  serverpath + "secured/year/0/20"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearemp(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearemp('parsedatasecuredFillYearemp',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
                 jQuery("#"+control).val(date.getFullYear())
            }
                  
    }



function FillDDlExchOffice(ReportID,control){
    var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
   securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', control)
      }
function parsedatasecuredFillDDlExchOffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDDlExchOffice();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
          
            
        }  
}




function FillEmploymentrpt(){
  

    var path = serverpath + "employmentRPTCategory/"+$('#ddlDistrict').val()+"/"+$('#Month').val()+"/"+$('#Year').val()+"";
   securedajaxget(path, 'parsedatasecuredFillEmploymentrpt', 'comment', 'control')
      }
function parsedatasecuredFillEmploymentrpt(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEmploymentrpt();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            $('#tablebody,#tablehead').empty();
            if(data.length){
            var data1= data[0]
           
             var appenddata="";
             
             var grandtotal_EmploymentExchange=0;
             var grandtotal_SC=0;
             var grandtotal_ST=0;
             var grandtotal_OBC=0;
             var grandtotal_UR=0;
             var grandtotal_Female=0;
             var grandtotal_PH=0;
             var grandtotal_Minority=0;
             var grandtotal_TotalJobFair=0;
             var grand_total=0;
          
            
            for (var i = 0; i < data1.length; i++) {
                
				grandtotal_EmploymentExchange+=parseInt(data1[i].EmploymentExchange);
                grandtotal_SC+=parseInt(data1[i].SC);
                grandtotal_ST+=parseInt(data1[i].ST);
                grandtotal_OBC+=parseInt(data1[i].OBC);
                grandtotal_UR+=parseInt(data1[i].UR);
                grandtotal_Female+=parseInt(data1[i].Female);
                grandtotal_PH+=parseInt(data1[i].PH);
                grandtotal_Minority+=parseInt(data1[i].Minority);
                grandtotal_TotalJobFair+=parseInt(data1[i].TotalJobFair);
                grand_total=grandtotal_EmploymentExchange+grandtotal_SC+grandtotal_ST+grandtotal_OBC+grandtotal_UR+grandtotal_Female+grandtotal_PH+grandtotal_Minority+grandtotal_TotalJobFair;

// Ex=Ex+data1[i].EmploymentExchangeCount
// NRLM=NRLM+data1[i].NRLMCount
// ITI=ITI+data1[i].ITICount
// OTHERCOLLEGES=OTHERCOLLEGES+data1[i].OtherCount
// T=T+total
// NA=NA+data1[i].NewAppointmentCount
// TE=TE+TOTALEMPLOYMENT
//var TOTAL=`<tr style=' background-color: aliceblue;font-weight: 700;'><td></td><td>Total</td><td>${Ex}</td><td>${NRLM}</td><td>${ITI}</td><td>${OTHERCOLLEGES}</td><td>${T}</td><td>${NA}</td><td>${TE}</td></tr>`;
      appenddata+=`<tr><td>`+[i+1]+`</td><td>${data1[i]['DistrictName']}</td><td>${data1[i].EmploymentExchange}</td><td>${data1[i].SC}</td><td>${data1[i].ST}</td><td>${data1[i].OBC}</td><td>${data1[i].UR}</td><td>${data1[i].Female}</td><td>${data1[i].PH}</td><td>${data1[i].Minority}</td><td>${data1[i].TotalJobFair}</td></tr>`;             
       
    } 
        var tablehead=`<th colspan=1 style='border-right: inset;border-bottom: inset;'>S.No</th>
<th colspan=1 style='border-right: inset;border-bottom: inset;'>District Name </th>

<th colspan=1 style='border-right: inset;border-bottom: inset;'>Employment Exchange</th>
<th style="border-bottom: inset;border-right: inset;">SC</th>
<th style="border-bottom: inset;border-right: inset;">ST</th>
<th style="border-bottom: inset;border-right: inset;">OBC</th>
<th style="border-bottom: inset;border-right: inset;">UR</th>
<th style="border-bottom: inset;border-right: inset;">Female</th>
<th style="border-bottom: inset;border-right: inset;">PH</th>
<th style="border-bottom: inset;border-right: inset;">Minority</th>
<th style="border-bottom: inset;border-right: inset;">Total Jobfair</th>` 
        $('#tablebody').html(appenddata+"<tr class='total'><td colspan=2><strong>Total</strong></td><td>"+grandtotal_EmploymentExchange+"</td><td>"+grandtotal_SC+"</td><td>"+grandtotal_ST+"</td><td>"+grandtotal_OBC+"</td><td>"+grandtotal_UR+"</td><td>"+grandtotal_Female+"</td><td>"+grandtotal_PH+"</td><td>"+grandtotal_Minority+"</td><td>"+grandtotal_TotalJobFair+"</td></tr>");  
        $('#tablehead').html(tablehead);
}
else{
    $('#tablehead').html("<th>No Records Found</th>");
}
}
}

function FillDistrictss() {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,'parsedatasecuredFillDistrictss','comment','control');
}

function parsedatasecuredFillDistrictss(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrictss();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery(`#ddlDistrict`).empty();
            var data1 = data[0];
         
           
                jQuery(`#ddlDistrict`).append(jQuery("<option></option>").val("0").html("All"));
                for (var i = 0; i < data1.length; i++) {
                jQuery(`#ddlDistrict`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'|| sessionStorage.User_Type_Id=='36'){
                $('#ddlDistrict,#districtlabel').hide();
                setTimeout(function(){ $('#ddlDistrict').val(sessionStorage.DistrictId); }, 300);
                }
             
        }
              
}