var date = new Date();
function FillCCMonthemp(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillCCMonthemp(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillCCMonthemp('parsedatasecuredFillCCMonthemp',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    var month=new Array();
    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
        month[i]=data1[i].MonthId;
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthNameHindi));
    }
     jQuery("#"+control).val(date.getMonth())
    
    }
    
    }



    function FillYearemp(funct,control) {
        var path =  serverpath + "secured/year/0/20"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearemp(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearemp('parsedatasecuredFillYearemp',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
                 jQuery("#"+control).val(date.getFullYear())
            }
                  
    }



function FillDDlExchOffice(ReportID,control){
    var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
   securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', control)
      }
function parsedatasecuredFillDDlExchOffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDDlExchOffice();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
          
            
        }  
}




function FillEmploymentrpt(){
  

    var path = serverpath + "employmentdeptwise/"+$('#ddlDistrict').val()+"/"+$('#Month').val()+"/"+$('#Year').val()+"";
   securedajaxget(path, 'parsedatasecuredFillEmploymentrpt', 'comment', 'control')
      }
function parsedatasecuredFillEmploymentrpt(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillEmploymentrpt();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            $('#tablebody,#tablehead').empty();
            if(data.length){
            var dataColumn= data[0][0].DyColumns.split(',');
            var data1= data[1]
            var deptColumn='';
            var dynColumn=""
            var selfempTotal=0
             var appenddata="";
           
          
             for(var j=0;j<dataColumn.length;j++){
                 
                deptColumn+=`<th style='background: #716aca;border-right: inset;border-bottom:inset'>${dataColumn[j]}</th>`
             //   dynColumn+=`<td>${data1[i][dataColumn[j]]}</td>`;
                
            //    selfempTotal+=parseInt(data1[i][dataColumn[j]]);
            }
            for (var i = 0; i < data1.length; i++) {
               // $("#headingtext").html("माह "+' '+$('#Month option:selected').text()+'-'+$('#Year').val() +" में रोजगार की जानकारी" )
               // var head='<tr><th></th><th colspan=12>'+"माह "+' '+$('#Month option:selected').text()+'-'+$('#Year').val() +" में रोजगार की जानकारी"+'</th></tr>';
               // $('#Detailhead').html(head);
               dynColumn="";
               selfempTotal=0;
               for(var j=0;j<dataColumn.length;j++){
                if(data1[i][dataColumn[j]]!=null){
                    var dataColumnj=data1[i][dataColumn[j]]
                   }
                   else{
                     var dataColumnj=0
                   }
                 
             //   deptColumn+=`<th style='background: #716aca;border-right: inset;border-bottom:inset'>${dataColumn[j]}</th>`
                // dynColumn+=`<td>${data1[i][dataColumn[j]]}</td>`;
                dynColumn+=`<td>${dataColumnj}</td>`;
                
            //    selfempTotal+=parseInt(data1[i][dataColumn[j]]);
            selfempTotal+=parseInt(dataColumnj);
            }
  
                if(data1[i]['New Appointment']==null){
                    var newaapp=0;
                }
                else{
                    var newaapp=data1[i]['New Appointment'];
                }
                if(data1[i]['SC']==null){
                    var sc=0;
                }
                else{
                    var sc=data1[i]['SC'];
                }
                if(data1[i]['ST']==null){
                    var st=0;
                }
                else{
                    var st=data1[i]['ST'];
                }
                if(data1[i]['OBC']==null){
                    var obc=0;
                }
                else{
                    var obc=data1[i]['OBC'];
                }
                
                if(data1[i]['UR']==null){
                    var ur=0;
                }
                else{
                    var ur=data1[i]['UR'];
                }
                
                if(data1[i]['Female']==null){
                    var female=0;
                }
                else{
                    var female=data1[i]['Female'];
                }
                if(data1[i]['PH']==null){
                    var ph=0;
                }
                else{
                    var ph=data1[i]['PH'];
                }
                if(data1[i]['Minority']==null){
                    var minority=0;
                }
                else{
                    var minority=data1[i]['Minority'];
                }
//                 total=data1[i].EmploymentExchangeCount+data1[i].NRLMCount+data1[i].ITICount+data1[i].OtherCount
                TOTALEMPLOYMENT=selfempTotal+parseInt(newaapp);
// Ex=Ex+data1[i].EmploymentExchangeCount
// NRLM=NRLM+data1[i].NRLMCount
// ITI=ITI+data1[i].ITICount
// OTHERCOLLEGES=OTHERCOLLEGES+data1[i].OtherCount
// T=T+total
// NA=NA+data1[i].NewAppointmentCount
// TE=TE+TOTALEMPLOYMENT
//var TOTAL=`<tr style=' background-color: aliceblue;font-weight: 700;'><td></td><td>Total</td><td>${Ex}</td><td>${NRLM}</td><td>${ITI}</td><td>${OTHERCOLLEGES}</td><td>${T}</td><td>${NA}</td><td>${TE}</td></tr>`;
      appenddata+=`<tr><td>`+[i+1]+`</td><td>${data1[i]['DistrictName_a']}</td>${dynColumn}<td> ${selfempTotal} </td><td>${newaapp}</td><td>${TOTALEMPLOYMENT}</td><td>${sc}</td><td>${st}</td><td>${obc}</td><td>${ur}</td><td>${female}</td><td>${ph}</td><td>${minority}</td></tr>`;             
       
    } 
        var tablehead=`<th colspan=1 style='border-right: inset;border-bottom: inset;'>S.No</th>
<th colspan=1 style='border-right: inset;border-bottom: inset;'>District Name </th>
${deptColumn}
<th colspan=1 style='border-right: inset;border-bottom: inset;'> Total </th>
<th colspan=1 style='border-right: inset;border-bottom: inset;border-bottom: inset;'> New Appointment </th>
<th  colspan=1 style="border-bottom: inset;border-right: inset;">Total Employement</th>
<th style="border-bottom: inset;border-right: inset;">SC</th>
<th style="border-bottom: inset;border-right: inset;">ST</th>
<th style="border-bottom: inset;border-right: inset;">OBC</th>
<th style="border-bottom: inset;border-right: inset;">UR</th>
<th style="border-bottom: inset;border-right: inset;">Female</th>
<th style="border-bottom: inset;border-right: inset;">PH</th>
<th style="border-bottom: inset;border-right: inset;">Minority</th>` 
        $('#tablebody').html(appenddata);
        $('#tablehead').html(tablehead);
}
else{
    $('#tablehead').html("<th>No Records Found</th>");
}
}
}

function FillDistrictss() {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,'parsedatasecuredFillDistrictss','comment','control');
}

function parsedatasecuredFillDistrictss(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrictss();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery(`#ddlDistrict`).empty();
            var data1 = data[0];
         
           
                jQuery(`#ddlDistrict`).append(jQuery("<option></option>").val("0").html("All"));
                for (var i = 0; i < data1.length; i++) {
                jQuery(`#ddlDistrict`).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'|| sessionStorage.User_Type_Id=='36'){
                $('#ddlDistrict,#districtlabel').hide();
                setTimeout(function(){ $('#ddlDistrict').val(sessionStorage.DistrictId); }, 300);
                }
             
        }
              
}