

function FillMonth(funct,control) {
  var path =  serverpath + "secured/month/0"
  securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonth(data,control){  
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillMonth('parsedatasecuredFillMonth','monthlyregisterdatewisemonth');
  }
  else if (data.status == 401){
      toastr.warning("Unauthorized", "", "info")
      return true;
  }
      else{
       

          jQuery("#"+control).empty();
          var data1 = data[0];
          jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
           }

          
      }
            
}

function FillES11Rpt(){

  var MasterData = {
      
"p_Ex_id":sessionStorage.Ex_id,
"p_Rpt_Month":$("#monthlyregisterdatewisemonth").val(),
"p_Rpt_Year":$("#textyear").val(),
"p_Prev_LR_M":$("#es1").val(),
"p_ReceiveRegistration_M":$("#es4").val(),
"p_FreshRegistration_M":$("#es7").val(),
"p_ReRegistration_M":$("#es10").val(),
"p_Total_3_M":$("#es13").val(),
"p_PlacedRegistration_M":$("#es16").val(),
"p_RemovedRegistration_M":$("#es19").val(),
"p_TransferRegistration_M":$("#es22").val(),
"p_Total_7_M":$("#es25").val(),
"p_LR_M":$("#es28").val(),
"p_Rural_LR_M":$("#es31").val(),
"p_Submissions_M":$("#es34").val(),
"p_NoOfNotify_M":$("#es37").val(),
"p_Prev_LR_F":$("#es2").val(),
"p_ReceiveRegistration_F":$("#es5").val(),
"p_FreshRegistration_F":$("#es8").val(),
"p_ReRegistration_F":$("#es11").val(),
"p_Total_3_F":$("#es14").val(),
"p_PlacedRegistration_F":$("#es17").val(),
"p_RemovedRegistration_F":$("#es20").val(),
"p_TransferRegistration_F":$("#es23").val(),
"p_Total_7_F":$("#es26").val(),
"p_LR_F":$("#es29").val(),
"p_Rural_LR_F":$("#es32").val(),
"p_Submissions_F":$("#es35").val(),
"p_NoOfNotify_F":$("#es38").val(),
"p_Total_PrevLR":$("#es3").val(),
"p_ReceiveRegistration":$("#es6").val(),
"p_FreshRegistration":$("#es9").val(),
"p_ReRegistration":$("#es12").val(),
"p_Total_3":$("#es15").val(),
"p_PlacedRegistration":$("#es18").val(),
"p_RemovedRegistration":$("#es21").val(),
"p_TransferRegistration":$("#es24").val(),
"p_Total_7":$("#es27").val(),
"p_LR":$("#es30").val(),
"p_Rural_LR":$("#es33").val(),
"p_TotalSubmission":$("#es36").val(),
"p_TotalNotify":$("#es39").val(),
"p_Verify_YN":0,

   }
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/ES11_Entry_Rpt";
securedajaxpost(path,'parsrdataFillES11Rpt','comment',MasterData,'control')
}


function parsrdataFillES11Rpt(data){
  data = JSON.parse(data)
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      FillES11Rpt();
      
  }
  
  if (data[0][0].ReturnValue == "1") {
      
        toastr.success("Insert Successful", "", "success")
        $('.disable,.VJdisable').val('');
        return true;
    }
     else if (data[0][0].ReturnValue == "2") {

        toastr.success("Update Successful", "", "success")
        $('.disable,.VJdisable').val('');
        return true;
    }
   }

   function FetchES11Rpt(){
   
    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
var ex_id =  $("#ddExchange").val();
    }
else{
  var ex_id =  sessionStorage.Ex_id;
}
    var path =  serverpath + "ES11_Rpt/'"+ex_id+"'/'"+$("#monthlyregisterdatewisemonth").val()+"'/'"+$("#textyear").val()+"'"
    ajaxget(path,'parsedataFetchES11Rpt','comment',"control");
   }
  
   function parsedataFetchES11Rpt(data){
    data = JSON.parse(data)
   if(data[0][0].Verify_YN=='1'){
    $( ".disable" ).prop( "disabled", true );
    $( ".VJdisable" ).prop( "disabled", true );
   }

   else   if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
    if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
      $( ".disable,.VJdisable" ).prop( "disabled", true );
    }
    if(sessionStorage.User_Type_Id=='7'){
      $('#btnSave').show();
      $( ".disable" ).prop( "disabled", true );
      $( ".VJdisable" ).prop( "disabled", false );
    }
    // if(sessionStorage.User_Type_Id=='1'){
    //   	$('#sbmtrpt').show();
    //     $('#btnSave').show();
    //     $( ".disable" ).prop( "disabled", false );
    //   }
      if(sessionStorage.User_Type_Id=='2'){
        $('#btnSave').show();
        $( ".disable" ).prop( "disabled", true );
        $( ".VJdisable" ).prop( "disabled", false );
      }
 
   }

   if(data[0][0]!==null){
    if(sessionStorage.User_Type_Id=='2'){
      $('#sbmtrpt').show();
      $('#btnSave').show();
    
    }
  }
// 3-7=8
var subtract_1=0; var subtract_2=0; var subtract_3=0;
   $('#year_month').text($("#monthlyregisterdatewisemonth option:selected").text()+' '+$("#textyear").val())
        $("#es1").val(data[0][0].Prev_LR_M),
        $("#es2").val(data[0][0].Prev_LR_F), 
        $("#es3").val(data[0][0].Prev_LR_M+data[0][0].Prev_LR_F),
        $("#es4").val(data[0][0].ReceiveRegistration_M),
        $("#es5").val(data[0][0].ReceiveRegistration_F), 
        $("#es6").val(data[0][0].ReceiveRegistration_M+data[0][0].ReceiveRegistration_F),
        $("#es7").val(data[0][0].FreshRegistration_M),
        $("#es8").val(data[0][0].FreshRegistration_F), 
        $("#es9").val(data[0][0].FreshRegistration_M+data[0][0].FreshRegistration_F),
        $("#es10").val(data[0][0].ReRegistration_M),
        $("#es11").val(data[0][0].ReRegistration_F), 
        $("#es12").val(data[0][0].ReRegistration_M+data[0][0].ReRegistration_F)
        var Total_3_M=parseInt(data[0][0].Prev_LR_M)+data[0][0].ReceiveRegistration_M+data[0][0].FreshRegistration_M+data[0][0].ReRegistration_M;
        $("#es13").val(Total_3_M)
        var Total_3_F=data[0][0].Prev_LR_F+data[0][0].ReceiveRegistration_F+data[0][0].FreshRegistration_F+data[0][0].ReRegistration_F;
        $("#es14").val(Total_3_F)
        var Total_3=Total_3_M+Total_3_F;
        $("#es15").val(Total_3);

        

        $("#es16").val(data[0][0].PlacedRegistration_M),
        $("#es17").val(data[0][0].PlacedRegistration_F), 
        $("#es18").val(data[0][0].PlacedRegistration_M+data[0][0].PlacedRegistration_F),
        $("#es19").val(data[0][0].RemovedRegistration_M),
        $("#es20").val(data[0][0].RemovedRegistration_F),
        $("#es21").val(data[0][0].RemovedRegistration_M+data[0][0].RemovedRegistration_F), 
        $("#es22").val(data[0][0].TransferRegistration_M),
        $("#es23").val(data[0][0].TransferRegistration_F),
        $("#es24").val(data[0][0].TransferRegistration_M+data[0][0].TransferRegistration_F)
        var Total_7_M=data[0][0].PlacedRegistration_M+data[0][0].RemovedRegistration_M+data[0][0].TransferRegistration_M;
        var Total_7_F=data[0][0].PlacedRegistration_F+data[0][0].RemovedRegistration_F+data[0][0].TransferRegistration_F;
        var Total_7=Total_7_M+Total_7_F;
        $("#es25").val(Total_7_M),
        $("#es26").val(Total_7_F),
        $("#es27").val(Total_7), 

 subtract_1=parseInt(Total_3_M)-$('#es25').val();
        $("#es28").val(subtract_1);

         subtract_2=parseInt(Total_3_F)-$('#es26').val();
        $("#es29").val(subtract_2);

         subtract_3=parseInt(Total_3)-$('#es27').val();
        $("#es30").val(subtract_3);
       // $("#es28").val(data[0][0].LR_M),
       // $("#es29").val(data[0][0].LR_F),
      //  $("#es30").val(data[0][0].LR_M+data[0][0].LR_F),
        $("#es31").val(data[0][0].Rural_LR_M),
        $("#es32").val(data[0][0].Rural_LR_F),
        $("#es33").val(data[0][0].Rural_LR_M+data[0][0].Rural_LR_F), 
        $("#es34").val(data[0][0].Submissions_M),
        $("#es35").val(data[0][0].Submissions_F),
        $("#es36").val(data[0][0].Submissions_M+data[0][0].Submissions_F), 
        $("#es37").val(data[0][0].NoOfNotify_M),
        $("#es38").val(data[0][0].NoOfNotify_F),
        $("#es39").val(data[0][0].NoOfNotify_M+data[0][0].NoOfNotify_F)
    

   }

   function ES11RptSubmit(usertypeid){
    ddExchange

    var MasterData = {
   "p_Ex_id":sessionStorage.Ex_id,
  "p_Rpt_Month":$("#monthlyregisterdatewisemonth").val(),
  "p_Rpt_Year":$("#textyear").val(),
   "p_User_Type_Id":usertypeid,

     }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "ES11_Submit_Rpt";
  securedajaxpost(path,'parsrdataES11RptSubmit','comment',MasterData,'control')
  }
  
  
  function parsrdataES11RptSubmit(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        ES11RptSubmit();
    }
          toastr.success("Records Successfully Send To Head Office", "", "success")
          $( ".disable" ).prop( "disabled", true );
          $( "#es37" ).prop( "disabled", true );
          return true;
     }


   function total(){
 var ft= parseInt($('#es16').val())+ parseInt($('#es17').val());
   $('#es18').val(ft);
 
   var nt= parseInt($('#es34').val())+ parseInt($('#es35').val());
   $('#es36').val(nt);

   var tt= parseInt($('#es37').val())+ parseInt($('#es38').val());
   $('#es39').val(tt);

   var t1=parseInt($('#es16').val())+parseInt($('#es19').val())+parseInt($('#es22').val());
   $('#es25').val(t1);

   var t2=parseInt($('#es17').val())+parseInt($('#es20').val())+parseInt($('#es23').val());
   $('#es26').val(t2);

   var t3=parseInt($('#es18').val())+parseInt($('#es21').val())+parseInt($('#es24').val());
   $('#es27').val(t3);
}
