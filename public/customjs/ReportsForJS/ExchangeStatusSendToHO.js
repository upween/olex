function FillDDlReportName(){

    var path = serverpath + "secured/GetRptName/0/'JS'/1";
    securedajaxget(path, 'parsedatasecuredGetRptName', 'comment', 'control')
    }
    function parsedatasecuredGetRptName(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillDDlReportName();
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    var data1 = data[0];
    
    jQuery("#textReportName").append(jQuery("<option ></option>").val("0").html("Select Report Name"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#textReportName").append(jQuery("<option></option>").val(data1[i].RptID).html(data1[i].RptNameEng));
    }
    }
    
    }
    function FillMonthddl(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillMonth(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillMonthddl('parsedatasecuredFillMonth',control);
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
    }
    
    
    }
    
    }
    function handleDDlchange(){
    var rptid=jQuery("#textReportName").val();
    sessionStorage.RptID=rptid;
    if(rptid=='101'){
    $('.criteriaddl').hide();
    $('#lblyearmonth').text('Select Month & Year :');
    $('.monthddl, .yearddl').show();
    }
    if(rptid=='102' || rptid=='113'){
    $('.monthddl, .criteriaddl').hide();
    $('#lblyearmonth').text('Select Year :');
    $('.yearddl').show();
    }
    if(rptid=='103' ||rptid=='104' ||rptid=='105' ||rptid=='106' ||rptid=='107' ||rptid=='108' ||rptid=='109' || rptid=='110'){
    $('.monthddl').hide();
    $('#lblyearmonth').text('Select Year :');
    $('.yearddl,.criteriaddl').show();
    }
    }
    function FillMonthsendtoho(funct,control) {
    var path = serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillMonthsendtoho(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillMonthsendtoho('parsedatasecuredFillMonthsendtoho');
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    
    
    // var appenddata="";
    // var data1 = data[0];
    // for (var i = 0; i < data1.length; i++) {
    // appenddata += "<tr><td>" + data1[i].Ex_id + "</td><td>" + data1[i].Ex_name + "</td></tr>";
    // }
    // jQuery("#tbodyvalue").html(appenddata);
    
    
    // var appenddata="";
    // var data1 = data[0];
    
    // if ($("#textReport" ).val() == "2" && $("#textYear" ).val() == "2019") {
    
    // for (var i = 0; i < data1.length; i++) {
    // appenddata += "<tr><td>" + data1[i].Ex_id + "</td><td>" + data1[i].Ex_name + "</td></tr>";
    // }
    
    // }
    
    // jQuery("#tbodyvalue").html(appenddata);
    
    jQuery("#exhchangestatussendtomonth").empty();
    var data1 = data[0];
    jQuery("#exhchangestatussendtomonth").append(jQuery("<option ></option>").val("0").html("Select Month"));
    for (var i = 0; i < data1.length; i++) {
    jQuery("#exhchangestatussendtomonth").append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
    }
    
    
    }
    
    }
    
    // function fetchExchangeName(funct) {
    // var path = serverpath + "exchangeoffice/0/0/1"
    // securedajaxget(path,funct,'comment','control');
    // }
    
    // function parsedatasecuredFillSecuredexchangeoffice(data){
    // data = JSON.parse(data)
    // if (data.message == "New token generated"){
    // sessionStorage.setItem("token", data.data.token);
    // FillSecuredexchangeoffice('parsedatasecuredFillSecuredexchangeoffice');
    // }
    // else if (data.status == 401){
    // toastr.warning("Unauthorized", "", "info")
    // return true;
    // }
    // else{
    
    // var appenddata="";
    // var data1 = data[0];
    
    // if ($("#textReport" ).val() == "2" && $("#textYear" ).val() == "2019") {
    
    // for (var i = 0; i < data1.length; i++) {
    // appenddata += "<tr><td>" + data1[i].Ex_id + "</td><td>" + data1[i].Ex_name + "</td></tr>";
    // }
    
    // }
    
    // jQuery("#tbodyvalue").html(appenddata);
    
    // }
    
    // }
    
    function FetchNotSendHOExchStatus() {
    var rptid=$("#textReportName").val();
    var FromDt='2020-01-01' ;
    var ToDt='2020-01-01';
    if(rptid=='103' ||rptid=='104' ||rptid=='105' ||rptid=='106' ||rptid=='107' ||rptid=='108' ||rptid=='109' || rptid=='110')
    {
    if($('#criteriaddl').val()=='1'){
    FromDt=`${$("#textYear").val()}-01-01`;
    ToDt=`${$("#textYear").val()}-06-30`;
    }
    else if($('#criteriaddl').val()=='2'){
    FromDt=`${$("#textYear").val()}-07-01`;
    ToDt=`${$("#textYear").val()}-12-31`;
    }
    }
    if(rptid=='102' || rptid=='113'|| rptid=='101'){
    FromDt=`${$("#textYear").val()}-01-01`;
    ToDt=`${$("#textYear").val()}-12-31`;
    }
    if ($("#textReport" ).val() == "1") {
    var path = serverpath + "secured/NotSendHOExchStatus/'"+FromDt+"'/'"+ToDt+"'/" + $("#exhchangestatussendtomonth").val() + "/" + $("#textYear").val() + "/" + $("#textReportName").val() + ""
    
    // var path = serverpath + "secured/RptHOExchStatu/'"+FromDt+"'/'"+ToDt+"'/" + $("#exhchangestatussendtomonth").val() + "/" + $("#textYear").val() + "/" + $("#textReportName").val() + ""
    }
    else if ($("#textReport" ).val() == "2") {
    var path = serverpath + "secured/RptHOExchStatu/'"+FromDt+"'/'"+ToDt+"'/" + $("#exhchangestatussendtomonth").val() + "/" + $("#textYear").val() + "/" + $("#textReportName").val() + ""
    
    // var path = serverpath + "secured/NotSendHOExchStatus/'"+FromDt+"'/'"+ToDt+"'/" + $("#exhchangestatussendtomonth").val() + "/" + $("#textYear").val() + "/" + $("#textReportName").val() + ""
    }
    securedajaxget(path, 'parsedatasecuredFetchNotSendHOExchStatus', 'comment', "control");
    
    
    }
    function parsedatasecuredFetchNotSendHOExchStatus(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FetchNotSendHOExchStatus('parsedatasecuredFetchNotSendHOExchStatus');
    }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    
    var appenddata="";
    var data1 = data[0];
    
    // if ($("#textReport" ).val() == "2") {
    
    for (var i = 0; i < data1.length; i++) {
    appenddata += "<tr><td>" + [i+1] + "</td><td style='text-align: left;'>" + data1[i].Ex_name + "</td></tr>";
    // }
    
    $("#headingtext").html(sessionStorage.Ex_name+"</br>"+$('#textReport option:selected').html()+"-"+$('#textReportName option:selected').html())
    
    var head='<th></th><th colspan=1>'+sessionStorage.Ex_name+' </br>Report: '+$('#textReport option:selected').html()+'<br>Report Name:'+$('#textReportName option:selected').html()+'<br>Month & Year :'+$('#exhchangestatussendtomonth option:selected').html()+$('#textYear option:selected').html()+'</th>';
    
    
    $('#Detailhead').html(head)
    
    }
    
    jQuery("#tbodyvalue").html(appenddata);
    
    }
    
    }