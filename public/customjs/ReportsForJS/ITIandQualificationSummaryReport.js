function FillMonths(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthReg(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonths('parsedatasecuredFillMonthReg','ddlmonth');
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#"+control).empty();
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}

function FillExamPassed(funct,control) {
    var path =  serverpath + "secured/qualification/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExamPassed(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExamPassed('parsedatasecuredFillExamPassed','ExamPassed');
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
         
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}


function ITIQualificationsummaryRep(){
    if($("#type").val()=='0'){
    var path =  serverpath + "ITIQualificationWiseSummaryReport/'"+$("#ddlmonth").val()+"'/'"+$("#ddlJsLiveyear").val()+"'/'0'/'"+$("#ExamPassed").val()+"'"
    ajaxget(path,'parsedataITIQualificationsummaryRep','comment',"control");
}
}
function parsedataITIQualificationsummaryRep(data){
    data = JSON.parse(data)
    var appenddata="";
     var data1 = data[0]; 
     
          for (var i = 0; i < data1.length; i++) {
            if($("#type").val()=='0'){

            var tablehead="<tr><th>Sno</th><th colspan=1>Exchange Name</th><th colspan=3>UR</th><th colspan=3>SC</th><th colspan=3>ST</th><th colspan=3>OBC</th><th>Total</th><th>PH</th><th colspan=3>Hindu</th><th colspan=3>Islam</th><th colspan=3>Sikh</th><th colspan=3>Christian</th><th colspan=3>Parsi</th><th colspan=3>Buddhist</th><th colspan=3>Jain</th><th colspan=3>Zorist</th><th colspan=3>Other</th></tr>"+
                          "<tr><th></th><th></th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th></th><th></th>"+
                          "<th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th></tr>";
    
        
                 appenddata += "<tr class='i'><td>" +[i+1]+ "</td><td style='text-align: left;'>" + data1[i].Sub_Group_Name+ "</td><td>" + data1[i].URCount_M + "</td><td>"+data1[i].URCount_F+"</td><td>"+data1[i].URCount_T+"</td><td>"+data1[i].SCCount_M+"</td><td>"+data1[i].SCCount_F+"</td><td>"+data1[i].SCCount_T+"</td><td>"+data1[i].STCount_M+"</td><td>"+data1[i].STCount_F+"</td><td>"+data1[i].STCount_T+"</td><td style='    word-break: break-word;'>" +data1[i].OBCCount_M+ "</td><td>" + data1[i].OBCCount_F+ "</td><td>" +data1[i].OBCCount_T+ "</td><td>" + data1[i].Total+ "</td><td>" + data1[i].Total_Handicap+ "</td>"+
                               "<td>" + data1[i].Hindu_Count_M+ "</td><td>" + data1[i].Hindu_Count_F+ "</td><td>" + data1[i].Hindu_Count_T+ "</td><td>" + data1[i].Islam_Count_M+ "</td><td>" + data1[i].Islam_Count_F+ "</td><td>" + data1[i].Islam_Count_T+ "</td><td>" + data1[i].Sikh_Count_M+ "</td><td>" + data1[i].Sikh_Count_F+ "</td><td>" + data1[i].Sikh_Count_T+ "</td><td>" + data1[i].Christian_Count_M+ "</td><td>" + data1[i].Christian_Count_F+ "</td><td>" + data1[i].Christian_Count_T+ "</td><td>" + data1[i].Parsi_Count_M+ "</td><td>" + data1[i].Parsi_Count_F+ "</td><td>" + data1[i].Parsi_Count_T+ "</td><td>" + data1[i].Buddhist_Count_M+ "</td>"+
                               "<td>" + data1[i].Buddhist_Count_F+ "</td><td>" + data1[i].Buddhist_Count_T+ "</td><td>" + data1[i].Jain_Count_M+ "</td><td>" + data1[i].Jain_Count_F+ "</td><td>" + data1[i].Jain_Count_T+ "</td><td>" + data1[i].Zorist_Count_M+ "</td><td>" + data1[i].Zorist_Count_F+ "</td><td>" + data1[i].Zorist_Count_T+ "</td><td>" + data1[i].Other_Count_M+ "</td><td>" + data1[i].Other_Count_F+ "</td><td>" + data1[i].Other_Count_T+ "</td></tr>";
            
           }
        }
       jQuery("#tbodyvalue").html(appenddata);  
       $("#tablehead").html(tablehead);
       $('#tbodyvalue').clientSidePagination();

    }



   


 
    