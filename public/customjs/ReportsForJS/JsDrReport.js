function fetchDRRecords() {
    
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "Lapse/"+$('#textContactYear').val()+"",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
     $("#headingtext").html(sessionStorage.Ex_name+" </br>Jobseeker DR Report "+$('#textContactYear').val() )
    var head='<th></th><th colspan=2>'+sessionStorage.Ex_name+' </br>Report Name : Jobseeker DR Report '+$('#textContactYear').val()+'</th>';
     $('#Detailhead').html(head)
    var tablehead="<tr><th>S.No</th><th>Month</th><th>DR Candidate Count</th></tr>"
  				var appenddata="";
			
               
                var grand_total=0;
				for (var i = 0; i < data1.length; i++) {
                    grand_total+=parseInt(data1[i].CandidateCount)
				appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i].FullMonthName+"</td><td>"+data1[i].CandidateCount+"</td></tr>";
				 }jQuery("#tbodyvalue").html(appenddata+"<tr class='total'><td></td><td><strong>Total</strong></td><td>"+grand_total+"</td></tr>");  
                 $("#tablehead").html(tablehead);
            

                 
        },
        
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}


function FillYear(funct,control) {
    var path =  serverpath + "secured/year/0/10"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    $.unblockUI()
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYear('parsedatasecuredFillYear','textContactYear');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

            
        }
              
}