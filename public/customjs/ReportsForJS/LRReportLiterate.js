function fetchLRLiterate() {
    var data =$('#m_datepicker_5').val();
    var arr = data.split('/');
    var frm_dt=arr[2] + "-" + arr[1]+"-"+arr[0]

    var data1 =$('#m_datepicker_6').val();
    var arr1 = data1.split('/');
    var to_dt=arr1[2] + "-" + arr1[1]+"-"+arr1[0]
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "Lrlitterate/'"+frm_dt+"'/'"+to_dt+"'",
        cache: false,
        dataType: "json",
        success: function (data) {
    var data1= data[0]
    var head="<th>Report Name-LR Report For Literate And Illiterate</th><th>From Date-"+$('#m_datepicker_5').val()+"</th><th>TO Date-"+$('#m_datepicker_6').val()+"</th>";
    $("#headingtext").html(sessionStorage.Ex_name+" </br>LR Report For Literate And Illiterate "+$('#m_datepicker_5').val()+'-'+$('#m_datepicker_6').val() )
    var head='<th></th><th colspan=4>'+sessionStorage.Ex_name+' </br>Report Name : LR Report For Literate And Illiterate '+$('#m_datepicker_5').val()+'-'+$('#m_datepicker_6').val()+'</th>';
    $('#Detailhead').html(head)
    var tablehead="<tr><th>S.No</th><th>Exchange Name</th><th>Literate</th><th>Illiterate</th><th>Total</th></tr>"
  				var appenddata="";
				var total=0;
                var grandtotal_literate=0;
                var grandtotal_illiterate=0;
                var grand_total=0;
				for (var i = 0; i < data1.length; i++) {
				total=parseInt(data1[i].Literate)+parseInt(data1[i].Illiterate);
				grandtotal_literate+=parseInt(data1[i].Literate);
                grandtotal_illiterate+=parseInt(data1[i].Illiterate);
                grand_total=grandtotal_literate+grandtotal_illiterate;	
				appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i].Ex_Name+"</td><td>"+data1[i].Literate+"</td><td>"+data1[i].Illiterate+"</td><td>"+total+"</td></tr>";
				 }jQuery("#tbodyvalue").html(appenddata+"<tr class='total'><td></td><td><strong>Total</strong></td><td>"+grandtotal_literate+"</td><td>"+grandtotal_illiterate+"</td><td>"+grand_total+"</td></tr>");  
                 $("#tablehead").html(tablehead);
                 $('#Detailhead').html(head);

                 
        },
        
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
