//DIMPLE//
function FillMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonth('parsedatasecuredFillMonth','monthlyregisterdatewisemonth');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}

function FilldivExchange(funct,control) {
    var path =  serverpath + "MstDivision/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFilldivExchange(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FilldivExchange('parsedatasecuredFilldivExchange','ddlDivision');
     
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Division Name"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Division_id).html(data1[i].Division_name));
             }
        }
              
}

jQuery('#ddlDivision').on('change', function () {

    FillExchange('parsedatasecuredFillExchange','ddExchange',jQuery('#ddlDivision').val());
    
});


function FillExchange(funct,control) {
    var path =  serverpath + "WorkingExchange/2/"+jQuery('#ddlDivision').val()+"/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExchange(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExchange('parsedatasecuredFillExchange','ddExchange');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Exchange"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
        }
              
}

function check(){
    if ($("#ddlDivision").val()=='0') {
        toastr.warning("Please Select Division", "", "info")
         return true;
 }
 else if ($("#ddExchange").val()=='0') {
    toastr.warning("Please Select Exchange Name", "", "info")
     return true;
}
else if ($("#monthlyregisterdatewisemonth").val()=='0') {
    toastr.warning("Please Select Status on Month ", "", "info")
     return true;
}
else if ($("#textyear").val()=='0') {
    toastr.warning("Please Select Status on Year ", "", "info")
     return true;
}
else{
    FillMonthlyRegDateWise('parsedatasecuredFillMonthlyRegDateWise');
}
}


function FillMonthlyRegDateWise(funct) {
    var path =  serverpath + "MonthlyRegistrationDateWise/"+jQuery('#ddlDivision').val()+"/"+jQuery('#ddExchange').val()+"/"+jQuery('#monthlyregisterdatewisemonth').val()+"/"+jQuery('#textyear').val()+""
    securedajaxget(path,funct,'comment');
}

function parsedatasecuredFillMonthlyRegDateWise(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthlyRegDateWise('parsedatasecuredFillMonthlyRegDateWise');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata1 ="";
           var days=daysInMonth(jQuery('#monthlyregisterdatewisemonth').val(),jQuery('#textyear').val())//"<th>Day</th>"
            
           Tablehead();
            for (var i = 0; i < data1.length; i++) {
                var appenddata ="";
                for (var j = 1; j <= days;) {
                    appenddata += "<th>"+ data1[i]['Day'+j] +"</th>";
                    j++;
                }
                appenddata1 += "<tr><th>"+[i+1]+"</th><th>"+data1[i].Division_Name+"</th><th>"+data1[i].Ex_name+"</th>"+appenddata+"</tr>";
            }
            $("#headingtext").html(sessionStorage.Ex_name+" </br>Monthly Register Date Wise  ")
            var head='<th></th><th colspan=2>'+sessionStorage.Ex_name+' </br>Report Name : Monthly Register Date Wise  <br> Division :'+$('#ddlDivision option:selected').html()+',<br>Exchange Name :'+$('#ddExchange option:selected').html()+',<br>Status on Month :'+$('#monthlyregisterdatewisemonth option:selected').html()+' '+$('#textyear option:selected').html()+'</th>';
            $('#Detailhead').html(head)

            $("#tbodyvalue").html(appenddata1)
$('#excel').show();
        }
              



}

function Tablehead(){
    var tablehead ="";
         
    var days=daysInMonth(jQuery('#monthlyregisterdatewisemonth').val(),jQuery('#textyear').val())//"<th>Day</th>"
     
    tablehead = "<th>Sr No</th><th>Division</th><th>Exchange</th>";
     for (var i = 1; i <= days;) {
         tablehead += "<th>"+"Day"+i+"</th>";
         i++;
     }
     $("#tablehead").html(tablehead)
}
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}



function FillYears(funct,control) {
    var path =  serverpath + "secured/year/0/4"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    $.unblockUI()
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYears('parsedatasecuredFillYear',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

            
        }
              
}