function FillJSQualiLevel(funct,control) {
    var path =  serverpath + "secured/Qualification/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJSQualiLevel(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJSQualiLevel('parsedatasecuredFillJSQualiLevel','QualificationLevel');
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
         
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_Level_id).html(data1[i].Qualif_Level_name));
            }
        }
                  
}
jQuery('#QualificationLevel').on('change', function () {
    FillJSExamPassed('parsedatasecuredFillJSExamPassed','ExamPassed',jQuery('#QualificationLevel').val());
         
         
  });
function FillJSExamPassed(funct,control,qual_level) {
    var path =  serverpath + "secured/qualification/0/0/"+qual_level+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJSExamPassed(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJSExamPassed('parsedatasecuredFillJSExamPassed','ExamPassed',jQuery('#QualificationLevel').val());
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#"+control).empty();
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}

jQuery('#ExamPassed').on('change', function () {
  //  FillJSCourse(jQuery('#ExamPassed').val());
  FillJSSubjectGroup('parsedatasecuredFillJSSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
       
       
});


function FillJSSubjectGroup(funct,control,EducationId) {
    var path =  serverpath + "secured/subjectgroup/0/'" + EducationId + "'/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJSSubjectGroup(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJSSubjectGroup('parsedatasecuredFillJSSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
             }
        }
    }
    function fetchNoofJsQualificationWise(){
    
        var path =  serverpath + "ExchJSSearch/'"+$('#noofqualificationwiseexchange').val()+"'/'"+$("#QualificationLevel").val()+"'/'"+$("#ExamPassed").val()+"'/'"+$("#SubjectGroup").val()+"'/3"
        ajaxget(path,'parsedataNoofJsQualificationWise','comment');
    }
    function parsedataNoofJsQualificationWise(data){  
        data = JSON.parse(data)
       var appenddata="";
        var data1 = data[0];    
              
                    var Categorytype="";
                
                    var gendertype="";
                   
                  $("#tableQualification").show()
                    
                        gendertype= "<th colspan=1>Gender Wise</th><td colspan=1 style='text-align: center;'>"+data[0][0].MenCount+"</td><td colspan=3 style='text-align: center;'>"+data[0][0].WomenCount+"</td><td rowspan=3 style='text-align: center;'>"+data[0][0].TotalCount+"</td>"
                   Categorytype= "	<th ></th><td style='text-align: center;'>"+data[0][0].URCount+"</td><td style='text-align: center;'>"+data[0][0].SCCount+"</td><td style='text-align: center;'>"+data[0][0].STCount+"</td><td style='text-align: center;'>"+data[0][0].OBCCount+"</td>";
                   
                    jQuery("#genderwisedetails").html(gendertype); 
                    jQuery("#categorywiseDetail").html(Categorytype); 
                    $("#headingtext").html(sessionStorage.Ex_name+" </br>No of Js Qualification wise  ")
                    var head='<th></th><th colspan=2>'+sessionStorage.Ex_name+' </br>Report Name : No of Js Qualification wise  <br> Location :'+$('#noofqualificationwiseexchange option:selected').html()+',<br>QualificationLevel:'+$('#QualificationLevel option:selected').html()+',<br>Qualification :'+$('#ExamPassed option:selected').html()+',<br>Subject Group :'+$('#SubjectGroup option:selected').html()+'</th>';
                    $('#Detailhead').html(head)
                            
                  
    }
    