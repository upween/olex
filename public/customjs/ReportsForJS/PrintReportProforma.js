jQuery('#textReportName').on('change', function () {
    $("#Es11").hide(); 
    $("#narrativemonthlyReport").hide()
    $("#es14reporttable").hide();
    $("#es23reporttable").hide();
    $("#es24reporttable").hide();
    $("#es24reporttable2").hide();
    $("#es25p1").hide();
    $("#es25rptpart2table").hide();
    $("#qulirpttable").hide();
    $("#categoryrpyt").hide();
    $("#es12rpttable").hide();
    $("#es21rpttable").hide();
    $("#es13rpttable").hide();
    $("#es11rpttable").hide();
    $("#JobfairYearWise").hide()
    
    $("#jfcrpttable").hide()
    $("#CategoryWiseSummaryReport").hide()
    $("#SubjectGroupWiseSummaryReport").hide()
    $("#Bolr12").hide()
    $("#modifyrpttable").hide()
    $("#AgeWiseQualification").hide()
    $("#CategoryWiseQualification").hide()
    $("#x63table").hide()

   

    FillDDlExchOffice(jQuery('#textReportName').val());
    if(jQuery('#textReportName').val()=='101' || jQuery('#textReportName').val()=='114' || jQuery('#textReportName').val()=='115'  || jQuery('#textReportName').val()=='111' || jQuery('#textReportName').val()=='112'){
        $('#toLabel').hide()
      //  $('#printproformatomonth').show()
        $('#frommonth').show()
        $("#frmYear").show()
        $("#frmLabel").html("Month & Year");
        $('#toYear').hide()
       // $('#typemonth').show();
       
        $('#typemonth,#typemonthLabel').hide();
        $('#frmLabel').show()
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
        $('#ddlexampassed').hide()

        $('#PRP').hide()
        $('#PRP1').hide()
        $('#PRP2').hide()

        $('#118PRP').hide()
        $('#118PRP1').hide()
        $('#118PRP2').hide()
        $('#118PRP3').hide()
        $('#118PRP4').hide()
        $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='102'  || jQuery('#textReportName').val()=='109' || jQuery('#textReportName').val()=='110' ||  jQuery('#textReportName').val()=='122'){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").html("Year");
        $("#frmLabel").show();
        $("#frmYear").show()
        $('#toYear').hide()
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
        $('#ddlexampassed').hide()
        $('#toLabel').hide()
        $('#PRP').hide()
        $('#PRP1').hide()
        $('#PRP2').hide()

        $('#118PRP').hide()
        $('#118PRP1').hide()
        $('#118PRP2').hide()
        $('#118PRP3').hide()
        $('#118PRP4').hide()
        $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='103' || jQuery('#textReportName').val()=='104'||jQuery('#textReportName').val()=='105' ||jQuery('#textReportName').val()=='106' || jQuery('#textReportName').val()=='107'  || jQuery('#textReportName').val()=='108'  ){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').show();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
        $('#ddlexampassed').hide()
        $('#PRP').hide()
        $('#PRP1').hide()
        $('#PRP2').hide()
       
        $('#118PRP').hide()
        $('#118PRP1').hide()
        $('#118PRP2').hide()
        $('#118PRP3').hide()
        $('#118PRP4').hide()
        $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='134' ||jQuery('#textReportName').val()=='123' ||jQuery('#textReportName').val()=='130' || jQuery('#textReportName').val()=='131' ){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").show()
        $("#frmdateLabel").show()
        $("#todateLabel").show()
        $("#todate").show()
        $("#frmYear").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
        $('#ddlexampassed').hide()
        $('#PRP').hide()
        $('#PRP1').hide()
        $('#PRP2').hide()
       
        $('#118PRP').hide()
        $('#118PRP1').hide()
        $('#118PRP2').hide()
        $('#118PRP3').hide()
        $('#118PRP4').hide()
        $('#118PRP5').hide()
    }
    // if(jQuery('#textReportName').val()=='119'  ){
    //     $('#toLabel').hide()
    //     $('#printproformatomonth').hide()
    //     $('#frommonth').hide()
    //     $("#frmLabel").hide();
    //     $('#toYear').hide();
    //     $('#typemonth,#typemonthLabel').hide();
    //     $("#fromdate").hide()
    //     $("#frmdateLabel").hide()
    //     $("#todateLabel").hide()
    //     $("#todate").hide()
    //     $("#frmYear").hide()
    //     $("#asonLabel").hide()
    //     $("#asondate").hide()
    //    $('#subjectGroupLabel').hide()
    //    $('#exampassed').hide()
    //     $('#ddlexampassed').hide()
    //     $('#PRP').hide()
    //     $('#PRP1').hide()
    //     $('#PRP2').hide()
        
    //     $('#118PRP').hide()
    //    $('#118PRP1').hide()
    //    $('#118PRP2').hide()
    //    $('#118PRP3').hide()
    //    $('#118PRP4').hide()
    //    $('#118PRP5').hide()
    // }
    if( jQuery('#textReportName').val()=='124' || jQuery('#textReportName').val()=='126' || jQuery('#textReportName').val()=='127' ){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#frmYear").hide()
        $("#asonLabel").show()
        $("#asondate").show()
      $('#subjectGroupLabel').hide()
      $('#exampassed').hide()
       $('#ddlexampassed').hide()
       $('#PRP').hide()
       $('#PRP1').hide()
       $('#PRP2').hide()

       $('#118PRP').hide()
       $('#118PRP1').hide()
       $('#118PRP2').hide()
       $('#118PRP3').hide()
       $('#118PRP4').hide()
       $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='125'  ){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#frmYear").hide()
        $("#asonLabel").show()
        $("#asondate").show()
       $('#subjectGroupLabel').show()
       $('#exampassed').show()
       $('#ddlexampassed').show()
       $('#PRP').hide()
       $('#PRP1').hide()
       $('#PRP2').hide()

       $('#118PRP').hide()
       $('#118PRP1').hide()
       $('#118PRP2').hide()
       $('#118PRP3').hide()
       $('#118PRP4').hide()
       $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='113'  ){
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()        
        $("#frmLabel").show();
        $("#frmLabel").html('Year');
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#frmYear").show()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
       $('#ddlexampassed').hide()
       $('#PRP').hide()
       $('#PRP1').hide()
       $('#PRP2').hide()

       $('#118PRP').hide()
       $('#118PRP1').hide()
       $('#118PRP2').hide()
       $('#118PRP3').hide()
       $('#118PRP4').hide()
       $('#118PRP5').hide()
    }

    if(jQuery('#textReportName').val()=='128' || jQuery('#textReportName').val()=='129'  ){
        FillYear('parsedatasecuredFillYear','prpfromyear');
        FillYear('parsedatasecuredFillYear','prptoyear'); 

        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $("#frmLabel").html('Year');
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#frmYear").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
       $('#ddlexampassed').hide()
       $('#PRP').show()
       $('#PRP1').show()
       $('#PRP2').show()

       $('#118PRP').hide()
       $('#118PRP1').hide()
       $('#118PRP2').hide()
       $('#118PRP3').hide()
       $('#118PRP4').hide()
       $('#118PRP5').hide()
    }
    if(jQuery('#textReportName').val()=='118' || jQuery('#textReportName').val()=='119' ){
        FillMonthPrintProforma('parsedatasecuredFillMonthPrintProforma','prpfrommonth');
        FillMonthPrintProforma('parsedatasecuredFillMonthPrintProforma','prptomonth');
        FillYear('parsedatasecuredFillYear','118prpfromyear');
        FillYear('parsedatasecuredFillYear','118prptoyear');
        $('#toLabel').hide()
        $('#printproformatomonth').hide()
        $('#frommonth').hide()
        $("#frmLabel").hide();
        $("#frmLabel").html('Year');
        $('#toYear').hide();
        $('#typemonth,#typemonthLabel').hide();
        $("#fromdate").hide()
        $("#frmdateLabel").hide()
        $("#todateLabel").hide()
        $("#todate").hide()
        $("#frmYear").hide()
        $("#asonLabel").hide()
        $("#asondate").hide()
       $('#subjectGroupLabel').hide()
       $('#exampassed').hide()
       $('#ddlexampassed').hide()
       $('#PRP').hide()
       $('#PRP1').hide()
       $('#PRP2').hide()


       $('#118PRP').show()
       $('#118PRP1').show()
       $('#118PRP2').show()
       $('#118PRP3').show()
       $('#118PRP4').show()
       $('#118PRP5').show()
    }
    
    
      });
      function FillDDlExchOffice(ReportID){
        
        var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
       securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', 'control')
          }
  function parsedatasecuredFillDDlExchOffice(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDDlExchOffice();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
                jQuery("#printreportproformaexchange").empty();
                if(jQuery('#textReportName').val()=='123' || jQuery('#textReportName').val()=='126'){
                    jQuery("#printreportproformaexchange").append(jQuery("<option></option>").val('0').html("All"));
    
                 }
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#printreportproformaexchange").append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                 }
            }  
                  
    }

    function FillDDlReportName(){
        
        var path = serverpath + "secured/GetRptName/0/'JS'/1";
       securedajaxget(path, 'parsedatasecuredGetRptName', 'comment', 'control')
          }
    function parsedatasecuredGetRptName(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDDlReportName();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
             
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#textReportName").append(jQuery("<option></option>").val(data1[i].RptID).html(data1[i].RptNameEng));
                 }
            }  
                  
    }

    function fetchReport(){
        // if(jQuery('#textReportName').val()=='128'){
        //     var path =  serverpath + "CounsellingByMonthWise/'"+$('#frmYear').val()+"'/'"+$('#toYear').val()+"'/'"+$('#printreportproformaexchange').val()+"'"
        //     ajaxget(path,'parsedatafetchReport','comment',"control");
   
        // }
        if(jQuery('#textReportName').val()=='101'){
            FetchES11Rpt() 
        }
     }
    function parsedatafetchReport(data){  
        data = JSON.parse(data)
       var appenddata="";
        var data1 = data[0]; 
     
                   
        for (var i = 0; i < data1.length; i++) {
        //     if(jQuery('#textReportName').val()=='128'){	
        //  appenddata += "<tr><td style='    word-break: break-word;'  >" +data1[i].Ex_NameH+ "</td> <td style='    word-break: break-word;'>" + data1[i].XMonthYear+ "</td><td style='    word-break: break-word;'>" + data1[i].Total_Counseling+ "</td><td>"+data1[i].Total_Candidates+"</td><td>"+data1[i].UR_Candidates+"</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td></tr>";
        //  }
        //  if(jQuery('#textReportName').val()=='101'){	
        //     appenddata += "<tr><td>1</td><td style='    word-break: break-word;'  >Applicants on the Live Register at the end of the Previous Month</td> <td style='    word-break: break-word;'>" + data1[i].Prev_LR_M+ "</td><td style='    word-break: break-word;'>" + data1[i].Prev_LR_F+ "</td><td>"+data1[i].Total_PrevLR+"</td></tr>"+
        //     "<tr><td>1a</td><td style='    word-break: break-word;'  >No. of Registration cards recieved on Transfer</td> <td style='    word-break: break-word;'>" + data1[i].TransferRegistration_M+ "</td><td style='    word-break: break-word;'>" + data1[i].TransferRegistration_F+ "</td><td>"+data1[i].TransferRegistration+"</td></tr>"+
        //     "<tr><td>2</td><td style='    word-break: break-word;'  >No. of Fresh registration made during the month</td> <td style='    word-break: break-word;'>" + data1[i].FreshRegistration_M+ "</td><td style='    word-break: break-word;'>" + data1[i].FreshRegistration_F+ "</td><td>"+data1[i].FreshRegistration+"</td></tr>"+
        //     "<tr><td>2a</td><td style='    word-break: break-word;'  >No. of Re-registration made during the month</td> <td style='    word-break: break-word;'>" + data1[i].ReRegistration_M+ "</td><td style='    word-break: break-word;'>" + data1[i].ReRegistration_F+ "</td><td>"+data1[i].ReRegistration+"</td></tr>"+
        //     "<tr><td>3</td><td style='    word-break: break-word;'  >Total: Items 1, 1a, 2 and 2a</td> <td style='    word-break: break-word;'>" + data1[i].Total_3_M + "</td><td style='    word-break: break-word;'>" + data1[i].Total_3_F+ "</td><td>"+data1[i].Total_3+"</td></tr>"+
        //     "<tr><td>4</td><td style='    word-break: break-word;'  >No. of Job-seekers placed during the Month</td> <td style='    word-break: break-word;'>0</td><td style='    word-break: break-word;'>0</td><td>0</td></tr>"+
        //     "<tr><td>5</td><td style='    word-break: break-word;'  >Registration Card removed from Live Register for reason other than Transfer to other exchanges</td> <td style='    word-break: break-word;'>" + data1[i].RemovedRegistration_M+ "</td><td style='    word-break: break-word;'>" + data1[i].RemovedRegistration_F+ "</td><td>"+data1[i].RemovedRegistration+"</td></tr>"+
        //     "<tr><td>6</td><td style='    word-break: break-word;'  >No. of Registration cards transferred during the month to other exchanges</td><td style='    word-break: break-word;'>" + data1[i].TransferRegistration_M+ "</td><td style='    word-break: break-word;'>" + data1[i].TransferRegistration_F+ "</td><td>"+data1[i].TransferRegistration+"</td></tr>"+
        //     "<tr><td>7</td><td style='    word-break: break-word;'  >Total: Items 4, 5 and 6</td><td style='    word-break: break-word;'>" + data1[i].Total_7_M+ "</td><td style='    word-break: break-word;'>" + data1[i].Total_7_F+ "</td><td>"+data1[i].Total_7+"</td></tr>"+
        //     "<tr><td>8</td><td style='    word-break: break-word;'  > Applicants remaining on the Live Register at the end of the Month</td> <td style='    word-break: break-word;'>" + data1[i].LR_M+ "</td><td style='    word-break: break-word;'>" + data1[i].LR_F+ "</td><td>"+data1[i].LR+"</td></tr>"+
        //     "<tr><td>8a</td><td style='    word-break: break-word;'  >Submissions made during the Month</td> <td style='    word-break: break-word;'>" + data1[i].Rural_LR_M+ "</td><td style='    word-break: break-word;'>" + data1[i].Rural_LR_F+ "</td><td>"+data1[i].Rural_LR+"</td></tr>"+
        //     "<tr><td>9</td><td style='    word-break: break-word;'  >No. of Registration cards transferred during the month to other exchanges</td> <td style='    word-break: break-word;'>" + data1[i].Submissions_M+ "</td><td style='    word-break: break-word;'>" + data1[i].Submissions_F+ "</td><td>"+data1[i].TotalSubmission+"</td></tr>"+
        //     "<tr><td>10</td><td style='    word-break: break-word;'  >Total Vacancies notified during the Month</td> <td style='    word-break: break-word;'>" + data1[i].NoOfNotify_M+ "</td><td style='    word-break: break-word;'>" + data1[i].NoOfNotify_F+ "</td><td>"+data1[i].TotalNotify+"</td></tr>";
        // }
         }jQuery("#tbodyvalue").html(appenddata);  
        
                   
    }
    function FillMonthPrintProforma(funct,control) {
        var path =  serverpath + "secured/month/0"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillMonthPrintProforma(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillMonthPrintProforma('parsedatasecuredFillMonthPrintProforma',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
                 }
    
                
            }
                  
    }
//  function TableHeader(){
     
//         if ($("#textReportName" ).val() == "101") {
//             $("#changeheader").text("E.S.1.1 (Monthly Report)")
//             var tablehead="<tr><th colspan=1>S.No.</th><th colspan=1>Item</th><th>Men</th><th colspan=1>Women</th><th colspan=1>Total</th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "112") {
//                 var tablehead="<tr><th>Sno</th><th colspan=3>Division</th><th colspan=3>Employment Exchange</th><th colspan=3>Registration</th><th colspan=3>Placement</th><th colspan=3>Submission</th><th colspan=3>Vacancy Notification</th><th colspan=3>Live Registration</th><th colspan=3>Transfer Registration</th></tr>"+
//                 "<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th><th>M</th><th>F</th><th>T</th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "102") {
//                 $("#changeheader").text("E.S.1.4 Report")
//                 var tablehead="<tr><th>Sno</th><th colspan=3>Educational Level</th><th colspan=7 style='text-align:center'>No. on Live Register as on 31st December   2019in the age group</th></tr>"+
//             	"<tr><th></th><th></th><th></th><th></th><th>Up to 19</th><th>20-29</th><th>30-39</th><th>40-49</th><th>50-59</th><th>60 & above</th><th>Total</th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "103") {
//                 $("#changeheader").text("E.S.2.3 (Half Yearly Report)")
//                 var tablehead="<tr><th colspan=1>S.No.</th><th colspan=1>Item</th><th>Muslim</th><th colspan=1>Christian</th><th colspan=1>Sikhs</th><th colspan=1>Budhists</th><th colspan=1>Zoarastria</th><th colspan=1>Total</th></tr>";
//                }

//         else if ($("#textReportName" ).val() == "104") {
//                 $("#changeheader").text("E.S.2.4 (Half Yearly Report) - Part I")
//                 var tablehead="<tr><th colspan=1>S.No.</th><th colspan=1>Item</th><th>SC</th><th colspan=1>ST</th><th colspan=1>OBC</th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "105") {
//                 $("#changeheader").text("E.S.2.4 (Half Yearly Report) - Part II")
//                 var tablehead="<tr><th>Sno</th><th colspan=1>Type of Establishment</th><th colspan=5>Outstanding at the end of the previous half year</th><th colspan=5>Notified during the half year</th><th colspan=5>Filled during the half year</th><th colspan=8>Cancelled during the year due to</th><th colspan=8>Outstanding at the end of the half year</th></tr>"+
//             	"<tr><th></th><th></th><th colspan=3></th><th colspan=3></th><th colspan=3></th><th colspan=3></th><th colspan=3></th><th colspan=3>Non-availability of suitable candidates</th><th colspan=3>Other reasons</th><th colspan=5></th></tr>"+
//                 "<tr><th></th><th></th><th>SC</th><th>ST</th><th>OBC</th><th></th><th></th><th>SC</th><th>ST</th><th>OBC</th><th></th><th></th><th>SC</th><th>ST</th><th>OBC</th><th></th><th></th><th>SC</th><th>ST</th><th>OBC</th><th>SC</th><th>ST</th><th>OBC</th><th></th><th></th><th>SC</th><th>ST</th><th>OBC</th></tr>";    
//         }
//         else if ($("#textReportName" ).val() == "106") {
//                 $("#changeheader").text("E.S.2.5 (Half Yearly Report) -  Part I")
//                 var tablehead="<tr><th colspan=1>S.No.</th><th colspan=1>Item</th><th>Blind</th><th colspan=1>Deaf & Dumb</th><th colspan=1>Orthopa-edics</th><th>Respirat-ory Disorder</th><th>Nag Leprosy Persons</th><th>Total (Col. 3-7)</th><th>Woman (Included in total)</th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "107") {
//                 $("#changeheader").text("E.S.2.5 (Half Yearly Report) -  Part II")
//                 var tablehead="<tr><th>Sno</th><th colspan=3>Item </th><th colspan=3>Central Govt.</th><th colspan=3>Union Territory</th><th colspan=3>State Govt.</th><th colspan=5>Quasi-Govt. Estt./Public Sector undertakings </th><th colspan=3>Local Bodies</th><th colspan=3>Private Sector</th><th colspan=3>Total</th></tr>"+
//                 "<tr><th></th><th colspan=5></th><th colspan=5></th><th colspan=5></th><th colspan=2>Central Govt.</th><th colspan=2>State Govt.</th><th colspan=2></th><th colspan=2></th><th colspan=2></th></tr>";
//         }
//         else if ($("#textReportName" ).val() == "108") {
//             $("#changeheader").text("E.S.2.1 (Half Yearly Report)")
//             var tablehead="<tr><th>Sno</th><th colspan=1>Education Level</th><th colspan=3>Total educated(All category)</th><th colspan=3>Woman (Included in total)</th><th colspan=3>S.C.(Included in total)</th><th colspan=3>S.T.(Included in total)</th><th colspan=3>OBC(Included in total)</th></tr>"+
//             "<tr><th></th><th></th><th>Regd</th><th>Placement</th><th>L.R.</th><th>Regd</th><th>Placement</th><th>L.R.</th><th>Regd</th><th>Placement</th><th>L.R.</th><th>Regd</th><th>Placement</th><th>L.R.</th><th>Regd</th><th>Placement</th><th>L.R.</th></tr>"+
//             "<tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th></tr>";    
//         }
//         else if ($("#textReportName" ).val() == "109") {
//             $("#changeheader").text("E.S.1.2 (Annual Report)")
//             var tablehead="<tr style='text-align:center'><th>Sno</th><th colspan=1>NCO(six digit level+Total at 4 digit level)</th><th colspan=6>No. of applicant on Live Register at the end of the year in respect of</th><th colspan=24>Number of Vacancies</th></tr>"+
//             "<tr><th></th><th></th><th colspan=6></th><th colspan=6>Notified during the year in respect of</th><th colspan=6>Filled during the year in respect of</th><th colspan=6>Cancelled during the year in respect of</th><th colspan=6>Outstanding at the end of the year in respect of</th></tr>"+
//             "<tr><th></th><th></th><th>Total</th><th>Woman</th><th>SC</th><th>ST</th><th>OBC</th><th>Disabled  Persons</th><th>Total</th><th>Woman</th><th>SC</th><th>ST</th><th>OBC</th><th>Disabled  Persons</th><th>Total</th><th>Woman</th><th>SC</th><th>ST</th><th>OBC</th><th>Disabled  Persons</th><th>Total</th><th>Woman</th><th>SC</th><th>ST</th><th>OBC</th><th>Disabled  Persons</th><th>Total</th><th>Woman</th><th>SC</th><th>ST</th><th>OBC</th><th>Disabled  Persons</th></tr>"+
//             "<tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th><th>32</th></tr>";    
//         } 
//         else if ($("#textReportName" ).val() == "110") {
//             $("#changeheader").text("E.S.1.3 (Annual)")
//             var tablehead="<tr style='text-align:center'><th>Sno</th><th colspan=1>Item</th><th>Central Govt.</th><th>UT</th><th>State Govt.</th><th colspan=2>Quasi-Govt. Estt./Public Sector undertakings</th><th>Local Bodies</th><th colspan=2>Private Sector</th><th>Total</th></tr>"+
//             "<tr><th></th><th></th><th></th><th></th><th></th><th>Central Govt.</th><th>State Govt.</th><th></th><th>Falling within the preview of Act</th><th>Not falling within the preview of Act</th><th></th></tr>"+
//             "<tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th></tr>";    
//         }   
        
//         $("#tablehead").html(tablehead);    
//         jQuery("#tbodyvalue").html(appenddata);
       
//     }
    function TableType(){
      var rptid=$("#textReportName" ).val();
        jQuery("#tbodyvalue").html(""); 
        $("#Es11").show(); 
            if ($("#textReportName" ).val() == "101") {
              //  $("#changeheader").text("E.S.1.1 (Monthly Report)")
              $("#headingtext").html(sessionStorage.Ex_name+" </br>  "+$('#textReportName option:selected').html()+"" )
                var head="<tr><th></th><th colspan=4> "+sessionStorage.Ex_name+" </br>  Exchange: "+$('#printreportproformaexchange option:selected').html()+" <br> Report Name: ES11 (Monthly Report) <br> Month & Year: "+$('#printproformafrommonth option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                var tablehead="<tr><th colspan=1 style='border-right: inset;'>S.No.</th><th colspan=1 style='border-right: inset;'>Item</th><th style='border-right: inset;'>Men</th><th colspan=1 style='border-right: inset;'>Women</th><th colspan=1 style='border-right: inset;'>Total</th></tr>";
                fetchReport();
                $("#tablehead").html(tablehead);  
                $("#Detailhead").html(head);
            }
            
            if(rptid=='102'){
                // $('#changeheader').text('E.S.1.4 (Report)');
                $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html() )

                $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.1.4 Reports ('+sessionStorage.Exname_es11+' )');
               var head="<tr><th></th><th colspan=9> Exchange: "+$('#printreportproformaexchange option:selected').html()+" </br> Report Name: ES14 (Report) </br>Year: "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);
                FillEduationalLevel('parsedataFillEduationalLevel','tbodyvalue');
            }
            if(rptid=='103'){
              //  $('#changeheader').text('E.S.2.3(Half Yealy)');
              $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+"" )

                $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchange in respect of Minority Communities during the half year ended:  ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.2.3 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=7>"+sessionStorage.Ex_name+" </br> Exchange: "+$('#printreportproformaexchange option:selected').html()+" </br> Report Name: E.S.2.3(Half Yealy) </br> Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

                var tablehead =`	<tr><th  style='background: #716aca;border-right: inset;'>Sr. No</th>
                <th  style='background: #716aca;border-right: inset;'>Item</th>
                <th  style='background: #716aca;border-right: inset;'>Musilm</th>
                <th  style='background: #716aca;border-right: inset;'>Christian</th>
                <th  style='background: #716aca;border-right: inset;'>Sikhs</th>
                <th  style='background: #716aca;border-right: inset;'>Budhists</th>
                <th  style='background: #716aca;border-right: inset;'>Zoarastrians</th>
                <th  style='background: #716aca;'>Total</th></tr>`;
                $('#tablehead').html(tablehead);
                var tbodyvalue=`<tr>
                <td>1</td>
                <td>	No. on the Live Register at the end of the previous half Year</td>
            
            
                <td id="LR_Prev_M" >
                </td>
                <td  id="LR_Prev_C" >
                </td>
                <td id="LR_Prev_S" >
                </td>
                <td  id="LR_Prev_B" >
                </td>
                <td  id="LR_Prev_Z" >
                </td>
                <td  id="LR_Prev_Total" >
                </td>
            
            </tr>
            <tr>
                    <td>2</td>
                    <td>		No. of registration/re-registration Effected</td>
                        
                <td id="Registration_M" >
                </td>
                <td  id="Registration_C" >
                </td>
                <td id="Registration_S" >
                </td>
                <td  id="Registration_B" >
                </td>
                <td  id="Registration_Z" >
                </td>
                <td  id="Registration_Total" >
                </td>
            
                </tr>
                <tr>
                        <td>3</td>
                        <td>	No. Placed during the Half year	</td>
                        
                        <td id="PlacesJs_M" >
                        </td>
                        <td  id="PlacesJs_C" >
                        </td>
                        <td id="PlacesJs_S" >
                        </td>
                        <td  id="PlacesJs_B" >
                        </td>
                        <td  id="PlacesJs_Z" >
                        </td>
                        <td  id="PlacesJs_Total" >
                        </td>
                    </tr>
                    <tr>
                            <td>4</td>
                            <td>No. removed from the Live register</td>
                        
                            
                            <td id="Registration_Removed_M" >
                            </td>
                            <td  id="Registration_Removed_C" >
                            </td>
                            <td id="Registration_Removed_S" >
                            </td>
                            <td  id="Registration_Removed_B" >
                            </td>
                            <td  id="Registration_Removed_Z" >
                            </td>
                            <td  id="Registration_Removed_Total" >
                            </td>
                            
                        
                        </tr>
                        
                    <tr>
                            <td>5</td>
                            <td>No. on the Live register at end of the half Year	</td>
                        
                            <td id="LR_M" >
                            </td>
                            <td  id="LR_C" >
                            </td>
                            <td id="LR_S" >
                            </td>
                            <td  id="LR_B" >
                            </td>
                            <td  id="LR_Z" >
                            </td>
                            <td  id="LR_Total" >
                            </td>
                        
                        </tr>
                        <tr>
                                <td>6</td>
                                <td>No. of Submission made	</td>
                                
                                <td id="Submissions_M" >
                                </td>
                                <td  id="Submissions_C" >
                                </td>
                                <td id="Submissions_S" >
                                </td>
                                <td  id="Submissions_B" >
                                </td>
                                <td  id="Submissions_Z" >
                                </td>
                                <td  id="Submissions_Total" >
                                </td>
                                
                            </tr>`;
                $('#tbodyvalue').html(tbodyvalue);
                ES23RptEntryForm();
            }
            
            if(rptid=='104'){
              //  $('#changeheader').text('E.S.2.4 Part I(Half Yearly)');
              $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+"" )

                $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchnages in respect of Scheduled Caste/Tribe and Other Backward Classes(OBC) applicats during Half Year-ended___ ${$('#criteriaddl').val()=='1'?'31/06/':'31/12/'}   ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.2.4 Part I Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=4> "+sessionStorage.Ex_name+" </br>Exchange: "+$('#printreportproformaexchange option:selected').html()+" </br> Report Name: E.S.2.4 Part I(Half Yearly)  </br> Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);
               
                var tbodyvalue=`															<tr>
                <td>1</td>
                <td>No.of applicants on the Live Register at the end of the previous half year</td>
                <td id='LR_Prev_1_SC' ></td>
                <td id='LR_Prev_1_ST' ></td>
                <td id='LR_Prev_1_OBC' ></td>
            </tr>
            <tr>
                <td>2</td>
                <td>No.of applicants registered/re-registered during the half Year</td>
                <td id='Registration_SC' ></td>
                <td id='Registration_ST' ></td>
                <td id='Registration_OBC' ></td>
            </tr>
            <tr>
                <td>3</td>
                <td>	No. of applicants placed during the half year in:</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>	a) Central Govt.</td>
                <td id='PlacesJs_CG_SC' ></td>
                <td id='PlacesJs_CG_ST' ></td>
                <td id='PlacesJs_CG_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>b) Union Territory</td>
                <td id='PlacesJs_UT_SC' ></td>
                <td id='PlacesJs_UT_ST' ></td>
                <td id='PlacesJs_UT_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>c) State Govt</td>
                <td id='PlacesJs_SG_SC' ></td>
                <td id='PlacesJs_SG_ST' ></td>
                <td id='PlacesJs_SG_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>d) Quasi-Govt. Estts/Public Sector undertakings under <br>i)  Center Govt.<br>ii) State Govt.</td>
                <td id='PlacesJs_QG_SC' ></td>
                <td id='PlacesJs_QG_ST' ></td>
                <td id='PlacesJs_QG_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>e) Local Bodies</td>
                <td id='PlacesJs_LB_SC' ></td>
                <td id='PlacesJs_LB_ST' ></td>
                <td id='PlacesJs_LB_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>f) Private Establishments</td>
                <td id='PlacesJs_Private_SC' ></td>
                <td id='PlacesJs_Private_ST' ></td>
                <td id='PlacesJs_Private_OBC' ></td>
            </tr>
            <tr>
                <td></td>
                <td>Total of Items 3 (a) to 3 (f)</td>
                <td id='PlacesJs_Total_SC' ></td>
                <td id='PlacesJs_Total_ST' ></td>
                <td id='PlacesJs_Total_OBC' ></td>
            
            </tr>
            <tr>
                <td>4</td>
                <td>	No.of applicants removed from the Live Register at the end of the half year.</td>
                <td id='Registration_Removed_SC' ></td>
                <td id='Registration_Removed_ST' ></td>
                <td id='Registration_Removed_OBC' ></td>
            </tr>
            <tr>
                <td>5</td>
                <td>No. of applicants remaining on the Live Register at the end of the half year. </td>
                <td id='LR_SC' ></td>
                <td id='LR_ST' ></td>
                <td id='LR_OBC' ></td>
            </tr>
            <tr>
                <td>6</td>
                <td>No. of Submissions made during the half year.</td>
                <td id='Submissions_SC' ></td>
                <td id='Submissions_ST' ></td>
                <td id='Submissions_OBC' ></td>
            </tr>`;
            $('#tbodyvalue').html(tbodyvalue);
            var tablehead=`	<tr><th  style='background: #716aca;border-right: inset;'>Sr. No</th>
            <th  style='background: #716aca;border-right: inset;'>Item</th>
            <th  style='background: #716aca;border-right: inset;'>SC</th>
            <th  style='background: #716aca;border-right: inset;'>ST</th>
            <th  style='background: #716aca;'>OBC</th></tr>
            `;
            $('#tablehead').html(tablehead);
            ES24Part1RptEntryForm();
            }
            if(rptid=='105'){
              //  $('#changeheader').text('E.S.2.4 Part II(Half Yearly)');
              $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+" " )
  
              $('.exchangenametext').text(sessionStorage.Exname_es11);
              
                $('#exnameess1rpt').text('View  E.S.2.4 Part II Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=20>"+sessionStorage.Ex_name+" </br> Exchange: "+$('#printreportproformaexchange option:selected').html()+" </br> Report Name: E.S.2.4 Part II(Half Yearly) </br> Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

            var tablehead=`<tr>
            <th  style='background: #716aca;border-right: inset;' colspan=1>Sno</th>
            <th  style='background: #716aca;border-right: inset;' colspan=1>Type of Establishment</th>
            <th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>Outstanding at the end of the previous half year</th>
            <th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>Notified during the half year</th>
            <th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>Filled during the half year</th>
            <th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=6>Cancelled during the year due to</th>
            <th  style='background: #716aca;border-bottom: inset;' colspan=3>Outstanding at the end of the half year</th>
            </tr>
            <tr>
                <th  style='background: #716aca;border-right: inset;' colspan=1>
                </th>
                <th  style='background: #716aca;border-right: inset;' colspan=1>
                </th>
                <th  style='background: #716aca;;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;border-bottom: inset;'>
                </th>
                <th  style='background: #716aca;border-bottom: inset;' >Non-availability of suitable candidates</th>
                <th  style='background: #716aca;border-right: inset;border-bottom: inset;'>
                </th>
                <th  style='background: #716aca;border-bottom: inset;'>
                </th>
                <th  style='background: #716aca;border-bottom: inset;' >Other reasons</th>
                <th  style='background: #716aca;border-right: inset;border-bottom: inset;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
                <th  style='background: #716aca;'>
                </th>
            </tr>
            <tr>
            
            <th  style='background: #716aca;border-right: inset;'></th>
            <th  style='background: #716aca;border-right: inset;'></th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;border-right: inset;'>OBC</th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;border-right: inset;'>OBC</th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;border-right: inset;'>OBC</th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;border-right: inset;'>OBC</th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;border-right: inset;'>OBC</th>
            <th  style='background: #716aca;'>SC</th>
            <th  style='background: #716aca;'>ST</th>
            <th  style='background: #716aca;'>OBC</th>
            </tr>
            <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
            <td>13</td>
            <td>14</td>
            <td>15</td>
            <td>16</td>
            <td>17</td>
            <td>18</td>
            <td>19</td>
            <td>20</td>`;
            $('#tablehead').html(tablehead);
            fetchES24Part2();
            }
            
            if(rptid=='106'){
             //   $('#changeheader').text('E.S.2.5 Part I(Half Yearly)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html() )
  
             $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchnages in respect of All Disabled
                <br />
                applicants during the Half Year ended__ ${$('#criteriaddl').val()=='1'?'31/06/':'31/12/'}   ${$('#frmYear').val()}`)
                
                
                $('#exnameess1rpt').text('View  E.S.2.5 Part I Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=8>"+sessionStorage.Ex_name+" </br> Exchnage: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: E.S.2.5 Part I(Half Yearly) </br>  Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

            var tablehead=`<tr>
            <th  style='background: #716aca;border-right: inset;'>Sr. No</th>
            <th  style='background: #716aca;border-right: inset;'>Item</th>
            <th  style='background: #716aca;border-right: inset;'>Blind</th>
            <th  style='background: #716aca;border-right: inset;'>Deaf & Dumb</th>
            <th  style='background: #716aca;border-right: inset;'>Orthopaedics</th>
            <th  style='background: #716aca;border-right: inset;'>Respiratory Disorde</th>
            <th  style='background: #716aca;border-right: inset;'>Nag.Leprosy Persons</th>
            <th  style='background: #716aca;border-right: inset;'>Total(col.3-7)</th>
            <th  style='background: #716aca;'>Women(Included in total)</th>;
            </tr>`
            $('#tablehead').html(tablehead);
            var tbodyvalue=`<tr>
            <td>1</td>
            
            <td>No.of Disabled on the live register at the end of the Previous Half Year.</td>
            <td class="visibleControls"  id="LR_Prev_1_Blind" ></td>
            <td class="visibleControls" id="LR_Prev_1_Deaf"></td>
            <td class="visibleControls" id="LR_Prev_1_Orthopaedics"></td>
            <td class="visibleControls" id="LR_Prev_1_Respiratory"></td>
            <td class="visibleControls" id="LR_Prev_1_Leprosy"></td>
            <td class="visibleControls" id="LR_Prev_1_Total"></td>
            <td class="visibleControls" id="LR_Prev_1_W"></td>
            </tr>
            <tr>
            <td>2</td>
            <td>No.of Disabled registrations during the Half Year.</td>
            <td class="visibleControls" id="Registration_Blind" ></td>
            <td class="visibleControls" id="Registration_Deaf"></td>
            <td class="visibleControls" id="Registration_Orthopaedics"></td>
            <td class="visibleControls" id="Registration_Respiratory"></td>
            <td class="visibleControls" id="Registration_Leprosy"></td>
            <td class="visibleControls" id="Registration_Total"></td>
            <td class="visibleControls" id="Registration_W"></td>
            </tr>
            <tr>
            <td>3</td>
            <td>No. of disabled applicants placed during the half year in:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td>	a) Central Govt.</td>
            <td class="visibleControls" id="PlacesJs_CG_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_CG_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_CG_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_CG_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_CG_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_CG_Total"></td>
            <td class="visibleControls" id="PlacesJs_CG_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>b) Union Territory</td>
            <td class="visibleControls" id="PlacesJs_UT_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_UT_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_UT_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_UT_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_UT_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_UT_Total"></td>
            <td class="visibleControls" id="PlacesJs_UT_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>c) State Govt</td>
            <td class="visibleControls" id="PlacesJs_SG_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_SG_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_SG_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_SG_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_SG_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_SG_Total"></td>
            <td class="visibleControls" id="PlacesJs_SG_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>d) Quasi-Govt. Estts/Public Sector undertakings under <br>i)  Center Govt.<br>ii) State Govt.</td>
            <td class="visibleControls" id="PlacesJs_QG_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_QG_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_QG_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_QG_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_QG_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_QG_Total"></td>
            <td class="visibleControls" id="PlacesJs_QG_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>e) Local Bodies</td>
            <td class="visibleControls" id="PlacesJs_LB_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_LB_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_LB_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_LB_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_LB_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_LB_Total"></td>
            <td class="visibleControls" id="PlacesJs_LB_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>f) Private Establishments</td>
            <td class="visibleControls" id="PlacesJs_Private_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_Private_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_Private_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_Private_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_Private_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_Private_Total"></td>
            <td class="visibleControls" id="PlacesJs_Private_W"></td>
            </tr>
            <tr>
            <td></td>
            <td>Total of Items 3 (a) to 3 (f)</td>
            <td class="visibleControls" id="PlacesJs_Total_Blind" ></td>
            <td class="visibleControls" id="PlacesJs_Total_Deaf"></td>
            <td class="visibleControls" id="PlacesJs_Total_Orthopaedics"></td>
            <td class="visibleControls" id="PlacesJs_Total_Respiratory"></td>
            <td class="visibleControls" id="PlacesJs_Total_Leprosy"></td>
            <td class="visibleControls" id="PlacesJs_Total_Total"></td>
            <td class="visibleControls" id="PlacesJs_Total_W"></td>
            </tr>
            <tr>
            <td>4</td>
            <td>No.of Disabled applicants removed from the Live Register during the  half year.</td>
            <td class="visibleControls" id="Registration_Removed_Blind" ></td>
            <td class="visibleControls" id="Registration_Removed_Deaf"></td>
            <td class="visibleControls" id="Registration_Removed_Orthopaedics"></td>
            <td class="visibleControls" id="Registration_Removed_Respiratory"></td>
            <td class="visibleControls" id="Registration_Removed_Leprosy"></td>
            <td class="visibleControls" id="Registration_Removed_Total"></td>
            <td class="visibleControls" id="Registration_Removed_W"></td>
            </tr>
            <tr>
            <td>5</td>
            <td>No. of Disable applicants on the Live Register at the end of the half year.  </td>
            <td class="visibleControls" id="LR_Blind" ></td>
            <td class="visibleControls" id="LR_Deaf"></td>
            <td class="visibleControls" id="LR_Orthopaedics"></td>
            <td class="visibleControls" id="LR_Respiratory"></td>
            <td class="visibleControls" id="LR_Leprosy"></td>
            <td class="visibleControls" id="LR_Total"></td>
            <td class="visibleControls" id="LR_W"></td>
            </tr>
            <tr>
            <td>6</td>
            <td>No. of Submissions made during the half year.</td>
            <td class="visibleControls" id="Submissions_Blind" ></td>
            <td class="visibleControls" id="Submissions_Deaf"></td>
            <td class="visibleControls" id="Submissions_Orthopaedics"></td>
            <td class="visibleControls" id="Submissions_Respiratory"></td>
            <td class="visibleControls" id="Submissions_Leprosy"></td>
            <td class="visibleControls" id="Submissions_Total"></td>
            <td class="visibleControls" id="Submissions_W"></td>
            </tr>`;
            
            $('#tbodyvalue').html(tbodyvalue);
            ES25RptEntryForm();
            }
            
            if(rptid=='107'){
              //  $('#changeheader').text('E.S.2.5 Part II(Half Yearly)');
              $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+" " )
 
              $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('#exnameess1rpt').text('View  E.S.2.5 Part II Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=8>"+sessionStorage.Ex_name+" </br>Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: E.S.2.5 Part II(Half Yearly)  </br>Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

            var tablehead=`	<tr>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>S.no</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Item</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Central Govt.</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Union Territory</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>State Govt.</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan="2">Quasi-Govt. Estt./Public Sector undertaking</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset' >Local Bodies</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Private Estts.</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
            </tr>
            <tr>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Non-availability of suitable candidates</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Other reasons</th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
            </tr>`
            $('#tablehead').html(tablehead);
            var tbodyvalue=`<tr>
            <td>1</td>
            <td> No.of vacancies for Disabled outstanding at the end of the prievious half year. </td>
            <td id='Outstanding_Prev_CG' ></td>
            <td id='Outstanding_Prev_UT' ></td>
            <td id='Outstanding_Prev_SG' ></td>
            <td id='Outstanding_Prev_Quasi_CG' ></td>
            <td id='Outstanding_Prev_Quasi_SG' ></td>
            <td id='Outstanding_Prev_LB' ></td>
            <td id='Outstanding_Prev_Private' ></td>
            <td id='Outstanding_Prev_Total' ></td>
            </tr>
            <tr>
            <td>2</td>
            <td> No. of vacancies for Disabled applicants
                a)Notified during the half year</td>
            <td id='Notified_CG' ></td>
            <td id='Notified_UT' ></td>
            <td id='Notified_SG' ></td>
            <td id='Notified_Quasi_CG' ></td>
            <td id='Notified_Quasi_SG' ></td>
            <td id='Notified_LB' ></td>
            <td id='Notified_Private' ></td>
            <td id='Notified_Total' ></td>
            </tr>
            <tr>
            <td></td>
            <td> b) filled during the half year</td>
            <td id='Filled_CG' ></td>
            <td id='Filled_UT' ></td>
            <td id='Filled_SG' ></td>
            <td id='Filled_Quasi_CG' ></td>
            <td id='Filled_Quasi_SG' ></td>
            <td id='Filled_LB' ></td>
            <td id='Filled_Private' ></td>
            <td id='Filled_Total' ></td>
            </tr>
            <tr>
            <td></td>
            <td> c) Cancelled during the half year due to :
                i)Non-availability of suitable candidates.</td>
            <td id='Cancelled_NA_CG' ></td>
            <td id='Cancelled_NA_UT' ></td>
            <td id='Cancelled_NA_SG' ></td>
            <td id='Cancelled_NA_Quasi_CG' ></td>
            <td id='Cancelled_NA_Quasi_SG' ></td>
            <td id='Cancelled_NA_LB' ></td>
            <td id='Cancelled_NA_Private' ></td>
            <td id='Cancelled_NA_Total' ></td>
            </tr>
            <tr>
            <td></td>
            <td>	ii)Other reasons </td>
            <td id='Cancelled_Other_CG' ></td>
            <td id='Cancelled_Other_UT' ></td>
            <td id='Cancelled_Other_SG' ></td>
            <td id='Cancelled_Other_Quasi_CG' ></td>
            <td id='Cancelled_Other_Quasi_SG' ></td>
            <td id='Cancelled_Other_LB' ></td>
            <td id='Cancelled_Other_Private' ></td>
            <td id='Cancelled_Other_Total' ></td>
            </tr>
            <tr>
            <td>3</td>
            <td> No. of vacancies for Disabled applicants outstanding at the end of the half year.  </td>
            <td id='Outstanding_CG' ></td>
            <td id='Outstanding_UT' ></td>
            <td id='Outstanding_SG' ></td>
            <td id='Outstanding_Quasi_CG' ></td>
            <td id='Outstanding_Quasi_SG' ></td>
            <td id='Outstanding_LB' ></td>
            <td id='Outstanding_Private' ></td>
            <td id='Outstanding_Total' ></td>
            </tr>`;
            $('#tbodyvalue').html(tbodyvalue);
            ES25RptP2Submit();
            }
            if(rptid=='108'){
             //   $('#changeheader').text('E.S.2.1(Half Yearly)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+" " )
  
                $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.2.1 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=18>"+sessionStorage.Ex_name+" </br> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: E.S.2.1(Half Yearly) </br>Criteria and Year: "+$('#criteriaddl option:selected').html()+" - "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

                var tablehead="<tr><th  style='background: #716aca;border-right: inset;'>S.NO</th><th  style='background: #716aca;border-right: inset;' colspan=3>Eduational Level</th><th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>Total Educated (All Categories)</th><th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>Women (Included in Total)</th><th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>SC (Included in Total)</th><th  style='background: #716aca;border-right: inset;border-bottom: inset;' colspan=3>ST (Included in Total)</th><th  style='background: #716aca;border-bottom: inset;' colspan=3>OBC (Included in Total)</th></tr>"+
                "<tr><th  style='background: #716aca;border-right: inset;'></th><th  style='background: #716aca;'></th><th  style='background: #716aca;'></th><th  style='background: #716aca;border-right: inset;'></th><th  style='background: #716aca;'>Regd</th><th  style='background: #716aca;'>Placement</th><th  style='background: #716aca;border-right: inset;'>LR</th></th><th  style='background: #716aca;'>Redg</th><th  style='background: #716aca;'>Placement</th><th  style='background: #716aca;border-right: inset;'>LR</th><th  style='background: #716aca;'>Redg</th><th  style='background: #716aca;'>Placement</th><th  style='background: #716aca;border-right: inset;'>LR</th><th  style='background: #716aca;'>Redg</th><th  style='background: #716aca;'>Placement</th><th  style='background: #716aca;border-right: inset;'>LR</th><th  style='background: #716aca;'>Redg</th><th  style='background: #716aca;'>Placement</th><th  style='background: #716aca;'>LR</th></tr>";
            $("#tablehead").html(tablehead);
            
            FillES21_Master('parsedataFillES21_Master','tbodyvalue');
            
            }
            if(rptid=='109'){
            //    $('#changeheader').text('E.S.1.2(Half Yearly)');
            $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html() )
  
            $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return Showing by NCO work done by the Employment Exchange during the year ended:   ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.1.2 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=30>"+sessionStorage.Ex_name+" </br> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br>  Report Name: E.S.1.2(Half Yearly)</br> Year: "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

                var tablehead=`<tr>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'></th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=6>No. of applicant on Live Register at the end of the year in respect of</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=6>Notified during the year in respect of</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=6>Filled during the year in respect of</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=6>Cancelled during the year in respect of</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=6>Outstanding at the end of the year in respect of</th>
            
               
                </tr>
            <tr>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>NCO</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>WomanLR</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>SC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>ST</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>OBC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Disabled Persons</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>WomanLR</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>SC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>ST</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>OBC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Disabled Persons</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>WomanLR</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>SC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>ST</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>OBC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Disabled Persons</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>WomanLR</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>SC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>ST</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>OBC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Disabled Persons</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Total</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>WomanLR</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>SC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>ST</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>OBC</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>Disabled Persons</th>
              </tr>`;
                $("#tablehead").html(tablehead);
                FetchES12Entry();
            
            }
            if(rptid=='110'){
             //   $('#changeheader').text('E.S.1.3(Half Yearly)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html() )
  
             $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.1.3 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=10>"+sessionStorage.Ex_name+" </br> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: E.S.1.3(Half Yearly) </br> Year: "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

                var tablehead=`	<tr>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=1>S.NO</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=1>Item</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=1>Centrel Govt.</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset'>State Govt.</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=1>UT</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=2 >Quasi-Govt.Estt./Public Sector undertakings</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=1>Local Bodies</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan=2>Private Sector</th>
                <th  style='background: #716aca;border-right: inset;border-bottom:inset' colspan='2'>Total</th>
            </tr>
            <tr>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'>Central Govt.</th>
            <th  style='background: #716aca;'>State Govt.</th>
            <th  style='background: #716aca;'></th>
            <th  style='background: #716aca;'>Falling within the Perview of Act</th>
            <th  style='background: #716aca;'>Not Falling within the Perview of Act</th>
             <th  style='background: #716aca;'></th>
            </tr>
            `;
            var tbodyvalue=`<tr><td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            </tr>
            <tr>
            <td>1</td>
            <td>Vacancies remained outstanding at the end of the Previous year</td>
            <td id="Outstanding_Prev_CG" >
            </td>
            <td  id="Outstanding_Prev_SG" >
            </td>
            <td id="Outstanding_Prev_UT" >
            </td>
            <td  id="Outstanding_Prev_Quasi_SG" >
            </td>
            <td id="Outstanding_Prev_Quasi_CG" >
            </td>
            <td  id="Outstanding_Prev_LB" >
            </td>
            <td id="Outstanding_Prev_Private_Act" >
            </td>
            <td  id="Outstanding_Prev_Private_NonAct" >
            </td>
            <td  id="Total_Prev_Outstanding" >
            </td>
            </tr>
            <tr>
                <td>1a</td>
                <td>	Vacancies received on transfer from other exchanges during the year</td>
                <td id="Transfer_Receive_CG" >
                </td>
                <td  id="Transfer_Receive_SG" >
                </td>
                <td id="Transfer_Receive_UT" >
                </td>
                <td  id="Transfer_Receive_Quasi_SG" >
                </td>
                <td id="Transfer_Receive_Quasi_CG" >
                </td>
                <td  id="Transfer_Receive_LB" >
                </td>
                <td id="Transfer_Receive_Private_Act" >
                </td>
                <td  id="Transfer_Receive_Private_NonAct" >
                </td>
                <td  id="Total_Transfer_Receive" >
                </td>
            </tr>
            <tr>
                    <td>2</td>
                    <td>	Vacancies notified during the Year</td>
                    <td id="Notified_CG" >
                    </td>
                    <td  id="Notified_SG" >
                    </td>
                    <td id="Notified_UT" >
                    </td>
                    <td  id="Notified_Quasi_SG" >
                    </td>
                    <td id="Notified_Quasi_CG" >
                    </td>
                    <td  id="Notified_LB" >
                    </td>
                    <td id="Notified_Private_Act" >
                    </td>
                    <td  id="Notified_Private_NonAct" >
                    </td>
                    <td  id="Total_Notified" >
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Total(1+1a+2)</td>
                    <td id="Total_1_1a_2_CG" >
                    </td>
                    <td  id="Total_1_1a_2_SG" >
                    </td>
                    <td id="Total_1_1a_2_UT" >
                    </td>
                    <td  id="Total_1_1a_2_Quasi_SG" >
                    </td>
                    <td id="Total_1_1a_2_Quasi_CG" >
                    </td>
                    <td  id="Total_1_1a_2_LB" >
                    </td>
                    <td id="Total_1_1a_2_Private_Act" >
                    </td>
                    <td  id="Total_1_1a_2_Private_NonAct" >
                    </td>
                    <td  id="Total_Total_1_1a_2" >
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>	Vacancies Filled during the Year</td>
                    <td id="Filled_CG" >
                    </td>
                    <td  id="Filled_SG" >
                    </td>
                    <td id="Filled_UT" >
                    </td>
                    <td  id="Filled_Quasi_SG" >
                    </td>
                    <td id="Filled_Quasi_CG" >
                    </td>
                    <td  id="Filled_LB" >
                    </td>
                    <td id="Filled_Private_Act" >
                    </td>
                    <td  id="Filled_Private_NonAct" >
                    </td>
                    <td  id="Total_Filled" >
                    </td>
                </tr>
                <tr>
                        <td>5</td>
                        <td>	Vacancies Cancelled during the Period</td>
                        <td id="Cancelled_CG" >
                        </td>
                        <td  id="Cancelled_SG" >
                        </td>
                        <td id="Cancelled_UT" >
                        </td>
                        <td  id="Cancelled_Quasi_SG" >
                        </td>
                        <td id="Cancelled_Quasi_CG" >
                        </td>
                        <td  id="Cancelled_LB" >
                        </td>
                        <td id="Cancelled_Private_Act" >
                        </td>
                        <td  id="Cancelled_Private_NonAct" >
                        </td>
                        <td  id="Total_Cancelled" >
                        </td>
                    </tr>
                    <tr>
                            <td>5a</td>
                            <td>	Vacancies transfered to other Exchanges during the Year</td>
                            <td id="Transfer_CG" >
                            </td>
                            <td  id="Transfer_SG" >
                            </td>
                            <td id="Transfer_UT" >
                            </td>
                            <td  id="Transfer_Quasi_SG" >
                            </td>
                            <td id="Transfer_Quasi_CG" >
                            </td>
                            <td  id="Transfer_LB" >
                            </td>
                            <td id="Transfer_Private_Act" >
                            </td>
                            <td  id="Transfer_Private_NonAct" >
                            </td>
                            <td  id="Total_Transfer" >
                            </td>
                        </tr>
                        <tr>
                                <td>6</td>
                                <td>		Vacancies Outstanding at the end of the Year</td>
                                <td id="Outstanding_CG" >
                                </td>
                                <td  id="Outstanding_SG" >
                                </td>
                                <td id="Outstanding_UT" >
                                </td>
                                <td  id="Outstanding_Quasi_SG" >
                                </td>
                                <td id="Outstanding_Quasi_CG" >
                                </td>
                                <td  id="Outstanding_LB" >
                                </td>
                                <td id="Outstanding_Private_Act" >
                                </td>
                                <td  id="Outstanding_Private_NonAct" >
                                </td>
                                <td  id="Total_Outstanding" >
                                </td>
                            </tr>
                            <tr>
                                    <td>7</td>
                                    <td>		Total(4+5+5a+6)	</td>
                                    <td id="Total_4_5_5a_6_CG" >
                                    </td>
                                    <td  id="Total_4_5_5a_6_SG" >
                                    </td>
                                    <td id="Total_4_5_5a_6_UT" >
                                    </td>
                                    <td  id="Total_4_5_5a_6_Quasi_SG" >
                                    </td>
                                    <td id="Total_4_5_5a_6_Quasi_CG" >
                                    </td>
                                    <td  id="Total_4_5_5a_6_LB" >
                                    </td>
                                    <td id="Total_4_5_5a_6_Private_Act" >
                                    </td>
                                    <td  id="Total_4_5_5a_6_Private_NonAct" >
                                    </td>
                                    <td  id="Total_Total_4_5_5a_6" >
                                    </td>
                                </tr>
                                <tr>
                                        <td>7a</td>
                                        <td>	Vacancies notified during the year by private employers falling within the preview of the Act against which submission is not required	</td>
                                        <td id="Notified_NotSubmitted_CG" >
                                        </td>
                                        <td  id="Notified_NotSubmitted_SG" >
                                        </td>
                                        <td id="Notified_NotSubmitted_UT" >
                                        </td>
                                        <td  id="Notified_NotSubmitted_Quasi_SG" >
                                        </td>
                                        <td id="Notified_NotSubmitted_Quasi_CG" >
                                        </td>
                                        <td  id="Notified_NotSubmitted_LB" >
                                        </td>
                                        <td id="Notified_NotSubmitted_Private_Act" >
                                        </td>
                                        <td  id="Notified_NotSubmitted_Private_NonAct" >
                                        </td>
                                        <td  id="Total_Notified_NotSubmitted" >
                                        </td>
                                    </tr>
                                    <tr>
                                            <td>8</td>
                                            <td>	Vacancies included in item 4 filled by the applicants of other exchange area during the year	</td>
                                            <td id="Filled_ByOtherExch_CG" >
                                            </td>
                                            <td  id="Filled_ByOtherExch_SG" >
                                            </td>
                                            <td id="Filled_ByOtherExch_UT" >
                                            </td>
                                            <td  id="Filled_ByOtherExch_Quasi_SG" >
                                            </td>
                                            <td id="Filled_ByOtherExch_Quasi_CG" >
                                            </td>
                                            <td  id="Filled_ByOtherExch_LB" >
                                            </td>
                                            <td id="Filled_ByOtherExch_Private_Act" >
                                            </td>
                                            <td  id="Filled_ByOtherExch_Private_NonAct" >
                                            </td>
                                            <td  id="Total_Filled_ByOtherExch" >
                                            </td>
                                        </tr>
                                        <tr>
                                                <td>9</td>
                                                <td>		Local applicants placed in other exchange during the year	</td>
                                                <td id="Filled_ForOtherExch_CG" >
                                                </td>
                                                <td  id="Filled_ForOtherExch_SG" >
                                                </td>
                                                <td id="Filled_ForOtherExch_UT" >
                                                </td>
                                                <td  id="Filled_ForOtherExch_Quasi_SG" >
                                                </td>
                                                <td id="Filled_ForOtherExch_Quasi_CG" >
                                                </td>
                                                <td  id="Filled_ForOtherExch_LB" >
                                                </td>
                                                <td id="Filled_ForOtherExch_Private_Act" >
                                                </td>
                                                <td  id="Filled_ForOtherExch_Private_NonAct" >
                                                </td>
                                                <td  id="Total_Filled_ForOtherExch" >
                                                </td>
                                            </tr>	
                                            <tr>
                                                <td>10</td>
                                                <td>No of employers using the exchange during the Year</td>
                                                <td id="EmpUseExch_CG" >
                                                </td>
                                                <td  id="EmpUseExch_SG" >
                                                </td>
                                                <td id="EmpUseExch_UT" >
                                                </td>
                                                <td  id="EmpUseExch_Quasi_SG" >
                                                </td>
                                                <td id="EmpUseExch_Quasi_CG" >
                                                </td>
                                                <td  id="EmpUseExch_LB" >
                                                </td>
                                                <td id="EmpUseExch_Private_Act" >
                                                </td>
                                                <td  id="EmpUseExch_Private_NonAct" >
                                                </td>
                                                <td  id="Total_EmpUseExch" >
                                                </td>
                                            </tr>
                                        
                                            <tr>
                                                    <td colspan="11"  style="text-align: end;" >Signature of Employment Officer<br>
                                                        (With name and complete postal address of the Employment Exchange)
                                                        
                                              </tr>`;
             $("#tablehead").html(tablehead);
                 $("#tbodyvalue").html(tbodyvalue);
                 ES13Form();
            
            }
            if(rptid=='113'){
             //   $('#changeheader').text('E.S.2.2( Annual Report)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html() )
 
             $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return Showing Trade-wise distribution of Craftmen trained at the ITIs and full term Apprentices trained under the apprenticeship Act, on the Live Registration by NCO at the end of the Year and their Number registrered & placed in employment during the year :From Date-01/01/${$('#frmYear').val()}-To Date- 31/12/
                ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.2.2 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=9> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: E.S.2.2( Annual Report) </br> Year: "+$('#frmYear').val()+"</th></tr>";
                $("#Detailhead").html(head);

                var tablehead=`<tr>
                <th  style='background: #716aca;border-right: inset;' >
                    S.<br />
                    No.</th>
                <th  style='background: #716aca;border-right: inset;' >
                    NCO Code (6 digit level &amp; totals at 4 digit level)</th>
                <th  style='background: #716aca;border-right: inset;' >
                    Trade in which Trained</th>
                <th  style='background: #716aca;border-right: inset;border-bottom: inset;'  colspan="3">
                    Ex-ITI Trainees</th>
                <th  style='background: #716aca;border-bottom: inset;' colspan="3">
                    Full term Apprentices
                </th>
            </tr>
            <tr class="lblHeadingFontType">
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;border-right: inset;'>
                </th>
                <th  style='background: #716aca;border-right: inset;' >Registration</th>
                <th  style='background: #716aca;border-right: inset;' >Placement</th>
                <th  style='background: #716aca;border-right: inset;' >Live Register</th>
                <th  style='background: #716aca;border-right: inset;' >Registration</th>
                <th  style='background: #716aca;border-right: inset;' >Placement</th>
                <th  style='background: #716aca;' >Live Register</th>
            </tr>
            
            `;
            $("#tablehead").html(tablehead);
            fetchES22();
            
            }
              
            if(rptid=='128'){
            //    $('#changeheader').text('Career Counselling(Consolidate Rpt) With Months');
            $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
                $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${$('#frmYear').val()}`)
                
                $('#exnameess1rpt').text('View  E.S.2.1 Reports ('+sessionStorage.Exname_es11+' )');
                var head="<tr><th></th><th colspan=11> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: Career Counselling(Consolidate Rpt) With Months </br>From Year - To Year: "+$('#prpfromyear').val()+" - "+$('#prptoyear').val()+"</th></tr>";
                $("#Detailhead").html(head);

              var tablehead=`<tr><th  colspan=12 style='border-bottom: inset;'> माह मे आयोजित काउन्सिलिंग एवं लाभान्वित आवेदकों की जानकारी</th> </tr>
              <tr><th colspan=1 style='border-right: inset;'>क्र.</th><th colspan=1 style='border-right: inset;'> कार्यालय का नाम </th><th colspan=1 style='border-right: inset;'>माह में आयोजित कुल कैरियर काउंसलिंग की संख्या</th><th colspan=1 style='border-right: inset;'>कुल लाभान्वित आवेदकों की संख्या</th><th colspan=7 style='border-right: inset;border-bottom: inset;'>कुल लाभान्वित आवेदकों में से</th><th>रिमार्क</th></tr>
              <tr><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'>अनु. जाति</th><th style='border-right: inset;'>अनुसूचित जनजाति</th><th style='border-right: inset;'>अ. पि. वर्ग</th><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>निः शक्त</th><th style='border-right: inset;'>अल्प-संख्यक</th><th></th></tr>
              <tr><th style='border-Top: inset;border-right: inset;'>1</th><th style='border-Top: inset;border-right: inset;'>2</th><th style='border-Top: inset;border-right: inset;'>3</th><th style='border-Top: inset;border-right: inset;'>4</th><th style='border-Top: inset;border-right: inset;'>5</th><th style='border-Top: inset;border-right: inset;'>6</th><th style='border-Top: inset;border-right: inset;'>7</th><th style='border-Top: inset;border-right: inset;'>8</th><th style='border-Top: inset;border-right: inset;'>9</th><th style='border-Top: inset;border-right: inset;'>10</th><th style='border-Top: inset;border-right: inset;'>11</th><th style='border-Top: inset;'>12</th></tr>
              `;
                $("#tablehead").html(tablehead);
            
                fetchCounselling_ByMonthWise();            
            }

            if(rptid=='118'){
             //   $('#changeheader').text('Career Counselling(Consolidate Report)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html()+" </br>"+$('#prpfrommonth option:selected').html())
  
             $('.exchangenametext').text(sessionStorage.Exname_es11);
                $('.descriptiontext').text(`Return showing the work done by the Employment Exchanges in respect of vacancies (Sector-wise) for the Year ended :   ${$('#frmYear').val()}`)
                var head="<tr><th></th><th colspan=10> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: Career Counselling(Consolidate Report) </br> From Month $ Year: "+$('#prpfrommonth option:selected').html()+" - "+$('#118prpfromyear').val()+" </br> To Month $ Year: "+$('#prptomonth option:selected').html()+" - "+$('#118prptoyear').val()+"</th></tr>";
                $("#Detailhead").html(head);
            
                var tablehead=`<tr><th colspan=1 style='border-right: inset;'>क्र.</th><th colspan=1 style='border-right: inset;'>माह में आयोजित कुल कैरियर काउंसलिंग की संख्या</th><th colspan=1 style='border-right: inset;'>कुल लाभान्वित आवेदकों की संख्या</th><th colspan=7 style='border-right: inset;border-bottom: inset;'>कुल लाभान्वित आवेदकों में से</th><th colspan=2>रिमार्क</th></tr><tr><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'>अनु. जाति</th><th style='border-right: inset;'>अनुसूचित जनजाति</th><th style='border-right: inset;'>अ. पि. वर्ग</th><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>निः शक्त</th><th style='border-right: inset;'>अल्प-संख्यक</th><th></th><th></th></tr>`;

                $("#tablehead").html(tablehead);
            
                fetchCounselling_ByMonthConsolidate();            
            }

            if(rptid=='119'){
             //   $('#changeheader').text('Job Fair (Consolidated Report)');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
             $('.exchangenametext').text(sessionStorage.Exname_es11);
            
                var head="<tr><th></th><th colspan=13> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br>Report Name: Job Fair (Consolidated Report)</br>From Month & Year: "+$('#prpfrommonth option:selected').html()+" - "+$('#118prpfromyear').val()+" </br> To Month & Year: "+$('#prptomonth option:selected').val()+" - "+$('#118prptoyear').val()+"</th></tr>";
                $("#Detailhead").html(head);

              var tablehead=`<tr><th style='border-right: inset;'>क्र</th><th style='border-right: inset;'> कार्यालय का नाम </th><th style='border-right: inset;'>जॉब फेयर आयोजित करने की दिनांक</th><th style='border-right: inset;'>कंपनी / संस्था का नाम एवं पता</th><th style='border-right: inset;'>पद का नाम</th><th style='border-right: inset;'>कुल चयनित आवदको की संख्या (7+8+9+10)</th><th colspan='7' style='border-right: inset;border-bottom: inset;'>कुल में से</th><th>रिमार्क</th></tr>
              <tr><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'>अनु. जाति</th><th style='border-right: inset;'>अ.ज.जा</th><th style='border-right: inset;'>पि.वर्ग</th><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>निः शक्त</th><th style='border-right: inset;'>अल्पसंख्यक</th><th></th></tr>
             <tr><th style='border-right: inset;border-top: inset;'>1</th><th style='border-right: inset;border-top: inset;'>2</th><th style='border-right: inset;border-top: inset;'>3</th><th style='border-right: inset;border-top: inset;'>4</th><th style='border-right: inset;border-top: inset;'>5</th><th style='border-right: inset;border-top: inset;'>6</th><th style='border-right: inset;border-top: inset;'>7</th><th style='border-right: inset;border-top: inset;'>8</th><th style='border-right: inset;border-top: inset;'>9</th><th style='border-right: inset;border-top: inset;'>10</th><th style='border-right: inset;border-top: inset;'>11</th><th style='border-right: inset;border-top: inset;'>12</th><th style='border-right: inset;border-top: inset;'>13</th><th style='border-top: inset;'>14</th></tr> `;
                $("#tablehead").html(tablehead);
            
                fetchJFRep_ByMonthConsolidate();            
            }

            if(rptid=='114'){
                $('#changeheader').text('Career Counseling Monthly Reporting');
                $('.exchangenametext').text(sessionStorage.Exname_es11);
            
                var head="<tr><th>Report Name: Career Counseling Monthly Reporting, Office: "+$('#printreportproformaexchange option:selected').html()+"</th><th>From Month $ Year: "+$('#printproformafrommonth').val()+" - "+$('#frmYear').val()+" </th></tr>";
                $("#Detailhead").html(head);
            //   var tablehead=`<tr><th colspan='7' style='border-bottom: inset;'>कुल में से</th></tr>
            //   <tr><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'> अनु. जाति </th><th style='border-right: inset;'> अनुसूचित जनजाति </th><th style='border-right: inset;'> अ. पि. वर्ग </th><th style='border-right: inset;'> विकलांग </th><th style='border-bottom: inset;'>अल्पसंख्यक</th></tr>
            //  <tr><th style='border-right: inset;border-top: inset;'>1</th><th style='border-right: inset;border-top: inset;'>2</th><th style='border-right: inset;border-top: inset;'>3</th><th style='border-right: inset;border-top: inset;'>4</th><th style='border-right: inset;border-top: inset;'>5</th><th style='border-right: inset;border-top: inset;'>6</th><th>7</th></tr> `;
            var tablehead="<tr><th colspan=1 style='border-right: inset;'>क्र.</th><th colspan=1 style='border-right: inset;'>माह में आयोजित कुल कैरियर काउंसलिंग की संख्या</th><th colspan=1 style='border-right: inset;'>कुल लाभान्वित आवेदकों की संख्या</th><th colspan=7 style='border-right: inset;border-bottom: inset;'>कुल लाभान्वित आवेदकों में से</th><th colspan=2>रिमार्क</th></tr><tr><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'>अनु. जाति</th><th style='border-right: inset;'>अनुसूचित जनजाति</th><th style='border-right: inset;'>अ. पि. वर्ग</th><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>निः शक्त</th><th style='border-right: inset;'>अल्प-संख्यक</th><th></th><th></th></tr>";

                $("#tablehead").html(tablehead);
            
               fetchCareerCounselingMonthlyRpt();            
            }

            if(rptid=='115'){
                $('#changeheader').text('Job Fair Monthly Reporting');
                $('.exchangenametext').text(sessionStorage.Exname_es11);
            
                var head="<tr><th>Report Name: Job Fair Monthly Reporting, Office: "+$('#printreportproformaexchange option:selected').html()+"</th><th>From Month $ Year: "+$('#printproformafrommonth').val()+" - "+$('#frmYear').val()+" </th></tr>";
                $("#Detailhead").html(head);
            //   var tablehead=`<tr><th colspan='7' style='border-bottom: inset;'>कुल में से</th></tr>
            //   <tr><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'> अनु. जाति </th><th style='border-right: inset;'> अनुसूचित जनजाति </th><th style='border-right: inset;'> अ. पि. वर्ग </th><th style='border-right: inset;'> विकलांग </th><th style='border-bottom: inset;'>अल्पसंख्यक</th></tr>
            //  <tr><th style='border-right: inset;border-top: inset;'>1</th><th style='border-right: inset;border-top: inset;'>2</th><th style='border-right: inset;border-top: inset;'>3</th><th style='border-right: inset;border-top: inset;'>4</th><th style='border-right: inset;border-top: inset;'>5</th><th style='border-right: inset;border-top: inset;'>6</th><th>7</th></tr> `;
            var tablehead="<tr><th colspan=1 style='border-right: inset;'>क्र.</th><th colspan=1 style='border-right: inset;'> जॉब फेयर आयोजित करने का दिनांक </th><th colspan=1 style='border-right: inset;'> कंपनी / संस्था का नाम एवं पता </th><th colspan=1 style='border-right: inset;'> पद का नाम </th><th colspan=1 style='border-right: inset;'> कुल चयनित आवेदकों कि संख्या</th><th colspan=7 style='border-right: inset;border-bottom: inset;'>कुल लाभान्वित आवेदकों में से</th><th colspan=2>रिमार्क</th></tr><tr><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'></th><th style='border-right: inset;'>महिला</th><th style='border-right: inset;'>अनारक्षित</th><th style='border-right: inset;'>अनु. जाति</th><th style='border-right: inset;'>अनुसूचित जनजाति</th><th style='border-right: inset;'>अ. पि. वर्ग</th><th style='border-right: inset;'>निः शक्त</th><th style='border-right: inset;'>अल्प-संख्यक</th><th></th><th></th></tr>";
  
            $("#tablehead").html(tablehead);
            
               fetchJobFairMonthlyReporting();            
            }
            if(rptid=='134'){
            //    $('#changeheader').text('X-63 Report');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())

               var head="<tr><th></th><th colspan=10> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: X-63 Report </br>From Date - To Date: "+$('#m_datepicker_5').val()+" - "+$('#m_datepicker_6').val()+" </th></tr>";
                $("#Detailhead").html(head);

                var tablehead= "<tr><th style='border-top: ridge;border-right: ridge;'>Sno</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Reg.No & Reg. Date</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Name/Father's/ Husband Name & Address</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>DOB/ Category/ Gender</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Area/ Domicile/PHP </th><th colspan=3 style='border-top: ridge;border-right: ridge;'>NCO</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Qualification</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Entered By/Current Status</th><th colspan=3 style='border-top: ridge;'>JobSeeker History</th></tr>"+
                "<tr><th style='border-right: ridge;'></th><th colspan=3 style='border-right: ridge;'></th><th colspan=3 style='border-right: ridge;'></th><th colspan=3 style='border-right: ridge;'></th><th colspan=3 style='border-right: ridge;'></th><th style='border-top: ridge;border-right: ridge;'>Alloted NCO</th><th style='border-top: ridge;border-right: ridge;'>Seniority Date</th><th style='border-top: ridge;border-right: ridge;'>NCO Detail</th><th style='border-top: ridge;border-right: ridge;'>Qualification</th><th style='border-top: ridge;border-right: ridge;' colspan=2>Entry Date</th><th colspan=3 style='border-right: ridge;'></th><th colspan=3></th></tr>";
              $("#tablehead").html(tablehead);
            
              fetchx63();           
            }
            if(rptid=='126'){
            //    $('#changeheader').text('Age Group Wise JS Summary Qualification Level');
            $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
            $('.exchangenametext').text(sessionStorage.Exname_es11);
            
                var head="<tr><th></th><th colspan=19> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: Age Group Wise Qualification Level JS Summary Report </br>As on Date: "+$('#m_datepicker_3').val()+"  </th></tr>";
                $("#Detailhead").html(head);

           var tablehead="<tr><th style='border-right: inset;'>Sno.</th><th style='border-right: inset;'> Qualification Name </th><th  style='border-right: inset;'>Male 19</th><th style='border-right: inset;'> Female 19 </th><th style='border-right: inset;'> Total 19 </th><th  style='border-right: inset;'> Male 29 </th><th style='border-right: inset;'> Female 29 </th><th style='border-right: inset;'> Total 29 </th><th  style='border-right: inset;'>Male 39</th><th style='border-right: inset;'> Female 39 </th><th style='border-right: inset;'> Total 39 </th><th  style='border-right: inset;'>Male 49</th><th style='border-right: inset;'> Female 49 </th><th style='border-right: inset;'> Total 49 </th><th  style='border-right: inset;'>Male 59</th><th style='border-right: inset;'> Female 59 </th><th style='border-right: inset;'> Total 59 </th><th  style='border-right: inset;'>Male 60</th><th style='border-right: inset;'> Female  60 </th><th> Total  60 </th><th style='font-weight: 600;'>Grand Total</th></tr>";

            $("#tablehead").html(tablehead);
            fetchagewiseQualiLevel();
            }
            if(rptid=='123'){
             //   $('#changeheader').text('Age Group Wise JS Summary Report');
             $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
             $('.exchangenametext').text(sessionStorage.Exname_es11);
            
                var head="<tr><th></th><th colspan=20> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: Age Group Wise Qualification JS Summary Report </br>From Date - To Date: "+$('#m_datepicker_5').val()+" - "+$('#m_datepicker_6').val()+" </th></tr>";
                $("#Detailhead").html(head);

            var tablehead="<tr><th style='border-right: inset;'>Sno.</th><th style='border-right: inset;'> Qualification Name </th><th  style='border-right: inset;'>Male Below 19</th><th style='border-right: inset;'> Female Below 19 </th><th style='border-right: inset;'> Total Below 19 </th><th  style='border-right: inset;'> Male 20-29 </th><th style='border-right: inset;'> Female 20-29 </th><th style='border-right: inset;'> Total 20-29 </th><th  style='border-right: inset;'>Male 30-39</th><th style='border-right: inset;'> Female 30-39 </th><th style='border-right: inset;'> Total 30-39 </th><th  style='border-right: inset;'>Male 40-49</th><th style='border-right: inset;'> Female 40-49 </th><th style='border-right: inset;'> Total 40-49 </th><th  style='border-right: inset;'>Male 50-59</th><th style='border-right: inset;'> Female 50-59 </th><th style='border-right: inset;'> Total 50-59 </th><th  style='border-right: inset;'>Male > 60</th><th style='border-right: inset;'> Female > 60 </th><th style='border-right: inset;'> Total > 60 </th><th style='font-weight: 600;'>Grand Total</th></tr>";
            $("#tablehead").html(tablehead);
            fetchagewiseQuali();     
            }

            if(rptid=='127'){
            //    $('#changeheader').text('Category Wise JS Summary Qualification Level');
            $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
                $('.exchangenametext').text(sessionStorage.Exname_es11);
                var head="<tr><th></th><th colspan=13> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: Category Wise JS Summary Qualification Level </br>As on Date :: "+$('#m_datepicker_3').val()+"  </th></tr>";
                $("#Detailhead").html(head);

            var tablehead="<tr><th style='border-right: inset;'>Sno.</th><th style='border-right: inset;'>Qualification</th><th  style='border-right: inset;'>Male UR</th><th style='border-right: inset;'> Female UR </th><th style='border-right: inset;'> Total UR </th><th  style='border-right: inset;'> Male SC </th><th style='border-right: inset;'> Female SC </th><th style='border-right: inset;'> Total SC </th><th  style='border-right: inset;'>Male ST</th><th style='border-right: inset;'> Female ST </th><th style='border-right: inset;'> Total ST </th><th  style='border-right: inset;'>Male OBC</th><th style='border-right: inset;'> Female OBC </th><th> Total OBC </th></tr>";
            $("#tablehead").html(tablehead);
            fetchCategorywiseQualifiLevel();     
            }

            if(rptid=='124'){
            //    $('#changeheader').text('Category Wise JS Summary Report');
            $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReportName option:selected').html())
  
                $('.exchangenametext').text(sessionStorage.Exname_es11);
                var head="<tr><th></th><th colspan=17> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name:Category Wise Qualification JS Summary Report <br>As on Date:"+$('#m_datepicker_3').val()+" </th></tr>";
                $("#Detailhead").html(head);

            var tablehead="<tr><th style='border-right: inset;'>Sno.</th><th style='border-right: inset;'>Qualification</th><th  style='border-right: inset;'>Male UR</th><th style='border-right: inset;'> Female UR </th><th style='border-right: inset;'> Total UR </th><th  style='border-right: inset;'> Male SC </th><th style='border-right: inset;'> Female SC </th><th style='border-right: inset;'> Total SC </th><th  style='border-right: inset;'>Male ST</th><th style='border-right: inset;'> Female ST </th><th style='border-right: inset;'> Total ST </th><th  style='border-right: inset;'>Male OBC</th><th style='border-right: inset;'> Female OBC </th><th style='border-right: inset;'> Total OBC </th><th style='border-right: inset;'>Total Male</th><th style='border-right: inset;'>Total Female</th><th>Grand Total</th></tr>";
            $("#tablehead").html(tablehead);
            fetchCategorywiseQualifi();     
            }
     }


    function FetchES11Rpt(){
        var path =  serverpath + "ES11_Rpt/'"+$('#printreportproformaexchange').val()+"'/'"+$("#printproformafrommonth").val()+"'/'"+$("#frmYear").val()+"'"
        ajaxget(path,'parsedataFetchES11Rpt','comment',"control");                          
       }
       function parsedataFetchES11Rpt(data){
        data = JSON.parse(data)
        
       
        var appenddata='';
        var Total_3_M=parseInt(data[0][0].Prev_LR_M)+data[0][0].ReceiveRegistration_M+data[0][0].FreshRegistration_M+data[0][0].ReRegistration_M;
   
        var Total_3_F=data[0][0].Prev_LR_F+data[0][0].ReceiveRegistration_F+data[0][0].FreshRegistration_F+data[0][0].ReRegistration_F;
      
        var Total_3=Total_3_M+Total_3_F;
        var Total_7_M=data[0][0].PlacedRegistration_M+data[0][0].RemovedRegistration_M+data[0][0].TransferRegistration_M;
        var Total_7_F=data[0][0].PlacedRegistration_F+data[0][0].RemovedRegistration_F+data[0][0].TransferRegistration_F;
        var Total_7=Total_7_M+Total_7_F;

 var LRendofmnthM=  Total_3_M - Total_7_M; 
 var LRendofmnthF=  Total_3_F - Total_7_F;    
 var LRendofmnthT=  Total_3 - Total_7;  
 if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
    if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
    jQuery("#tablehead").html('<th>No Record Found</th>');
    $('.fa').hide();
    jQuery("#tbodyvalue").html('');
    }
    }
    else{
        appenddata=`<tr>
                                                    <td>1</td>
                                                    <td>	Applicants on the Live Register at the end of the Previous Month</td>
                                                    <td>${data[0][0].Prev_LR_M}</td>
                                                    <td> ${data[0][0].Prev_LR_F}</td>
                                                    <td>  ${data[0][0].Prev_LR_M+data[0][0].Prev_LR_F}</td>
                                             </tr>
                                            <tr>
                                                    <td>1a</td>
                                                    <td>	No. of Registration cards recieved on Transfer</td>
                                                    <td> ${data[0][0].ReceiveRegistration_M}</td>
                                                    <td> ${data[0][0].ReceiveRegistration_F}</td>
                                                    <td> ${data[0][0].ReceiveRegistration_M+data[0][0].ReceiveRegistration_F}
                                                    </td>
                                            </tr>
                      
                                            <tr >
                                                <td>2</td>
                                                <td>	No. of Fresh registration made during the month</td>
                                                <td>  ${data[0][0].FreshRegistration_M}</td>
                                                <td>  ${data[0][0].FreshRegistration_F}</td>
                                                <td>  ${data[0][0].FreshRegistration_M+data[0][0].FreshRegistration_F}</input>
                                                </td>
                                              </tr>
                                              <tr >
                                                <td>2a</td>
                                                <td>		No. of Re-registration made during the month</td>
                                                <td>${data[0][0].ReRegistration_M}
                                                </td>
                                                <td>${data[0][0].ReRegistration_F}</td>
                                                <td>${data[0][0].ReRegistration_M+data[0][0].ReRegistration_F}</td>
                                             </tr>
                                              <tr >
                                                <td>3</td>
                                                <td>Total: Items 1, 1a, 2 and 2a</td>
                                                <td>  ${Total_3_M}
                                                </td>
                                                <td> ${Total_3_F}</td>
                                                <td>  ${Total_3} </td>
                                              </tr>
                                            <tr >
                                                <td>4</td>
                                                <td>No. of Job-seekers placed during the Month</td>
                                                <td>${data[0][0].PlacedRegistration_M}
                                                </td>
                                                <td>${data[0][0].PlacedRegistration_F}</td>
                                                <td>${data[0][0].PlacedRegistration_M+data[0][0].PlacedRegistration_F}</td>
                                            </tr>
                                            <tr >
                                                <td>5</td>
                                                <td>Registration Card removed from Live Register for reason other than Transfer to other exchanges</td>
                                                <td>  ${data[0][0].RemovedRegistration_M}
                                                </td>
                                                <td> ${data[0][0].RemovedRegistration_F}  </td>
                                                <td> ${data[0][0].RemovedRegistration_M+data[0][0].RemovedRegistration_F}  </td>
                                           </tr>
                                                          <tr >
                                                            <td>6</td>
                                                            <td>No. of Registration cards transferred during the month to other exchanges</td>
                                                            <td> ${data[0][0].TransferRegistration_M}
                                                            </td>
                                                            <td> ${data[0][0].TransferRegistration_F}</td>
                                                            <td> ${data[0][0].TransferRegistration_M+data[0][0].TransferRegistration_F}</td>
                                                      </tr>
                                                      <tr >
                                                        <td>7</td>
                                                        <td>	Total: Items 4, 5 and 6</td>
                                                        <td>${Total_7_M}
                                                        </td>
                                                        <td>${Total_7_F}</td>
                                                        <td>${Total_7}</td>
                                                  </tr>
                                                  <tr >
                                                    <td>8</td>
                                                    <td>Applicants remaining on the Live Register at the end of the Month</td>
                                                    <td>${LRendofmnthM}</td>
                                                    <td>${LRendofmnthF}</td>
                                                    <td>${LRendofmnthT}</td>
                                              </tr>
                                                  <tr >
                                                    <td>8a</td>
                                                    <td>No. of applicants out of item 8 above belong to Rural Area</td>
                                                    <td>${data[0][0].Rural_LR_M}</td>
                                                    <td>${data[0][0].Rural_LR_F}</td>
                                                    <td>${data[0][0].Rural_LR_M+data[0][0].Rural_LR_F}</td>
                                                    </tr>
                                              <tr >
                                                <td>9</td>
                                                <td>Submissions made during the Month</td>
                                                <td>${data[0][0].Submissions_M}</td>
                                                <td>${data[0][0].Submissions_F}</td>
                                                <td>${data[0][0].Submissions_M+data[0][0].Submissions_F}</td>
                                          </tr>
                                          <tr >
                                            <td>10</td>
                                            <td>	Total Vacancies notified during the Month</td>
                                            <td>${data[0][0].NoOfNotify_M}</td>
                                            <td>${data[0][0].NoOfNotify_F}</td>
                                            <td>${data[0][0].NoOfNotify_M+data[0][0].NoOfNotify_F}</td>
                                      </tr>
                                      <tr>
                                      <tr>
                                        <td colspan="5"  style="text-align: end;" >Signature of Employment Officer<br>
                                          (With name and complete postal address of the Employment Exchange)
                                          
                                      </tr>
                                  
     `;
     jQuery("#tbodyvalue").html(appenddata); 
       }
    }
       function fetchES22(){
        var todt=`${$('#frmYear').val()}-12-31`;
        var path =  serverpath + "getES22Entry/"+$('#printreportproformaexchange').val()+"/"+todt+"/1";
            ajaxget(path,'parsedatafetchES22','comment',"control"); 
    }
      function parsedatafetchES22(data){  
          data = JSON.parse(data);
          var appendata='';
          var data1 = data[0];
          var isData=false;
          if(isData=true){
            for (var i = 0; i < data1.length; i++) {
                // if(data1[i].Flag=='Add'){
                //     isData=true;
                
                appendata+=`<tr class="tbl_row" id="${data1[i].Flag}_${i}">
                <td class="ES24_II_Id">${i+1}</td>
                <td class="NCO">${data1[i].NCO_Code}</td>
                <td>${data1[i].Subject}</td>
                <td >${data1[i].TraineeReg}</td>
                <td>${data1[i].TraineePlacement} </td>
                <td >${data1[i].TraineeLR}</td>
                <td>${data1[i].ApprenticeReg} </td>
                <td >${data1[i].ApprenticePlace} </td>
                <td >${data1[i].ApprenticeLR}</td>
            </tr>`
         //     }
              
            // else if(isData=false) {
            //     $('#tbodyvalue, #tablehead').empty();
            //     $('#tablehead').html('<th>No Records Found</th>')
            //   }
            } $('#tbodyvalue').html(appendata);}
            else  if(isData=false) {
               $('#tbodyvalue, #tablehead').empty();
               $('#tablehead').html('<th>No Records Found</th>')
             }
            // else{
            //    $('#tbodyvalue').html(appendata);
            // }
      }



      
       function ES13Form(){
        var FromDt='';
        var ToDt='';
    if($('#criteriaddl').val()=='1'){
        FromDt=`${$('#frmYear').val()}-01-01`;
        ToDt=`${$('#frmYear').val()}-06-30`;
    }
    else if($('#criteriaddl').val()=='2'){
        FromDt=`${$('#frmYear').val()}-07-01`;
        ToDt=`${$('#frmYear').val()}-12-31`;
    }
        var MasterData = {
            
    "p_Ex_Id":$('#printreportproformaexchange').val(),
    "p_FromDt":FromDt,
    "p_ToDt":ToDt,
    "p_Outstanding_Prev_CG": $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),
    "p_Outstanding_Prev_SG": $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),
    "p_Outstanding_Prev_UT": $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val(),
    "p_Outstanding_Prev_Quasi_SG": $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val(),
    "p_Outstanding_Prev_Quasi_CG": $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val(),
    "p_Outstanding_Prev_LB": $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),
    "p_Outstanding_Prev_Private_Act": $("#Outstanding_Prev_Private_Act").val()==''?0:$("#Outstanding_Prev_Private_Act").val(),
    "p_Outstanding_Prev_Private_NonAct": $("#Outstanding_Prev_Private_NonAct").val()==''?0:$("#Outstanding_Prev_Private_NonAct").val(),
    "p_Total_Prev_Outstanding": $("#Total_Prev_Outstanding").val()==''?0:$("#Total_Prev_Outstanding").val(),
    
    "p_Transfer_Receive_CG": $("#Transfer_Receive_CG").val()==''?0:$("#Transfer_Receive_CG").val(),
    "p_Transfer_Receive_SG": $("#Transfer_Receive_SG").val()==''?0:$("#Transfer_Receive_SG").val(),
    "p_Transfer_Receive_UT": $("#Transfer_Receive_UT").val()==''?0:$("#Transfer_Receive_UT").val(),
    "p_Transfer_Receive_Quasi_SG": $("#Transfer_Receive_Quasi_SG").val()==''?0:$("#Transfer_Receive_Quasi_SG").val(),
    "p_Transfer_Receive_Quasi_CG": $("#Transfer_Receive_Quasi_CG").val()==''?0:$("#Transfer_Receive_Quasi_CG").val(),
    "p_Transfer_Receive_LB": $("#Transfer_Receive_LB").val()==''?0:$("#Transfer_Receive_LB").val(),
    "p_Transfer_Receive_Private_Act": $("#Transfer_Receive_Private_Act").val()==''?0:$("#Transfer_Receive_Private_Act").val(),
    "p_Transfer_Receive_Private_NonAct": $("#Transfer_Receive_Private_NonAct").val()==''?0:$("#Transfer_Receive_Private_NonAct").val(),
    "p_Total_Transfer_Receive": $("#Total_Transfer_Receive").val()==''?0:$("#Total_Transfer_Receive").val(),
    
    "p_Notified_CG": $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),
    "p_Notified_SG": $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),
    "p_Notified_UT": $("#Notified_UT").val()==''?0:$("#Notified_UT").val(),
    "p_Notified_Quasi_SG": $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val(),
    "p_Notified_Quasi_CG": $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val(),
    "p_Notified_LB": $("#Notified_LB").val()==''?0:$("#Notified_LB").val(),
    "p_Notified_Private_Act": $("#Notified_Private_Act").val()==''?0:$("#Notified_Private_Act").val(),
    "p_Notified_Private_NonAct": $("#Notified_Private_NonAct").val()==''?0:$("#Notified_Private_NonAct").val(),
    "p_Total_Notified": $("#Total_Notified").val()==''?0:$("#Total_Notified").val(),
    
    "p_Total_1_1a_2_CG": $("#Total_1_1a_2_CG").val()==''?0:$("#Total_1_1a_2_CG").val(),
    "p_Total_1_1a_2_SG": $("#Total_1_1a_2_SG").val()==''?0:$("#Total_1_1a_2_SG").val(),
    "p_Total_1_1a_2_UT": $("#Total_1_1a_2_UT").val()==''?0:$("#Total_1_1a_2_UT").val(),
    "p_Total_1_1a_2_Quasi_SG": $("#Total_1_1a_2_Quasi_SG").val()==''?0:$("#Total_1_1a_2_Quasi_SG").val(),
    "p_Total_1_1a_2_Quasi_CG": $("#Total_1_1a_2_Quasi_CG").val()==''?0:$("#Total_1_1a_2_Quasi_CG").val(),
    "p_Total_1_1a_2_LB": $("#Total_1_1a_2_LB").val()==''?0:$("#Total_1_1a_2_LB").val(),
    "p_Total_1_1a_2_Private_Act": $("#Total_1_1a_2_Private_Act").val()==''?0:$("#Total_1_1a_2_Private_Act").val(),
    "p_Total_1_1a_2_Private_NonAct": $("#Total_1_1a_2_Private_NonAct").val()==''?0:$("#Total_1_1a_2_Private_NonAct").val(),
    "p_Total_Total_1_1a_2": $("#Total_Total_1_1a_2").val()==''?0:$("#Total_Total_1_1a_2").val(),
    
    "p_Filled_CG": $("#Filled_CG").val()==''?0:$("#Filled_CG").val(),
    "p_Filled_SG": $("#Filled_SG").val()==''?0:$("#Filled_SG").val(),
    "p_Filled_UT": $("#Filled_UT").val()==''?0:$("#Filled_UT").val(),
    "p_Filled_Quasi_SG": $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val(),
    "p_Filled_Quasi_CG": $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val(),
    "p_Filled_LB": $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),
    "p_Filled_Private_Act": $("#Filled_Private_Act").val()==''?0:$("#Filled_Private_Act").val(),
    "p_Filled_Private_NonAct": $("#Filled_Private_NonAct").val()==''?0:$("#Filled_Private_NonAct").val(),
    "p_Total_Filled": $("#Total_Filled").val()==''?0:$("#Total_Filled").val(),
    
    "p_Cancelled_CG": $("#Cancelled_CG").val()==''?0:$("#Cancelled_CG").val(),
    "p_Cancelled_SG": $("#Cancelled_SG").val()==''?0:$("#Cancelled_SG").val(),
    "p_Cancelled_UT": $("#Cancelled_UT").val()==''?0:$("#Cancelled_UT").val(),
    "p_Cancelled_Quasi_SG": $("#Cancelled_Quasi_SG").val()==''?0:$("#Cancelled_Quasi_SG").val(),
    "p_Cancelled_Quasi_CG": $("#Cancelled_Quasi_CG").val()==''?0:$("#Cancelled_Quasi_CG").val(),
    "p_Cancelled_LB": $("#Cancelled_LB").val()==''?0:$("#Cancelled_LB").val(),
    "p_Cancelled_Private_Act": $("#Cancelled_Private_Act").val()==''?0:$("#Cancelled_Private_Act").val(),
    "p_Cancelled_Private_NonAct": $("#Cancelled_Private_NonAct").val()==''?0:$("#Cancelled_Private_NonAct").val(),
    "p_Total_Cancelled": $("#Total_Cancelled").val()==''?0:$("#Total_Cancelled").val(),
    
    "p_Transfer_CG": $("#Transfer_CG").val()==''?0:$("#Transfer_CG").val(),
    "p_Transfer_SG": $("#Transfer_SG").val()==''?0:$("#Transfer_SG").val(),
    "p_Transfer_UT": $("#Transfer_UT").val()==''?0:$("#Transfer_UT").val(),
    "p_Transfer_Quasi_SG": $("#Transfer_Quasi_SG").val()==''?0:$("#Transfer_Quasi_SG").val(),
    "p_Transfer_Quasi_CG": $("#Transfer_Quasi_CG").val()==''?0:$("#Transfer_Quasi_CG").val(),
    "p_Transfer_LB": $("#Transfer_LB").val()==''?0:$("#Transfer_LB").val(),
    "p_Transfer_Private_Act": $("#Transfer_Private_Act").val()==''?0:$("#Transfer_Private_Act").val(),
    "p_Transfer_Private_NonAct": $("#Transfer_Private_NonAct").val()==''?0:$("#Transfer_Private_NonAct").val(),
    "p_Total_Transfer": $("#Total_Transfer").val()==''?0:$("#Total_Transfer").val(),
    
    "p_Outstanding_CG": $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val(),
    "p_Outstanding_SG": $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val(),
    "p_Outstanding_UT": $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),
    "p_Outstanding_Quasi_SG": $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val(),
    "p_Outstanding_Quasi_CG": $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),
    "p_Outstanding_LB": $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val(),
    "p_Outstanding_Private_Act": $("#Outstanding_Private_Act").val()==''?0:$("#Outstanding_Private_Act").val(),
    "p_Outstanding_Private_NonAct": $("#Outstanding_Private_NonAct").val()==''?0:$("#Outstanding_Private_NonAct").val(),
    "p_Total_Outstanding": $("#Total_Outstanding").val()==''?0:$("#Total_Outstanding").val(),
    
    "p_Total_4_5_5a_6_CG": $("#Total_4_5_5a_6_CG").val()==''?0:$("#Total_4_5_5a_6_CG").val(),
    "p_Total_4_5_5a_6_SG": $("#Total_4_5_5a_6_SG").val()==''?0:$("#Total_4_5_5a_6_SG").val(),
    "p_Total_4_5_5a_6_UT": $("#Total_4_5_5a_6_UT").val()==''?0:$("#Total_4_5_5a_6_UT").val(),
    "p_Total_4_5_5a_6_Quasi_SG": $("#Total_4_5_5a_6_Quasi_SG").val()==''?0:$("#Total_4_5_5a_6_Quasi_SG").val(),
    "p_Total_4_5_5a_6_Quasi_CG": $("#Total_4_5_5a_6_Quasi_CG").val()==''?0:$("#Total_4_5_5a_6_Quasi_CG").val(),
    "p_Total_4_5_5a_6_LB": $("#Total_4_5_5a_6_LB").val()==''?0:$("#Total_4_5_5a_6_LB").val(),
    "p_Total_4_5_5a_6_Private_Act": $("#Total_4_5_5a_6_Private_Act").val()==''?0:$("#Total_4_5_5a_6_Private_Act").val(),
    "p_Total_4_5_5a_6_Private_NonAct": $("#Total_4_5_5a_6_Private_NonAct").val()==''?0:$("#Total_4_5_5a_6_Private_NonAct").val(),
    "p_Total_Total_4_5_5a_6": $("#Total_Total_4_5_5a_6").val()==''?0:$("#Total_Total_4_5_5a_6").val(),
    
    
    "p_Notified_NotSubmitted_CG": $("#Notified_NotSubmitted_CG").val()==''?0:$("#Notified_NotSubmitted_CG").val(),
    "p_Notified_NotSubmitted_SG": $("#Notified_NotSubmitted_SG").val()==''?0:$("#Notified_NotSubmitted_SG").val(),
    "p_Notified_NotSubmitted_UT": $("#Notified_NotSubmitted_UT").val()==''?0:$("#Notified_NotSubmitted_UT").val(),
    "p_Notified_NotSubmitted_Quasi_SG": $("#Notified_NotSubmitted_Quasi_SG").val()==''?0:$("#Notified_NotSubmitted_Quasi_SG").val(),
    "p_Notified_NotSubmitted_Quasi_CG": $("#Notified_NotSubmitted_Quasi_CG").val()==''?0:$("#Notified_NotSubmitted_Quasi_CG").val(),
    "p_Notified_NotSubmitted_LB": $("#Notified_NotSubmitted_LB").val()==''?0:$("#Notified_NotSubmitted_LB").val(),
    "p_Notified_NotSubmitted_Private_Act": $("#Notified_NotSubmitted_Private_Act").val()==''?0:$("#Notified_NotSubmitted_Private_Act").val(),
    "p_Notified_NotSubmitted_Private_NonAct": $("#Notified_NotSubmitted_Private_NonAct").val()==''?0:$("#Notified_NotSubmitted_Private_NonAct").val(),
    "p_Total_Notified_NotSubmitted": $("#Total_Notified_NotSubmitted").val()==''?0:$("#Total_Notified_NotSubmitted").val(),
    
    
    "p_Filled_ByOtherExch_CG": $("#Filled_ByOtherExch_CG").val()==''?0:$("#Filled_ByOtherExch_CG").val(),
    "p_Filled_ByOtherExch_SG": $("#Filled_ByOtherExch_SG").val()==''?0:$("#Filled_ByOtherExch_SG").val(),
    "p_Filled_ByOtherExch_UT": $("#Filled_ByOtherExch_UT").val()==''?0:$("#Filled_ByOtherExch_UT").val(),
    "p_Filled_ByOtherExch_Quasi_SG": $("#Filled_ByOtherExch_Quasi_SG").val()==''?0:$("#Filled_ByOtherExch_Quasi_SG").val(),
    "p_Filled_ByOtherExch_Quasi_CG": $("#Filled_ByOtherExch_Quasi_CG").val()==''?0:$("#Filled_ByOtherExch_Quasi_CG").val(),
    "p_Filled_ByOtherExch_LB": $("#Filled_ByOtherExch_LB").val()==''?0:$("#Filled_ByOtherExch_LB").val(),
    "p_Filled_ByOtherExch_Private_Act": $("#Filled_ByOtherExch_Private_Act").val()==''?0:$("#Filled_ByOtherExch_Private_Act").val(),
    "p_Filled_ByOtherExch_Private_NonAct": $("#Filled_ByOtherExch_Private_NonAct").val()==''?0:$("#Filled_ByOtherExch_Private_NonAct").val(),
    "p_Total_Filled_ByOtherExch": $("#Total_Filled_ByOtherExch").val()==''?0:$("#Total_Filled_ByOtherExch").val(),
    
    "p_Filled_ForOtherExch_CG": $("#Filled_ForOtherExch_CG").val()==''?0:$("#Filled_ForOtherExch_CG").val(),
    "p_Filled_ForOtherExch_SG": $("#Filled_ForOtherExch_SG").val()==''?0:$("#Filled_ForOtherExch_SG").val(),
    "p_Filled_ForOtherExch_UT": $("#Filled_ForOtherExch_UT").val()==''?0:$("#Filled_ForOtherExch_UT").val(),
    "p_Filled_ForOtherExch_Quasi_SG": $("#Filled_ForOtherExch_Quasi_SG").val()==''?0:$("#Filled_ForOtherExch_Quasi_SG").val(),
    "p_Filled_ForOtherExch_Quasi_CG": $("#Filled_ForOtherExch_Quasi_CG").val()==''?0:$("#Filled_ForOtherExch_Quasi_CG").val(),
    "p_Filled_ForOtherExch_LB": $("#Filled_ForOtherExch_LB").val()==''?0:$("#Filled_ForOtherExch_LB").val(),
    "p_Filled_ForOtherExch_Private_Act": $("#Filled_ForOtherExch_Private_Act").val()==''?0:$("#Filled_ForOtherExch_Private_Act").val(),
    "p_Filled_ForOtherExch_Private_NonAct": $("#Filled_ForOtherExch_Private_NonAct").val()==''?0:$("#Filled_ForOtherExch_Private_NonAct").val(),
    "p_Total_Filled_ForOtherExch": $("#Total_Filled_ForOtherExch").val()==''?0:$("#Total_Filled_ForOtherExch").val(),
    
    "p_EmpUseExch_CG": $("#EmpUseExch_CG").val()==''?0:$("#EmpUseExch_CG").val(),
    "p_EmpUseExch_SG": $("#EmpUseExch_SG").val()==''?0:$("#EmpUseExch_SG").val(),
    "p_EmpUseExch_UT": $("#EmpUseExch_UT").val()==''?0:$("#EmpUseExch_UT").val(),
    "p_EmpUseExch_Quasi_SG": $("#EmpUseExch_Quasi_SG").val()==''?0:$("#EmpUseExch_Quasi_SG").val(),
    "p_EmpUseExch_Quasi_CG": $("#EmpUseExch_Quasi_CG").val()==''?0:$("#EmpUseExch_Quasi_CG").val(),
    "p_EmpUseExch_LB": $("#EmpUseExch_LB").val()==''?0:$("#EmpUseExch_LB").val(),
    "p_EmpUseExch_Private_Act": $("#EmpUseExch_Private_Act").val()==''?0:$("#EmpUseExch_Private_Act").val(),
    "p_EmpUseExch_Private_NonAct": $("#EmpUseExch_Private_NonAct").val()==''?0:$("#EmpUseExch_Private_NonAct").val(),
    "p_Total_EmpUseExch": $("#Total_EmpUseExch").val()==''?0:$("#Total_EmpUseExch").val(),
    
    "p_Flag" :'3',
         }
      MasterData = JSON.stringify(MasterData)
      var path = serverpath + "ES13Entry";
      securedajaxpost(path,'parsrdataES13EntryForm','comment',MasterData,'control')
      }
      
      
      function parsrdataES13EntryForm(data){
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            ES25RptEntryForm();
            
        }
        if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
            if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
            jQuery("#tablehead").html('<th>No Record Found</th>');
            $('.fa').hide();
            jQuery("#tbodyvalue").html('');
            }
            }
            else{
          if(data[0][0]){
    
                    $("#Outstanding_Prev_CG").text(data[0][0].Outstanding_Prev_CG),
                    $("#Outstanding_Prev_SG").text(data[0][0].Outstanding_Prev_SG),
                    $("#Outstanding_Prev_UT").text(data[0][0].Outstanding_Prev_UT),
                    $("#Outstanding_Prev_Quasi_SG").text(data[0][0].Outstanding_Prev_Quasi_SG),
                    $("#Outstanding_Prev_Quasi_CG").text(data[0][0].Outstanding_Prev_Quasi_CG),
                    $("#Outstanding_Prev_LB").text(data[0][0].Outstanding_Prev_LB),
                    $("#Outstanding_Prev_Private_Act").text(data[0][0].Outstanding_Prev_Private_Act),
                    $("#Outstanding_Prev_Private_NonAct").text(data[0][0].Outstanding_Prev_Private_NonAct),
                    $("#Total_Prev_Outstanding").text(data[0][0].Total_Prev_Outstanding),
                
                    $("#Transfer_Receive_CG").text(data[0][0].Transfer_Receive_CG),
                    $("#Transfer_Receive_SG").text(data[0][0].Transfer_Receive_SG),
                    $("#Transfer_Receive_UT").text(data[0][0].Transfer_Receive_UT),
                    $("#Transfer_Receive_Quasi_SG").text(data[0][0].Transfer_Receive_Quasi_SG),
                    $("#Transfer_Receive_Quasi_CG").text(data[0][0].Transfer_Receive_Quasi_CG),
                    $("#Transfer_Receive_LB").text(data[0][0].Transfer_Receive_LB),
                    $("#Transfer_Receive_Private_Act").text(data[0][0].Transfer_Receive_Private_Act),
                    $("#Transfer_Receive_Private_NonAct").text(data[0][0].Transfer_Receive_Private_NonAct),
                    $("#Total_Transfer_Receive").text(data[0][0].Total_Transfer_Receive),
                
                    $("#Notified_CG").text(data[0][0].Notified_CG),
                    $("#Notified_SG").text(data[0][0].Notified_SG),
                    $("#Notified_UT").text(data[0][0].Notified_UT),
                    $("#Notified_Quasi_SG").text(data[0][0].Notified_Quasi_SG),
                    $("#Notified_Quasi_CG").text(data[0][0].Notified_Quasi_CG),
                    $("#Notified_LB").text(data[0][0].Notified_LB),
                    $("#Notified_Private_Act").text(data[0][0].Notified_Private_Act),
                    $("#Notified_Private_NonAct").text(data[0][0].Notified_Private_NonAct),
                    $("#Total_Notified").text(data[0][0].Total_Notified),
                
                    $("#Total_1_1a_2_CG").text(data[0][0].Total_1_1a_2_CG),
                    $("#Total_1_1a_2_SG").text(data[0][0].Total_1_1a_2_SG),
                    $("#Total_1_1a_2_UT").text(data[0][0].Total_1_1a_2_UT),
                    $("#Total_1_1a_2_Quasi_SG").text(data[0][0].Total_1_1a_2_Quasi_SG),
                    $("#Total_1_1a_2_Quasi_CG").text(data[0][0].Total_1_1a_2_Quasi_CG),
                    $("#Total_1_1a_2_LB").text(data[0][0].Total_1_1a_2_LB),
                    $("#Total_1_1a_2_Private_Act").text(data[0][0].Total_1_1a_2_Private_Act),
                    $("#Total_1_1a_2_Private_NonAct").text(data[0][0].Total_1_1a_2_Private_NonAct),
                    $("#Total_Total_1_1a_2").text(data[0][0].Total_Total_1_1a_2),
                
                    $("#Filled_CG").text(data[0][0].Filled_CG),
                    $("#Filled_SG").text(data[0][0].Filled_SG),
                    $("#Filled_UT").text(data[0][0].Filled_UT),
                    $("#Filled_Quasi_SG").text(data[0][0].Filled_Quasi_SG),
                    $("#Filled_Quasi_CG").text(data[0][0].Filled_Quasi_CG),
                    $("#Filled_LB").text(data[0][0].Filled_LB),
                    $("#Filled_Private_Act").text(data[0][0].Filled_Private_Act),
                    $("#Filled_Private_NonAct").text(data[0][0].Filled_Private_NonAct),
                    $("#Total_Filled").text(data[0][0].Total_Filled),
                
                    $("#Cancelled_CG").text(data[0][0].Cancelled_CG),
                    $("#Cancelled_SG").text(data[0][0].Cancelled_SG),
                    $("#Cancelled_UT").text(data[0][0].Cancelled_UT),
                    $("#Cancelled_Quasi_SG").text(data[0][0].Cancelled_Quasi_SG),
                    $("#Cancelled_Quasi_CG").text(data[0][0].Cancelled_Quasi_CG),
                    $("#Cancelled_LB").text(data[0][0].Cancelled_LB),
                    $("#Cancelled_Private_Act").text(data[0][0].Cancelled_Private_Act),
                    $("#Cancelled_Private_NonAct").text(data[0][0].Cancelled_Private_NonAct),
                    $("#Total_Cancelled").text(data[0][0].Total_Cancelled),
                
                    $("#Transfer_CG").text(data[0][0].Transfer_CG),
                    $("#Transfer_SG").text(data[0][0].Transfer_SG),
                    $("#Transfer_UT").text(data[0][0].Transfer_UT),
                    $("#Transfer_Quasi_SG").text(data[0][0].Transfer_Quasi_SG),
                    $("#Transfer_Quasi_CG").text(data[0][0].Transfer_Quasi_CG),
                    $("#Transfer_LB").text(data[0][0].Transfer_LB),
                    $("#Transfer_Private_Act").text(data[0][0].Transfer_Private_Act),
                    $("#Transfer_Private_NonAct").text(data[0][0].Transfer_Private_NonAct),
                    $("#Total_Transfer").text(data[0][0].Total_Transfer),
                
                    $("#Outstanding_CG").text(data[0][0].Outstanding_CG),
                    $("#Outstanding_SG").text(data[0][0].Outstanding_SG),
                    $("#Outstanding_UT").text(data[0][0].Outstanding_UT),
                    $("#Outstanding_Quasi_SG").text(data[0][0].Outstanding_Quasi_SG),
                    $("#Outstanding_Quasi_CG").text(data[0][0].Outstanding_Quasi_CG),
                    $("#Outstanding_LB").text(data[0][0].Outstanding_LB),
                    $("#Outstanding_Private_Act").text(data[0][0].Outstanding_Private_Act),
                    $("#Outstanding_Private_NonAct").text(data[0][0].Outstanding_Private_NonAct),
                    $("#Total_Outstanding").text(data[0][0].Total_Outstanding),
                
                    $("#Total_4_5_5a_6_CG").text(data[0][0].Total_4_5_5a_6_CG),
                    $("#Total_4_5_5a_6_SG").text(data[0][0].Total_4_5_5a_6_SG),
                    $("#Total_4_5_5a_6_UT").text(data[0][0].Total_4_5_5a_6_UT),
                    $("#Total_4_5_5a_6_Quasi_SG").text(data[0][0].Total_4_5_5a_6_Quasi_SG),
                    $("#Total_4_5_5a_6_Quasi_CG").text(data[0][0].Total_4_5_5a_6_Quasi_CG),
                    $("#Total_4_5_5a_6_LB").text(data[0][0].Total_4_5_5a_6_LB),
                    $("#Total_4_5_5a_6_Private_Act").text(data[0][0].Total_4_5_5a_6_Private_Act),
                    $("#Total_4_5_5a_6_Private_NonAct").text(data[0][0].Total_4_5_5a_6_Private_NonAct),
                    $("#Total_Total_4_5_5a_6").text(data[0][0].Total_Total_4_5_5a_6),
                
                    $("#Notified_NotSubmitted_CG").text(data[0][0].Notified_NotSubmitted_CG),
                    $("#Notified_NotSubmitted_SG").text(data[0][0].Notified_NotSubmitted_SG),
                    $("#Notified_NotSubmitted_UT").text(data[0][0].Notified_NotSubmitted_UT),
                    $("#Notified_NotSubmitted_Quasi_SG").text(data[0][0].Notified_NotSubmitted_Quasi_SG),
                    $("#Notified_NotSubmitted_Quasi_CG").text(data[0][0].Notified_NotSubmitted_Quasi_CG),
                    $("#Notified_NotSubmitted_LB").text(data[0][0].Notified_NotSubmitted_LB),
                    $("#Notified_NotSubmitted_Private_Act").text(data[0][0].Notified_NotSubmitted_Private_Act),
                    $("#Notified_NotSubmitted_Private_NonAct").text(data[0][0].Notified_NotSubmitted_Private_NonAct),
                    $("#Total_Notified_NotSubmitted").text(data[0][0].Total_Notified_NotSubmitted),
                
                    $("#Filled_ByOtherExch_CG").text(data[0][0].Filled_ByOtherExch_CG),
                    $("#Filled_ByOtherExch_SG").text(data[0][0].Filled_ByOtherExch_SG),
                    $("#Filled_ByOtherExch_UT").text(data[0][0].Filled_ByOtherExch_UT),
                    $("#Filled_ByOtherExch_Quasi_SG").text(data[0][0].Filled_ByOtherExch_Quasi_SG),
                    $("#Filled_ByOtherExch_Quasi_CG").text(data[0][0].Filled_ByOtherExch_Quasi_CG),
                    $("#Filled_ByOtherExch_LB").text(data[0][0].Filled_ByOtherExch_LB),
                    $("#Filled_ByOtherExch_Private_Act").text(data[0][0].Filled_ByOtherExch_Private_Act),
                    $("#Filled_ByOtherExch_Private_NonAct").text(data[0][0].Filled_ByOtherExch_Private_NonAct),
                    $("#Total_Filled_ByOtherExch").text(data[0][0].Total_Filled_ByOtherExch),
                
                    $("#Filled_ForOtherExch_CG").text(data[0][0].Filled_ForOtherExch_CG),
                    $("#Filled_ForOtherExch_SG").text(data[0][0].Filled_ForOtherExch_SG),
                    $("#Filled_ForOtherExch_UT").text(data[0][0].Filled_ForOtherExch_UT),
                    $("#Filled_ForOtherExch_Quasi_SG").text(data[0][0].Filled_ForOtherExch_Quasi_SG),
                    $("#Filled_ForOtherExch_Quasi_CG").text(data[0][0].Filled_ForOtherExch_Quasi_CG),
                    $("#Filled_ForOtherExch_LB").text(data[0][0].Filled_ForOtherExch_LB),
                    $("#Filled_ForOtherExch_Private_Act").text(data[0][0].Filled_ForOtherExch_Private_Act),
                    $("#Filled_ForOtherExch_Private_NonAct").text(data[0][0].Filled_ForOtherExch_Private_NonAct),
                    $("#Total_Filled_ForOtherExch").text(data[0][0].Total_Filled_ForOtherExch),
                
                    $("#EmpUseExch_CG").text(data[0][0].EmpUseExch_CG),
                    $("#EmpUseExch_SG").text(data[0][0].EmpUseExch_SG),
                    $("#EmpUseExch_UT").text(data[0][0].EmpUseExch_UT),
                    $("#EmpUseExch_Quasi_SG").text(data[0][0].EmpUseExch_Quasi_SG),
                    $("#EmpUseExch_Quasi_CG").text(data[0][0].EmpUseExch_Quasi_CG),
                    $("#EmpUseExch_LB").text(data[0][0].EmpUseExch_LB),
                    $("#EmpUseExch_Private_Act").text(data[0][0].EmpUseExch_Private_Act),
                    $("#EmpUseExch_Private_NonAct").text(data[0][0].EmpUseExch_Private_NonAct),
                    $("#Total_EmpUseExch").text(data[0][0].Total_EmpUseExch)
          }
          else{
           $(' #tbodyvalue, #tablehead').empty();
           $('#tablehead').html('<th>No Records Found</th>')
          }
        
        }
     
    
         }
       function FetchES12Entry(){
       
        var FromDt;
        var ToDt;
        if($('#criteriaddl').val()=='1'){
            FromDt=`${$('#frmYear').val()}-01-01`;
            ToDt=`${$('#frmYear').val()}-06-30`;
        }
        else if($('#criteriaddl').val()=='2'){
            FromDt=`${$('#frmYear').val()}-07-01`;
            ToDt=`${$('#frmYear').val()}-12-31`;
        }
        var MasterData = {
            
            "p_Ex_id":$('#printreportproformaexchange').val(),
            "p_FromDt":FromDt,
            "p_ToDt":ToDt,
            "p_NCO":"0",        
            "p_Women_LR":"0",           
            "p_SC_LR":"0",          
            "p_ST_LR":"0",          
            "p_OBC_LR":"0",          
            "p_Disabled_LR":"0",          
            "p_Total_LR":"0",
           
            "p_Women_Notify":"0",           
            "p_SC_Notify":"0",          
            "p_ST_Notify":"0",          
            "p_OBC_Notify":"0",          
            "p_Disabled_Notify":"0",          
            "p_Total_Notify":"0",
    
            "p_Women_Filled":"0",           
            "p_SC_Filled":"0",          
            "p_ST_Filled":"0",          
            "p_OBC_Filled":"0",          
            "p_Disabled_Filled":"0",          
            "p_Total_Filled":"0",
    
            "p_Women_Cancelled":"0",           
            "p_SC_Cancelled":"0",          
            "p_ST_Cancelled":"0",          
            "p_OBC_Cancelled":"0",          
            "p_Disabled_Cancelled":"0",          
            "p_Total_Cancelled":"0",
    
            "p_Women_Outstanding":"0",           
            "p_SC_Outstanding":"0",          
            "p_ST_Outstanding":"0",          
            "p_OBC_Outstanding":"0",          
            "p_Disabled_Outstanding":"0",          
            "p_Total_Outstanding":"0",
    
            "p_Verify_YN":'0',         
            "p_Flag":'5',        
            "p_ES12_Id":'0'
        
        }     
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "ES12_RptEntry";
        securedajaxpost(path,'parsedataES12Rpt','comment',MasterData,'control')   
    }
    
    function parsedataES12Rpt(data){
        data = JSON.parse(data);
        var data1=data[0];
        var appenddata="";
        if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
            if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
            jQuery("#tablehead").html('<th>No Record Found</th>');
            $('.fa').hide();
            jQuery("#tbodyvalue").html('');
            }
            }
            else{
            if(data1.length>0){
                
                for (var i = 0; i < data1.length; i++) {
                    appenddata+=`<tr class='addrow modify' id="modify_${data1[i].ES12_Id}">
                    
                    <td>${data1[i].NCO}</td>
                    <td>${data1[i].Women_LR}</td>
                    <td>${data1[i].SC_LR}</td>
                    <td>${data1[i].ST_LR}</td>
                    <td>${data1[i].OBC_LR}</td>
                    <td>${data1[i].Disabled_LR}</td>
                    <td>${data1[i].Total_LR}</td>
                    <td>${data1[i].Women_Notify}</td>
                    <td>${data1[i].SC_Notify}</td>
                    <td>${data1[i].ST_Notify}</td>
                    <td>${data1[i].OBC_Notify}</td>
                    <td>${data1[i].Disabled_Notify}</td>
                    <td>${data1[i].Total_Notify}</td>
                    <td>${data1[i].Women_Filled}</td>
                    <td>${data1[i].SC_Filled}</td>
                    <td>${data1[i].ST_Filled}</td>
                    <td>${data1[i].OBC_Filled}</td>
                    <td>${data1[i].Disabled_Filled}</td>
                    <td>${data1[i].Total_Filled}</td>
                    <td>${data1[i].Women_Cancelled}</td>
                    <td>${data1[i].SC_Cancelled}</td>
                    <td>${data1[i].ST_Cancelled}</td>
                    <td>${data1[i].OBC_Cancelled}</td>
                    <td>${data1[i].Disabled_Cancelled}</td>
                    <td>${data1[i].Total_Cancelled}</td>
                    <td>${data1[i].Women_Outstanding}</td>
                    <td>${data1[i].SC_Outstanding}</td>
                    <td>${data1[i].ST_Outstanding}</td>
                    <td>${data1[i].OBC_Outstanding}</td>
                    <td>${data1[i].Disabled_Outstanding}</td>
                    <td>${data1[i].Total_Outstanding}</td>
                    
                    </tr>`;
        }
        
        $('#tbodyvalue').html(appenddata);
            }
    else{
       $(' #tbodyvalue, #tablehead').empty();
       $('#tablehead').html('<th>No Records Found</th>')
    }
    }
}
    function ES14Rpt(){
       
            var todate = ''+$('#frmYear').val()+'-12-31'
    
    
       
            var MasterData = {
      
                "p_Ex_Id": $('#printreportproformaexchange').val(),  
                "p_ToDt":  todate ,  
                "p_Edu_id":0, 
                "p_Upto19":0,
                "p_Age_20_29":0, 
                "p_Age_30_39":0, 
                "p_Age_40_49":0,  
                "p_Age_50_59":0,  
                "p_Age_60_above":0,  
               "p_Flag":3, 
            }
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "ES14Entry";
            securedajaxpost(path,'parsrdataES14Entry','comment',MasterData,'control')
      
        
        }
    
    function parsrdataES14Entry(data){
        data = JSON.parse(data)
       
     if(data[0].length){
        for (var i = 0; i < data[0].length; i++) {
    $('#upto19'+data[0][i].Edu_id+'').text(data[0][i].Upto19);
    $('#Age_20_29'+data[0][i].Edu_id+'').text(data[0][i].Age_20_29);
    $('#Age_30_39'+data[0][i].Edu_id+'').text(data[0][i].Age_30_39);
    $('#Age_40_49'+data[0][i].Edu_id+'').text(data[0][i].Age_40_49);
    $('#Age_50_59'+data[0][i].Edu_id+'').text(data[0][i].Age_50_59);
    $('#Age_60_above'+data[0][i].Edu_id+'').text(data[0][i].Age_60_above);
    $('#total'+data[0][i].Edu_id+'').text(data[0][i].total);
        }
    }
        else{
           $(' #tbodyvalue, #tablehead').empty();
           $('#tablehead').html('<th>No Records Found</th>')
        }
    }
    function FillEduationalLevel(funct,control) {
        var path =  serverpath + "Edu_with_Gender"
        securedajaxget(path,funct,'comment',control);
      }
      
      function parsedataFillEduationalLevel(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillEduationalLevel('parsedataFillEduationalLevel','tbodyvalue');
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                var appenddata = ''
                var tablehead="<tr><th  style='background: #716aca;border-right: inset;'>S. No</th><th  style='background: #716aca;border-right: inset;'>Eduational Level</th><th  style='background: #716aca;border-right: inset;'>Sex</th><th  style='background: #716aca;border-right: inset;'>Up to 19</th><th  style='background: #716aca;border-right: inset;'>20-29</th><th  style='background: #716aca;border-right: inset;'>30-39</th><th  style='background: #716aca;border-right: inset;'>40-49</th><th  style='background: #716aca;border-right: inset;'>50-59</th><th  style='background: #716aca;border-right: inset;'>60 and above</th><th  style='background: #716aca;'>Total</th></tr>";
                $("#tablehead").html(tablehead);
    
                for (var i = 0; i < data1.length; i++) {
                    if(data1[i].Gender=='M'){
                        var gender= 'Male'
                        var EduName= data1[i].Edu_Name
                       }
                        else if(data1[i].Gender=='F'){
                       var gender='Female'
                       var EduName=''
                          }
                          else if(data1[i].Gender=='B'){
                              var gender='Both'
                              var EduName=''
                                 }
    
                  appenddata+= `<tr class='i'>
                  <td><label class='eduid'>`+data1[i].Edu_id+`</label></td>
                  <td>`+EduName+`</td>
              
                  <td > `+gender+`
                  </td>
                  <td    id='upto19`+data1[i].Edu_id+`' >
                  </td>
                  <td     id='Age_20_29`+data1[i].Edu_id+`' >
                  </td>
                  <td  id='Age_30_39`+data1[i].Edu_id+`'    >
                  </td>
                  <td    id='Age_40_49`+data1[i].Edu_id+`' >
                  </td>
                  <td   id='Age_50_59`+data1[i].Edu_id+`'  >
                  </td>
                  <td  id='Age_60_above`+data1[i].Edu_id+`'   >
                  </td>
                  <td  id="total`+data1[i].Edu_id+`"  >
                  </td>
              </tr>`
                 }
                 $("#tbodyvalue").append(appenddata);
                 ES14Rpt();
            }
                  
      }
    
    
      function ES23RptEntryForm(){
        var FromDt='';
        var ToDt='';
     
    if($('#criteriaddl').val()=='1'){
        FromDt=`${$('#frmYear').val()}-01-01`;
        ToDt=`${$('#frmYear').val()}-06-30`;
       printonTblheadYear=`30/06/${$('#frmYear').val()}`;
    }
    else if($('#criteriaddl').val()=='2'){
        FromDt=`${$('#frmYear').val()}-07-01`;
        ToDt=`${$('#frmYear').val()}-12-31`;
        printonTblheadYear=`${$('#frmYear').val()}`;
    }
    $('#tblheadYear').text(printonTblheadYear);
     var MasterData = {
            
    "p_Ex_Id":$('#printreportproformaexchange').val(),
    "p_FromDt":FromDt,
    "p_ToDt":ToDt,
    "p_Registration_M": $("#Registration_M").val()==''?0:$("#Registration_M").val(),
    "p_PlacesJs_M": $("#PlacesJs_M").val()==''?0:$("#PlacesJs_M").val(),
    "p_Registration_Removed_M": $("#Registration_Removed_M").val()==''?0:$("#Registration_Removed_M").val(),
    "p_LR_M": $("#LR_M").val()==''?0:$("#LR_M").val(),
    "p_Submissions_M": $("#Submissions_M").val()==''?0:$("#Submissions_M").val(),
    "p_LR_Prev_M": $("#LR_Prev_M").val()==''?0:$("#LR_Prev_M").val(),
    
    "p_Registration_C": $("#Registration_C").val()==''?0:$("#Registration_C").val(),
    "p_PlacesJs_C": $("#PlacesJs_C").val()==''?0:$("#PlacesJs_C").val(),
    "p_Registration_Removed_C": $("#Registration_Removed_C").val()==''?0:$("#Registration_Removed_C").val(),
    "p_LR_C": $("#LR_C").val()==''?0:$("#LR_C").val(),
    "p_Submissions_C": $("#Submissions_C").val()==''?0:$("#Submissions_C").val(),
    "p_LR_Prev_C": $("#LR_Prev_C").val()==''?0:$("#LR_Prev_C").val(),
    
    "p_Registration_S": $("#Registration_S").val()==''?0:$("#Registration_S").val(),
    "p_PlacesJs_S": $("#PlacesJs_S").val()==''?0:$("#PlacesJs_S").val(),
    "p_Registration_Removed_S": $("#Registration_Removed_S").val()==''?0:$("#Registration_Removed_S").val(),
    "p_LR_S": $("#LR_S").val()==''?0:$("#LR_S").val(),
    "p_Submissions_S": $("#Submissions_S").val()==''?0:$("#Submissions_S").val(),
    "p_LR_Prev_S": $("#LR_Prev_S").val()==''?0:$("#LR_Prev_S").val(),
    
    "p_Registration_B": $("#Registration_B").val()==''?0:$("#Registration_B").val(),
    "p_PlacesJs_B": $("#PlacesJs_B").val()==''?0:$("#PlacesJs_B").val(),
    "p_Registration_Removed_B": $("#Registration_Removed_B ").val()==''?0:$("#Registration_Removed_B").val(),
    "p_LR_B": $("#LR_B").val()==''?0:$("#LR_B").val(),
    "p_Submissions_B": $("#Submissions_B").val()==''?0:$("#Submissions_B").val(),
    "p_LR_Prev_B": $("#LR_Prev_B").val()==''?0:$("#LR_Prev_B").val(),
    
    "p_Registration_Z": $("#Registration_Z").val()==''?0:$("#Registration_Z").val(),
    "p_PlacesJs_Z": $("#PlacesJs_Z").val()==''?0:$("#PlacesJs_Z").val(),
    "p_Registration_Removed_Z": $("#Registration_Removed_Z").val()==''?0:$("#Registration_Removed_Z").val(),
    "p_LR_Z": $("#LR_Z").val()==''?0:$("#LR_Z").val(),
    "p_Submissions_Z": $("#Submissions_Z").val()==''?0:$("#Submissions_Z").val(),
    "p_LR_Prev_Z": $("#LR_Prev_Z").val()==''?0:$("#LR_Prev_Z").val(),
    
    "p_Registration_Total": $("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
    "p_PlacesJs_Total": $("#PlacesJs_Total").val()==''?0:$("#PlacesJs_Total").val(),
    "p_Registration_Removed_Total": $("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
    "p_LR_Total": $("#LR_Total").val()==''?0:$("#LR_Total").val(),
    "p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
    "p_LR_Prev_Total": $("#LR_Prev_Total").val()==''?0:$("#LR_Prev_Total").val(),
    
    "p_Flag" :'3',
         }
      MasterData = JSON.stringify(MasterData)
      var path = serverpath + "es23Entry";
      securedajaxpost(path,'parsrdataES23RptEntryForm','comment',MasterData,'control')
      }
      
      
      function parsrdataES23RptEntryForm(data){
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            ES23RptEntryForm();
            
        }
        if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
            if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
            jQuery("#tablehead").html('<th>No Record Found</th>');
            $('.fa').hide();
            jQuery("#tbodyvalue").html('');
            }
            }
            else{
        if(data[0][0]){
          
        $("#Registration_M").text(data[0][0].Registration_M)
        $("#LR_Prev_M").text(data[0][0].LR_Prev_M)
        $("#PlacesJs_M").text(data[0][0].PlacesJs_M) 
        $("#Registration_Removed_M").text(data[0][0].Registration_Removed_M)
        $("#LR_M").text(data[0][0].LR_M)
        $("#Submissions_M").text(data[0][0].Submissions_M)
    
        $("#Registration_C").text(data[0][0].Registration_C)
        $("#LR_Prev_C").text(data[0][0].LR_Prev_C)
        $("#PlacesJs_C").text(data[0][0].PlacesJs_C) 
        $("#Registration_Removed_C").text(data[0][0].Registration_Removed_C)
        $("#LR_C").text(data[0][0].LR_C)
        $("#Submissions_C").text(data[0][0].Submissions_C)
    
        $("#Registration_S").text(data[0][0].Registration_S)
        $("#LR_Prev_S").text(data[0][0].LR_Prev_S)
        $("#PlacesJs_S").text(data[0][0].PlacesJs_S) 
        $("#Registration_Removed_S").text(data[0][0].Registration_Removed_S)
        $("#LR_S").text(data[0][0].LR_S)
        $("#Submissions_S").text(data[0][0].Submissions_S)
    
        $("#Registration_B").text(data[0][0].Registration_B)
        $("#LR_Prev_B").text(data[0][0].LR_Prev_B)
        $("#PlacesJs_B").text(data[0][0].PlacesJs_B) 
        $("#Registration_Removed_B").text(data[0][0].Registration_Removed_B)
        $("#LR_B").text(data[0][0].LR_B)
        $("#Submissions_B").text(data[0][0].Submissions_B)
    
        $("#Registration_Z").text(data[0][0].Registration_Z)
        $("#LR_Prev_Z").text(data[0][0].LR_Prev_Z)
        $("#PlacesJs_Z").text(data[0][0].PlacesJs_Z) 
        $("#Registration_Removed_Z").text(data[0][0].Registration_Removed_Z)
        $("#LR_Z").text(data[0][0].LR_Z)
        $("#Submissions_Z").text(data[0][0].Submissions_Z)
    
        $("#Registration_Total").text(data[0][0].Registration_Total)
        $("#LR_Prev_Total").text(data[0][0].LR_Prev_Total)
        $("#PlacesJs_Total").text(data[0][0].PlacesJs_Total) 
        $("#Registration_Removed_Total").text(data[0][0].Registration_Removed_Total)
        $("#LR_Total").text(data[0][0].LR_Total)
        $("#Submissions_Total").text(data[0][0].Submissions_Total)
           
    
        }
        else{
           $(' #tbodyvalue, #tablehead').empty();
           $('#tablehead').html('<th>No Records Found</th>')
        }
    
    
         }
    
        }
         
    function ES24Part1RptEntryForm(){
        var FromDt='';
        var ToDt='';
    if($('#criteriaddl').val()=='1'){
        FromDt=`${$('#frmYear').val()}-01-01`;
        ToDt=`${$('#frmYear').val()}-06-30`;
    }
    else if($('#criteriaddl').val()=='2'){
        FromDt=`${$('#frmYear').val()}-07-01`;
        ToDt=`${$('#frmYear').val()}-12-31`;
    }   var MasterData = {
            
    "p_Ex_Id":$('#printreportproformaexchange').val(),
    "p_FromDt":FromDt,
    "p_ToDt":ToDt,
    "p_Registration_SC": $("#Registration_SC").val()==''?0:$("#Registration_SC").val(),
    "p_PlacesJs_CG_SC": $("#PlacesJs_CG_SC").val()==''?0:$("#PlacesJs_CG_SC").val(),
    "p_PlacesJs_UT_SC": $("#PlacesJs_UT_SC").val()==''?0:$("#PlacesJs_UT_SC").val(),
    "p_PlacesJs_SG_SC": $("#PlacesJs_SG_SC").val()==''?0:$("#PlacesJs_SG_SC").val(),
    "p_PlacesJs_QG_SC": $("#PlacesJs_QG_SC").val()==''?0:$("#PlacesJs_QG_SC").val(),
    "p_PlacesJs_LB_SC": $("#PlacesJs_LB_SC").val()==''?0:$("#PlacesJs_LB_SC").val(),
    "p_PlacesJs_Private_SC": $("#PlacesJs_Private_SC").val()==''?0:$("#PlacesJs_Private_SC").val(),
    "p_PlacesJs_Total_SC": $("#PlacesJs_Total_SC").val()==''?0:$("#PlacesJs_Total_SC").val(),
    "p_Registration_Removed_SC": $("#Registration_Removed_SC ").val()==''?0:$("#Registration_Removed_SC ").val(),
    "p_LR_SC": $("#LR_SC").val()==''?0:$("#LR_SC").val(),
    "p_Submissions_SC": $("#Submissions_SC").val()==''?0:$("#Submissions_SC").val(),
    "p_LR_Prev_1_SC": $("#LR_Prev_1_SC").val()==''?0:$("#LR_Prev_1_SC").val(),
    "p_Registration_ST": $("#Registration_ST").val()==''?0:$("#Registration_ST").val(),
    "p_PlacesJs_CG_ST": $("#PlacesJs_CG_ST").val()==''?0:$("#PlacesJs_CG_ST").val(),
    "p_PlacesJs_UT_ST": $("#PlacesJs_UT_ST").val()==''?0:$("#PlacesJs_UT_ST").val(),
    "p_PlacesJs_SG_ST": $("#PlacesJs_SG_ST").val()==''?0:$("#PlacesJs_SG_ST").val(),
    "p_PlacesJs_QG_ST": $("#PlacesJs_QG_ST").val()==''?0:$("#PlacesJs_QG_ST").val(),
    "p_PlacesJs_LB_ST": $("#PlacesJs_LB_ST").val()==''?0:$("#PlacesJs_LB_ST").val(),
    "p_PlacesJs_Private_ST": $("#PlacesJs_Private_ST").val()==''?0:$("#PlacesJs_Private_ST").val(),
    "p_PlacesJs_Total_ST": $("#PlacesJs_Total_ST").val()==''?0:$("#PlacesJs_Total_ST").val(),
    "p_Registration_Removed_ST": $("#Registration_Removed_ST ").val()==''?0:$("#Registration_Removed_ST ").val(),
    "p_LR_ST": $("#LR_ST").val()==''?0:$("#LR_ST").val(),
    "p_Submissions_ST": $("#Submissions_ST").val()==''?0:$("#Submissions_ST").val(),
    "p_LR_Prev_1_ST": $("#LR_Prev_1_ST").val()==''?0:$("#LR_Prev_1_ST").val(),
    "p_Registration_OBC": $("#Registration_OBC").val()==''?0:$("#Registration_OBC").val(),
    "p_PlacesJs_CG_OBC": $("#PlacesJs_CG_OBC").val()==''?0:$("#PlacesJs_CG_OBC").val(),
    "p_PlacesJs_UT_OBC": $("#PlacesJs_UT_OBC").val()==''?0:$("#PlacesJs_UT_OBC").val(),
    "p_PlacesJs_SG_OBC": $("#PlacesJs_SG_OBC").val()==''?0:$("#PlacesJs_SG_OBC").val(),
    "p_PlacesJs_QG_OBC": $("#PlacesJs_QG_OBC").val()==''?0:$("#PlacesJs_QG_OBC").val(),
    "p_PlacesJs_LB_OBC": $("#PlacesJs_LB_OBC").val()==''?0:$("#PlacesJs_LB_OBC").val(),
    "p_PlacesJs_Private_OBC": $("#PlacesJs_Private_OBC").val()==''?0:$("#PlacesJs_Private_OBC").val(),
    "p_PlacesJs_Total_OBC": $("#PlacesJs_Total_OBC").val()==''?0:$("#PlacesJs_Total_OBC").val(),
    "p_Registration_Removed_OBC": $("#Registration_Removed_OBC ").val()==''?0:$("#Registration_Removed_OBC ").val(),
    "p_LR_OBC": $("#LR_OBC").val()==''?0:$("#LR_OBC").val(),
    "p_Submissions_OBC": $("#Submissions_OBC").val()==''?0:$("#Submissions_OBC").val(),
    "p_LR_Prev_1_OBC": $("#LR_Prev_1_OBC").val()==''?0:$("#LR_Prev_1_OBC").val(),
    "p_Flag" :'3',
         }
      MasterData = JSON.stringify(MasterData)
      var path = serverpath + "es24p1RptEntry";
      securedajaxpost(path,'parsrdataES24part1RptEntryForm','comment',MasterData,'control')
      }
      
      
      function parsrdataES24part1RptEntryForm(data){
        data = JSON.parse(data)
      
          if(data[0][0]){
            if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
                if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
                jQuery("#tablehead").html('<th>No Record Found</th>');
                $('.fa').hide();
                jQuery("#tbodyvalue").html('');
                }
                }
                else{
                     $("#Registration_SC").text(data[0][0].Registration_SC),
        $("#Registration_ST").text(data[0][0].Registration_ST),
        $("#Registration_OBC").text(data[0][0].Registration_OBC)
        $("#LR_Prev_1_SC").text(data[0][0].LR_Prev_1_SC)
        $("#LR_Prev_1_ST").text(data[0][0].LR_Prev_1_ST)
        $("#LR_Prev_1_OBC").text(data[0][0].LR_Prev_1_OBC)
        $("#PlacesJs_CG_SC").text(data[0][0].PlacesJs_CG_SC)
        $("#PlacesJs_CG_ST").text(data[0][0].PlacesJs_CG_ST)
        $("#PlacesJs_CG_OBC").text(data[0][0].PlacesJs_CG_OBC)  
        $("#PlacesJs_UT_SC").text(data[0][0].PlacesJs_UT_SC)
        $("#PlacesJs_UT_ST").text(data[0][0].PlacesJs_UT_ST)
        $("#PlacesJs_UT_OBC").text(data[0][0].PlacesJs_UT_OBC)
        $("#PlacesJs_SG_SC").text(data[0][0].PlacesJs_SG_SC)
        $("#PlacesJs_SG_ST").text(data[0][0].PlacesJs_SG_ST)
        $("#PlacesJs_SG_OBC").text(data[0][0].PlacesJs_SG_OBC)
        $("#PlacesJs_QG_SC").text(data[0][0].PlacesJs_QG_SC)
        $("#PlacesJs_QG_ST").text(data[0][0].PlacesJs_QG_ST)
        $("#PlacesJs_QG_OBC").text(data[0][0].PlacesJs_QG_OBC)
        $("#PlacesJs_LB_SC").text(data[0][0].PlacesJs_LB_SC)
        $("#PlacesJs_LB_ST").text(data[0][0].PlacesJs_LB_ST)
        $("#PlacesJs_LB_OBC").text(data[0][0].PlacesJs_LB_OBC)
        $("#PlacesJs_Private_SC").text(data[0][0].PlacesJs_Private_SC)
        $("#PlacesJs_Private_ST").text(data[0][0].PlacesJs_Private_ST)
        $("#PlacesJs_Private_OBC").text(data[0][0].PlacesJs_Private_OBC)
        $("#Registration_Removed_SC").text(data[0][0].Registration_Removed_SC)
        $("#Registration_Removed_ST").text(data[0][0].Registration_Removed_ST)
        $("#Registration_Removed_OBC").text(data[0][0].Registration_Removed_OBC)
        $("#LR_SC").text(data[0][0].LR_SC)
        $("#LR_ST").text(data[0][0].LR_ST)
        $("#LR_OBC").text(data[0][0].LR_OBC)
        $("#Submissions_SC").text(data[0][0].Submissions_SC)
        $("#Submissions_ST").text(data[0][0].Submissions_ST)
        $("#Submissions_OBC").text(data[0][0].Submissions_OBC)
          }
        }
          else{
           $(' #tbodyvalue, #tablehead').empty();
           $('#tablehead').html('<th>No Records Found</th>')
          }
       
          
         }  
         
         function fetchES24Part2(){
             var FromDt;
             var ToDt;
            if($('#criteriaddl').val()='1'){
                FromDt=`${$('#frmYear').val()}-01-01`;
                ToDt=`${$('#frmYear').val()}-06-30`;
            }
            else if($('#criteriaddl').val()=='2'){
                FromDt=`${$('#frmYear').val()}-07-01`;
                ToDt=`${$('#frmYear').val()}-12-31`;
            }
            var MasterData = {
                
                "p_Ex_Id":$('#printreportproformaexchange').val(),
                "p_FromDt":FromDt,
                "p_ToDt":ToDt,
                "p_ES24_II_Id":'0',
                "p_Outstanding_Prev_SC":"0",
                "p_Outstanding_Prev_ST":"0",
                "p_Outstanding_Prev_OBC":"0",
                "p_Notified_SC":"0",
                "p_Notified_ST":"0",
                "p_Notified_OBC":"0",
                "p_Filled_SC":"0",
                "p_Filled_ST":"0",
                "p_Filled_OBC":"0",
            
                "p_Cancelled_NA_SC":"0",
                "p_Cancelled_NA_ST":"0",
                "p_Cancelled_NA_OBC":"0",
            
                "p_Cancelled_Other_SC":"0",
                "p_Cancelled_Other_ST":"0",
                "p_Cancelled_Other_OBC":"0",
            
                "p_Outstanding_SC":"0",
                "p_Outstanding_ST":"0",
                "p_Outstanding_OBC":"0",
            
                "p_Flag":'3'
          }
          MasterData = JSON.stringify(MasterData)
            var path = serverpath + "es24p2RptEntry";
            securedajaxpost(path,'parsedatafetchES24Part2','comment',MasterData,'control') 
        }
          function parsedatafetchES24Part2(data){  
              data = JSON.parse(data);
              var appendata='';
              var data1 = data[0];
          
           if(data1[0].Verify_YN==0||!data1.length){
            $('#tbodyvalue, #tablehead').empty();
            $('#tablehead').html('<th>No Records Found</th>')
           }
           else{
                for (var i = 0; i < data1.length; i++) {
                    

                  appendata+=`	<tr class="tbl_row" id="${data1[i].Flag}_${i}">
                  <td class="ES24_II_Id">${data1[i].ES24_II_Id}</td>
                  <td>${data1[i].Type_Of_Establishment}</td>
          
                  <td>${data1[i].Outstanding_Prev_SC}
                  </td>
                  <td >${data1[i].Outstanding_Prev_ST}
                  </td>
                  <td>${data1[i].Outstanding_Prev_OBC} 
                  </td>
                  <td >${data1[i].Notified_SC} 
                  </td>
                  <td>${data1[i].Notified_ST} 
                  </td>
                  <td >${data1[i].Notified_OBC} 
                  </td>
                  <td >${data1[i].Filled_SC} 
                  </td>
                      <td >${data1[i].Filled_ST} 
                  </td>
                  <td>${data1[i].Filled_OBC} 
                  </td>
                  <td >${data1[i].Cancelled_NA_SC} 
                  </td>
                  <td>${data1[i].Cancelled_NA_ST} 
                  </td>
                  <td >${data1[i].Cancelled_NA_OBC} 
                  </td>
                  <td>${data1[i].Cancelled_Other_SC} 
                  </td>
                  <td >${data1[i].Cancelled_Other_ST} 
                  </td>
                  <td >${data1[i].Cancelled_Other_OBC} 
                  </td>	
                  <td >${data1[i].Outstanding_SC} 
                  </td>
                  <td >${data1[i].Outstanding_ST} 
                  </td>
                  <td>${data1[i].Outstanding_OBC} 
                  </td>
             
          </td>
              </tr>`
                }
            }
               
    
                    $('#tbodyvalue').html(appendata);
              
              
              
        
          }
    
          function ES25RptEntryForm(){
            var FromDt='';
            var ToDt='';
        if($('#criteriaddl').val()=='1'){
            FromDt=`${$('#frmYear').val()}-01-01`;
            ToDt=`${$('#frmYear').val()}-06-30`;
        }
        else if($('#criteriaddl').val()=='2'){
            FromDt=`${$('#frmYear').val()}-07-01`;
            ToDt=`${$('#frmYear').val()}-12-31`;
        }
          var MasterData = {
                
        "p_Ex_Id":$('#printreportproformaexchange').val(),
        "p_FromDt":FromDt,
        "p_ToDt":ToDt,
        "p_Registration_Blind": $("#Registration_Blind").val()==''?0:$("#Registration_Blind").val(),
        "p_PlacesJs_CG_Blind": $("#PlacesJs_CG_Blind").val()==''?0:$("#PlacesJs_CG_Blind").val(),
        "p_PlacesJs_UT_Blind": $("#PlacesJs_UT_Blind").val()==''?0:$("#PlacesJs_UT_Blind").val(),
        "p_PlacesJs_SG_Blind": $("#PlacesJs_SG_Blind").val()==''?0:$("#PlacesJs_SG_Blind").val(),
        "p_PlacesJs_QG_Blind": $("#PlacesJs_QG_Blind").val()==''?0:$("#PlacesJs_QG_Blind").val(),
        "p_PlacesJs_LB_Blind": $("#PlacesJs_LB_Blind").val()==''?0:$("#PlacesJs_LB_Blind").val(),
        "p_PlacesJs_Private_Blind": $("#PlacesJs_Private_Blind").val()==''?0:$("#PlacesJs_Private_Blind").val(),
        "p_PlacesJs_Total_Blind": $("#PlacesJs_Total_Blind").val()==''?0:$("#PlacesJs_Total_Blind").val(),
        "p_Registration_Removed_Blind": $("#Registration_Removed_Blind ").val()==''?0:$("#Registration_Removed_Blind ").val(),
        "p_LR_Blind": $("#LR_Blind").val()==''?0:$("#LR_Blind").val(),
        "p_Submissions_Blind": $("#Submissions_Blind").val()==''?0:$("#Submissions_Blind").val(),
        "p_LR_Prev_1_Blind": $("#LR_Prev_1_Blind").val()==''?0:$("#LR_Prev_1_Blind").val(),
        "p_Registration_Deaf": $("#Registration_Deaf").val()==''?0:$("#Registration_Deaf").val(),
        "p_PlacesJs_CG_Deaf": $("#PlacesJs_CG_Deaf").val()==''?0:$("#PlacesJs_CG_Deaf").val(),
        "p_PlacesJs_UT_Deaf": $("#PlacesJs_UT_Deaf").val()==''?0:$("#PlacesJs_UT_Deaf").val(),
        "p_PlacesJs_SG_Deaf":$("#PlacesJs_SG_Deaf").val()==''?0:$("#PlacesJs_SG_Deaf").val(),
        "p_PlacesJs_QG_Deaf":$("#PlacesJs_QG_Deaf").val()==''?0:$("#PlacesJs_QG_Deaf").val(),
        "p_PlacesJs_LB_Deaf":$("#PlacesJs_LB_Deaf").val()==''?0:$("#PlacesJs_LB_Deaf").val(),
        "p_PlacesJs_Private_Deaf":$("#PlacesJs_Private_Deaf").val()==''?0:$("#PlacesJs_Private_Deaf").val(),
        "p_PlacesJs_Total_Deaf":$("#PlacesJs_Total_Deaf").val()==''?0:$("#PlacesJs_Total_Deaf").val(),
        "p_Registration_Removed_Deaf":$("#Registration_Removed_Deaf").val()==''?0:$("#Registration_Removed_Deaf").val(),
        "p_LR_Deaf":$("#LR_Deaf").val()==''?0:$("#LR_Deaf").val(),
        "p_Submissions_Deaf":$("#Submissions_Deaf").val()==''?0:$("#Submissions_Deaf").val(),
        "p_LR_Prev_1_Deaf":$("#LR_Prev_1_Deaf").val()==''?0:$("#LR_Prev_1_Deaf").val(),
        "p_Registration_Orthopaedics": $("#Registration_Orthopaedics").val()==''?0:$("#Registration_Orthopaedics").val(),
        "p_PlacesJs_CG_Orthopaedics": $("#PlacesJs_CG_Orthopaedics").val()==''?0:$("#PlacesJs_CG_Orthopaedics").val(),
        "p_PlacesJs_UT_Orthopaedics":$("#PlacesJs_UT_Orthopaedics").val()==''?0:$("#PlacesJs_UT_Orthopaedics").val(),
        "p_PlacesJs_SG_Orthopaedics":$("#PlacesJs_SG_Orthopaedics").val()==''?0:$("#PlacesJs_SG_Orthopaedics").val(),
        "p_PlacesJs_QG_Orthopaedics":$("#PlacesJs_QG_Orthopaedics").val()==''?0:$("#PlacesJs_QG_Orthopaedics").val(),
        "p_PlacesJs_LB_Orthopaedics":$("#PlacesJs_LB_Orthopaedics").val()==''?0:$("#PlacesJs_LB_Orthopaedics").val(),
        "p_PlacesJs_Private_Orthopaedics":$("#PlacesJs_Private_Orthopaedics").val()==''?0:$("#PlacesJs_Private_Orthopaedics").val(),
        "p_PlacesJs_Total_Orthopaedics":$("#PlacesJs_Total_Orthopaedics").val()==''?0:$("#PlacesJs_Total_Orthopaedics").val(),
        "p_Registration_Removed_Orthopaedics":$("#Registration_Removed_Orthopaedics").val()==''?0:$("#Registration_Removed_Orthopaedics").val(),
        "p_LR_Orthopaedics":$("#LR_Orthopaedics").val()==''?0:$("#LR_Orthopaedics").val(),
        "p_Submissions_Orthopaedics":$("#Submissions_Orthopaedics").val()==''?0:$("#Submissions_Orthopaedics").val(),
        "p_LR_Prev_1_Orthopaedics":$("#LR_Prev_1_Orthopaedics").val()==''?0:$("#LR_Prev_1_Orthopaedics").val(),
        "p_Registration_Respiratory":$("#Registration_Respiratory").val()==''?0:$("#Registration_Respiratory").val(),
        "p_PlacesJs_CG_Respiratory":$("#PlacesJs_CG_Respiratory").val()==''?0:$("#PlacesJs_CG_Respiratory").val(),
        "p_PlacesJs_UT_Respiratory":$("#PlacesJs_UT_Respiratory").val()==''?0:$("#PlacesJs_UT_Respiratory").val(),
        "p_PlacesJs_SG_Respiratory":$("#PlacesJs_SG_Respiratory").val()==''?0:$("#PlacesJs_SG_Respiratory").val(),
        "p_PlacesJs_QG_Respiratory":$("#PlacesJs_QG_Respiratory").val()==''?0:$("#PlacesJs_QG_Respiratory").val(),
        "p_PlacesJs_LB_Respiratory":$("#PlacesJs_LB_Respiratory").val()==''?0:$("#PlacesJs_LB_Respiratory").val(),
        "p_PlacesJs_Private_Respiratory":$("#PlacesJs_Private_Respiratory").val()==''?0:$("#PlacesJs_Private_Respiratory").val(),
        "p_PlacesJs_Total_Respiratory":$("#PlacesJs_Total_Respiratory").val()==''?0:$("#PlacesJs_Total_Respiratory").val(),
        "p_Registration_Removed_Respiratory":$("#Registration_Removed_Respiratory").val()==''?0:$("#Registration_Removed_Respiratory").val(),
        "p_LR_Respiratory":$("#LR_Respiratory").val()==''?0:$("#LR_Respiratory").val(),
        "p_Submissions_Respiratory":$("#Submissions_Respiratory").val()==''?0:$("#Submissions_Respiratory").val(),
        "p_LR_Prev_1_Respiratory":$("#LR_Prev_1_Respiratory").val()==''?0:$("#LR_Prev_1_Respiratory").val(),
        "p_Registration_Leprosy":$("#Registration_Leprosy").val()==''?0:$("#Registration_Leprosy").val(),
        "p_PlacesJs_CG_Leprosy":$("#PlacesJs_CG_Leprosy").val()==''?0:$("#PlacesJs_CG_Leprosy").val(),
        "p_PlacesJs_UT_Leprosy":$("#PlacesJs_UT_Leprosy").val()==''?0:$("#PlacesJs_UT_Leprosy").val(),
        "p_PlacesJs_SG_Leprosy":$("#PlacesJs_SG_Leprosy").val()==''?0:$("#PlacesJs_SG_Leprosy").val(),
        "p_PlacesJs_QG_Leprosy":$("#PlacesJs_QG_Leprosy").val()==''?0:$("#PlacesJs_QG_Leprosy").val(),
        "p_PlacesJs_LB_Leprosy":$("#PlacesJs_LB_Leprosy").val()==''?0:$("#PlacesJs_LB_Leprosy").val(),
        "p_PlacesJs_Private_Leprosy":$("#PlacesJs_Private_Leprosy").val()==''?0:$("#PlacesJs_Private_Leprosy").val(),
        "p_PlacesJs_Total_Leprosy":$("#PlacesJs_Total_Leprosy").val()==''?0:$("#PlacesJs_Total_Leprosy").val(),
        "p_Registration_Removed_Leprosy":$("#Registration_Removed_Leprosy").val()==''?0:$("#Registration_Removed_Leprosy").val(),
        "p_LR_Leprosy":$("#LR_Leprosy").val()==''?0:$("#LR_Leprosy").val(),
        "p_Submissions_Leprosy":$("#Submissions_Leprosy").val()==''?0:$("#Submissions_Leprosy").val(),
        "p_LR_Prev_1_Leprosy":$("#LR_Prev_1_Leprosy").val()==''?0:$("#LR_Prev_1_Leprosy").val(),
        "p_Registration_Total":$("#Registration_Total").val()==''?0:$("#Registration_Total").val(),
        "p_PlacesJs_CG_Total":$("#PlacesJs_CG_Total").val()==''?0:$("#PlacesJs_CG_Total").val(),
        "p_PlacesJs_UT_Total":$("#PlacesJs_UT_Total").val()==''?0:$("#PlacesJs_UT_Total").val(),
        "p_PlacesJs_SG_Total":$("#PlacesJs_SG_Total").val()==''?0:$("#PlacesJs_SG_Total").val(),
        "p_PlacesJs_QG_Total":$("#PlacesJs_QG_Total").val()==''?0:$("#PlacesJs_QG_Total").val(),
        "p_PlacesJs_LB_Total":$("#PlacesJs_LB_Total").val()==''?0:$("#PlacesJs_LB_Total").val(),
        "p_PlacesJs_Private_Total":$("#PlacesJs_Private_Total").val()==''?0:$("#PlacesJs_Private_Total").val(),
        "p_PlacesJs_Total_Total":$("#PlacesJs_Total_Total").val()==''?0:$("#PlacesJs_Total_Total").val(),
        "p_Registration_Removed_Total":$("#Registration_Removed_Total").val()==''?0:$("#Registration_Removed_Total").val(),
        "p_LR_Total":$("#LR_Total").val()==''?0:$("#LR_Total").val(),
        "p_Submissions_Total": $("#Submissions_Total").val()==''?0:$("#Submissions_Total").val(),
        "p_LR_Prev_1_Total": $("#LR_Prev_1_Total").val()==''?0:$("#LR_Prev_1_Total").val(),
        "p_Registration_W": $("#Registration_W").val()==''?0:$("#Registration_W").val(),
        "p_PlacesJs_CG_W": $("#PlacesJs_CG_W").val()==''?0:$("#PlacesJs_CG_W").val(),
        "p_PlacesJs_UT_W": $("#PlacesJs_UT_W").val()==''?0:$("#PlacesJs_UT_W").val(),
        "p_PlacesJs_SG_W": $("#PlacesJs_SG_W").val()==''?0:$("#PlacesJs_SG_W").val(),
        "p_PlacesJs_QG_W": $("#PlacesJs_QG_W").val()==''?0:$("#PlacesJs_QG_W").val(),
        "p_PlacesJs_LB_W": $("#PlacesJs_LB_W").val()==''?0:$("#PlacesJs_LB_W").val(),
        "p_PlacesJs_Private_W": $("#PlacesJs_Private_W").val()==''?0:$("#PlacesJs_Private_W").val(),
        "p_PlacesJs_Total_W": $("#PlacesJs_Total_W").val()==''?0:$("#PlacesJs_Total_W").val(),
        "p_Registration_Removed_W": $("#Registration_Removed_W").val()==''?0:$("#Registration_Removed_W").val(),
        "p_LR_W": $("#LR_W").val()==''?0:$("#LR_W").val(),
        "p_Submissions_W": $("#Submissions_W").val()==''?0:$("#Submissions_W").val(),
        "p_LR_Prev_1_W": $("#LR_Prev_1_W").val()==''?0:$("#LR_Prev_1_W").val(),
        "p_Flag" :'3',
             }
          MasterData = JSON.stringify(MasterData)
          var path = serverpath + "es25p1RptEntry";
          securedajaxpost(path,'parsrdataES25RptEntryForm','comment',MasterData,'control')
          }
          
          
          function parsrdataES25RptEntryForm(data){
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                ES25RptEntryForm();
                
            }
            if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
                if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
                jQuery("#tablehead").html('<th>No Record Found</th>');
                $('.fa').hide();
                jQuery("#tbodyvalue").html('');
                }
                }
                else{
          if(data[0][0]){
               
                        $("#Registration_Blind").text(data[0][0].Registration_Blind),
                        $("#PlacesJs_CG_Blind").text(data[0][0].PlacesJs_CG_Blind),
                        $("#PlacesJs_UT_Blind").text(data[0][0].PlacesJs_UT_Blind),
                        $("#PlacesJs_SG_Blind").text(data[0][0].PlacesJs_SG_Blind),
                        $("#PlacesJs_QG_Blind").text(data[0][0].PlacesJs_QG_Blind),
                        $("#PlacesJs_LB_Blind").text(data[0][0].PlacesJs_LB_Blind),
                        $("#PlacesJs_Private_Blind").text(data[0][0].PlacesJs_Private_Blind),
                        $("#PlacesJs_Total_Blind").text(data[0][0].PlacesJs_Total_Blind),
                        $("#Registration_Removed_Blind ").text(data[0][0].Registration_Removed_Blind),
                        $("#LR_Blind").text(data[0][0].LR_Blind),
                        $("#Submissions_Blind").text(data[0][0].Submissions_Blind),
                        $("#LR_Prev_1_Blind").text(data[0][0].LR_Prev_1_Blind),
                        
        
                        $("#Registration_Deaf").text(data[0][0].Registration_Deaf),
                        $("#PlacesJs_CG_Deaf").text(data[0][0].PlacesJs_CG_Deaf),
                        $("#PlacesJs_UT_Deaf").text(data[0][0].PlacesJs_UT_Deaf),
                        $("#PlacesJs_SG_Deaf").text(data[0][0].PlacesJs_SG_Deaf),
                        $("#PlacesJs_QG_Deaf").text(data[0][0].PlacesJs_QG_Deaf),
                        $("#PlacesJs_LB_Deaf").text(data[0][0].PlacesJs_LB_Deaf),
                        $("#PlacesJs_Private_Deaf").text(data[0][0].PlacesJs_Private_Deaf),
                        $("#PlacesJs_Total_Deaf").text(data[0][0].PlacesJs_Total_Deaf),
                        $("#Registration_Removed_Deaf ").text(data[0][0].Registration_Removed_Deaf),
                        $("#LR_Deaf").text(data[0][0].LR_Deaf),
                        $("#Submissions_Deaf").text(data[0][0].Submissions_Deaf),
                        $("#LR_Prev_1_Deaf").text(data[0][0].LR_Prev_1_Deaf),
        
                        $("#Registration_Orthopaedics").text(data[0][0].Registration_Orthopaedics),
                        $("#PlacesJs_CG_Orthopaedics").text(data[0][0].PlacesJs_CG_Orthopaedics),
                        $("#PlacesJs_UT_Orthopaedics").text(data[0][0].PlacesJs_UT_Orthopaedics),
                        $("#PlacesJs_SG_Orthopaedics").text(data[0][0].PlacesJs_SG_Orthopaedics),
                        $("#PlacesJs_QG_Orthopaedics").text(data[0][0].PlacesJs_QG_Orthopaedics),
                        $("#PlacesJs_LB_Orthopaedics").text(data[0][0].PlacesJs_LB_Orthopaedics),
                        $("#PlacesJs_Private_Orthopaedics").text(data[0][0].PlacesJs_Private_Orthopaedics),
                        $("#PlacesJs_Total_Orthopaedics").text(data[0][0].PlacesJs_Total_Orthopaedics),
                        $("#Registration_Removed_Orthopaedics ").text(data[0][0].Registration_Removed_Orthopaedics),
                        $("#LR_Orthopaedics").text(data[0][0].LR_Orthopaedics),
                        $("#Submissions_Orthopaedics").text(data[0][0].Submissions_Orthopaedics),
                        $("#LR_Prev_1_Orthopaedics").text(data[0][0].LR_Prev_1_Orthopaedics),
        
                        $("#Registration_Respiratory").text(data[0][0].Registration_Respiratory),
                        $("#PlacesJs_CG_Respiratory").text(data[0][0].PlacesJs_CG_Respiratory),
                        $("#PlacesJs_UT_Respiratory").text(data[0][0].PlacesJs_UT_Respiratory),
                        $("#PlacesJs_SG_Respiratory").text(data[0][0].PlacesJs_SG_Respiratory),
                        $("#PlacesJs_QG_Respiratory").text(data[0][0].PlacesJs_QG_Respiratory),
                        $("#PlacesJs_LB_Respiratory").text(data[0][0].PlacesJs_LB_Respiratory),
                        $("#PlacesJs_Private_Respiratory").text(data[0][0].PlacesJs_Private_Respiratory),
                        $("#PlacesJs_Total_Respiratory").text(data[0][0].PlacesJs_Total_Respiratory),
                        $("#Registration_Removed_Respiratory ").text(data[0][0].Registration_Removed_Respiratory),
                        $("#LR_Respiratory").text(data[0][0].LR_Respiratory),
                        $("#Submissions_Respiratory").text(data[0][0].Submissions_Respiratory),
                        $("#LR_Prev_1_Respiratory").text(data[0][0].LR_Prev_1_Respiratory),
        
                        $("#Registration_Leprosy").text(data[0][0].Registration_Leprosy),
                        $("#PlacesJs_CG_Leprosy").text(data[0][0].PlacesJs_CG_Leprosy),
                        $("#PlacesJs_UT_Leprosy").text(data[0][0].PlacesJs_UT_Leprosy),
                        $("#PlacesJs_SG_Leprosy").text(data[0][0].PlacesJs_SG_Leprosy),
                        $("#PlacesJs_QG_Leprosy").text(data[0][0].PlacesJs_QG_Leprosy),
                        $("#PlacesJs_LB_Leprosy").text(data[0][0].PlacesJs_LB_Leprosy),
                        $("#PlacesJs_Private_Leprosy").text(data[0][0].PlacesJs_Private_Leprosy),
                        $("#PlacesJs_Total_Leprosy").text(data[0][0].PlacesJs_Total_Leprosy),
                        $("#Registration_Removed_Leprosy ").text(data[0][0].Registration_Removed_Leprosy),
                        $("#LR_Leprosy").text(data[0][0].LR_Leprosy),
                        $("#Submissions_Leprosy").text(data[0][0].Submissions_Leprosy),
                        $("#LR_Prev_1_Leprosy").text(data[0][0].LR_Prev_1_Leprosy),
        
                        $("#Registration_Total").text(data[0][0].Registration_Total),
                        $("#PlacesJs_CG_Total").text(data[0][0].PlacesJs_CG_Total),
                        $("#PlacesJs_UT_Total").text(data[0][0].PlacesJs_UT_Total),
                        $("#PlacesJs_SG_Total").text(data[0][0].PlacesJs_SG_Total),
                        $("#PlacesJs_QG_Total").text(data[0][0].PlacesJs_QG_Total),
                        $("#PlacesJs_LB_Total").text(data[0][0].PlacesJs_LB_Total),
                        $("#PlacesJs_Private_Total").text(data[0][0].PlacesJs_Private_Total),
                        $("#PlacesJs_Total_Total").text(data[0][0].PlacesJs_Total_Total),
                        $("#Registration_Removed_Total ").text(data[0][0].Registration_Removed_Total),
                        $("#LR_Total").text(data[0][0].LR_Total),
                        $("#Submissions_Total").text(data[0][0].Submissions_Total),
                        $("#LR_Prev_1_Total").text(data[0][0].LR_Prev_1_Total),
        
                        $("#Registration_W").text(data[0][0].Registration_W),
                        $("#PlacesJs_CG_W").text(data[0][0].PlacesJs_CG_W),
                        $("#PlacesJs_UT_W").text(data[0][0].PlacesJs_UT_W),
                        $("#PlacesJs_SG_W").text(data[0][0].PlacesJs_SG_W),
                        $("#PlacesJs_QG_W").text(data[0][0].PlacesJs_QG_W),
                        $("#PlacesJs_LB_W").text(data[0][0].PlacesJs_LB_W),
                        $("#PlacesJs_Private_W").text(data[0][0].PlacesJs_Private_W),
                        $("#PlacesJs_Total_W").text(data[0][0].PlacesJs_W_W),
                        $("#Registration_Removed_W ").text(data[0][0].Registration_Removed_W),
                        $("#LR_W").text(data[0][0].LR_W),
                        $("#Submissions_W").text(data[0][0].Submissions_W),
                        $("#LR_Prev_1_W").text(data[0][0].LR_Prev_1_W)
              }

            else{
               $(' #tbodyvalue, #tablehead').empty();
               $('#tablehead').html('<th>No Records Found</th>')
            }
        }
        
             }
    
             function ES25RptP2Submit(){
                    if($('#criteriaddl').val()=='1'){
                        var fromdate = ''+$('#frmYear').val()+'/01/01'
                        var todate = ''+$('#frmYear').val()+'/06/30/'
                        }
                        else if($('#criteriaddl').val()=='2'){
                            var fromdate = ''+$('#frmYear').val()+'/07/01/'
                            var todate = ''+$('#frmYear').val()+'/12/31/'
                        }
                
                    var MasterData = {
                  
                "p_Ex_Id": $('#printreportproformaexchange').val(),  
                "p_FromDt":  fromdate,  
                "p_ToDt":   todate,  
                "p_Outstanding_Prev_CG":   $("#Outstanding_Prev_CG").val()==''?0:$("#Outstanding_Prev_CG").val(),  
                "p_Outstanding_Prev_UT":  $("#Outstanding_Prev_UT").val()==''?0:$("#Outstanding_Prev_UT").val() ,  
                "p_Outstanding_Prev_SG":   $("#Outstanding_Prev_SG").val()==''?0:$("#Outstanding_Prev_SG").val(),  
                "p_Outstanding_Prev_Quasi_CG":  $("#Outstanding_Prev_Quasi_CG").val()==''?0:$("#Outstanding_Prev_Quasi_CG").val() ,  
                "p_Outstanding_Prev_Quasi_SG":  $("#Outstanding_Prev_Quasi_SG").val()==''?0:$("#Outstanding_Prev_Quasi_SG").val() ,  
                "p_Outstanding_Prev_LB":   $("#Outstanding_Prev_LB").val()==''?0:$("#Outstanding_Prev_LB").val(),  
                "p_Outstanding_Prev_Private":  $("#Outstanding_Prev_Private").val()==''?0:$("#Outstanding_Prev_Private").val() ,  
                "p_Outstanding_Prev_Total":  $("#Outstanding_Prev_Total").val()==''?0:$("#Outstanding_Prev_Total").val() ,  
                "p_Notified_CG":   $("#Notified_CG").val()==''?0:$("#Notified_CG").val(),  
                "p_Notified_UT":  $("#Notified_UT").val()==''?0:$("#Notified_UT").val() ,  
                "p_Notified_SG":   $("#Notified_SG").val()==''?0:$("#Notified_SG").val(),  
                "p_Notified_Quasi_CG":  $("#Notified_Quasi_CG").val()==''?0:$("#Notified_Quasi_CG").val() ,  
                "p_Notified_Quasi_SG":  $("#Notified_Quasi_SG").val()==''?0:$("#Notified_Quasi_SG").val() ,  
                "p_Notified_LB":  $("#Notified_LB").val()==''?0:$("#Notified_LB").val() ,  
                "p_Notified_Private":  $("#Notified_Private").val()==''?0:$("#Notified_Private").val() ,  
                "p_Notified_Total":  $("#Notified_Total").val()==''?0:$("#Notified_Total").val() ,  
                "p_Filled_CG":  $("#Filled_CG").val()==''?0:$("#Filled_CG").val() ,  
                "p_Filled_UT":  $("#Filled_UT").val()==''?0:$("#Filled_UT").val() ,  
                "p_Filled_SG":  $("#Filled_SG").val()==''?0:$("#Filled_SG").val() ,  
                "p_Filled_Quasi_CG":  $("#Filled_Quasi_CG").val()==''?0:$("#Filled_Quasi_CG").val() ,  
                "p_Filled_Quasi_SG":  $("#Filled_Quasi_SG").val()==''?0:$("#Filled_Quasi_SG").val() ,  
                "p_Filled_LB":   $("#Filled_LB").val()==''?0:$("#Filled_LB").val(),  
                "p_Filled_Private":  $("#Filled_Private").val()==''?0:$("#Filled_Private").val() ,  
                "p_Filled_Total":  $("#Filled_Total").val()==''?0:$("#Filled_Total").val() ,  
                "p_Cancelled_NA_CG":  $("#Cancelled_NA_CG").val()==''?0:$("#Cancelled_NA_CG").val() ,  
                "p_Cancelled_NA_UT":  $("#Cancelled_NA_UT").val()==''?0:$("#Cancelled_NA_UT").val() ,  
                "p_Cancelled_NA_SG":  $("#Cancelled_NA_SG").val()==''?0:$("#Cancelled_NA_SG").val() ,  
                "p_Cancelled_NA_Quasi_CG":  $("#Cancelled_NA_Quasi_CG").val()==''?0:$("#Cancelled_NA_Quasi_CG").val() ,  
                "p_Cancelled_NA_Quasi_SG":  $("#Cancelled_NA_Quasi_SG").val()==''?0:$("#Cancelled_NA_Quasi_SG").val() ,  
                "p_Cancelled_NA_LB":   $("#Cancelled_NA_LB").val()==''?0:$("#Cancelled_NA_LB").val(),  
                "p_Cancelled_NA_Private":   $("#Cancelled_NA_Private").val()==''?0:$("#Cancelled_NA_Private").val(),  
                "p_Cancelled_NA_Total":  $("#Cancelled_NA_Total").val()==''?0:$("#Cancelled_NA_Total").val() ,  
                "p_Cancelled_Other_CG":  $("#Cancelled_Other_CG").val()==''?0:$("#Cancelled_Other_CG").val() ,  
                "p_Cancelled_Other_UT":  $("#Cancelled_Other_UT").val()==''?0:$("#Cancelled_Other_UT").val() ,  
                "p_Cancelled_Other_SG":  $("#Cancelled_Other_SG").val()==''?0:$("#Cancelled_Other_SG").val() ,  
                "p_Cancelled_Other_Quasi_CG":  $("#Cancelled_Other_Quasi_CG").val()==''?0:$("#Cancelled_Other_Quasi_CG").val() ,  
                "p_Cancelled_Other_Quasi_SG":   $("#Cancelled_Other_Quasi_SG").val()==''?0:$("#Cancelled_Other_Quasi_SG").val(),  
                "p_Cancelled_Other_LB":  $("#Cancelled_Other_LB").val()==''?0:$("#Cancelled_Other_LB").val() ,  
                "p_Cancelled_Other_Private":  $("#Cancelled_Other_Private").val()==''?0:$("#Cancelled_Other_Private").val() ,  
                "p_Cancelled_Other_Total":  $("#Cancelled_Other_Total").val()==''?0:$("#Cancelled_Other_Total").val() ,  
                "p_Outstanding_CG":  $("#Outstanding_CG").val()==''?0:$("#Outstanding_CG").val() ,  
                "p_Outstanding_UT":   $("#Outstanding_UT").val()==''?0:$("#Outstanding_UT").val(),  
                "p_Outstanding_SG":  $("#Outstanding_SG").val()==''?0:$("#Outstanding_SG").val() ,  
                "p_Outstanding_Quasi_CG":   $("#Outstanding_Quasi_CG").val()==''?0:$("#Outstanding_Quasi_CG").val(),  
                "p_Outstanding_Quasi_SG":  $("#Outstanding_Quasi_SG").val()==''?0:$("#Outstanding_Quasi_SG").val() ,  
                "p_Outstanding_LB":  $("#Outstanding_LB").val()==''?0:$("#Outstanding_LB").val() ,  
                "p_Outstanding_Private":   $("#Outstanding_Private").val()==''?0:$("#Outstanding_Private").val(),  
                "p_Outstanding_Total":  $("#Outstanding_Total").val()==''?0:$("#Outstanding_Total").val(),    
                "p_Flag": '3',
                
                     }
                  MasterData = JSON.stringify(MasterData)
                  var path = serverpath + "ES25P2Entry";
                  securedajaxpost(path,'parsrdataES25RptP2Submit','comment',MasterData,'control')
                  }
                  
                  
                  function parsrdataES25RptP2Submit(data){
                    data = JSON.parse(data)
                   
                    if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
                        if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
                        jQuery("#tablehead").html('<th>No Record Found</th>');
                        $('.fa').hide();
                        jQuery("#tbodyvalue").html('');
                        }
                        }
                        else{
                  
                
                  if(data[0][0]){
                       
                   $("#Outstanding_Prev_CG").text(data[0][0].Outstanding_Prev_CG);
                   $("#Outstanding_Prev_UT").text(data[0][0].Outstanding_Prev_UT);
                   $("#Outstanding_Prev_SG").text(data[0][0].Outstanding_Prev_SG);
                   $("#Outstanding_Prev_Quasi_CG").text(data[0][0].Outstanding_Prev_Quasi_CG);
                   $("#Outstanding_Prev_Quasi_SG").text(data[0][0].Outstanding_Prev_Quasi_SG) ; 
                   $("#Outstanding_Prev_LB").text(data[0][0].Outstanding_Prev_LB); 
                   $("#Outstanding_Prev_Private").text(data[0][0].Outstanding_Prev_Private) ; 
                   $("#Outstanding_Prev_Total").text(data[0][0].Outstanding_Prev_Total) ;
                   $("#Notified_CG").text(data[0][0].Notified_CG);
                     $("#Notified_UT").text(data[0][0].Notified_UT) ;  
                      $("#Notified_SG").text(data[0][0].Notified_SG);
                     $("#Notified_Quasi_CG").text(data[0][0].Notified_Quasi_CG) ;  
                     $("#Notified_Quasi_SG").text(data[0][0].Notified_Quasi_SG) ; 
                     $("#Notified_LB").text(data[0][0].Notified_LB) ;
                     $("#Notified_Private").text(data[0][0].Notified_Private) ; 
                     $("#Notified_Total").text(data[0][0].Notified_Total) ;
                     $("#Filled_CG").text(data[0][0].Filled_CG) ;  
                     $("#Filled_UT").text(data[0][0].Filled_UT) ;  
                     $("#Filled_SG").text(data[0][0].Filled_SG) ;
                    $("#Filled_Quasi_CG").text(data[0][0].Filled_Quasi_CG) ; 
                     $("#Filled_Quasi_SG").text(data[0][0].Filled_Quasi_SG) ; 
                     $("#Filled_LB").text(data[0][0].Filled_LB);
                     $("#Filled_Private").text(data[0][0].Filled_Private) ;  
                     $("#Filled_Total").text(data[0][0].Filled_Total) ;
                     $("#Cancelled_NA_CG").text(data[0][0].Cancelled_NA_CG) ;  
                     $("#Cancelled_NA_UT").text(data[0][0].Cancelled_NA_UT) ;  
                     $("#Cancelled_NA_SG").text(data[0][0].Cancelled_NA_SG) ;
                     $("#Cancelled_NA_Quasi_CG").text(data[0][0].Cancelled_NA_Quasi_CG) ; 
                     $("#Cancelled_NA_Quasi_SG").text(data[0][0].Cancelled_NA_Quasi_SG) ;  
                      $("#Cancelled_NA_LB").text(data[0][0].Cancelled_NA_LB);
                      $("#Cancelled_NA_Private").text(data[0][0].Cancelled_NA_Private);  
                     $("#Cancelled_NA_Total").text(data[0][0].Cancelled_NA_Total) ;  
                     $("#Cancelled_Other_CG").text(data[0][0].Cancelled_Other_CG) ;  
                     $("#Cancelled_Other_UT").text(data[0][0].Cancelled_Other_UT) ;  
                     $("#Cancelled_Other_SG").text(data[0][0].Cancelled_Other_SG) ;
                     $("#Cancelled_Other_Quasi_CG").text(data[0][0].Cancelled_Other_Quasi_CG) ;  
                     $("#Cancelled_Other_Quasi_SG").text(data[0][0].Cancelled_Other_Quasi_SG); 
                     $("#Cancelled_Other_LB").text(data[0][0].Cancelled_Other_LB) ;
                     $("#Cancelled_Other_Private").text(data[0][0].Cancelled_Other_Private) ;  
                     $("#Cancelled_Other_Total").text(data[0][0].Cancelled_Other_Total) ; 
                    $("#Outstanding_CG").text(data[0][0].Outstanding_CG) ; 
                      $("#Outstanding_UT").text(data[0][0].Outstanding_UT);  
                     $("#Outstanding_SG").text(data[0][0].Outstanding_SG) ;
                      $("#Outstanding_Quasi_CG").text(data[0][0].Outstanding_Quasi_CG);  
                     $("#Outstanding_Quasi_SG").text(data[0][0].Outstanding_Quasi_SG) ; 
                     $("#Outstanding_LB").text(data[0][0].Outstanding_LB) ;
                      $("#Outstanding_Private").text(data[0][0].Outstanding_Private);  
                     $("#Outstanding_Total").text(data[0][0].Outstanding_Total)
                }
    else{
       $(' #tbodyvalue, #tablehead').empty();
       $('#tablehead').html('<th>No Records Found</th>')
    }
}
    
    
               
                  
                     }
    
                     function FillES21_Master(funct,control) {
                        var path =  serverpath + "ES21_Master"
                        securedajaxget(path,funct,'comment',control);
                      }
                      
                      function parsedataFillES21_Master(data,control){  
                        data = JSON.parse(data)
                        if (data.message == "New token generated"){
                            sessionStorage.setItem("token", data.data.token);
                            FillES21_Master('parsedataFillES21_Master','tbodyvalue');
                        }
                        else if (data.status == 401){
                            toastr.warning("Unauthorized", "", "info")
                            return true;
                        }
                            else{
                                jQuery("#"+control).empty();
                                var data1 = data[0];
                                var appenddata = ''
                           
                    
                                for (var i = 0; i < data1.length; i++) {
                                   
                                  appenddata+= `<tr class='i'>
                                  <td><label class='Edu21_id'>`+data1[i].Edu21_id+`</label></td>
                                  <td colspan="3">`+data1[i].Edu_Name+`</td>
                    
                                  <td   id="Total_Regd`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="Total_Placement`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="Total_LR`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="W_Regd`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="W_Placement`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="W_LR`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="SC_Regd`+data1[i].Edu21_id+`">
                                  </td>	
                                  <td    id="SC_Placement`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="SC_LR`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="ST_Regd`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="ST_Placement`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="ST_LR`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="OBC_Regd`+data1[i].Edu21_id+`">
                                  </td>
                                  <td    id="OBC_Placement`+data1[i].Edu21_id+`">
                                  </td>
                                  <td   id="OBC_LR`+data1[i].Edu21_id+`">
                                  </td>
                              
                              </tr>`
                                 }
                                 $("#tbodyvalue").append(appenddata);
                                 ES21Entry();
                            }
                                  
                      }
    
    
                      function ES21Entry(){
                        
                            if($('#criteriaddl').val()=='1'){
                              //  var fromdate = ''+$("#years").val()+'/01/01'
                                var todate = ''+$('#frmYear').val()+'-06-30'
                                }
                                else if($('#criteriaddl').val()=='2'){
                             //    var fromdate = ''+$("#years").val()+'/07/01/'
                                    var todate = ''+$('#frmYear').val()+'-12-31'
                                }        
                              
                            var MasterData = {
                      
                                "p_Ex_id": $('#printreportproformaexchange').val(),  
                                "p_ToDt":  todate ,  
                                "p_Edu21_id":0, 
                                "p_Total_Regd":0,  
                                "p_Total_Placement":0,  
                                "p_Total_LR": 0,
                                "p_W_Regd":0,  
                                "p_W_Placement":0,  
                                "p_W_LR": 0,
                                "p_SC_Regd":0,
                                "p_SC_Placement": 0,
                                "p_SC_LR": 0,
                                "p_ST_Regd": 0,
                                "p_ST_Placement": 0,
                                "p_ST_LR": 0,
                                "p_OBC_Regd": 0,
                                "p_OBC_Placement": 0,
                                "p_OBC_LR": 0,
                                "p_Flag": '3', 
                            }
                            MasterData = JSON.stringify(MasterData)
                            var path = serverpath + "ES21Entry";
                            securedajaxpost(path,'parsrdataES21Entry','comment',MasterData,'control')
                       
                        
                    }
                    
                    
                    function parsrdataES21Entry(data){
                        data = JSON.parse(data)
                        if(data[0][0].Verify_YN=='0' || data[0][0].Verify_YN==null){
                            if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
                            jQuery("#tablehead").html('<th>No Record Found</th>');
                            $('.fa').hide();
                            jQuery("#tbodyvalue").html('');
                            }
                            }
                            else{
                        if(data[0].length){
                            for (var i = 0; i < data[0].length; i++) {
                                $('#Total_Regd'+data[0][i].Edu21_id+'').text(data[0][i].Total_Regd);
                                $('#Total_Placement'+data[0][i].Edu21_id+'').text(data[0][i].Total_Placement);
                                $('#Total_LR'+data[0][i].Edu21_id+'').text(data[0][i].Total_LR);
                            
                                $('#W_Regd'+data[0][i].Edu21_id+'').text(data[0][i].W_Regd);
                                $('#W_Placement'+data[0][i].Edu21_id+'').text(data[0][i].W_Placement);
                                $('#W_LR'+data[0][i].Edu21_id+'').text(data[0][i].W_LR);
                            
                                $('#SC_Regd'+data[0][i].Edu21_id+'').text(data[0][i].SC_Regd);
                                $('#SC_Placement'+data[0][i].Edu21_id+'').text(data[0][i].SC_Placement);
                                $('#SC_LR'+data[0][i].Edu21_id+'').text(data[0][i].SC_LR);
                            
                                $('#ST_Regd'+data[0][i].Edu21_id+'').text(data[0][i].ST_Regd);
                                $('#ST_Placement'+data[0][i].Edu21_id+'').text(data[0][i].ST_Placement);
                                $('#ST_LR'+data[0][i].Edu21_id+'').text(data[0][i].ST_LR);
                            
                                $('#OBC_Regd'+data[0][i].Edu21_id+'').text(data[0][i].OBC_Regd);
                                $('#OBC_Placement'+data[0][i].Edu21_id+'').text(data[0][i].OBC_Placement);
                                $('#OBC_LR'+data[0][i].Edu21_id+'').text(data[0][i].OBC_LR);
                            
                            }
                        }
                           
                       else{
                       $(' #tbodyvalue, #tablehead').empty();
                       $('#tablehead').html('<th>No Records Found</th>')
                       } 
                    }
                    }
                    

                    function fetchCounselling_ByMonthConsolidate(){
                            var path =  serverpath + "Counselling_ByMonthConsolidate/"+$('#prpfrommonth').val()+"/"+$('#prptomonth').val()+"/"+$('#118prpfromyear').val()+"/"+$('#118prptoyear').val()+"/"+$("#printreportproformaexchange").val()+""
                            ajaxget(path,'parsedataCounselling_ByMonthConsolidate','comment',"control");
                             }

                    function parsedataCounselling_ByMonthConsolidate(data){  
                        data = JSON.parse(data)
                       var appenddata="";
                        var data1 = data[0]; 
                        for (var i = 0; i < data1.length; i++) {
                            appenddata += "<tr><td>"+[i+1]+"</td><td style='    word-break: break-word;'  >" +data1[i].Total_Counseling+ "</td> <td style='    word-break: break-word;'>" + data1[i].Total_Candidates+ "</td><td>"+data1[i].UR_Candidates+"</td><td style='    word-break: break-word;'>" + data1[i].SC_Candidates+ "</td><td>"+data1[i].ST_Candidates+"</td><td></td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td><td>"+data1[i].Remark+"</td></tr>";
                        }
                        jQuery("#tbodyvalue").html(appenddata);  
                       }




                    function fetchCounselling_ByMonthWise(){
                        var path =  serverpath + "Counselling_ByMonthWise/"+$("#prpfromyear").val()+"/"+$("#prptoyear").val()+"/"+$("#printreportproformaexchange").val()+""
                        ajaxget(path,'parsedatafetchCounselling_ByMonthWise','comment',"control");
                         }

                function parsedatafetchCounselling_ByMonthWise(data){  
                    data = JSON.parse(data)
                   var appenddata="";
                    var data1 = data[0]; 
                    for (var i = 0; i < data1.length; i++) {
                     appenddata += "<tr><td>"+[i+1]+"</td><td style='word-break: break-word;'  >" +data1[i].Ex_NameH+ "</td> <td style='    word-break: break-word;'>" + data1[i].Total_Counseling+ "</td><td style='    word-break: break-word;'>" + data1[i].Total_Candidates+ "</td><td>"+data1[i].UR_Candidates+"</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td><td>"+data1[i].Remark+"</td></tr>";
                    }
                    jQuery("#tbodyvalue").html(appenddata);  
                }


                // function parsedatafetchCounselling_ByMonthWise(data){  
                //     data = JSON.parse(data)
                //    var appenddata="";
                //     var data1 = data[0]; 
                //     for (var i = 0; i < data1.length; i++) {
                //      appenddata += "<tr><td style='    word-break: break-word;'  >" +data1[i].Ex_NameH+ "</td> <td style='    word-break: break-word;'>" + data1[i].XMonthYear+ "</td><td style='    word-break: break-word;'>" + data1[i].Total_Counseling+ "</td><td>"+data1[i].Total_Candidates+"</td><td>"+data1[i].UR_Candidates+"</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td></tr>";
                //     }
                //     jQuery("#tbodyvalue").html(appenddata);  
                // }
                
function fetchx63(){
    
    var data =$('#m_datepicker_5').val();
    var arr = data.split('/');
    $("#m_datepicker_5").html("<span>"+arr[2] + "-" + arr[1]+"-"+arr[0]+" </span>");

    var Date =$('#m_datepicker_6').val();
    var datesplit = Date.split('/');
    $("#m_datepicker_6").html("<span>"+datesplit[2] + "-" + datesplit[1]+"-"+datesplit[0]+" </span>");
    var MasterData = {  
        "p_Ex_Id":$('#printreportproformaexchange').val(),
        "p_From":arr[2] + "-" + arr[1]+"-"+arr[0],
        "p_To":datesplit[2] + "-" + datesplit[1]+"-"+datesplit[0],
        "p_Category":0,
        "p_Gender":'Both',
        "p_DifferentlyAbled":'Both',
        "p_Resident":0,
        "p_Area":0
        
    };
  
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "RptCIJSX63";
    ajaxpost(path, 'parsedatafetchx63', 'comment', MasterData, 'control')
   
}
function parsedatafetchx63(data){  
    data = JSON.parse(data)
    
    
        var data1 = processSearchUtilityData(data[0]);
        var appenddata="";
        jQuery("#tbodyvalue").empty();  
      
        var sNo = 0;

        var head="<tr><th></th><th colspan=10> Exchange: "+$('#printreportproformaexchange option:selected').html()+"</br> Report Name: X-63 Report <br>From Date : "+$('#m_datepicker_5').val()+" - To Date : "+$('#m_datepicker_6').val()+" </th></tr>";
        $("#Detailhead").html(head);

        var tablehead= "<tr><th style='border-top: ridge;border-right: ridge;'>Sno</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Reg.No & Reg. Date</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Name/ Father's or Husband's Name / Address</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>DOB/ Category/ Gender</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Area/ Domicile/PHP </th><th colspan=3 style='border-top: ridge;border-right: ridge;'>NCO</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Qualification</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Entered By/Current Status</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>JobSeeker History</th></tr>"+
        "<tr><th style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th style='border-top: ridge;border-right: ridge;'>Alloted NCO</th><th style='border-top: ridge;border-right: ridge;'>Seniority Date</th><th style='border-top: ridge;border-right: ridge;'>NCO Detail</th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th></tr>";
        $("#tablehead").html(tablehead);
        for (var key in data1) {
            sNo++;
            var searchData = data1[key].data[0];
            var rowCount = data1[key].count;

            appenddata +=   `<tr >
                                <th style='vertical-align: middle' rowspan=${rowCount}>${sNo} </th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.RegistrationId} & ${searchData.RegDate} </th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.CandidateName} / ${searchData.GuardianFatherName}/${searchData.Address}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.DateOfBirth}<br> / ${searchData.CategoryName} / ${searchData.Gender}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.Area_Name}/${searchData.Domicile}/${searchData.DifferentlyAbled}</th>
                            
                                <th>${searchData.NCO_Code}</th>
                                <th>${searchData.SeniorityDt}</th>
                                <th>${searchData.NCO_detail}</th>
                                <th>${searchData.Qualification}</th>
                               
                                <th style='vertical-align: middle' rowspan=${rowCount}>${searchData.EnteredBy}/${searchData.CurrentStatus}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount}>${searchData.JobseekerHistory}</th>
                             
                                </tr>`;
                       
            for(var i = 1; i < data1[key].count; i++) {
                console.log("in");
                searchData = data1[key].data[i];
                console.log(searchData);
                appenddata +=   `<tr>
                <th>${searchData.NCO_Code}</th>
                <th>${searchData.SeniorityDt}</th>
                <th>${searchData.NCO_detail}</th>
                <th>${searchData.Qualification}</th>
               

                                </tr>`;
            }
        

            
        }jQuery("#tbodyvalue").html(appenddata); 
        $('#tbodyvalue').clientSidePagination();
        
     //   addTopScrollbar($('#tbodyvalue1').closest('table'), $('#tbodyvalue1').closest('.form-group.m-form__group.row'));

    
}
function processSearchUtilityData(data){
var returnData = {};
for (var i = 0; i < data.length; i++) {
if(returnData.hasOwnProperty(data[i].RegistrationId)) {
    returnData[data[i].RegistrationId].count += 1;
} else {
    returnData[data[i].RegistrationId] = {data:[], count:1};
}
returnData[data[i].RegistrationId].data.push(data[i]);
}
return returnData;
}

                function fetchJFRep_ByMonthConsolidate(){
                    var path =  serverpath + "JFRep_ByMonthConsolidate/"+$('#prpfrommonth').val()+"/"+$('#prptomonth').val()+"/"+$('#118prpfromyear').val()+"/"+$('#118prptoyear').val()+"/"+$("#printreportproformaexchange").val()+""
                    ajaxget(path,'parsedatafetchJFRep_ByMonthConsolidate','comment',"control");
                     }

            function parsedatafetchJFRep_ByMonthConsolidate(data){  
                data = JSON.parse(data)
               var appenddata="";
                var data1 = data[0]; 
                for (var i = 0; i < data1.length; i++) {
                appenddata += "<tr><td>"+[i+1]+"</td><td>" +data1[i].Ex_name+ "</td> <td>" + data1[i].JF_Date+ "</td><td>" + data1[i].Company_Name+ "</td><td>"+data1[i].Post_Name+"</td><td>"+data1[i].Selected_Candidates+"</td><td>"+data1[i].UR_Candidates+"</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td></td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidates+"</td><td>"+data1[i].Remark+"</td></tr>";}
                jQuery("#tbodyvalue").html(appenddata);  
            }


            function fetchCareerCounselingMonthlyRpt(){
                var MasterData = {  
                    "p_Flag":'1',
                    "p_Xmonth":jQuery("#printproformafrommonth").val(),
                    "p_Xyear":jQuery("#frmYear").val(),
                    "p_Ex_Id":jQuery("#printreportproformaexchange").val(),
                    "p_Tran_Id":'0',
                    "p_Total_Counseling":'0',
                    "p_Total_Candidates":'0',
                    "p_Female_Candidates":'0',
                    "p_SC_Candidates":'0',
                    "p_ST_Candidates":'0',
                    "p_OBC_Candidates":'0',
                    "p_PH_Candidates":'0',
                    "p_Minority_Candidates":'0',
                    "p_Remark":'0',
                    "p_UR_Candidates":'0',
                    "p_Client_IP":'0',
                    "p_LM_BY":'0',
                    "p_Status":'0'
                };
              
                MasterData = JSON.stringify(MasterData)
                var path = serverpath + "CareerCounsel_Pro_Reporting";
                ajaxpost(path, 'parsedatafetchCareerCounselingMonthlyRpt', 'comment', MasterData, 'control')
                }
        function parsedatafetchCareerCounselingMonthlyRpt(data){  
            data = JSON.parse(data)
           var appenddata="";
            var data1 = data[0]; 
            if(data1.length>0){
            for (var i = 0; i < data1.length; i++) {
             appenddata += "<tr><td>"+[i+1]+"</td><td style='word-break: break-word;'  >" +data1[i].Total_Counseling+ "</td> <td style='    word-break: break-word;'>" + data1[i].Total_Candidates+ "</td><td style='    word-break: break-word;'>" + data1[i].UR_Candidates+ "</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidate+"</td><td colspan=2>"+data1[i].Remark+"</td></tr>";
            }
            jQuery("#tbodyvalue").html(appenddata);  
        }
    }     


        function fetchJobFairMonthlyReporting(){

            var MasterData = {  
                "p_Flag":'1',
                "p_Xmonth":jQuery("#printproformafrommonth").val(),
                "p_Xyear":jQuery("#frmYear").val(),
                "p_Ex_Id":jQuery("#printreportproformaexchange").val(),
                "p_JF_Id":'0',
                "p_JF_Date":'1990/01/01',
                "p_Company_Name":'0',
                "p_Post_Name":'0',
                "p_Selected_Candidates":'0',
                "p_Female_Candidates":'0',
                "p_SC_Candidates":'0',
                "p_ST_Candidates":'0',
                "p_OBC_Candidates":'0',
                "p_PH_Candidates":'0',
                "p_Minority_Candidates":'0',
                "p_Remark":'0',
                "p_UR_Candidates":'0',
                "p_Client_IP":'0',
                "p_LM_BY":'0',
                "p_Status":'0'

            };
          
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "jobfairmonthlyreporting";
            ajaxpost(path, 'parsedatafetchJobFairMonthlyReporting', 'comment', MasterData, 'control')
            }
    function parsedatafetchJobFairMonthlyReporting(data){  
        data = JSON.parse(data)
       var appenddata="";
        var data1 = data[0]; 
        if(data1.length>0){
        for (var i = 0; i < data1.length; i++) {
            var date=data1[i].JF_Date.split('-');
        var JF_Date=date[0]+"/"+date[1]+"/"+date[2]
        var d1=date[2].split('')
        var d2=d1[0]+''+d1[1]
        var JF_Date1=date[0]+"/"+date[1]+"/"+d2

         appenddata += "<tr><td>"+[i+1]+"</td><td style='word-break: break-word;'  >" +JF_Date1+ "</td> <td style='    word-break: break-word;'>" + data1[i].Company_Name+ "</td><td style='    word-break: break-word;'>" + data1[i].Post_Name+ "</td><td>"+data1[i].Selected_Candidates+"</td><td>"+data1[i].Female_Candidates+"</td><td>"+data1[i].UR_Candidates+"</td><td>"+data1[i].SC_Candidates+"</td><td>"+data1[i].ST_Candidates+"</td><td>"+data1[i].OBC_Candidates+"</td><td>"+data1[i].PH_Candidates+"</td><td>"+data1[i].Minority_Candidates+"</td><td colspan=2>"+data1[i].Remark+"</td></tr>";
        }
        jQuery("#tbodyvalue").html(appenddata);  
    }
} 





function fetchagewiseQualiLevel(){
    var date1 = $("#m_datepicker_3").val().split('/')
    var date = date1[2]+"-"+date1[1]+"-"+date1[0]

    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1' ){
        var ex_id = $('#printreportproformaexchange').val();
    }
    else{
        var ex_id = sessionStorage.Ex_id
    }

    var path = serverpath +"Rpt_AgeWise_QualifLevel/"+ex_id+"/"+date+""
    ajaxget(path,'parsedatafetchagewiseQualiLevel','comment','control');
    }
   
    function parsedatafetchagewiseQualiLevel(data,control){
    data = JSON.parse(data);
    var appenddata='';
    var M19=0; var F19=0; var T19=0; M29=0; var F29=0; var T29=0; M39=0; var F39=0; var T39=0; M49=0; var F49=0; var T49=0; M59=0; var F59=0; var T59=0; M60=0; var F60=0; var T60=0; 
    for (var i = 0; i < data[0].length; i++) {
        M19=M19+data[0][i].M19; F19=F19+data[0][i].F19; T19=T19+data[0][i].T19; M29=M29+data[0][i].M29; F29=F29+data[0][i].F29; T29=T29+data[0][i].T29; M39=M39+data[0][i].M39; F39=F39+data[0][i].F39; T39=T39+data[0][i].T39; M49=M49+data[0][i].M49; F49=F49+data[0][i].F49; T49=T49+data[0][i].T49; M59=M59+data[0][i].M59; F59=F59+data[0][i].F59;T59=T59+data[0][i].T59; M60=M60+data[0][i].M60;F60=F60+data[0][i].F60;T60=T60+data[0][i].T60;
Total='<tr style="background-color: aliceblue;font-size: larger;"><th></th><th>Total</th><th>'+M19+'</th><th>'+F19+'</th><th>'+T19+'</th><th>'+M29+'</th><th>'+F29+'</th><th>'+T29+'</th><th>'+M39+'</th><th>'+F39+'</th><th>'+T39+'</th><th>'+M49+'</th><th>'+F49+'</th><th>'+T49+'</th><th>'+M59+'</th><th>'+F59+'</th><th>'+T59+'</th><th>'+M60+'</th><th>'+F60+'</th><th>'+T60+'</th><th>'+[T60+T59+T49+T39+T29+T19]+'</th></tr>'
    appenddata += "<tr class='i'><td>" + [i+1] + "</td><td>"+data[0][i].Qualif_Level_name+"</td><td>" + data[0][i].M19 + "</td><td>"+data[0][i].F19+"</td><td>"+data[0][i].T19+"</td><td> "+data[0][i].M29+" </td><td>"+data[0][i].F29+"</td><td>"+data[0][i].T29+"</td>  <td>"+data[0][i].M39+"</td><td>"+data[0][i].F39+"</td><td>"+data[0][i].T39+"</td><td>"+data[0][i].M49+"</td><td>"+data[0][i].F49+"</td><td>"+data[0][i].T49+"</td><td>"+data[0][i].M59+"</td><td>"+data[0][i].F59+"</td><td>"+data[0][i].T59+"</td><td>"+data[0][i].M60+"</td><td>"+data[0][i].F60+"</td><td>"+data[0][i].T60+"</td><td style='font-weight: 600;'>"+[data[0][i].T60+data[0][i].T59+data[0][i].T49+data[0][i].T39+data[0][i].T29+data[0][i].T19]+"</td></tr>";
    } jQuery("#tbodyvalue").html(appenddata+Total);
    $('#tbodyvalue').clientSidePagination();
    
    }


    function fetchagewiseQuali(){
        var date1 = $("#m_datepicker_5").val().split('/')
        var FromDate = date1[2]+"-"+date1[1]+"-"+date1[0]

        var date2 = $("#m_datepicker_6").val().split('/')
        var ToDate = date2[2]+"-"+date2[1]+"-"+date2[0]
    
        if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            var ex_id = $('#printreportproformaexchange').val();
        }
        else{
            var ex_id = sessionStorage.Ex_id
        }
    
        var path = serverpath +"AgeWise_QualifName/"+ex_id+"/"+FromDate+"/"+ToDate+""
        ajaxget(path,'parsedatafetchagewiseQuali','comment','control');
        }
       
        function parsedatafetchagewiseQuali(data,control){
        data = JSON.parse(data);
        var appenddata='';
         var MB19=0; var FB19=0; var TB19=0; var MB2029=0; var FB2029=0; var TB2029=0; var MB3039=0; var FB3039=0; var TB3039=0; var MB4049=0; var FB4049=0; var TB4049=0; var MB5059=0; var FB5059=0; var TB5059=0; var M60=0; var F60=0; var T60=0;
        for (var i = 0; i < data[0].length; i++) {
            MB19=MB19+data[0][i].M19; FB19=FB19+data[0][i].F19; TB19=TB19+data[0][i].T19; MB2029=MB2029+data[0][i].M29; FB2029=FB2029+data[0][i].F29; TB2029=TB2029+data[0][i].T29; MB3039=MB3039+data[0][i].M39; FB3039=FB3039+data[0][i].F39; TB3039=TB3039+data[0][i].T39; MB4049=MB4049+data[0][i].M49; FB4049=FB4049+data[0][i].F49; TB4049=TB4049+data[0][i].T49; MB5059=MB5059+data[0][i].M59; FB5059=FB5059+data[0][i].F59; TB5059=TB5059+data[0][i].T59; M60=M60+data[0][i].M60; F60=F60+data[0][i].F60; T60=T60+data[0][i].T60;
      Total=`<tr class='total'><td></td><td>Total</td><td>`+MB19+`</td><td>`+FB19+`</td><td>`+TB19+`</td><td>`+MB2029+`</td><td>`+FB2029+`</td><td>`+TB2029+`</td><td>`+MB3039+`</td><td>`+FB3039+`</td><td>`+TB3039+`</td><td>`+MB4049+`</td><td>`+FB4049+`</td><td>`+TB4049+`</td><td>`+MB5059+`</td><td>`+FB5059+`</td><td>`+TB5059+`</td><td>`+M60+`</td><td>`+F60+`</td><td>`+T60+`</td><td>${[T60+TB5059+TB4049+TB3039+TB2029+TB19]}</td></tr>`;
           
        
        appenddata += "<tr class='i'><td>" + [i+1] + "</td><td>"+data[0][i].Qualif_name+"</td><td>" + data[0][i].M19 + "</td><td>"+data[0][i].F19+"</td><td>"+data[0][i].T19+"</td><td> "+data[0][i].M29+" </td><td>"+data[0][i].F29+"</td><td>"+data[0][i].T29+"</td>  <td>"+data[0][i].M39+"</td><td>"+data[0][i].F39+"</td><td>"+data[0][i].T39+"</td><td>"+data[0][i].M49+"</td><td>"+data[0][i].F49+"</td><td>"+data[0][i].T49+"</td><td>"+data[0][i].M59+"</td><td>"+data[0][i].F59+"</td><td>"+data[0][i].T59+"</td><td>"+data[0][i].M60+"</td><td>"+data[0][i].F60+"</td><td>"+data[0][i].T60+"</td><td style='font-weight: 600;'>"+[data[0][i].T60+data[0][i].T59+data[0][i].T49+data[0][i].T39+data[0][i].T29+data[0][i].T19]+"</td></tr>";
        }  jQuery("#tbodyvalue").html(appenddata+Total);
        $('#tbodyvalue').clientSidePagination();
        
        }
        

        function fetchCategorywiseQualifiLevel(){
            var date1 = $("#m_datepicker_3").val().split('/')
            var date = date1[2]+"-"+date1[1]+"-"+date1[0]
        
            if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                var ex_id = $('#printreportproformaexchange').val();
            }
            else{
                var ex_id = sessionStorage.Ex_id
            }
        
            var path = serverpath +"CI_Pro_JS_Rpt_CategoryWise_QualifLevel/"+ex_id+"/"+date+""
            ajaxget(path,'parsedatafetchCategorywiseQualifiLevel','comment','control');
            }
           
            function parsedatafetchCategorywiseQualifiLevel(data,control){
            data = JSON.parse(data);
            var appenddata='';
             var  TMUR=0;var  TFUR=0;var  TUR=0;var  TMSC=0;var  TFSC=0;var  TSC=0;var  TMST=0;var  TFST=0;var  TST=0;var  TMOBC=0;var  TFOBC=0; TObc=0; 
              
            for (var i = 0; i < data[0].length; i++) {
               
                    TMUR=TMUR+data[0][i].MUR;
                    TFUR=TFUR+data[0][i].FUR;
                    TUR=TUR+data[0][i].TUR;
                    TMSC=TMSC+data[0][i].MSC;
                     TFSC=TFSC+data[0][i].FSC;
                      TSC=TSC+data[0][i].TSC;
                      TMST=TMST+data[0][i].MST;
                      TFST=TFST+data[0][i].FST;
                       TST=TST+data[0][i].TST;
                       TMOBC=TMOBC+data[0][i].MOBC;
                        TFOBC=TFOBC+data[0][i].FOBC;
                         TObc=TObc+data[0][i].TOBC;

                    Total="<tr class='total'><td></td><td>Total</td><td>"+TMUR+"</td><td>"+TFUR+"</td><td>"+TUR+"</td><td>"+TMSC+"</td><td>"+TFSC+"</td><td>"+TSC+"</td><td>"+TMST+"</td><td>"+TFST+"</td><td>"+TST+"</td><td>"+TMOBC+"</td><td>"+TFOBC+"</td><td>"+TObc+"</td></tr>"
               
            appenddata += "<tr class='i'><td>" + [i+1] + "</td><td>"+data[0][i].Qualif_Level_name+"</td><td>" + data[0][i].MUR + "</td><td>"+data[0][i].FUR+"</td><td>"+data[0][i].TUR+"</td><td> "+data[0][i].MSC+" </td><td>"+data[0][i].FSC+"</td><td>"+data[0][i].TSC+"</td>  <td>"+data[0][i].MST+"</td><td>"+data[0][i].FST+"</td><td>"+data[0][i].TST+"</td><td>"+data[0][i].MOBC+"</td><td>"+data[0][i].FOBC+"</td><td>"+data[0][i].TOBC+"</td></tr>";
            } jQuery("#tbodyvalue").html(appenddata+Total);
            $('#tbodyvalue').clientSidePagination();
            
            }
            
            function fetchCategorywiseQualifi(){
                var date1 = $("#m_datepicker_3").val().split('/')
                var date = date1[2]+"-"+date1[1]+"-"+date1[0]
            
                if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                    var ex_id = $('#printreportproformaexchange').val();
                }
                else{
                    var ex_id = sessionStorage.Ex_id
                }
            
                var path = serverpath +"CI_Pro_JS_Rpt_CategoryWise_QualifName/"+ex_id+"/"+date+""
                ajaxget(path,'parsedatafetchCategorywiseQualifi','comment','control');
                }
               
                function parsedatafetchCategorywiseQualifi(data,control){
                data = JSON.parse(data);
                var appenddata='';
                var totalmale=0; var totalfemale=0; var grandtotal=0;
                for (var i = 0; i < data[0].length; i++) {
                    totalmale=totalmale+data[0][i].MUR+data[0][i].MSC+data[0][i].MST+data[0][i].MOBC;
                    totalfemale=totalfemale+data[0][i].FUR+data[0][i].FSC+data[0][i].FST+data[0][i].FOBC;
                    grandtotal=grandtotal+data[0][i].TUR+data[0][i].TSC+data[0][i].TST+data[0][i].TOBC;
                appenddata += "<tr class='i'><td>" + [i+1] + "</td><td>"+data[0][i].Qualif_name+"</td><td>" + data[0][i].MUR + "</td><td>"+data[0][i].FUR+"</td><td>"+data[0][i].TUR+"</td><td> "+data[0][i].MSC+" </td><td>"+data[0][i].FSC+"</td><td>"+data[0][i].TSC+"</td>  <td>"+data[0][i].MST+"</td><td>"+data[0][i].FST+"</td><td>"+data[0][i].TST+"</td><td>"+data[0][i].MOBC+"</td><td>"+data[0][i].FOBC+"</td><td>"+data[0][i].TOBC+"</td><td>"+totalmale+"</td><td>"+totalfemale+"</td><td>"+grandtotal+"</td></tr>";
                } jQuery("#tbodyvalue").html(appenddata);
                $('#tbodyvalue').clientSidePagination();
                
                }
                


                function getreportname(){
                  var report=  $('#textReportName option:selected').html()
                  var rpt = report.split(".").join("");
                  var name = rpt
                  ExportExcel('tableglobal',name);
                }

                function checkvalidation(){
                    var rptid=$("#textReportName" ).val();

                    if(rptid=='101'){
                        if($('#printproformafrommonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='102'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='103'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='104'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='105'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='106'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='107'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='108'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='109'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='110'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='112'){
                        if($('#printproformafrommonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='113'){
                      
                        if($('#frmYear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='118'){
                        if($('#prpfrommonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#118prpfromyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        if($('#prptomonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#118prptoyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='119'){
                        if($('#prpfrommonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#118prpfromyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        if($('#prptomonth').val()=='0'){
                            toastr.warning('Please Select Month');
                            return false;
                        }
                        if($('#118prptoyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='123'){
                      
                        if($('#m_datepicker_5').val()==''){
                            toastr.warning('Please Select From Date');
                            return false;
                        }
                        if($('#m_datepicker_6').val()==''){
                            toastr.warning('Please Select To Date');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='124'){
                      
                        if($('#m_datepicker_3').val()==''){
                            toastr.warning('Please Select Date');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='126'){
                      
                        if($('#m_datepicker_3').val()==''){
                            toastr.warning('Please Select Date');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='127'){
                      
                        if($('#m_datepicker_3').val()==''){
                            toastr.warning('Please Select Date');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='128'){
                        if($('#prpfromyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        if($('#prptoyear').val()=='0'){
                            toastr.warning('Please Select Year');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                    if(rptid=='134'){
                      
                        if($('#m_datepicker_5').val()==''){
                            toastr.warning('Please Select From Date');
                            return false;
                        }
                        if($('#m_datepicker_6').val()==''){
                            toastr.warning('Please Select To Date');
                            return false;
                        }
                        else{
                            TableType(); 
                        }
                    }
                }
