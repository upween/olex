function ViewExchange(){
    var rptid=$("#textReportName").val();
    var FromDt='2020-01-01' ;
    var ToDt='2020-01-01';
if(rptid=='103' ||rptid=='104' ||rptid=='105' ||rptid=='106' ||rptid=='107' ||rptid=='108' ||rptid=='109' || rptid=='110')
{
 if($('#criteriaddl').val()=='1'){
     FromDt=`${$("#textyear").val()}-01-01`;
     ToDt=`${$("#textyear").val()}-06-30`;
 }
 else if($('#criteriaddl').val()=='2'){
     FromDt=`${$("#textyear").val()}-07-01`;
     ToDt=`${$("#textyear").val()}-12-31`;
 }
}
if(rptid=='102' || rptid=='113' || rptid=='114' || rptid=='115'){
 FromDt=`${$("#textyear").val()}-01-01`;
 ToDt=`${$("#textyear").val()}-12-31`;
}
    if ($("#textReport" ).val() == "1") {
       
    var path = serverpath + "secured/RptHOExchStatu/'"+FromDt+"'/'"+ToDt+"'/" + $("#monthlyregisterdatewisemonth").val() + "/" + $("#textyear").val() + "/" + $("#textReportName").val() + ""
    }
    else if ($("#textReport" ).val() == "2") {
        var path = serverpath + "secured/NotSendHOExchStatus/'"+FromDt+"'/'"+ToDt+"'/" + $("#monthlyregisterdatewisemonth").val() + "/" + $("#textyear").val() + "/" + $("#textReportName").val() + ""
    }
    securedajaxget(path, 'parsedataViewExchange', 'comment', "control");
 }
   function parsedataViewExchange(data){
    data = JSON.parse(data)
     
     $("#headingtext").html(sessionStorage.Ex_name+" </br> "+$('#textReport option:selected').html()+'-'+$('#textReportName option:selected').html())
    var appenddata='';
    if ($("#textReport" ).val() == "1") {
    var tablehead=`<th>Sr No</th><th>Exchange</th><th colspan=2>View Report</th>`;
    $('#tablehead').html(tablehead);
    }
    for (var i = 0; i < data[0].length; i++) {   
        if ($("#textReport" ).val() == "2") {
        appenddata +='<tr><td>'+[i+1]+'</td><td>'+data[0][i].Ex_name+'</td><td><button type="button" class="btn btn-success" onclick=setSessionrpt("'+data[0][i].Ex_id+'","'+$('#monthlyregisterdatewisemonth').val()+'","'+$("#textyear").val()+'","'+encodeURI(data[0][i].Ex_name)+'") style="background-color:#716aca;border-color:#716aca;">View Report</button></td><td><button type="button"  class="btn btn-success" onclick="revertReport('+data[0][i].Ex_id+')"  style="background-color:#716aca;border-color:#716aca;">Revert</button></td></tr>'
        }
        else{
            appenddata +='<tr><td>'+[i+1]+'</td><td>'+data[0][i].Ex_name+'</td><td><button type="button" class="btn btn-success" onclick=setSessionrpt("'+data[0][i].Ex_id+'","'+$('#monthlyregisterdatewisemonth').val()+'","'+$("#textyear").val()+'","'+encodeURI(data[0][i].Ex_name)+'") style="background-color:#716aca;border-color:#716aca;">View Report</button></td></tr>'
      
        }
    }
 jQuery("#tbodyvalue").html(appenddata); 
   }
   function FillMonthddl(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthddl('parsedatasecuredFillMonth',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}


function setSessionrpt(Exid,month,year,exname){
    sessionStorage.setItem('ES11Exid',Exid);
    sessionStorage.Monthrpt=month;
    sessionStorage.yearrpt=year;
    sessionStorage.Monthname=$('#monthlyregisterdatewisemonth option:selected').text();
    sessionStorage.Exname_es11=decodeURI(exname);
    sessionStorage.criteria=$('#criteriaddl').val();
    window.location='/ES11Reportforview';
    }
    function ES11RptSubmit(exid,month,year){

        var MasterData = {
            
      "p_Ex_id":exid,
      "p_Rpt_Month":month,
      "p_Rpt_Year":year,
      "p_User_Type_Id":'1',
      
         }
      MasterData = JSON.stringify(MasterData)
      var path = serverpath + "ES11_Submit_Rpt";
      securedajaxpost(path,'parsrdataES11RptSubmit','comment',MasterData,'control')
      }
      
      
      function parsrdataES11RptSubmit(data){
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            ES11RptSubmit();
            
        }
     
      
              toastr.success("Revert Successful", "", "success")
              return true;
       
         
        
         }

         function FillDDlReportName(){
        
            var path = serverpath + "secured/GetRptName/0/'JS'/1";
           securedajaxget(path, 'parsedatasecuredGetRptName', 'comment', 'control')
              }
        function parsedatasecuredGetRptName(data){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDDlReportName();
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    var appenddata="";
                 
                 jQuery("#textReportName").append(jQuery("<option ></option>").val("0").html("Select Report"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#textReportName").append(jQuery("<option></option>").val(data1[i].RptID).html(data1[i].RptNameEng));
                     }
                     
				$('#textReportName').val(sessionStorage.RptID?sessionStorage.RptID:'0');
                }  
                     
        }

        function handleDDlchange(){
            var rptid=jQuery("#textReportName").val();
            sessionStorage.RptID=rptid;
            if(rptid=='101' || rptid=='114' || rptid=='115'){
                $('.criteriaddl').hide();
                $('#lblyearmonth').text('Select Month & Year :');
                $('.monthddl, .yearddl').show();
            }
            if(rptid=='102' || rptid=='113'){
                $('.monthddl, .criteriaddl').hide();
                $('#lblyearmonth').text('Select Year :');
                $('.yearddl').show();
            }
            if(rptid=='103' ||rptid=='104' ||rptid=='105' ||rptid=='106' ||rptid=='107' ||rptid=='108' ||rptid=='109' || rptid=='110'){
                $('.monthddl').hide();
                $('#lblyearmonth').text('Select Year :');
                $('.yearddl,.criteriaddl').show();
            }
        }

        function revertReport(ex_id){
            var rptid=sessionStorage.RptID;
            if(sessionStorage.RptID=='101'){
                ES11RptSubmit(ex_id,$('#monthlyregisterdatewisemonth').val(),$("#textyear").val());
            }
            else{ 
               var FromDt ;
               var ToDt;
        if(rptid=='103' ||rptid=='104' ||rptid=='105' ||rptid=='106' ||rptid=='107' ||rptid=='108' ||rptid=='109' || rptid=='110')
           {
            if($('#criteriaddl').val()=='1'){
                FromDt=`${$("#textyear").val()}-01-01`;
                ToDt=`${$("#textyear").val()}-06-30`;
            }
            else if($('#criteriaddl').val()=='2'){
                FromDt=`${$("#textyear").val()}-07-01`;
                ToDt=`${$("#textyear").val()}-12-31`;
            }
           }
           if(rptid=='102' || rptid=='113'){
            FromDt=`${$("#textyear").val()}-01-01`;
            ToDt=`${$("#textyear").val()}-12-31`;
           }
           if(rptid=='114' || rptid=='115'){
            FromDt= $("#textyear").val()+'-'+$("#monthlyregisterdatewisemonth").val()+'-'+'01';
            ToDt=$("#textyear").val()+'-'+$("#monthlyregisterdatewisemonth").val()+'-'+'31';
           }
               
                 var MasterData = {
            
                "p_Ex_id":ex_id,
                "p_Year":$("#textyear").val(),
                "p_frmdt":FromDt,
                "p_todt":ToDt,
                "p_rptid":sessionStorage.RptID
                
                   }
                MasterData = JSON.stringify(MasterData)
                var path = serverpath + "RevertReport";
                securedajaxpost(path,'parsrdatarevertReport','comment',MasterData,'control')
                }
            }
                
                function parsrdatarevertReport(data){
                  data = JSON.parse(data)
                 if(data[0][0].ReturnValue=='1'){
                     toastr.success('Revert Successfully');
                 }
            }
        