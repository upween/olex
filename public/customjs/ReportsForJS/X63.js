
function FillHCCategory(funct,control) {
    var path =  serverpath + "secured/HCCategory/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillHCCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillHCCategory('parsedatasecuredFillHCCategory','DisabilityDetail');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Handicapped Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_Description).html(data1[i].HC_Description));
             }
        }
              
  }



function fetchx63(){
    var data =$('#m_datepicker_5').val();
    var arr = data.split('/');
    $("#m_datepicker_5").html("<span>"+arr[2] + "-" + arr[1]+"-"+arr[0]+" </span>");

    var Date =$('#m_datepicker_6').val();
    var datesplit = Date.split('/');
    $("#m_datepicker_6").html("<span>"+datesplit[2] + "-" + datesplit[1]+"-"+datesplit[0]+" </span>");
    var MasterData = {  
        "p_Ex_Id":$('#x63exchange').val(),
        "p_From":arr[2] +"-"+ arr[1]+"-"+arr[0],
        "p_To":datesplit[2] + "-" + datesplit[1]+"-"+datesplit[0],
        "p_Category":$("#x63category").val(),
        "p_Gender":$("#Gender").val(),
        "p_DifferentlyAbled":$("#HCCategory").val(),
        "p_Resident":$("#x63domicile").val(),
        "p_Area":$("#x63area").val()
        
    };
  
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "RptCIJSX63";
    ajaxpost(path, 'parsedatafetchx63', 'comment', MasterData, 'control')
  
    
    // if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
    // var Ex_id='0';
    // }
    // else{
    //     var Ex_id = $('#x63exchange').val();
    // }
   // var path =  serverpath + "ReportX63/"+$('#x63exchange').val()+"/'"+arr[2] + "-" + arr[1]+"-"+arr[0]+"'/'"+datesplit[2] + "-" + datesplit[1]+"-"+datesplit[0]+"'/"+$("#x63category").val()+"/'"+$("#Gender").val()+"'/'"+$(encodeURI("#HCCategory")).val()+"'/"+$("#x63domicile").val()+"/"+$("#x63area").val()+""
    
}
function parsedatafetchx63(data){  
    data = JSON.parse(data)
    
    
        var data1 = processSearchUtilityData(data[0]);
        var appenddata="";
         $('.fa').show();
        jQuery("#tbodyvalue").empty();  
      
        var sNo = 0;
var categorytext='All'
  if(sessionStorage.User_Type_Id=='1' || sessionStorage.User_Type_Id=='0'){
            var Exname=$('#x63exchange').val();
            }
            else{
                var Exname = sessionStorage.Ex_name
            }
            if($('#x63category').val()!='0'){
                categorytext=$('#x63category option:selected').html();
            }
        var head="<tr><th></th><th colspan=8>Report Name : Search for X-63<br>Exchange Name : "+$('#x63exchange option:selected').html()+"<br>From Date : "+$('#m_datepicker_5').val()+" - To Date : "+$('#m_datepicker_6').val()+"<br>Category : "+categorytext+" ,   Gender : "+$('#Gender').val()+",  Differently Abled : "+$('#HCCategory').val()+" ,  Domicile : "+$('#x63domicile option:selected').html()+",  Area : "+$('#x63area option:selected').html()+"</th></tr>";

        $("#headingtext").html(sessionStorage.Ex_name+" </br>X-63  ")

              var tablehead= "<tr><th style='border-top: ridge;border-right: ridge;'>Sno</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Reg.No & Reg. Date</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Name/ Father's or Husband's Name / Address</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>DOB/ Category/ Gender</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Area/ Domicile/PHP </th><th colspan=3 style='border-top: ridge;border-right: ridge;'>NCO</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Qualification</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>Entered By/Current Status</th><th colspan=1 style='border-top: ridge;border-right: ridge;'>JobSeeker History</th></tr>"+
            "<tr><th style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th style='border-top: ridge;border-right: ridge;'>Alloted NCO</th><th style='border-top: ridge;border-right: ridge;'>Seniority Date</th><th style='border-top: ridge;border-right: ridge;'>NCO Detail</th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th><th colspan=1 style='border-top: ridge;border-right: ridge;'></th></tr>";
            $("#tablehead").html(tablehead);
 $('#Detailhead').html(head);  
        for (var key in data1) {
            sNo++;
            var searchData = data1[key].data[0];
            var rowCount = data1[key].count;

            appenddata +=   `<tr >
                                <th style='vertical-align: middle' rowspan=${rowCount}>${sNo} </th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.RegistrationId} & ${searchData.RegDate} </th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.CandidateName} / ${searchData.GuardianFatherName}/${searchData.Address}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.DateOfBirth}<br> / ${searchData.CategoryName} / ${searchData.Gender}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount} >${searchData.Area_Name}/${searchData.Domicile}/${searchData.DifferentlyAbled}</th>
                            
                                <th>${searchData.NCO_Code}</th>
                                <th>${searchData.SeniorityDt}</th>
                                <th>${searchData.NCO_detail}</th>
                                <th>${searchData.Qualification}</th>                              
                                <th style='vertical-align: middle' rowspan=${rowCount}>${searchData.EnteredBy}/${searchData.CurrentStatus}</th>
                                <th style='vertical-align: middle' rowspan=${rowCount}>${searchData.JobseekerHistory}</th>
                             
                                </tr>`;
                       
            for(var i = 1; i < data1[key].count; i++) {
                console.log("in");
                searchData = data1[key].data[i];
                console.log(searchData);
                appenddata +=   `<tr>
                <th>${searchData.NCO_Code}</th>
                <th>${searchData.SeniorityDt}</th>
                <th>${searchData.NCO_detail}</th>
                <th>${searchData.Qualification}</th>
               
                                </tr>`;
            }
        

            
        }jQuery("#tbodyvalue").html(appenddata); 
        $('#tbodyvalue').clientSidePagination();
        
     //   addTopScrollbar($('#tbodyvalue1').closest('table'), $('#tbodyvalue1').closest('.form-group.m-form__group.row'));

    
}
function processSearchUtilityData(data){
var returnData = {};
for (var i = 0; i < data.length; i++) {
if(returnData.hasOwnProperty(data[i].RegistrationId)) {
    returnData[data[i].RegistrationId].count += 1;
} else {
    returnData[data[i].RegistrationId] = {data:[], count:1};
}
returnData[data[i].RegistrationId].data.push(data[i]);
}
return returnData;
}
        // for (var i = 0; i < data1.length; i++) {           

        //     var tablehead= "<tr><th style='border-top: ridge;border-right: ridge;'>Sno</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Reg.No & Reg. Date</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Name/Father's/ Husband Name & Address</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>DOB/ Category/ Gender</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Area/ Domicile/PHP </th><th colspan=3 style='border-top: ridge;border-right: ridge;'>NCO</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Qualification</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>Entered By/Current Status</th><th colspan=3 style='border-top: ridge;border-right: ridge;'>JobSeeker History</th></tr>"+
        //     "<tr><th style='border-top: ridge;border-right: ridge;'></th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th><th style='border-top: ridge;border-right: ridge;'>Alloted NCO</th><th style='border-top: ridge;border-right: ridge;'>Seniority Date</th><th style='border-top: ridge;border-right: ridge;'>NCO Detail</th><th style='border-top: ridge;border-right: ridge;'>Qualification</th><th style='border-top: ridge;border-right: ridge;'>Entry Date</th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th><th colspan=3 style='border-top: ridge;border-right: ridge;'></th></tr>";
        
        //    appenddata +="<tr  class='i'><th style='border-color: #b2b3c9;'>"+[i+1]+"</th><th colspan=3>"+data1[i].RegistrationId+" ,
      //   "+data1[i].RegDate+"</th><th colspan=3>"+data1[i].CandidateName+" / "+data1[i].GuardianFatherName+" / "+data1[i].Address+"</th><th colspan=3>"+data1[i].DateOfBirth+" <br>"+data1[i].Gender+"</th><th colspan=3>"+data1[i].Area_Name+" <br>"+data1[i].Domicile+"</th><th>"+data1[i].NCO_Code+"</th><th>"+data1[i].SeniorityDt+"</th><th>"+data1[i].NCO_detail+"</th><th>"+data1[i].Qualification+"</th><th></th><th colspan=3>"+data1[i].EnteredBy+" /"+data1[i].CurrentStatus+"</th><th colspan=3></th></tr>";

           
           
        // //    "<tr><td>"+data1[i].F1+"</td><td>"+split1[0]+"</td><td>"+split1[1]+"</td><td style='text-align: left;'>"+split2[0]+"</td><td style='text-align: left;'>"+split2[1]+"</td><td style='text-align: left;'>" +split2[2]+ "</td><td style='text-align: left;'>" + split3[0]+'/'+ split3[1]+'/'+ split3[2]+ "</td><td style='text-align: left;'>"+ split3[3]+"</td><td style='text-align: left;'>"+ split3[4]+"</td><td style='text-align: left;'>"+split4[0]+"</td><td style='text-align: left;'>"+split4[1]+"</td><td style='text-align: left;'>"+split4[2]+' , ' + data1[i].F6 +"</td><td>" + f7 +"</td><td>" + f8 +"</td><td style='text-align: left;'>" + f10 +"</td><td style='text-align: left;'>" + f13 +' , '+ f14 +"</td><td style='text-align: left;'>" + split5[0] +"</td><td style='text-align: left;'>" + split5[1] +"</td><td style='text-align: left;'>" + f17 +"</td></tr>";
            
        // }
        // $("#tablehead").html(tablehead);
        //  jQuery("#tbodyvalue").html(appenddata);  
        //  $('#tbodyvalue').clientSidePagination();
                    



        