function fetchYearWiseLRStatus(){
    
    var path =  serverpath + "YearWiseLRStatus/" + $("#textContactYear").val() + ""
    ajaxget(path,'parsedatafetchYearWiseLRStatus','comment',"control");
}
function parsedatafetchYearWiseLRStatus(data){  
    data = JSON.parse(data)
   var appenddata="";

   var sumLR=0  
  
$("#headingtext").html(sessionStorage.Ex_name+" </br>Yearly Registration and Renewal status "+$("#textContactYear").val() )
    var data1 = data[0];    
    var m=0;
    var sum_renewal=0;
 $('#excel').show();
var head='<th></th><th colspan=3>'+sessionStorage.Ex_name+' </br>Report Name : Yearly Registration and Renewal status '+$("#textContactYear").val() +'</th>';
$('#Detailhead').html(head)
            for (var i = 0; i < data1.length; i++) {
              
        m=1+i;

        if(data1[i].RegCount==null){
       var RegCount = '0'
        }
        else{
            var RegCount = data1[i].RegCount
        }
        sumLR+=parseInt(RegCount)  

        
        if(data1[i].RenCount==null){
            var RenCount = '0'
             }
             else{
                 var RenCount = data1[i].RenCount
             }
             sum_renewal+=parseInt(RenCount)  



     
       appenddata += "<tr><td style='    word-break: break-word;text-align: center;'>"+m+"</td><td style='word-break: break-word;'>" + data1[i].ex_name+ "</td><td style='    word-break: break-word;text-align: center;'>" + RegCount+ "</td><td style='    word-break: break-word;text-align: center;'>" + RenCount+ "</td></tr>";
       appenddata2 ="<tr class='total'><td></td><td>Total:</td><td >" + sumLR + "</td><td>" + sum_renewal + "</td></tr>"

        }jQuery("#tbodyvalue").html(appenddata+appenddata2);  
      
              
}



function FillYear(funct,control) {
    var path =  serverpath + "secured/year/0/10"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    $.unblockUI()
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYear('parsedatasecuredFillYear','textContactYear');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

            
        }
              
}