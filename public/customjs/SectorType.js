

function FillSectorType() {
    var path = serverpath +"sectortype/0/0" 
    ajaxget(path, 'parsedataSectorType', 'comment', "control");
}
function parsedataSectorType(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        if (data1[i].Active_YN == '0') {
        var	active = "No"
        }
         if (data1[i].Active_YN == '1') {
          var  active = "Yes"
        }
        appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].S_Id + "</td><td style='    word-break: break-word;'>" + data1[i].Des_cription  + "</td><td>"+active+"</td><td><button type='button' onclick=EditMode("+data1[i].S_Id+",'"+encodeURI(data1[i].Des_cription)+"','"+encodeURI(data1[i].Active_YN )+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);


}
function InsUpdSectorType(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_S_Id":localStorage.s_id,
    "p_Des_cription":jQuery("#Description").val(),
    "p_Active_YN":chkval,
    

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "sectortype";
ajaxpost(path, 'parsrdataSectorType', 'comment', MasterData, 'control')
}

function parsrdataSectorType(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdSectorType();
	}
	else if (data[0][0].ReturnValue == "1") {
        
        FillSectorType() 
        resetMode() ;
         toastr.success("Insert Successful", "", "success")
        
        
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        FillSectorType() 
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
       resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function CheckValidationLanguageType(){
  
    
    if(jQuery("#Description").val()==""){
          getvalidated('Description','text','Description');
          return false;
      }
      else{
         InsUpdSectorType()
            
      }
  }
  function EditMode(IDLanguage,Description,IsActive) {
    jQuery("#Description").val(decodeURI(Description))
    
    localStorage.s_id=IDLanguage
    if (decodeURI(IsActive) == '0') {
        $("#ActiveStatus")[0].checked = false;
    }
    else {
        $("#ActiveStatus")[0].checked = true;
    }
}
function resetMode() {
   
    localStorage.s_id= "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#Description").val("")
}