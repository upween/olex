

function parsedatasecuredFillexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillexchangeoffice('parsedatasecuredFillexchangeoffice',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option></option>").val('0').html('Select Exchange'));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
             jQuery("#"+control).val(sessionStorage.Ex_id);
        }
              
}

function fetchempCount(funct,control) {
    var path =  serverpath +"empcountbyjfId/"+sessionStorage.JobfairId+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatafetchempCount(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fetchempCount('parsedatafetchempCount',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            $('#NoOfEmployers'+sessionStorage.Roworder+'').val(data[0][0].EmployerCount);
           $('#NoOfVacancies'+sessionStorage.Roworder+'').val(data[0][0].Vacancy);
           $('#NoOfCandidatesSelected'+sessionStorage.Roworder+'').val(data[0][0].NoofCandidates);
           $('#NoOfCandidatesPrimary'+sessionStorage.Roworder+'').val(data[0][0].NoofCandidateSelected_Primary);
           $('#NoOfCandidatesOfferd'+sessionStorage.Roworder+'').val(data[0][0].NoofCanddiateSelected_Offered);
           $('.empinput').val('');
        }
              
}

function addrow(){

    var x = $('.addrow').length;
    var row = `<div class='addrow' id='addrow`+x+`'>
    <div class="form-group m-form__group row">
                           
                                <div class="col-lg-3">
                                        <input type="text" id="jobfairempname`+x+`" class="form-control m-input empinput" placeholder="Employer Name">
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" id="Sector`+x+`" class="form-control m-input empinput" placeholder="Sector">
                            </div>
                            <div class="col-lg-3">
                                <input type="text" id="Vacancy`+x+`" class="form-control m-input empinput" placeholder="Vacancy">
                        </div>
                        <div class="col-lg-3">
                            <select type="text" id="jobfairreportdis`+x+`" class="form-control m-input" placeholder="Plese Enter Employer Name"></select>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <div class="col-lg-2">
                        <input type="text" id="Salary`+x+`" class="form-control m-input empinput" placeholder="Salary">
                </div> 
                <div class="col-lg-2">
                  <input type="number" id="NoofCandidates`+x+`" class="form-control m-input empinput" placeholder="No. Of Candidate Selected">
          </div>
          <div class="col-lg-3">
              <input type="number" id="NoofCandidateSelected_Primary`+x+`" class="form-control m-input empinput" placeholder="Primary Selection">
      </div>
      <div class="col-lg-3">
          <input type="number" id="NoofCanddiateSelected_Offered`+x+`" class="form-control m-input empinput" placeholder="No. of Candidates Recieved Offer Letter">
      </div>
                <div class="col-lg-2" style="margin-top: -1%;">
                <button type="button" onclick="$('#addrow`+x+`').remove();"   class="btn btn-success" style="background-color:#716aca;border-color:#716aca;">
                Remove</button>
                  </div>
                        </div> 
</div>`;
    $('#newdiv').append(row);
    Fillexchangeoffice('parsedatasecuredFillexchangeoffice',"jobfairreportdis"+x+"");
}

function addemployer(){
    $('.addrow').each(function (i) {
   
    var MasterData ={
        "p_EmpId":'0',
        "p_JobFair_Id":sessionStorage.JobfairId,
        "p_EmployerName":$('#jobfairempname'+i+'').val(),
        "p_EntryBy":sessionStorage.CandidateId,
        "p_Sector":$('#Sector'+i+'').val(),
        "p_Vacancy":$('#Vacancy'+i+'').val(),
        "p_DistrictId":$('#jobfairreportdis'+i+'').val(),
        "p_Salary":$('#Salary'+i+'').val(),
        "p_NoofCandidates":$('#NoofCandidates'+i+'').val(),
        "p_NoofCandidateSelected_Primary":$('#NoofCandidateSelected_Primary'+i+'').val(),
        "p_NoofCanddiateSelected_Offered":$('#NoofCanddiateSelected_Offered'+i+'').val()

    }


    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "exchangejobfairEmp";
    ajaxpost(path, 'parsrdataaddemployer', 'comment', MasterData, 'control')
});
$('#myModal').modal('toggle');
//FetchJobfairReport();
fetchempCount('parsedatafetchempCount','');       
}
    function parsrdataaddemployer(data) {
        data = JSON.parse(data)
        if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                addemployer();
        }
        else if (data[0][0].ReturnValue == "1") {
            fetchempCount('parsedatafetchempCount','');  
        }
      

    }


    function FetchJobfairReport() {
        if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
            var Exid = $('#ddlExchangeName').val();
        }
        else{
            var Exid = sessionStorage.Ex_id
        }
        
        var fd = $('#m_datepicker_6').val().split('/')
        var fromdate = fd[2]+'-'+fd[1]+'-'+fd[0]
        
        var td = $('#m_datepicker_5').val().split('/')
        var todate = td[2]+'-'+td[1]+'-'+td[0]
        
            var path = serverpath + "softskillreport/"+Exid+"/"+fromdate+"/"+todate+""
            securedajaxget(path, 'parsedataFetchJobfairReport', 'comment', 'control');
        }
        function parsedataFetchJobfairReport(data, control) {
            data = JSON.parse(data)
            if (data.message == "New token generated") {
                sessionStorage.setItem("token", data.data.token);
                FetchJobfairReport();
            }
            else {
                var data1 = data[0];
                var appenddata ='';
                var Total ='';
          
        console.log(data1);
     
        
                for (var i = 0; i < data1.length; i++) {
                 
        
     var	tablehead ="<th onclick='sortTable(0);'>S.No.</th><th>District</th><th style='cursor: pointer;' >Soft Skill Training Date</th>";
        $('#tablehead').html(tablehead);
        appenddata +=`<tr class="jf" id='${data1[i].SoftSkill_Id}'>
        <td>${[i+1]}</td>
        <td>${data1[i].Ex_name}</td>
        <td>${data1[i].SoftSkill_Title}  (${data1[i].SoftSkill_FromDt} To ${data1[i].SoftSkill_ToDt})</td>
        </tr>`;
        
    
     
            }
            
            $('#tbodyvalue').html(appenddata);
            $('#tbodyvalue').clientSidePagination();
          }
        }



        function insjobfairReport(JobFair_Id,i){
            if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
                var Exid = $('#jobfairreportexchange').val();
            }
            else{
                var Exid = sessionStorage.Ex_id
            }
           
            var MasterData ={
                "p_Ex_id":Exid,
                "p_JobFair_Id":JobFair_Id,
                "p_NoOfEmployers":$('#NoOfEmployers'+i+'').val(),
                "p_NoOfVacancies":$('#NoOfVacancies'+i+'').val(),
                "p_NoOfCandidates_Selected":$('#NoOfCandidatesSelected'+i+'').val(),
                "p_NoOfCandidates_Primary":$('#NoOfCandidatesPrimary'+i+'').val(),
                "p_NoOfCandidates_Offerd":$('#NoOfCandidatesOfferd'+i+'').val()

                }
        
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "ExchangeWiseJF";
            ajaxpost(path, 'parsrdataFilljobfairReport', 'comment', MasterData, 'control')
   

  
        }
            function parsrdataFilljobfairReport(data) {
                data = JSON.parse(data)
                 console.log(data)
                if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        insjobfairReport();
                }
                else{
                   toastr.success('Save Successfully');
                }
                // else if (data[0][0].ReturnValue == "1") {                  
                // }             
              }
        
              function reset(){
                  $('.NoOfEmployers,.NoOfVacancies,.NoOfCandidatesSelected,.NoOfCandidatesPrimary,.NoOfCandidatesOfferd').val("");
                  $('#tablehead,#tbodyvalue').html("");
              }