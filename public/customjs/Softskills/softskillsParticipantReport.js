





function CheckCandidate(){
    var seletedid=''
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;
 }
  else{
    updSelectionStatus();
    
  }
}

function FillSoftSkill(Ex_Id){
  
            var path =  serverpath + "fetchsoftskill/0/"+$('#ddlExchangeName').val()+"/0"
     
    securedajaxget(path,'parsedatasecuredSoftSkill','comment',"control");
      }
      function parsedatasecuredSoftSkill(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSoftSkill();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                jQuery("#jobfairddl").empty();
                jQuery("#jobfairddl").append(jQuery("<option ></option>").val("0").html("Select Soft Skills"));
                for (var i = 0; i < data1.length; i++) {
                  
   jQuery("#jobfairddl").append(jQuery("<option></option>").val(data1[i].SoftSkill_Id).html(data1[i].SoftSkill_Title + ' ' + data1[i].SoftSkill_FromDt + ' To ' + data1[i].SoftSkill_ToDt));
                    
                    
                }
        }    
                  
    }


   
   

     function fetchCandidateList() {
        
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "reminder/"+$('#jobfairddl').val()+"/SoftSkill",
            cache: false,
            dataType: "json",
            success: function (data) {
        var data1= data[0]
        if(data[0].length){
                      var appenddata="";
                      sessionStorage.Title=data[0][0].Title;
                      sessionStorage.Details=data[0][0].Details;
                      sessionStorage.Venue=data[0][0].Venue;
                      sessionStorage.FromDate=data[0][0].FromDate;
                      sessionStorage.EventType=data[0][0].EventType;
                      sessionStorage.ZoomLink=data[0][0].ZoomLink;
                      sessionStorage.TimeRange=data[0][0].TimeRange;
                      var tablehead="<th class='notinclude'><input type='checkbox' onclick='checkAll()' id='selectall'></th><th><a  style='color: white;'>SrNo</a></th><th>Candidate Name</th><th>Email Id</th><th>Mobile Number</th>";
                      $("#tablehead").html(tablehead);
                    for (var i = 0; i < data1.length; i++) {
                        
                    
                
                    appenddata += "<tr><td style='word-break: break-word;' class='notinclude'><input name='checkbox'  type='checkbox' id=" + data1[i].MobileNumber + " value=" + data1[i].CandidateId+'_'+data1[i].EmailId + "></td><td >" +[i+1]+ "</td><td>"+data1[i].CandidateName+"</td><td class='can_email'>"+data1[i].EmailId+"</td><td class='can_mobile'>"+data1[i].MobileNumber+"</td></tr>";
                    }
                   
                     $('#tbodyvalue').html(appenddata);
                     $('.show').show()
                }
               
              }

                    });
                }
            

                function CheckCandidate(type){
                    var seletedid=''
                    var radios = document.getElementsByName("checkbox");
                    var formValid = false;
                
                    var i = 0;
                    while (!formValid && i < radios.length) {
                        if (radios[i].checked)
                            formValid = true;
                        i++;
                    }
                
                    if (!formValid) {
                
                        $(window).scrollTop(0);
                        toastr.warning("Please Select Atleast One Candidate", "", "info")
                        return false;
                 }
                  else{
                      if(type=='email'){
                                    if(sessionStorage.EventType=='Online'){
var heading=`<p style='font-size:14px'>Please find link  below to attend soft skills training online:</p>`
var ifoffline=`<p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join The Soft Skill Training</a></strong></p>`
        
          }
          else{
            var heading=`<p style='font-size:14px'>Please find details below to attend soft skils training :</p>`
            var ifoffline=`<p style='font-size:14px'><strong>Venue:</strong>${sessionStorage.Venue}</p>`
         
          }
          var subject='YASHASWI INVITES YOU FOR ATTENDING ONLINE SOFT SKILL CRASH COURSE AS TRAINEE FOR EVENT !!';
 var dt1 = new Date(sessionStorage.FromDate.split('/')[1]+'/'+sessionStorage.FromDate.split('/')[0]+'/'+sessionStorage.FromDate.split('/')[2])
    // console.log('date',dt1);
      var dt=dt1.toLocaleString('en-us', {weekday:'long'})
        // var dt=new Date(sessionStorage.FromDate).toLocaleString('en-us', {weekday:'long'});
          var body=`<p style='font-size:14px'>Dear Candidate,</p>
                    
                    <p style='font-size:14px'>
As per present situation of Covid- 19 which we all are facing where we can not call candidates to our office and can not do much activity impersonal.
                   </p>
                   
                    <p style='font-size:14px'> 

I feel proud to inform you that to resolve the problem of MP candidates, Yashaswi has developed the Virtual platform for conducting soft skill training through our "mprojgar.gov.in" portal.</p>
                  
                   <p style='font-size:14px'>
                   
By this facility the trainee and soft skill trainer both can come to one platform and candidates would able to do training without coming out of their houses. They can avail this facility only by registering themselves on our portal www.mprojgar.gov.in</p>
                  
                   <p style='font-size:14px'>
In response to your interest shown by you to attending Online Soft Skill Crash Course we request you to attend your 7 days free course by clicking below mention link and can take the benefit of this new developed facility.
</p>
<br>
<p style='font-size:14px'><strong> We would like to invite you for attending Online Soft Skill Crash Course as Trainee for Event.</strong></p>
                    <p style='font-size:14px;color:red'><strong >Day & Date : </strong>${dt} i.e. ${sessionStorage.FromDate}</p> 
                    <p style='font-size:14px;color:red'><strong> Time: </strong> ${sessionStorage.TimeRange}</p>   
<p style='font-size:14px;color:red'><strong>Soft Skills Training Link:</strong><p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join  Soft Skills Training</a></strong></p>`;     

          $('#subjecttxt').val(subject);
          $('.note-editable').html(body);
                        $('#m_modal_5').modal('toggle');
                      }
                      else{
                        $('#m_modal_6').modal('toggle');
                      }
                    
                  }
                }          
// function CheckCandidate(type){

//       if(type=='email'){
//           if(sessionStorage.EventType=='Online'){
// var heading=`<p style='font-size:14px'>Please find link  below to attend jobfair online:</p>`
// var ifoffline=`<p style='font-size:14px'><strong><a href='${sessionStorage.ZoomLink}'>Click Here To Join The ${$('#formtype option:selected').text()}</a></strong></p>`
        
//           }
//           else{
//             var heading=`<p style='font-size:14px'>Please find details below to attend jobfair :</p>`
//             var ifoffline=`<p style='font-size:14px'><strong>Venue:</strong>${sessionStorage.Venue}</p>`
         
//           }
//           var subject=sessionStorage.EventType+' '+$('#formtype option:selected').text();
//           var body=`<p style='font-size:14px'>Dear Candidate</p>
//           <br>
//           ${heading}
//           <br>
//           <p style='font-size:14px'><strong> ${$('#formtype option:selected').text()} Title: </strong>${sessionStorage.Title}</p> 
//           <p style='font-size:14px'><strong> ${$('#formtype option:selected').text()} Date: </strong> ${sessionStorage.FromDate}</p>
//           ${ifoffline} `
//           $('#subjecttxt').val(subject);
//           $('.note-editable').html(body);
//         $('#m_modal_5').modal('toggle');
//       }
//       else{
//         $('#m_modal_6').modal('toggle');
//       }
    
//   }

 
      
function SendAlert(type) {
   
    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function ()  {
       var emailID=this.value.split('_')[1];
       //var mobNo=$(this).find('td.can_mobile').text();
      // console.log('emailID',emailID);
        if (type == 'email') {
          sentmailglobal(emailID,$('.note-editable').html(),$('#subjecttxt').val(),'helpdesk.mprojgar@mp.gov.in');

                //reset();
          toastr.success('Mail Sent Successfully')
            
        }
        if (type == 'mobile') {
            // sendemail(this.value)
            sentSmsGlobal(this.id,$('#SmsDetail').val(),'1307159886762924438');
            toastr.success('SMS Sent Successfully')
           // reset();
        }

    });
   
}
function checkAll() {
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').prop('checked', true);
    } else {
        $('input:checkbox').prop('checked', false);
    }
}
    
    