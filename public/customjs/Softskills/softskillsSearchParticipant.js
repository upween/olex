
function FillQualification() {
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

    var path = serverpath + "qualification/0/0/0"
    securedajaxget(path, 'parsedataFillQualification', 'comment', 'control');
}
function parsedataFillQualification(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillQualification();
    }
    else {
        var data1 = data[0];
        $('#Qualification').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Qualif_name',
            labelField: 'Qualif_name',
            searchField: 'Qualif_name',
            options: data1,
            create: true
        });
    }
}
function FillDesignation() {
    var path = serverpath + "adminDesignation/0/0"
    securedajaxget(path, 'parsedataFillDesignation', 'comment', 'control');
}
function parsedataFillDesignation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillDesignation();
    }
    else {
        var data1 = data[0];
        $('#Designation').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Designation',
            labelField: 'Designation',
            searchField: 'Designation',
            options: data1,
            create: true
        });
    }
}
function FillLocation() {
    var path = serverpath + "district/19/0/0/0"
    securedajaxget(path, 'parsedataFillLocation', 'comment', 'control');
}
function parsedataFillLocation(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillLocation();
    }
    else {
        var data1 = data[0];
        $('#Location').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'DistrictName',
            labelField: 'DistrictName',
            searchField: 'DistrictName',
            options: data1,
            create: true
        });
    }
}


function FillSearchCandidate() {

    var qualification = '0'
    var Designation = '0'
    var Location = '0'
    var Gender = '0'
    var MinAge = '0'
    var MaxAge = '0'
    var MinSalary = '0'
    var MaxSalary = '0'

   
        if ($('#Qualification').val() != '') {
            qualification = $('#Qualification').val()
        }
       if ($('#Designation').val() != '') {
            Designation = $('#Designation').val()
        }
        if ($('#Location').val() != '') {
            Location = $('#Location').val()
        }
        if ($('#Gender').val() != '') {
            Gender = $('#Gender').val()
        }
        if ($('#MinAge').val() != '') {
            MinAge = $('#MinAge').val()
        }
        if ($('#MaxAge').val() != '') {
            MaxAge = $('#MaxAge').val()
        }
        if ($('#MinSalary').val() != '') {
            MinSalary = $('#MinSalary').val()
        }
        if ($('#MaxSalary').val() != '') {
            MaxSalary = $('#MaxSalary').val()
        }
    if(!sessionStorage.SoftSkill_Id ){
      var SoftSkillId = $('#SoftSkill').val();
    }
    else {
        var SoftSkillId = sessionStorage.SoftSkill_Id
            }
if($('#m_datepicker_2').val()==''){
    var fromday= '' 
}else{
            var fd1= $('#m_datepicker_2').val().split('/')
            var fromday= fd1[2]+'-'+fd1[1]+'-'+fd1[0]
}
if($('#m_datepicker_5').val()==''){
    var today= '' 
}else{
    var td1= $('#m_datepicker_5').val().split('/')
    var today= td1[2]+'-'+td1[1]+'-'+td1[0]
}
      
            // 23/11/2020


        var MasterData ={
    
            "p_EducationId":qualification,
            "p_DesignationId":Designation,
            "p_DistrictId":Location,
            "p_Gender":Gender,
            "p_MinAge":MinAge,
            "p_MaxAge":MaxAge,
            "p_MinSalary": MinSalary,
            "p_MaxSalary":MaxSalary,
            "p_Limit":$('#noofcandidate').val()==''?100:$('#noofcandidate').val(),
            "p_SoftSkill_Id":SoftSkillId, 
            "p_From":fromday,
            "p_To":today, 
            
            
            };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "candidateSoftSkill";
        securedajaxpost(path, 'parsedataSearchCandidate', 'comment', MasterData, 'control')
   
}
function parsedataSearchCandidate(data) {

    data = JSON.parse(data)
    var data1 = data[0];
    var appenddata = "";
    var active = "";
    for (var i = 0; i < data1.length; i++) {

        var tablehead = "<th><a  style='color: white;'><input type='checkbox' onclick='checkAll()' id='selectall'>Total Count: " + data1.length + "</a></th><th>Name</th><th>Mobile</th><th>Email</th>";
        $("#tablehead").html(tablehead);
        appenddata += "<tr class='i'><td style='word-break: break-word;' ><input name='checkbox'  type='checkbox' id=" + data1[i].MobileNumber + " value=" + data1[i].CandidateId+'_'+data1[i].EmailId + "></td><td style='    word-break: break-word;'>" + data1[i].CandidateName + "</td><td>" + data1[i].MobileNumber + "</td><td>" + data1[i].EmailId + "</td></tr>";
    } 
    jQuery("#tbodyvalue").html(appenddata);

    
    $('#tbodyvalue').clientSidePagination();
    $("#sendAlert").show()
}
function insParticipant(candidateid){
var MasterData ={
    
    "p_SoftSkillParticipantId":'0',
    "p_SoftSkill_Id":sessionStorage.SoftSkill_Id?sessionStorage.SoftSkill_Id:$('#SoftSkill').val(),
    "p_CandidateId":candidateid,
    "p_Selection_Status":'0',
    "p_EntryBy":sessionStorage.CandidateId,
    "p_IpAddress":sessionStorage.ipAddress,
    "p_EmpId":0
    
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "softskillparticipant";
securedajaxpost(path, 'parsedataparticipant', 'comment', MasterData, 'control')

}
function parsedataparticipant(data) {
    data = JSON.parse(data)
    console.log(data);
}

function checkAll() {
    if ($('#selectall').is(':checked')) {
        $('input:checkbox').prop('checked', true);
    } else {
        $('input:checkbox').prop('checked', false);
    }
}
function SendAlert(type) {
   
    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
        insParticipant(this.value.split('_')[0]);
        if (type == 'email') {
            if($("#JFDetail").val()==""){
                 getvalidated('JFDetail','text','Content')
                 return false;
            }
            else{
                sendemail(this.value.split('_')[1])
                reset();
            }
            
        }
        if (type == 'mobile') {
            // sendemail(this.value)
            sentSmsGlobal(this.id,$('#SmsDetail').val(),'1307159886762924438');
            reset();
            $('#m_modal_6').modal('toggle');
        }

    });
  
   

}
function sendemail(emailid) {
    var sub = $('#subjecttxt').val();
    var body = `<div style='font-size:14px;color:darkgreen'>
    </div>
    </br>
    </br>
    <div style='font-size:13px;'>
    ${$('.note-editable').html().replace("'"," ")}</div></br>`;
        sentmailglobal(emailid,body,sub,'');
        $('#m_modal_5').modal('toggle');
}

// $('.note-editable').html().replace("'"," "),
// $('#JFDetail').val()

function reset() {
    // $('input[type=checkbox]').attr('checked',false);
    $('input:checkbox').each(function () { this.checked = false; });
}


function FillTradeName() {
    var path = serverpath + "trademaster"
    securedajaxget(path, 'parsedataFillTradeName', 'comment', 'control');
}
function parsedataFillTradeName(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillTradeName();
    }
    else {
        var data1 = data[0];
        $('#Trade').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'TradeName',
            labelField: 'TradeName',
            searchField: 'TradeName',
            options: data1,
            create: true
        });
    }
}

function goBack() {
    window.history.back()
}

function lengthCheck(){

    if ($("#MinAge").val() < '14'){
        $("#validMinAge").html("Age cannot be less than 14")
        $("#MinAge").val("")
        $("#MinAge").focus()
    }
   else if ($("#MaxAge").val() > '65'){
         $("#validMaxAge").html("Age cannot be greater than 65")
         $("#MaxAge").val("")
         $("#validMaxAge").val("")
         $("#MaxAge").focus()
       }
  else  if ($("#MinAge").val() > $("#MaxAge").val()){
        $("#validMaxAge").html("Age cannot be less than " +$("#MinAge").val())
        $("#MaxAge").val("")
    }

      else{
        $("#validMaxAge").html("")
       
     }
    }


function CheckCandidate(type){
    var seletedid=''
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;
 }
  else{
      if(type=='email'){
        $('#m_modal_5').modal('toggle');
      }
      else{
        $('#m_modal_6').modal('toggle');
      }
    
  }
}



function FillSoftSkillDetail(){

    if(sessionStorage.User_Type_Id=='0' || sessionStorage.User_Type_Id=='1'){
        var Ex = $('#exchange').val();
    }
    else{
       var Ex = sessionStorage.Ex_id 
    }

    var path =  serverpath + "fetchsoftskill/0/"+Ex+"/0"
    securedajaxget(path,'parsedatasecuredFillSoftSkillDetail','comment',"control");
}
function parsedatasecuredFillSoftSkillDetail(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSoftSkillDetail();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#SoftSkill").empty();
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                if(data1[i].Active_YN=='1'){
                jQuery("#SoftSkill").append(jQuery("<option></option>").val(data1[i].SoftSkill_Id).html(data1[i].SoftSkill_Title));
                }
            }
           
    }    
}