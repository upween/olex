jQuery('#ddlExchangeName').on('change', function () {
    FillSoftSkillDetail(jQuery('#ddlExchangeName').val());
      });
     function FillSoftSkillDetail(Ex_Id){
        var path =  serverpath + "fetchsoftskill/0/"+Ex_Id+"/0"
        securedajaxget(path,'parsedatasecuredFillSoftSkillDetail','comment',"control");
    }
    function parsedatasecuredFillSoftSkillDetail(data){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSoftSkillDetail(jQuery('#ddlExchangeName').val());
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var data1 = data[0];
                var appenddata="";
                var active="";
                for (var i = 0; i < data1.length; i++) {
               if(data1[i].Active_YN=='0'){
        active="N"
               }
               else if(data1[i].Active_YN=='1'){
                active="Y"
                       }
                       if(data1[i].Status=='0'){
                        status='Not Verified'
                      disabled=false
                        
                       }
                       else if(data1[i].Status=='1'){
                        status='Verified';
                        disabled=true
                        
                       }
                       else if(data1[i].Status=='2'){
                        status='Reject'
                        disabled=true
                       
                       }
                       
           appenddata += "<tr><td >" + data1[i].SoftSkill_Id+ "</td><td><a href='#' onclick=JFDetails('"+data1[i].SoftSkill_Id+"','"+encodeURI(data1[i].Ex_name)+"','"+encodeURI(data1[i].SoftSkill_Title)+"','"+encodeURI(data1[i].SoftSkill_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].SoftSkill_FromDt)+"','"+encodeURI(data1[i].SoftSkill_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"')>" + data1[i].SoftSkill_Title+ "</a></td><td style='    word-break: break-word;'>"+data1[i].Venue+"</td><td >" + data1[i].SoftSkill_FromDt+ "</td><td>"+data1[i].SoftSkill_ToDt+"</td><td>"+active+"</td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' id='btnModify' onclick=editMode('"+data1[i].SoftSkill_Id+"','"+data1[i].Ex_id+"','"+encodeURI(data1[i].SoftSkill_Title)+"','"+encodeURI(data1[i].SoftSkill_Details)+"','"+encodeURI(data1[i].Venue)+"','"+encodeURI(data1[i].SoftSkill_FromDt)+"','"+encodeURI(data1[i].SoftSkill_ToDt)+"','"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].SoftSkill_link)+"','"+encodeURI(data1[i].SoftSkill_Type )+"')>Modify</button></td><td><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' data-toggle='modal' data-target='#m_modal_5' onclick=setJfID('"+data1[i].SoftSkill_Id+"','"+data1[i].Ex_id+"') id='btnDelete'>Delete</button></tr>";
            }jQuery("#tbodyvalue").html(appenddata);  
        }    
        //onclick=Delete("+data1[i].SoftSkill_Id+",'JFDetail')        
    }
    function setJfID(id,ex_id){
        localStorage.JFId=id;
        localStorage.Ex_Id=ex_id;
        localStorage.JFtype='ajf'
        
    }
    
function CheckValidation(){
if($("#JFTitle").val()==""){
    getvalidated('JFTitle','text','Title');
    return false;
}
// if($("#JFVenue").val()==""){
//     getvalidated('JFVenue','text','Venue')
//     return false;
// }
if($("#m_datepicker_5").val()==""){
    getvalidated('m_datepicker_5','text','From Date');
    return false;
}
if($("#m_datepicker_6").val()==""){
    getvalidated('m_datepicker_6','text','To Date')
    return false;
}
else{
    //document.getElementById("myBtn").click();
    //InsUpdSoftSkillDetail();
     sessionStorage.ModalType='SoftSkillFile'
     sessionStorage.SoftSkillTitle=$("#JFTitle").val()
     sessionStorage.SoftSkillVenue=$("#JFVenue").val()
     sessionStorage.SoftSkill_FromDate=$("#m_datepicker_5").val();
     sessionStorage.SoftSkill_ToDate=$("#m_datepicker_6").val();
     sessionStorage.Exchange=$("#addjobfairexchange").val();
     var chkval = $("#chkActiveStatus")[0].checked
     if (chkval == true){
        sessionStorage.chkval = "1"
     }else{
        sessionStorage.chkval="0"
     }
    $("#jfpostattach").click();
    
    
}

}
if( sessionStorage.ModalType=='SoftSkillFile'){
    if($("#JFMsg").html()==" File Uploaded!"){
        InsUpdSoftSkillDetail()
   
}

else if($("#JFMsg").html()==" Error: Document Only!"){
    toastr.warning("Please select Document file only", "", "success")

}
}
$('#jfupload').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function(response) {
            if (response == "Error No File Selected For GST Upload"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Request Entity Too Large"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Document Only!"){
                toastr.warning(response, "", "info")
            }
            else{
                var str = response;
                console.log(str);
                var res = str.split("!");
                sessionStorage.setItem("JFDoc", res[1])
               InsUpdSoftSkillDetail();
              
            }
        }
    });
    return false;
 });


function InsUpdSoftSkillDetail(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
    }
    var SoftSkill_FromDt = sessionStorage.SoftSkill_FromDate.split(" ")
    var SoftSkill_FromDt1 = SoftSkill_FromDt[0].split("/")
    var SoftSkill_FromDate = SoftSkill_FromDt1[2] + "-" + SoftSkill_FromDt1[1] + "-" + SoftSkill_FromDt1[0] ;

    var SoftSkill_ToDt =sessionStorage.SoftSkill_ToDate.split(" ")
    var SoftSkill_ToDt1 = SoftSkill_ToDt[0].split("/")
    var SoftSkill_ToDate = SoftSkill_ToDt1[2] + "-" + SoftSkill_ToDt1[1] + "-" + SoftSkill_ToDt1[0] ;
   
var MasterData ={
    
    "p_SoftSkill_Id":localStorage.JFId,
	"p_Ex_id":$('#addjobfairexchange').val(),
	"p_SoftSkill_Title":sessionStorage.SoftSkillTitle,
	"p_Venue":sessionStorage.SoftSkillVenue,
	"p_SoftSkill_Details":$("#JFDetail").val(),
	"p_Attachment_Filename":sessionStorage.getItem("JFDoc"),
	"p_Date_of_Entry": "0",
	"p_SoftSkill_FromDt":SoftSkill_FromDate+' '+SoftSkill_FromDt[1],
	"p_SoftSkill_ToDt":SoftSkill_ToDate+' '+SoftSkill_ToDt[1],
	"p_SoftSkill_CreatedBy":sessionStorage.CandidateId,
	"p_Status":"0",
	"p_Verified_By":"",
	"p_Verified_Dt":"",
	"p_Reject_Reason":"",
	"p_Active_YN":sessionStorage.chkval,
	"p_LMDT":"0",
	"p_LMBY":sessionStorage.CandidateId,
    "p_SoftSkill_link":$("#zoomlink").val(),
    "p_SoftSkill_Type":$("#form").val()
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "softskill";
securedajaxpost(path, 'parsrdataitInsUpdSoftSkillDetail', 'comment', MasterData, 'control')
}
function parsrdataitInsUpdSoftSkillDetail(data) {
    data = JSON.parse(data)
     console.log(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdSoftSkillDetail();
	}
	else if (data[0][0].ReturnValue == "1") {
        sessionStorage.SoftSkill_Id=data[0][0].SoftSkill_Id;
        window.location='/softskillsSearchParticipant';
        toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
         resetmode()
         FillSoftSkillDetail('5')
         $('#addjobdetails').hide();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
          resetmode()
          FillSoftSkillDetail('5')
	toastr.warning("Already Exist", "", "info")
			return true;
	}
 
}
function resetmode(){
    localStorage.JFId='0',
    $("#addjobfairexchange").val("5"),
    $("#JFTitle").val(""),
    $("#JFVenue").val(""),
    $("#chkActiveStatus")[0].checked=false;
    $("#m_datepicker_5").val("");
    $("#m_datepicker_6").val("");
    FillSoftSkillDetail('5');
    $("#JFDetail").val("")
    $("#SoftSkillFile").text("")
    $("#zoomlink").val("")
    $("#form").val("")
    sessionStorage.ModalType=''
    Cookies.set('modaltype', '', { expires: 1, path: '/'} )
    sessionStorage.SoftSkillTitle=''
    sessionStorage.SoftSkillVenue=''
    sessionStorage.SoftSkill_FromDate=''
    sessionStorage.SoftSkill_ToDate=''
    sessionStorage.Exchange=''
    sessionStorage.chkval=''

    }
    function searchTextInTable(tblName) {
        var tbl;
        tbl = tblName;
        jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.
    
        var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value
    
        if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
        {
            jQuery("#" + tbl + " tr:has(td)").show();
            return false;
        }
        //Iterate through all the td.
        jQuery("#" + tbl + " tr:has(td)").children().each(function () {
            var cellText = jQuery(this).text().toLowerCase();
            if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
            {
                jQuery(this).parent().show();
                return true;
            }
        });
        //e.preventDefault();
    }
    function editMode(SoftSkill_Id,Ex_id,SoftSkill_Title,SoftSkill_Details,Venue,SoftSkill_FromDt,SoftSkill_ToDt,Active_YN,SoftSkill_link,SoftSkill_Type )
    {
        
       // Cookies.set('JFTitle', SoftSkill_Title, { expires: 1, path: '/' });
        $('#addjobdetails').show()
        window.location='#addjobdetails';
        localStorage.JFId=SoftSkill_Id,
    $("#addjobfairexchange").val(Ex_id),
    $("#JFTitle").val(decodeURI(SoftSkill_Title)),
    $("#JFVenue").val(decodeURI(Venue)),
    
    $("#m_datepicker_5").val(decodeURI(SoftSkill_FromDt));
    $("#m_datepicker_6").val(decodeURI(SoftSkill_ToDt));
    $("#zoomlink").val(decodeURI(SoftSkill_link));
    $("#form").val(decodeURI(SoftSkill_Type));
    $("#JFDetail").val(decodeURI(SoftSkill_Details))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
        }
        if ( SoftSkill_Type == 'Online')
        {
          $(".dropdown").show();
          $('.venuetxt').hide();
        }
        else
        {
          $(".dropdown").hide();
          $('.venuetxt').show();
        }
        $(window).scrollTop(0);

    }
    function JFDetails(CC_Id,Ex_name,CC_Title,CC_Details,Venue,CC_FromDt_c,CC_ToDt,Active_YN){
        $('#JFdetailsById').show();
        $('#searchjobdetail').hide();
        $('#ExchangeById').html(decodeURI(Ex_name))
        $('#TitleById').html(decodeURI(CC_Title))
        $('#DetailById').html(decodeURI(CC_Details))
        $('#VenueById').html(decodeURI(Venue))
        $('#FrmdtById').html(decodeURI(CC_FromDt_c))
        $('#ToDtById').html(decodeURI(CC_ToDt))
        if(decodeURI(Active_YN)=='0'){
            $("#chkActiveStatusbyId")[0].checked=false;
            }
            else{	
                $("#chkActiveStatusbyId")[0].checked=true;
            }
        
                }
        //         function callme(type){
        //             sessionStorage.setItem("OpenModal",type);
                    
		// 	sessionStorage.setItem("JFFileName", $('#SoftSkillFile').text())
		// 	if ($("#JFMsg").html() == " File Uploaded!") {

		// 		CheckValidation();
		// 	}
		// 	else if ($("#JFMsg").html() == " Error: No File Selected!") {
		// 		toastr.warning("No File Selected", "", "info")

		// 	}
		// 	else if ($("#JFMsg").html() == " Error: Document Only!") {
		// 		toastr.warning("Please select document file only", "", "success")

			




		// }
                   
        //         }
               
    
                function FillSecuredexchangeoffice1(funct,control) {
                    var path =  serverpath +"secured/GetOfficeReport/101"
                    securedajaxget(path,funct,'comment',control);
                }
                
                function parsedatasecuredFillSecuredexchangeoffice1(data,control){  
                    data = JSON.parse(data)
                    if (data.message == "New token generated"){
                        sessionStorage.setItem("token", data.data.token);
                        FillSecuredexchangeoffice1('parsedatasecuredFillSecuredexchangeoffice1','addjobfairexchange');                     }
                    else if (data.status == 401){
                        toastr.warning("Unauthorized", "", "info")
                        return true;
                    }
                        else{
                            jQuery("#"+control).empty();
                            var data1 = data[0];
                            for (var i = 0; i < data1.length; i++) {
                                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
                             }
                            if(sessionStorage.User_Type_Id !=0 && sessionStorage.User_Type_Id!=1){
                                console.log('in')
                                $('#addjobfairexchange').val(sessionStorage.Ex_id); 
                            }

                        }
                              
                }

                function Checkpercentage(){

                    if ($("#Percentage").val() < '35'){
                        $("#validPercentage").html("Age cannot be less than 35")
                         $("#Percentage").focus()
                    }
                
                    
                   else if ($("#Percentage").val() > '100'){
                         $("#validPercentage").html("Age cannot be greater than 100")
                         $("#Percentage").val("")
                        
                         $("#Percentage").focus()
                       }
                
                
                
                      else{
                        $("#validPercentage").html("")
                       
                     }
                    }
   





                    // function sortTable() {
                    //     var table, rows, switching, i, x, y, shouldSwitch;
                    //     table = document.getElementById("tbodyvalue");
                    //     switching = true;
                    //     /*Make a loop that will continue until
                    //     no switching has been done:*/
                    //     while (switching) {
                    //       //start by saying: no switching is done:
                    //       switching = false;
                    //       rows = table.rows;
                    //       /*Loop through all table rows (except the
                    //       first, which contains table headers):*/
                    //       for (i = 1; i < (rows.length - 1); i++) {
                    //         //start by saying there should be no switching:
                    //         shouldSwitch = false;
                    //         /*Get the two elements you want to compare,
                    //         one from current row and one from the next:*/
                    //         x = rows[i].getElementsByTagName("TD")[0];
                    //         y = rows[i + 1].getElementsByTagName("TD")[0];
                    //         //check if the two rows should switch place:
                    //         if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //           //if so, mark as a switch and break the loop:
                    //           shouldSwitch = true;
                    //           break;
                    //         }
                    //       }
                    //       if (shouldSwitch) {
                    //         /*If a switch has been marked, make the switch
                    //         and mark that a switch has been done:*/
                    //         rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    //         switching = true;
                    //       }
                    //     }
                    //   }


                 
