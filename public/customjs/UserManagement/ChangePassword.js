function ChangePassword(){
  if($("#currentpass").val()==sessionStorage.CandidatePassword){
if(jQuery('#chngpass').val()==jQuery('#confrmpass').val()){

    var MasterData ={
 
        "p_UserName":sessionStorage.CandidateUserName,
        "p_Pass":md5(jQuery('#chngpass').val()),
         };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "AdmProUserUpdatePwd";
        securedajaxpost(path, 'parsrdataChangePassword', 'comment', MasterData, 'control') 
}
else{
    toastr.warning("Password Not Match", "", "info")
    return false;
}
}
else{
    toastr.warning("Wrong Current Password", "", "info")
    return false;
}
}
function parsrdataChangePassword(data) {

data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			ChangePassword();
    }
    else{
      var MasterData1={"PrevPass":sessionStorage.CandidatePassword}
      sessionStorage.MasterData = JSON.stringify(MasterData1);
      Insupduserloginlog(sessionStorage.Emp_Id,'PasswordChange');
      toastr.success("Password Changed Successfully", "", "success");
      sessionStorage.CandidatePassword=jQuery('#chngpass').val();
			return true;

    }
}
function checkValidation(){
  if($("#currentpass").val()==''){
    toastr.warning("Please Enter Current Password", "", "info")
    return false;
  }
else  if($("#chngpass").val()==''){
    toastr.warning("Please Enter Password", "", "info")
    return false;
  }
 else if($("#confrmpass").val()==''){
    toastr.warning("Please Confirm Password", "", "info")
    return false;
  }
  else{
    validateCaptchaheader();
  }
}
var codeheader;
                function createcaptchaheader() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaheader').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaheader";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeheader = captcha.join("");
                  document.getElementById("captchaheader").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaheader() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBoxheader").value == codeheader) {
                    ChangePassword()
                    
                  }else{
                    jQuery('#cpatchaTextBoxheader').val("");
                    toastr.warning("Invalid Captcha", "", "info");
                    createcaptchaheader();
                    jQuery('#cpatchaTextBoxheader').css('border-color', 'red'); 
                    return true;
                    
                  }
                }
    
           