function FillExchangeOfficeType() {
    var path =  serverpath + "officetype/"
    ajaxget(path,"parsedataFillExchangeOfficeType",'comment',"control");
}

function parsedataFillExchangeOfficeType(data){  
    data = JSON.parse(data)
  
      
            jQuery("#ddlExchangeType").empty();
            var data1 = data[0];
             
            jQuery("#ddlExchangeType").append(jQuery("<option ></option>").val("0").html("Select Exchange Type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#ddlExchangeType").append(jQuery("<option></option>").val(data1[i].Ex_Type_id).html(data1[i].Ex_Type_Name));
             }
             
        
              
}
function InsupdExchangeOffice(){
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData ={
    "p_Ex_id":localStorage.ExchangeOffice,
    "p_Ex_Type_id":$("#ddlExchangeType").val(),
    "p_District_id":$("#ddlDistrict").val(),
    "p_Ex_name":$("#txtExchangename").val(),
    "p_Ex_NameH":$("#txtExnamehindi").val(),
    "p_Emp_id":"0",
    "p_Address":$("#txtAddress").val(),
    "p_AddressH":$("#txtAddressH").val(),
    "p_Email_id":$("#txtemail").val(),
    "p_STDCode":$("#txtSTD").val(),
    "p_Ophone1":$("#txtOphone1").val(),
    "p_Ophone2":$("#txtOphone2").val(),
    "p_Mobile_no":$("#Mobile").val(),
    "p_LMBY":sessionStorage.CandidateName,
    "p_Client_IP":sessionStorage.IpAddress,
    "p_Active_YN":chkval,
    "p_s_no":$("#txtSno").val(),
    "p_open_YN":"1"
    };
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/AdminExchangeoffice";
securedajaxpost(path, 'parsrdataitInsupdExchangeOffice', 'comment', MasterData, 'control')
}


function parsrdataitInsupdExchangeOffice(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsupdExchangeOffice();
	}
	else if (data[0][0].ReturnValue == "1") {
        resetmode()
        FillExchangeOfficeTable() 
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        resetmode()
        FillExchangeOfficeTable() 
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetmode()
        FillExchangeOfficeTable() 
	toastr.warning("already exist", "", "info")
			return true;
	}
 
}
function FillExchangeOfficeTable() {
    var path =  serverpath + "exchangeoffice/0/0/0"
    securedajaxget(path,'parsedatasecuredFillExchangeOfficeTable','comment',"control");
}

function parsedatasecuredFillExchangeOfficeTable(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExchangeOfficeTable();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
            var active="";
            for (var i = 0; i < data1.length; i++) {
           if(data1[i].Active_YN=='0'){
    active="N"
           }
           else if(data1[i].Active_YN=='1'){
            active="Y"
                   }
       appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Ex_id+ "</td><td style='    word-break: break-word;'>" + data1[i].Ex_name+ "</td><td style='    word-break: break-word;'>" + data1[i].Ex_NameH+ "</td><td>"+data1[i].DistrictName+"</td><td>"+data1[i].s_no+"</td><td>"+active+"</td><td><button type='button' onclick=editMode('"+data1[i].Ex_id+"','"+data1[i].Ex_Type_id+"','"+data1[i].District_id+"','"+encodeURI(data1[i].Ex_name)+"','"+encodeURI(data1[i].Ex_NameH)+"','"+encodeURI(data1[i].Address)+"','"+encodeURI(data1[i].AddressH)+"','"+encodeURI(data1[i].Email_id)+"','"+encodeURI(data1[i].STDCode)+"','"+encodeURI(data1[i].Ophone1)+"','"+encodeURI(data1[i].Ophone2)+"','"+encodeURI(data1[i].Mobile_no)+"','"+encodeURI(data1[i].s_no)+"','"+encodeURI(data1[i].Active_YN)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' onclick=Delete('"+data1[i].Ex_id+"','Exchangeoffice') style='background-color:#716aca;border-color:#716aca;'>Delete</button></td></tr>";
        }jQuery("#tbodyvalue").html(appenddata);  
    }    
              
}
function editMode(Ex_id,Ex_Type_id,District_id,Ex_name,Ex_NameH,Address,AddressH,Email_id,STDCode,Ophone1,Ophone2,Mobile_no,s_no,Active_YN){
    localStorage.ExchangeOffice=Ex_id,
    $("#ddlExchangeType").val(Ex_Type_id),
    $("#ddlDistrict").val(District_id),
    $("#txtExchangename").val(decodeURI(Ex_name)),
    $("#txtExnamehindi").val(decodeURI(Ex_NameH)),
    $("#txtAddress").val(decodeURI(Address)),
    $("#txtAddressH").val(decodeURI(AddressH)),
    $("#txtemail").val(decodeURI(Email_id)),
    $("#txtSTD").val(decodeURI(STDCode)),
    $("#txtOphone1").val(decodeURI(Ophone1)),
    $("#txtOphone2").val(decodeURI(Ophone2)),
    $("#Mobile").val(decodeURI(Mobile_no))
    if(decodeURI(Active_YN)=='0'){
		$("#chkActiveStatus")[0].checked=false;
		}
		else{	
			$("#chkActiveStatus")[0].checked=true;
		}
}
function resetmode(){
    localStorage.ExchangeOffice='0',
    $("#ddlExchangeType").val('0'),
    $("#ddlDistrict").val('1'),
    $("#txtExchangename").val(''),
    $("#txtExnamehindi").val(''),
    $("#txtAddress").val(''),
    $("#txtAddressH").val(''),
    $("#txtemail").val(''),
    $("#txtSTD").val(''),
    $("#txtOphone1").val(''),
    $("#txtOphone2").val(''),
    $("#Mobile").val('')
    $("#chkActiveStatus")[0].checked=false;
}