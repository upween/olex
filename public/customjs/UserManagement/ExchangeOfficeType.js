function InsUpdExchOfficeType(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_Ex_Type_id":jQuery("#ExchangeTypeId").val(),
    "p_Ex_Type_Name":jQuery("#ExchangeTypeName").val(),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/officetype";
securedajaxpost(path, 'parsrdataExchangeOfficeType', 'comment', MasterData, 'control')
}

function parsrdataExchangeOfficeType(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdExchOfficeType();
	}
	else if (data[0][0].ReturnValue == "1") {
		FillExchangeOfficeType();
		resetMode();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		FillExchangeOfficeType();
		resetMode();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
	toastr.info("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {

    jQuery("#ExchangeTypeId").val("");
    jQuery("#ExchangeTypeName").val("");
    $("#ActiveStatus").prop("checked", false);
}

function FillExchangeOfficeType() {
    var path =  serverpath + "secured/officetype"
    securedajaxget(path,'parsedatasecuredFillExchangeOfficeType','comment','control');
}

function parsedatasecuredFillExchangeOfficeType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExchangeOfficeType();
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            
            var appenddata ="";
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                appenddata +="<tr><td scope=row>"+ i +"</td><td>"+ data1[i].Ex_Type_id +"</td><td>"+ data1[i].Ex_Type_Name +"</td><td>"+ data1[i].Active_YN+"</td><td><button type='button' onclick=editMode('"+data1[i].Ex_Type_id+"','"+encodeURI(data1[i].Ex_Type_Name)+"','"+encodeURI(data1[i].Active_YN)+"') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=Delete('"+data1[i].Ex_Type_id+"','officetype')>Delete</button></td></tr>"
                
             }
             jQuery("#tbodyvalue").html(appenddata);
        }
              
}
function editMode(Ex_Type_id,Ex_Type_Name,Active_YN){
	jQuery('#m_scroll_top').click();
	localStorage.ExchangeOffice=Ex_Type_id,
	$("#ExchangeTypeId").val(decodeURI(Ex_Type_id))
    $("#ExchangeTypeName").val(decodeURI(Ex_Type_Name))
    if(decodeURI(Active_YN)=='0'){
		$("#ActiveStatus")[0].checked=false;
		}
		else{	
			$("#ActiveStatus")[0].checked=true;
		}
}
jQuery("#txtSearch").keyup(function () {
	searchTextInTable("tbodyvalue");
})



