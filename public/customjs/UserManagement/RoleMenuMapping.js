
function FillUserType(funct,control) {
    var path = serverpath +"secured/olexusertype"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillUserType(data,control){
    data = JSON.parse(data);
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillUserType('parsedatasecuredFillUserType','usertype'); }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    jQuery("#"+control).append(jQuery("<option ></option>").val("A").html("Select User Type"));
    
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].User_Type_Id).html(data1[i].User_Name));
    }
    }
    
    }
    function FetchMenu() {
    var path = serverpath + "AdminMenu/0/1"
    ajaxget(path, 'parsedataFetchMenu', 'comment', "control");
    }
    function parsedataFetchMenu(data) {
    data = JSON.parse(data)
    
    
    
    
    var data1 = data[0];
    var appenddata="";
    var active="";
    for (var i = 0; i < data1.length; i++) {
    
    appenddata += "<tr><td><input type='checkbox' name='checkbox' id='chk_"+data1[i].Menu_Id+"' /></td><td style=' word-break: break-word;'>" + data1[i].Menu_Name+ "</td></tr>";
    }jQuery("#tbodyvalue").html(appenddata);
    
    
    }
    function checkAll(){
    if ($('#selectall').is(':checked')) {
    $('input:checkbox').attr('checked', true);
    } else {
    $('input:checkbox').attr('checked', false);
    }
    }
    
    function checkValidation(){
    var radios = document.getElementsByName("checkbox");
    var formValid = false;
    
    var i = 0;
    while (!formValid && i < radios.length) {
    if (radios[i].checked)
    formValid = true;
    i++;
    }
    
  

    if (!formValid) {
    
    $(window).scrollTop(0);
    toastr.warning("Please Select Atleast One Menu", "", "info")
    return false;
    
    }
    else {
        $('#tbodyvalue').find('input[type="checkbox"]:unchecked').each(function () {
            DeleteMenus(this.id.substring(4));
        });

    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
    InsUpdateRoleMenu(this.id.substring(4));
    
    });
    resetmode();
        toastr.success('Insert Successful');
    }
    
    
    
    }
    
    function InsUpdateRoleMenu(menuid){
    var MasterData ={
    "p_RoleMenu_Id":'0',
    "p_User_Type_Id":$('#usertype').val(),
    "p_Menu_Id":menuid,
    "p_Active_YN":'1'
    
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "rolemenumapping";
    securedajaxpost(path, 'parsedatasecuredInsUpdateRoleMenu', 'comment', MasterData, 'control')
    }
    function parsedatasecuredInsUpdateRoleMenu(data){
    data = JSON.parse(data)
    console.log(data);
   
    }
    
    
    function resetmode(){
        $('#tbodyvalue').find('input[type="checkbox"]:checked').prop('checked', false);
    $('#usertype').val('A');
    }
    function rolemenumapped() {
    var path = serverpath +"rolemenumapping/"+$('#usertype').val()+""
    securedajaxget(path,'parsedatarolemenumapped','comment');
    }
    
    function parsedatarolemenumapped(data){
    data = JSON.parse(data);
    console.log(data);
    var data1=data[0];
    $('#tbodyvalue').find('input[type="checkbox"]:checked').prop('checked', false);
    for (var i = 0; i < data1.length; i++) {
    $('#tbodyvalue input[type="checkbox"]#chk_' + data1[i].Menu_Id).prop('checked', true);
    }
    }


    function DeleteMenus(menuid){
        var path = serverpath +"DeteteMenus/"+$('#usertype').val()+"/"+menuid+"/0/rolemenu"
        securedajaxget(path,'parsedataDeleteMenus','comment');
        }
        function parsedataDeleteMenus(data){
            data = JSON.parse(data);
            if(data[0][0].ReturnValue=='1'){
                console.log(data);
            }
            }