
   jQuery('#searchutilitysector').on('change', function () {
    FillJobPrefrences('parsedatasecuredFillJobPrefrences','searchutilityfuncarea',jQuery('#searchutilitysector').val());
       });

function FillJobPrefrences(funct,control,ESID) {
    var path =  serverpath + "secured/jobprefrences/'" + ESID + "'/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJobPrefrences(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobPrefrences('parsedatasecuredFillJobPrefrences','searchutilityfuncarea',jQuery('#searchutilitysector').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
           jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Functional Area"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
             }
        }
              
}

//Qualificaion
function FillQualiLevel(funct,control) {
    var path =  serverpath + "secured/Qualification/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillQualiLevel(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillQualiLevel('parsedatasecuredFillQualiLevel','QualificationLevel');
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
         jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Qualification Level"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_Level_id).html(data1[i].Qualif_Level_name));
            }
        }
                  
}
jQuery('#QualificationLevel').on('change', function () {
    FillExamPassed('parsedatasecuredFillExamPassed','ExamPassed',jQuery('#QualificationLevel').val());
         
         
  });
function FillExamPassed(funct,control,qual_level) {
    var path =  serverpath + "secured/qualification/0/0/"+qual_level+""
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExamPassed(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExamPassed('parsedatasecuredFillExamPassed','ExamPassed',jQuery('#QualificationLevel').val());
       
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#"+control).empty();
			 jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Exam Passed"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
            }
        }
                  
}

jQuery('#ExamPassed').on('change', function () {
  //  FillCourse(jQuery('#ExamPassed').val());
  FillSubjectGroup('parsedatasecuredFillSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
       
       
});


function FillSubjectGroup(funct,control,EducationId) {
    var path =  serverpath + "secured/subjectgroup/0/'" + EducationId + "'/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSubjectGroup(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSubjectGroup('parsedatasecuredFillSubjectGroup','SubjectGroup',jQuery('#ExamPassed').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
        else{
            jQuery("#"+control).empty();
			 jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Subject Group"));
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Subject Group"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
             }
        }
    }

 
     function FillDistrictbyDivision(funct,control) {
        var path =  serverpath + "DistrictWithDivision/"+$('#Division').val()+""
        securedajaxget(path,funct,'comment',control);
    }
    function parsedatasecuredFillDistrictbyDivision(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillDistrictbyDivision('parsedatasecuredFillDistrictbyDivision',control);
         
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                // jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District Name"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
                 }
                 if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
                    jQuery("#"+control).val(sessionStorage.DistrictId);
                    }
            }
                  
    }

    function FilldivExchange(funct,control) {
        var path =  serverpath + "MstDivision/0/1"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFilldivExchange(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FilldivExchange('parsedatasecuredFilldivExchange','Division');
         
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Division Name"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Division_id).html(data1[i].Division_name));
                 }
                 if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
                 jQuery("#"+control).val(sessionStorage.DivisionId)
                 FillDistrictbyDivision('parsedatasecuredFillDistrictbyDivision','District');
                 }
            }
                  
    }


 

    function InsSearchUtility(){
        var qualif_YN='0';
        var age_yn='0';
        var lang_yn='0';
        var regdt_yn='0';
        if($('#ExamPassed').val()){
            qualif_YN='1';
        }
        else{
            qualif_YN='0';
        }
        if($('#textminage').val()!=''||$('#textmaxage').val()!=''){
            age_yn='1';
        }
        else{
            age_yn='0';
        }
        if($('#searchutilitylanguage').val()!='0'||$('#searchutilitytstype').val()!='0'){
            lang_yn='1';
        }
        else{
            lang_yn='0';
        }
        if($('#m_datepicker_2').val()!=''||$('#m_datepicker_3').val()!=''){
            regdt_yn='1';
        }
        else{
            regdt_yn='0';
        }
        if($('#nco1').val()==''&& $('#nco2').val()=='' && $('#nco3').val()==''){
            nco_yn='0';
        }
        else{
            nco_yn='1';
        }
        if($('#nco1').val()==''&& $('#nco2').val()=='' && $('#nco3').val()==''){
            nco_yn='0';
        }
        else{
            nco_yn='1';
        }
    var MasterData = {
        
                "p_Gender":$("#GenderName").val()==''?0:$("#GenderName").val(),
                "p_Category_id":$("#searchengineutilitycategory").val()==''?0:$("#searchengineutilitycategory").val(),
                "p_NoOFCandidates":$("#MaxNoOfCandidateShortList").val()==''?0:$("#MaxNoOfCandidateShortList").val(),
                "p_Division_id":$("#Division").val()==''?0:$("#Division").val(),
                "p_District_id":$("#District").val()==''?0:$("#District").val(),
                "p_Ex_Id":$('#searchengineutilityexchange').val()==''?0:$('#searchengineutilityexchange').val(),
                "p_Qualif_YN":qualif_YN,
                "p_Qualif_Level_id":$('#QualificationLevel').val()==''?0:$('#QualificationLevel').val(),
                "p_Qualif_id":$('#ExamPassed').val()?$('#ExamPassed').val():'0',
                "p_Sub_Group_id":$('#SubjectGroup').val()?$('#SubjectGroup').val():'0',
                "p_Q_Division_id":$('#searchutilitydivision').val()?$('#searchutilitydivision').val():'0',
                "p_Q_PassingYear":$('#passedYear').val()?$('#passedYear').val():'0',
                "p_Criteria_Age_YN":age_yn,
                "p_minAge":$('#textminage').val()==''?0:$('#textminage').val(),
                "p_maxAge":$('#textmaxage').val()==''?0:$('#textmaxage').val(),
                "p_Asondate":$('#m_datepicker_1').val()==''?'0':$('#m_datepicker_1').val(),
                "p_Criteria_TS_YN":lang_yn,
                "p_Lang_id":$('#searchutilitylanguage').val()==''?0:$('#searchutilitylanguage').val(),
                "p_Type_id":$('#searchutilitytstype').val()==''?0:$('#searchutilitytstype').val(),
                "p_Speed":$('#textSpeed').val()==''?0:$('#textSpeed').val(),
                "p_TS_Pass_Year":$('#LanguagepassedYear').val()==''?0:$('#LanguagepassedYear').val(),
                "p_Criteria_Reg_YN":regdt_yn,
                "p_Fromdate":$('#m_datepicker_2').val()==''?0:$('#m_datepicker_2').val(),
                "p_Tomdate":$('#m_datepicker_3').val()==''?0:$('#m_datepicker_3').val(),
                "p_NCO_YN":nco_yn,
                "p_NCO_m":$('#nco3').val(),
                "p_NCO_1":$('#nco1').val(),
                "p_NCO_2":$('#nco2').val(),
                "p_ph_YN":$('#searchutilityhandicapcheck').val()==0?0:1,
                "p_HC_Category_id":$('#searchutilityhandicap').val()==''?'0':$('#searchutilityhandicap').val(),
                "p_ph_all":$('#searchutilityhandicap').val()==0?1:0,
                "p_City_id":'0',
                "p_Location_id":'0',
                "p_FunctionalArea_id":'0'
             }
      
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "secured/DataBaseSearch";
        securedajaxpost(path,'parsrdataInsSearchUtility','comment',MasterData,'control')
    }

    function parsrdataInsSearchUtility(data){
        data = JSON.parse(data)
        if (data.message == "New token generated"){
          sessionStorage.setItem("token", data.data.token);
          InsSearchUtility();
          
      }
      else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    var data1 = data[0];
                    var appenddata="";
                    jQuery("#tbodyvalue1").empty();  
                  console.log(data)
                    for (var i = 0; i < data1.length; i++) {
                     
                            appenddata += "<tr><td><input name='checkbox' value='"+data1[i].Mobile+" , "+data1[i].EmailId+"' type='checkbox'></td><td>" +[i+1]+ "</td><td>" + data1[i].ex_name+ "</td><td>" + data1[i].Regno+ "&<br>"+data1[i].Regdt+"</td><td>"+data1[i].Js_Name+"</td><td>"+data1[i].Mobile+"</td><td></td></tr>";
                   
                   }jQuery("#tbodyvalue1").html(appenddata);  
                  
                }
    }
    function FillSecuredexchangeofficedatasearch(funct,control) {
        var path =  serverpath +"secured/exchangeoffice/0/0/1"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecureddatasearch(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillSecuredexchangeofficedatasearch('parsedatasecureddatasearch',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                var ex_id='';
                console.log(data)
                jQuery("#"+control).empty();
                var data1 = data[0];
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
               if(sessionStorage.User_Type_Id=='2' || sessionStorage.User_Type_Id=='7'){
                jQuery("#"+control).val(sessionStorage.Ex_id);
               }
               else{
                if(data1[i].District_id==$('#District').val()){
                    var ex_id=data1[i].Ex_id;
                }
                }
                 jQuery("#"+control).val(ex_id);
               }
                   
            }
                  
    }
    function checkAll(){
        if ($('#selectall').is(':checked')) {
            $('input:checkbox').prop('checked', true);
        } else {
            $('input:checkbox').prop('checked', false);
        }
    }
    function AddCandidate(){
        var radios = document.getElementsByName("checkbox");
        var formValid = false;
    
        var i = 0;
        while (!formValid && i < radios.length) {
            if (radios[i].checked)
                formValid = true;
            i++;
        }
    
        if (!formValid) {
    
            $(window).scrollTop(0);
            toastr.warning("Please Select Atleast One Candidate", "", "info")
            return false;
    
       }
        else {
            var candidatemailid=''
     $('#tbodyvalue1').find('input[type="checkbox"]:checked').each(function () {
        var candidatedetail1 = this.value.split(",")
        candidatemailid+=candidatedetail1[0]+',' ;
        
  
       
     });
     $('#mailTo').val(candidatemailid.substring(0,candidatemailid.length-1));
    }
    
    
    
    }