function sendMailtoCandidate() {
    var FromDt = $('#m_datepicker_1').val().split(" ");
    var FromDt1 = FromDt[0].split("/");
    var FromDate = FromDt1[2] + "-" + FromDt1[1] + "-" + FromDt1[0] ;

    var ToDt = $('#m_datepicker_2').val().split(" ")
    var ToDt1 = ToDt[0].split("/")
    var ToDate = ToDt1[2] + "-" + ToDt1[1] + "-" + ToDt1[0] ;

    var path = serverpath + "getcandidatelistformail/"+FromDate+"/"+ToDate+""
    securedajaxget(path, 'parsedatasendmail', 'comment', 'control');
  }
  
  function parsedatasendmail(data) {
    data = JSON.parse(data);
    $.unblockUI();
    jQuery("#tbodyvalue").empty();
	if(!data[0].length||data[0]==[]) {
  toastr.warning('No records found','info');
}
else{
  var appenddata='';
  for (var i = 0; i < data[0].length; i++) {
    sentmailglobal(data[0][i].EmailId,$('.note-editable').html(),$('#subject').val());
  }
}
  }

function  checkValidation(type){
  if(isValidation){
    if($('#subject').val()==''){
        getvalidated('subject','text','Subject');
        return true;
    }
   else if($('.note-editable').html()=='<p><br></p>'){
        getvalidated('body','text','Body');
        return true;
    }
   else if($('#m_datepicker_1').val()==''){
        getvalidated('m_datepicker_1','text','From Date');
        return true;
    }
   else if($('#m_datepicker_2').val()==''){
        getvalidated('m_datepicker_2','text','To Date');
        return true;
    }
    else{
if(type=='test'){
    sentmailglobal('gunjan.gautam@yashaswigroup.in,Siiddharth.shrivastava@yashaswi.edu.in',$('.note-editable').html(),$('#subject').val())
}
else{
  sendMailtoCandidate();
}
  }
}

else{
    sendMailtoCandidate();
}
    }
