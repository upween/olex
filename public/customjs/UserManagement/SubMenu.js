function FillMenu() {
	var path = serverpath + "AdminMenu/0/0"
	ajaxget(path, "parsedataFillMenu", 'comment', "control");
}

function parsedataFillMenu(data) {
	data = JSON.parse(data)


	jQuery("#ddlMenu").empty();
	var data1 = data[0];

	jQuery("#ddlMenu").append(jQuery("<option ></option>").val("0").html("Select Menu"));
	for (var i = 0; i < data1.length; i++) {
		jQuery("#ddlMenu").append(jQuery("<option></option>").val(data1[i].Menu_Id).html(data1[i].Menu_Name));
	}
}

function CheckValidation() {
	var chkval = $("#chkActiveStatus")[0].checked
	if(isValidation){
	if (jQuery("#ddlMenu").val() == '') {
		getvalidated("ddlMenu", "text", "Menu Name")
		return false;
	}
	if (jQuery("#textsubmenu").val() == '') {
		getvalidated("textsubmenu", "text", "Sub Menu Name")
		return false;
	}
	if (jQuery("#txtMenuURL").val() == '') {
		getvalidated("txtMenuURL", "text", "URL")
		return false;
	}
	if (jQuery("#txtMenuDescription").val() == '') {
		getvalidated("txtMenuDescription", "text", "Menu Description")
		return false;
	}
	if (jQuery("#Sequence").val() == '') {
		getvalidated("Sequence", "text", "Sequence")
		return false;
	}
	else {
		InsupdSubmenu()
	}
}
else {
	InsupdSubmenu()
}
}
function InsupdSubmenu() {
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true) {
		chkval = "1"
	} else {
		chkval = "0"
	}
	var MasterData = {
		"p_Sub_Menu_Id": localStorage.Sub_Menu_Id,
		"p_Menu_Id": $("#ddlMenu").val(),
		"p_Sub_Menu_Name": jQuery("#textsubmenu").val(),
		"p_URL": jQuery("#txtMenuURL").val(),
		"p_Sub_Menu_Description": jQuery("#txtMenuDescription").val(),
		"p_Active_YN": chkval,
		"p_Sequence": jQuery("#Sequence").val(),
		"p_open_for": "0"

	};
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "secured/AdminSubMenu";
	securedajaxpost(path, 'parsrdataitSubmenu', 'comment', MasterData, 'control')
}


function parsrdataitSubmenu(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated") {
		sessionStorage.setItem("token", data.data.token);
		InsupdSubmenu();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetmode();
		FetchsubMenu();
		toastr.success("Insert Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetmode();
		FetchsubMenu();
		toastr.success("Update Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetmode()
		toastr.warning("already exist", "", "info")
		return true;
	}

}
function resetmode() {
	$("#chkActiveStatus")[0].checked = false;
	jQuery("#textsubmenu").val('')
	jQuery("#txtMenuDescription").val('')
	jQuery("#txtMenuURL").val('')
	localStorage.Sub_Menu_Id = '0'
	jQuery("#Sequence").val('')
	jQuery("#ddlMenu").val('0')
}
function FetchsubMenu() {
	var path = serverpath + "AdminSubMenu/0/0/1"
	ajaxget(path, 'parsedataFetchsubMenu', 'comment', "control");
}
function parsedataFetchsubMenu(data) {
	data = JSON.parse(data)




	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
			active = "N"
		}
		else if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].Sub_Menu_Id + "</td><td style='    word-break: break-word;'>" + data1[i].Sub_Menu_Name + "</td><td style='    word-break: break-word;'>" + data1[i].URL + "</td><td>" + data1[i].Sequence + "</td><td>" + active + "</td><td><button type='button'  onclick=EditMenu('" + data1[i].Sub_Menu_Id + "','" + data1[i].Menu_Id + "','" + encodeURI(data1[i].Sub_Menu_Name) + "','" + encodeURI(data1[i].URL) + "','" + encodeURI(data1[i].Sub_Menu_Description) + "','" + encodeURI(data1[i].Active_YN) + "','" + encodeURI(data1[i].Sequence) + "') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=Delete('" + data1[i].Sub_Menu_Id + "','Submenu')>Delete</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMenu(Sub_Menu_Id, MenuId, Sub_Menu_Name,URL, Sub_Menu_Description,  Active_YN, Sequence) {
	jQuery('#m_scroll_top').click();
	localStorage.Sub_Menu_Id = Sub_Menu_Id
	if (decodeURI(Active_YN) == '0') {
		$("#chkActiveStatus")[0].checked = false;
	}
	else {
		$("#chkActiveStatus")[0].checked = true;
	}
	$("#ddlMenu").val(MenuId),
		jQuery("#textsubmenu").val(decodeURI(Sub_Menu_Name))
	
	jQuery("#txtMenuDescription").val(decodeURI(Sub_Menu_Description))
	jQuery("#txtMenuURL").val(decodeURI(URL))
	jQuery("#Sequence").val(decodeURI(Sequence))
}
