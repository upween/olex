function InsUpdUserCreation(){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
    
       
    var chkval = $("#Active1")[0].checked
    if (chkval == true){
    chkval = "1"
    }else{
    chkval="0"
    }



    var MasterData = {
    "p_Emp_Id":sessionStorage.userEmp_id,
    "p_Ex_Id": jQuery("#exchange").val(),
    "p_Emp_Name": jQuery("#names").val(),
    "p_Address": jQuery("#address1").val(),
    "p_Mobile": jQuery("#Mobno").val(),
    "p_Email_Id": jQuery("#email").val(),
    "p_Active_YN": chkval,
    "p_User_Type_Id":jQuery("#usertype").val(),
    "p_UserName":$('#nameuser').val(),
    "p_Pass": md5(jQuery("#newpassword").val()),
    
    }
    MasterData = JSON.stringify(MasterData)
    sessionStorage.MasterData=MasterData
    var path = serverpath + "secured/olexuser";
    securedajaxpost(path,'parsrdataInsUpdUserCreation','comment',MasterData,'control')
    }
    function parsrdataInsUpdUserCreation(data) {
    data = JSON.parse(data)

   

    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    InsUpdUserCreation();
    }
    else if (data[0][0].ReturnValue == "1") {
        Insupduserloginlog(sessionStorage.userEmp_id,'Insert')
    resetmode();
    fetchuserDetails('parsedatasecuredfetchuserDetails')
    toastr.success("Insert Successful", "", "success")

    return true;
    }
    
    else if (data[0][0].ReturnValue == "2") {
        Insupduserloginlog(sessionStorage.userEmp_id,'Modify')
        fetchuserDetails('parsedatasecuredfetchuserDetails')
        toastr.success("Update Successful", "", "success")
      
        return true;
    }

    else if (data[0][0].ReturnValue == "0") {
    
    toastr.warning("Already exist", "", "info")
    return true;
    }
    
    }


function changepass(){
    var MasterData = {
        "p_Emp_Id":sessionStorage.userEmp_id,
    }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "olexuserpass";
        ajaxpost(path,'parsrdatachangepass','comment',MasterData,'control')
}
function parsrdatachangepass(data){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    changepass();
    }
        $('#mppas').show();
        localStorage.removeItem('changepass')
        Insupduserloginlog(sessionStorage.userEmp_id,'PasswordChange')
    toastr.success("PasswordChange Successful", "", "success")
    return true;
}

    function resetmode(){
    jQuery("#exchange").val('0')
    jQuery("#names").val('')
    jQuery("#address1").val('')
    jQuery("#Mobno").val('')
    jQuery("#email").val('')
    jQuery("#Repassword").val('')
    jQuery("#usertype").val('0')
    $('#nameuser').val('')
    jQuery("#newpassword").val('')
    sessionStorage.userEmp_id='0'
    jQuery("#Repassword").attr('disabled',false)
    jQuery("#usertype").attr('disabled',false)
    $('#nameuser').attr('disabled',false)
    jQuery("#newpassword").attr('disabled',false)
    }
    
    
    function FillExchangeoffice1(funct,control) {
    var path = serverpath +"secured/exchangeoffice/0/0/0"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillExchangeoffice1(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillExchangeoffice1('parsedatasecuredFillExchangeoffice1','exchange'); }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Exchange Name"));
    
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
    }
    jQuery("#"+control).val('5');
    }
    
    }
    
    
    
    
    
    function FillUserType(funct,control) {
    var path = serverpath +"secured/olexusertype"
    securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillUserType(data,control){
    data = JSON.parse(data);
    if (data.message == "New token generated"){
    sessionStorage.setItem("token", data.data.token);
    FillUserType('parsedatasecuredFillUserType','usertype'); }
    else if (data.status == 401){
    toastr.warning("Unauthorized", "", "info")
    return true;
    }
    else{
    jQuery("#"+control).empty();
    var data1 = data[0];
    jQuery("#"+control).append(jQuery("<option ></option>").val("00").html("Select User Type"));
    
    for (var i = 0; i < data1.length; i++) {
    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].User_Type_Id).html(data1[i].User_Name));
    }
    jQuery("#"+control).val('5');
    }
    
    }
    
    
    
    
    
    function checkvalidation1(){
    if(isValidation){
    if(jQuery("#exchange").val()=="0"){
    getvalidated('exchange','Select','Exchange');
    return false;
    }
    if(jQuery("#names").val()==""){
    getvalidated('names','text','Name');
    return false;
    }
    if(jQuery("#Mobno").val()==""){
    getvalidated('Mobno','text','Mobile Number');
    return false;
    }
    if(jQuery("#email").val()==""){
    getvalidated('email','text','Email');
    return false;
    }
    
    if(jQuery("#address1").val()==""){
    getvalidated('address1','text','Address');
    return false;
    }
    if(jQuery("#usertype").val()==""){
    getvalidated('usertype','select','User Type');
    return false;
    }
    if(jQuery("#nameuser").val()==""){
    getvalidated('nameuser','text','User Name');
    return false;
    }
    
        if(jQuery("#newpassword").val()=="" && sessionStorage.userEmp_id=='0'){
        getvalidated('newpassword','text','Password');
        return false;
        }
        if(jQuery("#Repassword").val()=="" && sessionStorage.userEmp_id=='0'){
        getvalidated('Repassword','text','Confirm Password');
        return false;
        }
        if(jQuery("#Repassword").val()!=jQuery("#newpassword").val() && sessionStorage.userEmp_id=='0'){
          $('#valideRepassword').text('Password not matched');
            return false;
            }
        
    else{
        $('#valideRepassword').text('');
    InsUpdUserCreation()
    
    }

   
    
    }
    
    
    else{
    InsUpdUserCreation()
    
    }
    }
    
    
    
    
    
    
    
    
    
    function checkemail() {
    var Email;
    Email = jQuery('#email').val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(Email) == false) {
    $("#email").focus();
    $("#validemail").html("Please Enter Correct Email ID", "", "info")
    return true;
    }
    
    
    }
    function fetchuserDetails(funct){
    var path = serverpath +"olexuserProfile"
    securedajaxget(path,funct,'comment','control');
    }
   
    function parsedatasecuredfetchuserDetails(data,control){
    data = JSON.parse(data);
    var appenddata='';
    for (var i = 0; i < data[0].length; i++) {
    activeyn='No';
    if(data[0][i].Active_YN=='1'){
    activeyn='Yes';
    }
    appenddata += "<tr><td>" + [i+1] + "</td><td>"+data[0][i].Emp_Name+"</td><td>" + data[0][i].Mobile + "</td><td>"+data[0][i].Email_Id+"</td><td>"+data[0][i].Address+"</td><td> "+data[0][i].User_Type_Name+" </td><td>"+activeyn+"</td><td><button id='btnM' type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=editMode('"+data[0][i].Emp_Id+"','"+data[0][i].Ex_Id+"','"+encodeURI(data[0][i].Emp_Name)+"','"+encodeURI(data[0][i].Address)+"','"+encodeURI(data[0][i].Email_Id)+"','"+data[0][i].Mobile+"','"+data[0][i].Active_YN+"','"+data[0][i].User_Type_Id+"','"+encodeURI(data[0][i].UserName)+"');scrolltop() >Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);
    
    
    }
    
    

    function editMode(empid,exid,name,address,email,mobile,activeyn,usertype,username){
       
          
        $('#chngpass').show();
        $('#mppas').hide();
        jQuery("#exchange").val(exid)
        jQuery("#names").val(decodeURI(name))
        jQuery("#address1").val(decodeURI(address))
        jQuery("#Mobno").val(mobile)
        jQuery("#email").val(decodeURI(email))
        jQuery("#usertype").val(usertype)
        $('#nameuser').val(decodeURI(username))
        jQuery("#newpassword").val('')
        jQuery("#p").hide()
        jQuery("#p1").hide()
        jQuery("#cp").hide()
        jQuery("#cp1").hide()
        sessionStorage.userEmp_id=empid;
        if(activeyn=='1'){
            $("#Active1")[0].checked=true;
        }
        else{
            $("#Active1")[0].checked=false;
        }
    }
    
    function scrolltop(){
      $(window).scrollTop(0); 
   //   window.scrollTo({ top: 100, left: 100, behavior: 'smooth' }); ----2
  // document.location.href = "#top"; --3
   
}


function checkAvailabilityUserName(user){
	var nameuser=user;
	if($.trim(user)!=""){
		var path =  serverpath + "getUsername/'"+nameuser+"'";
		ajaxget(path,'parsedataUserName','comment',"control");
	}

}
function parsedataUserName(data){
	data = JSON.parse(data)
	if(data[0][0].ReturnValue=='1'){

		toastr.warning('Already exist Username');
   }
  
}


$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
