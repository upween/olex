
function FillCVTemplate(funct) {

  var path = serverpath + "secured/JobSeekerProfileStatusOLEX/'" + sessionStorage.getItem("CandidateId") + "'"
  securedajaxget(path, funct, 'comment', 'control');
}

function parsedatasecuredFillCVTemplate(data) {
  data = JSON.parse(data)
  console.log(data);
  if (data.message == "New token generated") {
    sessionStorage.setItem("token", data.data.token);
    FillCVTemplate('parsedatasecuredFillCVTemplate');
  }
  else if (data.status == 401) {
    toastr.warning("Unauthorized", "", "info")
    return true;
  }
  else {
  





    var personal = "";
   
    var designationname=""
    if(data[0][0].DesignationName==null || data[0][0].DesignationName==undefined ||data[0][0].DesignationName=='' || data[0][0].Organisation==null || data[0][0].Organisation==undefined ||data[0][0].Organisation=='')
    {
      designationname=''
    }
    else {
      designationname=data[0][0].DesignationName +' at '+data[0][0].Organisation
    }
 
   
      personal +=`<div class='row'>
                    <div class='col-md-3 col-sm-2'>Name:</div>
                    <div class='col-md-3 col-sm-4' id='NameCVT'>${data[0][0].CandidateName}</div>
                    <div class='col-md-3 col-sm-2'>First Name:</div>
                    <div class='col-md-3 col-sm-4' id='fname'>${data[0][0].FirstName}</div>
                  </div>
                  <div class='row'>
                 <div class='col-md-3 col-sm-2'>Mobile Number:</div>
                 <div class='col-md-3 col-sm-4' id='phoneNoCVT'>${data[0][0].MobileNumber}</div>
                 <div class='col-md-3 col-sm-2'>Email Id:</div>
                 <div class='col-md-3 col-sm-4' id='emailCVT'>${data[0][0].EmailId}</div>
                 </div>
                 <div class='row'>
                <div class='col-md-3 col-sm-2'>Date Of Birth:</div>
                <div class='col-md-3 col-sm-4' id='dob'>${data[0][0].DateOfBirth}</div>
                <div class='col-md-3 col-sm-2'>Reg Date:</div>
                <div class='col-md-3 col-sm-4' id='Rdate'>${data[0][0].RegDate}</div>
                </div>
                <div class='row'>
                <div class='col-md-3 col-sm-2'>Renew Date:</div>
                <div class='col-md-3 col-sm-4' id='Renwdate'>${data[0][0].renewal_date}</div>
                <div class='col-md-3 col-sm-2'>Username:</div>
                <div class='col-md-3 col-sm-4' id='user'>${data[0][0].UserName}</div>
                </div>`;
 

                $("#firstname").val(data[0][0].FirstName)
                $("#midlename").val(data[0][0].MiddleName)
                $("#lastname").val(data[0][0].LastName)
                $("#fathername").val(data[0][0].GuardianFatherName)
                $("#m_datepicker_1").val(data[0][0].DateOfBirth)
                $("#aadhar").val(data[0][0].IdentificationNumber)

                $("#mobno").val(data[0][0].MobileNumber)
                $("#emailid").val(data[0][0].EmailId)


    $("#RegistrationNo").html(data[0][0].RegistrationId);
     $("#PersonalDetailCV").html(personal);
 $('#profilestatus').text(`${data[0][0].ProfileStatus} %`)
  }

}
function resetUserPassword(){
 
  var MasterData = {
      "p_CandidateId":sessionStorage.getItem("CandidateId"),
      
  };
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "secured/adminresetPass";
  securedajaxpost(path, 'parsrdataresetUserPassword', 'comment', MasterData, 'control')


}
function parsrdataresetUserPassword(data) {
  data = JSON.parse(data)
  $('#nextrenwDate').text("");
  $('#nextrenwDate').html('');
  if (data.message == "New token generated"){
      sessionStorage.setItem("token", data.data.token);
      resetUserPassword();
  }
  
  else {
    if(data.errno){
      return;
    }
  if(data[0][0]){
    InsupdViewuser('ResetPassword')
toastr.success('Password Update Successfully', "", "success");
$('#nextrenwDate').html(`<p style='color:green;'>User name: ${data[0][0].UserName}</p><p style='color:green;'>  Password: mprojgar123</p>`);
     var body=`<p>Dear Candidate,</p>
              <p style='color:green'>Password Reset Successfully</p>
              <p style='color:green'>Name: ${data[0][0].CandidateName}</p>
              <p>User Name: ${data[0][0].UserName}</p>
              <p>Password: mprojgar123</p>`;
     var sub='Mp Rojgar (Reset Password)';
   //  sentmailglobal(localStorage.userEmail,body,sub);
      return true;
  }
  }
}
function renewRegistration() {
 
  var MasterData = {
      
  
      "p_RegistrationId": $("#RegistrationNo").text()
  };
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "registration_renewforolduser";
  ajaxpost(path, 'parsrdatarenewRegistration', 'comment', MasterData, 'control');
  }
  
  function parsrdatarenewRegistration(data) {
    data = JSON.parse(data)
    $.unblockUI();
    $('#nextrenwDate').text("");
    $('#nextrenwDate').html('');
    if(data.errno) {
        toastr.warning("Something went wrong Please try again later", "", "info")
        return false;
    }
    else if(data[0][0].ReturnValue=='1'){
      if(data[0][0].renewal_date){
        $('#nextrenwDate').text('Your next renew date is ' + data[0][0].renewal_date);
        toastr.warning("Renew Registration is not available", "", "info")
      
      }
    
    
    }
    else{
      FillCVTemplate('parsedatasecuredFillCVTemplate');
      $('#nextrenwDate').text('Your next renew date is ' + data[0][0].renewal_date_new);
      InsupdViewuser(CandidateId,Renew)
        toastr.success("Renew Registration Successfully", "", "success");
    }
    
    
    

  
  }
  
  
  
  


  function UpdDetail(){
    // var DOB1= jQuery("#m_datepicker_1").val().split(" ")
    // var DOB2= DOB1[0].split("/")
    // var DOB=DOB2[2]+"/"+DOB2[1]+"/"+DOB2[0];

    var MasterData = {  
      
        "p_CandidateId":sessionStorage.CandidateId,
        "p_FirstName":jQuery("#firstname").val(),
        "p_MiddleName":jQuery("#midlename").val(),
        "p_LastName":jQuery("#lastname").val(),
        "p_FatherName":jQuery("#fathername").val(),
        "p_DateOfBirth":jQuery("#m_datepicker_1").val(),
        "p_Aadhar":jQuery("#aadhar").val(),
        // "p_MobileNumber":jQuery("#mobno").val(),
        // "p_EmailId":jQuery("#emailid").val()
        "p_MobileNumber":$("#phoneNoCVT").text(),
        "p_EmailId":$("#emailCVT").text()
          
        
    
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "updjobseekerregistrationlogin";
    ajaxpost(path, 'parsrdataUpdDetail', 'comment', MasterData, 'control')
    }
    
    function parsrdataUpdDetail(data) {
      data = JSON.parse(data)
      if (data.message == "New token generated"){
          sessionStorage.setItem("token", data.data.token);
          UpdDetail();
      }
      
      else if(data[0][0].ReturnValue=='1'){
        $("#myModalprintforolduser").modal("toggle");
        InsupdViewuser('Updatedetails')
   toastr.success("Submit Successful", "", "success")
   FillCVTemplate('parsedatasecuredFillCVTemplate');
   return true;
     }
       
  //     else if (data[0][0].ReturnValue == "1") {
  //       $("#myModalprintforolduser").modal("toggle");
  //       // DistrictOffice();
  //     //  resetMode()
  //        toastr.success("Update Successful", "", "success")
  //       return true;
  // } 
     


    }
    


    function chkvalidation(){
    
      if(jQuery("#firstname").val()==""){
          getvalidated('firstname','text','First Name');
          return false;
      }
   
      if(jQuery("#lastname").val()==""){
          getvalidated('lastname','text','Last Name');
          return false;
      }
      if(jQuery("#fathername").val()==""){
        getvalidated('fathername','text','Father Name');
        return false;
    }
     
      if(jQuery("#m_datepicker_1").val()==""){
          getvalidated('m_datepicker_1','text','DateOfBirthday');
          return false;
      }
      if(jQuery("#aadhar").val()==""){
          getvalidated('aadhar','text','AadharNumber');
          return false;
      }
     
      else{
        UpdDetail()
            
      }
  }





  
function FillDistrict() {
  jQuery.ajax({
      type: "GET",
      contentType: "application/json; charset=utf-8",
      url: serverpath + "district/19/0/0/0",
      cache: false,
      dataType: "json",
      success: function (data) {
          jQuery("#district").empty();
          jQuery("#district").append(jQuery("<option ></option>").val("99999").html("Select District"));
          for (var i = 0; i < data[0].length; i++) {
              jQuery("#district").append(jQuery("<option></option>").val(data[0][i].DistrictId).html(data[0][i].DistrictName));
          }
         
      },
      error: function (xhr) {
          if(data.errno) {
              toastr.warning("Something went wrong Please try again later", "", "info")
              return false;
          }
      }
  });
}
jQuery('#district').on('change', function () {
 
  FillCity(jQuery('#district').val(),'99999');
  FillVillage('99999','99999');
  
});

function FillCity(district) {
  jQuery.ajax({
      type: "GET",
      contentType: "application/json; charset=utf-8",
      url: serverpath + "city/" + district + "/0/0/0",
      cache: false,
      dataType: "json",
      success: function (data) {
          var data1 = data[0];
          jQuery("#city").empty();
          jQuery("#city").append(jQuery("<option ></option>").val("99999").html("Select Tehsil"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#city").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
          }
      },
      error: function (xhr) {
          if(data.errno) {
              toastr.warning("Something went wrong Please try again later", "", "info")
              return false;
          }
      }
  });
}
jQuery('#city').on('change', function () {
  FillVillage(jQuery('#city').val(),'99999');
  
});

function FillVillage(city) {
  jQuery.ajax({
      type: "GET",
      contentType: "application/json; charset=utf-8",
      url: serverpath + "village/" + city + "/0/0/0",
      cache: false,
      dataType: "json",
      success: function (data) {
          var data1 = data[0];
          jQuery("#village").empty();
          jQuery("#village").append(jQuery("<option ></option>").val("99999").html("Select City/Village"));
          jQuery("#village").append(jQuery("<option ></option>").val("89898").html("Other"));
          for (var i = 0; i < data1.length; i++) {
              jQuery("#village").append(jQuery("<option></option>").val(data1[i].VillageId).html(data1[i].VillageName));
          }
      },
      error: function (xhr) {
          if(data.errno) {
              toastr.warning("Something went wrong Please try again later", "", "info")
              return false;
          }
      }
  });
}




function UpdDistrict(){
if($("#district").val()=='99999'){
  toastr.warning("Please Select District", "", "info")
    return true;
}
else if($("#city").val()=='99999'){
  toastr.warning("Please Select Tehsil", "", "info")
    return true;
}
else if($("#village").val()=='99999'){
  toastr.warning("Please Select City/Village", "", "info")
    return true;
}
else{
  var MasterData = {  
    
      "p_CandidateId":sessionStorage.CandidateId,
      "p_DistrictId":jQuery("#district").val(),
      "p_CityId":jQuery("#city").val(),
      "p_VillageId":jQuery("#village").val(),
      "p_EntryBy":sessionStorage.User_Type_Id,
      "p_Ip_Address":sessionStorage.getItem("ipAddress")
      
  
  };
}
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "Transfer_location";
  ajaxpost(path, 'parsrdataUpdDistrict', 'comment', MasterData, 'control')
  }
  
  function parsrdataUpdDistrict(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        UpdDistrict();
    }
    
    else if (data[0][0].ReturnValue == "1") {
      $("#myModaldistrict").modal("toggle");
      InsupdViewuser('DistrictUpdate') 
 toastr.success("Update Successful", "", "success")
 return true;
   }
   else if (data[0][0].ReturnValue == "0") {
    $("#myModaldistrict").modal("toggle");
    // toastr.warning("District Allready Updated", "", "info")
    // return true;
$('#myModal').modal('show')
   


   }

   else if (data[0][0].ReturnValue == "2") {
    $("#myModaldistrict").modal("toggle");
     toastr.warning("No changes Detected", "", "info")
    // return true;
//$('#myModal').modal('show')
   


   }
  }
  
  function InsupdViewuser(ActionTaken) {
    var MasterData = {
      "p_EmpId": sessionStorage.Emp_Id,
      "p_Candidateid": sessionStorage.CandidateId,
      "p_ActionTaken": ActionTaken,
      "p_IpAddress": sessionStorage.ipAddress
     
  
  
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "userdetails";
    securedajaxpost(path, 'parsrdataitViewuser', 'comment', MasterData, 'control')
  }
  
  
  function parsrdataitViewuser(data) {
    data = JSON.parse(data)
    if (data[0][0].ReturnValue == "1") {
     
  
    }
  
  
  }