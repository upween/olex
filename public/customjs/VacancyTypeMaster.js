function InsUpdvacancytype(){
	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_VacancyType_id":localStorage.VacancyType_id,
    "p_VacancyType":$.trim(jQuery("#vacancytypename").val()),
    "p_Active_YN":chkval


};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "vacancytype";
securedajaxpost(path, 'parsrdatavtmaster', 'comment', MasterData, 'control')
}

function parsrdatavtmaster(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdvacancytype();
	}
	else if (data[0][0].ReturnValue == "1") {
       Fetchvacancytype();
       resetMode() ;
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
        Fetchvacancytype();
        resetMode() ;
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
        resetMode() ;
	toastr.info("Already exist", "", "info")
			return true;
	}
 
}
function resetMode() {
   
    localStorage.p_VacancyType_id = "0";
    $("#ActiveStatus")[0].checked = false;
        jQuery("#vacancytypename").val("");
    }
function Fetchvacancytype() {
	var path = serverpath + "vacancytype/0/0"
	ajaxget(path, 'parsrdatavacancytypemaster', 'comment', "control");
}
function parsrdatavacancytypemaster(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
		 	active = "N"
	 }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].VacancyType_id  + "</td><td style='word-break: break-word;'>" + data1[i].VacancyType  + "</td><td style='word-break: break-word;'>" + active  + "</td><td><button type='button' onclick=EditMode("+data1[i].VacancyType_id+",'"+encodeURI(data1[i].Active_YN)+"','"+encodeURI(data1[i].VacancyType)+"') 'class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td></tr>";
    } jQuery("#tbodyvalue").html(appenddata);
    }
function EditMode(VacancyType_id,Active_YN,VacancyType) {
  
    localStorage.p_VacancyType_id =VacancyType_id
    if (decodeURI(Active_YN) == '0') {
		$("#ActiveStatus")[0].checked = false;
	}
	else {
		$("#ActiveStatus")[0].checked = true;
    }
    jQuery("#vacancytypename").val(decodeURI(VacancyType))
}
function CheckValidationVacancyTypeMaster(){
  
    
  if($.trim(jQuery("#vacancytypename").val())==""){
        getvalidated('vacancytypename','text','Vacancy Type Name');
        return false;
    }
    else{
        InsUpdvacancytype();
    }
}