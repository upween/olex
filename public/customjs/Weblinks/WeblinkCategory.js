function InsUpdweblinkcategory(){

	var chkval = $("#ActiveStatus")[0].checked
	if (chkval == true){
		chkval = "1"
	}else{
		chkval="0"
	}
var MasterData = {  
	
    "p_WL_Cat_Id":localStorage.WL_Cat_Id,
    "p_WL_Category":jQuery("#wblinkcategory").val(),
    "p_Active_YN":chkval

};
MasterData = JSON.stringify(MasterData)
var path = serverpath + "secured/mstwlcategory";
securedajaxpost(path, 'parsrdataweblinkcategory', 'comment', MasterData, 'control')
}

function parsrdataweblinkcategory(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated"){
			sessionStorage.setItem("token", data.data.token);
			InsUpdweblinkcategory();
	}
	else if (data[0][0].ReturnValue == "1") {
		Fetchweblink();
		resetMode();
		 toastr.success("Insert Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		Fetchweblink();
		resetMode();
			toastr.success("Update Successful", "", "success")
			return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetMode();
	toastr.info("already exist", "", "info")
			return true;
	}
 
}

function resetMode() {

	localStorage.WL_Cat_Id='0'
    jQuery("#wblinkcategory").val("");
    $("#ActiveStatus").prop("checked", false);
}



function Fetchweblink() {
	var path = serverpath + "mstwlcategory/0/0"
	ajaxget(path, 'parsedataFetchweblink', 'comment', "control");
}
function parsedataFetchweblink(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		if (data1[i].Active_YN == '0') {
			active = "N"
		}
		else if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='    word-break: break-word;'>" + data1[i].WL_Cat_Id + "</td><td style='    word-break: break-word;'>" + data1[i].WL_Category + "</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button'  onclick=EditMode('" + data1[i].WL_Cat_Id + "','" + encodeURI(data1[i].WL_Category)+ "','" + encodeURI(data1[i].Active_YN) + "') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=Delete('" + data1[i].WL_Cat_Id + "','WL_Category')>Delete</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMode(WL_Cat_Id,WL_Category,Active_YN){
	jQuery('#m_scroll_top').click();
	localStorage.WL_Cat_Id=WL_Cat_Id,
	$("#wblinkcategory").val(decodeURI(WL_Category))
    if(decodeURI(Active_YN)=='0'){
		$("#ActiveStatus")[0].checked=false;
		}
		else{	
			$("#ActiveStatus")[0].checked=true;
		}
}
jQuery("#txtSearch").keyup(function () {
	searchTextInTable("tbodyvalue");
})