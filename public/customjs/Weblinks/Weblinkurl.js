function Fillweblinkurl() {
	var path = serverpath + "mstwlcategory/0/0"
	ajaxget(path, "parsedataFillweblinkurl", 'comment', "control");
}
function parsedataFillweblinkurl(data) {
	data = JSON.parse(data)
	jQuery("#ddlweblink").empty();
	var data1 = data[0];

	jQuery("#ddlweblink").append(jQuery("<option ></option>").val("0").html("Select Category"));
	for (var i = 0; i < data1.length; i++) {
		jQuery("#ddlweblink").append(jQuery("<option></option>").val(data1[i].WL_Cat_Id).html(data1[i].WL_Category));
	}
}

function Insupdweblinkurl() {
	var chkval = $("#chkActiveStatus")[0].checked
	if (chkval == true) {
		chkval = "1"
	} else {
		chkval = "0"
	}
	var MasterData = {
		"p_WL_Id": localStorage.WL_Id,
		"p_WL_Cat_Id": jQuery("#ddlweblink").val(),
		"p_WL_Description": jQuery("#weblinkdes").val(),
		"p_WL_URL": jQuery("#txturl").val(),
		"p_Active_YN": chkval
	};
	MasterData = JSON.stringify(MasterData)
	var path = serverpath + "secured/wlurl";
	securedajaxpost(path, 'parsrdataitweblinkurl', 'comment', MasterData, 'control')
}


function parsrdataitweblinkurl(data) {
	data = JSON.parse(data)
	if (data.message == "New token generated") {
		sessionStorage.setItem("token", data.data.token);
		Insupdweblinkurl();
	}
	else if (data[0][0].ReturnValue == "1") {
		resetmode();
		Fetchweblinkurl();
		toastr.success("Insert Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "2") {
		resetmode();
		Fetchweblinkurl();
		toastr.success("Update Successful", "", "success")
		return true;
	}
	else if (data[0][0].ReturnValue == "0") {
		resetmode()
		toastr.warning("already exist", "", "info")
		return true;
	}

}
function resetmode() {
	$("#chkActiveStatus")[0].checked = false;
	jQuery("#ddlweblink").val("")
	jQuery("#weblinkdes").val("")
	jQuery("#txturl").val("")
	localStorage.WL_Id = '0'

}
function Fetchweblinkurl() {
	var path = serverpath + "wlurl/0/0/0"
	ajaxget(path, 'parsedataFetchweblinkurl', 'comment', "control");
}
function parsedataFetchweblinkurl(data) {
	data = JSON.parse(data)
	var data1 = data[0];
	var appenddata = "";
	var active = "";
	for (var i = 0; i < data1.length; i++) {
		// if (data1[i].Active_YN == '0') {
		// 	active = "N"
		// }
		 if (data1[i].Active_YN == '1') {
			active = "Y"
		}
		appenddata += "<tr><td style='word-break: break-word;'>" + data1[i].WL_Id + "</td><td style='    word-break: break-word;'>" + data1[i].WL_Description + "</td><td>'"+data1[i].WL_URL+"'</td><td style='    word-break: break-word;'>" + active + "</td><td><button type='button'  onclick=EditMenu('" + data1[i].WL_Id + "','" + data1[i].WL_Cat_Id + "','" + encodeURI(data1[i].WL_Description) + "','" + encodeURI(data1[i].WL_URL) + "','" + encodeURI(data1[i].Active_YN) + "') class='btn btn-success' style='background-color:#716aca;border-color:#716aca;'>Modify</button></td><td><button type='button' class='btn btn-success' style='background-color:#716aca;border-color:#716aca;' onclick=Delete('" + data1[i].WL_Id + "','WL_URL')>Delete</button></td></tr>";
	} jQuery("#tbodyvalue").html(appenddata);


}
function EditMenu(WL_Id, WL_Cat_Id, WL_Description, WL_URL,  Active_YN) {
	jQuery('#m_scroll_top').click();
	localStorage.WL_Id = WL_Id
	if (decodeURI(Active_YN) == '0') {
		$("#chkActiveStatus")[0].checked = false;
	}
	else {
		$("#chkActiveStatus")[0].checked = true;
	}
    $("#ddlweblink").val(decodeURI(WL_Cat_Id))
	
	jQuery("#weblinkdes").val(decodeURI(WL_Description))
	jQuery("#txturl").val(decodeURI(WL_URL))
}
