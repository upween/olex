var strDate,endDate;
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

jQuery("#txtSearch").keyup(function () {
    searchTextInTable("tbodyvalue");
})
//$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
function searchTextInTable(tblName) {
    var tbl;
    tbl = tblName;
    jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.

    var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value

    if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
    {
        jQuery("#" + tbl + " tr:has(td)").show();
        return false;
    }
    //Iterate through all the td.
    jQuery("#" + tbl + " tr:has(td)").children().each(function () {
        var cellText = jQuery(this).text().toLowerCase();
        if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
        {
            jQuery(this).parent().show();
            return true;
        }
    });
    //e.preventDefault();
}
function fetchMenuTopItemsFromSession() {
    var data = sessionStorage.getItem('UserMenu');
    var data1 = sessionStorage.getItem('UserMenu');
    var data2 = sessionStorage.getItem('UserMenu');
    var i = 1;
    jQuery("#m_header_menu").empty();
    jQuery("#m_header_menu").append('<ul class="m-menu__nav  m-menu__nav--submenu-arrow " id="ulMainTop"><li class="m-menu__item   "  aria-haspopup="true"  id="dashboard"><a  href="/index" class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Home</span></a></li></ul>')
    jQuery.each(jQuery.parseJSON(data), function (key, value) {
        if (value.parent_id == '0') {
            jQuery("#ulMainTop").append('<li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a  href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><span class="m-menu__link-text">' + value.menu_name + '</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a><div class="m-menu__submenu  m-menu__submenu--fixed-xl m-menu__submenu--center" ><span class="m-menu__arrow m-menu__arrow--adjust"></span><div  class="m-menu__subnav" style="max-height: 300px;overflow: auto;" ><ul id="MenuHeader'+value.menu_id+'" class="m-menu__content">');
            
            jQuery.each(jQuery.parseJSON(data1), function (key, value1) {
                //debugger;
                
                if (value.menu_id == value1.parent_id){

                    jQuery("#MenuHeader" + value.menu_id).append('<li class="m-menu__item"><h3 class="m-menu__heading m-menu__toggle"><span class="m-menu__link-text">'+value1.menu_name+'</span><i class="m-menu__ver-arrow la la-angle-right"></i></h3><ul class="m-menu__inner" id="MenuHeader1'+value1.menu_id+'">');
                    jQuery.each(jQuery.parseJSON(data2), function (key, value2) {
                        //debugger;
                        
                        if (value1.menu_id == value2.parent_id){
                            jQuery("#MenuHeader1" + value1.menu_id).append('<li class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true"><a  href="'+value2.url+'" class="m-menu__link "><span class="m-menu__link-text" >'+value2.menu_name+'</span></a></li>');
                        }
                    });
                    jQuery("#MenuHeader1" + value1.menu_id).append('</ul></li>');
                }
                
                                            
                    
            });
            if (value.url == "#") {
                jQuery("#MenuHeader" + value.menu_id).append('</ul></div></div></li>');
            }
            i++;
        }
    });

}
fetchMenuTopItemsFromSession();
function ExportExcel(id,filename) {
    $("#"+id).table2excel({
        filename: filename
    });
}
function showdatatemp(table){
    
            $("#"+table).show();
    
    
}		
function FillSecuredexchangeoffice(funct,control) {
    var path =  serverpath +"secured/GetOfficeReport/101"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredexchangeoffice('parsedatasecuredFillSecuredexchangeoffice',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
           
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
             
                jQuery("#"+control).val(sessionStorage.Ex_id=='100'?'109':sessionStorage.Ex_id);
            
        }
              
}
function FillMonth(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonth(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonth('parsedatasecuredFillMonth',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FullMonthName).html(data1[i].FullMonthName));
             }

            
        }
              
}



function FillMonthwithValue(funct,control) {
    var path =  serverpath + "secured/month/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillMonthwithValue(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthwithValue('parsedatasecuredFillMonthwithValue',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].MonthId).html(data1[i].FullMonthName));
             }

            
        }
              
}


function FillCategory(funct,control) {
    var path =  serverpath + "secured/category/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCategory('parsedatasecuredFillCategory',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Category"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
             }
        }
              
  }
  function FillDomicile(funct,control) {
    var path =  serverpath + "secured/Resident/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillDomicile(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDomicile('parsedatasecuredFillDomicile',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Both"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Resident_id).html(data1[i].Resident_name));
             }
        }
              
  }
  function FillArea(funct,control) {
    var path =  serverpath + "secured/Area/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillArea(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillArea('parsedatasecuredFillArea',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Both"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Area_Id).html(data1[i].Area_Name));
             }
        }
              
  }
  function FillDivision(funct,control) {
    var path =  serverpath + "secured/QualificationDivision/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDivision(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDivision('parsedatasecuredFillDivision',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Division"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Q_Division_id).html(data1[i].Q_Division_Detail));
             }
        }
              
}
function FillHCCategory(funct,control) {
    var path =  serverpath + "secured/HCCategory/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillHCCategory(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillHCCategory('parsedatasecuredFillHCCategory',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("All"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].HC_Category_id).html(data1[i].HC_Description));
             }
        }
              
  }
  function FillLanguage(funct,control) {
    var path =  serverpath + "secured/Languagetype/0/0"
    securedajaxget(path,funct,'comment',control);
      }
  
  function parsedatasecuredFillLanguage(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillLanguage('parsedatasecuredFillLanguage',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Language"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Lang_id).html(data1[i].Lang_name));
             }
        }
              
  }
  function FillTSType(funct,control) {
    var path =  serverpath + "secured/typingstenomaster/0/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillTSType(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillTSType('parsedatasecuredFillTSType',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Type_id).html(data1[i].Type));
             }
        }
              
  }
  function FillSector(funct,control) {
    var path =  serverpath + "secured/SkillSet/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSector(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSector('parsedatasecuredFillSector',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sector"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
             }
             
        }
              
}
function getvalidated(controlid,type,format){
    var validid = "valid"+controlid;
    if (type == "text"){
        if($("#"+controlid).val() == ""){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Enter "+format);
        }
        else{
            jQuery("#"+controlid).css('border-color', '');
            $("#"+validid).html("");
        }
    }
    else if (type == "select"){
        if($("#"+controlid).val() == "0"){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Select "+format);
        }
        else if($("#"+controlid).val() == "99999"){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Select "+format);
        }
        else if($("#"+controlid).val() == ""){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Select "+format);
        }
        else if($("#"+controlid).val() == null){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Select "+format);
        }
        else{
            jQuery("#"+controlid).css('border-color', '');
            $("#"+validid).html("");
        }
    }
    else if (type == "email"){
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if($("#"+controlid).val() == ""){
            $("#"+controlid).css('border-color', 'red');
            $("#"+validid).html("Please Enter "+format);
        }
        else if ($("#"+controlid).val() != ""){
            if($("#"+controlid).val().match(mailformat))
            {
                jQuery("#"+controlid).css('border-color', '');
                $("#"+validid).html("");
                return true;
            }
            else
            {$("#"+controlid).val("");
                $("#"+controlid).css('border-color', 'red');
                $("#"+validid).html("Please Enter Correct "+format);
                return false;
            }
        }
        else{
            jQuery("#"+controlid).css('border-color', '');
            $("#"+validid).html("")
            return true;
        }
    }
    else if (type == "number"){
        if (format == "Mobile Number"){
            if($("#"+controlid).val() == ""){
                $("#"+controlid).css('border-color', 'red');
                $("#"+validid).html("Please Enter "+format);
            }
            else if ($("#"+controlid).val() != ""){
                var phoneno = /^\d{10}$/;
                if($("#"+controlid).val().match(phoneno))
                {
                    jQuery("#"+controlid).css('border-color', '');
                    $("#"+validid).html("");
                    return true;
                }
                else
                {$("#"+controlid).val("");
                    $("#"+controlid).css('border-color', 'red');
                    $("#"+validid).html("Please Enter Correct "+format);
                    return false;
                }
            }
            else{
                jQuery("#"+controlid).css('border-color', '');
                $("#"+validid).html("");
            }
        }      
    }
    
}
function FillSecuredDistrict(funct,control) {
    var path =  serverpath + "secured/district/19/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredDistrict('parsedatasecuredFillSecuredDistrict',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
              jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));

              for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             
        }
              
}
function FillDistrict(funct,control) {
    var path =  serverpath + "district/19/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrict('parsedatasecuredFillDistrict',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
         
            for (var i = 0; i < data1.length; i++) {
                //jQuery("#"+control).append(jQuery("<option></option>").val("0").html("Select District"));

                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             
        }
              
}
function Delete(Id,Type) {
    jQuery.ajax({
        url: serverpath + "deleteadm/'"+Id+"'/'"+Type+"'",
      type: "GET",
      contentType: "application/json; charset=utf-8",
      cache: false,
      dataType: "json",
      success: function (data) {
        if (Type == 'Exchangeoffice') {
            FillExchangeOfficeType() 
        }

        if (Type == 'designation') {
            FillDesignationTable();
         }

        if (Type == 'officetype') {
       
           }

        if (Type == 'Menu') {
            FetchMenu();
        }
        
        if (Type == 'JFDetail') {
            if(localStorage.JFtype='v'){
                FillJobFairDetailVerification(localStorage.Ex_Id,localStorage.VStatus);
            }
            else if(localStorage.JFtype='ajf'){
            FillJobFairDetail(localStorage.Ex_Id)
            }
        }

        if (Type == 'Submenu') {
            FetchsubMenu();
        }
        if (Type == 'WL_Category') {
            Fetchweblink();
        }
        if (Type == 'Adv_Category') {
            FetchAdv_Category();
        }
        if (Type=='WL_URL'){
            Fillweblinkurl();
        }
        if (Type=='GDDetail'){
            FillGDDetail(localStorage.Ex_Id);
        }
        if (Type=='CCDetail'){
            FillCCDetail(localStorage.Ex_Id);
        }
        if (Type=='X6'){
            FillVacancyDetail('0');
        }
      
        
       
    }


    })
}
function FillYear(funct,control) {
    var path =  serverpath + "secured/year/0/20"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    $.unblockUI()
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYear('parsedatasecuredFillYear',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
             }

            
        }
              
}
function fetchJobseekerDetailByRegNo(RegId,CandidateName,RegDate,DOB,RegMonth,RegYear){
    if(RegDate!=0){
        var indate = RegDate.split("/")
var newRegDate = indate[0] + "-" + indate[1] + "-" + indate[2] ;
    }
    else{
        var newRegDate='0'
    }
    if(DOB!=0){
        var indateofb = DOB.split("/")

var newDOB = indateofb[0] + "-" + indateofb[1] + "-" + indateofb[2] ;
    }
    else{
        var newDOB='0'
    }
    var path =  serverpath + "AdminVerification/'"+RegId+"'/'"+CandidateName+"'/'"+newRegDate+"'/'"+newDOB+"'/'"+RegMonth+"'/'"+RegYear+"'"
    ajaxget(path,'parsedatafetchJobseekerDetailByRegNo','comment',"control");
}
function parsedatafetchJobseekerDetailByRegNo(data){  
    data = JSON.parse(data)
   var appenddata="";
   
    var data1 = data[0];    
            
         if(data1[0].DateOfBirth=="" || data1[0].DateOfBirth==null){
          
            DateOfBirth='-';
         }
         else{
            DateOfBirth=data1[0].DateOfBirth;
         }
         if(data1[0].RegDate=="" || data1[0].RegDate==null){
          
            RegDate='-';
         }
         else{
            RegDate=data1[0].RegDate;
         }
         if(data1[0].renewal_date=="" || data1[0].renewal_date==null){
          
            renewal_date='-';
         }
         else{
            renewal_date=data1[0].renewal_date;
         }
         localStorage.JSID=data1[0].CandidateId
       $('#RegNoById').html(data1[0].RegistrationId);
       $('#RegDateById').html(RegDate);
       $('#renewdtById').html(renewal_date);
       $('#JSNameById').html(data1[0].CandidateName);
       $('#FathernameById').html(data1[0].GuardianFatherName);
       $('#DoBById').html(DateOfBirth);
       $('#CategoryById').html(data1[0].CategoryName);
       $('#GenderById').html(data1[0].Gender);
       $('#JSdetailsById').show();
        
        
              
}

function SessionClear(){
    sessionStorage.clear();
    localStorage.clear();
    Cookies.remove('1P_JAR');
    Cookies.remove('langCookie');

}


function goBack() {
    window.history.back()
  } 
  function onlyNumbers(event) {
    if (event.type == "paste") {
      var clipboardData = event.clipboardData || window.clipboardData;
      var pastedData = clipboardData.getData('Text');
      if (isNaN(pastedData)) {
        event.preventDefault();
  
      } else {
        return;
      }
    }
    var keyCode = event.keyCode || event.which;
    if (keyCode == 9){
        return true;
    }
    if ((keyCode >= 96 && keyCode <= 105)) { 
        keyCode -= 48;
    }
    if ((keyCode >= 48 && keyCode <= 57) ) { 
        return true;
    }
    var charValue = String.fromCharCode(keyCode);
    if (isNaN(parseInt(charValue)) && event.keyCode != 8 ) {
      event.preventDefault();
    }
  }
   function sentSmsGlobal(to,msg,templateid) {

    var MasterData = {
           "mobile":to,
           "message":msg,
           "templateid":templateid
          
    }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "sendsms";
    ajaxpost(path, 'datasentsms', 'comment', MasterData, 'control')
 }
 function datasentsms(data) {
    data = JSON.parse(data)
 
 }
  function sentmailglobal(to,body,subject,cc) {
    var msg=`<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
    <tr>
    <td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'>
    <img src='http://mprojgar.gov.in/images/header_name.gif' style='width:100%'> 
    </td>
     </tr>
     <tr>
     <td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'>
     <tr>
     <td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'>
     <div style='font-size:22px;'>
     ${body}
     </div>
     <div>
     <br> 
        For any further queries, please write to us at helpdesk.mprojgar@mp.gov.in with your current MP Rojgar portal username or call us at our toll free number 18002757751 (Monday to Saturday between 10 am – 6 pm ). 
      <br><br>
      Note: This is autogenerated email please do not reply.<br></div></td></tr></table></td></tr>
    </table>`
            var MasterData = {
                   "To":to,
                   "Msg":msg,
                   "Subject":subject,
                   "CC":cc?cc:''
                  
            }
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "sentmail";
            ajaxpost(path, 'datasentmail', 'comment', MasterData, 'control')
         }
          function datasentmail(data) {
        data = JSON.parse(data)
     
     }
     function addTopScrollbar(ele, pos) {
         if($('#topScrollbar').length>0){
            return;
         }
        var outerDiv = document.createElement('div');
        $(outerDiv).attr('id','topScrollbar');
        $(outerDiv).css({ 'overflow-x': 'scroll' });
        var innerDiv = document.createElement('div');
        $(innerDiv).css({ 'min-width': $(ele).width() + 'px', 'visibility': 'hidden', 'min-height': '10px' });
        $(outerDiv).insertBefore(pos);
        $(outerDiv).append(innerDiv);
        $(pos).on('scroll', function () {
            $(outerDiv).scrollLeft($(pos).scrollLeft());
        });
        $(outerDiv).on('scroll', function () {
            $(pos).scrollLeft($(outerDiv).scrollLeft());
        });
    }
    



    function FillYearwithvalue(funct,control) {
        var path =  serverpath + "secured/year/0/3"
        securedajaxget(path,funct,'comment',control);
    }
    
    function parsedatasecuredFillYearwithvalue(data,control){  
        data = JSON.parse(data)
        $.unblockUI()
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FillYearwithvalue('parsedatasecuredFillYearwithvalue',control);
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
                for (var i = 0; i < data1.length; i++) {
                    jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Year).html(data1[i].Year));
                 }
    
                
            }
                  
    }



function FillDDlExchOffice(ReportID,control){

var path = serverpath + "secured/GetOfficeReport/"+ReportID+"";
securedajaxget(path, 'parsedatasecuredFillDDlExchOffice', 'comment', control)
}
function parsedatasecuredFillDDlExchOffice(data,control){
data = JSON.parse(data)
if (data.message == "New token generated"){
sessionStorage.setItem("token", data.data.token);
FillDDlExchOffice();
}
else if (data.status == 401){
toastr.warning("Unauthorized", "", "info")
return true;
}
else{
var data1 = data[0];
var appenddata="";

for (var i = 0; i < data1.length; i++) {
jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
}
}

}
  function checkEmploymentcutoff(type,tableid,spantextid){
            var path = serverpath + "cutoffdates/'"+type+"'";
           // securedajaxget(path, 'parsedataEmploymentcutoff', 'comment', 'control')
            jQuery.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: path,
                cache: false,
                dataType: "json",
                success: function (data) {
            var data1 = data[0];
           var stdt=new Date(data1[0].StDate).getDate();
           var enddt=new Date(data1[0].EdDate).getDate();
           var today = new Date(data1[0].todayDate).getDate();
           //var dd = String(today.getDate()).padStart(2, '0');
          var startDate_formated=data1[0].StDate.split('-')
          var startDate_formated1=startDate_formated[2]+'-'+startDate_formated[1]+'-'+startDate_formated[0]
           var isallowed=false;
           var endDate_formated=data1[0].EdDate.split('-')
           var endDate_formated1=endDate_formated[2]+'-'+endDate_formated[1]+'-'+endDate_formated[0]
     
           
var spantext="You are not allow to fill the detail.The Date For Entry  ";
          console.log("today",today,"stdt",stdt,"enddt",enddt);
           if(today >= stdt && today <= enddt){
               console.log("in1");
               
               $(`#${tableid}`).show()
            }
            else{
                $('.empbtn').append('disabled',true);
                console.log("in2");
                if(data1[0].StDate==data1[0].EdDate){
                    var iseq=" is ";
                    var daterange=startDate_formated1+'.'
                }
                else{
                    var iseq=' are ';
                    var daterange=startDate_formated1+' & '+endDate_formated1+'.'
                }
               $(`#${spantextid}`).text(spantext+iseq+daterange);
            
       
            }
        }
        });
            
            
        
    }
    function InsupdPlacementdetailslogs(Placementid,Action,UploadType,RealFileName,Filename) {
      
        var MasterData = {
            "p_Logid":0,
            "p_Placementid":Placementid,
            "p_Entryby":sessionStorage.Emp_Id,
            "p_Action":Action,
            "p_Ipaddress": sessionStorage.ipAddress,
            "p_UploadType": UploadType,
            "p_RealFileName": RealFileName,
            "p_Filename": Filename ,
            "p_UploadedBy": sessionStorage.Emp_Id,
            
        };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "placementdetailslogs";
        securedajaxpost(path, 'parsrdataplacement', 'comment', MasterData, 'control')
    }
    
    
    function parsrdataplacement(data) {
        data = JSON.parse(data)
      
    
    }
    function InsupdEmploymentlogs(UniqueId,Formtype,Action) {

        var MasterData = {
        "p_LogId":0,
        "p_UniqueId":UniqueId,
        "p_Formtype":Formtype,
        "p_Action":Action,
        "p_EntryBy": sessionStorage.Emp_Id,
        "p_IPaddress": sessionStorage.ipAddress,
        
        
        };
        MasterData = JSON.stringify(MasterData)
     
        var path = serverpath + "employmentdetailslogs";
        securedajaxpost(path, 'parsrdataemploymentdetailslogs', 'comment', MasterData, 'control')
        }
        
        
        function parsrdataemploymentdetailslogs(data) {
        data = JSON.parse(data)
        
        
        }

        function Insupduserloginlog(empid,Action) {
            var MasterData = {
              "p_Emp_Id": empid,
              "p_Action": Action,
              "p_EntryBy": sessionStorage.Emp_Id,
              "p_IpAddress": sessionStorage.ipAddress,
              "p_EnteredValue":sessionStorage.MasterData
              
          
          
            };
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "olexuserlogs";
            securedajaxpost(path, 'parsrdataituserloginlog', 'comment', MasterData, 'control')
          }
          
          
          function parsrdataituserloginlog(data) {
            data = JSON.parse(data)
            if (data[0][0].ReturnValue == "1") {
              
          
            }
          
          
          }