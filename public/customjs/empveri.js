function fetchInActiveEmp(Emp_Regno,V_type,Type){
    $(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);
//sessionStorage.EmpLogId=Emp_Regno;
      var path =  serverpath +"InActiveEmp/'"+Emp_Regno+"'/'"+V_type+"'/'"+Type+"'"
      ajaxget(path,'parsedatafetchInActiveEmp','comment',"control");
  }
  function parsedatafetchInActiveEmp(data){  
      data = JSON.parse(data)
     var appenddata="";
     jQuery("#tbodyvalue").empty();
      var data1 = data[0];  
      console.log(data)
      if(data[0].length==0){
          return;
      
      }
        if(data1[0].ReturnValue){
            fetchInActiveEmp('0','0','Fetch')
            sendemail(data1[0].Email,'verify')
            
            InsupdEmpverificationlogs(sessionStorage.EmpLogId,'Verify')
            toastr.success("Verify Successful", "", "success")
            return true;
        }
        else{
              for (var i = 0; i < data1.length; i++) {
          if (data1[i].V_Status=='0'){
              var VStatus="Not Verified";
              var btns="<td ><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=fetchInActiveEmp('"+data1[i].EmpId+"','0','Update');sessionStorage.EmpLogId='"+data1[i].EmpId+"'; >Verify</button></td><td ><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick=modalmaildetail('"+data1[i].Email+"') >Reply</button></td><td ><button type='button' class='btn btn-success'  style='background-color:#716aca;border-color:#716aca;' onclick='aboutdelete("+data1[i].EmpId+")' data-toggle='modal' data-target='#myModal'>Delete</button></td></tr>"
              var btn="<th>Verify</th><th>Reply</th><th>Delete</th>"
              // "+data1[i].Email+"
            }
          else if (data1[i].V_Status=='1'){
            var VStatus="Verified";
          }
          else if (data1[i].V_Status=='4'){
            var VStatus="Deleted";
          }
  if (data1[i].GST_File==null || data1[i].GST_File=='0' || data1[i].GST_File==undefined || data1[i].GST_File=='undefined'){
      var file="No File";
  }
 
  else{
    if(data1[i].EntryBy=='Olex'){
      var file= "<a href='#' onclick=showFileinModal('"+encodeURI(data1[i].GST_File)+"','GST','"+data1[i].EntryBy+"')>"+data1[i].GST_File+"</a>" ;
 //"+globalpath+"gstdoc/"+data1[i].GST_File+"
    }
    else{
      var file= "<a href='#'  onclick=showFileinModal('"+encodeURI(data1[i].GST_File)+"','GST','"+data1[i].EntryBy+"')>"+data1[i].GST_File+"</a>" ;
 //'"+EmployerDocPath+"gstdoc/"+data1[i].GST_File+"'
    }
  }
  if (data1[i].PAN_File==null || data1[i].PAN_File=='0' || data1[i].PAN_File==undefined || data1[i].PAN_File=='undefined'){
    var pfile="No File";
}

else{
  if(data1[i].EntryBy=='Olex'){
    var pfile= "<a href='#' onclick=showFileinModal('"+encodeURI(data1[i].PAN_File)+"','PAN','"+data1[i].EntryBy+"')>"+data1[i].PAN_File+"</a>" ;
//"+globalpath+"pandoc/"+data1[i].PAN_File+"'
  }
  else{
    var pfile= "<a href='#' onclick=showFileinModal('"+encodeURI(data1[i].PAN_File)+"','PAN','"+data1[i].EntryBy+"')>"+data1[i].PAN_File+"</a>" ;
//"+EmployerDocPath+"pandoc/"+data1[i].PAN_File+"
  }
 }

     var tablehead="<th class='sticky-col first-col'>Sr No</th><th class='sticky-col second-col'><a  style='color: white;'>Name</a></th><th>Mobile Number</th><th>Email</th><th>PAN Number</th><th>PAN Document</th><th>GST Number</th><th>GST Document</th><th>Registration Date</th><th>Status</th>"+btn+"";

         appenddata += "<tr class='i'><td class='sticky-col first-col'>"+[i+1]+"</td><td class='sticky-col second-col'>" +data1[i].CompName+ "</td><td>"+data1[i].ContactNo_M+"</td><td>" + data1[i].Email+ "</td><td>" + data1[i].PanNo+ "</td><td>"+pfile+"</td><td>"+data1[i].GST_Number+"</td><td>"+file+"</td><td>"+data1[i].RegDt+"</td><td>"+VStatus+"</td>"+btns+"";
          }
          jQuery("#tbodyvalue").html(appenddata);  
          $("#tablehead").html(tablehead);
          $('#tbodyvalue').clientSidePagination();
          addTopScrollbar($('#tbodyvalue').closest('table'), $('#tbodyvalue').closest('.form-group.m-form__group.row'));
          //jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');

        }             
  }

function modalmaildetail(employermailid){
  $('#myModalEmpVerify').modal('show')
var editmsg= 'It is regarding your registration on MY MP Rojgar Portal ( www.mprojgar.gov.in), it is noted  that your registration cannot be processed due to incomplete information or credentials (PAN, GST Number, Company details, Mobile number and official email id / you have not uploaded the copy of either PAN,GST). Please re-apply for registration with complete information and necessary documentation to be uploaded for verification purposes.'

  $("#Subject").val('MP Rojgar')
  $("#Employermail").val(employermailid);
  $("#bccMail").val('yatm.opshead@yashaswigroup.in');
  $("#mailbody").val(editmsg);
 
}


  function sendemail(employermailid,type,BCC,editmsg,sub){
    if(type=='verify'){
      var sub = "MP Rojgar";
      var body = "Dear Employer,<br>"+
      "<br>You are verified successfully.";
      sentmailglobal(employermailid,body,sub)
    }
    if(type=='reply'){
     
      sentmailreply(employermailid,BCC,editmsg,sub)
      $('#myModalEmpVerify').modal('hide')
    }
  
  
   }
function aboutdelete(Id){
   $("#deletebtn").on('click',function(){
    Delete(Id);
  $("#myModal").modal('hide')
   })
  }
   function Delete(Id) {
    jQuery.ajax({
        url: serverpath + "DeleteEmpDetail/'"+Id+"'",
      type: "GET",
      contentType: "application/json; charset=utf-8",
      cache: false,
      dataType: "json",
      success: function (data) {
        console.log(data);
      	fetchInActiveEmp('0','0','Fetch');
        InsupdEmpverificationlogs(Id,'Delete')
      }


    })
}

//     It is regarding your registration on MY MP Rojgar Portal ( www.mprojgar.gov.in), it is noted  that your registration cannot be processed due to incomplete information or credentials (PAN, GST Number, Company details, Mobile number and official email id / you have not uploaded the copy of either PAN,GST).<br><br>
//      Please re-apply for registration with complete information and necessary documentation to be uploaded for verification purposes.

function sentmailreply(to,BCC,editmsg,sub) {
  var msg=`<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
  <tr>
  <td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'>
  <img src='http://mprojgar.gov.in/images/header_name.gif' style='width:100%'> 
  </td>
   </tr>
   <tr>
   <td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'>
   <tr>
   <td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'>
  
   <div>
   <br> 
     Dear Sir / Madam,
    <br><br>
    This is an auto generated system email, please do not reply to it.<br><br>
    `+editmsg+`   <br><br>
   In case of any difficulty / or queries regarding the registration process kindly contact us as under:   <br><br>
   Customer Care Executive, <br>
   Email ID : helpdesk.mprojgar@mp.gov.in<br>
   Toll Free Number : 1800-5727-751<br>
   Office Hours : 9.30 am to 6.00 pm<br><br>
   Madhya Pradesh Government Employment Call Centre, <br>
   Operated by Yashaswi Academy for Talent Management.<br>
   Bhopal, Madhya Pradesh.
   </div></td></tr></table></td></tr>
  </table>`
          var MasterData = {
                 "To":to,
                 "Msg":msg,
                 "Subject":sub,
                 //"BCC":'yatm.opshead@yashaswigroup.in'
                 "BCC":BCC
                
          }
          MasterData = JSON.stringify(MasterData)
          var path = serverpath + "sentmail";
          ajaxpost(path, 'datasentmail', 'comment', MasterData, 'control')
       }
        function datasentmail(data) {
      data = JSON.parse(data)
      console.log(data);
   
   }
   function showFileinModal(file,type,entryby){
     var path;
     var extension=file.split('.')[1];
     if(entryby=='Olex'){
      if(type=='GST'){
        path=globalpath+"gstdoc/"+decodeURI(file);
      }
      else if(type=='PAN'){
      
        path=globalpath+"pandoc/"+decodeURI(file);
      }
     }
     else{
      if(type=='GST'){
        path=EmployerDocPath+"gstdoc/"+decodeURI(file);
      }
      else if(type=='PAN'){
      
        path=EmployerDocPath+"pandoc/"+decodeURI(file);
      }
     }
     $('#modalFile').modal('toggle');
     console.log(extension);
     
    if(extension=='jpg'||extension=='png'||extension=='jpeg'){
      $('#filesrc').html(`<a target='blank'  href='${path}'><img src="${path}"  style="width: 610px;" download/></a>`);
    } 
    else if(extension=='pdf' ||extension=='doc'){

      $('#filesrc').html(`<a target='blank'  href='${path}'> <iframe   src="${path}"  frameborder="0"> </iframe></a>`);

    }
    $('#zoom').html(`<a target='blank'  href='${path}'><button type="button" class="btn btn-default" style="background-color:#716aca;border-color:#716aca;">Zoom</button></a>`);
   }


   
   function InsupdEmpverificationlogs(EmpId,Action) {

    var MasterData = {
    "p_EmpId":EmpId,
    "p_Action":Action,
    "p_ActionTakenBy":sessionStorage.Emp_Id,
    "p_IpAddress": sessionStorage.ipAddress,
    
    
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "empverificationlogs";
    securedajaxpost(path, 'parsrdataEmpverificationlogs', 'comment', MasterData, 'control')
    }
    
    
    function parsrdataEmpverificationlogs(data) {
    data = JSON.parse(data)
    console.log(data);
    
    }
