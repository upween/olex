createcaptchaforgot();
var codeforgot;
                function createcaptchaforgot() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaforgot').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaforgot";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeforgot = captcha.join("");
                  document.getElementById("captchaforgot").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaforgot() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchaTextBoxforgot").value == codeforgot) {
                    ValidateLoginHome() ;
                    
                  }else{
                    jQuery('#cpatchaTextBoxforgot').val("");
                    toastr.warning("Invalid Captcha", "", "info");
                    createcaptchaforgot();
                    jQuery('#cpatchaTextBoxforgot').css('border-color', 'red'); 
                    return true;
                    
                  }
                }