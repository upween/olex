
function FilldivExchange(funct,control) {
    var path =  serverpath + "MstDivision/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFilldivExchange(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FilldivExchange('parsedatasecuredFilldivExchange','ddlDivision');
     
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Division Name"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Division_id).html(data1[i].Division_name));
             }
        }
              
}

jQuery('#ddlDivision').on('change', function () {

    FillExchange('parsedatasecuredFillExchange','ddlExchange',jQuery('#ddlDivision').val());
    
});

         
 
  


function FillExchange(funct,control,Division_id) {
    var path =  serverpath + "WorkingExchange/2/"+Division_id+"/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillExchange(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillExchange('parsedatasecuredFillExchange','ddlExchange');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Exchange"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
        }
              
}