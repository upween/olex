var globalString = "This can be accessed anywhere!";
//var serverpath = "https://rojgarportal.herokuapp.com/";
//var JobseekerPath=   "https://rojgar-jobseeker.herokuapp.com/";
//var EmployerDocPath = "https://rojgar-employer.herokuapp.com/";
//var globalpath = "https://rojgar-olex.herokuapp.com/";
var JobseekerPath = "http://182.70.254.93:300/";
var EmployerDocPath=   "http://vacancy.mprojgar.gov.in/";
var globalpath = "http://182.70.254.93:700/";
//var serverpath = "http://localhost:3003/";
//var serverpath="http://182.70.254.93:303/"
var serverpath = "http://mprojgar.gov.in/api/";
//var globalpath = "http://localhost:7200/";

var globalemailid = ''
var globalemailpass = ''
var isValidation=true;
var isCaptchaValidated=true;
$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
//ValidateLogin();
function securedajaxget(path, type, comment, control) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata), control);
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                window[type](JSON.stringify(errordata.responseJSON), control);
                return true;
            }
            else {
                window[type](JSON.stringify(errordata), control);
                return true;
            }
        }
    });
}
function securedajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxget(path, type, comment) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}





$('#dashboard').on('click', function () {
    $(this).addClass('m-menu__item--active');
});


$('#jobfair').on('click', function () {
    $(this).addClass('m-menu__item--active');
});
$('#database').on('click', function () {
    $(this).addClass('m-menu__item--active');
});




function searchTextInTable(tblName) {
    var tbl;
    tbl = tblName;
    jQuery("#" + tbl + " tr:has(td)").hide(); // Hide all the rows.

    var sSearchTerm = jQuery('#txtSearch').val(); //Get the search box value

    if (sSearchTerm.length == 0) //if nothing is entered then show all the rows.
    {
        jQuery("#" + tbl + " tr:has(td)").show();
        return false;
    }
    //Iterate through all the td.
    jQuery("#" + tbl + " tr:has(td)").children().each(function () {
        var cellText = jQuery(this).text().toLowerCase();
        if (cellText.indexOf(sSearchTerm.toLowerCase()) >= 0) //Check if data matches
        {
            jQuery(this).parent().show();
            return true;
        }
    });
    //e.preventDefault();
}
$("#username").html(sessionStorage.getItem('CandidateName'))
$("#userEmail").html(sessionStorage.getItem('EmailId'))
$("#username1").html(sessionStorage.getItem('CandidateName'))
$("#Exchange").html(sessionStorage.getItem('Ex_name'))
function EmailConfig() {
    var path = serverpath + "emailconfig"
    securedajaxget(path, 'parsedatasecuredEmailConfig', 'comment', 'control');
}

function parsedatasecuredEmailConfig(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        EmailConfig();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        globalemailid = data[0][0].Email
        globalemailpass = data[0][0].Password
    }

}
function employerurl() {
    window.location = EmployerLogoPath;
}
function employerurlreg() {
    window.location = EmployerLogoPath + "Registration";
}
function adminurl() {
    window.location = photogallerypath;
}
document.getElementsByClassName('yatmlogo').src = JobseekerPath + 'images/YATMLogo.png';
document.getElementsByClassName('mplogo').src = JobseekerPath + 'images/logo1.png';