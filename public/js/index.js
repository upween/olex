function FetchDashboardTable() {
	var path = serverpath + "USPGetSummary1"
	ajaxget(path, 'parsedataFetchDashboardTable', 'comment', "control");
}


function parsedataFetchDashboardTable(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchDashboardTable('parsedataFetchDashboardTable');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
                var appenddata="";
                var ur=0,sc=0,st=0,obc=0,da=0,total=0,withaadhar=0,withoutaadhar=0,mobile=0,nonmobile=0,male=0,female=0;
                
                  for (var i = 0; i < data[0].length; i++) {
                        appenddata += "<tr><td>"+ data[0][i].sno +"</td><td>" + data[0][i].Agegroup + "</td><td>" + data[0][i].Total + "</td><td>" + data[0][i].withAdhar + "</td><td>" + data[0][i].withoutAdhar + "</td><td >" + data[0][i].withMobile + "</td><td >" + data[0][i].withoutMobile + "</td><td >" + data[0][i].UR + "</td><td >" + data[0][i].SC + "</td><td >" + data[0][i].ST + "</td><td >" + data[0][i].OBC + "</td><td>"+ data[0][i].Male +"</td><td>"+ data[0][i].FeMale +"</td><td>"+ data[0][i].Phs +"</td></tr>";
                        ur = ur+data[0][i].UR
                        sc = sc+data[0][i].SC
                        st = st+data[0][i].ST
                        obc = ur+data[0][i].OBC
                        da = da+data[0][i].Phs
                        total = total+data[0][i].Total
                        withaadhar = withaadhar+data[0][i].withAdhar
                        withoutaadhar = withoutaadhar+data[0][i].withoutAdhar
                        mobile = mobile+data[0][i].withMobile
                        nonmobile = nonmobile+data[0][i].withoutMobile
                        male = male+data[0][i].Male
                        female = female+data[0][i].FeMale
                    } 
                    appenddata += "<tr style='background-color: beige;'><td></td><td>Total:</td><td>" + total + "</td><td>" + withaadhar + "</td><td>" + withoutaadhar + "</td><td >" + mobile + "</td><td >" + nonmobile + "</td><td >" + ur + "</td><td >" + sc + "</td><td >" + st + "</td><td >" + obc + "</td><td>"+ male +"</td><td>"+ female +"</td><td>"+ da +"</td></tr>";
                    jQuery("#tbodyvalue").html(appenddata);  
                    chart(ur,sc,st,obc,da)
        }
              
}
function chart(ur,sc,st,obc,da){
    var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['UR (General)', 'SC (Scheduled Caste)', 'ST (Scheduled Tribe)', 'OBC (Other Backward Classes)', 'DA (Differently Abled)'],
        datasets: [{
            label:'',
            data: [ur, sc, st, obc, da],
            backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            display: false
        }
    }
});
    
    
}
function chart1(Below,	th10,	th12,	Graduate,	Post,	ITI,	Certificate,	Diploma,	BE,	Other){
        
    var ctx = document.getElementById('myChart1').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Below 10th", "10th", "12th", "Graduate", "Post Graduate", "ITI", "Certificate", "Diploma", "BE", "Other"],
        datasets: [{
            label:'',
            data: [Below,	th10,	th12,	Graduate,	Post,	ITI,	Certificate,	Diploma,	BE,	Other],
            backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            display: false
        }
    }
});
    
}

function FetchDashboardTableQualificationWise() {
	var path = serverpath + "USPGetSummary2"
	ajaxget(path, 'parsedataFetchDashboardTableQualificationWise', 'comment', "control");
}


function parsedataFetchDashboardTableQualificationWise(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchDashboardTableQualificationWise('parsedataFetchDashboardTableQualificationWise');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
                var appenddata="";
               var Registration=0,	below=0,	th10=0,	th12=0,	Graduate=0,	Post=0,	ITI=0,	Certificate=0,	Diploma=0,	BE=0,	Other=0
                   for (var i = 0; i < data[0].length; i++) {
                        appenddata += "<tr><td>"+ data[0][i].sno +"</td><td>" + data[0][i].Agegroup + "</td><td>" + data[0][i].Total + "</td><td>" + data[0][i].Below10th + "</td><td>" + data[0][i].standard10th + "</td><td >" + data[0][i].standard12th + "</td><td >" + data[0][i].graduate + "</td><td >" + data[0][i].postgraduate + "</td><td >" + data[0][i].ITI + "</td><td >" + data[0][i].Certificate + "</td><td >" + data[0][i].diploma + "</td><td>"+ data[0][i].BE +"</td><td>"+ data[0][i].other +"</td></tr>";
                        Registration = Registration+data[0][i].Total
                        th10 = th10+data[0][i].standard10th
                        th12 = th12+data[0][i].standard12th
                        below = below+data[0][i].Below10th
                        Graduate = Graduate+data[0][i].graduate
                        Post = Post+data[0][i].postgraduate
                        ITI = ITI+data[0][i].ITI
                        Certificate = Certificate+data[0][i].Certificate
                        Diploma = Diploma+data[0][i].diploma
                        BE = BE+data[0][i].BE
                        Other = Other+data[0][i].other
                    } 

                    appenddata += "<tr  style='background-color: beige;'><td></td><td>Total:</td><td>" + Registration + "</td><td>" + below + "</td><td>" + th10 + "</td><td >" + th12 + "</td><td >" + Graduate + "</td><td >" + Post + "</td><td >" + ITI + "</td><td >" + Certificate + "</td><td >" + Diploma + "</td><td>"+ BE +"</td><td>"+ Other +"</td></tr>";
                    jQuery("#tbodyvalue2").html(appenddata);  
                    chart1(below,	th10,	th12,	Graduate,	Post,	ITI,	Certificate,	Diploma,	BE,	Other)          
                
           
        }
              
}


function FetchDashboardStatus() {
	var path = serverpath + "USPGetJobfairStatus"
	ajaxget(path, 'parsedataFetchDashboardStatus', 'comment', "control");
}


function parsedataFetchDashboardStatus(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FetchDashboardStatus('parsedataFetchDashboardStatus');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
              
                            
            $("#RegisterEmployee").html(data[0][0].totalRegcompany +' - '+ 'Registered Employer');       
            $("#JobFairTotal").html(data[0][0].TotalJF + ' - '+'Job Fair Total'); 
            $("#TotalPlaced").html(data[0][0].TotalPlaced + ' - '+'Total Placed'); 
        }
              
}


function JobFairCounter(){
    var exid=0;
    if (sessionStorage.getItem("User_Type_Id") == "1" || sessionStorage.getItem("User_Type_Id") == "0") {
        exid=0;
        
    }
    else{
        exid=sessionStorage.Ex_id;
    }
    var path = serverpath + "JobFairCounterDashboard/"+exid+""
    ajaxget(path,'parsedatacounter','comment','control');
}
function parsedatacounter(data,control){
    data = JSON.parse(data)
    $("#jobseeker").html(data[0][0].JobfairCount);
    $("#participant").html(data[0][0].ParticipantCount);
    $("#Employer").html(data[0][0].EmployerCount);
   $("#totalVacancies").html(data[0][0].VacancyCounts);
    $("#totalprimary").html(data[0][0].PrimarySelectionCount);
    $("#Final").html(data[0][0].FinalSelectionCount);
    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}