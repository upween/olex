// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });
// /* GET about page. */
// router.get('/about', function(req, res) {
//   res.render('pages/about');
// });
// module.exports = router;
module.exports = function (app) {

  app.get('/', function (req, res) {
    req.session.login = "Loggedout";
    res.render('pages/login');
  });
  app.get('/index', function (req, res) {
    req.session.login = "Loggedin";
    res.render('index');
  });

  app.get('/error', function (req, res) {
    res.render('pages/error');
  });

  app.get('/inner', function (req, res) {
    res.render('pages/inner');
  });
  app.get('/Search', function (req, res) {
    res.render('pages/Search');
  });

  app.get('/Table', function (req, res) {
    res.render('pages/Table');
  });
  app.get('/JobFairStatus', function (req, res) {
    res.render('pages/JobFair/JobFairStatus');
  });
  app.get('/CompanyDetails', function (req, res) {
    res.render('pages/JobFair/CompanyDetails');
  });
  app.get('/JobFairDetails', function (req, res) {
    res.render('pages/JobFair/JobFairDetails');
  });
  app.get('/JobFairVerification', function (req, res) {
    res.render('pages/JobFair/JobFairVerification');
  });
  app.get('/ModifyExistingJobFair', function (req, res) {
    res.render('pages/JobFair/ModifyExistingJobFair');
  });
  app.get('/GroupGuidanceDetails', function (req, res) {
    res.render('pages/GuidanceCounseling/GroupGuidanceDetails');
  });
  app.get('/GroupGuidanceVerification', function (req, res) {
    res.render('pages/GuidanceCounseling/GroupGuidanceVerification');
  });
 
  app.get('/DesignationDetail', function (req, res) {
    res.render('pages/JobFair/DesignationDetail');
  });
  app.get('/DetailJobFair', function (req, res) {
    res.render('pages/JobFair/DetailJobFair');
  });
  app.get('/JobFairReport', function (req, res) {
    res.render('pages/JobFair/JobFairReport');
  });
  app.get('/CareerCounsellingReport', function (req, res) {
    res.render('pages/CareerCounselling/CareerCounsellingReport');
  });
  app.get('/CareerCounsellingDetails', function (req, res) {
    res.render('pages/CareerCounselling/CareerCounsellingDetails');
  });
  app.get('/SelectedJobseekerDetails', function (req, res) {
    res.render('pages/CareerCounselling/SelectedJobseekerDetails');
  });
  app.get('/ResetJobSeekerPassword', function (req, res) {
    res.render('pages/UserManagement/ResetJobSeekerPassword');
  });
  
  app.get('/StatisticalReportJobSeeker', function (req, res) {
    res.render('pages/ReportsForJS/StatisticalReportJobSeeker');
  });
  app.get('/MonthlyRegisterDateWise', function (req, res) {
    res.render('pages/ReportsForJS/MonthlyRegisterDateWise');
  });
  app.get('/X63', function (req, res) {
    res.render('pages/ReportsForJS/X63');
  });
  app.get('/GenerateCallLetter', function (req, res) {
    res.render('pages/DatabaseSearchingUtility/GenerateCallLetter');
  });
  app.get('/NotWorkingExchangeList', function (req, res) {
    res.render('pages/ReportsForJS/NotWorkingExchangeList');
  });
  app.get('/BookingStatusReport', function (req, res) {
    res.render('pages/ReportsForJS/BookingStatusReport');
  });
  app.get('/RenewalStatusReport', function (req, res) {
    res.render('pages/ReportsForJS/RenewalStatusReport');
  });
  app.get('/SummaryJSExchWise', function (req, res) {
    res.render('pages/ReportsForJS/SummaryJSExchWise');
  });
  app.get('/NoOfJSQualificationWise', function (req, res) {
    res.render('pages/ReportsForJS/NoOfJSQualificationWise');
  });
  app.get('/YearWiseLRStatus', function (req, res) {
    res.render('pages/ReportsForJS/YearWiseLRStatus');
  });
  app.get('/PrintReportProforma', function (req, res) {
    res.render('pages/ReportsForJS/PrintReportProforma');
  });
  app.get('/CurrentStatusReport', function (req, res) {
    res.render('pages/ReportsForJS/CurrentStatusReport');
  });
  app.get('/MonthlyRegStatus', function (req, res) {
    res.render('pages/ReportsForJS/MonthlyRegStatus');
  });
  app.get('/DuplicateRegistrationStatus', function (req, res) {
    res.render('pages/ReportsForJS/DuplicateRegistrationStatus');
  });
  app.get('/ExchangeStatusSendToHO', function (req, res) {
    res.render('pages/ReportsForJS/ExchangeStatusSendToHO');
  });
  app.get('/SummaryReport', function (req, res) {
    res.render('pages/ReportsForJS/SummaryReport');
  });
  app.get('/ExchangeStatus&RevertBackProcess', function (req, res) {
    res.render('pages/ReportsForJS/ExchangeStatus&RevertBackProcess');
  });
  app.get('/GenerateSMS', function (req, res) {
    res.render('pages/DatabaseSearchingUtility/GenerateSMS');
  });
  app.get('/WebLinksCategory', function (req, res) {
    res.render('pages/Weblinks/WebLinksCategory');
  });
  app.get('/WebLinksURL', function (req, res) {
    res.render('pages/Weblinks/WebLinksURL');
  });
  app.get('/RegistredEmployerDetails', function (req, res) {
    res.render('pages/ExchangeMajorReports/RegistredEmployerDetails');
  });
  app.get('/FeedBackForm', function (req, res) {
    res.render('pages/EmployeeFeedback/FeedBackForm');
  });
  app.get('/FeedbackDetail', function (req, res) {
    res.render('pages/EmployeeFeedback/FeedbackDetail');
  });
  app.get('/AdvertisementCategory', function (req, res) {
    res.render('pages/AdvertisementSection/AdvertisementCategory');
  });
  app.get('/AdvertisementUpload', function (req, res) {
    res.render('pages/AdvertisementSection/AdvertisementUpload');
  });
  app.get('/EventDetailsMaster', function (req, res) {
    res.render('pages/PhotoGallery/EventDetailsMaster');
  });
  app.get('/PhotoUpload', function (req, res) {
    res.render('pages/PhotoGallery/PhotoUpload');
  });
  app.get('/ExchangeOfficeType', function (req, res) {
    res.render('pages/UserManagement/ExchangeOfficeType');
  });
  app.get('/ExchangeOffice', function (req, res) {
    res.render('pages/UserManagement/ExchangeOffice');
  });
  app.get('/MenuMaster', function (req, res) {
    res.render('pages/UserManagement/MenuMaster');
  });
  app.get('/AssignEmployeeSeniority', function (req, res) {
    res.render('pages/UserManagement/AssignEmployeeSeniority');
  });
  app.get('/EmployeeMaster', function (req, res) {
    res.render('pages/UserManagement/EmployeeMaster');
  });
  app.get('/SubMenu', function (req, res) {
    res.render('pages/UserManagement/SubMenu');
  });
  app.get('/UserLoginCreation', function (req, res) {
    res.render('pages/UserManagement/UserLoginCreation');
  });
  app.get('/ChangePassword', function (req, res) {
    res.render('pages/UserManagement/ChangePassword');
  });
  app.get('/NewEmployerRegistration', function (req, res) {
    res.render('pages/EmployerServices/NewEmployerRegistration');
  });
  app.get('/Empdetails', function (req, res) {
    res.render('pages/EmployerServices/Userdetails');
  });
  app.get('/EmpProfileview', function (req, res) {
    res.render('pages/EmployerServices/EmpProfileview');
  });
  app.get('/NICAllotmentSystem', function (req, res) {
    res.render('pages/EmployerServices/NICAllotmentSystem');
  });
  app.get('/RegistrationEmployer', function (req, res) {
    res.render('pages/JobFair/RegistrationEmployer');
  });
  app.get('/PostCreate', function (req, res) {
    res.render('pages/JobFair/PostCreate');
  });
  app.get('/VacancyNotificationDetails', function (req, res) {
    res.render('pages/EmployerServices/VacancyNotificationDetails');
  });
  app.get('/QualificationWiseJobSeekerSearch', function (req, res) {
    res.render('pages/EmployerServices/QualificationWiseJobSeekerSearch');
  });
  app.get('/EmployerSearchVerfication', function (req, res) {
    res.render('pages/EmployerServices/EmployerSearchVerfication');
  });
  app.get('/Verification', function (req, res) {
    res.render('pages/JobseekerServices/Verification');
  });
  app.get('/RegistrationTransferRecieved', function (req, res) {
    res.render('pages/JobseekerServices/RegistrationTransferRecieved');
  });
  app.get('/RegistrationTransferTootherexchange', function (req, res) {
    res.render('pages/JobseekerServices/RegistrationTransferTootherexchange');
  });
  app.get('/RenewalofJobSeeker', function (req, res) {
    res.render('pages/JobseekerServices/RenewalofJobSeeker');
  });
  app.get('/RegistrationCancellation', function (req, res) {
    res.render('pages/JobseekerServices/RegistrationCancellation');
  });
  app.get('/RegistrationRecievedOnTransfermanually', function (req, res) {
    res.render('pages/JobseekerServices/RegistrationRecievedOnTransfermanually');
  });
  app.get('/JobseekerLiveRegister', function (req, res) {
    res.render('pages/ReportsForJS/JobseekerLiveRegister');
  });
  app.get('/ReportSearching', function (req, res) {
    res.render('pages/ReportsForJS/ReportSearching');
  });
  app.get('/TransferRecievedstatus', function (req, res) {
    res.render('pages/ReportsForJS/TransferRecievedstatus');
  });
  app.get('/JobSeekerRegistrationDeadRecords', function (req, res) {
    res.render('pages/ReportsForJS/JobSeekerRegistrationDeadRecords');
  });
  app.get('/LapseandLapseDueReport', function (req, res) {
    res.render('pages/ReportsForJS/LapseandLapseDueReport');
  });
  app.get('/ITIandQualificationSummaryReport', function (req, res) {
    res.render('pages/ReportsForJS/ITIandQualificationSummaryReport');
  });
  app.get('/RenewalLogReport', function (req, res) {
    res.render('pages/ReportsForJS/RenewalLogReport');
  });
  app.get('/TransferStatusReport', function (req, res) {
    res.render('pages/ReportsForJS/TransferStatusReport');
  });
  app.get('/TransferAndCancelRollbackAndReRegistration', function (req, res) {
    res.render('pages/JobseekerServices/TransferAndCancelRollbackAndReRegistration');
  });
  app.get('/MasterReports', function (req, res) {
    res.render('pages/EMIReports/MasterReports');
  });
  app.get('/ERIBasedReports', function (req, res) {
    res.render('pages/EMIReports/ERIBasedReports');
  });
  app.get('/ERIfinalReports', function (req, res) {
    res.render('pages/EMIReports/ERIfinalReports');
  });
  app.get('/OEIAnnualStatementForPublic', function (req, res) {
    res.render('pages/EMIReports/OEIAnnualStatementForPublic');
  });
  app.get('/OEIAnnualStatementForPrivate', function (req, res) {
    res.render('pages/EMIReports/OEIAnnualStatementForPrivate');
  });
  app.get('/AppendixX', function (req, res) {
    res.render('pages/EMIReports/AppendixX');
  });
  app.get('/DuplicateCardIssueDetails', function (req, res) {
    res.render('pages/ReportsForJS/DuplicateCardIssueDetails');
  });
  app.get('/ES11Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES11Report');
  });

  app.get('/ES12Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES12Report');
  });
  app.get('/ES13Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES13Report');
  });
  app.get('/ES14Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES14Report');
  });
  app.get('/ES21Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES21Report');
  });
  app.get('/ES23Report', function (req, res) {
    res.render('pages/ReportProformaEntry/ES23Report');
  });
  app.get('/ES24ReportP1', function (req, res) {
    res.render('pages/ReportProformaEntry/ES24ReportP1');
  });
  app.get('/ES24ReportP2', function (req, res) {
    res.render('pages/ReportProformaEntry/ES24ReportP2');
  });
  app.get('/ES25ReportP1', function (req, res) {
    res.render('pages/ReportProformaEntry/ES25ReportP1');
  });
  app.get('/ES25ReportP2', function (req, res) {
    res.render('pages/ReportProformaEntry/ES25ReportP2');
  });
  app.get('/CareerCouncellingMonthlyProforma', function (req, res) {
    res.render('pages/ReportProformaEntry/CareerCouncellingMonthlyProforma');
  });
  app.get('/JobFairMonthlyProforma', function (req, res) {
    res.render('pages/ReportProformaEntry/JobFairMonthlyProforma');
  });
  app.get('/MonthlyNarrativeReport', function (req, res) {
    res.render('pages/ReportProformaEntry/MonthlyNarrativeReport');
  });
  app.get('/MonthlyNarrativeReport', function (req, res) {
    res.render('pages/ReportProformaEntry/MonthlyNarrativeReport');
  });
  app.get('/ES22AnnualReport', function (req, res) {
    res.render('pages/ReportProformaEntry/ES22AnnualReport');
  });
  app.get('/ES11MonthlyReport', function (req, res) {
    res.render('pages/ExchangeMajorReports/ES11MonthlyReport');
  });
  app.get('/JobseekerInformation', function (req, res) {
    res.render('pages/JobseekerServices/JobseekerInformation');
  });
  app.get('/CVTemplate', function (req, res) {
    res.render('pages/JobseekerServices/CVTemplate');
  });
  app.get('/JobSeekerProfile', function (req, res) {
    res.render('pages/JobseekerServices/JobSeekerProfile');
  });
  app.get('/JobseekerServices/ChangePass', function (req, res) {
    res.render('pages/JobseekerServices/ChangePass');
  });
  app.get('/PrintRegCard', function (req, res) {
    res.render('pages/JobseekerServices/PrintRegCard');
  });
  app.get('/EmployerRegistrationDetails', function (req, res) {
    res.render('pages/EmployerServices/EmployerRegistrationDetails');
  });
  app.get('/EmployerServices/ChangePassword', function (req, res) {
    res.render('pages/EmployerServices/ChangePassword');
  });
  app.get('/RegisterJobseekerlist', function (req, res) {
    res.render('pages/JobFair/RegisterJobseekerlist');
  });
  app.get('/PlacementDetails', function (req, res) {
    res.render('pages/PlacementDetails/PlacementForm');
  });
  
  app.get('/ER1SubmissionDetails', function (req, res) {
    res.render('pages/EmployerServices/ER1SubmissionDetails');
  });

  app.get('/ER1Submissionandverification', function (req, res) {
    res.render('pages/EmployerServices/ER1SubmissionandVerification');
  });

  app.get('/createordercard', function (req, res) {
    res.render('pages/VacancySubmission/createordercard');
  });

  app.get('/Shortlistjsordercardwise', function (req, res) {
    res.render('pages/VacancySubmission/Shortlistjsordercardwise');
  });
  app.get('/SendSubmissionListToEmployer', function (req, res) {
    res.render('pages/VacancySubmission/SendSubmissionListToEmployer');
  });

  app.get('/SendselectedfromLRtoDR', function (req, res) {
    res.render('pages/VacancySubmission/SendselectedfromLRtoDR');
  });

  app.get('/SubmissionNotRequired', function (req, res) {
    res.render('pages/VacancySubmission/SubmissionNotRequired');
  });

  app.get('/SelectJSOrderCardWise', function (req, res) {
    res.render('pages/VacancySubmission/SelectJSOrderCardWise');
  });

  app.get('/VerifySelectionList', function (req, res) {
    res.render('pages/VacancySubmission/VerifySelectionList');
  });

  app.get('/SubmissionOrderReport', function (req, res) {
    res.render('pages/VacancySubmission/SubmissionOrderReport');
  });

  app.get('/LogMaintenaceRegister', function (req, res) {
    res.render('pages/ReportsForJS/LogMaintenaceRegister');
  });

  app.get('/BackLogLR', function (req, res) {
    res.render('pages/BackLogVerification/BackLogLR');
  });

  app.get('/BackLogDR', function (req, res) {
    res.render('pages/BackLogVerification/BackLogDR');
  });

  app.get('/ChangeRegNoAndDate', function (req, res) {
    res.render('pages/BackLogVerification/ChangeRegNoAndDate');
  });
  app.get('/NarativeMonthlyRpt', function (req, res) {
    res.render('pages/ReportForJS/NarativeMonthlyReport');
  });
  app.get('/JobSeekerModifiedDataList', function (req, res) {
    res.render('pages/PlacementDetails/JobSeekerModifiedDataListReport');
  });
  app.get('/es14reporttable', function (req, res) {
    res.render('pages/ReportForJS/es14reporttable');
  });
  app.get('/es23reporttable', function (req, res) {
    res.render('pages/ReportForJS/es23reporttable');
  });
  app.get('/es24reporttable', function (req, res) {
    res.render('pages/ReportForJS/es24reporttable');
  });
  app.get('/es24reporttable2', function (req, res) {
    res.render('pages/ReportForJS/es24reporttable2');
  });
  app.get('/qulirpttable', function (req, res) {
    res.render('pages/ReportForJS/qulirpttable');
  });
  app.get('/categoryrpyt', function (req, res) {
    res.render('pages/ReportForJS/categoryrpyt');
  });
  app.get('/es25rptpart2table', function (req, res) {
    res.render('pages/ReportForJS/es25rptpart2table');
  });
  app.get('/es21rpttable', function (req, res) {
    res.render('pages/ReportForJS/es21rpttable');
  });
  app.get('/es12rpttable', function (req, res) {
    res.render('pages/ReportForJS/es12rpttable');
  });
  app.get('/es13rpttable', function (req, res) {
    res.render('pages/ReportForJS/es13rpttable');
  });
  app.get('/jobfairrpttable', function (req, res) {
    res.render('pages/ReportForJS/jobfairrpttable');
  });
  app.get('/ccrpttable', function (req, res) {
    res.render('pages/ReportForJS/ccrpttable');
  });
  app.get('/jfcrpttable', function (req, res) {
    res.render('pages/ReportForJS/jfcrpttable');
  });
  app.get('/modifyrpttable', function (req, res) {
    res.render('pages/ReportForJS/modifyrpttable');
  });
  app.get('/PlacementForm', function (req, res) {
    res.render('pages/PlacementDetails/PlacementForm');
  });
  app.get('/placementreport', function (req, res) {
    res.render('pages/PlacementDetails/placementreport');
  });
  app.get('/olx', function (req, res) {
    res.render('pages/olx');
  });
  app.get('/village', function (req, res) {
    res.render('pages/Masters/village');
  });

   
  app.get('/headline', function (req, res) {
    res.render('pages/Masters/headline');
      });
  app.get('/Employerverification', function (req, res) {
    res.render('pages/Employerverification');
  });
  app.get('/StateMaster', function (req, res) {
    res.render('pages/Masters/StateMaster');
  });
  app.get('/Sectormaster', function (req, res) {
    res.render('pages/Masters/Sectormaster');
  });
  app.get('/Rolemaster', function (req, res) {
    res.render('pages/Masters/Rolemaster');
  });
  app.get('/SectorRoleMapping', function (req, res) {
    res.render('pages/Masters/SectorRoleMapping');
  });

  app.get('/Tehsil', function (req, res) {
    res.render('pages/Masters/Tehsil');
  });
  app.get('/CourseTypeMaster', function (req, res) {
    res.render('pages/Masters/CourseTypeMaster');
  });
  app.get('/District', function (req, res) {
    res.render('pages/Masters/District');
  });
  app.get('/Universitymaster', function (req, res) {
    res.render('pages/Masters/Universitymaster');
  });
  app.get('/LanguageType', function (req, res) {
    res.render('pages/Masters/LanguageType');
  });
  app.get('/QualificationLevel', function (req, res) {
    res.render('pages/Masters/QualificationLevel');
  });
  app.get('/Category', function (req, res) {
    res.render('pages/Masters/Category');
  });
  app.get('/SectorType', function (req, res) {
    res.render('pages/Masters/SectorType');
  });
  app.get('/CompanyType', function (req, res) {
    res.render('pages/Masters/CompanyType');
  });
  app.get('/Ncccertificate', function (req, res) {
    res.render('pages/Masters/Ncccertificate');
  });
  app.get('/SearchCandidate', function (req, res) {
    res.render('pages/JobFair/SearchCandidate');
  });
  app.get('/Activity', function (req, res) {
    res.render('pages/Masters/Activity');
  });
  app.get('/VacancyTypeMaster', function (req, res) {
    res.render('pages/Masters/VacancyTypeMaster');
  });
  app.get('/bloodgroupmaster', function (req, res) {
    res.render('pages/Masters/bloodgroupmaster');
  });
  app.get('/PreferredLocationMaster', function (req, res) {
    res.render('pages/Masters/PreferredLocationMaster');
  });

  app.get('/MaritalStatus', function (req, res) {
    res.render('pages/Masters/MaritalStatus');
  });
  app.get('/NationalityMaster', function (req, res) {
    res.render('pages/Masters/NationalityMaster');
  });
  app.get('/HandicapMaster', function (req, res) {
    res.render('pages/Masters/HandicapMaster');
  });
 app.get('/InterstedCandidateReport', function (req, res) {
    res.render('pages/JobFair/InterstedCandidateReport');
  });

  app.get('/ReligionMaster', function (req, res) {
    res.render('pages/Masters/ReligionMaster');
  });
  app.get('/HandiCaptSubCategoryMaster', function (req, res) {
    res.render('pages/Masters/HandiCaptSubCategoryMaster');
  });
  app.get('/ITI/JobFairDetails', function (req, res) {
    res.render('pages/ITI/JobFairDetails');
  });
  app.get('/ITI/CompanyRegister', function (req, res) {
    res.render('pages/ITI/CompanyRegister');
  });
  app.get('/ITI/JobfairEmployerMapping', function (req, res) {
    res.render('pages/ITI/JobfairEmployerMapping');
  });
  app.get('/ITI/JobFairResultUpload', function (req, res) {
    res.render('pages/ITI/JobFairResult');
  });
  app.get('/ITI/JobFairResult', function (req, res) {
    res.render('pages/ITI/JobFairResultWithCheck');
  });
  app.get('/CancelJob', function (req, res) {
    res.render('pages/EmployerServices/CancelJob');
  });
  // app.get('/Dashboard', function (req, res) {
  //   res.render('pages/Dashboard');
  // });
  
  app.get('/UserDetails', function (req, res) {
    res.render('pages/UserManagement/UserDetails');
  });
 
  app.get('/EditDetail', function (req, res) {
    res.render('pages/Masters/EditDetail');
  });
  app.get('/Viewprofile', function (req, res) {
    res.render('pages/UserManagement/Viewprofile');
  });  
  app.get('/ContactUs', function (req, res) {
    res.render('pages/Masters/ContactUs');
  });
  app.get('/PrintRegistrationcard', function(req, res) {
    res.render('pages/UserManagement/PrintRegistrationcard');
  });

  app.get('/EditUserDetails', function (req, res) {
    res.render('pages/UserManagement/EditUserDetails');
  });

  app.get('/UserCreations', function (req, res) {
    res.render('pages/UserManagement/UserCreations');
  });
 
  app.get('/SendEmail', function (req, res) {
    res.render('pages/UserManagement/SendEmail');
  });
  app.get('/ViewES11Report', function (req, res) {
    res.render('pages/ReportsForJS/ViewES11Report');
  });
  
  app.get('/ES11Reportforview', function (req, res) {
    res.render('pages/ReportsForJS/ES11Reportforview');
  });
  app.get('/CounsellingList', function (req, res) {
    res.render('pages/CareerCounselling/CounsellingList');
  });
  app.get('/SendBulkContent', function (req, res) {
    res.render('pages/UserManagement/SendBulkContent');
  });
  app.get('/PlacementReportByExcel', function (req, res) {
    res.render('pages/PlacementDetails/PlacementReportByExcel');
  });
  app.get('/UserPrivileges', function (req, res) {
    res.render('pages/UserManagement/UserPrivileges');
  });
  app.get('/InterestedCandidateList', function (req, res) {
    res.render('pages/EmployerServices/InterestedCandidateList');
  });
  app.get('/RoleMenuMapping', function (req, res) {
    res.render('pages/UserManagement/RoleMenuMapping');
  });
  app.get('/RoleSubmenuMapping', function (req, res) {
    res.render('pages/UserManagement/RoleSubmenuMapping');
  });

  app.get('/ER1Form', function (req, res) {
    res.render('pages/ER1Form');
  });

  app.get('/ER1FormDetails', function (req, res) {
    res.render('pages/ER1FormDetails');
  });
  
  app.get('/AgewiseQualification', function (req, res) {
    res.render('pages/AgewiseQualification');
  });

  app.get('/AgewiseQualifiLevel', function (req, res) {
    res.render('pages/AgewiseQualifiLevel');
  });
  
  app.get('/AgewiseQualifiLevel', function (req, res) {
    res.render('pages/AgewiseQualifiLevel');
  });
  
  app.get('/SearchParticipant', function (req, res) {
    res.render('pages/JobFair/SearchParticipant');
  });
  app.get('/JobfairParticipantReport', function (req, res) {
    res.render('pages/JobFair/JobfairParticipantReport');
  });
  app.get('/EmployerServices/ER1Form', function (req, res) {
    res.render('pages/EmployerServices/ER1Form');
  });
  app.get('/Dashboardnew', function (req, res) {
    res.render('pages/Dashboardnew');
  });
  app.get('/PlacementCount', function (req, res) {
    res.render('pages/PlacementCount');
  });
  
  app.get('/ER1empRpt', function (req, res) {
    res.render('pages/ER1empRpt');
  });

  app.get('/Dashboard', function (req, res) {
    res.render('pages/EmployerServices/Dashboard');
  });

  app.get('/ER1EmpCount', function (req, res) {
    res.render('pages/ER1EmpCount');
  });

  app.get('/ExwiseEmpDetails', function (req, res) {
    res.render('pages/ExwiseEmpDetails');
  });

  app.get('/Jobfairemployerreport', function (req, res) {
    res.render('pages/JobFair/Jobfairemployerreport');
  });

  app.get('/Employerlogolist', function (req, res) {
    res.render('pages/EmployerServices/Employerlogolist');
  });
  app.get('/LRReportLiterate', function (req, res) {
    res.render('pages/ReportsForJS/LRReportLiterate');
  });
  app.get('/Datewiseregrenewal', function (req, res) {
    res.render('pages/ReportsForJS/Datewiseregrenewal');
  });
  app.get('/Upcomingjobfair', function (req, res) {
    res.render('pages/JobFair/Upcomingjobfair');
  });
  app.get('/Datewisejobfair', function (req, res) {
    res.render('pages/JobFair/Datewisejobfair');
  });
  app.get('/ClientDetails', function (req, res) {
    res.render('pages/DWR/ClientDetails');
  });
  app.get('/PlacementExchangeStatus', function (req, res) {
    res.render('pages/PlacementDetails/PlacementExchangeStatus');
  });

  app.get('/ActiveJobfairList', function (req, res) {
    res.render('pages/JobFair/ActiveJobfairList');
  });
  app.get('/JobFairReportExchangeWise', function (req, res) {
    res.render('pages/JobFair/JobFairReportExchangeWise');
  });

  app.get('/RegistrationJobseeker', function (req, res) {
    res.render('pages/JobFair/RegistrationJobseeker');
  });
  app.get('/JobfairEmployerReport', function (req, res) {
    res.render('pages/JobFair/JobfairEmployerReport');
  });

   app.get('/JobfairReportManual', function (req, res) {
    res.render('pages/JobFair/JobfairReportManual');
  });
  app.get('/SearchEngine', function (req, res) {
    res.render('pages/DatabaseSearchingUtility/SearchEngine');
  });


  app.get('/JSRegistration', function (req, res) {
    res.render('pages/JobSeekerRegistrationXML');
  });

  app.get('/Appointmententry', function (req, res) {
    res.render('pages/ReportProformaEntry/Appointmententry');
  });
  app.get('/NewAppointmentEntry', function (req, res) {
    res.render('pages/ReportProformaEntry/NewAppointmentEntry');
  });

  app.get('/EmploymentEntry', function (req, res) {
    res.render('pages/ReportProformaEntry/EmploymentEntry');
    });

  app.get('/SelfEmploymentEntry', function (req, res) {
    res.render('pages/Employment/SelfEmploymentEntry');
  });

  app.get('/SelfEmploymentReport', function (req, res) {
    res.render('pages/Employment/SelfEmploymentReport');
  });

  app.get('/EmploymentAndSelfemployment', function (req, res) {
    res.render('pages/Employment/EmploymentAndSelfemployment');
  });
  app.get('/Department', function (req, res) {
    res.render('pages/Masters/Department');
  });

  app.get('/Schememaster', function (req, res) {
    res.render('pages/Masters/Schememaster');
  });

  app.get('/SelfEmploymentDepartementwise', function (req, res) {
    res.render('pages/Employment/SelfEmploymentDepartementwise');
  });
  app.get('/EmploymentReport', function (req, res) {
    res.render('pages/ReportsForJS/EmploymentReport');
  });

  app.get('/EmploymentCutoffMaster', function (req, res) {
    res.render('pages/Masters/EmploymentCutoffMaster');
  });
  app.get('/EmploymentReportCategoryWise', function (req, res) {
    res.render('pages/ReportsForJS/EmploymentReportCategoryWise');
  });

  app.get('/Reminder', function (req, res) {
    res.render('pages/JobFair/Reminder');
  });

  app.get('/SearchEngineUtility', function (req, res) {
    console.log(req.cookies.User_Type_Id);
    if(req.cookies.User_Type_Id == '1'||req.cookies.User_Type_Id == '2'||req.cookies.User_Type_Id == '7'){
      res.render('pages/DatabaseSearchingUtility/SearchEngineUtility');
    }
    else{
      res.render('pages/DatabaseSearchingUtility/SearchEngineUtility_Yatm');
    }

  });

  app.get('/Placementverificationstatus', function (req, res) {
    res.render('pages/PlacementDetails/Placementverificationstatus');
  });

  app.get('/placementreportMonths', function (req, res) {
    res.render('pages/PlacementDetails/placementreportMonths');
    });
    app.get('/placementlogsdetails', function (req, res) {
      res.render('pages/PlacementDetails/placementlogsdetails');
      });

      app.get('/softskillsdetails', function (req, res) {
        res.render('pages/Softskills/softskillsdetails');
      });
      app.get('/softskillsSearchParticipant', function (req, res) {
        res.render('pages/Softskills/softskillsSearchParticipant');
      });
      app.get('/softskillsParticipantReport', function (req, res) {
        res.render('pages/Softskills/softskillsParticipantReport');
      });
      app.get('/softskillReport', function (req, res) {
        res.render('pages/Softskills/softskillReport');
      });
      app.get('/CareerSearchParticipant', function (req, res) {
        res.render('pages/CareerCounselling/CareerSearchParticipant');
      });
      app.get('/CareerParticipantReport', function (req, res) {
        res.render('pages/CareerCounselling/CareerParticipantReport');
      });
      app.get('/CareerReport', function (req, res) {
        res.render('pages/CareerCounselling/CareerReport');
      });
      app.get('/JsDrReport', function (req, res) {
        res.render('pages/ReportsForJS/JsDrReport');
      });
      app.get('/DailyWorkEntry', function (req, res) {
        res.render('pages/DWR/DailyWorkEntry');
      }); 
      
      app.get('/DailyWorkReport', function (req, res) {
        res.render('pages/DWR/DailyWorkReport');
      }); 
    
      app.get('/joiningdetails', function (req, res) {
        res.render('pages/DWR/joiningdetails');
        });
    
        app.get('/ClientDetails', function (req, res) {
          res.render('pages/DWR/ClientDetails');
        });
        app.get('/BulkMail', function (req, res) {
          res.render('pages/CRM/BulkMail1');
        });
        app.get('/crmsms', function (req, res) {
          res.render('pages/CRM/crmsms1');
        });
        app.get('/TemplateSMSMaster', function (req, res) {
          res.render('pages/Masters/TemplateSMSMaster');
        });
        app.get('/crmsmsreport', function (req, res) {
          res.render('pages/CRM/crmsmsreport1');
        });
        app.get('/crmuserdetails', function (req, res) {
          res.render('pages/CRM/crmuserdetails');
        });
        app.get('/crmqueryform', function (req, res) {
          res.render('pages/CRM/crmqueryform');
        });
        app.get('/EmploymentReportCategory', function (req, res) {
          res.render('pages/ReportsForJS/EmploymentReportCategory');
        });
        app.get('/Callingdwr', function (req, res) {
          res.render('pages/DWR/Callingdwr1');
        });
        app.get('/DwrcallingReport', function (req, res) {
          res.render('pages/DWR/DwrcallingReport1');
        });
        app.get('/DwrCallingReportclientwise', function (req, res) {
          res.render('pages/DWR/DwrCallingReportclientwise');
        });
        app.get('/dwr_clientsMaster', function (req, res) {
          res.render('pages/DWR/dwr_clientsMaster');
        });
        app.get('/DwrCallingReportDatewise', function (req, res) {
          res.render('pages/DWR/DwrCallingReportDatewise');
        });
        app.get('/PlacementDetailsDistrictwise', function (req, res) {
          res.render('pages/PlacementDetails/PlacementDetailsDistrictwise');
        });
        app.get('/JobfairEmploymentReportAgencyWise', function (req, res) {
          res.render('pages/ReportsForJS/JobfairEmploymentReportAgencyWise');
        });
        app.get('/EmploymentOfficesMaster', function (req, res) {
          res.render('pages/Masters/EmploymentOfficesMaster');
          });
          app.get('/DWRRequestMaster', function (req, res) {
            res.render('pages/Masters/DWRRequestMaster');
          });
        
          app.get('/CRMdashboard', function (req, res) {
            res.render('pages/CRM/crmdashboard');
          });
          app.get('/OlexResetPassword', function (req, res) {
            res.render('pages/Masters/OlexResetPassword');
          });
          app.get('/OutboundTracker', function (req, res) {
            res.render('pages/EmployerServices/OutboundTracker1');
          });
          app.get('/OutboundTrackerReport', function (req, res) {
            res.render('pages/EmployerServices/OutboundTrackerReport1');
          });
          app.get('/CRM/SearchEngineUtility_Yatm', function (req, res) {
            res.render('pages/CRM/CRMdatadownload');
          });
          app.get('/CVFolderMaster', function (req, res) {
            res.render('pages/CRM/CVFolderMaster1');
          });
          app.get('/CVDatabase', function (req, res) {
            res.render('pages/CRM/CVDatabase1');
          });
          app.get('/AdminCVUpload', function (req, res) {
            res.render('pages/CRM/AdminCVUpload');
          });
          app.get('/BulkMailReport', function(req, res){
            res.render('pages/CRM/BulkMailReport1');
          });
        
          
}
  